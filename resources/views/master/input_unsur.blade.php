@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Unsur Biaya</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Unsur Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Unsur Biaya Internal Pusertif.</p>
                <div class="panel">
                    {!! Form::open(['url'=>'master/unsur/input', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>UNSUR BIAYA</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Kode</label>
                            </div>
                            <div class="col-sm-4">
                                <div class="append-icon">
                                    <input type="text" required="required" name="kode"
                                           class="form-control form-white"
                                           placeholder="Kode Unsur Biaya"
                                           value="<?php echo ($unsur != null) ? $unsur->kode : ""; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Nama Unsur Biaya</label>
                            </div>
                            <div class="col-sm-4">
                                <div class="append-icon">
                                    <input type="text" required="required" name="nama_unsur"
                                           class="form-control form-white"
                                           placeholder="Nama Unsur Biya"
                                           value="<?php echo ($unsur != null) ? $unsur->unsur : ""; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Jenis</label>
                            </div>
                            <div class="col-sm-4">
                                <span id="jenis_1">
                                    <select name="jenis" class="form-control"
                                            data-placeholder="Choose one or create new">
                                        @if(isset($jenis))
                                            @foreach($jenis as $item)
                                                <option value="{{$item->jenis}}" {{($unsur != null)?(($item->jenis == $unsur->jenis)? 'selected':''):''}}>{{$item->jenis}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </span>
                                <input disabled type="text" required="required" name="jenis" id="jenis_2"
                                       class="form-control form-white"
                                       placeholder="Jenis Unsur Biaya"
                                        style="display:none">
                            </div>
                            <div class="col-sm-4">
                                <button id="btn_new_jenis" class="jenis btn btn-primary" type="button"><i
                                            class="fa fa-plus"></i></i>New
                                </button>
                                <button id="btn_pilih_jenis" class="jenis btn btn-warning" type="button" style="display: none"><i
                                            class="fa fa-minus"></i></i>Pilih
                                </button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Tarif</label>
                            </div>
                            <div class="col-sm-4">
                                <div class="append-icon">
                                    <input type="text" required="required" name="tarif"
                                           class="form-control form-white"
                                           placeholder="Tarif"
                                           value="<?php echo ($unsur != null) ? $unsur->tarif : ""; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id" value="<?php echo ($unsur != null) ? $unsur->id : 0; ?>">
                            <a href="{{ url('master/unsur') }}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script>
                $('.jenis').click(function () {
                    if($("#jenis_2").is(':hidden')){
                        $('#jenis_1').css({"display": "none"});
                        $('#jenis_1').attr('disabled');
                        $('#jenis_2').show();
                        $('#jenis_2').prop('disabled', false);
                        $('#btn_pilih_jenis').show();
                        $('#btn_new_jenis').hide();
                    }else{
                        $('#jenis_2').css({"display": "none"});
                        $('#jenis_2').attr('disabled');
                        $('#jenis_1').show();
                        $('#jenis_1').prop('disabled', false);
                        $('#btn_new_jenis').show();
                        $('#btn_pilih_jenis').hide();
                    }
                });
            </script>
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->

@endsection