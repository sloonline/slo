@extends('../layout/layout_internal')

@section('page_css')
	<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
	<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Master <strong>Peralatan</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Peralatan</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 portlets">
				<div class="panel">
					{!! Form::open(['url'=>'master/create_peralatan','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
					<div class="panel-header bg-primary">
						<h3><i class="icon-bulb"></i> Form <strong>{{($peralatan != null) ? "Ubah" : "Tambah"}} Peralatan</strong></h3>
						<input type="hidden" class="form-control form-white" name="id" value="{{($peralatan != null) ? $peralatan->id : null}}" />
					</div>
					<div class="panel-content">
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Nama Alat</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->nama_alat}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Merk</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->merk_alat}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Tipe</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->tipe_alat}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Nomor Seri</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->nomor_seri}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Area Bidang</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->bidang->nama_bidang}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Sub Area Bidang</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->subBidang->nama_sub_bidang}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Kondisi Peralatan</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->status->nama_reference}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Lokasi Peralatan</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->lokasi->nama_reference}}" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Kategori Peralatan</label>
							</div>
							<div class="col-sm-9">
								<input type="text" disabled class="form-control form-white" value="{{@$peralatan->kategori->nama_reference}}" />
							</div>
						</div>
						@if(@$peralatan->foto_peralatan != null)
							<div class="form-group">
								<div class="col-sm-3">
									<label class="col-sm-12 control-label">Foto Peralatan</label>
								</div>
								<div class="col-sm-9">
									<div class="file">
										<div class="option-group">
											<img id="preview_img"
											src="{{url('upload/'.$peralatan->foto_peralatan )}}"
											alt="foto" width="200"/>
										</div>
									</div>
								</div>
							</div>
						@endif
						<br><br>
						<div class="nav-tabs2" id="tabs">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#general" data-toggle="tab"><i class="icon-plus"></i>
										History Peralatan
									</a>
								</li>
							</ul>
							<div class="tab-content bg-white">
									<table id="my_table" class="table table-hover table-dynamic">
										<thead>
											<tr>
												<th>No</th>
												<th>Pekerjaan</th>
												<th>Keterangan</th>
												<th>Status</th>
												<th>Tanggal</th>
												<th>Dibuat Oleh</th>
											</tr>
										</thead>
										<tbody>
											<?php  $no =1;?>
											@foreach (@$history_peralatan as $item)
												<tr>
													<td>{{$no}}</td>
													<td>{{@$item->pekerjaan->pekerjaan}}</td>
													<td>{{@$item->keterangan}}</td>
													<td>{{@$item->status_peralatan->nama_reference}}</td>
													<td>{{date('d M Y H:i:s',strtotime(@$item->created_at))}}</td>
													<td>{{@$item->created_by}}</td>
												</tr>
												<?php $no++?>
											@endforeach
										</tbody>
									</table>
							</div>
						</div>
						<br><br>
					</div>
					<hr/>
					<div class="panel-footer clearfix bg-white">
						<div class="pull-right">
							<a href="{{url('/master/peralatan')}}" class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;<i class="icon-ban"></i></a>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	@endsection

	@section('page_script')
		<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
		<script>

		$("#sub_bidang").chained("#bidang");
		@if($peralatan == null || $peralatan->foto_peralatan == null)
		$('#preview_img').hide();
		@endif

		$("#file_foto_peralatan").change(function () {
			$('#preview_img').hide();
			readURL(this);
		});


		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#preview_img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
				$('#preview_img').show();
			}
		}
		</script>
		<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
		<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
		<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
		<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->

	@endsection
