@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Personil Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Personil Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Personil Inspeksi Internal Pusertif.</p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PERSONIL))
                                <div class="btn-group">
                                    <a href="{{ url('/') }}/master/personil_inspeksi/input"
                                       class="btn btn-success btn-square btn-block btn-embossed"><i
                                                class="fa fa-plus"></i> Tambah Personil Inspeksi</a>
                                </div>
                            @endif
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Jabatan Inspeksi</th>
                                <th>Status Pekerja</th>
                                <th>Bidang</th>
                                <th>Sub Bidang</th>
                                <th>Perusahaan</th>
                                <th>Grade</th>
                                <th>No Hp</th>
                                <th style="width: 150px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1 ?>
                            @foreach($personil as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ ($item->user != null) ? $item->user->nip_user : $item->nip}}</td>
                                    <td>{{ ($item->user != null) ? $item->user->nama_user : $item->nama}}</td>
                                    <td>
                                        @foreach($item->jabatan_personil as $row)
                                            {{@$row->jabatan->nama_jabatan}}<br/>
                                        @endforeach
                                    </td>
                                    <td>{{ @$item->status_pekerja->status}}</td>
                                    <td>{{ @$item->bidang->nama_bidang}}</td>
                                    <td>
                                        @foreach($item->personil_bidang as $row)
                                            {{@$row->sub_bidang->nama_sub_bidang}}<br/>
                                        @endforeach</td>
                                    <td>{{ ($item->user != null) ? "PLN JASER" : $item->nama_perusahaan}}</td>
                                    <td>{{ ($item->user != null) ? $item->user->grade_user : $item->grade}}</td>
                                    <td>{{ ($item->user != null) ? $item->user->no_hp_user : $item->no_hp}}</td>
                                    <td>
                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_EDIT_PERSONIL))
                                            <a href="{{ url('/') }}/master/personil_inspeksi/input/{{$item->id}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip"
                                               data-placement="top" data-original-title="Edit"><i
                                                        class="fa fa-pencil-square-o"></i></a>
                                            @if($item->is_aktif == 1)
                                                <a href="{{ url('/') }}/master/personil_inspeksi/isaktif/{{$item->id}}"
                                                   class="btn btn-sm btn-danger btn-square btn-embossed"
                                                   data-rel="tooltip"
                                                   data-placement="top" data-original-title="Non-aktifkan"><i
                                                            class='glyphicon glyphicon-off'></i></a>
                                            @else
                                                <a href="{{ url('/') }}/master/personil_inspeksi/isaktif/{{$item->id}}"
                                                   class="btn btn-sm btn-success btn-square btn-embossed"
                                                   data-rel="tooltip"
                                                   data-placement="top" data-original-title="Aktifkan"><i
                                                            class='glyphicon glyphicon-ok'></i></a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection
