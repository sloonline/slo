<h2>Tambah <strong>Riwayat Training</strong></h2>
<div class="row">
    <div class="col-lg-3">
        <div class="btn-group">
            @if($personil != null)
                <a href="{{url('/master/personil_training/input/'.$personil->id)}}"
                   class="btn btn-success btn-square btn-block btn-embossed"><i
                            class="fa fa-plus"></i> Tambah</a>
            @endif
        </div>
    </div>
</div>
<table class="table table-hover table-dynamic">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama Training</th>
        <th>Penyelenggara</th>
        <th>Periode</th>
        <th>Lokasi</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @if(is_array($personil) || is_object($personil))
        <?php $no = 1; ?>
        @foreach($personil->training as $item)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $item->training->nama_training}}</td>
                <td>{{ $item->training->penyelenggara }}</td>
                <td>{{ $item->training->lokasi }}</td>
                <td>{{ $item->training->periode }}</td>
                <td>
                        <a href="{{url('/master/delete_personil_training/'.$item->id)}}"
                           class="btn btn-sm btn-danger btn-square btn-embossed"
                           onclick="return confirm('Apakah anda yakin untuk menghapus riwayat ini?')"><i
                                    class="fa fa-trash"></i></a>
                </td>
            </tr>
            <?php $no++; ?>
        @endforeach
    @endif
    </tbody>
</table>