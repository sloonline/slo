@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Peralatan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Peralatan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Peralatan</p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{ url('/') }}/master/create_peralatan"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Tambah Peralatan</a>
                            </div>
                        </div>

                        <div class="m-b-20">
                            <div class="row">
                                <div class="col-lg-2" style="margin-right: -100px;">
                                    <strong>SUB BIDANG : </strong>
                                </div>
                                <div class="col-lg-6">
                                    <select class="form-control form-white" data-search="true" name="sub_bidang"
                                            id="sub_bidang">
                                        <option value=""><b>-ALL SUB BIDANG-</b></option>
                                        @foreach($sub_bid as $item)
                                            <option value="{{$item->nama_sub_bidang}}"><b>{{$item->nama_sub_bidang}}</b>
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <table class="table table-hover table-dynamic" id="table-master">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Alat</th>
                                <th>Tipe Alat</th>
                                <th>Bidang</th>
                                <th>Sub Bidang</th>
                                <th>Kategori Alat</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @if(is_array($peralatan) || is_object($peralatan))
                                @foreach($peralatan as $item)
                                    <tr class="row-{{$item->sub_bidang_id}}">
                                        <td>{{ $no }}</td>
                                        <td>{{ @$item->nama_alat}}</td>
                                        <td>{{ @$item->tipe_alat }}</td>
                                        <td>{{ @$item->bidang->nama_bidang }}</td>
                                        <td>{{ @$item->subBidang->nama_sub_bidang }}</td>
                                        <td>{{ @$item->kategori->nama_reference }}</td>
                                        <td>{{ @$item->status->nama_reference }}</td>
                                        <td style="white-space: nowrap;">
                                            <a href="{{url('/master/detail_peralatan/'.$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"></i></a>
                                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_EDIT_MASTER_PERALATAN))
                                                @if(@$item->status->nama_reference  != PERALATAN_DIPAKAI)
                                                    <a href="{{url('/master/create_peralatan/'.$item->id)}}"
                                                       class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                @endif
                                                <a href="{{url('/master/delete_peralatan/'.$item->id)}}"
                                                   class="btn btn-sm btn-danger btn-square btn-embossed"
                                                   onclick="return confirm('Apakah anda yakin untuk menghapus alat ini?')"><i
                                                            class="fa fa-trash"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script>
                {{--var sub_bid = [];--}}
                {{--@foreach($sub_bid as $item)--}}
                        {{--sub_bid.push({{$item->id}});--}}
                {{--@endforeach--}}
                {{--function changeSubBidang() {--}}
                    {{--var val = $('#sub_bidang').val();--}}
                    {{--for(var i = 0 ; i < sub_bid.length ; i++){--}}
                        {{--if(sub_bid[i] == val || val == ""){--}}
                            {{--$(".row-" + sub_bid[i]).css("display", "");--}}
                        {{--}else{--}}
                            {{--$(".row-" + sub_bid[i]).css("display", "none");--}}
                        {{--}--}}
                    {{--}--}}
                {{--}--}}
                $(function () {
                    //        $("#main-grid").DataTable();
                    oTable = $('#table-master').DataTable();   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

                    $('#sub_bidang').each(function () {
                        oTable.search($(this).val()).draw();
                    });

                    $('#sub_bidang').change(function () {
                        oTable.search($(this).val()).draw();
                    });
                });
            </script>
@endsection
