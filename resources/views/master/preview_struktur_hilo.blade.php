@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Struktur HILO</strong></h2>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        @if($lingkup_pekerjaan != null)
                            <div class="row">
                                <div class="col-lg-9">
                                    <h2>Lingkup Pekerjaan :
                                        <strong>{{@$lingkup_pekerjaan->jenis_lingkup_pekerjaan}}</strong></h2>
                                </div>
                            </div>
                        @endif
                        <hr/>
                        <?php
                        $mata_uji = $struktur->where('is_dokumen', '0');
                        $dokumen = $struktur->where('is_dokumen', '1');
                        ?>
                        @if($dokumen->count() > 0)
                            <h2><strong>A. Mata Uji Laik Operasi</strong></h2><br/>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width:70px;text-align:center;">No.</th>
                                <th style="width:450px;text-align:center;">Mata Uji</th>
                                <th style="width:150px;text-align:center;">Kriteria Penilaian SLO</th>
                                <th style="text-align:center;">Kesesuaian</th>
                                <th style="text-align:center;">Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mata_uji as $item)
                                @if(@$item->is_parent == "1")
                                    <tr style='font-weight:bold;font-size:16px;background-color:#319db5;'>
                                        <td>{{@$item->nomor}}</td>
                                        <td colspan='4'>{{@$item->mata_uji}}</td>
                                    </tr>
                                @elseif(@$item->is_parent == "2")
                                    <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;'>
                                        <td>{{@$item->nomor}}</td>
                                        <td colspan='4'>{{@$item->mata_uji}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{@$item->nomor}}</td>
                                        <td>{{@$item->mata_uji}}</td>
                                        <td style="text-align:center;">{{@$item->kriteria}}</td>
                                        <td style="text-align:center;">
                                            <input type="radio" value="1" name="kesesuaian_{{@$item->id}}"> Ya &nbsp;
                                            <input type="radio" value="0" name="kesesuaian_{{@$item->id}}"> Tidak
                                        </td>
                                        <td><textarea cols="12" rows="2" class="form-control form-white"
                                                      placeholder="Keterangan.." name="ket_{{@$item->id}}"></textarea>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        @if($dokumen->count() > 0)
                            <h2><strong>B. Persyaratan Kelengkapan Registrasi dan Penerbitan SLO</strong></h2><br/>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width:70px;text-align:center;">Nomor</th>
                                    <th style="width:450px;text-align:center;">Dokumen</th>
                                    <th style="text-align:center;">Ketersediaan</th>
                                    <th style="text-align:center;">Keterangan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($dokumen as $item)
                                    @if(@$item->is_parent == "1")
                                        <tr style='font-weight:bold;font-size:16px;background-color:#319db5;'>
                                            <td>{{@$item->nomor}}</td>
                                            <td colspan='4'>{{@$item->mata_uji}}</td>
                                        </tr>
                                    @elseif(@$item->is_parent == "2")
                                        <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;'>
                                            <td>{{@$item->nomor}}</td>
                                            <td colspan='4'>{{@$item->mata_uji}}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td>{{@$item->nomor}}</td>
                                            <td>{{@$item->mata_uji}}</td>
                                            <td style="text-align:center;">
                                                <input type="radio" value="1" name="dokumen_kesesuaian_{{@$item->id}}">
                                                Ya &nbsp;
                                                <input type="radio" value="0" name="dokumen_kesesuaian_{{@$item->id}}">
                                                Tidak
                                            </td>
                                            <td><textarea cols="12" rows="2" class="form-control form-white"
                                                          placeholder="Keterangan.."
                                                          name="dokumen_ket_{{@$item->id}}"></textarea></td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection