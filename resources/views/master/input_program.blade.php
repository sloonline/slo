@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plujpns/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Jenis Program</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Program</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">Program untuk surat tugas.</p>
                <div class="panel">
                    {!! Form::open(['url'=>'master/program/store','id'=>'form_rejpster', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <input type="hidden" name="id" value="{{($program == null) ? 0 : $program->id}}">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> <?php echo ($jenis_program != null) ? "Update" : "Input"; ?>
                            <strong>Program</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Surat Tugas</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="hidden" name="id_surat_tugas" value="{{$st->id}}"/>
                                <input type="text" class="form-control form-white" readonly name="surat_tugas"
                                       value="{{$st->nomor_surat}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Jenis Program</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control form-white" data-search="true" name="jp">
                                    @if($program != null)
                                        <option value="{{$program->id_jenis_program}}">{{$program->jenisProgram->jenis_program}}</option>
                                    @endif
                                    @foreach($jenis_program as $item)
                                        <option value="{{$item->id}}">{{$item->jenis_program}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Periode (Tahun)</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="append-icon">
                                    <input type="text" required="required" name="periode"
                                           class="form-control form-white"
                                           value="<?php echo ($program != null) ? $program->periode : ""; ?>">
                                    <i class="fa fa-building"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Deskripsi Program</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="append-icon">
                                    <input type="text" required="required" name="deskripsi"
                                           class="form-control form-white"
                                           value="<?php echo ($program != null) ? $program->deskripsi : ""; ?>">
                                    <i class="fa fa-building"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id" value="<?php echo ($program != null) ? $program->id : 0; ?>">
                            <a href="{{ url('master/create_surat_tugas/'.$st->id) }}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plujpns/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plujpns/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plujpns/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection