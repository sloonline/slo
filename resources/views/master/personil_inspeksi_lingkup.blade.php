<h2>Lingkup Pekerjaan</h2>
<div class="col-lg-12">
  @foreach($tipe_instalasi as $instalasi)
    <div class="panel row">
      <div class="panel-header bg-primary">
        <h3><i class="fa fa-list"></i><strong>{{$instalasi->nama_instalasi}}</strong></h3>
      </div>
      <div class="panel-content ">
        <div class="row">
          @foreach ($instalasi->lingkup_pekerjaan as $item)
            <input type="checkbox" {{(in_array($item->id,$lingkup_personil)) ?"checked" : ""}} name="lingkup_pekerjaan_{{$instalasi->id}}[]" class="form-control" value="{{$item->id}}" /> {{$item->jenis_lingkup_pekerjaan}}<br/>
          @endforeach
        </div>
      </div>
    </div>
  @endforeach
</div>
