@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Training</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Training</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Training Internal Pusertif.</p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PERSONIL))
                                <div class="btn-group">
                                    <a href="{{ url('/') }}/master/training/input"
                                       class="btn btn-success btn-square btn-block btn-embossed"><i
                                                class="fa fa-plus"></i> Tambah Training</a>
                                </div>
                            @endif
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Training</th>
                                <th>Penyelanggara</th>
                                <th>Periode</th>
                                <th>lokasi</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1 ?>
                            @foreach($training as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nama_training }}</td>
                                    <td>{{ $item->penyelenggara }}</td>
                                    <td>{{ $item->periode }}</td>
                                    <td>{{ $item->lokasi }}</td>
                                    <td>
                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_EDIT_PERSONIL))
                                            <a href="{{url('/master/training/input/'.$item->id)}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                        class="fa fa-pencil-square-o"></i></a>
                                            <a href="{{url('/master/delete_training/'.$item->id)}}"
                                               class="btn btn-sm btn-danger btn-square btn-embossed"
                                               onclick="return confirm('Apakah anda yakin untuk menghapus training ini?')"><i
                                                        class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection