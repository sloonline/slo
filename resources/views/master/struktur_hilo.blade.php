@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Struktur HILO</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Struktur HILO</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="row">
                            <div class="col-lg-1">
                                <label>Lingkup Pekerjaan</label>
                            </div>
                            <div class="col-lg-4">
                                <select class="form-control form-white" name="lingkup_pekerjaan"
                                        id="lingkup_pekerjaan">
                                    <option value="">--Pilih Lingkup Pekerjaan--</option>
                                    @foreach($lingkup as $item)
                                        <option value="{{@$item->id}}">{{@$item->jenis_lingkup_pekerjaan}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-3">
                                <a href="{{ url('/') }}/master/create_struktur_hilo/{{@$lingkup_id}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i
                                            class="fa fa-plus"></i> Tambah Komponen</a>
                            </div>
                        </div>
                        <hr/>
                        @if($lingkup_pekerjaan != null)
                            <div class="row">
                                <div class="col-lg-9">
                                    <h2>Lingkup Pekerjaan :
                                        <strong>{{@$lingkup_pekerjaan->jenis_lingkup_pekerjaan}}</strong></h2>
                                </div>
                                <div class="col-lg-3">
                                    <a target="_blank"
                                       href="{{ url('/') }}/master/preview_struktur_hilo/{{@$lingkup_id}}"
                                       class="btn btn-primary btn-square btn-block btn-embossed"><i
                                                class="fa fa-eye"></i> Preview Form HILO</a>
                                </div>
                            </div>
                        @endif
                        <hr/>
                        <table class="table table-hover table-dynamic" id="table">
                            <thead>
                            <tr>
                                <th>Nomor</th>
                                <th style="width: 500px;">Mata Uji</th>
                                <th>Kriteria</th>
                                <th>Level Parent</th>
                                <th>Jenis</th>
                                <th>Urutan</th>
                                <th style="width: 150px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($struktur as $item)
                                <tr>
                                    <td>{{ $item->nomor }}</td>
                                    <td>{{ $item->mata_uji }}</td>
                                    <td>{{ $item->kriteria }}</td>
                                    <td style="white-space: nowrap">{{ ($item->is_parent == 0) ? 'Isian' : (($item->is_parent == 1) ? 'Judul Bagian' : 'Sub Isian') }}</td>
                                    <td style="white-space: nowrap">{{ ($item->is_dokumen == 1) ? 'Pemeriksaan Dokumen' : 'Mata Uji' }}</td>
                                    <td>{{ $item->sort_num }}</td>
                                    <td>
                                        <a href="{{ url('/') }}/master/edit_struktur_hilo/{{$item->id}}"
                                           class="btn btn-sm btn-warning btn-square btn-embossed"
                                           data-rel="tooltip"
                                           data-placement="top" data-original-title="Edit"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a href="{{ url('/') }}/master/delete_struktur_hilo/{{$item->id}}"
                                           class="btn btn-sm btn-danger btn-square btn-embossed"
                                           data-rel="tooltip" onclick="return confirm('Anda yakin menghapus {{@$item->nomor}} {{@$item->mata_uji}} ?')"
                                           data-placement="top" data-original-title="Delete"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script>
                $("#lingkup_pekerjaan").on('change', function () {
                    var val = $("#lingkup_pekerjaan option:selected").val();
                    window.location.href = '{{url('/master/struktur_hilo')}}/' + val;
                });
            </script>
@endsection