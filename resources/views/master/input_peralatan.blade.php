@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>Master <strong>Peralatan</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="#">Master</a></li>
				<li class="active">Peralatan</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<div class="panel">
				{!! Form::open(['url'=>'master/create_peralatan','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>{{($peralatan != null) ? "Ubah" : "Tambah"}} Peralatan</strong></h3>
					<input type="hidden" class="form-control form-white" name="id" value="{{($peralatan != null) ? $peralatan->id : null}}" />
				</div>
				<div class="panel-content">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nama Alat</label>
						</div>
						<div class="col-sm-9">
							<input type="text" class="form-control form-white" name="nama_alat" value="{{($peralatan != null) ? $peralatan->nama_alat : null}}" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Merk</label>
						</div>
						<div class="col-sm-9">
							<input type="text" class="form-control form-white" name="merk_alat" value="{{($peralatan != null) ? $peralatan->merk_alat : null}}" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Tipe</label>
						</div>
						<div class="col-sm-9">
							<input type="text" class="form-control form-white" name="tipe_alat" value="{{($peralatan != null) ? $peralatan->tipe_alat : null}}"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nomor Seri</label>
						</div>
						<div class="col-sm-9">
							<input type="text" class="form-control form-white" name="nomor_seri" value="{{($peralatan != null) ? $peralatan->nomor_seri : null}}"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Area Bidang</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="bidang" id="bidang">
								<option value="">--- Pilih ---</option>
								@foreach($bidang as $item)
								<option value="{{$item->id}}" {{($peralatan != null && $peralatan->bidang_id == $item->id) ? "selected" : ""}}>{{$item->nama_bidang}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Sub Area Bidang</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="sub_bidang" id="sub_bidang">
								<option value="">--- Pilih ---</option>
								@foreach($sub_bidang as $item)
								<option class="{{$item->id_bidang}}" value="{{$item->id}}" {{($peralatan != null && $peralatan->sub_bidang_id == $item->id) ? "selected" : ""}}>{{$item->nama_sub_bidang}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kondisi Peralatan</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="kondisi_alat">
								<option value="">--- Pilih ---</option>
								@foreach($kondisi_alat as $item)
								<option value="{{$item->id}}" {{($peralatan != null && $peralatan->kondisi_alat == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Lokasi Peralatan</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="lokasi_alat">
								<option value="">--- Pilih ---</option>
								@foreach($lokasi_alat as $item)
								<option value="{{$item->id}}" {{($peralatan != null && $peralatan->lokasi_alat == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kategori Peralatan</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="kategori_alat">
								<option value="">--- Pilih ---</option>
								@foreach($kategori_alat as $item)
								<option value="{{$item->id}}" {{($peralatan != null && $peralatan->kategori_alat == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Foto Peralatan</label>
						</div>
						<div class="col-sm-9">
							<div class="file">
								<div class="option-group">
									<span class="file-button btn-primary">Choose File</span>
									<input type="file" name="foto_peralatan" id="file_foto_peralatan" class="custom-file"
									name="avatar" id="avatar" accept="image/*"
									onchange="document.getElementById('uploader').value = this.value;">
									<input type="text" class="form-control form-white" id="uploader"
									placeholder="no file selected" readonly="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
						</div>
						<div class="col-sm-9">
							<img id="preview_img"
							src="{{($peralatan != null && $peralatan->foto_peralatan != null) ?  url('upload/'.$peralatan->foto_peralatan ): "#"}}"
							alt="foto" width="200"/>
						</div>
					</div>
				</div>
				<hr/>
				<div class="panel-footer clearfix bg-white">
					<div class="pull-right">
						<a href="{{url('/master/peralatan')}}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
						<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endsection

	@section('page_script')
	<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
	<script>

		$("#sub_bidang").chained("#bidang");
		@if($peralatan == null || $peralatan->foto_peralatan == null)
		$('#preview_img').hide();
		@endif

		$("#file_foto_peralatan").change(function () {
			$('#preview_img').hide();
			readURL(this);
		});


		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#preview_img').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
				$('#preview_img').show();
			}
		}
	</script>
	<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
	<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
	<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->

	@endsection
