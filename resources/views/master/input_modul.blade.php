@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Datamaster <strong>Modul</strong></h2>
			<div class="breadcrumb-wrapper">
			  <ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="#">Datamaster</a></li>
				<li class="active">Modul</li>
			  </ol>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 portlets">
				<p class="m-t-10 m-b-20 f-16">List Modul Internal Pusertif.</p>
				<div class="panel">
					{!! Form::open(['url'=>'master/modul/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
					<div class="panel-header bg-primary">
					  <h3><i class="fa fa-table"></i> INPUT <strong>Modul</strong></h3>
					</div>
					<div class="panel-content">
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Kode Modul</label>
							</div>
							<div class="col-sm-9">
								<div class="append-icon">
									<input type="text" required="required" name="kode"
										   class="form-control form-white lastname"
										   placeholder="Kode Modul"
										   value="<?php echo ($modul != null) ? $modul->kode : ""; ?>" >
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Nama Modul</label>
							</div>
							<div class="col-sm-9">
								<div class="append-icon">
									<input type="text" required="required" name="nama_modul" 
												  class="form-control form-white lastname"
												  placeholder="Nama Modul"
												  value="<?php echo ($modul != null) ? $modul->nama_modul : ""; ?>" >
								</div>
							</div>
						</div>
					</div>
					<hr />
					<div class="panel-footer clearfix bg-white">
						<div class="pull-right">
							<input type="hidden" name="id" value="<?php echo ($modul != null) ? $modul->id : 0; ?>">
							<a href="{{ url('master/bidang') }}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
							<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
@endsection

@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection