<h2>Tambah <strong>Riwayat Keahlian</strong></h2>
<div class="row">
    <div class="col-lg-3">
        <div class="btn-group">
            @if($personil != null)
                <a href="{{url('/master/keahlian_personil/input')}}"
                   class="btn btn-success btn-square btn-block btn-embossed"><i
                            class="fa fa-plus"></i> Tambah</a>
            @endif
        </div>
    </div>
</div>
<table class="table table-hover table-dynamic">
    <thead>
    <tr>
        <th>No</th>
        <th>Keahlian</th>
        <th>Level</th>
        <th>Periode</th>
        <th>Nomor Sertifikat</th>
        <th>Lembaga</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @if(is_array($personil) || is_object($personil))
        <?php $no = 1; ?>
        @foreach($personil->keahlian as $item)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $item->keahlian->nama_keahlian}}</td>
                <td>{{ $item->level_keahlian }}</td>
                <td>{{ $item->periode }}</td>
                <td>{{ $item->no_sertifikat }}</td>
                <td>{{ $item->lembaga }}</td>
                <td>
                    <a href="{{url('/master/keahlian_personil/input/'.$item->id)}}"
                       class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                class="fa fa-pencil-square-o"></i></a>
                    <a href="{{url('/master/delete_keahlian_personil/'.$item->id)}}"
                       class="btn btn-sm btn-danger btn-square btn-embossed"
                       onclick="return confirm('Apakah anda yakin untuk menghapus riwayat ini?')"><i
                                class="fa fa-trash"></i></a>
                </td>
            </tr>
            <?php $no++; ?>
        @endforeach
    @endif
    </tbody>
</table>
