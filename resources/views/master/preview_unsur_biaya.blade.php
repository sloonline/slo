@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Create <strong>RAB</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    <div class="panel-content">
                        {!! Form::open(array('url'=> '/internal/create_rancangan_biaya', 'files'=> true, 'class'=> 'form-horizontal m-l-0')) !!}
                        <div class="panel panel-default">
                            <div class="panel-header bg-primary">
                                <h2 class="panel-title">PREVIEW <strong>RAB</strong></h2>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 m-l-0">
                                        <div class="panel-content p-0">
                                            <ul class="nav nav-tabs nav-primary">
                                                <?php $no = 0 ?>
                                                @foreach($jenis as $item)
                                                    <li {{($no == 0) ? "class=active":''}}>
                                                        <a href="{{'#'.$item->div_id}}" data-toggle="tab">
                                                            {{$item->jenis}}
                                                        </a>
                                                    </li>
                                                    <?php $no++ ?>
                                                @endforeach
                                            </ul>
                                            <div class="tab-content">
                                                <?php $no = 0 ?>
                                                @foreach($jenis as $i => $item)
                                                    <div class="tab-pane fade {{($no == 0) ? "active in":''}}"
                                                         id="{{$item->div_id}}">
                                                        <table class="table table-bordered m-b-0">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2" colspan="2" class="text-left">NO
                                                                </th>
                                                                <th rowspan="2" colspan="2" class="text-left">
                                                                    KODE
                                                                </th>
                                                                <th rowspan="2" class="text-left"
                                                                    style="width:20%">UNSUR BIAYA
                                                                </th>
                                                                <th rowspan="2" class="text-center"
                                                                    style="width:15%">TARIF
                                                                </th>
                                                                <th colspan="2" class="text-center">HARI ORANG
                                                                    (MD)
                                                                </th>
                                                                <th rowspan="2" class="text-center"
                                                                    style="width:5%">SATUAN
                                                                </th>
                                                                <th rowspan="2" class="text-center"
                                                                    style="width:20%">VOLUME
                                                                </th>
                                                                <th rowspan="2" class="text-center"
                                                                    style="width:20%">JUMLAH BIAYA
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center" style="width:10%">
                                                                    HARI
                                                                </th>
                                                                <th class="text-center" style="width:10%">ORANG
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $no = 1;
                                                            ?>
                                                            @foreach($unsur_arr[$item->jenis] as $j => $unsur_item)
                                                                <input name="{{'rab_detail_'.$item->div_id.'_id[]'}}"
                                                                       type="hidden">
                                                                <input name="{{$item->div_id.'_id[]'}}"
                                                                       type="hidden"
                                                                       class="form-control form-white"/>
                                                                <tr class="item-row">
                                                                    <td colspan="2">{{$no}}</td>
                                                                    <td colspan="2">{{$unsur_item->kode}}</td>
                                                                    <td>{{$unsur_item->unsur}}</td>
                                                                    <td class="text-center"><input
                                                                                id="{{'tarif_'.$i.'_'.$j}}"
                                                                                value="{{$unsur_item->tarif}}"
                                                                                name="{{'tarif_'.$i.'[]'}}"
                                                                                type="number"
                                                                                min="0"
                                                                                style="text-align: right"
                                                                                class="form-control form-white"
                                                                                {{($unsur_item->tarif > 0)?'readonly':''}}>
                                                                    </td>
                                                                    <td class="text-center"><input
                                                                                id="{{'hari_'.$i.'_'.$j}}"
                                                                                name="{{'hari_'.$i.'[]'}}"
                                                                                type="number"
                                                                                min="0"
                                                                                style="text-align: right"
                                                                                class="form-control form-white">
                                                                    </td>
                                                                    <td class="text-center"><input
                                                                                id="{{'orang_'.$i.'_'.$j}}"
                                                                                name="{{'orang_'.$i.'[]'}}"
                                                                                type="number"
                                                                                min="0"
                                                                                style="text-align: right"
                                                                                class="form-control form-white">
                                                                    </td>
                                                                    <td class="text-center">{{($unsur_item->satuan==0)?'HO':'BO'}}</td>
                                                                    <td class="text-center"><input
                                                                                id="{{'jumlah_'.$i.'_'.$j}}"
                                                                                name="{{'jumlah_'.$i.'[]'}}"
                                                                                type="number"
                                                                                min="0"
                                                                                style="text-align: right"
                                                                                class="form-control bg-aero"
                                                                                value="{{@$unsur->jumlah}}"
                                                                                readonly
                                                                        >
                                                                    </td>
                                                                    <td class="text-center"><input
                                                                                id="{{'jumlah_biaya_'.$i.'_'.$j}}"
                                                                                name="{{'jumlah_biaya_'.$i.'[]'}}"
                                                                                type="number" min="0"
                                                                                style="text-align: right"
                                                                                class="form-control bg-aero"
                                                                                value="{{@$unsur->jumlah_biaya}}"
                                                                                readonly>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $no++;
                                                                ?>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php $no++ ?>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end rancangan biaya--}}
                        <div class="panel-content clearfix bg-white p-t-0 p-b-0">
                            <div class="pull-right">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Jumlah Biaya</th>
                                        <th><input name="total_biaya" id="total_biaya" class="form-control form-white"
                                                   type="hidden" readonly min="0"
                                                   style="width:300px;">
                                            <input id="total_biaya_text" class="form-control form-white"
                                                   type="text" readonly min="0"
                                                   style="text-align: right">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>PPN 10%</th>
                                        <th><input name="total_ppn" id="total_ppn" class="form-control form-white"
                                                   type="hidden" readonly min="0"
                                                   style="width:300px">
                                            <input id="total_ppn_text" class="form-control form-white"
                                                   type="text" readonly min="0"
                                                   style="text-align: right">
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>TOTAL BIAYA JASA SETELAH PPN</th>
                                        <th><input name="total_bayar" id="total_bayar"
                                                   class="form-control input-lg bg-aero"
                                                   type="hidden" readonly min="0"
                                                   style="width:300px">
                                            <input id="total_bayar_text" class="form-control input-lg bg-aero"
                                                   type="text" readonly min="0"
                                                   style="text-align: right">
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right p-b-10">
                                <a onclick="window.close()"
                                   class="btn btn-warning btn-square btn-embossed">Kembali <i
                                            class="icon-ban"></i></a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
@endsection