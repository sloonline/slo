@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Pemilik Instalasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pemilik Instalasi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('master/pemilik_instalasi/add')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Tambah Pemilik</a>
                            </div>
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pemilik</th>
                                <th>Provinsi</th>
                                <th>Kota</th>
                                <th>Alamat</th>
                                <th>Jenis Ijin Usaha</th>
                                {{--<th>Nama Kontrak</th>--}}
                                {{--<th>Nomor Kontrak</th>--}}
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach(@$pemilik as $item)
                                <tr>
                                    <td>{{ @$no++ }}</td>
                                    <td>{{ @$item->nama_pemilik }}</td>
                                    <td>{{ @$item->province->province }}</td>
                                    <td>{{ @$item->city->city }}</td>
                                    <td>{{ @$item->alamat_pemilik }}</td>
                                    <td>{{ @$item->jenisIjinUsaha->nama_reference }}</td>
                                    {{--<td>{{ @$item->nama_kontrak }}</td>--}}
                                    {{--<td>{{ @$item->no_kontrak }}</td>--}}
                                    <td width="20%">
                                        <a href="{{url('/master/pemilik_instalasi/detail/'.@$item->id)}}"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"></i></a>
                                        {{--@if(@$item->perusahaan->id == Auth::user()->perusahaan_id )--}}
                                        <a href="{{url('/master/pemilik_instalasi/edit/'.@$item->id)}}"
                                           class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                    class="fa fa-pencil-square-o"></i></a>

                                        <a href="{{url('/master/pemilik_instalasi/delete/'.@$item->id)}}"
                                           class="btn btn-sm btn-danger btn-square btn-embossed"
                                           onclick="return confirm('Apakah anda yakin untuk menghapus pemilik instalasi ini?')"><i
                                                    class="fa fa-trash"></i></a>
                                        {{--@endif--}}
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection