@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Bidang</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Bidang</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    {!! Form::open(['url'=>'master/create_struktur_hilo','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>STRUKTUR FORM HILO</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" readonly
                                       class="form-control"
                                       value="{{@$lingkup->jenis_lingkup_pekerjaan}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Nomor</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" required="required" name="nomor"
                                       class="form-control form-white"
                                       placeholder="Nomor"
                                       value="{{@$struktur->nomor}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Mata Uji</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" required="required" name="mata_uji"
                                       class="form-control form-white"
                                       placeholder="Deskripsi mata uji"
                                       value="{{@$struktur->mata_uji}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Kriteria</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="kriteria"
                                       class="form-control form-white"
                                       placeholder="Deskripsi kriteria"
                                       value="{{@$struktur->kriteria}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Level</label>
                            </div>
                            <div class="col-sm-9">
                                <select required="required" name="is_parent"
                                        class="form-control form-white"
                                        placeholder="Deskripsi mata uji">
                                    <option {{(@$struktur->is_parent == 1) ? 'selected' : ''}} value="1">Judul Bagian</option>
                                    <option {{(@$struktur->is_parent == 0) ? 'selected' : ''}} value="0">Isian</option>
                                    <option {{(@$struktur->is_parent == 2) ? 'selected' : ''}} value="2">Sub Isian</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Jenis</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="radio" required name="is_dokumen"
                                       class="form-control form-white"  {{(@$struktur->is_dokumen == 0) ? 'checked' : ''}}
                                       value="0"/> Mata Uji &nbsp;&nbsp;
                                <input type="radio" required name="is_dokumen"
                                       class="form-control form-white"  {{(@$struktur->is_dokumen == 1) ? 'checked' : ''}}
                                       value="1"/> Pemeriksaan Dokumen
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Urutan</label>
                            </div>
                            <div class="col-sm-2">
                                <input type="number" name="sort_num"
                                       class="form-control form-white" min="1"
                                       value="{{@$struktur->sort_num}}">
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id" value="{{@$id}}"/>
                            <input type="hidden" name="lingkup_id" value="{{@$lingkup_id}}"/>
                            <a href="{{ url('master/struktur_hilo/'.$lingkup_id) }}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection