@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Riwayat Kompetensi Personil</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Riwayat Kompetensi Personil</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Riwayat Kompetensi Personil Internal Pusertif.</p>
                <div class="panel">
                    {!! Form::open(['url'=>'master/keahlian_personil/input','files'=> true,'id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>BIDANG</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Nama Personil</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" readonly name="nama_keahlian_personil"
                                       class="form-control form-white lastname"
                                       value="{{$personil->nama}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Kompetensi</label>
                            </div>
                            <div class="col-sm-9">
                                <select required="required" name="keahlian_id" class="form-control form-white lastname"
                                        placeholder="Keahlian">
                                    <option value="">--Pilih Kompetensi--</option>
                                    @foreach($keahlian as $row)
                                        <option value="{{$row->id}}" {{($keahlian_personil != null && $keahlian_personil->keahlian_id == $row->id) ? "selected" : "" }}>{{$row->nama_keahlian}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Level Kompetensi</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="number" name="level_keahlian" min="1"
                                       class="form-control form-white lastname"
                                       placeholder="Level Keahlian Kompetensi"
                                       value="<?php echo ($keahlian_personil != null) ? $keahlian_personil->level_keahlian : ""; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Periode</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" required="required"
                                       name="periode" id="periode"
                                       value="{{(@$keahlian_personil == null) ? "":  date('d-m-Y',strtotime(@$keahlian_personil->periode))}}"
                                       class="form-control b-datepicker form-white"
                                       data-date-format="dd-mm-yyyy"
                                       data-lang="en"
                                       data-RTL="false"
                                       placeholder="Periode">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">No. Sertifikat</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="no_sertifikat"
                                       class="form-control form-white lastname"
                                       placeholder="Nomor Sertifikat Kompetensi"
                                       value="<?php echo ($keahlian_personil != null) ? $keahlian_personil->no_sertifikat : ""; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">File Sertifikat</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="file">
                                    <div class="option-group">
                                                        <span class="file-button btn-primary">
                                                        {{($keahlian_personil != null && $keahlian_personil->file_sertifikat != null) ? "Ganti File" : "Choose File"}}
                                                        </span>
                                        <input type="file" class="custom-file" name="file_sertifikat"
                                               onchange="document.getElementById('uploader').value = this.value;" {{($keahlian_personil == null) ? "required" : ""}}>
                                        <input type="text" class="form-control form-white" id="uploader"
                                               placeholder="no file selected"
                                               value="{{($keahlian_personil != null && $keahlian_personil->file_sertifikat != null) ? $keahlian_personil->file_sertifikat : null}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($keahlian_personil != null && $keahlian_personil->file_sertifikat != null){
                        ?>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label"></label>
                            </div>
                            <div class="col-sm-9">
                                <a href="{{url('upload/'.$keahlian_personil->file_sertifikat)}}" class="btn btn-primary"><i
                                            class="fa fa-download"></i>View</a>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Lembaga</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="lembaga"
                                       class="form-control form-white lastname"
                                       placeholder="Lembaga Sertifikasi"
                                       value="<?php echo ($keahlian_personil != null) ? $keahlian_personil->lembaga : ""; ?>">
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id" value="{{$id}}">
                            <input type="hidden" name="personil_id" value="{{$personil_id}}">
                            <a href="{{ url('/master/personil_inspeksi/input/'.$personil_id)  }}"
                               class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection
