@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Personil Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Personil Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">Input Personil Inspeksi Internal Pusertif.</p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-bookmark"></i><strong>Personil Inspeksi</strong>
                        </h3>
                    </div>
                    <div class="panel-content">
                      {!! Form::open(['url'=>'master/personil_inspeksi/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
                      <div class="nav-tabs2" id="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                        General</a></li>
                                <li><a href="#lingkup_pekerjaan" data-toggle="tab"><i
                                                class="icon-wrench"></i>Lingkup Pekerjaan</a></li>
                                @if($personil != null)
                                <li><a href="#training"  data-toggle="tab"><i
                                                class="icon-enroll"></i>
                                        Training</a></li>
                                <li><a href="#keahlian" data-toggle="tab"><i
                                                class="icon-user"></i>Kompetensi</a></li>
                                @endif
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active" id="general">
                                    <input type="hidden" name="user_id" id="user_id"
                                           value="{{($personil != null) ? $personil->id_user : 0}}"/>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <img id="preview_img"
                                                 src="{{($personil != null) ?  url('upload/'.(($personil->user == null) ? $personil->foto_personil : $personil->user->file_foto_user)): "#"}}"
                                                 alt="foto" width="300"/>
                                        </div>
                                        <div class="col-lg-7">
                                            <input type="hidden" name="id" value="{{$id}}"/>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Username</label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="append-icon">
                                                        <input type="text" id="username" readonly name="username"
                                                               id="username"
                                                               class="form-control form-white"
                                                               placeholder="Username Pengguna"
                                                               style="background-color: gainsboro"
                                                               value="<?php echo ($personil != null && $personil->user != null) ? $personil->user->username : ""; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a id="btn-search" href="#modal-select" style="margin-left: -5px;"
                                                       data-toggle="modal" class="btn btn-sm btn-primary"
                                                       data-rel="tooltip" data-placement="left"
                                                       data-original-title="Cari Pengguna"><i
                                                                class="fa fa-search"></i></a>
                                                </div>
                                                <div class="col-sm-1" style="margin-left: -5px;">
                                                    <span id="btn-baru" onclick="resetToogle()"
                                                          class="btn btn-sm btn-warning"
                                                          data-rel="tooltip" data-placement="left"
                                                          data-original-title="Pendaftaran Baru"><i
                                                                class="fa fa-refresh"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIP</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" id="nip" required="required" name="nip"
                                                               class="form-control form-white"
                                                               placeholder="NIP Personil"
                                                               value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->nip : $personil->user->nip_user) : ""; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" required="required" id="nama" name="nama"
                                                               class="form-control form-white"
                                                               placeholder="Nama Lengkap"
                                                               value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->nama : $personil->user->nama_user) : ""; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Foto Personil</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="file">
                                                        <div class="option-group">
                                                            <span id="btn-file" class="file-button btn-primary">Choose File</span>
                                                            <input type="file" name="foto_personil"
                                                                   id="file_foto_personil"
                                                                   class="custom-file"
                                                                   name="avatar" id="avatar" accept="image/*"
                                                                   onchange="document.getElementById('uploader').value = this.value;">
                                                            <input id="file_foto_personil2" type="text"
                                                                   class="form-control form-white"
                                                                   id="uploader"
                                                                   placeholder="no file selected" readonly="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Email</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="email" name="email" id="email"
                                                               class="form-control form-white" placeholder="Email"
                                                               value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->email : $personil->user->email) : ""; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tempat Lahir</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" name="tempat_lahir" id="tempat_lahir"
                                                               class="form-control form-white"
                                                               placeholder="Tempat Lahir"
                                                               value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->tempat_lahir : $personil->user->tempat_lahir_user) : ""; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Lahir</label>
                                                </div>
                                                <div class="col-sm-9 prepend-icon">
                                                    <input name="tanggal_lahir" id="tanggal_lahir"
                                                           value="{{($personil != null) ?  (($personil->user == null) ? $personil->tanggal_lahir : date('d/m/y',strtotime($personil->user->tanggal_lahir)) ): ""}}"
                                                           type="text"
                                                           class="date-picker form-control form-white"
                                                           placeholder="Tanggal Lahir"
                                                           required>
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-9">
                                <textarea rows="3" name="alamat" id="alamat"
                                          class="form-control form-white"
                                          placeholder="Alamat Lengkap">{{($personil != null)? (($personil->user == null) ? $personil->alamat : $personil->user->alamat_user) : null}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor HP (Kontak)</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" name="no_hp" id="no_hp"
                                                               class="form-control form-white"
                                                               placeholder="Nomor Kontak"
                                                               value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->no_hp : $personil->user->no_hp_user) : ""; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            if($personil != null){
                                                $jenis_kelamin = ($personil->user == null) ? $personil->jenis_kelamin :$personil->user->gender_user;
                                                $status_pernikahan = ($personil->user == null) ? $personil->status_menikah : $personil->user->status_pernikahan;
                                            }else{
                                                $jenis_kelamin = "PRIA";
                                                $status_pernikahan = "SINGLE";
                                            }
                                            ?>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jenis Kelamin</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <select name="jenis_kelamin" id="jenis_kelamin"
                                                            class="form-control form-white"
                                                            data-style="white" data-placeholder="Jenis Kelamin">
                                                        <option value="PRIA" {{($jenis_kelamin == "PRIA")? "selected": ""}}>
                                                            Pria
                                                        </option>
                                                        <option value="WANITA" {{($jenis_kelamin == "WANITA") ? "selected" : ""}}>
                                                            Wanita
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Status Menikah</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <select name="status_menikah" id="status_menikah"
                                                            class="form-control form-white"
                                                            data-style="white" data-placeholder="Status Menikah">
                                                        <option value="">--Pilih Status Menikah--</option>
                                                        <option value="SINGLE" {{($status_pernikahan == "SINGLE")? "selected": ""}}>
                                                            Lajang
                                                        </option>
                                                        <option value="MENIKAH" {{($status_pernikahan == "MENIKAH") ? "selected" : ""}}>
                                                            Menikah
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Status Pekerja</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <select required="required" name="id_status_pekerja"
                                                                class="form-control form-white lastname"
                                                                placeholder="Status Pekerja">
                                                            <option value="">--Pilih Status Pekerja--</option>
                                                            @foreach($status as $row)
                                                                <option value="{{$row->id}}" {{($personil != null && $personil->id_status_pekerja == $row->id) ? "selected" : "" }}>{{$row->status}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jabatan Inspeksi</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        @foreach($jabatan as $row)
                                                          <input type="checkbox" {{(in_array($row->id,$jabatan_personil)) ?"checked" : ""}} name="id_jabatan_inspeksi[]" class="form-control" value="{{$row->id}}" />{{$row->nama_jabatan}}<br/>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Bidang</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <select name="id_bidang" id="id_bidang" required="required"
                                                                class="form-control form-white" onchange="getSubBidang()"
                                                                data-style="white" data-placeholder="Pilih bidang...">
                                                                <option value="0">--Pilih Bidang--</option>
                                                            @foreach($bidang as $row)
                                                                <option value="{{$row->id}}" {{($personil != null && $personil->id_bidang == $row->id) ? "selected" : "" }}> {{$row->nama_bidang}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Sub Bidang</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                      @foreach ($sub_bidang->groupBy('id_bidang') as $key => $sub_bidang)
                                                        <div id="sub_bidang_{{$key}}" class="box_sub_bidang">
                                                          @foreach ($sub_bidang as $item)
                                                            <input type="checkbox" {{(in_array($item->id,$personil_bidang)) ?"checked" : ""}} name="sub_bidang_{{$key}}[]" value="{{$item->id}}" class="form-control" />{{$item->nama_sub_bidang}}<br/>
                                                          @endforeach
                                                        </div>
                                                      @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Grade</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text" name="grade" id="grade"
                                                           class="form-control form-white" placeholder="Grade"
                                                           value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->grade : $personil->user->grade_user) : ""; ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text" name="nama_perusahaan" required="required"
                                                           id="nama_perusahaan"
                                                           class="form-control form-white" placeholder="Nama Perusahaan"
                                                           value="<?php echo ($personil != null) ? (($personil->user == null) ? $personil->nama_perusahaan : "PLN JASER") : ""; ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="training">
                                    @include('master/personil_inspeksi_training')
                                </div>
                                <div class="tab-pane" id="keahlian">
                                    @include('master/personil_inspeksi_keahlian')
                                </div>
                                <div class="tab-pane" id="lingkup_pekerjaan">
                                    @include('master/personil_inspeksi_lingkup')
                                </div>
                            </div>
                        </div>

                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a href="{{ url('master/personil_inspeksi') }}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali
                                    &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit"
                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                      {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN MODAL USER SELECT -->
    <div class="modal fade" id="modal-select" style="margin-left: -150px" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: teal;color:white;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="icons-office-52"></i></button>
                    <h4 class="modal-title"><strong>Pencarian</strong> user</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-hover table-dynamic">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Bidang & Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                ?>
                                @foreach($user as $row)
                                    <?php
                                        $row->tanggal_lahir = date('d/m/y',strtotime($row->tanggal_lahir));
                                        $row->alamat_user = trim($row->alamat_user);
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->nip_user}}</td>
                                        <td>{{$row->nama_user}}</td>
                                        <td>{{$row->username}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>{{(($row->sub_bidang != null) ? $row->sub_bidang->nama_sub_bidang : "")." ".(($row->bidang != null) ? $row->bidang->nama_bidang : "" )." (".$row->jabatan_user.")"}}</td>
                                        <td>
                                            <button class="btn btn-success" onclick="setUser('{{json_encode($row)}}')">
                                                Pilih
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL USER SELECT-->

@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
    <script type="text/javascript">
        // $("#sub_bidang").chained("#id_bidang");
        $("#btn-baru").hide();
        getSubBidang();
        @if($personil != null)
            $("#btn-search").hide();
        @if($personil != null && $personil->id_user != null)
            disableToogle();
        $("#btn-baru").hide();
        @endif
    @endif
    @if($personil == null)
        $('#preview_img').hide();
        @endif

        $("#file_foto_personil").change(function () {
            $('#preview_img').hide();
            readURL(this);
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('#preview_img').show();
            }
        }

        var tab = "general";
        @if(Session::get('tab') != null)
                tab = "{{Session::get('tab')}}";
        @endif
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');

        //set detail by user
        function setUser(data) {
            data = jQuery.parseJSON(data);
            console.log(data);
            $("#user_id").val(data.id);
            $("#username").val(data.username);
            $("#nama").val(data.nama_user);
            $("#nip").val(data.nip_user);
            $("#email").val(data.email);
            $("#tempat_lahir").val(data.tempat_lahir_user);
            $("#tanggal_lahir").val(data.tanggal_lahir);
            $("#alamat").val(data.alamat_user);
            $("#no_hp").val(data.no_hp_user);
            $("#grade").val(data.grade_user);
            $("#nama_perusahaan").val('PLN JASER');
            $("#status_menikah").val(data.status_pernikahan);
            $("#preview_img").attr('src', "{{url('upload')}}/" + data.file_foto_user);
            $('#preview_img').show();
            $("#btn-baru").show();
            disableToogle();
            $("#modal-select").modal('toggle');
        }

        function resetToogle(data) {
            $("#user_id").val("0");
            $("#username").val("");
            $("#nama").val("");
            $("#nip").val("");
            $("#email").val("");
            $("#tempat_lahir").val("");
            $("#tanggal_lahir").val("");
            $("#alamat").val("");
            $("#no_hp").val("");
            $("#grade").val("");
            $("#btn-baru").hide();
            $('#file_foto_personil').show();
            $('#file_foto_personil2').show();
            $("#preview_img").attr('src', "");
            $('#preview_img').hide();
            $('#btn-file').hide();
            $("#nama").removeAttr('readonly');
            $("#nip").removeAttr('readonly');
            $("#email").removeAttr('readonly');
            $("#tempat_lahir").removeAttr('readonly');
            $("#tanggal_lahir").removeAttr('readonly');
            $("#alamat").removeAttr('readonly');
            $("#no_hp").removeAttr('readonly');
            $("#grade").removeAttr('readonly');
            $("#nama_perusahaan").removeAttr('readonly');
        }
        function disableToogle() {

            $('#file_foto_personil').hide();
            $('#file_foto_personil2').hide();
            $('#btn-file').hide();
            $("#nama").attr('readonly', "readonly");
            $("#nip").attr('readonly', "readonly");
            $("#email").attr('readonly', "readonly");
            $("#tempat_lahir").attr('readonly', "readonly");
            $("#tanggal_lahir").attr('readonly', "readonly");
            $("#alamat").attr('readonly', "readonly");
            $("#no_hp").attr('readonly', "readonly");
            $("#grade").attr('readonly', "readonly");
            $("#nama_perusahaan").attr('readonly', "readonly");
        }
        function getSubBidang(){
            $(".box_sub_bidang").hide();
            var bidang = $("#id_bidang option:selected").val();
            $("#sub_bidang_"+bidang).show('medium');
        }

    </script>
@endsection
