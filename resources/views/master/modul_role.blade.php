@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/multi-select/css/pickList.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Hak Akses Modul</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Hak Akses Modul</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Hak Akses Modul Pusertif.</p>
                <div class="panel">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> MANAJEMEN <strong>LEVEL HAK AKSES</strong></h3>
                    </div>
                    <div class="panel-content">
                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                            </div>
                        @endif
                        {!! Form::open(['url'=>'master/hak_akses_modul','id'=>'form_role', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Level Akses</label>
                            </div>
                            <div class="col-sm-4">
                                <select required="required" name="id_role"
                                        class="form-control form-white"
                                        placeholder="role">
                                    @foreach($roles as $row)
                                        <option value="{{$row->id}}" {{(isset($role) && $role->id == $row->id) ? "selected" : "" }}>{{$row->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-warning btn-square btn-embossed">Pilih &nbsp;<i
                                            class="glyphicon glyphicon-wrench"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        @if(isset($role))
                            <hr/>
                            @foreach($modules as $module)
                                <div class="panel">
                                    <div class="panel-header bg-primary">
                                        {{$module->kode ." - ".$module->nama_modul}}
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 portlets">
                                            <div id="pickList{{$module->id}}"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {!! Form::open(['url'=>'master/hak_akses_modul/','id'=>'form_role_modul', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                            <input type="hidden" name="id_role" value="{{isset($role) ? $role->id : ''}}"/>
                            <input type="hidden" name="arr_permission" id="arr_permission"/>
                            <hr/>
                            <div class="row">
                                <div class="col-lg-12 portlets">
                                    <div class="pull-right">
                                        <button type="submit" id="simpan"
                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                data-style="zoom-in">Simpan &nbsp;<i
                                                    class="glyphicon glyphicon-floppy-saved"></i></button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.3.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap-3.3.6.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/multi-select/js/pickList.js"></script>
            <script type="text/javascript">
                        @if(isset($role))
                var permission = new Array();
                var modul_selected = new Array();
                var picks = new Array();

                //set array modul yg bisa dipilih
                @foreach($modules as $row)
                        permission = new Array();
                var option = "";
                @foreach($permission  as $perm)
                    @if($perm->modul_id == $row->id)
                        permission.push(
                        {id: {{ $perm->id}}, text: "{{$perm->description}}"}
                );
                @endif
                @endforeach
                //set array modul yg sudah dipilih
                @foreach($permission_role  as $perm)
                        @if($perm->permission->modul_id == $row->id)
                        option += '<option id={{$perm->permission_id}}>{{$perm->permission->description}}<option>';
                        @endif
                        @endforeach
                var pick = $("#pickList{{$row->id}}").pickList({data: permission, parentId: "{{$row->id}}"});
                picks.push(pick);
                //append modul ke pick list kiri
                $('#pickListResult{{$row->id}}').append(option);
                @endforeach
                @endif

                //manipulasi modul yg dipilih disimpan ke text box, nantinya di explode untuk diproses
                $("#simpan").click(function () {
                    var ids = "";
                    $.each(picks, function (key, pick) {
                        var val = pick.getValues();
                        for (var i = 0; i < val.length; i++) {
                            ids = ids + " " + val[i].id;
                        }
                    });
                    $("#arr_permission").val(ids);
                });
            </script>
@endsection