@extends('../layout/layout_public')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Lokasi <strong>Instalasi</strong></h2>
            <h4 style="margin: 0 0 0 0">PT PLN (Persero) - Pusat Sertifikasi</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <fieldset class="cart-summary">
                            <a href="#" class="btn btn-warning"><i class="icon-map"></i> Lokasi</a>
                            <a target="_blank" href="{{url('public/lhpp/'.@$lhpp->id)}}" class="btn btn-primary"><i class="icon-eye"></i> LHPP</a>
                            <a target="_blank" href="{{url('public/lhpp/foto/'.@$lhpp->id)}}" class="btn btn-success"><i class="fa fa-photo"></i> Photo</a>
                        </fieldset>
                        <fieldset class="cart-summary">
                            <legend>Lokasi Instalasi</legend>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td width="300px">Nomor LHPP</td>
                                    <td width="10px">:</td>
                                    <td>{{@$lhpp->no_lhpp}}</td>
                                </tr>
                                <tr>
                                    <td>Nomor SLO</td>
                                    <td width="10px">:</td>
                                    <td>{{@$lhpp->no_slo}}</td>
                                </tr>
                                <tr>
                                    <td>Longitude Awal</td>
                                    <td width="10px">:</td>
                                    <td>{{@$ltlng['long_awal']}}</td>
                                </tr>
                                <tr>
                                    <td>Latitude Awal</td>
                                    <td width="10px">:</td>
                                    <td>{{@$ltlng['lat_awal']}}</td>
                                </tr>
                                @if(@$ltlng['long_akhir'] != null)
                                    <tr>
                                        <td>Longitude Akhir</td>
                                        <td width="10px">:</td>
                                        <td>{{@$ltlng['long_akhir']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Latitude Akhir</td>
                                        <td width="10px">:</td>
                                        <td>{{@$ltlng['lat_akhir']}}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div id="mapid" style="width: 100%; height: 500px; z-index: 1; position:relative"></div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();
                });

                generateMap();

                function generateMap() {
                    //First, specify your Mapbox API access token
                    var mymap = L.map('mapid');
                    var instalasi_ltlng = {!! $ltlng !!};
                    var ltlng = [-6.1751, 106.8650];
                    var ltlng_line = [];
                    var isMark = false;
                    var isPoly = false;

                    if (instalasi_ltlng['long_awal'] != null && instalasi_ltlng['lat_awal'] != null) {
                        isMark = true;
                        ltlng = [instalasi_ltlng['lat_awal'], instalasi_ltlng['long_awal']];
                    }

                    if (instalasi_ltlng['long_akhir'] != null && instalasi_ltlng['lat_akhir'] != null) {
                        if(instalasi_ltlng['long_akhir'] == instalasi_ltlng['long_awal'] && instalasi_ltlng['lat_akhir'] == instalasi_ltlng['lat_awal']){
                            isMark = true;
                            ltlng = [instalasi_ltlng['lat_awal'], instalasi_ltlng['long_awal']];
                        }else {
                            ltlng_line = [[instalasi_ltlng['lat_awal'], instalasi_ltlng['long_awal']],
                                [instalasi_ltlng['lat_akhir'], instalasi_ltlng['long_akhir']]];
                            isPoly = true;
                        }
                    }

                    console.log(instalasi_ltlng);
                    mymap.setView(ltlng, 10);

                    if (isPoly) {
                        var polyline = L.polyline(ltlng_line, {color: 'blue'}).addTo(mymap);
                        mymap.fitBounds(polyline.getBounds());
                    } else if (isMark) {
                        var marker = L.marker(ltlng).addTo(mymap);
                    }

                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                        maxZoom: 18,
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
                        id: 'mapbox.streets'
                    }).addTo(mymap);
                }
            </script>
@endsection
    