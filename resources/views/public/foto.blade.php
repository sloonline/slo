@extends('../layout/layout_public')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <style>
        textarea {
            resize: both;
        }
    </style>
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Foto <strong>Pelaksanaan Inspeksi</strong></h2>
            <h4 style="margin: 0 0 0 0">PT PLN (Persero) - Pusat Sertifikasi</h4>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">LHPP</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="panel-content pagination2 table-responsive">
                            <div class="panel-content pagination2 table-responsive">
                                <fieldset class="cart-summary">
                                    <a target="_blank" href="{{url('public/lhpp/lokasiInstalasi/'.@$lhpp->id)}}" class="btn btn-warning"><i class="icon-map"></i> Lokasi</a>
                                    <a target="_blank" href="{{url('public/lhpp/'.@$lhpp->id)}}" class="btn btn-primary"><i class="icon-eye"></i> LHPP</a>
                                    <a href="#" class="btn btn-success"><i class="fa fa-photo"></i> Photo</a>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Foto - foto Pelaksanaan</legend>
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <td width="300px">Nomor LHPP</td>
                                            <td width="10px">:</td>
                                            <td>{{@$lhpp->no_lhpp}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor SLO</td>
                                            <td width="10px">:</td>
                                            <td>{{@$lhpp->no_slo}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Instalasi</td>
                                            <td width="10px">:</td>
                                            <td>{{@$lhpp->instalasi->nama_instalasi}}</td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi Instalasi</td>
                                            <td width="10px">:</td>
                                            <td>{{@$lhpp->instalasi->alamat_instalasi}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Papan Nama Lokasi Instalasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[0] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[0])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Instalasi Secara Umum</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[1] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[1])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Pelaksanaan Inspeksi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[2] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[2])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Rapat Kickoff Meeting</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[3] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[3])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();
                });
            </script>
@endsection
    