@extends('../layout/layout_public')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <style>
        textarea {
            resize: both;
        }
    </style>
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Laporan Hasil <strong>Pemeriksaan dan Pengujian</strong></h2>
            <h4 style="margin: 0 0 0 0">PT PLN (Persero) - Pusat Sertifikasi</h4>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">LHPP</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="panel-content pagination2 table-responsive">
                            <div class="panel-content pagination2 table-responsive">
                                <fieldset class="cart-summary">
                                    <a target="_blank" href="{{url('public/lhpp/lokasiInstalasi/'.@$lhpp->id)}}" class="btn btn-warning"><i class="icon-map"></i> Lokasi</a>
                                    <a href="#" class="btn btn-primary"><i class="icon-eye"></i> LHPP</a>
                                    <a target="_blank" href="{{url('public/lhpp/foto/'.@$lhpp->id)}}" class="btn btn-success"><i class="fa fa-photo"></i> Photo</a>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor LHPP</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input value='{{@$lhpp->no_lhpp}}' class="form-control form-white" readonly
                                                   name="no_permohonan">
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group col-lg-12"></div>
                                <div class="form-group col-lg-12"></div>
                                <div class="form-group col-lg-12"></div>
                                <fieldset class="cart-summary">
                                    <legend>Sertifikasi</legend>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor SLO</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->no_slo}}' class="form-control form-white" readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Agenda DJK</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$no_agenda}}' class="form-control form-white" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Permohonan DJK</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$no_permohonan_djk}}' class="form-control form-white" name="no_permohonan"
                                                   readonly>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Data Umum</legend>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Permohonan</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->no_permohonan}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama Pemohon</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->nama_pemohon}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Surat</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->no_surat}}' class="form-control form-white" readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal Surat</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->tgl_surat}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama Pemilik</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->instalasi->pemilik->nama_pemilik}}' readonly
                                                   class="form-control form-white" name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Alamat Pemilik</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->instalasi->pemilik->alamat_pemilik}}' readonly
                                                   class="form-control form-white" name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tipe Instalasi Tegangan
                                                Listrik</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->tipeInstalasi->nama_instalasi}}' readonly
                                                   class="form-control form-white" name="no_permohonan">
                                        </div>

                                    </div>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Badan Usaha Jasa Pembangunan dan Pemasangan Instalasi</legend>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->nama_badan_usaha}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Alamat</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->alamat_badan_usaha}}' readonly
                                                   class="form-control form-white" name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kode Badan Usaha</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->kode_badan_usaha}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Klasifikasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->klasifikasi}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kualifikasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->kualifikasi}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Penanggung Jawab Badan Usaha</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->pj_badan_usaha}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Penanggung Jawab Teknik</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->pjt_badan_usaha}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Masa Berlaku</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->masa_berlaku}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Gambar Main Single Line
                                                Diagram</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->file_gbr_msl_diagram != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->file_gbr_msl_diagram)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @endif
                                        </div>

                                    </div>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Pelaksanaan Uji Laik Operasi</legend>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Waktu Pelaksanaan</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->waktu_pelaksanaan}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->no_spkppp}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value='{{@$lhpp->tgl_spkppp}}' class="form-control form-white"
                                                   readonly
                                                   name="no_permohonan">
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12"></div>
                                    <div class="form-group col-lg-12"></div>
                                    <fieldset class="cart-summary">
                                        <legend>Ketua Tim Pelaksana</legend>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->ketua_pelaksana()->nama}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor Register Kompetensi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->ketua_pelaksana()->no_reg_kompetensi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Klasifikasi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->ketua_pelaksana()->klasifikasi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Level</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->ketua_pelaksana()->level}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                    </fieldset>
                                    <fieldset class="cart-summary">
                                        <legend>Anggota</legend>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->anggota()->nama}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor Register Kompetensi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->anggota()->no_reg_kompetensi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Klasifikasi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->anggota()->klasifikasi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Level</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->anggota()->level}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                    </fieldset>
                                    <fieldset class="cart-summary">
                                        <legend>Penanggung Jawab Teknik</legend>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->pjt()->nama}}' class="form-control form-white"
                                                       readonly
                                                       name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor Register Kompetensi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->pjt()->no_reg_kompetensi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Klasifikasi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->pjt()->klasifikasi}}' readonly
                                                       class="form-control form-white" name="no_permohonan">
                                            </div>

                                        </div>
                                        <div class="form-group col-lg-12">

                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Level</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->pjt()->level}}' class="form-control form-white"
                                                       readonly
                                                       name="no_permohonan">
                                            </div>

                                        </div>
                                    </fieldset>
                                    <fieldset class="cart-summary">
                                        <legend>PJT & TT *(Registrasi SLO DJK)</legend>
                                        <div class="form-group col-lg-12">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama PJT</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->pjt_djk->nama}}' readonly
                                                       class="form-control form-white">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama TT</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value='{{@$lhpp->tt_djk->nama}}' readonly
                                                       class="form-control form-white">
                                            </div>
                                        </div>
                                    </fieldset>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Foto Pelaksanaan</legend>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Papan Nama Lokasi Instalasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[0] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[0])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Instalasi Secara Umum</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[1] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[1])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Pelaksanaan Inspeksi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[2] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[2])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Rapat Kickoff Meeting</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$foto[3] != null)
                                                <img id="preview_img"
                                                     src="{{url('upload/'.@$foto[3])}}"
                                                     alt="foto" width="200"/>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="cart-summary">
                                    <legend>Dokumen dan Kesimpulan</legend>
                                    @if(@$lhpp->tipeInstalasi->id == ID_PEMBANGKIT)
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File Konsumsi Bahan Bakar</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->lhpp_kit != null)
                                                <a class="btn btn-primary" target="_blank"
                                                href="{{url('upload/'.@$lhpp->lhpp_kit->url_file_konsumsi_bb)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Lembar LHPP</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->lembar_lhpp != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->lembar_lhpp)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Laporan Final</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->laporan_final != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->laporan_final)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Draft Sertifikat SLO</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->draft_sertifikat_slo != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->draft_sertifikat_slo)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Sertifikat SLO (untuk cetak
                                                ulang)</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->sertifikat_slo != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->sertifikat_slo)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Hasil Pemeriksaaan dan
                                                Pengujian</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <textarea class="form-control form-white" rows="10" readonly
                                                      name="no_permohonan">{{@$lhpp->hasil_pp}}</textarea>
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kesimpulan</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <textarea class="form-control form-white" rows="10" readonly
                                                      name="no_permohonan">{{@$lhpp->kesimpulan}}</textarea>
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Rekomendasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <textarea class="form-control form-white" rows="10" readonly
                                                      name="no_permohonan">{{@$lhpp->rekomendasi}}</textarea>
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Surat Permohonan SLO dari Pemilik
                                                Instalasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->surat_permohonan != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->surat_permohonan)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">SPJBTL/Dokumen Pendukung
                                                Perjanjian</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->spjbtl != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->spjbtl)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12">

                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Surat Pernyataan Kesesuaian Hasil
                                                Pemeriksaan dan Pengujian</label>
                                        </div>
                                        <div class="col-sm-6">
                                            @if(@$lhpp->surat_kesesuaian != null)
                                                <a class="btn btn-primary" target="_blank"
                                                   href="{{url('upload/'.@$lhpp->surat_kesesuaian)}}"><i
                                                            class="fa fa-download"></i> Download</a>
                                            @else
                                                <i class="fa fa-times"></i>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();
                });
            </script>
@endsection
    