<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    {{--<meta name="_token" content="{!! csrf_token() !!}"/>--}}
    <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png" type="image/png">
    <title>Pusat Sertifikasi - PT PLN (Persero)</title>
    <link href="{{ url('/') }}/assets/global/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/theme.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/ui.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/layout4/css/layout.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->

    {{--Please move this to their page--}}
    {{--<link href="{{ url('/') }}/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">--}}

            <!-- END PAGE STYLE -->

    @yield('page_css')

    <style>
        button:disabled {
            cursor: not-allowed;
            pointer-events: all !important;
        }

        .twitter-typeahead {
            width: 100%;
            z-index: 999;
            position: relative;
        }

        .tt-input {
            width: 100%;
            z-index: 999;
            position: relative;
        }

        .tt-hint {
            color: #999999;
            z-index: 999;
        }

        .tt-menu {
            background-color: #FFFFFF;
            border: 1px solid rgba(0, 0, 0, 0.2);
            padding: 3px 0;
            width: 100%;
            max-height: 100px;
            overflow-y: scroll;
            overflow-x: hidden;
            z-index: 999;
            position: relative;
        }

        .tt-suggestion {
            padding: 3px 20px;
            z-index: 999;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            background-color: #0097CF;
            color: #FFFFFF;
        }

        .tt-suggestion p {
            margin: 0;
        }
    </style>
    <script src="{{ url('/') }}/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
<!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
<!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
<!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
<!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
<!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
<!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
<!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
<!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

<!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
<!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

<!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
<!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
<!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
<!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
<!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
<!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
<!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
<!-- BEGIN BODY -->
{{--<body class="sidebar-top theme-sdtl color-default fixed-topbar sidebar boxed">--}}
<body class="sidebar-top fixed-topbar fixed-sidebar theme-sdtl color-default">
<!--[if lt IE 7]>

<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar">
        <div class="logopanel">
            <h1>
                <a href="{{url('/')}}"></a>
            </h1>
        </div>
        <div class="sidebar-inner">
            <ul class="nav nav-sidebar">
                <li class="nav"><a href="#"><i
                                class="icon-home"></i><span>Dashboard</span></a></li>
                <li class="nav"><a href="#"><i
                                class="fa fa-phone"></i><span>Contact Us</span></a></li>
            </ul>
        </div>
    </div>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <ul class="nav nav-icons">
                        <li><a href="#" class="pull-left toggle_fullscreen"><i class="icon-size-fullscreen"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        @yield('content')
                <!-- END PAGE CONTENT -->
        <div class="footer">
            <div class="copyright">
                <p class="pull-left sm-pull-reset">
                    <span>Copyright <span class="copyright">©</span> 2016 | </span>
                    <span><strong>PLN KANTOR PUSAT - </strong></span>
                    <span><strong>DIVISI SISTEM DAN TEKNOLOGI INFORMASI</strong></span>
                </p>
                <p class="pull-right sm-pull-reset">
                                <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms
                                        of use</a> | <a
                                            href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
            </div>
        </div>
    </div>
    </div>

</section>
<!-- END PRELOADER -->
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/jasny-bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
<!-- Jquery Cookies, for theme -->
<script src="{{ url('/') }}/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>
<!-- simulate synchronous behavior when using AJAX -->
<script src="{{ url('/') }}/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="{{ url('/') }}/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- Custom Scrollbar sidebar -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
<!-- Show Dropdown on Mouseover -->
<script src="{{ url('/') }}/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
<script src="{{ url('/') }}/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="{{ url('/') }}/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Animated Progress Bar -->
<script src="{{ url('/') }}/assets/global/js/builder.js"></script> <!-- Theme Builder -->
<script src="{{ url('/') }}/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="{{ url('/') }}/assets/global/js/application.js"></script> <!-- Main Application Script -->
<script src="{{ url('/') }}/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
<script src="{{ url('/') }}/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
<!-- BEGIN PAGE SCRIPT -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<!-- Inline Edition X-editable -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
<!-- Context Menu -->
<script src="{{ url('/') }}/assets/global/js/widgets/todo_list.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
<script src="{{ url('/') }}/assets/global/js/pages/dashboard.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/slick/slick.min.js"></script> <!-- Slider -->
<script src="{{ url('/') }}/assets/global/plugins/prettify/prettify.min.js"></script> <!-- Show html code -->
<script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/countup/countUp.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
<!-- Buttons Loading State -->
<script src="{{ url('/') }}/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
<script src="{{ url('/') }}/assets_front/plugins/typeahead.bundle.js"></script>  <!-- Notifications -->
{{--<script src="{{ url('/') }}/assets/global/js/pages/notifications2.js"></script>--}}
{{--<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>--}}

@yield('page_script')

        <!-- END PAGE SCRIPT -->
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
<script src="../assets/admin/md-layout4/material-design/js/material.js"></script>
<script>
    @if (session('success') || session('error') || session('warning'))
            @if(session('success'))
            content = '<div class="alert alert-success media fade in"><p style="text-align: center"><strong>Success!</strong> {{session('success')}}</p></div>';
    notifType = 'success';
    @elseif(session('error'))
            content = '<div class="alert alert-danger media fade in"><p style="text-align: center"><strong>Failed!</strong> {{session('error')}}</p></div>';
    notifType = 'error';
    @else
            content = '<div class="alert alert-warning media fade in"><p style="text-align: center"><strong>Warning!</strong> {{session('warning')}}</p></div>';
    notifType = 'warning';
    @endif
    setTimeout(function () {
        var n = noty({
            text: content,
            type: notifType,
            dismissQueue: true,
            layout: 'top',
            closeWith: ['click'],
            theme: 'made',
            maxVisible: 10,
            animation: {
                open: 'animated fadeIn',
                close: 'animated fadeOut',
                easing: 'swing',
                speed: 100
            },
            timeout: 5000,
            buttons: '',
            callback: {
                onShow: function () {
                    $('#noty_top_layout_container').css('margin-top', -50).css('top', 0);
                    $('#noty_top_layout_container').css('left', 0).css('right', 0);

                }
            }
        });
    }, 1000);
    @endif

    $.material.init();
    function toogleTracking() {
        $("#tracking-panel").toggle("medium");
    }
</script>
@yield('script')
</body>
</html>
        