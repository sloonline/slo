<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png" type="image/png">
    <title>PT. PLN (Persero) Pusat Sertifikasi</title>
    <link href="{{ url('/') }}/assets/global/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/theme.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/ui.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/layout4/css/layout.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    @yield('page_css')
    <!-- END PAGE STYLE -->
    <script src="{{ url('/') }}/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <style>
    body { overflow-y: hidden;}
    </style>
  </head>
  <!-- BEGIN BODY -->
  <body class="theme-sdtl color-default">
    <!--[if lt IE 7]>
    <![endif]-->
    <section style="height: auto;">
      <div class="main-content m-0 p-0 style="height: auto;"">
        <!-- BEGIN PAGE CONTENT -->
		@yield('content')
        <!-- END PAGE CONTENT -->
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </section>
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </div>
    <!-- END PRELOADER -->
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/gsap/main-gsap.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/jasny-bootstrap.min.js"></script>

    <script src="{{ url('/') }}/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
    <script src="{{ url('/') }}/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
    <script src="{{ url('/') }}/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
    <script src="{{ url('/') }}/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
    <script src="{{ url('/') }}/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
    <script src="{{ url('/') }}/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
    <script src="{{ url('/') }}/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
    <script src="{{ url('/') }}/assets/global/plugins/charts-chartjs/Chart.min.js"></script>
    <script src="{{ url('/') }}/assets/global/js/builder.js"></script> <!-- Theme Builder -->
    <script src="{{ url('/') }}/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
    <script src="{{ url('/') }}/assets/global/js/application.js"></script> <!-- Main Application Script -->
    <script src="{{ url('/') }}/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
    <script src="{{ url('/') }}/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
    <script src="{{ url('/') }}/assets/global/js/quickview.js"></script> <!-- Chat Script -->
    <script src="{{ url('/') }}/assets/global/js/pages/search.js"></script> <!-- Search Script -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
    <!-- BEGIN PAGE SCRIPTS -->
    @yield('page_script')
    <!-- END PAGE SCRIPTS -->
    <script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
    @yield('script')
  </body>
</html>