<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>e-inspection</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png">
        <link href="{{ url('/') }}/assets/global/css/style.css" rel="stylesheet">
        <link href="{{ url('/') }}/assets/global/css/ui.css" rel="stylesheet">
        <link href="{{ url('/') }}/assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
		<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    </head>
        @yield('content')
        <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
		<script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/icheck/icheck.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="{{ url('/') }}/assets/global/js/plugins.js"></script>
        <script src="{{ url('/') }}/assets/global/js/pages/login-v2.js"></script>
        <script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
		<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="{{ url('/') }}/assets/global/js/pages/login-v1.js"></script>
    
		<script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
        @yield('page_script')
    </body>
</html>
