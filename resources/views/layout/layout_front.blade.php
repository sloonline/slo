<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png" type="image/png">
    <title>PT. PLN (Persero) Pusat Sertifikasi</title>
    <meta name="keywords" content="HTML5,CSS3,Template"/>
    <meta name="description" content=""/>
    <meta name="Author" content="PT. PLN (Persero)"/>

    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- WEB FONTS : use %7C instead of | (pipe) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700"
          rel="stylesheet" type="text/css"/>

    <!-- CORE CSS -->
    <link href="{{ url('/') }}/assets_front/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- THEME CSS -->
    <link href="{{ url('/') }}/assets_front/css/essentials.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets_front/css/layout.css" rel="stylesheet" type="text/css"/>

    <!-- PAGE LEVEL SCRIPTS -->
    <link href="{{ url('/') }}/assets_front/css/header-1.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/assets_front/css/color_scheme/green.css" rel="stylesheet" type="text/css"
          id="color_scheme"/>

    <style>
        button:disabled {
            cursor: not-allowed;
            pointer-events: all !important;
        }
    </style>
</head>

<!--
		AVAILABLE BODY CLASSES:
		
		smoothscroll 			= create a browser smooth scroll
		enable-animation		= enable WOW animations

		bg-grey					= grey background
		grain-grey				= grey grain background
		grain-blue				= blue grain background
		grain-green				= green grain background
		grain-blue				= blue grain background
		grain-orange			= orange grain background
		grain-yellow			= yellow grain background
		
		boxed 					= boxed layout
		pattern1 ... patern11	= pattern background
		menu-vertical-hide		= hidden, open on click
		
		BACKGROUND IMAGE [together with .boxed class]
		data-background="{{ url('/') }}/assets_front/images/boxed_background/1.jpg"
	-->
<body class="smoothscroll enable-animation">

<!-- wrapper -->
<div id="wrapper">

    <!--
        AVAILABLE HEADER CLASSES

        Default nav height: 96px
        .header-md 		= 70px nav height
        .header-sm 		= 60px nav height

        .noborder 		= remove bottom border (only with transparent use)
        .transparent	= transparent header
        .translucent	= translucent header
        .sticky			= sticky header
        .static			= static header
        .dark			= dark header
        .bottom			= header on bottom

        shadow-before-1 = shadow 1 header top
        shadow-after-1 	= shadow 1 header bottom
        shadow-before-2 = shadow 2 header top
        shadow-after-2 	= shadow 2 header bottom
        shadow-before-3 = shadow 3 header top
        shadow-after-3 	= shadow 3 header bottom

        .clearfix		= required for mobile menu, do not remove!

        Example Usage:  class="clearfix sticky header-sm transparent noborder"
    -->
    @yield('header')

            <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">

            <!-- BUTTONS -->
            <ul class="pull-right nav nav-pills nav-second-main">

                <!-- LOGIN -->
                <li class="search">
                    @if(Auth::user() == null)
                        <a href="{{ url('auth/login') }}">
                            <i class="fa fa-lock"></i>
                        </a>
                    @else
                        <a href="{{ url('/'.((Auth::user()->perusahaan == null) ? 'internal' : 'eksternal')) }}">
                            <div style="margin-top: -5px;">
                                <button class="btn btn-warning"><b><i class="icon-user"></i> MY PANEL</b>
                                </button>
                            </div>
                        </a>
                    @endif
                </li>
                <!-- /LOGIN -->
            </ul>
            <!-- /BUTTONS -->

            @yield('logo')

                    <!--
							Top Nav 
							
							AVAILABLE CLASSES:
							submenu-dark = dark sub menu
						-->
            <div class="navbar-collapse pull-right nav-main-collapse collapse">
                <nav class="nav-main">

                    <!--
                        .nav-onepage
                        Required for onepage navigation links

                        Add .external for an external link!
                    -->
                    <ul id="topMain" class="nav nav-pills nav-main nav-onepage">
                        <li>
                            <a href="{{url('/')}}#slider">
                                HOME
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#tentang">
                                PUSERTIF
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#visimisi">
                                VISI & MISI
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#layanan">
                                LAYANAN
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#akreditasi">
                                AKREDITASI
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#alur">
                                ALUR PROSES SERTIFIKASI
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/')}}#footer">
                                CONTACT US
                            </a>
                        </li>
                        <!--<li>
										<a href="{{url('/')}}#pelanggan">
											PENGGUNA JASA
										</a>
									</li>
									<li>
										<a href="{{url('/')}}#feedback">
											FEEDBACK
										</a>
									</li>-->
                    </ul>

                </nav>
            </div>

        </div>
    </header>
    <!-- /Top Nav -->

</div>

@yield('content')

        <!-- FOOTER -->
<!-- FOOTER -->
<footer id="footer">
    <div class="container" style="padding-top: 20px; margin-bottom: 20px">

        <div class="row">

            <div class="col-md-3">
                <!-- Footer Logo -->
                <img class="footer-logo" src="{{ url('/') }}/assets_front/images/logo_light.png" alt=""/>

                <!-- Contact Address -->
                <address>
                    <ul class="list-unstyled">
                        <li class="footer-sprite address">
                            Jalan Laboratorium<br>
                            Duren Tiga, Jakarta Selatan<br>
                            Jakarta, 12760<br>
                        </li>
                        <li class="footer-sprite phone">
                            Phone: (021) 790 0034
                        </li>
                        <li class="footer-sprite globe">
                            Fax: (021) 798 2034
                        </li>
                        <li class="footer-sprite email">
                            <a href="#">niaga@pln-jaser.co.id</a>
                        </li>
                    </ul>
                </address>
                <!-- /Contact Address -->

            </div>

            <div class="col-md-7" style="padding-top: 20px;">
                <h3 class="letter-spacing-1">WHY US?</h3>

                <!-- Small Description -->
                <p>
                    Pusat Sertifikasi merupakan salah satu unit penunjang <strong>PT. PLN (Persero)</strong> yang
                    mempunyai tugas utama menjalankan kegiatan
                    sertifikasi di bidang ketenagalistrikan dan telah terakreditasi yang meliputi Sertifikasi Kelaikan
                    Instalasi Pembangkit Tenaga Listrik, Sertifikasi Kelaikan Instalasi Jaringan, Sertifikasi Produk
                    (SPM / SNI), Sertifikasi
                    Manajemen Mutu, Sertifikasi Sistem Manajemen Lingkungan, Sertifikasi Sistem Manajemen K3,
                    Sertifikasi OHSAS 18001, Komisioning Pembangkit dan Jaringan.</p>
                <h2>http://pusertif.pln.co.id</h2>
            </div>

            <div class="col-md-2" style="padding-top: 20px;">

                <!-- Links -->
                <h4 class="letter-spacing-1">EXPLORE PUSERTIF</h4>
                <ul class="footer-links list-unstyled">
                    <li><a href="{{url('/')}}#slider">Home</a></li>
                    <li><a href="{{url('/')}}#tentang">Tentang Pusertif</a></li>
                    <li><a href="{{url('/')}}#layanan">Layanan</a></li>
                    <li><a href="{{url('/')}}#alur">Alur Proses Sertifikasi</a></li>
                </ul>
                <!-- /Links -->

            </div>

        </div>

    </div>

    <div class="copyright">
        <div class="container">
            <ul class="pull-right nomargin list-inline mobile-block">
                <li><a href="#">Terms &amp; Conditions</a></li>
                <li>&bull;</li>
                <li><a href="#">Privacy</a></li>
            </ul>
            Copyright &copy; 2016 | PLN KANTOR PUSAT - DIVISI SISTEM DAN TEKNOLOGI INFORMASI
        </div>
    </div>
</footer>
<!-- /FOOTER -->

</div>
<!-- /wrapper -->


<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>


<!-- PRELOADER -->
<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div><!-- /PRELOADER -->


<!-- JAVASCRIPT FILES -->
<script type="text/javascript">var plugin_path = '{{ url('/') }}/assets_front/plugins/'</script>
<script type="text/javascript" src="{{ url('/') }}/assets_front/plugins/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/assets_front/js/scripts.js"></script>
<script type="text/javascript" src="{{ url('/') }}/assets_front/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- PAGELEVEL SCRIPTS -->
<script type="text/javascript" src="{{ url('/') }}/assets_front/js/contact.js"></script>
<script type="text/javascript">
    $('.max-file').on('change', function (control) {
        var sizeMb = this.files[0].size / 1024 / 1024;
        if (sizeMb > 1) {
            alert("File tidak boleh melebihi 1 MB. ");
            $("#" + control.target.id).val("");
            $("#" + control.target.id + "_text").val("");
        }
    });
</script>
@yield('page_script')
</body>
</html>