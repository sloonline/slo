<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    {{--<meta name="_token" content="{!! csrf_token() !!}"/>--}}
    <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png" type="image/png">
    <title>Pusat Sertifikasi - PT PLN (Persero)</title>
    <link href="{{ url('/') }}/assets/global/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/theme.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/ui.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/layout4/css/layout.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->

    {{--Please move this to their page--}}
    {{--<link href="{{ url('/') }}/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">--}}

    <!-- END PAGE STYLE -->

    @yield('page_css')

    <style>
    button:disabled {
        cursor: not-allowed;
        pointer-events: all !important;
    }
    .twitter-typeahead{
        width: 100%;
        z-index : 999;
        position: relative;
    }
    .tt-input {
        width: 100%;
        z-index : 999;
        position: relative;
    }
    .tt-hint {
        color: #999999;
        z-index : 999;
    }
    .tt-menu {
        background-color: #FFFFFF;
        border: 1px solid rgba(0, 0, 0, 0.2);
        padding: 3px 0;
        width: 100%;
        max-height : 100px;
        overflow-y: scroll;
        overflow-x: hidden;
        z-index : 999;
        position: relative;
    }

    .tt-suggestion {
        padding: 3px 20px;
        z-index : 999;
    }
    .tt-suggestion:hover {
        cursor: pointer;
        background-color: #0097CF;
        color: #FFFFFF;
    }
    .tt-suggestion p {
        margin: 0;
    }
    #ajax_loader{
        position:fixed;
        top:0px;
        right:0px;
        width:100%;
        height:100%;
        background-color:#666;
        background-image:url('{{ url('/') }}/assets/global/images/ajax-loader.gif');
        background-repeat:no-repeat;
        background-position:center;
        z-index:10000000;
        opacity: 0.4;
        filter: alpha(opacity=40); /* For IE8 and earlier */
        display: none;
    }
    </style>
    <script src="{{ url('/') }}/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
<!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
<!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
<!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
<!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
<!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
<!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
<!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
<!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

<!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
<!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

<!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
<!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
<!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
<!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
<!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
<!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
<!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
<!-- BEGIN BODY -->
{{--<body class="sidebar-top theme-sdtl color-default fixed-topbar sidebar boxed">--}}
<body class="sidebar-top fixed-topbar fixed-sidebar theme-sdtl color-default">
    <!--[if lt IE 7]>

    <![endif]-->
    <section>
        <!-- BEGIN SIDEBAR -->
        <div class="sidebar">
            <div class="logopanel">
                <h1>
                    <a href="{{url('/eksternal')}}"></a>
                </h1>
            </div>
            <div class="sidebar-inner">
                <ul class="nav nav-sidebar">
                    <li class="nav {{setActive(['eksternal'])}}"><a href="{{url('/eksternal')}}"><i
                        class="icon-home"></i><span>Dashboard</span></a></li>
                    <li class="nav-parent {{
                                            setActive(['eksternal/view_instalasi_pembangkit',
                                            'eksternal/view_instalasi_transmisi',
                                            'eksternal/view_instalasi_distribusi',
                                            'eksternal/view_instalasi_pemanfaatan_tt',
                                            'eksternal/view_instalasi_pemanfaatan_tm',
                                            'eksternal/create_instalasi_pembangkit',
                                            'eksternal/create_instalasi_pembangkit/*',
                                            'eksternal/create_instalasi_transmisi',
                                            'eksternal/create_instalasi_transmisi/*',
                                            'eksternal/create_instalasi_distribusi',
                                            'eksternal/create_instalasi_distribusi/*',
                                            'eksternal/create_instalasi_pemanfaatan_tt',
                                            'eksternal/create_instalasi_pemanfaatan_tt/*',
                                            'eksternal/create_instalasi_pemanfaatan_tm',
                                            'eksternal/create_instalasi_pemanfaatan_tm/*',
                                            ])}}
                            ">
                        <a href="#"><i class="glyphicon glyphicon-flash"></i><span>Data Instalasi</span> <span
                                    class="fa arrow"></span></a>
                        <ul class="children collapse">
                            <li><a href="{{url('eksternal/view_instalasi_pembangkit')}}"> Pembangkit</a></li>
                            <li><a href="{{url('eksternal/view_instalasi_transmisi')}}"> Transmisi</a></li>
                            @if(Auth::user()->jenis_user == TIPE_PLN)
                                <li><a href="{{url('eksternal/view_instalasi_distribusi')}}"> Distribusi</a></li>
                            @endif
                            @if(Auth::user()->jenis_user == TIPE_EKSTERNAL)
                                <li><a href="{{url('eksternal/view_instalasi_pemanfaatan_tt')}}"> Pemanfaatan TT</a></li>
                                <li><a href="{{url('eksternal/view_instalasi_pemanfaatan_tm')}}"> Pemanfaatan TM</a></li>
                            @endif
                        </ul>
                    </li>
                        <li class="nav {{setActive(['eksternal/view_order',
                            'eksternal/detail_order/*',
                            'eksternal/rancangan_biaya/*',
                            'eksternal/create_order/*'])}}">
                            <a href="{{url('/eksternal/view_order')}}"><i class="icon-clock"></i></i>
                                <span>History Order</span></a></li>
                                <li class="nav {{setActive(['eksternal/kontrak',
                                                            'eksternal/eksternalDetailKontrak/*',
                                                            'eksternal/detail_kontrak/*'
                                                            ])}}">
                                    @if(Auth::user()->jenis_user == TIPE_PLN)
                                    <a href="{{url('/eksternal/kontrak')}}"><i class="icon-briefcase"></i></i>
                                        <span>Konfirmasi Biaya</span>
                                    </a>
                                    @else
                                        <a href="{{url('/eksternal/kontrak')}}"><i class="icon-briefcase"></i></i>
                                            <span>Kontrak</span>
                                        </a>
                                    @endif

                                </li>
                                        <li class="nav {{setActive(['eksternal/tracking'
                                        ])}}"><a href="{{url('eksternal/tracking')}}"><i class="icon-magnifier"></i><span>Tracking</span></a>
                                        </li>
                                        {{--  <li class="nav"><a onclick="toogleTracking();"><i class="icon-magnifier"></i><span>Tracking</span></a>
                                        </li>  --}}
                                        {{--<li class="nav {{setActive(['eksternal/view_order', 'eksternal/create_order'])}}"><a href="{{url('/eksternal/view_order')}}"><i--}}
                                        {{--class="icon-note"></i><span>Order</span></a></li>--}}
                                        {{--<li class="nav-parent">--}}
                                        {{--<a href="#"><i class="icon-loop"></i><span>Riwayat SLO</span> <span class="fa arrow"></span></a>--}}
                                        {{--<ul class="children collapse">--}}
                                        {{--<li><a href="{{url('eksternal/riwayat')}}"> Riwayat</a></li>--}}
                                        {{--<li><a href="{{url('eksternal/riwayat')}}"> Riwayat</a></li>--}}
                                        {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-parent">--}}
                                        {{--<a href="#"><i class="icon-screen-desktop"></i><span>Customer Care</span> <span class="fa arrow"></span></a>--}}
                                        {{--<ul class="children collapse">--}}
                                        {{--<li><a target="_blank" href="#"> Pengaduan produk/layanan</a></li>--}}
                                        {{--<li><a target="_blank" href="#"> Survey kepuasan</a></li>--}}
                                        {{--</ul>--}}
                                        {{--</li>--}}


                                            {{--@if(Auth::user()->jenis_user == "PLN")--}}
                                                {{--<li class="nav {{setActive(['eksternal/surat_tugas',--}}
                                                    {{--'eksternal/program_distribusi',--}}
                                                    {{--'eksternal/create_program/',--}}
                                                    {{--'eksternal/create_program/*',--}}
                                                    {{--'eksternal/program_tree/*'])}}"><a href="{{url('/eksternal/surat_tugas')}}"><i--}}
                                                        {{--class="fa fa-files-o"></i><span>Surat Tugas</span></a>--}}
                                                    {{--</li>--}}
                                                {{--@endif--}}

                                                {{-- <li class="nav {{setActive(['eksternal/pemilik_instalasi'])}}"><a
                                                    href="{{url('/eksternal/pemilik_instalasi')}}"><i class="icon-users"></i><span>Pemilik Instalasi</span></a>
                                                </li> --}}
                                                <li class="nav {{setActive(['eksternal/download/manual'])}}"><a
                                                    target="_blank" href="{{url('eksternal/download/manual')}}"><i class="fa fa-download"></i><span>User Manual</span></a>
                                                </li>
                                                {{--<li class="nav {{setActive(['eksternal/laporan','eksternal/laporan_pekerjaan/*',--}}
                                                    {{--'eksternal/laporan/*'])}}">--}}
                                                    {{--<a href="{{url('eksternal/laporan')}}"><i class="fa fa-file-text-o"></i></i>--}}
                                                        {{--<span>Laporan</span></a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                                <div class="sidebar-footer clearfix">
                                                    <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top"
                                                    data-original-title="Settings">
                                                    <i class="icon-settings"></i></a>
                                                    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top"
                                                    data-original-title="Fullscreen">
                                                    <i class="icon-size-fullscreen"></i></a>
                                                    <a class="pull-left" href="#" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
                                                        <i class="icon-lock"></i></a>
                                                        <a class="pull-left btn-effect" href="auth/logout" data-modal="modal-1" data-rel="tooltip"
                                                        data-placement="top" data-original-title="Logout">
                                                        <i class="icon-power"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END SIDEBAR -->
                                            <div class="main-content">
                                                <!-- BEGIN TOPBAR -->
                                                <div class="topbar">
                                                    <div class="header-left">
                                                        <div class="topnav">
                                                            <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span
                                                                class="menu__handle"><span>Menu</span></span></a>
                                                                <ul class="nav nav-icons">
                                                                    <li><a href="#" class="pull-left toggle_fullscreen"><i class="icon-size-fullscreen"></i></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="header-right">
                                                            <ul class="header-menu nav navbar-nav">
                                                                <!-- BEGIN NOTIFIKASI -->
                                                                <li class="dropdown" id="messages-header">
                                                                    <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                        <i class="icon-bell"></i>
                                                                        @if(Auth::user()->notif()->count() > 0)
                                                                            <span class="badge badge-danger badge-header">
                                                                                {{Auth::user()->notif()->count()}}
                                                                            </span>
                                                                        @endif
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li class="dropdown-header clearfix">
                                                                            <p class="pull-left">
                                                                                You have {{Auth::user()->notif()->count()}} Notification
                                                                            </p>
                                                                        </li>
                                                                        <li class="dropdown-body" style="overflow: auto;overflow-x:hidden;height: 300px; ">
                                                                            <ul class="dropdown-menu-list withScroll">
                                                                                @if(Auth::user()->notif()->count() > 0)
                                                                                    @foreach(Auth::user()->notif() as $notif)
                                                                                        <a href="{{url("notif/" . $notif->id)}}">
                                                                                            <li class="clearfix">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-2">
                                                                                                        <span class="pull-left p-r-5">
                                                                                                            <button type="button"
                                                                                                            class="btn btn-sm btn-icon btn-rounded {{$notif->color}}"><i
                                                                                                            class="fa {{$notif->icon}}"></i></button>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div class="col-lg-10">
                                                                                                        <strong class="text-info">{{$notif->subject}}</strong>
                                                                                                        <p><?php echo $notif->message?></p>
                                                                                                        <small class="pull-left text-muted"><span
                                                                                                            class="glyphicon glyphicon-time p-r-5"></span>{{App\HitungMundur::hitung_mundur($notif->created_at)}}
                                                                                                        </small>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </li>
                                                                                        </a>
                                                                                    @endforeach
                                                                                @else
                                                                                    <li class="clearfix">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-12">
                                                                                                <small class="pull-left text-muted">Tidak Ada Notifikasi</small>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                @endif
                                                                            </ul>
                                                                        </li>
                                                                        <li class="dropdown-footer clearfix">
                                                                            <a href="{{url("notif_list")}}" class="pull-left">See all notification</a>
                                                                            <a href="{{url("notif_list")}}" class="pull-right">
                                                                                <i class="icon-settings"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <!-- END NOTIFIKASI -->
                                                                <!-- BEGIN USER DROPDOWN -->
                                                                <?php $loc = storage_path() . '/upload/foto_user/' . Auth::user()->file_foto_user; ?>
                                                                <li class="dropdown" id="user-header">
                                                                    <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                        {{--@if (file_exists($loc))--}}
                                                                            {{--<img src="{{url('upload/'.Auth::user()->file_foto_user)}}" alt="user image">--}}
                                                                        {{--@else--}}
                                                                            {{--default foto jika tidak ada--}}
                                                                            <img src="{{url('assets/global/images/avatars/avatar12@2x.png')}}" alt="user image">
                                                                        {{--@endif--}}
                                                                        {{--        											<img src="{{ url('/') }}/assets/global/images/avatars/avatar13@2x.png" alt="user image">--}}
                                                                        <span class="username">{{(Auth::user() != null) ? Auth::user()->nama_user : "Guest"}}
                                                                            </span>
                                                                        </a>
                                                                        <ul class="dropdown-menu">
                                                                            <li>
                                                                                <a href="{{url('eksternal/profil/'.Auth::user()->id)}}"><i class="icon-user"></i><span>My Profile</span></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="{{url('/auth/logout')}}"><i class="icon-logout"></i><span>Logout</span></a>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <!-- END USER DROPDOWN -->
                                                                    <?php
                                                                    $permohonan = \App\Permohonan::getAllPermohonan();
                                                                    $permohonan_surat = \App\Permohonan::getPermohonanSuratTugas();
                                                                    $jml_permohonan = $permohonan->count() + $permohonan_surat->count();
                                                                    ?>

                                                                    <!-- BEGIN NOTIFIKASI -->
                                                                    <li class="dropdown" id="messages-header">
                                                                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                                            <i class="glyphicon glyphicon-shopping-cart"></i>
                                                                            <span class="badge badge-danger badge-header">{{($jml_permohonan!=0) ? $jml_permohonan : ""}}</span>
                                                                        </a>
                                                                        <ul class="dropdown-menu">
                                                                            <li class="dropdown-header clearfix">
                                                                                <p class="pull-left">
                                                                                    Anda Memiliki {{$jml_permohonan}} Permohonan di Keranjang
                                                                                </p>
                                                                            </li>
                                                                            @if($jml_permohonan>0)
                                                                                <li class="dropdown-body" style="overflow: auto;overflow-x:hidden;height: 300px;">
                                                                                    <ul class="dropdown-menu-list withScroll">
                                                                                        @foreach($permohonan_surat as $wa)
                                                                                            <a href="{{url('/eksternal/keranjang')}}">
                                                                                                <li class="clearfix">
                                                                                                    <div class="row">
                                                                                                        <div class="col-lg-2">
                                                                                                            <span class="pull-left p-r-5">
                                                                                                                <button type="button"
                                                                                                                class="btn btn-sm btn-icon btn-rounded blue">
                                                                                                                <i
                                                                                                                class="fa fa-pencil"></i>
                                                                                                            </button>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    <div class="col-lg-10">
                                                                                                        <strong class="text-info">{{@$wa->nomor_surat}}</strong>
                                                                                                        <?php $layanan = $wa->produk ?>
                                                                                                        @foreach(@$layanan as $row)
                                                                                                            <p>{{@$row->produk->produk_layanan}}</p>
                                                                                                        @endforeach
                                                                                                    </div>
                                                                                                </div>
                                                                                            </li>
                                                                                        </a>
                                                                                    @endforeach
                                                                                    @foreach($permohonan as $wa)
                                                                                        <a href="{{url('/eksternal/keranjang')}}">
                                                                                            <li class="clearfix">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-2">
                                                                                                        <span class="pull-left p-r-5">
                                                                                                            <button type="button"
                                                                                                            class="btn btn-sm btn-icon btn-rounded blue">
                                                                                                            <i
                                                                                                            class="fa fa-pencil"></i>
                                                                                                        </button>
                                                                                                    </span>
                                                                                                </div>
                                                                                                <div class="col-lg-10">
                                                                                                    <strong class="text-info">{{@$wa->produk->produk_layanan}}</strong>
                                                                                                    <p>{{@$wa->lingkup_pekerjaan->jenis_lingkup_pekerjaan}} - {{@$wa->instalasi->nama_instalasi}}</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                    </a>
                                                                                @endforeach
                                                                            </ul>
                                                                        </li>
                                                                        <li class="dropdown-header clearfix" style="background: none;">
                                                                            <a href="{{url('/eksternal/keranjang')}}" style="color:#fff">
                                                                                <button class="btn btn-success btn-square btn-embossed btn-block">
                                                                                    Total {{$jml_permohonan}} Permohonan
                                                                                    &nbsp;<i class="glyphicon glyphicon-shopping-cart"></i>
                                                                                </button>
                                                                            </a>
                                                                        </li>
                                                                    @else
                                                                        <li class="dropdown-body">
                                                                            <div style="margin: 20px;text-align: center;">
                                                                                <strong class="text-info">Anda belum memiliki permohonan.</strong>
                                                                                <br>
                                                                                Silakan pilih layanan yang diinginkan.
                                                                            </div>
                                                                        </li>
                                                                    @endif
                                                                </ul>
                                                            </li>
                                                            <!-- END NOTIFIKASI -->

                                                        </ul>
                                                    </div>
                                                    <!-- header-right -->
                                                </div>
                                                <!-- END TOPBAR -->

                                                <!-- BEGIN PAGE CONTENT -->
                                                <div class="page-content" style="margin-bottom: -70px;">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="panel bg-primary border" id="tracking-panel" hidden>
                                                                <div class="panel-header">
                                                                    <h3><i class="icon-magnifier"></i> <strong>Tracking Order</strong></h3>
                                                                </div>
                                                                <div class="panel-content">
                                                                    <input name="firstname" id="finder" class="form-control input-lg bg-primary"
                                                                    placeholder="Masukkan Nomor Registrasi. . ." autofocus="" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id='ajax_loader'>
                                                    {{-- <img src="{{ url('/') }}/assets/global/images/ajax-loader.gif"></img> --}}
                                                </div>
                                                @yield('content')
                                                <!-- END PAGE CONTENT -->
                                                <div class="footer">
                                                    <div class="copyright">
                                                        <p class="pull-left sm-pull-reset">
                                                            <span>Copyright <span class="copyright">©</span> 2016 | </span>
                                                            <span><strong>PLN KANTOR PUSAT - </strong></span>
                                                            <span><strong>DIVISI SISTEM DAN TEKNOLOGI INFORMASI</strong></span>
                                                        </p>
                                                        <p class="pull-right sm-pull-reset">
                                                            <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a
                                                                href="#" class="m-l-10">Privacy Policy</a></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END MAIN CONTENT -->
                                            <!-- BEGIN BUILDER -->
                                            {{--<div class="builder hidden-sm hidden-xs" id="builder">--}}
                                            {{--<a class="builder-toggle"><i class="icon-wrench"></i></a>--}}
                                            {{--<div class="inner">--}}
                                            {{--<div class="builder-container">--}}
                                            {{--<a href="#" class="btn btn-sm btn-default" id="reset-style">reset default style</a>--}}
                                            {{--<h4>Layout options</h4>--}}
                                            {{--<div class="layout-option">--}}
                                            {{--<span> Fixed Sidebar</span>--}}
                                            {{--<label class="switch pull-right">--}}
                                            {{--<input data-layout="sidebar" id="switch-sidebar" type="checkbox" class="switch-input" checked>--}}
                                            {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                            {{--<span class="switch-handle"></span>--}}
                                            {{--</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="layout-option">--}}
                                            {{--<span>Fixed Topbar</span>--}}
                                            {{--<label class="switch pull-right">--}}
                                            {{--<input data-layout="topbar" id="switch-topbar" type="checkbox" class="switch-input" checked>--}}
                                            {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                            {{--<span class="switch-handle"></span>--}}
                                            {{--</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="layout-option">--}}
                                            {{--<span>Boxed Layout</span>--}}
                                            {{--<label class="switch pull-right">--}}
                                            {{--<input data-layout="boxed" id="switch-boxed" type="checkbox" class="switch-input">--}}
                                            {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
                                            {{--<span class="switch-handle"></span>--}}
                                            {{--</label>--}}
                                            {{--</div>--}}
                                            {{--<h4 class="border-top">Color</h4>--}}
                                            {{--<div class="row">--}}
                                            {{--<div class="col-xs-12">--}}
                                            {{--<div class="theme-color bg-dark" data-main="default" data-color="#2B2E33"></div>--}}
                                            {{--<div class="theme-color background-primary" data-main="primary" data-color="#319DB5"></div>--}}
                                            {{--<div class="theme-color bg-red" data-main="red" data-color="#C75757"></div>--}}
                                            {{--<div class="theme-color bg-green" data-main="green" data-color="#1DA079"></div>--}}
                                            {{--<div class="theme-color bg-orange" data-main="orange" data-color="#D28857"></div>--}}
                                            {{--<div class="theme-color bg-purple" data-main="purple" data-color="#B179D7"></div>--}}
                                            {{--<div class="theme-color bg-blue" data-main="blue" data-color="#4A89DC"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4 class="border-top">Theme</h4>--}}
                                            {{--<div class="row row-sm">--}}
                                            {{--<div class="col-xs-6">--}}
                                            {{--<div class="theme clearfix sdtl" data-theme="sdtl">--}}
                                            {{--<div class="header theme-left"></div>--}}
                                            {{--<div class="header theme-right-light"></div>--}}
                                            {{--<div class="theme-sidebar-dark"></div>--}}
                                            {{--<div class="bg-light"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-xs-6">--}}
                                            {{--<div class="theme clearfix sltd" data-theme="sltd">--}}
                                            {{--<div class="header theme-left"></div>--}}
                                            {{--<div class="header theme-right-dark"></div>--}}
                                            {{--<div class="theme-sidebar-light"></div>--}}
                                            {{--<div class="bg-light"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-xs-6">--}}
                                            {{--<div class="theme clearfix sdtd" data-theme="sdtd">--}}
                                            {{--<div class="header theme-left"></div>--}}
                                            {{--<div class="header theme-right-dark"></div>--}}
                                            {{--<div class="theme-sidebar-dark"></div>--}}
                                            {{--<div class="bg-light"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-xs-6">--}}
                                            {{--<div class="theme clearfix sltl" data-theme="sltl">--}}
                                            {{--<div class="header theme-left"></div>--}}
                                            {{--<div class="header theme-right-light"></div>--}}
                                            {{--<div class="theme-sidebar-light"></div>--}}
                                            {{--<div class="bg-light"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<h4 class="border-top">Background</h4>--}}
                                            {{--<div class="row">--}}
                                            {{--<div class="col-xs-12">--}}
                                            {{--<div class="bg-color bg-clean" data-bg="clean" data-color="#F8F8F8"></div>--}}
                                            {{--<div class="bg-color bg-lighter" data-bg="lighter" data-color="#EFEFEF"></div>--}}
                                            {{--<div class="bg-color bg-light-default" data-bg="light-default" data-color="#E9E9E9"></div>--}}
                                            {{--<div class="bg-color bg-light-blue" data-bg="light-blue" data-color="#E2EBEF"></div>--}}
                                            {{--<div class="bg-color bg-light-purple" data-bg="light-purple" data-color="#E9ECF5"></div>--}}
                                            {{--<div class="bg-color bg-light-dark" data-bg="light-dark" data-color="#DCE1E4"></div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <!-- END BUILDER -->
                                        </section>
                                        <!-- BEGIN PRELOADER -->
                                        {{-- <div class="loader-overlay">
                                        <div class="spinner">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                </div> --}}
                                <!-- END PRELOADER -->
                                <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
                                <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/gsap/main-gsap.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/jasny-bootstrap.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
                                <!-- Jquery Cookies, for theme -->
                                <script src="{{ url('/') }}/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>
                                <!-- simulate synchronous behavior when using AJAX -->
                                <script src="{{ url('/') }}/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
                                <script src="{{ url('/') }}/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
                                <!-- Custom Scrollbar sidebar -->
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
                                <!-- Show Dropdown on Mouseover -->
                                <script src="{{ url('/') }}/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
                                <script src="{{ url('/') }}/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
                                <script src="{{ url('/') }}/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
                                <script src="{{ url('/') }}/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
                                <script src="{{ url('/') }}/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
                                <!-- Animated Progress Bar -->
                                <script src="{{ url('/') }}/assets/global/js/builder.js"></script> <!-- Theme Builder -->
                                <script src="{{ url('/') }}/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
                                <script src="{{ url('/') }}/assets/global/js/application.js"></script> <!-- Main Application Script -->
                                <script src="{{ url('/') }}/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
                                <script src="{{ url('/') }}/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
                                <!-- BEGIN PAGE SCRIPT -->
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script>
                                <!-- Inline Edition X-editable -->
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script>
                                <!-- Context Menu -->
                                <script src="{{ url('/') }}/assets/global/js/widgets/todo_list.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/metrojs/metrojs.min.js"></script> <!-- Flipping Panel -->
                                <script src="{{ url('/') }}/assets/global/js/pages/dashboard.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/slick/slick.min.js"></script> <!-- Slider -->
                                <script src="{{ url('/') }}/assets/global/plugins/prettify/prettify.min.js"></script> <!-- Show html code -->
                                <script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/countup/countUp.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                                <!-- Buttons Loading State -->
                                <script src="{{ url('/') }}/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
                                <script src="{{ url('/') }}/assets_front/plugins/typeahead.bundle.js"></script>  <!-- Notifications -->
                                {{--<script src="{{ url('/') }}/assets/global/js/pages/notifications2.js"></script>--}}
                                {{--<script type="text/javascript">
                                $.ajaxSetup({
                                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                            });
                        </script>--}}

                        @yield('page_script')

                        <!-- END PAGE SCRIPT -->
                        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                        <!-- Tables Filtering, Sorting & Editing -->
                        <script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
                        <script src="../assets/admin/md-layout4/material-design/js/material.js"></script>
                        <script>
                        @if (session('success') || session('error') || session('warning'))
                        @if(session('success'))
                        content = '<div class="alert alert-success media fade in"><p style="text-align: center"><strong>Success!</strong> {{session('success')}}</p></div>';
                        notifType = 'success';
                        @elseif(session('error'))
                        content = '<div class="alert alert-danger media fade in"><p style="text-align: center"><strong>Failed!</strong> {{session('error')}}</p></div>';
                        notifType = 'error';
                        @else
                        content = '<div class="alert alert-warning media fade in"><p style="text-align: center"><strong>Warning!</strong> {{session('warning')}}</p></div>';
                        notifType = 'warning';
                        @endif
                        setTimeout(function () {
                            var n = noty({
                                text: content,
                                type: notifType,
                                dismissQueue: true,
                                layout: 'top',
                                closeWith: ['click'],
                                theme: 'made',
                                maxVisible: 10,
                                animation: {
                                    open: 'animated fadeIn',
                                    close: 'animated fadeOut',
                                    easing: 'swing',
                                    speed: 100
                                },
                                timeout: 5000,
                                buttons: '',
                                callback: {
                                    onShow: function () {
                                        $('#noty_top_layout_container').css('margin-top', -50).css('top', 0);
                                        $('#noty_top_layout_container').css('left', 0).css('right', 0);

                                    }
                                }
                            });
                        }, 1000);
                        @endif

                        $.material.init();
                        function toogleTracking() {
                            $("#tracking-panel").toggle("medium");
                        }
                        </script>
                        @yield('script')
                    </body>
                    </html>
