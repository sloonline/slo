<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ url('/') }}/assets/global/images/favicon.png" type="image/png">
    <title>Pusat Sertifikasi - PT PLN (Persero)</title>
    <link href="{{ url('/') }}/assets/global/css/style.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/theme.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/css/ui.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/layout4/css/layout.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    @yield('page_css')

    <style>
        button:disabled {
            cursor: not-allowed;
            pointer-events: all !important;
        }

        #ajax_loader {
            position: fixed;
            top: 0px;
            right: 0px;
            width: 100%;
            height: 100%;
            background-color: #666;
            background-image: url('{{ url('/') }}/assets/global/images/ajax-loader.gif');
            background-repeat: no-repeat;
            background-position: center;
            z-index: 10000000;
            opacity: 0.4;
            filter: alpha(opacity=40); /* For IE8 and earlier */
            display: none;
        }
    </style>
    <!-- END PAGE STYLE -->
</head>
<!-- BEGIN BODY -->
<body class="sidebar-top fixed-topbar fixed-sidebar theme-sdtl color-default">
<!--[if lt IE 7]>

<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar">
        <div class="logopanel">
            <h1>
                <a href="{{url('internal')}}"></a>
            </h1>
        </div>
        <div class="sidebar-inner">
            <ul class="nav nav-sidebar">
                <li class="nav {{setActive(['internal'])}}"><a href="{{url('/internal')}}"><i
                                class="icon-home"></i><span>Dashboard</span></a></li>
                {{--<li class="nav-parent {{--}}
                {{--setActive(['internal/view_instalasi_pembangkit',--}}
                {{--'internal/view_instalasi_transmisi',--}}
                {{--'internal/view_instalasi_distribusi',--}}
                {{--'internal/view_instalasi_pemanfaatan_tt',--}}
                {{--'internal/view_instalasi_pemanfaatan_tm',--}}
                {{--'internal/create_instalasi_pembangkit',--}}
                {{--'internal/create_instalasi_pembangkit/*',--}}
                {{--'internal/create_instalasi_transmisi',--}}
                {{--'internal/create_instalasi_transmisi/*',--}}
                {{--'internal/create_instalasi_distribusi',--}}
                {{--'internal/create_instalasi_distribusi/*',--}}
                {{--'internal/create_instalasi_pemanfaatan_tt',--}}
                {{--'internal/create_instalasi_pemanfaatan_tt/*',--}}
                {{--'internal/create_instalasi_pemanfaatan_tm',--}}
                {{--'internal/create_instalasi_pemanfaatan_tm/*',--}}
                {{--])}}--}}
                {{--">--}}
                {{--<a href="#"><i class="glyphicon glyphicon-flash"></i><span>Data Instalasi</span> <span--}}
                {{--class="fa arrow"></span></a>--}}
                {{--<ul class="children collapse">--}}
                {{--<li><a href="{{url('internal/view_instalasi_pembangkit')}}"> Pembangkit</a></li>--}}
                {{--<li><a href="{{url('internal/view_instalasi_transmisi')}}"> Transmisi</a></li>--}}
                {{--@if(Auth::user()->jenis_user == "PLN")--}}
                {{--<li><a href="{{url('internal/view_instalasi_distribusi')}}"> Distribusi</a></li>--}}
                {{--@endif--}}
                {{--<li><a href="{{url('internal/view_instalasi_pemanfaatan_tt')}}"> Pemanfaatan TT</a></li>--}}
                {{--<li><a href="{{url('internal/view_instalasi_pemanfaatan_tm')}}"> Pemanfaatan TM</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="nav {{setActive(['internal/order',--}}
                {{--'internal/detail_order/*',--}}
                {{--'internal/create_permohonan/*'])}}">--}}
                {{--<a href="{{url('/internal/order')}}"><i class="icon-basket"></i></i>--}}
                {{--<span>Order</span></a></li>--}}
                <li class="nav nav-parent {{setActive(['internal/order',
                            'internal/detail_order/*',
                            'internal/create_permohonan/*','internal/tracking'])}}">
                    <a href="#"><i class="icon-basket"></i></i><span>Order</span></a>
                    <ul class="children collapse">
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW_ORDER) || \App\User::isCurrUserAllowPermission(PERMISSION_VIEW_PERMOHONAN))
                            <li class="nav {{setActive(['/internal/order', 'internal/detail_order/*','internal/create_permohonan/*'])}}">
                                <a href="{{url('internal/order')}}"><span>List Order</span></a></li>
                        @endif
                        <li class="nav {{setActive(['internal/tracking'])}}"><a
                                    href="{{url('internal/tracking')}}"><span>Tracking</span></a></li>
                    </ul>
                </li>
                <li class="nav {{setActive(['internal/rab',
                                'internal/rancangan_biaya_order/*',
                                'internal/create_rancangan_biaya/*',
                                ])}}">
                    <a href="{{url('/internal/rab')}}"><i class="icon-note"></i></i>
                        <span>Rancangan Biaya</span></a></li>
                <li class="nav {{setActive(['internal/kontrak',
                                    'internal/create_kontrak/*',
                                    'internal/detail_kontrak/*',
                                    ])}}">
                    <a href="{{url('/internal/kontrak')}}"><i class="icon-briefcase"></i></i>
                        <span>Kontrak</span></a></li>
                <li class="nav-parent nav {{setActive(['internal/show_wbs_io',
                                        'internal/create_wbs',
                                        'internal/create_wbs/*',
                                        'internal/save_wbs_io',
                                        'internal/create_io',
                                        'internal/create_io/*',
                                        'internal/monitoring_wbs',
                                        'internal/monitoring_io/*',
                                        ])}}">
                    <a href="#"><i class="icon-pie-chart"></i></i>
                        <span>WBS/IO</span></a>
                    <ul class="children collapse">
                        {{--<li><a href="#"> Create</a>--}}
                        {{--<ul class="children collapse">--}}
                        {{--<li><a href="{{url('internal/create_wbs')}}"> WBS</a></li>--}}
                        {{--<li><a href="{{url('internal/create_io')}}"> IO</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_MONITOR_WBS_IO) || \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO))
                            <li><a href="{{url('internal/show_wbs_io')}}"> List WBS/IO</a></li>
                        @endif
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_REALISASI_WBS_IO))
                            <li><a href="{{url('internal/realisasi')}}"> Realisasi WBS/IO</a></li>
                            <li><a href="{{url('internal/pengalihan_biaya')}}"> Pengalihan Biaya WBS</a></li>
                        @endif
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_MONITOR_WBS_IO) )
                            <li><a href="{{url('internal/monitoring_wbs')}}"> Monitoring WBS</a></li>
                            <li><a href="{{url('internal/monitoring_io')}}"> Monitoring IO</a></li>
                        @endif
                    </ul>
                </li>
                <li class="nav nav-parent {{setActive(['internal/pekerjaan',
                        'internal/pekerjaan/*',
                        'internal/pekerjaan/peralatan/*',
                        ])}}">
                    <a href="#"><i class="icon-wrench"></i></i>
                        <span>Pekerjaan</span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/internal/pekerjaan')}}"> List Pekerjaan</a></li>
                        <li><a href="{{url('/internal/pekerjaan/peralatan')}}"> Permohonan Peralatan</a></li>
                    </ul>
                </li>
                <li class="nav nav-parent {{setActive(['internal/permohonan_rlb',
                        'internal/permohonan_rls',
                        'internal/create_rlb',
                        'internal/rls',
                        'internal/create_rls',
                        'internal/blangko_inspeksi',
                        'internal/kick_off',
                        'internal/pending_item',
                        'internal/hilo',
                        'internal/data_teknik',
                        'internal/berita_inspeksi',
                        ])}}">
                    <a href="#"><i class="fa fa-file-text-o"></i></i>
                        <span>Laporan Inspeksi</span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/internal/kick_off')}}"> Kick Off Meeting</a></li>
                        <li><a href="{{url('/internal/data_teknik')}}"> Data Teknik</a></li>
                        <li><a href="{{url('/internal/blangko_inspeksi')}}"> Blangko Inspeksi</a></li>
                        <li><a href="{{url('/internal/berita_inspeksi')}}"> Berita Acara Inspeksi</a></li>
                        <li><a href="{{url('/internal/hilo')}}"> HILO</a></li>
                        <li><a href="{{url('/internal/pending_item')}}"> Pending Item</a></li>
                        <li><a href="{{url('/internal/permohonan_rlb')}}">Permohonan RLB</a></li>
                        <li><a href="{{url('/internal/permohonan_rls')}}">Permohonan RLS</a></li>
                        <li><a href="{{url('/internal/lpi')}}">LPI</a></li>
                    </ul>
                </li>
                <li class="nav {{setActive(['internal/lhpp/*', 'internal/lhpp'])}}">
                    <a href="{{url('/internal/lhpp')}}"><i class="icon-badge"></i></i>
                        <span>LHPP</span></a>
                </li>
                @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW_USER))
                    <li class="nav {{setActive(['internal/users',
                        'internal/register',
                        'internal/view_user/*',
                        'internal/register/*',
                        'internal/app_reg_peminta'
                        ])}}"><a href="{{url('/internal/users')}}"><i
                                    class="icon-users"></i></i>
                            <span>Manajemen User</span></a>
                        {{--<ul class="children collapse">--}}
                        {{--<li><a href="{{url('/internal/users')}}"> Manajemen User</a></li>--}}
                        {{--<li><a href="{{url('/internal/app_reg_peminta')}}"> Approval User</a></li>--}}
                        {{--</ul>--}}
                    </li>
                @endif
                {{--<li class="nav-parent nav {{setActive(['master/personil_inspeksi',--}}
                {{--'master/training',--}}
                {{--'master/keahlian',--}}
                {{--'master/personil_inspeksi/*',--}}
                {{--'master/training/*',--}}
                {{--'master/keahlian/*',--}}
                {{--])}}">--}}
                {{--<a href="#"><i class="fa fa-pied-piper-alt"></i><span>Personil Inspeksi</span></a>--}}
                {{--<ul class="children collapse">--}}
                {{--<li><a href="{{url('master/personil_inspeksi')}}"> Master Data Personil</a></li>--}}
                {{--<li><a href="{{url('master/training')}}"> Master Training</a></li>--}}
                {{--<li><a href="{{url('master/keahlian')}}"> Master Kompetensi</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                <li class="nav-parent nav {{setActive([
                            'master/pemilik_instalasi',
                            'master/pemilik_instalasi/*',
                            'master/personil_inspeksi',
                            'master/training',
                            'master/keahlian',
                            'master/personil_inspeksi/*',
                            'master/training/*',
                            'master/keahlian/*',
                            'master/bidang',
                            'master/bidang/*',
                            'master/sub_bidang',
                            'master/sub_bidang/*',
                            'master/modul',
                            'master/modul/*',
                            'master/level_akses',
                            'master/level_akses/*',
                            'master/hak_akses_modul',
                            'master/hak_akses_modul/*',
                            'master/unsur',
                            'master/unsur/*',
                            'master/peralatan',
                            'master/peralatan/*',
                            'internal/workflow/*',
                            ])}}">
                    <a href="#"><i class="fa fa-database"></i><span>Data Master</span> <span
                                class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('master/pemilik_instalasi')}}"> Master Pemilik Instalasi</a></li>
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW_PERSONIL))
                            <li><a href="{{url('master/personil_inspeksi')}}"> Master Data Personil</a></li>
                            <li><a href="{{url('master/training')}}"> Master Training</a></li>
                            <li><a href="{{url('master/keahlian')}}"> Master Kompetensi</a></li>
                        @endif
                        <li><a href="{{url('master/bidang')}}"> Master Bidang</a></li>
                        <li><a href="{{url('master/sub_bidang')}}"> Master Sub Bidang</a></li>
                        {{--<li><a href="{{url('master/modul')}}"> Master Modul</a></li>--}}
                        <li><a href="{{url('master/level_akses')}}"> Master Level Akses</a></li>
                        <li><a href="{{url('master/hak_akses_modul')}}"> Master Level Akses - Modul</a></li>
                        <li><a href="{{url('master/unsur')}}"> Master Unsur Biaya RAB</a></li>
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW_MASTER_PERALATAN))
                            <li><a href="{{url('master/peralatan')}}"> Master Peralatan</a></li>
                        @endif
                        <li><a href="{{url('master/struktur_hilo')}}"> Master Form Hilo</a></li>
                        <li><a href="{{url('internal/workflow')}}"> Master Workflow</a></li>
                    </ul>
                </li>
                <li class="nav-parent nav {{setActive([
                                'internal/log_email',
                                'internal/log_activity',
                                ])}}">
                    <a href="#"><i class="icon-hourglass"></i><span>Log</span> <span
                                class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li><a href="{{url('/internal/log_email')}}">Log Email</a></li>
                        <li><a href="{{url('/internal/log_activity')}}">Log Aktivitas</a></li>
                    </ul>
                </li>
                <li class="nav {{setActive(['internal/api/*'])}}"><a href="{{url('/internal/api/showPermohonan')}}"><i
                                class="icon-settings"></i></i><span>API</span></a></li>
                {{--<ul class="nav nav-parent {{setActive(['internal/api/*','internal/tracking'])}}">--}}
                {{--<a href="#"><i class="fa fa-file-text-o"></i></i><span>Tools</span></a>--}}
                {{--<ul class="children collapse">--}}
                {{--<li class="nav {{setActive(['internal/api/*'])}}"><a href="{{url('/internal/api/showPermohonan')}}"><i class="icon-settings"></i></i><span>API</span></a></li>--}}
                {{--<li class="nav {{setActive(['internal/tracking'])}}"><a href="{{url('internal/tracking')}}"><i class="icon-magnifier"></i><span>Tracking</span></a></li>--}}
                {{--</ul>--}}
                {{--</ul>--}}

                <div class="sidebar-footer clearfix">
                    <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top"
                       data-original-title="Settings">
                        <i class="icon-settings"></i></a>
                    <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top"
                       data-original-title="Fullscreen">
                        <i class="icon-size-fullscreen"></i></a>
                    <a class="pull-left" href="#" data-rel="tooltip" data-placement="top"
                       data-original-title="Lockscreen">
                        <i class="icon-lock"></i></a>
                    <a class="pull-left btn-effect" href="#" data-modal="modal-1" data-rel="tooltip"
                       data-placement="top"
                       data-original-title="Logout">
                        <i class="icon-power"></i></a>
                </div>
            </ul>
        </div>
    </div>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span
                                class="menu__handle"><span>Menu</span></span></a>
                    <ul class="nav nav-icons">
                        <li><a href="#" class="pull-left toggle_fullscreen"><i class="icon-size-fullscreen"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="header-right">
                <ul class="header-menu nav navbar-nav">
                    <!-- BEGIN NOTIFIKASI -->
                    <li class="dropdown" id="messages-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" class="p-r-0">
                            <i class="icon-bell"></i>
                            @if(Auth::user()->notif()->count() > 0)
                                <span class="badge badge-danger badge-header">
                                                            {{Auth::user()->notif()->count()}}
                                                        </span>
                            @endif
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header clearfix">
                                <p class="pull-left">
                                    You have {{Auth::user()->notif()->count()}} Notification
                                </p>
                            </li>
                            <li class="dropdown-body" style="overflow: auto;overflow-x:hidden;height: 300px; ">
                                <ul class="dropdown-menu-list withScroll">
                                    @if(Auth::user()->notif()->count() > 0)
                                        @foreach(Auth::user()->notif() as $notif)
                                            <a href="{{url("notif/" . $notif->id)}}">
                                                <li class="clearfix">
                                                    <div class="row">
                                                        <div class="col-lg-2">
                                                                                <span class="pull-left p-r-5">
                                                                                    <button type="button"
                                                                                            class="btn btn-sm btn-icon btn-rounded {{$notif->color}}">
                                                                                        <i
                                                                                                class="fa {{$notif->icon}}"></i>
                                                                                    </button>
                                                                                </span>
                                                        </div>
                                                        <div class="col-lg-10">
                                                            <strong class="text-info">{{$notif->subject}}</strong>
                                                            <p><?php echo $notif->message;?></p>
                                                            <small class="pull-left text-muted"><span
                                                                        class="glyphicon glyphicon-time p-r-5"></span>{{App\HitungMundur::hitung_mundur($notif->created_at)}}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </li>
                                            </a>
                                        @endforeach
                                    @else
                                        <li class="clearfix">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <small class="pull-left text-muted">Tidak Ada Notifikasi</small>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                            <li class="dropdown-footer clearfix">
                                <a href="{{url("notif_list")}}" class="pull-left">See all notification</a>
                                <a href="{{url("notif_list")}}" class="pull-right">
                                    <i class="icon-settings"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFIKASI -->
                    <!-- BEGIN USER DROPDOWN -->

                    <?php $loc = storage_path() . '/upload/foto_user/' . Auth::user()->file_foto_user; ?>
                    <li class="dropdown" id="user-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            @if (Auth::user()->file_foto_user != null)
                                @if(file_exists($loc))
                                    <img src="{{url('upload/'.Auth::user()->file_foto_user)}}" alt="user image">
                                @endif
                            @else
                                {{--default foto jika tidak ada--}}
                                <img src="{{url('assets/global/images/avatars/default.png')}}" alt="user image">
                            @endif
                            <span class="username"
                                  style="text-transform: uppercase">{{(Auth::user() != null) ? Auth::user()->nama_user : "Guest"}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{url('internal/profil/'.Auth::user()->id)}}"><i class="icon-user"></i><span>My Profile</span></a>
                            </li>
                            <li>
                                <a href="{{url('auth/logout')}}"><i class="icon-logout"></i><span>Logout</span></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div id='ajax_loader'>
            {{-- <img src="{{ url('/') }}/assets/global/images/ajax-loader.gif"></img> --}}
        </div>
        @yield('content')
                <!-- END PAGE CONTENT -->
        <div class="footer">
            <div class="copyright">
                <p class="pull-left sm-pull-reset">
                    <span>Copyright <span class="copyright">©</span> 2016 | </span>
                    <span><strong>PLN KANTOR PUSAT - </strong></span>
                    <span><strong>DIVISI SISTEM DAN TEKNOLOGI INFORMASI</strong></span>
                </p>
                <p class="pull-right sm-pull-reset">
                                                <span><a href="#"
                                                         class="m-r-10">Support</a> | <a
                                                            href="#" class="m-l-10 m-r-10">Terms
                                                        of use</a> | <a
                                                            href="#" class="m-l-10">Privacy
                                                        Policy</a></span>
                </p>
            </div>
        </div>
    </div>
    </div>
    <!-- END MAIN CONTENT -->
    <!-- BEGIN BUILDER -->
    {{--<div class="builder hidden-sm hidden-xs" id="builder">--}}
    {{--<a class="builder-toggle"><i class="icon-wrench"></i></a>--}}
    {{--<div class="inner">--}}
    {{--<div class="builder-container">--}}
    {{--<a href="#" class="btn btn-sm btn-default" id="reset-style">reset default style</a>--}}
    {{--<h4>Layout options</h4>--}}
    {{--<div class="layout-option">--}}
    {{--<span> Fixed Sidebar</span>--}}
    {{--<label class="switch pull-right">--}}
    {{--<input data-layout="sidebar" id="switch-sidebar" type="checkbox" class="switch-input" checked>--}}
    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
    {{--<span class="switch-handle"></span>--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--<div class="layout-option">--}}
    {{--<span>Fixed Topbar</span>--}}
    {{--<label class="switch pull-right">--}}
    {{--<input data-layout="topbar" id="switch-topbar" type="checkbox" class="switch-input" checked>--}}
    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
    {{--<span class="switch-handle"></span>--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--<div class="layout-option">--}}
    {{--<span>Boxed Layout</span>--}}
    {{--<label class="switch pull-right">--}}
    {{--<input data-layout="boxed" id="switch-boxed" type="checkbox" class="switch-input">--}}
    {{--<span class="switch-label" data-on="On" data-off="Off"></span>--}}
    {{--<span class="switch-handle"></span>--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--<h4 class="border-top">Color</h4>--}}
    {{--<div class="row">--}}
    {{--<div class="col-xs-12">--}}
    {{--<div class="theme-color bg-dark" data-main="default" data-color="#2B2E33"></div>--}}
    {{--<div class="theme-color background-primary" data-main="primary" data-color="#319DB5"></div>--}}
    {{--<div class="theme-color bg-red" data-main="red" data-color="#C75757"></div>--}}
    {{--<div class="theme-color bg-green" data-main="green" data-color="#1DA079"></div>--}}
    {{--<div class="theme-color bg-orange" data-main="orange" data-color="#D28857"></div>--}}
    {{--<div class="theme-color bg-purple" data-main="purple" data-color="#B179D7"></div>--}}
    {{--<div class="theme-color bg-blue" data-main="blue" data-color="#4A89DC"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h4 class="border-top">Theme</h4>--}}
    {{--<div class="row row-sm">--}}
    {{--<div class="col-xs-6">--}}
    {{--<div class="theme clearfix sdtl" data-theme="sdtl">--}}
    {{--<div class="header theme-left"></div>--}}
    {{--<div class="header theme-right-light"></div>--}}
    {{--<div class="theme-sidebar-dark"></div>--}}
    {{--<div class="bg-light"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-6">--}}
    {{--<div class="theme clearfix sltd" data-theme="sltd">--}}
    {{--<div class="header theme-left"></div>--}}
    {{--<div class="header theme-right-dark"></div>--}}
    {{--<div class="theme-sidebar-light"></div>--}}
    {{--<div class="bg-light"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-6">--}}
    {{--<div class="theme clearfix sdtd" data-theme="sdtd">--}}
    {{--<div class="header theme-left"></div>--}}
    {{--<div class="header theme-right-dark"></div>--}}
    {{--<div class="theme-sidebar-dark"></div>--}}
    {{--<div class="bg-light"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-6">--}}
    {{--<div class="theme clearfix sltl" data-theme="sltl">--}}
    {{--<div class="header theme-left"></div>--}}
    {{--<div class="header theme-right-light"></div>--}}
    {{--<div class="theme-sidebar-light"></div>--}}
    {{--<div class="bg-light"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h4 class="border-top">Background</h4>--}}
    {{--<div class="row">--}}
    {{--<div class="col-xs-12">--}}
    {{--<div class="bg-color bg-clean" data-bg="clean" data-color="#F8F8F8"></div>--}}
    {{--<div class="bg-color bg-lighter" data-bg="lighter" data-color="#EFEFEF"></div>--}}
    {{--<div class="bg-color bg-light-default" data-bg="light-default" data-color="#E9E9E9"></div>--}}
    {{--<div class="bg-color bg-light-blue" data-bg="light-blue" data-color="#E2EBEF"></div>--}}
    {{--<div class="bg-color bg-light-purple" data-bg="light-purple" data-color="#E9ECF5"></div>--}}
    {{--<div class="bg-color bg-light-dark" data-bg="light-dark" data-color="#DCE1E4"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
            <!-- END BUILDER -->
</section>
<!-- BEGIN SEARCH -->
<div id="morphsearch" class="morphsearch">
    <form class="morphsearch-form">
        <input class="morphsearch-input" type="search" placeholder="Search{{ url('/') }}."/>
        <button class="morphsearch-submit" type="submit">Search</button>
    </form>
    <!-- /morphsearch-content -->
    <span class="morphsearch-close"></span>
</div>
<!-- END SEARCH -->
<!-- BEGIN PRELOADER -->
{{--<div class="loader-overlay">--}}
{{--<div class="spinner">--}}
{{--<div class="bounce1"></div>--}}
{{--<div class="bounce2"></div>--}}
{{--<div class="bounce3"></div>--}}
{{--</div>--}}
{{--</div>--}}
        <!-- END PRELOADER -->

<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/gsap/main-gsap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/jasny-bootstrap.min.js"></script>

<script src="{{ url('/') }}/assets/global/plugins/jquery-cookies/jquery.cookies.min.js"></script>
<!-- Jquery Cookies, for theme -->
<script src="{{ url('/') }}/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>
<!-- simulate synchronous behavior when using AJAX -->
<script src="{{ url('/') }}/assets/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="{{ url('/') }}/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- Custom Scrollbar sidebar -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
<!-- Show Dropdown on Mouseover -->
<script src="{{ url('/') }}/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
<script src="{{ url('/') }}/assets/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="{{ url('/') }}/assets/global/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Animated Progress Bar -->
<script src="{{ url('/') }}/assets/global/plugins/charts-chartjs/Chart.min.js"></script>
<script src="{{ url('/') }}/assets/global/js/builder.js"></script> <!-- Theme Builder -->
<script src="{{ url('/') }}/assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="{{ url('/') }}/assets/global/js/application.js"></script> <!-- Main Application Script -->
<script src="{{ url('/') }}/assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
<script src="{{ url('/') }}/assets/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="{{ url('/') }}/assets/global/js/quickview.js"></script> <!-- Chat Script -->
<script src="{{ url('/') }}/assets/global/js/pages/search.js"></script> <!-- Search Script -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
<script src="{{ url('/') }}/assets/global/js/loading.js"></script> <!-- Loading -->
<script src="{{ url('/') }}/assets/global/plugins/jquery.masknumber.js"></script>

<script type="text/javascript">
    $('.max-file').on('change', function (control) {
        var sizeMb = this.files[0].size / 1024 / 1024;
        if (sizeMb > 1) {
            alert("File tidak boleh melebihi 1 MB. ");
            $("#" + control.target.id).val("");
            $("#" + control.target.id + "_text").val("");
        }
    });
</script>
<!-- BEGIN PAGE SCRIPTS -->
@yield('page_script')
        <!-- END PAGE SCRIPTS -->
<script src="{{ url('/') }}/assets/admin/layout4/js/layout.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  <!-- Notifications -->
@yield('script')
<script>
    @if (session('success') || session('error') || session('warning'))
            @if(session('success'))
            content = '<div class="alert alert-success media fade in"><p style="text-align: center"><strong>Success!</strong> {{session('success')}}</p></div>';
    notifType = 'success';
    @elseif(session('error'))
            content = '<div class="alert alert-danger media fade in"><p style="text-align: center"><strong>Failed!</strong> {{session('error')}}</p></div>';
    notifType = 'error';
    @else
            content = '<div class="alert alert-warning media fade in"><p style="text-align: center"><strong>Warning!</strong> {{session('warning')}}</p></div>';
    notifType = 'warning';
    @endif
    setTimeout(function () {
        var n = noty({
            text: content,
            type: notifType,
            dismissQueue: true,
            layout: 'top',
            closeWith: ['click'],
            theme: 'made',
            maxVisible: 10,
            animation: {
                open: 'animated fadeIn',
                close: 'animated fadeOut',
                easing: 'swing',
                speed: 100
            },
            timeout: 5000,
            buttons: '',
            callback: {
                onShow: function () {
                    $('#noty_top_layout_container').css('margin-top', -50).css('top', 0);
                    $('#noty_top_layout_container').css('left', 0).css('right', 0);

                }
            }
        });
    }, 1000);
    @endif

    //        $.material.init();

    function toogleTracking() {
        $("#tracking-panel").toggle("medium");
    }
</script>
</body>
</html>
