@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Order <strong>Baru</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-content">
                        <div class="nav-tabs2">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#general" data-toggle="tab"><i class="icon-home"></i> General</a>
                                </li>
                                <li>
                                    @if($surat != null)
                                        <a href="#program" data-toggle="tab"><i
                                                    class="icon-user"></i> Program Distribusi</a>
                                    @endif
                                </li>
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active" id="general">
                                    {!! Form::open(array('files'=> true, 'class'=> 'form-horizontal')) !!}
                                    <input type="hidden" name="id" value="{{($surat == null)?0 : $surat->id}}"/>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <h1>{{$surat->perusahaan->nama_perusahaan}}</h1>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Nomor Surat Tugas</label>
                                            </div>
                                            <div class="col-sm-6 prepend-icon">
                                                <input name="nomor_surat" readonly
                                                       value="{{($surat != null) ? $surat->nomor_surat : "" }}"
                                                       type="text" class="form-control form-white"
                                                       placeholder="Nomor Surat" required>
                                                <i class="icon-envelope"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Tanggal Surat Tugas</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <input name="tanggal_surat" readonly
                                                       value="{{($surat != null) ? date('d/m/Y',strtotime($surat->tanggal_surat)) : ""}}"
                                                       type="text"
                                                       class="form-control form-white" placeholder="Tanggal"
                                                       required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Periode (Tahun)</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <input name="periode" type="number" min="1900" readonly
                                                       value="{{($surat != null) ? $surat->periode : "" }}"
                                                       class="form-control form-white" placeholder="Nomor Surat"
                                                       required/>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Layanan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                @foreach($layanan as $row)
                                                    <div class="col-sm-4">
                                                        <input type="checkbox" readonly disabled
                                                               id="layanan_{{$row->id}}" name="produk[]"
                                                               {{(in_array(array('produk_id' => $row->id), $used_layanan)) ? "checked='checked'" : ""}}
                                                               value="{{$row->id}}"/> {{$row->produk_layanan}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">File Surat</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="col-sm-9">
                                                    @if($surat != null && $surat->file_surat != null)
                                                        <a href="{{url('upload/'.$surat->file_surat)}}" target="_blank"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-download"></i>View</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane" id="program">
                                    <table class="tree table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 70px;">No</th>
                                            <th>Nama Program</th>
                                            <th style="width: 150px;">Jenis Program</th>
                                            <th>Periode</th>
                                            <th style="width: 70px;">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1; ?>
                                        @if($surat != null)
                                            @foreach($surat->program as $row)
                                                <tr class="treegrid-ac1{{$row->id}}">
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->deskripsi}}</td>
                                                    <td>{{$row->jenisProgram->jenis_program}}</td>
                                                    <td>{{$row->periode }}</td>
                                                    <td>
                                                        <button type="button" onclick="formProgram({{$row->id}})"
                                                                class="btn btn-sm btn-primary btn-square btn-embossed"
                                                                data-toggle="tooltip"
                                                                title="DETAIL"><i
                                                                    class="fa fa-eye"></i></button>
                                                    </td>
                                                </tr>
                                                <?php $firstChild = $row->firstChild();?>
                                                @if(sizeof($firstChild) > 0)
                                                    <tr class="treegrid-ac20  treegrid-parent-ac1{{$row->id}}">
                                                        <td style="background-color: lightgrey;"></td>
                                                        <td colspan="4"
                                                            style="background-color: lightgrey;">
                                                            <b>{{$row->jenisProgram->acuan_1}}</b></td>
                                                    </tr>
                                                @endif
                                                @foreach($firstChild as $child1)
                                                    <tr class="treegrid-ac2{{$child1->id}}  treegrid-parent-ac1{{$row->id}}">
                                                        <td></td>
                                                        <td colspan="3">{{$child1->name}}</td>
                                                        <td>
                                                            <button type="button"
                                                                    onclick="formLevel1('{{$row->jenisProgram->acuan_1}}','{{$row->id}}','{{$child1->id}}')"
                                                                    class="btn btn-sm btn-primary btn-square btn-embossed"
                                                                    data-toggle="tooltip"
                                                                    title="DETAIL"><i
                                                                        class="fa fa-eye"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php $secondChild = $row->secondChild($child1->obj);?>
                                                    @if(sizeof($secondChild) > 0)
                                                        <tr class="treegrid-ac30  treegrid-parent-ac2{{$child1->id}}">
                                                            <td style="background-color: lightgrey;"></td>
                                                            <td colspan="4"
                                                                style="background-color: lightgrey;padding-left: 50px;">
                                                                <b>{{$row->jenisProgram->acuan_2}}</b></td>
                                                        </tr>
                                                    @endif
                                                    @foreach($secondChild as $child2)
                                                        <tr class="treegrid-ac3{{$child2->id}}  treegrid-parent-ac2{{$child1->id}}">
                                                            <td colspan="1"></td>
                                                            <td colspan="3"
                                                                style="padding-left: 50px;">{{$child2->name}}</td>
                                                            <td>
                                                                <button type="button"
                                                                        onclick="formLevel2('{{$row->jenisProgram->acuan_2}}','{{$row->id}}','{{$child1->id}}','{{$child2->id}}')"
                                                                        class="btn btn-sm btn-primary btn-square btn-embossed"
                                                                        data-toggle="tooltip"
                                                                        title="{{$row->jenisProgram->acuan_1}}"><i
                                                                            class="fa fa-eye"></i></button>
                                                            </td>
                                                        </tr>
                                                        <?php $instalasi = $child2->instalasi; ?>
                                                        @foreach($instalasi as $dt)
                                                            <tr class="treegrid-ac4{{$dt->id}}  treegrid-parent-ac3{{$child2->id}}">
                                                                <td colspan="1"></td>
                                                                <td colspan="3"
                                                                    style="padding-left: 100px;">{{$dt->nama_instalasi}}</td>
                                                                <td>
                                                                    <a href="{{url('/internal/detail_instalasi_program/'.$row->id.'/'.$child1->id.'/'.$child2->id.'/'.$dt->id)}}"
                                                                      class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                                <?php $no++; ?>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="pull-right">
                            <a href="javascript:history.back()"
                               class="btn btn-danger btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-remove"></i></a>
                        </div>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>
        </div>

        <!-- BEGIN MODAL USER SELECT -->
        <div class="modal fade" id="modal-form" aria-hidden="true">
            <div class="modal-dialog" style="width: 1000px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><span id="form-title"></span></h4>
                    </div>
                    <div class="modal-body" id="form-body">
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & DETAILing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/jquery-treegrid/js/jquery.treegrid.js"></script>
            <script type="text/javascript">
                $('.tree').treegrid();

                function formProgram(id) {
                    $("#form-title").html('FORM PROGRAM DISTRIBUSI');
                    showFormKomponenProgram('{{$surat->id}}', id, 0, 0, '{{TIPE_PROGRAM}}');
                }

                function formLevel1(tipe, id_program, id) {
                    switch (tipe) {
                        case "{{TIPE_AREA}}":
                            $("#form-title").html('FORM AREA PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_KONTRAK}}":
                            $("#form-title").html('FORM KONTRAK PROGRAM DISTRIBUSI');
                            break;
                    }
                    showFormKomponenProgram(id_program, id, 0, 0, tipe);
                }

                function formLevel2(tipe, id_program, id_area_kontrak, id) {
                    switch (tipe) {
                        case "{{TIPE_LOKASI}}":
                            $("#form-title").html('FORM LOKASI PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_PENYULANG}}":
                            $("#form-title").html('FORM PENYULANG PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_GARDU}}":
                            $("#form-title").html('FORM GARDU PROGRAM DISTRIBUSI');
                            break;
                    }
                    showFormKomponenProgram(id_program, id_area_kontrak, id, 0, tipe);
                }


                function showFormKomponenProgram(param1, param2, param3, param4, tipe) {
                    $("#form-body").load("{{url('/eksternal/detail_komponen_program/')}}/" + param1 + "/" + param2 + "/" + param3 + "/" + param4 + "/" + tipe, function () {
                        $("#modal-form").modal('show');
                    });
                }
            </script>
            <script type="text/javascript">
                var tab = "general";
                @if(Session::get('tab') != null)
                        tab = "program";
                @endif
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            </script>
@endsection