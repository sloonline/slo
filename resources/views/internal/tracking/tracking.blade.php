@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Tracking  <strong>Order</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Order</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Permohonan</th>
                                <th>Tanggal Order</th>
                                <th>Nama Instalasi</th>
                                <th>Layanan</th>
                                <th>Perusahaan</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(@$tracking != null)
                                    <?php $no = 1; ?>
                                    @foreach($tracking as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ @$item->permohonan->nomor_permohonan }}</td>
                                        <td>{{ date('d-M-Y',strtotime(@$item->order->tanggal_order ))}}</td>
                                        <td>{{ @$item->permohonan->instalasi->nama_instalasi }}</td>
                                        <td>{{ @$item->permohonan->produk->produk_layanan }}</td>
                                        <td>{{ @$item->permohonan->user->perusahaan->nama_perusahaan }}</td>
                                        <td><button class="btn btn-primary btn-transparent">
                                                {{ @$item->status }}
                                            </button></td>
                                    </tr>
                                    @endforeach 
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();

                    // Sort immediately with columns 0 and 1
                    oTable.fnSort([[0, 'asc']]);
                });
            </script>
@endsection