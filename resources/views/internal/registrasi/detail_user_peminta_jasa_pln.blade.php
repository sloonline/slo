@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
        <div class="header">
            <h2>Edit User <strong>Peminta Jasa PLN</strong></h2>
            <div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">User Peminta Jasa PLN</a></li>
					<li class="active">Detail User</li>
				</ol>
            </div>
        </div>
		<div class="row">
			<div class="placeholder-container">
				<div class="placeholder">
					<div class="placeholder-content col-md-12 ui-droppable">
						<p class="m-t-10 m-b-20 f-16">Silahkan inputkan data user peminta jasa PLN.</p>
						<div class="bg-white m-b-20">
							<div class="p-20 p-b-0">
								<h1 class="text-primary"><strong>{{$user->nama_perusahaan}}</strong></h1>
								<h4 class="m-t-0"><i class="fa fa-envelope"></i> {{$user->email_perusahaan}} &nbsp;<span class="bd-blue">&nbsp;&nbsp;&nbsp;</span><i class="fa fa-phone"></i> {{$user->no_telepon_perusahaan}} &nbsp;<span class="bd-blue">&nbsp;&nbsp;</span><i class="fa fa-fax"></i> {{$user->no_fax_perusahaan}} &nbsp;<span class="bd-blue">&nbsp;&nbsp;</span><strong><span class="sender">JOIN DATE</span></strong> • <span class="date">16 January 2016</span></h4>
								<hr>
							</div>
							<div class="p-t-0">
								<form class="form-horizontal">
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Nama Pemimpin Perusahaan</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="nama_pemimpin_perusahaan" value="{{$user->nama_pemimpin_perusahaan}}" readonly="">
											<i class="fa fa-user-secret"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">NPWP Perusahaan</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
										<div class="col-sm-7 prepend-icon">
											<input type="text" class="form-control form-white" name="no_npwp_perusahaan" value="{{$user->no_npwp_perusahaan}}" readonly="">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Alamat Perusahaan</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<textarea rows="3" class="form-control form-white" name="alamat_perusahaan" readonly="">{{$user->alamat_perusahaan}}</textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Kode Pos Perusahaan</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="kode_pos_perusahaan" value="{{$user->kode_pos_perusahaan}}" readonly="">
											<i class="fa fa-map-marker"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Kabupaten / Kota Perusahaan</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="no_npwp_perusahaan" value="{{$user->city}}" readonly="">
											<i class="fa fa-map-marker"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Provinsi Perusahaan</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="no_npwp_perusahaan" value="{{$user->province}}" readonly="">
											<i class="fa fa-map-marker"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Surat Ijin Usaha Perusahaan</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Surat Keterangan Domisili Perusahaan</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Surat Ijin Tempat Usaha Perusahaan</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Tanda Daftar Perusahaan</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
									</div>
								<hr>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="placeholder">
					<div class="placeholder-content col-md-3 ui-droppable">
						<div class="bg-white p-20 bd-6">
							<center>
								<img src="{{url('upload/'.$user->file_foto_user)}}" width="200px" height="200px" class="img-thumbnail">
								<hr>
								<a type="button" class="btn btn-block btn-danger btn-embossed btn-square"><i class="glyphicon glyphicon-lock pull-left"></i>Ganti Password</a>
								<a type="button" class="btn btn-block btn-warning btn-embossed btn-square"><i class="fa fa-pencil-square-o pull-left"></i>Ubah Data</a>
								<hr>
							</center>
						</div>
					</div>
					<div class="placeholder-content col-md-9 ui-droppable">
						<div class="bg-white m-b-20">
							<div class="p-20 p-b-0">
								<h1 class="text-primary"><strong>{{$user->nama_user}}</strong></h1>
								<h4 class="m-t-0"><i class="fa fa-envelope"></i> {{$user->email}} &nbsp;<span class="bd-blue">&nbsp;&nbsp;&nbsp;</span><i class="fa fa-phone"></i> {{$user->no_hp_user}}</h4>
								<hr>
							</div>
							<div class="p-t-0">
								<form class="form-horizontal">
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Nama User</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="nama_user" value="{{$user->nama_user}}" readonly="">
											<i class="fa fa-user"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Username</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="nama_user" value="{{$user->nama_user}}" readonly="">
											<i class="fa fa-user"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">TTL</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="tempat_lahir" value="{{$user->tempat_lahir_user.", ".$user->tanggal_lahir}}" readonly="">
											<i class="fa fa-legal"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Gender</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="gender_user" value="{{$user->gender_user}}" readonly="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">KTP</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
										<div class="col-sm-7 prepend-icon">
											<input type="text" class="form-control form-white" name="no_ktp_user" value="{{$user->no_ktp_user}}" readonly="">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Alamat User</label>
										</div>
										<div class="col-sm-8 prepend-icon">
											<textarea rows="3" class="form-control form-white" name="alamat_user" readonly="">{{$user->alamat_user}}</textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">NPWP</label>
										</div>
										<div class="col-sm-2">
											<a href="#" target="_blank" onclick="window.open('{{url('upload/'.$user->file_npwp_perusahaan)}}')" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-search"></i> View File</a>
										</div>
									</div>
									<hr>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection		
		

@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
@endsection