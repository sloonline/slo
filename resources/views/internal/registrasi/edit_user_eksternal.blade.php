@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Edit User <strong>Peminta Jasa Eksternal</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/users')}}">Manajemen User</a></li>
                    <li class="active">Edit User Peminta Jasa Eksternal</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(array('name'=>'form_register','url'=> 'internal/register_eksternal/', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    {{--{!! Form::open(array('name'=>'form_register','url'=> 'internal/edit-eksternal/'.$user->id, 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}--}}
                    {!! Form::hidden('id',$user->id) !!}
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#dataperusahaan" data-toggle="tab"><i class="fa fa-building-o"></i> Data Perusahaan</a></li>
                            <li><a href="#datauser" data-toggle="tab"><i class="icon-user"></i> Data User</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="dataperusahaan">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Kategori Perusahaan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select class="form-control form-white" name="kategori" onchange="checkform();"
                                                            id="kategori" required>
                                                        <option value="">--- Pilih Kategori Perusahaan ---</option>
                                                        @foreach($kategori as $item)
                                                            <option value="{{$item->id}}" {{($user != null && $user->perusahaan != null && $user->perusahaan->kategori_perusahaan == $item->id)  ? "selected='selected'" : ""}}>{{$item->nama_kategori}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fa fa-building-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama Perusahaan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white"
                                                           name="nama_perusahaan"
                                                           required onkeyup="checkform();"
                                                           value="{{($user == null) ? "" : $user->perusahaan->nama_perusahaan}}"
                                                           placeholder="Nama Perusahan">
                                                    <i class="fa fa-building"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama Pemimpin Perusahaan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white"
                                                           name="nama_pemimpin_perusahaan" required onkeyup="checkform();"
                                                           placeholder="Nama Pemimpin Perusahan"
                                                           value="{{($user == null) ? "": $user->perusahaan->nama_pemimpin_perusahaan}}">
                                                    <i class="fa fa-user-secret"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor NPWP Perusahaan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white"
                                                           name="no_npwp_perusahaan" onkeyup="checkform();"
                                                           required placeholder="Nomor NPWP"
                                                           value="{{($user == null) ? "": $user->perusahaan->no_npwp_perusahaan}}">
                                                    <i class="fa fa-credit-card"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File NPWP Perusahaan</label>
                                            </div>
                                            @if($user != null && $user->perusahaan->file_npwp_perusahaan != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->perusahaan->file_npwp_perusahaan)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" class="custom-file max-file"
                                                               name="file_npwp_perusahaan"
                                                               id="file_npwp_perusahaan"
                                                               accept="application/pdf"
                                                               onchange="document.getElementById('file_npwp_perusahaan_text').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_npwp_perusahaan_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Alamat Perusahaan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <textarea rows="3" class="form-control form-white"
                                                              name="alamat_perusahaan" required onkeyup="checkform();"
                                                              placeholder="Alamat Perusahaan...">{{($user == null) ? "": $user->perusahaan->alamat_perusahaan}}</textarea>
                                                    <i class="icon-pointer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Provinsi *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select class="form-control form-white" required data-search="true"
                                                            name="provinsi" onchange="checkform();"
                                                            id="province">
                                                        @if($user != null)
                                                            <option value="{{$user->perusahaan->id_province}}">{{$user->perusahaan->province->province}}</option>
                                                        @else
                                                            <option valuse="">--- Pilih Provinsi ---</option>
                                                        @endif
                                                        @foreach($province as $item)
                                                            <option value="{{$item->id}}">{{$item->province}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="icon-pointer"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Kabupaten / Kota *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select class="form-control form-white" required data-search="true"
                                                            name="kabupaten" onchange="checkform();"
                                                            id="city">
                                                        @if($user != null)
                                                            <option value="{{$user->perusahaan->id_city}}">{{$user->perusahaan->city->city}}</option>
                                                        @else
                                                            <option value="">--- Pilih Kabupaten / Kota ---</option>
                                                        @endif
                                                        @foreach($city as $item)
                                                            <option value="{{$item->id}}"
                                                                    class="{{$item->id_province}}">{{$item->city}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="icon-pointer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Kode Pos</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" onkeyup="checkform();"
                                                           name="kode_pos_perusahaan" placeholder="Kode Pos"
                                                           value="{{($user == null) ? "":$user->perusahaan->kode_pos_perusahaan}}">
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor Telepon Perusahaan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">

                                                    <input type="text" class="form-control form-white"
                                                           placeholder="Nomor Telepon" onkeyup="checkform();"
                                                           name="no_telepon_perusahaan"
                                                           value="{{($user == null) ? "":$user->perusahaan->no_telepon_perusahaan}}">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nomor Fax Perusahaan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">

                                                    <input type="text" class="form-control form-white"
                                                           name="no_fax_perusahaan" onkeyup="checkform();"
                                                           placeholder="Nomor Fax"
                                                           value="{{($user == null) ? "":$user->perusahaan->no_fax_perusahaan}}">
                                                    <i class="fa fa-fax"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Email Perusahaan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="email" required class="form-control form-white"
                                                           name="email_perusahaan" onkeyup="checkform();"
                                                           placeholder="Email"
                                                           value="{{($user == null) ? "":$user->perusahaan->email_perusahaan}}">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File Surat Ijin Usaha</label>
                                            </div>
                                            @if($user != null && $user->perusahaan->file_siup_perusahaan != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->perusahaan->file_siup_perusahaan)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" class="custom-file max-file"
                                                               name="file_surat_ijin_usaha"
                                                               id="file_surat_ijin_usaha"
                                                               accept="application/pdf"
                                                               onchange="document.getElementById('file_surat_ijin_usaha_text').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_surat_ijin_usaha_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File Surat Keterangan Domisili
                                                    Perusahaan</label>
                                            </div>
                                            @if($user != null && $user->perusahaan->file_skdomisili_perusahaan != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->perusahaan->file_skdomisili_perusahaan)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" id="file_skdp"
                                                               accept="application/pdf"
                                                               class="custom-file max-file" name="file_skdp"
                                                               onchange="document.getElementById('file_skdp_text').value = this.value;checkform();">
                                                        <input type="text" id="file_skdp_text"
                                                               class="form-control form-white" id="file_skdp"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File Surat Ijin Tempat Usaha</label>
                                            </div>
                                            @if($user != null && $user->perusahaan->file_situ_perusahaan != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->perusahaan->file_situ_perusahaan)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file"
                                                               accept="application/pdf"
                                                               class="custom-file max-file" name="file_situ" id="file_situ"
                                                               onchange="document.getElementById('file_situ_text').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_situ_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File Tanda Daftar Perusahaan</label>
                                            </div>
                                            @if($user != null && $user->perusahaan->file_tdp_perusahaan != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->perusahaan->file_tdp_perusahaan)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file"
                                                               accept="application/pdf"
                                                               class="custom-file max-file" name="file_tdp_perusahaan"
                                                               id="file_tdp_perusahaan"
                                                               onchange="document.getElementById('file_tdp_perusahaan_text').value =
                                                        this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_tdp_perusahaan_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="datauser">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama User *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" name="nama_user"
                                                           required onkeyup="checkform();"
                                                           placeholder="Nama User"
                                                           value="{{($user == null) ? "":$user->nama_user}}">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Username *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" name="username"
                                                           required onkeyup="checkform();"
                                                           placeholder="Username"
                                                           value="{{($user == null) ? "":$user->username}}">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Email User *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="email" class="form-control form-white" name="email_user"
                                                           required onkeyup="checkform();"
                                                           placeholder="Email"
                                                           value="{{($user == null) ? "":$user->email}}">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Tempat Lahir *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" name="tempat_lahir"
                                                           required onkeyup="checkform();"
                                                           placeholder="Tempat Lahir"
                                                           value="{{($user == null) ? "":$user->tempat_lahir_user}}">
                                                    <i class="fa fa-legal"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Tanggal Lahir *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" name="tgl_lahir"
                                                           class="b-datepicker form-control form-white" required
                                                           placeholder="Tanggal Lahir" data-view="2" onchange="checkform();"
                                                           value="{{($user == null) ? "":@$user->tanggal_lahir}}">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Gender *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="icheck-inline">
                                                        <label class="text-muted block"><input type="radio"
                                                                                               name="gender_user" value="PRIA"
                                                                                               {{($user != null && $user->gender_user == "PRIA")? "checked": ""}} data-radio="iradio_square-blue">
                                                            Pria</label>
                                                        <label class="text-muted block"><input type="radio"
                                                                                               name="gender_user" value="WANITA"
                                                                                               {{($user != null && $user->gender_user == "WANITA") ? "checked" : ""}} data-radio="iradio_square-blue">
                                                            Wanita</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Alamat User *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <textarea rows="3" class="form-control form-white" name="alamat_user"
                                                              required onkeyup="checkform();"
                                                              placeholder="Alamat...">{{($user == null) ? "":$user->alamat_user}}</textarea>
                                                    <i class="icon-pointer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">No KTP</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" name="no_ktp_user"
                                                           placeholder="Nomor KTP" onkeyup="checkform();"
                                                           value="{{($user == null) ? "":$user->no_ktp_user}}">
                                                    <i class="fa fa-credit-card"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">No Handphone</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" class="form-control form-white" name="no_hp_user"
                                                           placeholder="Nomor Handphone" onkeyup="checkform();"
                                                           value="{{($user == null) ? "":$user->no_hp_user}}">
                                                    <i class="fa fa-mobile"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File NPWP</label>
                                            </div>
                                            @if($user != null && $user->file_npwp_user != null)
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->file_npwp_user)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" class="custom-file max-file"
                                                               accept="application/pdf"
                                                               name="file_npwp_user" id="file_npwp_user"
                                                               onchange="document.getElementById('file_npwp_user_text').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_npwp_user_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">File KTP</label>
                                            </div>
                                            @if($user != null && $user->file_ktp_user != "")
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->file_ktp_user)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            @endif
                                            <div class="{{($user == null) ? "col-sm-9": "col-sm-7"}}">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" class="custom-file max-file" name="file_ktp_user"
                                                               id="file_ktp_user"
                                                               onchange="document.getElementById('file_ktp_user_text').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white"
                                                               id="file_ktp_user_text"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb(pdf)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Foto</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" name="file_foto_user" id="file_foto_user"
                                                               class="custom-file max-file" id="file_foto_user"
                                                               name="avatar" id="avatar" accept="image/*"
                                                               onchange="document.getElementById('file_foto_user_text').value = this.value;checkform();">
                                                        <input type="text" id="file_foto_user_text"
                                                               class="form-control form-white"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb (jpg/png/gif)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-9">
                                                <img id="preview_img"
                                                     src="{{($user != null && $user->file_foto_user != null) ?  url('upload/'.$user->file_foto_user ): "#"}}"
                                                     alt="foto" width="200"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="row">
                            <div class="col-md-12">
                            @if($user != null && $user->status_user != "")
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control bg-aero" type="text" value="{{$user->status_user}}">
                                    </div>
                                </div>
                            @endif
                            @if($user != null && $user->keterangan_pending!= "" && $user->status_user == "PENDING")
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea rows="3"
                                                  class="form-control bg-aero">{{$user->keterangan_pending}}</textarea>
                                    </div>
                                </div>
                            @endif
                            </div>

                        {{--@endif--}}
                        {{--@if($user != null && $user->keterangan_pending != "" && $user->status_user == "PENDING")--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-sm-2 col-sm-offset-3">--}}
                                    {{--<label class="col-sm-12 control-label">Keterangan</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-7">--}}
                                    {{--<textarea rows="3"--}}
                                              {{--class="form-control bg-aero">{{$user->keterangan_pending}}</textarea>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <hr>
                        <div class="pull-right p-b-10">
                            <a href="{{url('/internal/users')}}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            @if($user != null && $user->status_user != "VERIFIED")
                                {{--user perlu approval--}}

                                <button type="submit" name="approved" value="approved"
                                        onclick="return confirm('Apakah anda yakin untuk meangaktifkan user ini?')"
                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Approve &nbsp;<i class="fa fa-thumbs-o-up"></i>
                                </button>
                                <a data-toggle="modal" href="#modal-pending"
                                   class="btn btn-danger ladda-button btn-square btn-embossed"
                                   data-style="zoom-in">Pending &nbsp;<i class="fa fa-thumbs-o-down"></i>
                                </a>
                                <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            @else
                                <button type="submit" id="submit_form" value="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        {{--modal isi keterangan alasan di pending--}}
        <div class="modal fade" id="modal-pending" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Keterangan Pending User</strong> user</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {!! Form::open(array('url'=> 'internal/pending_user/', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                                <input type="hidden" name="id" value="{{$id}}"/>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                <textarea rows="5" class="form-control form-white" name="keterangan_pending" required
                                          placeholder="
                                          Alasan pending user...">{{($user == null) ? "": $user->keterangan_pending}}</textarea>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button onclick="return confirm('Apakah anda yakin melakukan pending untuk user ini?')"
                                            type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                            data-style="zoom-in">Pending &nbsp;<i
                                                class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
    <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.tab-pane input, .tab-pane textarea').on('invalid', function () {

                // Find the tab-pane that this element is inside, and get the id
                var $closest = $(this).closest('.tab-pane');
                var id = $closest.attr('id');

                // Find the link that corresponds to the pane and have it show
                $('.nav a[href="#' + id + '"]').tab('show');

            });
        });
        $("#city").chained("#province");
        @if($user == null || $user->file_foto_user == null)
            $('#preview_img').hide();
        @endif

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('#preview_img').show();
            }
        }

        $("#file_foto_user").change(function () {
            $('#preview_img').hide();
            readURL(this);
        });

        /*===06092016 ENABLE SUBMIT WHEN ALL INPUTS COMPLETED====*/
        function checkform() {
            var f = document.forms["form_register"].elements;
            var cansubmit = true;

            for (var i = 0; i < f.length; i++) {
                if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                    cansubmit = false;
                    console.log(f[i]);
                }
            }
            document.getElementById('submit_form').disabled = !cansubmit;
        }
        window.onload = checkform;
    </script>
@endsection


