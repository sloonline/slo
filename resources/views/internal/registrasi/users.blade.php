@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Manajemen <strong>User</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Manajemen User</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                {{--@if(Session::has('message'))--}}
                    {{--<div class="alert alert-success">--}}
                        {{--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}}
                        {{--<i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}--}}
                    {{--</div>--}}
                {{--@endif--}}

                <div class="panel">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#internal" data-toggle="tab"><i class="icon-user"></i> User Internal</a></li>
                            <li><a href="#pln" data-toggle="tab"><i class="icon-energy"></i> User PLN</a></li>
                            <li><a href="#eksternal" data-toggle="tab"><i class="icon-globe"></i> User Eksternal</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="internal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-b-20 m-t-20">
                                            <div class="btn-group">
                                                <a href="{{url('/internal/register')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah User</a>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-dynamic">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIP</th>
                                                <th>Nama</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Bidang & Jabatan</th>
                                                <th>Level Akses</th>
                                                <th class="text-center" style="width: 150px;">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no = 1;
                                            ?>
                                            @foreach($user_internal as $row)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->nip_user}}</td>
                                                    <td>{{$row->nama_user}}</td>
                                                    <td>{{$row->username}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>
                                                        {{((isset($row->sub_bidang->nama_sub_bidang)) ? @$row->sub_bidang->nama_sub_bidang : "")." ".@$row->bidang->nama_bidang}}
                                                    </td>
                                                    <td>
                                                        @foreach($row->user_roles as $role)
                                                            {{@$role->roles->display_name}} ,
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-primary btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Detail"
                                                               href="{{url("internal/view_user/" . $row->id)}}">
                                                               <i class="fa fa-eye"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-warning btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Edit"
                                                               href="{{url("internal/register/" . $row->id)}}">
                                                               <i class="fa fa-pencil-square-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-danger btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Delete"
                                                               onclick="return confirm('Apakah anda yakin untuk menghapus user ini?')"
                                                               href="{{url("internal/delete_internal/" . $row->id)}}">
                                                               <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $no++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pln">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-b-20 m-t-20">
                                            <div class="btn-group">
                                                <a href="{{url('/internal/create_user_peminta_jasa_pln')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah User</a>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-dynamic">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                {{--<th>NIP</th>--}}
                                                <th>Nama</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Unit</th>
                                                <th class="text-center" style="width: 150px;">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no = 1;
                                            ?>
                                            @foreach($user_pln as $row)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    {{--<td>{{$row->nip}}</td>--}}
                                                    <td>{{$row->nama_user}}</td>
                                                    <td>{{$row->username}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{($row->perusahaan != null && $row->perusahaan->company_code != null) ? $row->perusahaan->company_code->description : ""}}</td>
                                                    <td>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-primary btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Detail"
                                                               href="{{url("internal/view_user/" . $row->id)}}">
                                                               <i class="fa fa-eye"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-warning btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Edit"
                                                               href="{{url("internal/edit_user_peminta_jasa_pln/" . $row->id)}}">
                                                               <i class="fa fa-pencil-square-o"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <a class="btn btn-sm btn-danger btn-square btn-embossed"
                                                               data-rel="tooltip"
                                                               data-placement="top"
                                                               data-original-title="Delete"
                                                               onclick="return confirm('Apakah anda yakin untuk menghapus user ini?')"
                                                               href="{{url("internal/delete_user_peminta_jasa_pln/" . $row->id)}}">
                                                               <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $no++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="eksternal">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-b-20 m-t-20">
                                            <div class="btn-group">
                                                <a href="{{url('/internal/register_eksternal')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah User</a>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-dynamic">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Kategori Perusahaan</th>
                                                <th>Status</th>
                                                <th class="text-center" style="width: 150px;">Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no = 1;
                                            ?>
                                            @foreach($user_eksternal as $row)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->nama_user}}</td>
                                                    <td>{{$row->username}}</td>
                                                    <td>{{$row->email}}</td>
                                                    <td>{{($row->perusahaan != null && $row->perusahaan->kategori != null) ? $row->perusahaan->kategori->nama_kategori : ""}}</td>
                                                    <td>{{$row->status_user}}</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <a class="btn btn-sm btn-primary btn-square btn-embossed"
                                                                   data-rel="tooltip"
                                                                   data-placement="top"
                                                                   data-original-title="Detail"
                                                                   href="{{url("internal/det_reg_peminta/" . $row->id)}}">
                                                                   <i class="fa fa-eye"></i>
                                                                </a>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <a class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                   data-rel="tooltip"
                                                                   data-placement="top"
                                                                   data-original-title="Edit"
                                                                   href="{{url("/internal/register_eksternal/".$row->id)}}">
                                                                   <i class="fa fa-pencil-square-o"></i>
                                                                </a>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <a class="btn btn-sm btn-danger btn-square btn-embossed"
                                                                   data-rel="tooltip"
                                                                   data-placement="top"
                                                                   data-original-title="Delete"
                                                                   onclick="return confirm('Apakah anda yakin untuk menghapus user ini?')"
                                                                   href="{{url("/internal/delete_user_eksternal/".$row->id)}}">
                                                                   <i class="fa fa-trash"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $no++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <!-- Buttons Loading State -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
    <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- >Bootstrap Date Picker -->
@endsection