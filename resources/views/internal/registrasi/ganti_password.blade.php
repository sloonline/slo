@extends('../layout/layout_window')

@section('page_css')
@endsection

@section('content')
    <div class="page-content m-0 p-0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-content">
                        <div class="">
                            <h3 style="font-weight: bold;">
                                <br/>
                            </h3>
                        </div>
                        <br/>
                        <div class="">
                            <div class="btn-group">
                                {!! Form::open(array('url'=> (strtoupper($program->jenisProgram->acuan_1) == "AREA")?"/master/area_program/input" : "/master/kontrak/input", 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                <input type="hidden" name="id_program"
                                       value="{{$program->id}}"/>
                                <input type="hidden" name="id" value="0"/>
                                <button type="submit"
                                        class="btn btn-success btn-square btn-block btn-embossed"><i
                                            class="fa fa-plus"></i> Tambah Area/Kontrak
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <table class="tree table table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe</th>
                                <th>Nama</th>
                                <th colspan="2">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach($acuan1 as $row)
                                <tr class="treegrid-ac1{{$row['id']}}">
                                    <td>{{$no}}</td>
                                    <td>{{$row['tipe']}}</td>
                                    <td>{{$row['name']}}</td>
                                    <td style="width: 50px;">
                                        {!! Form::open(array('url'=> $row['url_ac2'], 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                        <input type="hidden" name="id_program"
                                               value="{{$program->id}}"/>
                                        <input type="hidden" name="id_area_kontrak"
                                               value="{{$row['id']}}"/>
                                        <input type="hidden" name="id" value="0"/>
                                        <button type="submit"
                                                class="btn btn-sm btn-default btn-square btn-embossed"
                                                data-rel="tooltip" data-placement="top"
                                                data-original-title="Tambah Lokasi / Penyulang / GI">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                    <td style="width: 50px;">
                                        {!! Form::open(array('url'=> 'master/delete_program_child', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                        <input type="hidden" name="id_program"
                                               value="{{$program->id}}"/>
                                        <input type="hidden" name="id"
                                               value="{{$row['id']}}"/>
                                        <input type="hidden" name="tipe"
                                               value="{{$row['tipe']}}"/>
                                        <button type="submit"
                                                class="btn btn-sm btn-danger btn-square btn-embossed"
                                                data-rel="tooltip" data-placement="top"
                                                data-original-title="Hapus">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                <?php $no_2 = 1; ?>
                                @foreach($row['ac2'] as $row2)
                                    <tr class="treegrid-ac2{{$row2['id']}}  treegrid-parent-ac1{{$row['id']}}">
                                        <td>{{$no.".".$no_2}}</td>
                                        <td>{{$row2['tipe']}}</td>
                                        <td>{{$row2['name']}}</td>
                                        <td>
                                            {!! Form::open(array('url'=> "/master/lingkup_program/input", 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                            <input type="hidden" name="id_program"
                                                   value="{{$program->id}}"/>
                                            <input type="hidden" name="id_acuan_2"
                                                   value="{{$row2['id']}}"/>
                                            <input type="hidden" name="id" value="0"/>
                                            <button type="submit"
                                                    class="btn btn-sm btn-default btn-square btn-embossed"
                                                    data-rel="tooltip" data-placement="top"
                                                    data-original-title="Tambah Lingkup Pekerjaan">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            {!! Form::open(array('url'=> 'master/delete_program_child', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                            <input type="hidden" name="id_program"
                                                   value="{{$program->id}}"/>
                                            <input type="hidden" name="id"
                                                   value="{{$row2['id']}}"/>
                                            <input type="hidden" name="tipe"
                                                   value="{{$row2['tipe']}}"/>
                                            <button type="submit"
                                                    class="btn btn-sm btn-danger btn-square btn-embossed"
                                                    data-rel="tooltip" data-placement="top"
                                                    data-original-title="Hapus">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    <?php $no_3 = 1; ?>
                                    @foreach($row2['ac3'] as $row3)
                                        <tr class="treegrid-ac3{{$row3['id']}}  treegrid-parent-ac2{{$row2['id']}}">
                                            <td>{{$no.".".$no_2.".".$no_3}}</td>
                                            <td>{{"LINGKUP"}}</td>
                                            <td>{{$row3->lingkupPekerjaan->jenis_lingkup_pekerjaan}}</td>
                                            <td colspan="2">
                                                {!! Form::open(array('url'=> 'master/delete_program_child', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                                <input type="hidden" name="id_program"
                                                       value="{{$program->id}}"/>
                                                <input type="hidden" name="id"
                                                       value="{{$row3->id}}"/>
                                                <input type="hidden" name="tipe"
                                                       value="LINGKUP"/>
                                                <button type="submit"
                                                        class="btn btn-sm btn-danger btn-square btn-embossed"
                                                        data-rel="tooltip" data-placement="top"
                                                        data-original-title="Hapus">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        <?php $no_3++; ?>
                                    @endforeach
                                    <?php $no_2++; ?>
                                @endforeach
                                <?php $no++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id" value="">
                            <button type="button" onclick="window.open('', '_self', ''); window.close();"
                                    class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Selesai &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('page_script')
@endsection