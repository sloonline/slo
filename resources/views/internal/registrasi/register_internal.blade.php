@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Input User<strong> Internal Pusertif</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/users')}}">Manajemen User</a></li>
                    <li class="active">{{($id != null) ? "Ubah" : "Tambah"}} User Internal Pusertif</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    {!! Form::open(array('name' => 'form_register','url'=> 'internal/register', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    {!! Form::hidden('id',$id) !!}
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#dataumum" data-toggle="tab"><i class="icon-info"></i> Data Umum</a></li>
                            <li><a href="#datauser" data-toggle="tab"><i class="icon-user"></i> Data User</a></li>
                            <li><a href="#hakakses" data-toggle="tab"><i class="fa fa-gear"></i> Hak Akses User</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="dataumum">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Status Pekerja</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select name="status_pekerja" id="status_pekerja"
                                                            class="form-control form-white"
                                                            data-style="white"
                                                            data-placeholder="Pilih status pekerja..." onchange="checkform();">
                                                        <option value="ORGANIK" {{($user != null && $user->status_pekerja == "ORGANIK") ? "selected" : "" }}>
                                                            Organik PLN Jaser
                                                        </option>
                                                        <option value="OS" {{($user != null && $user->status_pekerja == "OS") ? "selected" : "" }}>
                                                            Non-organik/OS PLN Jaser
                                                        </option>
                                                        <option value="KOORDINATOR" {{($user != null && $user->status_pekerja == "KOORDINATOR") ? "selected" : "" }}>
                                                            Koordinator Perwakilan
                                                        </option>
                                                    </select>
                                                    <i class="fa fa-wrench"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Area Bidang</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select name="id_bidang" id="id_bidang" required="required"
                                                            class="form-control form-white" onchange="checkform();"
                                                            data-style="white" data-placeholder="Pilih bidang...">
                                                        @foreach($bidang as $row)
                                                            <option value="{{$row->id}}" {{($user != null && $user->id_bidang == $row->id) ? "selected" : "" }}> {{$row->nama_bidang}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fa fa-street-view"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Sub Bidang</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select name="id_sub_bidang" id="sub_bidang" onchange="checkform();"
                                                            class="form-control form-white" data-style="white"
                                                            data-placeholder="Pilih Sub Bidang...">
                                                        @foreach($sub_bidang as $row)
                                                            <option value="{{$row->id}}"
                                                                    class="{{$row->id_bidang}}" {{($user != null && $user->id_sub_bidang == $row->id) ? "selected" : "" }}> {{$row->nama_sub_bidang}}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="fa fa-street-view"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="datauser">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Nama Lengkap *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" required="required" name="nama_user" id="nama_user"
                                                           class="form-control form-white lastname"
                                                           placeholder="Nama Lengkap" onkeyup="checkform()"
                                                           {{($user != null) ? "readonly" : ""}}
                                                           value="{{($user != null)?$user->nama_user : null}}">
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">NIP / User ID </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" name="nip_user" id="nip_user"
                                                           class="form-control form-white lastname"
                                                           placeholder="NIP / User ID" onkeyup="checkform()"
                                                           value="{{($user != null)?$user->nip_user : null}}">
                                                    <i class="icon-tag"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Username *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" required="required" id="username" name="username"
                                                           id="username"
                                                           class="form-control form-white username" placeholder="Username"
                                                           value="{{($user != null)?$user->username : null}}"
                                                           onkeyup="checkform()"
                                                            {{($user != null && $user->is_user_ad == 1) ? "readonly" : ""}}>
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Email *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="email" required="required" id="email" name="email"
                                                           id="lastname"
                                                           class="form-control form-white lastname" placeholder="Email"
                                                           onkeyup="checkform()"
                                                           value="{{($user != null)?$user->email : null}}">
                                                    <i class="icon-envelope"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Tempat Lahir </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" name="tempat_lahir_user"
                                                           id="tempat_lahir_user" onkeyup="checkform()"
                                                           class="form-control form-white lastname"
                                                           placeholder="Tempat Lahir"
                                                           value="{{($user != null)?$user->tempat_lahir_user : null}}">
                                                    <i class="icon-direction"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Tanggal Lahir </label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" name="tanggal_lahir" id="tanggal_lahir"
                                                           onchange="checkform()"
                                                           class="date-picker form-control form-white"
                                                           placeholder="Tanggal Lahir"
                                                           value="{{($user != null)? $user->tanggal_lahir : null}}"/>
                                                    <i class="icon-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Alamat</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <textarea rows="3" name="alamat_user" id="alamat_user"
                                                              class="form-control form-white" onkeyup="checkform()"
                                                              placeholder="Alamat Lengkap...">{{($user != null)?$user->alamat_user : null}}</textarea>
                                                    <i class="icon-pointer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">No HP</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" name="no_hp_user" id="no_hp_user"
                                                           onkeyup="checkform()"
                                                           class="form-control form-white lastname"
                                                           placeholder="Nomor Handphone"
                                                           value="{{($user != null)?$user->no_hp_user : null}}">
                                                    <i class="icon-call-in"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Grade *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" required="required" name="grade_user" id="grade_user"
                                                           class="form-control form-white lastname" placeholder="Grade"
                                                           onkeyup="checkform()"
                                                           value="{{($user != null)?$user->grade_user : null}}">
                                                    <i class="icon-briefcase"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Jabatan *</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <input type="text" required="required" name="jabatan_user"
                                                           id="jabatan_user"
                                                           class="form-control form-white lastname" placeholder="Jabatan"
                                                           onkeyup="checkform()"
                                                           value="{{($user != null)?$user->jabatan_user : null}}">
                                                    <i class="icon-briefcase"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Pendidikan Terakhir</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="append-icon">
                                                    <select name="pendidikan_user" id="pendidikan_user"
                                                            class="form-control form-white"
                                                            data-style="white" onchange="checkform();"
                                                            data-placeholder="Pendidikan Terakhir">
                                                        <option value="">--- Pilih Tingkat Pendidikan ---</option>
                                                        <option value="S3" {{($user != null && $user->pendidikan_user == "S3")? "selected='selected'": ""}}>
                                                            S3
                                                        </option>
                                                        <option value="S2" {{($user != null && $user->pendidikan_user == "S2")? "selected='selected'": ""}}>
                                                            S2
                                                        </option>
                                                        <option value="S1" {{($user != null && $user->pendidikan_user == "S1")? "selected='selected'": ""}}>
                                                            S1
                                                        </option>
                                                        <option value="D4" {{($user != null && $user->pendidikan_user == "D4")? "selected='selected'": ""}}>
                                                            D4
                                                        </option>
                                                        <option value="D3" {{($user != null && $user->pendidikan_user == "D3")? "selected='selected'": ""}}>
                                                            D3
                                                        </option>
                                                        <option value="D1" {{($user != null && $user->pendidikan_user == "D1")? "selected='selected'": ""}}>
                                                            D1
                                                        </option>
                                                        <option value="SMA" {{($user != null && $user->pendidikan_user == "SMA")? "selected='selected'": ""}}>
                                                            SMA/SMK/Sederajat
                                                        </option>
                                                    </select>
                                                    <i class="icon-graduation"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Jenis Kelamin</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="icheck-inline">
                                                        <label class="text-muted block"><input type="radio" value="PRIA"
                                                                                               name="gender_user"
                                                                                               {{($user != null && $user->gender_user == "PRIA")? "checked": ""}} data-radio="iradio_square-blue">
                                                            Pria</label>
                                                        <label class="text-muted block"><input type="radio" value="WANITA"
                                                                                               name="gender_user"
                                                                                               {{($user != null && $user->gender_user == "WANITA") ? "checked" : ""}} data-radio="iradio_square-blue">
                                                            Wanita</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Status Pernikahan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="icheck-inline">
                                                        <label class="text-muted block"><input type="radio" value="MENIKAH"
                                                                                               name="status_pernikahan"
                                                                                               {{($user != null && $user->status_pernikahan == "MENIKAH")? "checked": ""}} data-radio="iradio_square-blue"
                                                                                               id="status_pernikahan_user">
                                                            Menikah</label>
                                                        <label class="text-muted block"><input type="radio" value="SINGLE"
                                                                                               name="status_pernikahan"
                                                                                               {{($user != null && $user->status_pernikahan == "SINGLE")? "checked": ""}} data-radio="iradio_square-blue"
                                                                                               id="status_pernikahan_user">
                                                            Single</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Foto</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">Choose File</span>
                                                        <input type="file" name="file_foto_user" id="file_foto_user"
                                                               class="custom-file"
                                                               name="avatar" id="avatar" accept="image/*"
                                                               onchange="document.getElementById('uploader').value = this.value;checkform();">
                                                        <input type="text" class="form-control form-white" id="uploader"
                                                               placeholder="no file selected" readonly="">
                                                        <small class="text-muted block"><i class="icon-paper-clip"></i> Max
                                                            file size: 1Mb (jpg/png/gif)
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-9">
                                                <img id="preview_img"
                                                     src="{{($user != null && $user->file_foto_user != null) ?  url('upload/'.$user->file_foto_user ): "#"}}"
                                                     alt="foto" width="200"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hakakses">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Level Akses</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="icheck-list">
                                                    @foreach($roles as $row)
                                                        <input type="checkbox" data-checkbox="icheckbox_square-blue"
                                                               name="roles[]"
                                                               class="roles"
                                                               value="{{$row->id}}"
                                                        <?php
                                                                $stat = 0;
                                                                $i = 0;
                                                                while ($stat == 0 && $i < sizeof($user_roles)) {
                                                                    if ($user_roles[$i]->role_id == $row->id) {
                                                                        $stat = 1;
                                                                        echo "checked";
                                                                    } else {
                                                                        $i++;
                                                                    }
                                                                }
                                                                ?>
                                                        />
                                                        {{$row->display_name}}<br/><br/>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix bg-white no-border">
                        <hr>
                        <div class="pull-right p-b-10">
                            <a href="{{ url('internal/users') }}" class="btn btn-warning btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" onclick="return validate()" id="submit_form" value="simpan"
                                    class="btn btn-success btn-square btn-embossed">
                                Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.tab-pane input, .tab-pane textarea').on('invalid', function () {

                // Find the tab-pane that this element is inside, and get the id
                var $closest = $(this).closest('.tab-pane');
                var id = $closest.attr('id');

                // Find the link that corresponds to the pane and have it show
                $('.nav a[href="#' + id + '"]').tab('show');

            });
        });

        $('.penugasan').hide();
        $(document).ready(function () {
            @if($user == null)
            $('.box_penugasan').iCheck('uncheck');
            @endif
            $(".penugasan").hide();
            $(".subbidang_" + $("#sub_bidang").val()).show();

        });
        @if($user == null || $user->file_foto_user == null)
            $('#preview_img').hide();
        @endif

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('#preview_img').show();
            }
        }

        $("#file_foto_user").change(function () {
            $('#preview_img').hide();
            readURL(this);
        });
        $("#sub_bidang").chained("#id_bidang");
        $("#sub_bidang").change(function () {
            $('.box_penugasan').iCheck('uncheck');
            $(".penugasan").hide();
            $(".subbidang_" + this.value).show();
        });
        function validate() {
            var checked = $("input[name='roles[]']:checked").length;
            if (checked == 0) {
                alert("Pilih salah satu level akses");
                return false;
            }
        }

        /*===06092016 ENABLE SUBMIT WHEN ALL INPUTS COMPLETED====*/
        function checkform() {
            var f = document.forms["form_register"].elements;
            var cansubmit = true;

            for (var i = 0; i < f.length; i++) {
                if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required")  != null) {
                    cansubmit = false;
                }
            }
            document.getElementById('submit_form').disabled = !cansubmit;
        }
        window.onload = checkform;
    </script>
    <!-- Buttons Loading State -->
@endsection

