@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Input User <strong>Peminta Jasa PLN</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/users')}}">Manajemen User</a></li>
                    <li class="active">Tambah User Peminta Jasa PLN</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(array('name'=>'form_register','url'=> 'internal/create_user_peminta_jasa_pln', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    <div class="panel-content">
                        <div class="nav-tabs2" id="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#data_perusahaan" data-toggle="tab"><i
                                                class="icon-home"></i>Data Perusahaan</a></li>
                                <li><a href="#data_user" data-toggle="tab" }}><i class="icon-user"></i> Data User</a>
                                <li><a href="#ijin_usaha" data-toggle="tab" }}><i class="icon-wrench"></i> Ijin
                                        Usaha</a>
                                </li>
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active" id="data_perusahaan">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Company Code *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control form-white" data-search="true"
                                                    name="id_company_code" onchange="checkform();"
                                                    id="id_company_code" required>
                                                @foreach($company_code as $item)
                                                    <option value="{{$item->company_code}}">{{$item->company_code ." - ".$item->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Area *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control form-white" data-search="true"
                                                    name="id_business_area" onchange="checkform();"
                                                    id="id_business_area" required>
                                                @foreach($business_area as $item)
                                                    <option value="{{$item->id}}"
                                                            class="{{$item->company_code}}">{{$item->business_area." - ".$item->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama Pemimpin Unit *</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white"
                                                   name="nama_pemimpin_perusahaan" onkeyup="checkform();" required>
                                            <i class="fa fa-user-secret"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Alamat Unit</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                      <textarea rows="3" class="form-control form-white" name="alamat_perusahaan" onkeyup="checkform();"
                                placeholder="Alamat"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Provinsi *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control form-white" data-search="true" name="provinsi"
                                                    onchange="checkform();"
                                                    id="province" required>
                                                @foreach($province as $item)
                                                    <option value="{{$item->id}}">{{$item->province}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kabupaten / Kota *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control form-white" data-search="true" name="kabupaten"
                                                    onchange="checkform();"
                                                    id="city" required>
                                                @foreach($city as $item)
                                                    <option value="{{$item->id}}"
                                                            class="{{$item->id_province}}">{{$item->city}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kode Pos</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white"
                                                   name="kode_pos_perusahaan" onkeyup="checkform();">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Telepon Unit</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white"
                                                   name="no_telepon_perusahaan" onkeyup="checkform();">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Fax Unit</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="no_fax_perusahaan"
                                                   onkeyup="checkform();">
                                            <i class="fa fa-fax"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="data_user">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama User *</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="nama_user" required
                                                   onkeyup="checkform();">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Username *</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="username"
                                                   onkeyup="checkform();" required>
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Email User *</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="email" class="form-control form-white" name="email_user"
                                                   onkeyup="checkform();" required>
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tempat Lahir</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="tempat_lahir"
                                                   onkeyup="checkform();">
                                            <i class="fa fa-legal"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal Lahir</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" name="tgl_lahir"
                                                   class="b-datepicker form-control form-white" onchange="checkform();"
                                                   placeholder="Pilih Tanggal" data-view="2">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Gender *</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <select name="gender_user" class="form-control form-white" required
                                                    data-style="white">
                                                <option value="PRIA">Pria</option>
                                                <option value="WANITA">Wanita</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Alamat User</label>
                                        </div>
                                        <div class="col-sm-8 prepend-icon">
                  <textarea rows="3" class="form-control form-white" name="alamat_user" onkeyup="checkform();"
                            placeholder="Alamat.."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">No KTP</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="no_ktp_user"
                                                   onkeyup="checkform();">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">No Handphone</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="text" class="form-control form-white" name="no_hp_user">
                                            <i class="fa fa-mobile"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File NPWP</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file max-file"
                                                           name="file_npwp_user" id="file_npwp_user"
                                                           accept="application/pdf"
                                                           onchange="document.getElementById('file_npwp_user_text').value = this.value;checkform();">
                                                    <input type="text" class="form-control form-white"
                                                           id="file_npwp_user_text"
                                                           placeholder="no file selected" readonly="">
                                                    <small class="text-muted block">Max file size: 1Mb(pdf)</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File KTP</label>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file max-file" id="file_ktp_user"
                                                           name="file_ktp_user" accept="application/pdf"
                                                           onchange="document.getElementById('file_ktp_user_text').value = this.value;checkform();">
                                                    <input type="text" class="form-control form-white"
                                                           id="file_ktp_user_text"
                                                           placeholder="no file selected" readonly="">
                                                    <small class="text-muted block">Max file size: 1Mb(pdf)</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="ijin_usaha">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jenis Ijin *</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="append-icon">
                                                        <select class="form-control form-white" name="jenis_ijin_usaha"
                                                                onchange="checkform();" required
                                                                id="kategori">
                                                            <option value="">--- Pilih Jenis Ijin ---</option>
                                                            @foreach($jenis_ijin_usaha as $item)
                                                                <option value="{{$item->id}}" {{(@$pemilik->jenis_ijin_usaha == $item->id)  ? "selected='selected'" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input name="penerbit_ijin_usaha" type="text"
                                                           class="form-control form-white"
                                                           value="{{@$pemilik->penerbit_ijin_usaha}}"
                                                           onkeyup="checkform()"
                                                           placeholder="Penerbit Ijin Usaha">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input name="no_ijin_usaha" type="text"
                                                           class="form-control form-white"
                                                           value="{{@$pemilik->no_ijin_usaha}}"
                                                           placeholder="Nomor Ijin Usaha" onkeyup="checkform()">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku Ijin
                                                        Usaha</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text"
                                                           name="masa_berlaku_iu" id="masa_berlaku_iu"
                                                           value="{{(@$pemilik == null) ? "":  date('d-m-Y',strtotime(@$pemilik->masa_berlaku_iu))}}"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="Masa Berlaku Ijin Usaha">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Surat Ijin Usaha</label>
                                                </div>
                                                @if(@$pemilik->file_siup != null)
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.@$pemilik->file_siup)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                @endif
                                                <div class="{{(@$pemilik->file_siup == null) ? "col-sm-8": "col-sm-6"}}">
                                                    <div class="file">
                                                        <div class="option-group">
                                                            <span class="file-button btn-primary">Choose File</span>
                                                            <input type="file" class="custom-file max-file"
                                                                   accept="application/pdf"
                                                                   name="file_siup" id="file_siup"
                                                                   onchange="document.getElementById('file_siup_text').value = this.value;checkform();">
                                                            <input type="text" class="form-control form-white"
                                                                   id="file_siup_text"
                                                                   placeholder="no file selected" readonly="">
                                                            <small class="text-muted block"><i
                                                                        class="icon-paper-clip"></i>
                                                                Max
                                                                file size: 1Mb(pdf)
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Kontrak</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input name="nama_kontrak" type="text"
                                                           class="form-control form-white"
                                                           onchange="checkform()" value="{{@$pemilik->nama_kontrak}}"
                                                           placeholder="Nama Kontrak"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input name="no_kontrak" type="text" class="form-control form-white"
                                                           onchange="checkform()" value="{{@$pemilik->no_kontrak}}"
                                                           placeholder="Nomor Kontrak"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Pengesahan
                                                        Kontrak</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text"
                                                           name="tgl_pengesahan_kontrak" id="tgl_pengesahan_kontrak"
                                                           value="{{(@$pemilik == null) ? "":  date('d-m-Y',strtotime(@$pemilik->tgl_pengesahan_kontrak))}}"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="Tanggal Pengesahan Kontrak">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku Kontrak</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text"
                                                           name="masa_berlaku_kontrak" id="masa_berlaku_kontrak"
                                                           value="{{(@$pemilik == null) ? "":  date('d-m-Y',strtotime(@$pemilik->masa_berlaku_kontrak))}}"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="Masa Berlaku Kontrak">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                                </div>
                                                @if(@$pemilik->file_kontrak != null)
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.@$pemilik->file_kontrak)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                @endif
                                                <div class="{{(@$pemilik->file_kontrak == null) ? "col-sm-8": "col-sm-6"}}">
                                                    <div class="file">
                                                        <div class="option-group">
                                                            <span class="file-button btn-primary">Choose File</span>
                                                            <input type="file" class="custom-file max-file"
                                                                   accept="application/pdf"
                                                                   name="file_kontrak_sewa" id="file_kontrak"
                                                                   onchange="document.getElementById('file_kontrak_text').value = this.value;checkform();">
                                                            <input type="text" class="form-control form-white"
                                                                   id="file_kontrak_text"
                                                                   placeholder="no file selected" readonly="">
                                                            <small class="text-muted block"><i
                                                                        class="icon-paper-clip"></i>
                                                                Max
                                                                file size: 1Mb(pdf)
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input name="no_spjbtl" type="text" class="form-control form-white"
                                                           onkeyup="checkform()" value="{{@$pemilik->no_spjbtl}}"
                                                           placeholder="Nomor SPJBTL"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text"
                                                           name="tgl_spjbtl" id="tgl_spjbtl"
                                                           value="{{(@$pemilik == null) ? "":  date('d-m-Y',strtotime(@$pemilik->tgl_spjbtl))}}"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="Tanggal SPJBTL">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku SPJBTL</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text"
                                                           name="masa_berlaku_spjbtl" id="masa_berlaku_spjbtl"
                                                           value="{{(@$pemilik == null) ? "":  date('d-m-Y',strtotime(@$pemilik->masa_berlaku_spjbtl))}}"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="Masa Berlaku SPJBTL">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File SPJBTL</label>
                                                </div>
                                                @if(@$pemilik->file_spjbtl != null)
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.@$pemilik->file_spjbtl)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                @endif
                                                <div class="{{(@$pemilik->file_spjbtl == null) ? "col-sm-8": "col-sm-6"}}">
                                                    <div class="file">
                                                        <div class="option-group">
                                                            <span class="file-button btn-primary">Choose File</span>
                                                            <input type="file" class="custom-file max-file"
                                                                   accept="application/pdf"
                                                                   name="file_spjbtl" id="file_spjbtl"
                                                                   onchange="document.getElementById('file_spjbtl_text').value = this.value;checkform();">
                                                            <input type="text" class="form-control form-white"
                                                                   id="file_spjbtl_text"
                                                                   placeholder="no file selected" readonly="">
                                                            <small class="text-muted block"><i
                                                                        class="icon-paper-clip"></i>
                                                                Max
                                                                file size: 1Mb(pdf)
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Supervisor Penanggung Jawab *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select name="user_spv" class="form-control form-white" data-style="white" data-search="true"
                                                required onchange="checkform();">
                                            <option value="">- Pilih Supervisor Penanggungjawab -</option>
                                            @foreach($user_spv as $item)
                                                <option value="{{$item->id}}" {{(@$spv->user_id == $item->id) ? "selected" : ""}} >{{$item->nama_user}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="pull-right p-b-10">
                            <a href="{{url('/internal/users')}}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" id="submit_form" value="submit"
                                    class="btn btn-success btn-square btn-embossed">Submit &nbsp;<i
                                        class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <!-- Select Inputs -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- >Bootstrap Date Picker -->
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
            <script type="text/javascript">
                $(function () {
                    $('.tab-pane input, .tab-pane textarea').on('invalid', function () {

                        // Find the tab-pane that this element is inside, and get the id
                        var $closest = $(this).closest('.tab-pane');
                        var id = $closest.attr('id');

                        // Find the link that corresponds to the pane and have it show
                        $('.nav a[href="#' + id + '"]').tab('show');

                    });
                });
                $("#city").chained("#province");
                $("#id_business_area").chained("#id_company_code");
                $('#preview_img').hide();

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#preview_img').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                        $('#preview_img').show();
                    }
                }

                $("#file_foto_user").change(function () {
                    $('#preview_img').hide();
                    readURL(this);
                });
                /*===06092016 ENABLE SUBMIT WHEN ALL INPUTS COMPLETED====*/
                function checkform() {
                    var f = document.forms["form_register"].elements;
                    var cansubmit = true;

                    for (var i = 0; i < f.length; i++) {
                        if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                            cansubmit = false;
                        }
                    }
                    document.getElementById('submit_form').disabled = !cansubmit;
                }
                window.onload = checkform;

                /*=====ON SUBMIT CHECK EMAIL ALREADY EXIST OR NOT=====*/
                $(document).ready(function () {
                    $("form").submit(function () {
                        //get email user
                        var email_user = $("#email_user").val();
                        var valid;
                        //check email already exist or not
                        $.ajax({
                            url: "{{(url('/check_email'))}}" + "/" + email_user,
                            async: false,
                            success: function (data) {
                                if (data == "invalid") {
                                    valid = data;
                                }
                            }
                        });
                        if (valid == "invalid") {
                            alert("Email sudah digunakan oleh user lain, mohon menggunakan email lain.");
                            return false;
                        }
                    });
                });
            </script>
@endsection
