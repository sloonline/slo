@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Approval <strong>Regitrasi Peminta Jasa</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Approval</a></li>
                    <li class="active">Registrasi Peminta Jasa</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">List Approval Registrasi Permintaan Jasa.</p>
                <div class="panel">
                    <div class="panel-header panel-controls">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tanggal</th>
                                <th>Nama Perusahaan</th>
                                <th>Nama Pemohon</th>
                                <th>Email Pemohon</th>
                                <th>Status Approval</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach($user as $data)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$data->created_at}}</td>
                                    <td>{{$data->nama_perusahaan}}</td>
                                    <td>{{$data->nama_user}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>
                                        @if(($data->status)==="1")
                                            <label class="label label-success">Approved</label>
                                        @elseif(($data->status)==="2")
                                            <label class="label label-danger">Reject</label>
                                        @else <label class="label label-warning">Pending</label>
                                    @endif
                                    <td><a href="{{url('internal/det_reg_peminta/'.$data->id)}}"
                                           class="btn btn-success ladda-button" data-style="expand-right"><i
                                                    class="icons-office-51"></i> Detail</a></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection