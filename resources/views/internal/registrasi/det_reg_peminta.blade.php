@extends('../layout/layout_internal')

@section('page_css')

@endsection

@section('content')
    <?php
    $getSpvUser = \App\User::getSpv($user);
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Approval Registrasi <strong>Peminta Jasa</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Approval</a></li>
                    <li><a href="#">Registrasi Peminta Jasa</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">Detail Regitrasi Peminta Jasa.</p>
                <div class="panel">
                    {!! Form::open(array('url'=> 'internal/det_reg_peminta/'.@$user->id, 'files'=> true, 'class'=> 'form-horizontal')) !!}

                    <div class="panel-header bg-primary">
                        <h3><i class="icon-bulb"></i> Form <strong>Approval</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Nomor Registrasi Peminta Jasa</label>
                            </div>
                            <div class="col-sm-9 prepend-icon">

                                <input type="text" class="form-control form-white"
                                       value="{{ "REG".@$user->created_at->format('Ymd').'-'.@$user->id }}" readonly="">
                                <input type="hidden" class="form-control form-white" value="{{ @$user->id }}" id="id"
                                       name="id">
                                <i class="icon-note"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Tanggal Registrasi</label>
                            </div>
                            <div class="col-sm-9 prepend-icon">
                                <input type="text" class="form-control form-white" value="{{@$user->created_at}}"
                                       readonly="">
                                <i class="icon-calendar"></i>
                            </div>
                        </div>
                        <fieldset class="cart-summary">
                            <legend>Data User</legend>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nama User</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white" value="{{@$user->nama_user}}"
                                           readonly="">
                                    <i class="icon-user"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Email User</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white" value="{{@$user->email}}"
                                           readonly="">
                                    <i class="icon-envelope"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Alamat</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <textarea class="form-control form-white col-sm-12" row="2"
                                              readonly="">{{@$user->alamat_user}}</textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">KTP</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white" value="{{@$user->no_ktp_user}}"
                                           readonly="">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File KTP</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->file_ktp_user!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->file_ktp_user}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File NPWP</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->file_npwp_user!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->file_npwp_user}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="cart-summary">
                            <legend>Data Perusahaan</legend>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->nama_perusahaan}}" readonly="">
                                    <i class="fa fa-building"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nama Pemimpin Perusahaan</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->nama_pemimpin_perusahaan}}" readonly="">
                                    <i class="fa fa-user-secret"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">NPWP</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->no_npwp_perusahaan}}" readonly="">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor Telepon</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->no_telepon_perusahaan}}" readonly="">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor Fax</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->no_fax_perusahaan}}" readonly="">
                                    <i class="fa fa-fax"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Email</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$user->perusahaan->email_perusahaan}}" readonly="">
                                    <i class="fa fa-envelope"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File NPWP</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->perusahaan->file_npwp_perusahaan!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->perusahaan->file_npwp_perusahaan}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Surat Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->perusahaan->file_siup_perusahaan!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->perusahaan->file_siup_perusahaan}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Surat Keterangan Domisili
                                        Perusahaan</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->perusahaan->file_skdomisili_perusahaan!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->perusahaan->file_skdomisili_perusahaan}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Surat Surat Ijin Tempat Usaha</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->perusahaan->file_situ_perusahaan!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->perusahaan->file_situ_perusahaan}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Tanda Daftar Perusahaan</label>
                                </div>
                                <div class="col-sm-9 prepend-icon">
                                    @if (@$user->perusahaan->file_tdp_perusahaan!=null)
                                        <a href="{{url('/upload/')}}/{{@$user->perusahaan->file_tdp_perusahaan}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="cart-summary">
                            <legend>Data Ijin Usaha</legend>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Jenis Ijin</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$pemilik->jenisIjinUsaha->nama_reference}}" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$pemilik->penerbit_ijin_usaha}}" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$pemilik->no_ijin_usaha}}" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Masa Berlaku Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{\Carbon\Carbon::parse(@$pemilik->masa_berlaku_iu)->format('d M Y')}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Surat ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    @if (@$pemilik->file_siup !=null)
                                        <a href="{{url('/upload/')}}/{{@$pemilik->file_siup}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nama Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{@$pemilik->nama_kontrak}}" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" value="{{@$pemilik->no_kontrak}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Tanggal Pengesahaan Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{(@$pemilik->tgl_pengesahan_kontrak == null) ? "-" : \Carbon\Carbon::parse(@$pemilik->tgl_pengesahan_kontrak)->format('d M Y')}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Masa Berlaku Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{(@$pemilik->masa_berlaku_kontrak == null) ? "-" : \Carbon\Carbon::parse(@$pemilik->masa_berlaku_kontrak)->format('d M Y')}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                </div>
                                <div class="col-sm-9">
                                    @if (@$pemilik->file_kontrak !=null)
                                        <a href="{{url('/upload/')}}/{{@$pemilik->file_kontrak}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" value="{{@$pemilik->no_spjbtl}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{(@$pemilik->tgl_spjbtl == null) ? "-" : \Carbon\Carbon::parse(@$pemilik->tgl_spjbtl)->format('d M Y')}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Masa Berlaku SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{(@$pemilik->masa_berlaku_spjbtl == null) ? "-" : \Carbon\Carbon::parse(@$pemilik->masa_berlaku_spjbtl)->format('d M Y')}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">File SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    @if (@$pemilik->file_spjbtl !=null)
                                        <a href="{{url('/upload/')}}/{{@$pemilik->file_spjbtl}}"
                                           class="btn btn-sm btn-info col-sm-2 text-center"> Detail</a>
                                    @else
                                        Tidak Ada File
                                    @endif
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="cart-summary">
                            <legend>Pengaturan User</legend>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Supervisor Penanggung Jawab</label>
                                </div>
                                <div class="col-sm-9">
                                    @if(@$user->status_user == VERIFIED)
                                        <input type="text" class="form-control form-white"
                                               value="{{\App\User::getSpv(@$user)->nama_user}}" readonly="">
                                    @else
                                        <select name="user_spv" class="form-control form-white" data-style="white" data-search="true"
                                                required>
                                            <option>-- Pilih Supervisor Penanggungjawab --</option>
                                            @foreach(@$user_spv as $spv)
                                                <option value="{{$spv->id}}" {{(@$getSpvUser->id == $spv->id) ? "selected" : ""}}>{{$spv->nama_user}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Kategori Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                    @if(@$user->status_user == VERIFIED)
                                        <input type="text" class="form-control form-white"
                                               value="{{@$user->perusahaan->kategori->nama_kategori}}" readonly="">
                                    @else
                                        <select class="form-control form-white" name="kategori"
                                                onchange="checkform();"
                                                id="kategori" required>
                                            <option value="">--- Pilih Kategori Perusahaan ---</option>
                                            @foreach($kategori as $item)
                                                <option value="{{$item->id}}" {{(@$user->perusahaan->kategori_perusahaan == $item->id)  ? "selected='selected'" : ""}}>{{$item->nama_kategori}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Status</label>
                                </div>
                                <div class="col-sm-9">
                                    <input class="form-control bg-aero" type="text" value="{{@$user->status_user}}">
                                </div>
                            </div>
                            @if(@$user->status_user == "PENDING")
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan Pending</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control bg-aero" type="text"
                                               value="{{@$user->keterangan_pending}}">
                                    </div>
                                </div>
                            @endif
                        </fieldset>
                        {{--<div class="form-group">--}}
                        {{--<div class="col-sm-3">--}}
                        {{--<label class="col-sm-12 control-label">Level Akses</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-9 prepend-icon">--}}
                        {{--<select name="jenis_transmisi" class="form-control form-white" data-style="white">--}}
                        {{--<option value="1">Satu</option>--}}
                        {{--<option value="2">Dua</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                        {{--<div class="col-sm-3">--}}
                        {{--<label class="col-sm-12 control-label">Modul Akses</label>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-9 prepend-icon">--}}
                        {{--<select name="jenis_transmisi" class="form-control form-white" data-style="white">--}}
                        {{--<option value="1">Satu</option>--}}
                        {{--<option value="2">Dua</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_VERIFICATE_USER))
                                    @if(@$user->status_user != VERIFIED)
                                        <button type="submit" id="approve" class="btn btn-success ladda-button"
                                                data-style="zoom-in"><i class="glyphicon glyphicon-floppy-saved"></i>
                                            Approve
                                        </button>
                                    @endif
                                    @if(@$user->status_user == CREATED)
                                        <a class="btn btn-danger" id="reject" data-toggle="modal"
                                           data-target="#confirmDelete"><i class="glyphicon glyphicon-ban-circle"></i>
                                            Pending</a>
                                    @endif
                                @endif
                                <a href="{{url('internal/users')}}" class="btn btn-warning">Kembali</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @endsection


            @section('page_script')

                <script>
                    $(document.getElementById('reject')).click(function () {
                        return confirm("Apakah Anda yakin akan menolak Data User ini?");
                    });
                    $(document.getElementById('approve')).click(function () {
                        return confirm("Apakah Anda yakin akan menyetujui Data User ini?");
                    });

                </script>
                <div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="confirmDelete">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: teal;color:white;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                            class="icons-office-52"></i></button>
                                <h4 class="modal-title"><strong>Keterangan Pending User</strong> user</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        {!! Form::open(array('url'=> 'internal/pending_user/', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                                        <input type="hidden" name="id" value="{{@$user->id}}"/>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Keterangan</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                          <textarea rows="5" class="form-control form-white" name="keterangan_pending" required
                                    placeholder="
                          Alasan pending user...">{{@ @$user->keterangan_pending}}</textarea>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <button onclick="return confirm('Apakah anda yakin melakukan pending untuk user ini?')"
                                                    type="submit"
                                                    class="btn btn-success ladda-button btn-square btn-embossed"
                                                    data-style="zoom-in">Pending &nbsp;<i
                                                        class="glyphicon glyphicon-floppy-saved"></i>
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
