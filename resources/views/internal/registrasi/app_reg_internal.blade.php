@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
  <div class="header">
	<h2>Approval <strong>Regitrasi Peminta Jasa</strong></h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
		<li><a href="{{url('/internal')}}">Dashboard</a></li>
		<li><a href="#">Approval</a></li>
		<li class="active">Internal Pusertif</li>
	  </ol>
	</div>
  </div>
  <div class="row">
	<div class="col-lg-12 portlets">
	  <p class="m-t-10 m-b-20 f-16">Silahkan inputkan data pemohon dan data trasnmisi yang hendak diajukan dalam permohonan ini.</p>
	  <div class="panel">
		<div class="panel-header panel-controls bg-primary">
		  <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
		</div>
		<div class="panel-content pagination2 table-responsive">
		  <table class="table table-hover table-dynamic">
			<thead>
			  <tr>
				<th>Rendering engine</th>
				<th>Browser</th>
				<th>Platform(s)</th>
				<th>Engine version</th>
				<th>CSS grade</th>
				<th>Aksi</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td>Trident</td>
				<td>Internet Explorer 4.0</td>
				<td>Win 95+</td>
				<td>4</td>
				<td>X</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Trident</td>
				<td>Internet Explorer 5.0</td>
				<td>Win 95+</td>
				<td>5</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Trident</td>
				<td>Internet Explorer 5.5</td>
				<td>Win 95+</td>
				<td>5.5</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Trident</td>
				<td>Internet Explorer 6</td>
				<td>Win 98+</td>
				<td>6</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Trident</td>
				<td>Internet Explorer 7</td>
				<td>Win XP SP2+</td>
				<td>7</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Trident</td>
				<td>AOL browser (AOL desktop)</td>
				<td>Win XP</td>
				<td>6</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Firefox 1.0</td>
				<td>Win 98+ / OSX.2+</td>
				<td>1.7</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Firefox 1.5</td>
				<td>Win 98+ / OSX.2+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Firefox 2.0</td>
				<td>Win 98+ / OSX.2+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Firefox 3.0</td>
				<td>Win 2k+ / OSX.3+</td>
				<td>1.9</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Camino 1.0</td>
				<td>OSX.2+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Camino 1.5</td>
				<td>OSX.3+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Netscape 7.2</td>
				<td>Win 95+ / Mac OS 8.6-9.2</td>
				<td>1.7</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Netscape Browser 8</td>
				<td>Win 98SE+</td>
				<td>1.7</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Netscape Navigator 9</td>
				<td>Win 98+ / OSX.2+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.0</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.1</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.1</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.2</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.2</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.3</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.3</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.4</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.4</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.5</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.5</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.6</td>
				<td>Win 95+ / OSX.1+</td>
				<td>1.6</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.7</td>
				<td>Win 98+ / OSX.1+</td>
				<td>1.7</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Mozilla 1.8</td>
				<td>Win 98+ / OSX.1+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Gecko</td>
				<td>Seamonkey 1.1</td>
				<td>Win 98+ / OSX.2+</td>
				<td>1.8</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr class="gradeC">
				<td>Tasman</td>
				<td>Internet Explorer 5.1</td>
				<td>Mac OS 7.6-9</td>
				<td>1</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr class="gradeC">
				<td>Tasman</td>
				<td>Internet Explorer 5.2</td>
				<td>Mac OS 8-X</td>
				<td>1</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>NetFront 3.1</td>
				<td>Embedded devices</td>
				<td>-</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>NetFront 3.4</td>
				<td>Embedded devices</td>
				<td>-</td>
				<td>A</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>Dillo 0.8</td>
				<td>Embedded devices</td>
				<td>-</td>
				<td>X</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>Links</td>
				<td>Text only</td>
				<td>-</td>
				<td>X</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>Lynx</td>
				<td>Text only</td>
				<td>-</td>
				<td>X</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>IE Mobile</td>
				<td>Windows Mobile 6</td>
				<td>-</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Misc</td>
				<td>PSP browser</td>
				<td>PSP</td>
				<td>-</td>
				<td>C</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			  <tr>
				<td>Other browsers</td>
				<td>All others</td>
				<td>-</td>
				<td>-</td>
				<td>U</td>
				<td><a href="{{url('internal/det_reg_internal')}}" class="btn btn-success ladda-button" data-style="expand-right"><i class="icons-office-51"></i> Detail</button></td>
			  </tr>
			</tbody>
		  </table>
		</div>
	  </div>
	</div>
  </div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection