@extends('../layout/layout_internal')

@section('page_css')

@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Detail <strong>User</strong></h2>
            <div class="breadcrumb-wrapper">
               <ol class="breadcrumb">
                   <li><a href="{{url('/internal')}}">Dashboard</a></li>
                   <li><a href="{{url('/internal/users')}}">Manajemen User</a></li>
                   <li class="active">Detail User</li>
               </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#user" data-toggle="tab"><i class="icon-user"></i> User</a></li>
                            @if($user->perusahaan == null)
                            <li><a href="#pusertif" data-toggle="tab"><i class="fa fa-building-o"></i> Pusertif</a></li>
                            <li><a href="#peminta_jasa" data-toggle="tab"><i class="fa icon-users"></i> Peminta Jasa</a></li>
                            @else
                            <li><a href="#perusahaan" data-toggle="tab"><i class="fa fa-building-o"></i> Perusahaan</a></li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="user">
                                <div class="row column-seperation">
                                    <div class="col-md-2 line-separator">
                                    <?php $loc = storage_path() . '/upload/foto_user/' . $user->file_foto_user; ?>
                                    {{--@if (file_exists($loc))--}}
                                    {{--<img id="preview_img" class="img-thumbnail" src="{{url('upload/'.$user->file_foto_user)}}"--}}
                                         {{--alt="foto" width="200"/>--}}
                                    {{--@else--}}
                                    <img id="preview_img" class="img-thumbnail" src="{{url('assets/global/images/avatars/avatar12_big.png')}}"
                                         alt="foto" width="200"/>
                                    {{--@endif--}}
                                    <hr>
                                    {{--<a href="{{url('/')}}" class="btn btn-primary btn-square btn-embossed btn-block"><i class="icon-camera pull-left"></i> Ganti Foto</a>--}}
                                    {{--<a href="{{url('/')}}" class="btn btn-success btn-square btn-embossed btn-block"><i class="icon-lock pull-left"></i> Ganti Password</a>--}}
                                    </div>
                                    <div class="col-md-10">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Lengkap</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->nama_user}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Username</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->username}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Email</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->email}}">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tempat, Tanggal Lahir</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->tempat_lahir_user.", ".$user->tanggal_lahir}}">
                                                        <i class="fa fa-legal"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jenis Kelamin</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->gender_user}}">
                                                        <i class="fa fa-male"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">No Handphone</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->no_hp_user}}">
                                                        <i class="fa fa-mobile"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <textarea rows="3" class="form-control form-white" readonly="">{{$user->alamat_user}}</textarea>
                                                        <i class="icon-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($user->perusahaan != null)
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">KTP</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->file_ktp_user)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                                <div class="col-sm-7">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->no_ktp_user}}">
                                                        <i class="fa fa-credit-card"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NPWP</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="#" target="_blank"
                                                       onclick="window.open('{{url('upload/'.$user->file_npwp_user)}}')"
                                                       class="btn btn-info btn-square btn-embossed"><i
                                                                class="glyphicon glyphicon-search"></i>View File</a>
                                                </div>
                                            </div>
                                            @else
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIP / User ID</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->nip_user}}">
                                                        <i class="icon-tag"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Grade</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->grade_user}}">
                                                        <i class="icon-briefcase"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jabatan</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->jabatan_user}}">
                                                        <i class="icon-briefcase"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pendidikan Terakhir</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->pendidikan_user}}">
                                                        <i class="icon-graduation"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Status Pernikahan</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->status_pernikahan}}">
                                                        <i class="icon-heart"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </form>
                                    </div>
                              </div>
                            </div>
                            @if($user->perusahaan != null)
                            <div class="tab-pane fade" id="perusahaan">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kategori Perusahaan</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        {{--<input type="text" class="form-control form-white" readonly=""--}}
                                                               {{--@if($user->perusahaan == null || $user->perusahaan->kategori_perusahaan == 1 )--}}
                                                               {{--value="PLN"--}}
                                                               {{--@else--}}
                                                               {{--value="{{$kategori->nama_kategori}}"--}}
                                                               {{--@endif--}}
                                                        {{-->--}}
                                                        <input type="text" class="form-control form-white" readonly="" value="{{$kategori}}">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white"
                                                               name="nama_pemimpin_perusahaan" readonly=""
                                                               @if($user->perusahaan == null || $user->perusahaan->kategori_perusahaan == 1)
                                                               {{--value="PLN"--}}
                                                               value="{{$user->perusahaan->nama_perusahaan}}"
                                                               @else
                                                               value="{{$user->perusahaan->nama_perusahaan}}"
                                                               @endif
                                                        >
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($user->perusahaan->kategori_perusahaan == 1)
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Unit</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->perusahaan->company_code->company_code." - ".$user->perusahaan->company_code->description}}">
                                                        <i class="icon-graduation"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Area</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->perusahaan->business_area->business_area." - ".$user->perusahaan->business_area->description}}">
                                                        <i class="icon-graduation"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Pemimpin {{$user->perusahaan->kategori_perusahaan == 1? "Unit" : "Perusahaan"}}</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->perusahaan->nama_pemimpin_perusahaan}}">
                                                        <i class="fa fa-user-secret"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Telepon Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->no_telepon_perusahaan}}">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Fax Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->no_fax_perusahaan}}">
                                                            <i class="fa fa-fax"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->email_perusahaan}}">
                                                            <i class="fa fa-envelope"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <textarea rows="3" class="form-control form-white" readonly="">{{$user->perusahaan->alamat_perusahaan}}</textarea>
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->kode_pos_perusahaan}}">
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->city->city}}">
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->province->province}}">
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">NPWP Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_npwp_perusahaan)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white" readonly=""
                                                                   value="{{$user->perusahaan->no_npwp_perusahaan}}">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_siup_perusahaan)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Keterangan Domisili Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_skdomisili_perusahaan)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Ijin Tempat Usaha</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_situ_perusahaan)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanda Daftar Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_tdp_perusahaan)}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="tab-pane fade" id="pusertif">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Status Pekerja</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->status_pekerja}}">
                                                        <i class="fa fa-wrench"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Bidang</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->bidang->nama_bidang}}">
                                                        <i class="fa fa-street-view"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Sub Bidang</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->sub_bidang->nama_sub_bidang}}">
                                                        <i class="fa fa-street-view"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Level Akses</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="icheck-list">
                                                        @foreach($roles as $row)
                                                            <input type="checkbox" data-checkbox="icheckbox_square-blue"
                                                                   value="{{$row->id}}" disabled=""
                                                            <?php
                                                                    $stat = 0;
                                                                    $i = 0;
                                                                    while ($stat == 0 && $i < sizeof($user_roles)) {
                                                                        if ($user_roles[$i]->role_id == $row->id) {
                                                                            $stat = 1;
                                                                            echo "checked";
                                                                        } else {
                                                                            $i++;
                                                                        }
                                                                    }
                                                                    ?>
                                                            />
                                                            {{$row->display_name}}<br/><br/>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="peminta_jasa">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><strong>PLN</strong></h2>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                {{--<th>ID</th>--}}
                                                <th>Unit</th>
                                                <th>Nama PIC</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $x=1;
                                            ?>
                                            @foreach($user->businessAreaUser as $wa)
                                                <tr>
                                                    <td>{{$x++}}</td>
{{--                                                    <td>{{@$wa->id}}</td>--}}
                                                    <td>{{@$wa->perusahaan->nama_perusahaan}}</td>
                                                    <td>{{@$wa->perusahaan->user->nama_user}}</td>
                                                    <td>{{@$wa->perusahaan->user->username}}</td>
                                                    <td>{{@$wa->perusahaan->user->email}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2><strong>Swasta</strong></h2>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Perusahaan</th>
                                                <th>Nama PIC</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $x=1;
                                            ?>
                                            @foreach($user->perusahaanSpv as $wa)
                                            <tr>
                                                <td>{{$x++}}</td>
                                                <td>{{@$wa->perusahaan->nama_perusahaan}}</td>
                                                <td>{{@$wa->perusahaan->user->nama_user}}</td>
                                                <td>{{@$wa->perusahaan->user->username}}</td>
                                                <td>{{@$wa->perusahaan->user->email}}</td>
                                            </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <hr>
                        <div class="pull-right p-b-10">
                            <a href="{{ url('internal/users') }}" class="btn btn-warning btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-ban"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('page_script')

@endsection