@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Manajemen <strong>User Internal</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Approval</a></li>
                    <li class="active">Internal Pusertif</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <p class="m-t-10 m-b-20 f-16"></p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Data <strong>User</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
						<div class="m-b-20 border-bottom">
							<div class="btn-group">
								<a href="{{url('internal/register')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah User</a>
							</div>
						</div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Bidang & Jabatan</th>
                                <th>Level Akses</th>
                                <th style="width: 170px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 1;
                                ?>
                                @foreach($users as $row)
                                    <tr>
                                        <td>{{$no}}</td>

                                        <td>{{$row->nip_user}}</td>
                                        <td>{{$row->nama_user}}</td>
                                        <td>{{$row->username}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>{{((isset($row->sub_bidang->nama_sub_bidang)) ? $row->sub_bidang->nama_sub_bidang : "")." ".$row->bidang->nama_bidang . " (".$row->jabatan_user.")"}}</td>
                                        <td>
                                            @foreach($row->user_roles as $role)
                                                {{$role->roles->display_name}}<br/>
                                            @endforeach
                                        </td>
                                        <td>
											<a class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Edit" href="{{url('internal/register/'.$row->id)}}"><i class="fa fa-pencil-square-o"></i></a>
                                            <a class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus user ini?')" href="{{url('internal/delete_internal/'.$row->id)}}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection