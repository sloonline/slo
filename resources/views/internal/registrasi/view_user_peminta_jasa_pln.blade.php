@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>User <strong>Peminta Jasa</strong> PLN</h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">User Peminta Jasa PLN</a></li>
					<li class="active">View User</li>
				</ol>
			</div>
		</div>
	    <div class="row">
			<div class="col-lg-12 portlets">
				<p class="m-t-10 m-b-20 f-16">View User Peminta Jasa PLN.</p>
				<div class="panel">
					<div class="panel-header panel-controls bg-primary">
					  <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
					</div>
					<div class="panel-content pagination2 table-responsive">
						<div class="m-b-20 border-bottom">
							<div class="btn-group">
								<a href="{{url('internal/create_user_peminta_jasa_pln')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah User</a>
							</div>
						</div>
						<table class="table table-hover table-dynamic">
							<thead>
							  <tr>
								<th>No</th>
								<th>Nama Perusahaan</th>
								<th>Unit</th>
								<th>Nama Pemimpin</th>
								<th>Nama User</th>
								<th>Email User</th>
								<th>Password</th>
								<th>Aksi</th>
							  </tr>
							</thead>
							<tbody>
							  <?php $no=1; ?>
							  @foreach($peminta_jasa_pln as $data)
							  <tr>
								<td>{{$no}}</td>
								<td>{{$data->nama_perusahaan}}</td>
								<td>{{ 'Hayo Unit Mana' }}</td>
								<td>{{$data->nama_pemimpin_perusahaan}}</td>
								<td>{{$data->nama_user}}</td>
								<td>{{$data->email}}</td>
								<td>{{ '******' }}</td>
								<td>
									<a href="{{url('internal/detail_user_peminta_jasa_pln/'.$data->id)}}" class="btn btn-sm btn-primary btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye"></i></a>
									<a href="{{url('internal/edit_user_peminta_jasa_pln/'.$data->id)}}" class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></a>
									<a href="{{url('internal/delete_user_peminta_jasa_pln/'.$data->id)}}" class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus user ini?')"><i class="fa fa-trash"></i></a>
								</td>
							  </tr>
							  <?php $no++; ?>
							  @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection