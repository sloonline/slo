@extends('../layout/layout_internal')

@section('page_css')

@endsection

@section('content')
<div class="page-content">
          <div class="header">
            <h2>Approval Registrasi <strong>Internal Pusertif</strong></h2>
            <div class="breadcrumb-wrapper">
              <ol class="breadcrumb">
                <li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="#">Approval</a></li>
                <li><a href="#">Registrasi Internal Pusertif</a></li>
                <li class="active">Detail</li>
              </ol>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 portlets">
              <p class="m-t-10 m-b-20 f-16">Silahkan inputkan data pemohon dan data trasnmisi yang hendak diajukan dalam permohonan ini.</p>
              <div class="panel">
                {!! Form::open(array('url'=> 'transmisi', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                <input type="hidden" name="_token" id="_token" value="{!! $csrf_token !!}}" />
                <div class="panel-header bg-primary">
                    <h3><i class="icon-bulb"></i> Form <strong>Approval</strong></h3>
                </div>
                <div class="panel-content">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nomor Registrasi User</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-note"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Tanggal Registrasi</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-calendar"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nama User</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-user"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Email User</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-envelope"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Password</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-lock"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Status Approval</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<select name="jenis_transmisi" class="form-control form-white" data-style="white">
								<option value="1">Satu</option>
								<option value="2">Dua</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Bidang</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-user"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Sub Bidang</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="Value BOSS!" readonly="">
							<i class="icon-envelope"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kosentrasi Penugasan</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<select name="jenis_transmisi" class="form-control form-white" data-style="white">
								<option value="1">Satu</option>
								<option value="2">Dua</option>
							</select>
						</div>
					</div>
				</div>
				<hr />
                <div class="panel-footer clearfix bg-white">
                  <div class="pull-right">
                    <a href="{{url('internal/app_reg_internal')}}" class="btn btn-warning"><i class="icon-ban"></i> Cancel</a>
                    <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan</button>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
@endsection


@section('page_script')

@endsection