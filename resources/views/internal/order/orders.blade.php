@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
<?php
$summary = \App\Order::calculateSummary($order);
?>
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data Order <strong>Perusahaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Order</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="col-lg-12 clearfix">
                            <div class="row">
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-success btn-transparent btn-square"><i
                                                class="glyphicon glyphicon-exclamation-sign"></i><b>{{@$summary[SUBMITTED]}}
                                            ORDER CREATED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-primary btn-transparent btn-square"><i
                                                class="fa fa-thumbs-up"></i><b>{{@$summary[APPROVED]}} ORDER
                                            APPROVED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-danger btn-transparent btn-square"><i
                                                class="fa fa-thumbs-down"></i><b>{{@$summary[REJECTED]}} ORDER
                                            REJECTED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-warning btn-transparent btn-square"><i
                                                class="fa fa-pencil"></i><b>{{@$summary[REVISED]}} ORDER REVISED</b>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br/><br/>
                        <hr/>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Order</th>
                                <th>Instalasi</th>
                                <th>Nomor Surat</th>
                                <th>Pemilik</th>
                                <th>Status</th>
                                <th>Perusahaan</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($order) && (is_array($order) || is_object($order)))
                                <?php $no = 1; ?>
                                @foreach($order as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td style="white-space: nowrap;">{{ @$item->nomor_order }}</td>
                                        <td>
                                            <ul style="list-style-type: none;">
                                                @foreach(@$item->nama_instalasi as $row)
                                                    <li>{{$row}}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ @$item->nomor_sp }}</td>
                                        <td>{{ @$item->user->nama_user }}</td>
                                        <td>{{ @$item->flow_status->status }}</td>
                                        <td>{{ @$item->perusahaan->nama_perusahaan }}</td>
                                        <td width="15%">
                                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW_PERMOHONAN))
                                                <a href="{{url('/internal/detail_order/'.@$item->id)}}"
                                                   class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                            class="fa fa-eye"></i></a>
                                            @endif
                                            @if((@$item->flow_status->modul == MODUL_ORDER && @$item->flow_status->tipe_status==APPROVED) || @$item->flow_status->modul == MODUL_RAB )
                                                @if(@$item->flow_status->tipe_status==APPROVED && @$item->flow_status->modul == MODUL_RAB)
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.@$item->id)}}"
                                                       data-rel="tooltip"
                                                       class="btn btn-sm btn-primary btn-square btn-embossed"
                                                       data-placement="right" title="RAB Approved">RAB</a>
                                                @elseif(@$item->flow_status->tipe_status==SUBMITTED || @$item->flow_status->tipe_status==SENT)
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.@$item->id)}}"
                                                       data-rel="tooltip"
                                                       class="btn btn-sm btn-warning btn-square btn-embossed"
                                                       data-placement="right" title="RAB Submitted">RAB</a>
                                                @elseif(@$item->flow_status->tipe_status==REVISED)
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.@$item->id)}}"
                                                       data-rel="tooltip"
                                                       class="btn btn-sm btn-success btn-square btn-embossed"
                                                       data-placement="right" title="RAB Revised">RAB</a>
                                                @elseif(@$item->flow_status->tipe_status==REJECTED)
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.@$item->id)}}"
                                                       data-rel="tooltip"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       data-placement="right" title="RAB Rejected">RAB</a>
                                                @elseif(@$item->flow_status->tipe_status==APPROVED && @$item->flow_status->modul == MODUL_ORDER )
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.@$item->id)}}"
                                                       data-rel="tooltip"
                                                       class="btn btn-sm btn-success btn-square btn-embossed"
                                                       data-placement="right" title="Create RAB">RAB</a>
                                                @elseif($item->flow_status->tipe_status==CREATED)
                                                    <a href="{{url('/internal/rancangan_biaya_order/'.$item->id)}}"
                                                       data-rel="tooltip" style="background-color: mediumaquamarine"
                                                       class="btn btn-sm btn-info btn-square btn-embossed"
                                                       data-placement="right" title="RAB Created">RAB</a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <fieldset class="cart-summary">
                            <legend>KETERANGAN</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-2" style="border-right: 1px solid #ccc">
                                        <a style="display: inline;background-color: mediumaquamarine"
                                           class="btn btn-sm btn-info btn-square btn-embossed">RAB</a>
                                        <p style="display: inline">RAB Created</p>
                                    </div>
                                    <div class="col-md-2" style="border-right: 1px solid #ccc">
                                        <a style="display: inline"
                                           class="btn btn-sm btn-warning btn-square btn-embossed">RAB</a>
                                        <p style="display: inline">RAB Submitted</p>
                                    </div>
                                    <div class="col-md-2" style="border-right: 1px solid #ccc">
                                        <a style="display: inline"
                                           class="btn btn-sm btn-primary btn-square btn-embossed">RAB</a>
                                        <p style="display: inline">RAB Approved</p>
                                    </div>
                                    <div class="col-md-2" style="border-right: 1px solid #ccc">
                                        <a style="display: inline"
                                           class="btn btn-sm btn-danger btn-square btn-embossed">RAB</a>
                                        <p style="display: inline">RAB Rejected</p>
                                    </div>
                                    <div class="col-md-2">
                                        <a style="display: inline"
                                           class="btn btn-sm btn-success btn-square btn-embossed">RAB</a>
                                        <p style="display: inline">Create RAB</p>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();

                    // Sort immediately with columns 0 and 1
                    oTable.fnSort([[0, 'asc']]);
                });
            </script>
@endsection