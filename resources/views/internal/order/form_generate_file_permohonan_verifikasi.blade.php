<div class="form-group">
    <div class="col-sm-3">
        <h4 class="col-sm-12 control-label"><strong>Pemeriksaan Dokumen</strong></h4>
    </div>
</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>
            <label class="col-sm-12 control-label">Jenis Dokumen</label>
        </th>
        <th class="col-sm-2">
            Kesesuaian
        </th>
        <th class="col-sm-6">
            Dokumen
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($fields as $field)
        <?php $nama_field = $field['name']; ?>
        <tr>
            <td>
                <label class="col-sm-12 control-label">{{$field['label']}}</label>
            </td>
            <td style="text-align: center;">
                @if(! \App\User::isEksternal(Auth::user()))
                    <input class="form-control" {{($field['value'] =="") ? "disabled" : ""}} type="checkbox"
                           {{(isset($files['valid_'.$nama_field]) && $files['valid_'.$nama_field] == true) ? "checked" : ""}} name="valid_{{$nama_field}}"/>
                @else
                    @if(isset($files['valid_'.$nama_field]) && $files['valid_'.$nama_field] == true)
                        <span class="btn btn-sm btn-success"><i class="fa fa-check"></i></span>
                    @else
                        <span class="btn btn-sm btn-warning"><i class="fa fa-remove"></i></span>
                    @endif
                @endif
            </td>
            <td>
                @if($field['value'] != "")
                    <a target="_blank" href="{{url('/upload/'.$field['value'])}}" class="btn btn-primary"><i
                                class="fa fa-download"></i> View </a> {{$field['value']}}
                @else
                    <span style="color: red"><i class="fa fa-info-circle"></i> File tidak ditemukan</span>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>