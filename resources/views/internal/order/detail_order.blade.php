@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Order <strong></strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                    General</a></li>
                            <li><a href="#permohonan" data-toggle={{($order == null) ? "" :"tab"}}><i
                                            class="icon-user"></i> Permohonan</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                {!! Form::open(array('url'=> '/internal/create_order', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                <input type="hidden" name="tipe" value="general"/>
                                <input type="hidden" name="perusahaan" value="{{$perusahaan->id}}"/>
                                <input type="hidden" name="order_id" value="{{($order != null) ? $order->id : ""}}">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="panel border p-b-20">
                                            <div class="panel-header bg-dark">
                                                <h2 class="panel-title">DATA <strong>DETAIL ORDER</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Nomor
                                                            Order</label>
                                                    </div>
                                                    <div class="col-sm-7 prepend-icon">
                                                        <h1>
                                                            <strong>{{($order != null)?$order->nomor_order: "-"}}</strong>
                                                        </h1>
                                                        <input type="hidden" name="nomor_order"
                                                               value="{{($order != null)?$order->nomor_order: ""}}">
                                                    </div>
                                                </div>
                                                <div class="form-group" id="surat_tugas"
                                                     style="display:none">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Surat
                                                            Tugas</label>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <select class="form-control form-white"
                                                                {{($order != null && $order->id_surat_tugas != null) ? "disabled='true'" : ""}}
                                                                data-search="true" name="surat_tugas">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($surat_tugas as $item)
                                                                <option {{($order != null && $order->id_surat_tugas == $item->id) ? "selected='selected'" : ""}} value="{{$item->id}}">{{$item->nomor_surat}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Nomor Surat
                                                            Permintaan</label>
                                                    </div>
                                                    <div class="col-sm-7 prepend-icon">
                                                        <input name="nomor_sp"
                                                               value="{{($order != null) ? $order->nomor_sp : ''}}"
                                                               type="text" class="form-control form-white"
                                                               placeholder="Nomor Surat" required/>
                                                        <i class="icon-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Tanggal Surat
                                                            Permintaan</label>
                                                    </div>
                                                    <div class="col-sm-7 prepend-icon">
                                                        <input name="tanggal_sp"
                                                               value="{{($order != null) ? $order->tanggal_sp : ''}}"
                                                               type="text"
                                                               class="date-picker form-control form-white"
                                                               placeholder="Tanggal" required>
                                                        <i class="icon-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">File Surat
                                                            Permintaan</label>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="file">
                                                            <div class="option-group">
                                                                @if(($order != null))
                                                                    <a target="_blank"
                                                                       href="{{url('upload/'.$order->file_sp)}}"
                                                                       style="color:deepskyblue"><i
                                                                                class="fa fa-paperclip"></i> {{$order->file_sp}}
                                                                    </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel border" style="text-align:center;">
                                            <div class="panel-header bg-dark">
                                                <h2 class="panel-title">DATA <strong>DETAIL ORDER</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-11 prepend-icon">
                                                        <input name="nama_perusahaan" type="text"
                                                               class="form-control form-white"
                                                               placeholder="Nama Perusahaan"
                                                               required readonly
                                                               value="{{$perusahaan->nama_perusahaan}}">
                                                        <i class="icon-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-11 prepend-icon">
                                                        <input name="nama_perusahaan" type="text"
                                                               class="form-control form-white"
                                                               placeholder="Nama Perusahaan"
                                                               required readonly
                                                               value="{{$perusahaan->kategori->nama_kategori}}">
                                                        <i class="icon-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-11 prepend-icon">
                                                        <input name="nama_perusahaan" type="text"
                                                               class="form-control form-white"
                                                               placeholder="Nama Perusahaan"
                                                               required readonly
                                                               value="{{$perusahaan->city->city}}">
                                                        <i class="icon-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-11 prepend-icon">
                                                                        <textarea name="alamat_perusahaan"
                                                                                  class="form-control form-white"
                                                                                  placeholder="Alamat" required
                                                                                  readonly>{{$perusahaan->alamat_perusahaan}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="permohonan">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel border p-b-20">
                                            <div class="panel-header bg-dark">
                                                <h2 class="panel-title">DATA <strong>PERMOHONAN</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white"
                                                 style="overflow-x: scroll;overflow-y:hidden;">
                                                @if($permohonan->count() > 0)
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Foto Instalasi</th>
                                                            <th>Nomor Permohonan</th>
                                                            <th>Tanggal Permohonan</th>
                                                            <th>Nama Instalasi</th>
                                                            <th>Jenis Instalasi</th>
                                                            <th>Jenis Pekerjaan</th>
                                                            <th>Jenis Lingkup</th>
                                                            <th style="text-align:center">Aksi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(is_array($permohonan) || is_object($permohonan))
                                                            <?php $no = 1; ?>
                                                            @foreach($permohonan as $item)
                                                                <tr>
                                                                    <td>{{ $no }}</td>
                                                                    <td>
                                                                        <img style="margin: 0 auto;"
                                                                             src="{{(@$item->instalasi->foto_1 != null) ?  url('upload/instalasi/'.$item->instalasi->foto_1) : url('/assets/global/images/picture.png')}}"
                                                                             class="img-thumbnail" width="100"></td>
                                                                    <td>{{ @$item->nomor_permohonan}}</td>
                                                                    <td>{{ date('d M Y',strtotime( $item->tanggal_permohonan ))}}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->instalasi->nama_instalasi}}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->tipeInstalasi->nama_instalasi }}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->produk->produk_layanan }}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->lingkup_pekerjaan->jenis_lingkup_pekerjaan }}
                                                                        @if(@$item->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                                            <br/>
                                                                            @foreach($item->bayPermohonan as $bg)
                                                                                - {{@$bg->bayGardu->nama_bay}}<br/>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                    <td style="text-align:center">
                                                                        <a href="{{url('/internal/create_permohonan/'.$item->id_orders.'/'.$item->id)}}"
                                                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                    class="fa fa-eye"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <?php $no++; ?>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                @endif
                                                @if($permohonan_distribusi->count() > 0)
                                                    <h2>PERMOHONAN INSTALASI DISTRIBUSI</h2>
                                                    <hr/>
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nomor Surat</th>
                                                            <th>Tanggal Surat</th>
                                                            <th>Periode</th>
                                                            <th>Jenis Pekerjaan</th>
                                                            <th style="width: 50px;">Aksi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach($permohonan_distribusi as $wa)
                                                            <tr>
                                                                <td>
                                                                    {{$no}}
                                                                </td>
                                                                <td>
                                                                    {{$wa->nomor_surat}}
                                                                </td>
                                                                <td>
                                                                    {{date('d F Y',strtotime($wa->tanggal_surat))}}
                                                                </td>
                                                                <td>
                                                                    {{$wa->periode}}
                                                                </td>
                                                                <td>
                                                                    <?php $layanan = $wa->produk ?>
                                                                    @foreach(@$layanan as $row)
                                                                        <p>{{@$row->produk->produk_layanan}}</p>
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    <a href="{{url('/internal/view_surat_tugas/'.$wa->id)}}"
                                                                       class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--</div>--}}
                        </div>
                        @if(@$workflow['history'][0]->modul == MODUL_ORDER)
                            @include('workflow_view')
                        @endif
                    </div>
                </div>
            </div>
            @endsection
            @section('page_script')
                <script type="text/javascript">
                    var tab = "general";
                    @if(Session::get('tab') != null)
                            tab = "order";
                    @endif
                    suratTugas();
                    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                    $('#tipe_instalasi').change(function () {
                        suratTugas();
                    });
                    function suratTugas() {
                        if ($('#tipe_instalasi').val() == 3) { // sementara manual, 3 merupakan id tipe instalasi distribusi
                            $('#surat_tugas').show();
                        } else {
                            $('#surat_tugas').hide();
                        }
                    }

                </script>
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <!-- Buttons Loading State -->
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
                <!-- Select Inputs -->
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- >Bootstrap Date Picker -->
@endsection
