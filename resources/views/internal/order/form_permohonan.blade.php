@extends('../layout/layout_internal')

@section('page_css')
        <!-- BEGIN PAGE STYLE -->
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
      rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/rateit/rateit.css" rel="stylesheet">
<!-- END PAGE STYLE -->
<style>
    .disabled {
        pointer-events: none;
        cursor: not-allowed;
    }
</style>
@endsection

@section('content')
        <!-- BEGIN PAGE CONTENT -->
<div class="page-content">
    <div class="header">
        {{-- <h2>Permohonan <strong>{{$tipe->nama_instalasi}}</strong></h2> --}}
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{url('/internal')}}">Dashboard</a></li>
                <li><a href="#">Order</a></li>
                <li class="active">Permohonan</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <p class="m-t-10 m-b-20 f-16">{{-- Silahkan inputkan detail permohonan anda. --}}</p>
            <div class="panel">

                {!! Form::open(array('url'=> '/internal/store_permohonan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                <input type="hidden" name="order_id" value="{{$order_id}}"/>
                <input type="hidden" name="permohonan_id" id="permohonan_id" value="{{$permohonan_id}}"/>

                <div class="panel-header bg-primary">
                    <h3><i class="icon-bulb"></i> Form <strong>Input</strong></h3>
                </div>
                <br/>
                <div class="form-group">
                    <div class="col-sm-10" style="font-size: 16pt;">
                        <label class="col-sm-10 control-label">Nomor Permohonan : {{($permohonan == null) ? "-" :$permohonan->nomor_permohonan}}</label>

                    </div>
                </div>
                <div class="panel-content">
                    <div class="nav-tabs2" id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                    Data Umum Permohonan</a>
                            </li>
                            <li><a href="#order" onclick="generateForm();" data-toggle={{($order == null) ? "" :"tab"}}><i
                                            class="icon-user"></i> Produk dan Lingkup Pekerjaan</a>
                            </li>
                        </ul>
                        <div class="tab-content bg-white">
                            {{--start tab order--}}
                            <div class="tab-pane active" id="general">
                                <div class="panel-body bg-white">
                                    <input type="hidden" name="no_order"
                                           value="{{($order != null)?$order->id: 0}}"/>
                                    <input type="hidden" name="permohonan_id"
                                           value="{{($permohonan != null) ? $permohonan->id : 0}}"/>
                                    @include($file_detail, ['instalasi' => $instalasi])
                                </div>
                            </div>
                            {{--end tab order--}}
                            {{--start tab produk dan lingkup pekerjaan--}}
                            <div class="tab-pane" id="order">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Produk Layanan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="produk_layanan"
                                               readonly value="{{$permohonan->produk->produk_layanan}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="jenis_lingkup_pekerjaan"
                                               readonly value="{{$permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"/>
                                        <select id="lingkup_pekerjaan" style="visibility: hidden"
                                                class="form-control form-white disabled" data-style="white"
                                                data-search="true" name="lingkup_pekerjaan" required>
                                            @foreach($lingkup_pekerjaan as $row)
                                                <option {{($permohonan != null && $permohonan->id_lingkup_pekerjaan == $row->id) ? "selected='selected'" : ""}} value="{{$row->id}}">{{$row->jenis_lingkup_pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="form_upload">
                                    {{--Form upload file digenerate di sini--}}
                                </div>
                            </div>
                            {{--end tab produk dan lingkup pekerjaan--}}
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="panel-footer clearfix bg-white">
                    <div class="pull-right">
                        <a class="btn btn-warning btn-square"
                           href="{{url(($order != null) ? '/internal/detail_order/'.$order->id:'#')}}">Kembali</a>
                        {{--<button type="submit" class="btn btn-success btn-square">Simpan</button>--}}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<!-- END MAIN CONTENT -->
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

    <script type="text/javascript">
        if ($('#permohonan_id').val() > 0) generateForm();
        if ($('#permohonan_id').val() > 0) generateFormInstalasi();
        $('#lingkup_pekerjaan').change(function () {
            generateForm();
        });
        $('#lingkup_program').change(function () {
            generateForm();
        });
        $('#id_instalasi').change(function () {
            generateFormInstalasi();
        });
        //function generateForm() {
        //    if ($('#lingkup_pekerjaan').val() != "") {
        //        $('#form_upload').load("/internal/custom_field_verifikasi/" + $('#lingkup_pekerjaan').val() + "/" + $('#permohonan_id').val()+ "/{{$permohonan->instalasi->status_baru}}");
        //        $('#form_upload').show();
        //    }
        //}

        function generateFormInstalasi() {
            if ($('#id_instalasi').val() == "") {
                $('#detail_instalasi').html("");
            } else {
                $('#detail_instalasi').load("/eksternal/instalasi_field/" + $('#tipe_instalasi').val() + "/" + $('#id_instalasi').val());
            }
        }

    </script>
@endsection