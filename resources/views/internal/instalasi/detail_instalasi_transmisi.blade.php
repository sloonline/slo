@extends('../layout/layout_internal')

@section('page_css')
    <link rel="stylesheet" type="text/css"
          href="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.demo2.css"/>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Instalasi</strong> Transmisi</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/view_instalasi_transmisi')}}">Instalasi Transmisi</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-content p-t-0">
                        <h2><strong>{{@$instalasi->nama_instalasi}}</strong></h2>
                        <hr/>
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#kapasitas" data-toggle="tab"><i
                                            class="icon-speedometer"></i> KAPASITAS</a></li>
                            <li><a href="#pemilik" data-toggle="tab"><i class="icon-user"></i> PEMILIK</a></li>
                            <li><a href="#lokasi" data-toggle="tab"><i class="icon-map"></i> LOKASI</a></li>
                            <li><a href="#foto" data-toggle="tab"><i class="icon-picture"></i> FOTO</a></li>
                            <li><a href="#history" data-toggle="tab"><i
                                            class="fa fa-history"></i> RIWAYAT</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="control-label">Status Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               value="{{(@$instalasi != null)? ((@$instalasi->status_baru == 1) ? "INSTALASI BARU" : "INSTALASI LAMA") :""}}"
                                               name="nama_instalasi" readonly="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        {{--{{@$instalasi->jenis_instalasi->jenis_instalasi}}--}}
                                        {{--<i class="fa fa-building" style="margin-left: 15px;"></i>--}}
                                        <input type="text" class="form-control form-white"
                                               value="{{(@$instalasi != null)? @$instalasi->jenis_instalasi->jenis_instalasi : ""}}"
                                               name="jenis_instalasi" readonly="">

                                        {{--<select class="form-control form-white" data-search="true" name="jenis_instalasi" id="jenis_instalasi">--}}
                                        {{--<option value="">--- Pilih ---</option>--}}
                                        {{--@foreach($jenis_instalasi as $item)--}}
                                        {{--<option value="{{$item->id}}" {{(@$instalasi != null && @$instalasi->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               value="{{(@$instalasi != null)? @$instalasi->nama_instalasi : ""}}"
                                               name="nama_instalasi" readonly="">
                                        {{--<i class="fa fa-building" style="margin-left: 15px;"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Sistem Jaringan Transmisi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               value="{{(@$instalasi->sistemJaringanTransmisi != null)? @$instalasi->sistemJaringanTransmisi->nama_reference : ""}}"
                                               name="sistem_jaringan_transmisi" readonly="">
                                        {{--<select class="form-control form-white" data-search="true" name="sistem_jaringan_transmisi">--}}
                                        {{--<option value="">--- Pilih ---</option>--}}
                                        {{--@foreach($sistem_jaringan as $item)--}}
                                        {{--<option value="{{$item->id}}" {{(@$instalasi != null && @$instalasi->sis_jar_tower == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kontraktor</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                               value="{{(@$instalasi->kode_kontraktor != null)? @$instalasi->kode_kontraktor ." - ".@$instalasi->kontraktor: ""}}"
                                               readonly="">
                                    </div>
                                </div>
                                @if(@$instalasi->file_pernyataan != null)
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File Pernyataan</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$instalasi->file_pernyataan)}}"
                                               class="btn btn-primary"><i
                                                        class="fa fa-download"></i>View</a>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Jenis <strong>Lingkup</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                @if(@$instalasi->jenis_instalasi->keterangan == JENIS_TRANSMISI)
                                                    <div class="form-group row">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Panjang Saluran TET /
                                                                TT</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control form-white"
                                                                   name="panjang_saluran"
                                                                   value="{{(@$instalasi != null)? @$instalasi->panjang_tt : ""}}"
                                                                   readonly="">
                                                            {{--<i class="fa fa-credit-card"></i>--}}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <label class="col-sm-12 control-label">kms</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Jumlah Tower</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control form-white"
                                                                   name="jumlah_tower"
                                                                   value="{{(@$instalasi != null)? @$instalasi->jml_tower : ""}}"
                                                                   readonly="">
                                                            {{--<i class="fa fa-credit-card"></i>--}}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <label class="col-sm-12 control-label">buah</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">SUTET / SUTT</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control form-white"
                                                                   name="sutet"
                                                                   value="{{(@$instalasi != null)? @$instalasi->sutet : ""}}"
                                                                   readonly="">
                                                            {{--<i class="fa fa-credit-card"></i>--}}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <label class="col-sm-12 control-label">kms</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">SKTT</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control form-white"
                                                                   name="sktt"
                                                                   value="{{(@$instalasi != null)? @$instalasi->sktt : ""}}"
                                                                   readonly="">
                                                            {{--<i class="fa fa-credit-card"></i>--}}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <label class="col-sm-12 control-label">kms</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">SKLT</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control form-white"
                                                                   name="sklt"
                                                                   value="{{(@$instalasi != null)? @$instalasi->sklt : ""}}"
                                                                   readonly="">
                                                            {{--<i class="fa fa-credit-card"></i>--}}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <label class="col-sm-12 control-label">kms</label>
                                                        </div>
                                                    </div>

                                                @else
                                                    <table class="table table-dynamic table-bordered">
                                                        <thead>
                                                        <th>NO.</th>
                                                        <th>NAMA</th>
                                                        <th>STATUS INSTALASI</th>
                                                        <th>JENIS</th>
                                                        <th>KEPEMILIKAN</th>
                                                        <th>PEMILIK</th>
                                                        <th>KAPASITAS PEMUTUS TENAGA (kA)</th>
                                                        <th>KAPASITAS TRAFO TENAGA/KAPASITOR/REAKTOR (MVA)</th>
                                                        <th>TEGANGAN PENGENAL</th>
                                                        <th>FILE LAMPIRAN</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i = 1;?>
                                                        @foreach(@$instalasi->BayGardu as $bg)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$bg->nama_bay}}</td>
                                                                <td style="white-space: nowrap;">{{(@$bg->status_baru == "0") ? "Instalasi Lama" : "Instalasi Baru"}}
                                                                    @if(@$bg->status_baru == "0")
                                                                        <br/>
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->surat_pernyataan_status)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SURAT PERNYATAAN</a>
                                                                    @endif
                                                                </td>
                                                                <td>{{$bg->jenis_bay}}</td>
                                                                <td>{{$bg->tipe_pemilik}}</td>
                                                                <td>{{($bg->tipe_pemilik == MILIK_SENDIRI) ? "" : $bg->pemilik->nama_pemilik}}</td>
                                                                <td>{{@$bg->kapasitas_pemutus}}</td>
                                                                <td>{{@$bg->kapasitas_trafo}}</td>
                                                                <td>{{($bg->pengenal != null) ? $bg->pengenal->nama_reference : ""}}</td>
                                                                <td style="white-space: nowrap">
                                                                    @if(@$bg->file_sbujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sbujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SBUJPTL</a><br/>
                                                                    @endif
                                                                    @if(@$bg->file_iujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_iujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>IUJPTL</a><br/>
                                                                    @endif
                                                                    @if(@$bg->file_sld != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sld)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SINGLE
                                                                            LINE DIAGRAM</a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <?php $i++;?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kapasitas">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kapasitas Total Gardu Induk (MVA)</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="kapasitas_gardu_induk"
                                               value="{{(@$instalasi != null)? @$instalasi->kapasitas_gi : ""}}"
                                               readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                               value="{{(@$instalasi->teganganPengenal != null)? @$instalasi->teganganPengenal->nama_reference : ""}}"
                                               readonly="">
                                        {{--<select class="form-control form-white" data-search="true" name="tegangan_pengenal">--}}
                                        {{--<option value="">--- Pilih ---</option>--}}
                                        {{--@foreach($tegangan_pengenal as $item)--}}
                                        {{--<option value="{{$item->id}}" {{(@$instalasi != null && @$instalasi->tegangan_pengenal == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pemilik">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Pemilik <strong>Instalasi</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kepemilikan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kepemilikan" name="kepemilikan"
                                                               placeholder="Kepemilikan" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->perusahaan->kategori->nama_kategori : null}}"
                                                               readonly="">
                                                        {{--<i class="fa fa-building"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="nama_pemilik" name="nama_pemilik"
                                                               placeholder="Nama Pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->nama_pemilik : null}}"
                                                               readonly="">
                                                        {{--<i class="fa fa-building"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                <textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
                                                          class="form-control form-white"
                                                          placeholder="Alamat Instalasi..."
                                                          readonly="">{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->alamat_pemilik : null}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi" name="provinsi" placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik->province != null) ? @$instalasi->pemilik->province->province : null}}"
                                                               readonly="">

                                                        {{--<select name="provinsi" class="form-control form-white" data-search="true"--}}
                                                        {{--id="province">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($province as $item)--}}
                                                        {{--<option value="{{$item->id}}"--}}
                                                        {{--{{($pemilik != null && $pemilik->id_province == $item->id) ? "selected" : ""}}--}}
                                                        {{-->{{$item->province}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kabupaten" name="kabupaten"
                                                               placeholder="Kabupaten / Kota" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik->city != null) ? @$instalasi->pemilik->city->city : null}}"
                                                               readonly="">

                                                        {{--<select name="kabupaten" id="city" class="form-control form-white" data-search="true">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($city as $item)--}}
                                                        {{--<option value="{{$item->id}}"--}}
                                                        {{--class="{{$item->id_province}}"--}}
                                                        {{--{{($pemilik != null && $pemilik->id_city == $item->id) ? "selected" : ""}}--}}
                                                        {{-->{{$item->city}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kode_pos" name="kode_pos" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->kode_pos_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Telepon</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="telepon" name="telepon" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->telepon_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Fax</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_fax" name="no_fax" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->no_fax_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="email_pemilik" name="email_pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->email_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Ijin <strong>Usaha</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="jenis_ijin_usaha" name="jenis_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->jenisIjinUsaha->nama_reference : null}}"
                                                               readonly="">
                                                        {{--<select id="jenis_ijin_usaha" name="jenis_ijin_usaha" class="form-control form-white">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($jenis_ijin_usaha as $item)--}}
                                                        {{--                                                                <option value="{{$item->id}}" {{($pemilik != null && $pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
                                                        {{--<option value="{{$item->id}}" >{{$item->nama_reference}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Penerbit Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="penerbit_ijin_usaha" name="penerbit_ijin_usaha"
                                                               type="text" class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->penerbit_ijin_usaha : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_ijin_usaha" name="no_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->no_ijin_usaha : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_iu" name="masa_berlaku_iu" type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                                               value="{{(@$instalasi->pemilik->masa_berlaku_iu != null) ? @$instalasi->pemilik->masa_berlaku_iu->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Surat Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if(@$instalasi->pemilik->file_siup != null)
                                                            <a href="{{url('upload/'.@$instalasi->pemilik->file_siup)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{@$instalasi->pemilik->file_siup}}
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="nama_kontrak" name="nama_kontrak" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->nama_kontrak : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_kontrak" name="no_kontrak" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->no_kontrak : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal Pengesahaan
                                                            Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="tgl_pengesahan_kontrak" name="tgl_pengesahan_kontrak"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{(@$instalasi->pemilik->tgl_pengesahan_kontrak != null) ? @$instalasi->pemilik->tgl_pengesahan_kontrak->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_kontrak" name="masa_berlaku_kontrak"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{(@$instalasi->pemilik->masa_berlaku_kontrak != null) ? @$instalasi->pemilik->masa_berlaku_kontrak->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if(@$instalasi->pemilik->file_kontrak != null)
                                                            <a href="{{url('upload/'.@$instalasi->pemilik->file_kontrak)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{@$instalasi->pemilik->file_kontrak}}
                                                            </a>
                                                        @endif

                                                        {{--<div class="file">--}}
                                                        {{--<div class="option-group">--}}
                                                        {{--<span class="file-button btn-primary">Choose File</span>--}}
                                                        {{--<input type="file" class="custom-file" name="file_kontrak_sewa"--}}
                                                        {{--onchange="document.getElementById('uploader2').value = this.value;">--}}
                                                        {{--<input type="text" class="form-control form-white" id="uploader2"--}}
                                                        {{--placeholder="no file selected"--}}
                                                        {{--value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">--}}
                                                        {{--value="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                <?php
                                                //if($pemilik != null && $pemilik->file_kontrak != null){
                                                ?>
                                                {{--<div class="form-group">--}}
                                                {{--<div class="col-sm-3">--}}
                                                {{--<label class="control-label"></label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9">--}}
                                                {{--<a href="{{url('upload/'.$pemilik->file_kontrak)}}" class="btn btn-primary"><i--}}
                                                {{--class="fa fa-download"></i>View</a>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                <?php
                                                //}
                                                ?>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_spjbtl" name="no_spjbtl" type="text"
                                                               class="form-control form-white"
                                                               value="{{(@$instalasi->pemilik != null) ? @$instalasi->pemilik->no_spjbtl : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="tgl_spjbtl" name="tgl_spjbtl" type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                                               value="{{(@$instalasi->pemilik->tgl_spjbtl != null) ? @$instalasi->pemilik->tgl_spjbtl->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_spjbtl" name="masa_berlaku_spjbtl"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{(@$instalasi->pemilik->masa_berlaku_spjbtl != null) ? @$instalasi->pemilik->masa_berlaku_spjbtl->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if(@$instalasi->pemilik->file_spjbtl != null)
                                                            <a href="{{url('upload/'.@$instalasi->pemilik->file_spjbtl)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{@$instalasi->pemilik->file_spjbtl}}
                                                            </a>
                                                        @endif

                                                        {{--<div class="file">--}}
                                                        {{--<div class="option-group">--}}
                                                        {{--<span class="file-button btn-primary">Choose File</span>--}}
                                                        {{--<input type="file" class="custom-file" name="file_spjbtl"--}}
                                                        {{--onchange="document.getElementById('uploader3').value = this.value;">--}}
                                                        {{--<input type="text" class="form-control form-white" id="uploader3"--}}
                                                        {{--placeholder="no file selected"--}}
                                                        {{--                                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">--}}
                                                        {{--value="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                {{--<br>--}}
                                {{--<form class="form-horizontal">--}}
                                {{--<div class="form-group">--}}
                                {{--<div class="col-sm-3">--}}
                                {{--<label class="control-label">Ijin Usaha</label>--}}
                                {{--</div>--}}
                                {{--<div class="col-sm-9">--}}
                                {{--<div class="append-icon">--}}
                                {{--<input type="text" class="form-control form-white">--}}
                                {{--<i class="fa fa-building-o"></i>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</form>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="tab-pane fade" id="lokasi">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                                  <textarea rows="3" class="form-control form-white"
                                                            name="alamat_instalasi"
                                                            placeholder="Alamat Instalasi..."
                                                            readonly=""> {{(@$instalasi != null)? @$instalasi->alamat_instalasi : ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Provinsi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                               type="text"
                                               class="form-control form-white"
                                               value="{{(@$instalasi->provinsi != null) ? @$instalasi->provinsi->province : null}}"
                                               readonly="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                               type="text"
                                               class="form-control form-white"
                                               value="{{(@$instalasi->kota != null) ? @$instalasi->kota->city : null}}"
                                               readonly="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Koordinat <strong>LOKASI (AWAL)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{(@$instalasi != null)? @$instalasi->long_awal : ""}}"
                                                               name="longitude_awal" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{(@$instalasi != null)? @$instalasi->lat_awal : ""}}"
                                                               name="latitude_awal" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Koordinat <strong>LOKASI (AKHIR)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{(@$instalasi != null)? @$instalasi->long_akhir : ""}}"
                                                               name="longitude_akhir" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{(@$instalasi != null)? @$instalasi->lat_akhir : ""}}"
                                                               name="latitude_akhir" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12"></div>
                                    <div id="mapdiv" class="col-md-12">
                                        <fieldset class="cart-summary">
                                            <legend>Lokasi Instalasi</legend>
                                            <div id="mapid"
                                                 style="width: 100%; height: 200px; z-index: 1; position:relative"></div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="foto">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img id="img-instalasi" width="200"
                                             src="{{(@$instalasi->foto_1 != null) ?  url('upload/instalasi/'.@$instalasi->foto_1) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-lg-3">
                                        <img id="img-instalasi" width="200"
                                             src="{{(@$instalasi->foto_2 != null) ?  url('upload/instalasi/'.@$instalasi->foto_2) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-lg-3">
                                        <img id="img-instalasi" width="200"
                                             src="{{(@$instalasi->foto_3 != null) ?  url('upload/instalasi/'.@$instalasi->foto_3) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-lg-3">
                                        <img id="img-instalasi" width="200"
                                             src="{{(@$instalasi->foto_4 != null) ?  url('upload/instalasi/'.@$instalasi->foto_4) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="history">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-dynamic">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor Order</th>
                                                <th>Nomor Permohonan</th>
                                                <th>Tanggal Order</th>
                                                <th>Nama Layanan</th>
                                                <th>Aksi</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(is_array($permohonan) || is_object($permohonan))
                                                <?php $no = 1; ?>
                                                @foreach($permohonan as $item)

                                                    <tr>
                                                        <td>{{ $no}}</td>
                                                        <td>{{ $item->nomor_order}}</td>
                                                        <td>{{ $item->nomor_permohonan}}</td>
                                                        <td>{{ date('d M Y',strtotime($item->tanggal_order)) }}</td>
                                                        <td>{{ $item->produk_layanan}}</td>

                                                        <td>
                                                            <a href="{{url('/eksternal/detail_order/'.$item->id_orders)}}"
                                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                        class="fa fa-eye"></i></a></td>

                                                    </tr>
                                                    <?php $no++; ?>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <div class="pull-right">
                                    <a onclick="return window.history.back()"
                                       class="btn btn-warning btn-square btn-embossed">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.min.js"></script>
            <script>
                generateMap();

                function generateMap() {
                    //First, specify your Mapbox API access token
                    var mymap = L.map('mapid');
                    var ltlng = [-6.1751, 106.8650];
                    var ltlng_line = [];
                    var isMark = false;
                    var isPoly = false;

                    @if($instalasi->long_awal != null && $instalasi->lat_awal != null)
                            ltlng = [{{$instalasi->lat_awal}}, {{$instalasi->long_awal}}];
                    isMark = true;
                    @endif

                            @if($instalasi->long_akhir != null && $instalasi->lat_akhir != null)
                            ltlng_line = [[{{$instalasi->lat_awal}}, {{$instalasi->long_awal}}],
                        [{{$instalasi->lat_akhir}}, {{$instalasi->long_akhir}}]];
                    isPoly = true;
                    @endif

                    mymap.setView(ltlng, 5);

                    if (isPoly) {
                        var polyline = L.polyline(ltlng_line, {color: 'blue'}).addTo(mymap);
                        mymap.fitBounds(polyline.getBounds());
                    } else if (isMark) {
                        var marker = L.marker(ltlng).addTo(mymap);
                    }


                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                        maxZoom: 18,
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
                        id: 'mapbox.streets'
                    }).addTo(mymap);
                }
            </script>
@endsection
