@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Create <strong>Workflow</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Worflow</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    {!! Form::open(['url'=>'internal/workflow/role','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <input type="hidden" name="workflow_id" value="{{$workflow_id}}">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> Workflow <strong>Role</strong></h3>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-content ">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Pilih Role</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="icheck-list">
                                        @foreach($roles as $row)
                                            <input type="checkbox" data-checkbox="icheckbox_square-blue"
                                                    name="roles[]"
                                                    class="roles"
                                                    value="{{$row->id}}"
                                                    <?php
                                                        $stat = 0;
                                                        $i = 0;
                                                        while ($stat == 0 && $i < sizeof($workflow_role)) {
                                                            if ($workflow_role[$i]->role_id == $row->id) {
                                                                $stat = 1;
                                                                echo "checked";
                                                            } else {
                                                                $i++;
                                                            }
                                                        }
                                                        ?>
                                                    >
                                            {{$row->display_name}}<br/><br/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <input type="hidden" name="id" value="{{@$cr_workflow->id}}">
                                <a href="{{ url('internal/workflow') }}" class="btn btn-warning btn-square btn-embossed">Batal
                                    &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection
