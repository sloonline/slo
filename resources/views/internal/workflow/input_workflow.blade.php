@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Create <strong>Workflow</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Worflow</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    {!! Form::open(['url'=>'internal/workflow/add','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>Workflow</strong></h3>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-content ">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Flow Status</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <select name="flow_status_id"class="form-control form-white">
                                            @foreach(@$flow_status as $item)
                                                <option value="{{$item->id}}" {{($item->id == @$cr_workflow->flow_status_id)?'selected':''}}>{{$item->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Role</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <select name="role_id" class="form-control form-white">
                                            @foreach(@$role as $item)
                                                <option value="{{$item->id}}" {{($item->id == @$cr_workflow->role_id)?'selected':''}}>{{$item->display_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Modul</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                       <input name="modul" type="text" class="form-control form-white" value="{{@@$cr_workflow->modul}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Seen By</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                       <input name="seen_by" type="text" class="form-control form-white" value="{{@@$cr_workflow->seen_by}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Next</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <select name="next_workflow_id" class="form-control form-white" data-live-search="true">
                                            <option value="">--Pilih Workflow--</option>
                                            @foreach ($workflow as $key => $item)
                                                <option value="{{$item->id}}" {{($item->id == @$cr_workflow->next)?'selected':''}}>{{$item->id.' - '.$item->flow_status->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Prev</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <select name="prev_workflow_id" class="form-control form-white">
                                            <option value="">--Pilih Workflow--</option>
                                            @foreach ($workflow as $key => $item)
                                                <option value="{{$item->id}}" {{($item->id == @$cr_workflow->prev)?'selected':''}}>{{$item->id.' - '.$item->flow_status->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <input type="hidden" name="id" value="{{@$cr_workflow->id}}">
                                <a href="{{ url('master/bidang') }}" class="btn btn-warning btn-square btn-embossed">Batal
                                    &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection
