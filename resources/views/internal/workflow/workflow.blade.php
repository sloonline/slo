@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Workflow <strong>Setting</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Workflow</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('success')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{ url('/') }}/internal/workflow/add" class="btn btn-success btn-square btn-block btn-embossed">
                                    <i class="fa fa-plus"></i> Tambah Workflow</a>
                            </div>
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Workflow ID</th>
                                <th>Flow Status</th>
                                <th>Role</th>
                                <th>Modul</th>
                                <th>Next</th>
                                <th>Prev</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($workflow) && (is_array($workflow) || is_object($workflow)))
                                <?php $no = 1; ?>
                                @foreach($workflow as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ @$item->id }}</td>
                                        <td>{{ @$item->flow_status->status}}</td>
                                        <td>{{ @$item->role->display_name }}</td>
                                        <td>{{ @$item->modul }}</td>
                                        <td>{{ @$item->next.' - '. @$item->nextWf->flow_status->status }}</td>
                                        <td>{{ @$item->prev.' - '. @$item->prevWf->flow_status->status }}</td>
                                        <td style="width:10%">
                                            <a href="{{url('/internal/edit_workflow/'.@$item->id)}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                        class="fa fa-pencil"></i></a>
                                            <a href="{{url('/internal/workflow/createNotif/'.@$item->id)}}"
                                                class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-envelope"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <!-- Buttons Loading State -->

    <script>
        $(document).ready(function () {
            var oTable = $('#my_table').dataTable();

            // Sort immediately with columns 0 and 1
//            oTable.fnSort([[2, 'desc']]);
        });
    </script>
@endsection
