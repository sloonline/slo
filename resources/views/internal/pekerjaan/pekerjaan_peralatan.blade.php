@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Permohonan Peralatan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Peralatan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PEMINJAMAN_PERALATAN))
                                    <a href="{{url('internal/create_pekerjaan_peralatan')}}"
                                       class="btn btn-success btn-square btn-block btn-embossed"><i
                                                class="fa fa-plus"></i>
                                        Permohonan Baru</a>
                                @endif
                            </div>
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Peminjam</th>
                                <th>Pekerjaan</th>
                                <th>Tanggal Pinjam</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <?php
                            $no = 1;
                            ?>
                            <tbody>
                            @foreach ($permohonan_peralatan as $key => $item)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$item->nama_pemohon}}</td>
                                    <td>{{$item->pekerjaan->pekerjaan}}</td>
                                    <td>{{$item->tanggal_pemakaian}}</td>
                                    <td>{{$item->flow_status->status}}</td>
                                    <td>
                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PEMINJAMAN_PERALATAN))
                                            @if(\App\PermohonanPeralatan::isAllowEdit($item))
                                                <a href="{{url('internal/create_pekerjaan_peralatan/'.$item->id)}}"
                                                   class="btn btn-sm btn-warning btn-square btn-embossed"
                                                   data-rel="tooltip"
                                                   data-placement="right"
                                                   title="Edit Permohonan Peralatan"><i
                                                            class="fa fa-pencil"></i></a>
                                            @endif
                                        @endif
                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PENGEMBALIAN_PERALATAN))
                                            @if(\App\PermohonanPeralatan::isAllowManagePeralatan($item))
                                                <a href="{{url('internal/manage_pekerjaan_peralatan/'.$item->id)}}"
                                                   class="btn btn-sm btn-success btn-square btn-embossed"
                                                   data-rel="tooltip"
                                                   data-placement="right"
                                                   title="Pengaturan Peralatan"><i
                                                            class="fa fa-wrench"></i></a>
                                            @endif
                                        @endif
                                        <a href="{{url('internal/detail_pekerjaan_peralatan/'.$item->id)}}"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"
                                           data-rel="tooltip"
                                           data-placement="right" title="Detail Permohonan Peralatan"><i
                                                    class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php $no++?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();

                    // Sort immediately with columns 0 and 1
                    oTable.fnSort([[2, 'desc']]);
                });
            </script>
@endsection
