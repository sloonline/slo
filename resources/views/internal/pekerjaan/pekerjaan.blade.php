@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Pekerjaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pekerjaan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('internal/create_pekerjaan')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Pekerjaan Baru</a>
                            </div>
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Peminta Jasa</th>
                                <th>No Surat Permohonan</th>
                                <th>Pekerjaan</th>
                                <th>No Surat Penugasan</th>
                                <th>Progress</th>
                                <th>Status</th>
                                <th style="width:15%">Aksi</th>
                            </tr>
                            </thead>
                            <?php
                            $no = 1;
                            ?>
                            <tbody>
                            @if(is_array($pekerjaan) || is_object($pekerjaan))
                                @foreach($pekerjaan as $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{@$item->permohonan->user->perusahaan->nama_perusahaan}}</td>
                                        <td>{{@$item->permohonan->nomor_permohonan}}</td>
                                        <td>{{@$item->pekerjaan}}</td>
                                        <td>{{@$item->no_surat_penugasan}}</td>
                                        <td>{{@$item->progress}} %</td>
                                        <td>
                                            @if($item->status_pekerjaan != null)
                                                {{@$item->status_pekerjaan}}
                                            @else
                                                {{@$item->flow_status->status}}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{url('internal/detail_pekerjaan/'.$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"
                                                        data-rel="tooltip"
                                                        data-placement="right" title="Detail Pekerjaan"></i>
                                            </a>
                                            @if(\App\Pekerjaan::isAllowEdit($item))
                                                <a href="{{url('internal/create_pekerjaan/'.$item->id)}}"
                                                   class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                            class="fa fa-pencil"
                                                            data-rel="tooltip"
                                                            data-placement="right" title="Edit Pekerjaan"></i>
                                                </a>

                                                <a href="{{url('internal/delete_pekerjaan/'.$item->id)}}"
                                                   class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                            class="fa fa-trash"
                                                            data-rel="tooltip"
                                                            onclick="return confirm('Apakah anda yakin untuk menghapus pekerjaan ini?')"
                                                            data-placement="right" title="Hapus Pekerjaan"></i></a>
                                            @endif
                                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_PRINT_SURAT_TUGAS_PERSONIL))
                                                @if(\App\Pekerjaan::isLastApproval($item))
                                                    <a href="{{url('internal/print_form_pekerjaan/'.$item->id)}}"
                                                       class="btn btn-sm btn-success btn-square btn-embossed"><i
                                                                class="fa fa-print"
                                                                data-rel="tooltip"
                                                                data-placement="top"
                                                                title="Template Surat Penugasan"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();
                });
            </script>
@endsection
                