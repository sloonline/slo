@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
<?php
$allowManage = \App\PermohonanPeralatan::isAllowManagePeralatan($permohonan_peralatan);
?>

@section('content')
    <?php
    $tmp_wbs = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Permohonan Peralatan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Peralatan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                            Permohonan Peminjaman
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->pekerjaan->pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIP Pemohon</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->nip_pemohon}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Pemohon</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->nama_pemohon}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Pemakaian</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->tanggal_pemakaian}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Lokasi Inspeksi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->lokasi_inspeksi}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Maksud Perjalanan Dinas</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" value="{{$permohonan_peralatan->maksud_perjalanan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Keterangan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea name="keterangan" class="form-control" rows="4"
                                                    placeholder="Keterangan" disabled
                                                    >{{$permohonan_peralatan->keterangan}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <!-- list peralatan -->
                                <div class="nav-tabs2" id="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                List Peralatan
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content bg-white">
                                        <table id="my_table" class="table table-hover table-dynamic">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Alat</th>
                                                    <th>Merk</th>
                                                    <th>Type Model</th>
                                                    <th>Nomor Seri</th>
                                                    <th>Status</th>
                                                    {{-- <th>Pilih</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  $no =1;?>
                                                @foreach ($pekerjaan_peralatan as  $item)
                                                    <tr>
                                                        <td>{{$no}}</td>
                                                        <td>{{@$item->peralatan->nama_alat}}</td>
                                                        <td>{{@$item->peralatan->merk_alat}}</td>
                                                        <td>{{@$item->peralatan->tipe_alat}}</td>
                                                        <td>{{@$item->peralatan->nomor_seri}}</td>
                                                        <td>{{@$item->status_peralatan->nama_reference}}</td>
                                                        {{-- <td>
                                                            <input  disabled name="peralatan[]" type="checkbox" class="form-control" value="{{$item->id}}">
                                                        </td> --}}
                                                    </tr>
                                                    <?php $no++?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>{{--List Surat Peminjaman & Surat Pengembalian--}}
                                @if($allowManage)
                                    <br/><br/>
                                    <div class="nav-tabs2" id="tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                    Surat Peminjaman & Pengembalian
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <div class="col-sm-3 pull-right">
                                                <div class="col-sm-12 prepend-icon">
                                                    <a href="{{url('/internal/print_form_peralatan/'.@$permohonan_peralatan->id)}}">
                                                        <button type="button"
                                                                class="col-lg-12 btn btn-success">
                                                            <center><i
                                                                        class="fa fa-print"></i>Download Form Peralatan
                                                            </center>
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                            <br/><br/><hr/>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Permohonan Peralatan</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    @if(@$permohonan_peralatan->file_permohonan != null)
                                                        <div class="col-sm-12 prepend-icon">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.@$permohonan_peralatan->file_permohonan)}}">
                                                                <button type="button"
                                                                        class="col-lg-12 btn btn-primary">
                                                                    <center><i
                                                                                class="fa fa-download"></i>Preview
                                                                        File
                                                                    </center>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Pengembalian Peralatan</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    @if(@$permohonan_peralatan->file_pengembalian != null)
                                                        <div class="col-sm-12 prepend-icon">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.@$permohonan_peralatan->file_pengembalian)}}">
                                                                <button type="button"
                                                                        class="col-lg-12 btn btn-primary">
                                                                    <center><i
                                                                                class="fa fa-download"></i>Preview
                                                                        File
                                                                    </center>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <br><br>
                                <hr/>
                                @include('workflow_view')
                            </div>
                        </div>
                    </div>
                </div>
            @endsection


            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- Buttons Loading State -->

                <script>
                // $(document).ready(function () {
                //     var oTable = $('#my_table').dataTable();
                //     // Sort immediately with columns 0 and 1
                //     oTable.fnSort([[2, 'desc']]);
                // });

                </script>
            @endsection
