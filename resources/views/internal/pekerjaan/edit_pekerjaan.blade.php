@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_wbs = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Pekerjaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pekerjaan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> '/internal/save_pekerjaan', 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-pekerjaan')) !!}
                            {!! Form::hidden('id', @$id) !!}
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    <li id="li_personil">
                                        <a href="#personil" data-toggle="tab"><i class="icon-user" class="disabled"></i>
                                            Personil
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">WBS/IO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="hidden" name="wbs_io" value="{{@$pekerjaan->wbs_id}}">
                                                    <input type="text" class="form-control" disabled
                                                           value="{{@$pekerjaan->wbs_io->nomor}} ({{@$pekerjaan->wbs_io->jenis->nama_reference}})">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Permohonan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="hidden" name="permohonan"
                                                           value="{{@$pekerjaan->permohonan_id}}">
                                                    <input type="text" class="form-control" name="permohonan"
                                                           value="{{@$pekerjaan->permohonan->nomor_permohonan.' - '.@$pekerjaan->permohonan->produk->produk_layanan." : ".@$pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control form-white" name="pekerjaan"
                                                           value="{{@$pekerjaan->pekerjaan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Mulai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_mulai" type="text"
                                                           value="{{@$pekerjaan->tgl_mulai_pekerjaan}}"
                                                           class="form-control" placeholder="Tanggal Mulai Pekerjaan"
                                                           readonly required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Selesai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_selesai" type="text"
                                                           value="{{@$pekerjaan->tgl_selesai_pekerjaan}}"
                                                           class="form-control" placeholder="Tanggal Selesai Pekerjaan"
                                                           readonly required>
                                                </div>
                                            </div>
                                            {{--<div class="form-group col-lg-12">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-sm-12 control-label">No Surat Penugasan</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-4">--}}
                                                    {{--<input name="no_surat_penugasan"--}}
                                                           {{--value="{{@$pekerjaan->no_surat_penugasan}}" type="text"--}}
                                                           {{--class="form-control form-white"--}}
                                                           {{--placeholder="Nomor Surat Penugasan" required>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-2">--}}
                                                    {{--<input name="tgl_surat_penugasan"--}}
                                                           {{--type="text" value="{{@$pekerjaan->tgl_surat_penugasan}}"--}}
                                                           {{--class="form-control b-datepicker form-white"--}}
                                                           {{--data-date-format="dd-mm-yyyy"--}}
                                                           {{--data-lang="en"--}}
                                                           {{--data-RTL="false"--}}
                                                           {{--placeholder="Tanggal" required>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group col-lg-12">--}}
                                                {{--<div class="col-sm-3">--}}
                                                    {{--<label class="col-sm-12 control-label">File Surat Penugasan</label>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-6">--}}
                                                    {{--<input name="file_surat_penugasan" type="file"--}}
                                                           {{--class="form-control form-white" placeholder="Pekerjaan"--}}
                                                           {{--{{(@$pekerjaan->file_surat_penugasan != null) ? "" : "required"}}>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--@if(@$pekerjaan->file_surat_penugasan != null)--}}
                                                {{--<div class="form-group col-lg-12">--}}
                                                    {{--<div class="col-sm-3">--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-4">--}}
                                                        {{--<div class="col-sm-12 prepend-icon">--}}
                                                            {{--<a target="_blank"--}}
                                                               {{--href="{{url('upload/'.@$pekerjaan->file_surat_penugasan)}}">--}}
                                                                {{--<button type="button"--}}
                                                                        {{--class="col-lg-12 btn btn-primary">--}}
                                                                    {{--<center><i--}}
                                                                                {{--class="fa fa-download"></i>Preview--}}
                                                                        {{--File--}}
                                                                    {{--</center>--}}
                                                                {{--</button>--}}
                                                            {{--</a>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--@endif--}}
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control form-white form-white" rows="4"
                                                              name="uraian_pekerjaan"
                                                              placeholder="Uraian Pekerjaan"
                                                              required>{{@$pekerjaan->uraian_pekerjaan}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="personil">
                                        @foreach($data_personil as $item)
                                        <div class="nav-tabs2" id="tabs">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#general" data-toggle="tab"
                                                                      aria-expanded="true"><i class="icon-plus"></i>{{$item["jabatan"]}}</a></li>
                                            </ul>
                                            <div class="tab-content bg-white">
                                                <table id="{{str_replace(' ','_',$item["jabatan"])}}" class="table table-hover table-dynamic">
                                                    <thead>
                                                    <tr>
                                                        <th>NIP</th>
                                                        <th>Nama</th>
                                                        <th>Status Pekerjaan</th>
                                                        <th>Bidang</th>
                                                        <th>Sub Bidang</th>
                                                        <th>Grade</th>
                                                        <th>Pilih</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($item['personil'] as $personil)
                                                        <tr>
                                                            <td>{{@$personil->nip}}</td>
                                                            <td>{{@$personil->nama}}</td>
                                                            <td>{{@$personil->status_pekerja->status}}</td>
                                                            <td>{{@$personil->bidang->nama_bidang}}</td>
                                                            <td>{{@$personil->sub_bidang->nama_sub_bidang}}</td>
                                                            <td>{{@$personil->grade}}</td>
                                                            <td><input type="checkbox" {{(in_array(array('personil_id' => @$personil->id,'jabatan_id' => $item["jabatan_id"]), $id_personil)) ? "checked" : ""}}
                                                                       name="pilih_personil_{{$item["jabatan_id"]}}[]"
                                                                       value="{{$personil->id}}"
                                                                       class="form-control"></td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <br><br>
                                            @endforeach
                                    </div>
                                </div>

                                <hr/>
                                <div class="panel-footer clearfix bg-white">
                                    <div class="pull-right">
                                        <a href="{{ url('internal/pekerjaan') }}"
                                           class="btn btn-warning btn-square btn-embossed">Batal
                                            &nbsp;<i class="icon-ban"></i></a>
                                        <button type="submit"
                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                data-style="zoom-in">Simpan &nbsp;<i
                                                    class="glyphicon glyphicon-floppy-saved"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @endsection


            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- Buttons Loading State -->
    <script>
        $("#form-pekerjaan").submit(function(){
            if($('#{{str_replace(' ','_',PELAKSANA_INSPEKSI)}}  input:checked').length == 0)
            {
                alert('Salah satu pelaksana inspeksi harus dipilih');
                return false;
            }else{
                return true;
            }
        });
    </script>

@endsection
