@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_wbs = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Permohonan Peralatan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Peralatan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> '/internal/save_pekerjaan_peralatan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                            Permohonan Peminjaman
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    @if($permohonan_peralatan->id == null)
                                                        <select data-search="true" name="pekerjaan_id" class="form-control form-white" id="wbs" required>
                                                            <option>- Pilih Pekerjaan -</option>
                                                            @foreach ($pekerjaan_dm_approved as $key => $item)
                                                                <option value="{{$item->id}}">{{$item->pekerjaan}}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        {!! Form::hidden('pekerjaan_id',@$permohonan_peralatan->id) !!}
                                                        <input type="text" class="form-control" disabled value="{{@$permohonan_peralatan->pekerjaan->pekerjaan}}">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIP Pemohon</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="nip_pemohon" value="{{@$permohonan_peralatan->nip_pemohon}}" class="form-control form-white form-white form-white" placeholder="NIP Pemohon" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Pemohon</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="nama_pemohon" value="{{@$permohonan_peralatan->nama_pemohon}}" class="form-control form-white form-white form-white" placeholder="Nama Pemohon" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Pemakaian</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_pemakaian"
                                                    type="text" value="{{@$permohonan_peralatan->tanggal_pemakaian}}"
                                                    class="form-control b-datepicker form-white"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en"
                                                    data-RTL="false"
                                                    placeholder="Tanggal Pemakaian" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Lokasi Inspeksi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="lokasi" value="{{@$permohonan_peralatan->lokasi_inspeksi}}" class="form-control form-white form-white" placeholder="Lokasi Inspeksi" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Maksud Perjalanan Dinas</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="maksud_perjalanan" value="{{@$permohonan_peralatan->maksud_perjalanan}}" class="form-control form-white form-white" placeholder="Maksud Perjalanan Dinas" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Keterangan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea name="keterangan" class="form-control form-white form-white form-white" rows="4"
                                                    placeholder="Keterangan" required
                                                    >{{@$permohonan_peralatan->keterangan}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <!-- list peralatan -->
                                <div class="nav-tabs2" id="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                List Peralatan
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content bg-white">
                                        <table id="my_table" class="table table-hover table-dynamic">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Alat</th>
                                                    <th>Merk</th>
                                                    <th>Type Model</th>
                                                    <th>Nomor Seri</th>
                                                    <th>Status</th>
                                                    <th>Pilih</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php  $no =1;?>
                                                @foreach ($peralatan as $key => $item)
                                                    <tr>
                                                        <td>{{$no}}</td>
                                                        <td>{{@$item->nama_alat}}</td>
                                                        <td>{{@$item->merk_alat}}</td>
                                                        <td>{{@$item->tipe_alat}}</td>
                                                        <td>{{@$item->nomor_seri}}</td>
                                                        <td>{{@$item->status->nama_reference}}</td>
                                                        <td>
                                                            <input name="peralatan[]" {{(in_array(array('peralatan_id'=>$item->id),$pekerjaan_peralatan)) ? "checked" : ""}} type="checkbox" class="form-control" value="{{$item->id}}">
                                                        </td>
                                                    </tr>
                                                    <?php $no++?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br><br>
                                <hr/>
                                <div class="panel-footer clearfix bg-white">
                                    <div class="pull-right">
                                        <a href="{{ url('internal/pekerjaan/peralatan') }}"
                                        class="btn btn-warning btn-square btn-embossed">Batal
                                        &nbsp;<i class="icon-ban"></i></a>
                                        <button type="submit"
                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                        class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

            <script>
            // $(document).ready(function () {
            //     var oTable = $('#my_table').dataTable();
            //     // Sort immediately with columns 0 and 1
            //     oTable.fnSort([[2, 'desc']]);
            // });

            </script>
        @endsection
