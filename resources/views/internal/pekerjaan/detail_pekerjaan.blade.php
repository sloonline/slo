@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_wbs = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Pekerjaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pekerjaan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#personil" data-toggle="tab"><i class="icon-user"></i>
                                            Personil
                                        </a>
                                    </li>
                                    <li id="li_peralatan" class="disabled">
                                        <a href="#peralatan"><i class="icon-user"></i>
                                            Peralatan
                                        </a>
                                    </li>
                                    <li id="li_laporan_inspeksi" class="disabled">
                                        <a href="#laporan_inspeksi"><i class="icon-user"></i>
                                            Laporan Inspeksi
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">WBS/IO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->wbs_io->nomor}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Permohonan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->permohonan->nomor_permohonan}}"
                                                           disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Mulai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->tgl_mulai_pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Selesai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->tgl_selesai_pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control" rows="4"
                                                              name="uraian_pekerjaan"
                                                              placeholder="Uraian Pekerjaan"
                                                              disabled="">{{$pekerjaan->uraian_pekerjaan}}</textarea>
                                                </div>
                                            </div>
                                            @if(\App\Pekerjaan::isLastApproval($pekerjaan))
                                                <div class="form-group col-lg-9"
                                                     style="border-bottom: #EEEEEE 1px solid;">
                                                    <div class="col-sm-4">
                                                        <label style="font-size: medium"
                                                               class="col-sm-12 control-label">Surat Tugas</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <a href="#modal-surat-penugasan" data-toggle="modal"
                                                           class="btn btn-warning  pull-right"><i
                                                                    class="fa fa-pencil"></i> Edit Surat </a>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">No Surat Tugas</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->no_surat_penugasan}}" disabled>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->tgl_surat_penugasan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Surat Tugas</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    @if($pekerjaan->file_surat_penugasan != null)
                                                        <a href="{{url('upload/'.$pekerjaan->file_surat_penugasan)}}"
                                                           class="btn btn-primary"><i class="fa fa-download"></i>
                                                            Download Surat Penugasan</a>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12" style="display:none" id="progress">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label"><h1><b>Progress</b> Pekerjaan
                                                        </h1></label>
                                                </div>
                                                {!! Form::open(array('url'=> '/internal/update_progress_pekerjaan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                                {!! Form::hidden('pekerjaan_id', @$pekerjaan_id) !!}
                                                <div class="col-sm-2" id="progress_pekerjaan">
                                                    <label class="col-sm-12 control-label" id="progress_pekerjaan">
                                                        <h2>{{$pekerjaan->progress}} %</h2></label>
                                                    @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PROGRESS_PEKERJAAN))
                                                        @if(@$pekerjaan->status_pekerjaan == null)
                                                            <button class="btn btn-warning" id="btn_update"><i
                                                                        class="fa fa-pencil"></i> Update Progress
                                                            </button>
                                                        @endif
                                                    @endif
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="personil">

                                    </div>
                                    <div class="tab-pane" id="peralatan">
                                        <!-- permohonan peminjaman -->
                                        <div class="nav-tabs2" id="tabs">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                        Permohonan Peminjaman
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content bg-white">
                                                <div class="tab-pane active" id="general">
                                                    <div class="panel-content pagination2 table-responsive">
                                                        @if(@$pekerjaan->status_pekerjaan == null)
                                                            <div class="m-b-20 border-bottom">
                                                                <div class="btn-group">
                                                                    <a href="{{url('internal/create_pekerjaan_peralatan')}}"
                                                                       class="btn btn-success btn-square btn-block btn-embossed"><i
                                                                                class="fa fa-plus"></i> Permohonan Baru</a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <table id="my_table" class="table table-hover table-dynamic">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Peminjam</th>
                                                                <th>Pekerjaan</th>
                                                                <th>Tanggal Pinjam</th>
                                                                <th>Status</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <?php
                                                            $no = 1;
                                                            ?>
                                                            <tbody>
                                                            @foreach ($permohonan_peralatan as $key => $item)
                                                                <tr>
                                                                    <td>{{$no}}</td>
                                                                    <td>{{$item->nama_pemohon}}</td>
                                                                    <td>{{$item->pekerjaan->pekerjaan}}</td>
                                                                    <td>{{$item->tanggal_pemakaian}}</td>
                                                                    <td>{{$item->flow_status->status}}</td>
                                                                    <td>
                                                                        <a href="{{url('internal/detail_pekerjaan_peralatan/'.$item->id)}}"
                                                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                    class="fa fa-eye"
                                                                                    data-rel="tooltip"
                                                                                    data-placement="right"
                                                                                    title="Detail Permohonan Peralatan"></i></a>
                                                                    </td>
                                                                </tr>
                                                                <?php $no++?>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="laporan_inspeksi">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kick Off Meeting</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('/internal/kick_off')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Kick Off Meeting</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Data Teknik</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('/internal/data_teknik')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Data Teknik</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Blangko Inspeksi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/blangko_inspeksi')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Blangko Inspeksi</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Berita Acara Inspeksi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/berita_inspeksi')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Berita Acara Inspeksi</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">HILO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/hilo')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> HILO</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pending Item</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/pending_item')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Pending Item</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Permohonan RLB</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/permohonan_rlb')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Permohonan RLB</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Permohonan RLS</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="{{url('internal/permohonan_rls')}}" target="_blank"
                                                       class="btn btn-success ladda-button btn-square btn-embossed"><i
                                                                class="fa fa-file-text-o"></i> Permohonan RLS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @include('workflow_view')
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- BEGIN MODAL SURAT PENUGASAN-->
                    <div class="modal fade" id="modal-surat-penugasan" tabindex="-1" role="dialog"
                         aria-hidden="true">
                        <div class="modal-dialog" style="width: 700px;">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color: teal;color:white;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                                class="icons-office-52"></i></button>
                                    <h4 class="modal-title"><strong>Surat</strong> Tugas</h4>
                                </div>
                                {!! Form::open(array('url'=> '/internal/save_surat_penugasan_pekerjaan', 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-surat','enctype'=>"multipart/form-data")) !!}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            {!! Form::hidden('id', @$pekerjaan->id) !!}
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-8">
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="{{url('internal/print_form_pekerjaan/'.@$pekerjaan->id)}}"
                                                       class="btn btn-warning"><i class="fa fa-download"></i> Download
                                                        Template</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor surat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control form-white" required
                                                           name="nomor_surat"
                                                           value="{{@$pekerjaan->no_surat_penugasan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Surat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_surat_penugasan"
                                                           type="text"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="dd-mm-yyyy"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           value="{{@$pekerjaan->tgl_surat_penugasan}}"
                                                           placeholder="Tanggal" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File Surat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="file_surat_penugasan" type="file"
                                                           class="form-control form-white"
                                                           placeholder="Pekerjaan" {{($pekerjaan->file_surat_penugasan != null) ? "" : "required"}}>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                </div>
                                                <div class="col-sm-6">
                                                    @if($pekerjaan->file_surat_penugasan != null)
                                                        <a href="{{url('upload/'.$pekerjaan->file_surat_penugasan)}}"
                                                           class="btn btn-primary"><i class="fa fa-download"></i>
                                                            Download Surat Tugas</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="pull-right">
                                        <button onclick="$('#modal-surat-penugasan').modal('hide')"
                                                class="btn btn-warning btn-square btn-embossed">Batal
                                            &nbsp;<i class="icon-ban"></i></button>
                                        <button type="submit"
                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                data-style="zoom-in">Simpan &nbsp;<i
                                                    class="glyphicon glyphicon-floppy-saved"></i>
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- END MODAL SURAT PENUGASAN-->
                    @endsection


                    @section('page_script')
                        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                        <!-- Tables Filtering, Sorting & Editing -->
                        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                        <!-- Buttons Loading State -->

                        <script>

                            //                $( document ).ready(function(){
                            generatePersonil();

                            if ("{{$isLastApproval}}") {
                                $('#progress').show();
                                $('#li_peralatan').removeClass('disabled');
                                $('#li_peralatan a').attr("data-toggle", "tab");

                                $('#li_laporan_inspeksi').removeClass('disabled');
                                $('#li_laporan_inspeksi a').attr("data-toggle", "tab");

                                if ("{{@$laporan_pekerjaan->id}}" != "") {
                                    $("#form_laporan :input").prop("disabled", true);
                                    $('#laporan_save').hide();
                                }
                            }
                            //                });

                            function generatePersonil() {
                                $("#personil").html("");
                                var personil = "";
                                @foreach ($jabatan as $key => $item)
                                        personil += "<div class='nav-tabs2' id='tabs'>" +
                                        "<ul class='nav nav-tabs'>" +
                                        "<li class='active'>" +
                                        "<a href='#general' data-toggle='tab'><i class='icon-plus'></i>" +
                                        "{{@$item->nama_jabatan}}" +
                                        "</a>" +
                                        "</li>" +
                                        "</ul>" +
                                        "<div class='tab-content bg-white'>" +
                                        "<table id='my_table' class='table table-hover table-dynamic'>" +
                                        "<thead>" +
                                        "<tr>" +
                                        "<th>No</th>" +
                                        "<th>NIP</th>" +
                                        "<th>Nama</th>" +
                                        "<th>Status Pekerjaan</th>" +
                                        "<th>Bidang</th>" +
                                        "<th>Sub Bidang</th>" +
                                        "<th>Grade</th>" +
                                        "<th>Pilih</th>" +
                                        "</tr>" +
                                        "</thead>" +
                                        "<tbody>" +
                                        <?php $no = 1;?>
                                                @foreach ($item->personil as $key => $personil)
                                        <?php
                                                $isChecked = "";
                                                foreach($pekerjaan_personil as $p){
                                                if($p->jabatan_id == $item->id && $p->personil_id == $personil->id){
                                                $isChecked = "checked";
                                                ?>
                                                "<tr>" +
                                        "<td>{{$no}}</td>" +
                                        "<td>{{@$personil->nip}}</td>" +
                                        "<td>{{@$personil->nama}}</td>" +
                                        "<td>{{@$personil->status_pekerja->status}}</td>" +
                                        "<td>{{@$personil->bidang->nama_bidang}}</td>" +
                                        "<td>{{@$personil->sub_bidang->nama_sub_bidang}}</td>" +
                                        "<td>{{@$personil->grade}}</td>" +
                                        "<td>" +
                                        "<input disabled type='checkbox' name='pilih_personil_{{$item->id}}[]' value='{{$personil->id}}' class='form-control form-white' {{$isChecked}}>" +
                                        "</td>" +
                                        "</tr>" +
                                        <?php
                                                break;
                                                }
                                                }
                                                $no++;
                                                ?>
                                                @endforeach
                                                "</tbody>" +
                                        "</table>" +
                                        "</div>" +
                                        "</div>" +
                                        "<br><br>"
                                @endforeach
                                $("#personil").html(personil);
                            }

                            $('#btn_update').click(function () {
                                var field_update = "<input type='number' min='{{$pekerjaan->progress}}' max='100' name='progress' class='form-control form-white' value='{{$pekerjaan->progress}}'><br><br>" +
                                        "<button class='btn btn-success'><i class='fa fa-paper-plane'></i> Update Progress</button>";
                                $('#progress_pekerjaan').html(field_update);
                            });


                        </script>
@endsection
