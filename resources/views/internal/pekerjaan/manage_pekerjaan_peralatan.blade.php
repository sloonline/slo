@extends('../layout/layout_internal')

@section('page_css')
  <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
<?php
$allowManage = \App\PermohonanPeralatan::isAllowManagePeralatan($permohonan_peralatan);
?>
@section('content')
  <?php
  $tmp_wbs = null;
  ?>
  <div class="page-content">
    <div class="header">
      <h2>Data <strong>Permohonan Peralatan</strong></h2>
      <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
          <li><a href="{{url('/internal')}}">Dashboard</a></li>
          <li class="active">Peralatan</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        @if(Session::has('message'))
          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
          </div>
        @endif
        <div class="panel border">
          <div class="panel">
            <div class="panel-content">
              {!! Form::open(array('url'=> '/internal/manage_pekerjaan_peralatan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
              {!! Form::hidden('id',@$permohonan_peralatan->id) !!}
              <div class="nav-tabs2" id="tabs">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                      Permohonan Peminjaman
                    </a>
                  </li>
                </ul>
                <div class="tab-content bg-white">
                  <div class="tab-pane active" id="general">
                    <div class="panel-content pagination2 table-responsive">
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Pekerjaan</label>
                        </div>
                        <div class="col-sm-6">
                          {!! Form::hidden('pekerjaan_id',@$permohonan_peralatan->pekerjaan_id) !!}
                          <input type="text" class="form-control" disabled value="{{@$permohonan_peralatan->pekerjaan->pekerjaan}}">
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">NIP Pemohon</label>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" disabled  name="nip_pemohon" value="{{@$permohonan_peralatan->nip_pemohon}}" class="form-control" placeholder="NIP Pemohon" required>
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Nama Pemohon</label>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" disabled name="nama_pemohon" value="{{@$permohonan_peralatan->nama_pemohon}}" class="form-control" placeholder="Nama Pemohon" required>
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Tanggal Pemakaian</label>
                        </div>
                        <div class="col-sm-6">
                          <input name="tgl_pemakaian"
                          type="text" value="{{@$permohonan_peralatan->tanggal_pemakaian}}"
                          class="form-control b-datepicker"
                          data-date-format="dd-mm-yyyy"
                          data-lang="en" disabled
                          data-RTL="false"
                          placeholder="Tanggal Pemakaian" required>
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Lokasi Inspeksi</label>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" disabled name="lokasi" value="{{@$permohonan_peralatan->lokasi_inspeksi}}" class="form-control" placeholder="Lokasi Inspeksi" required>
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Maksud Perjalanan Dinas</label>
                        </div>
                        <div class="col-sm-6">
                          <input type="text" disabled name="maksud_perjalanan" value="{{@$permohonan_peralatan->maksud_perjalanan}}" class="form-control" placeholder="Maksud Perjalanan Dinas" required>
                        </div>
                      </div>
                      <div class="form-group col-lg-12">
                        <div class="col-sm-3">
                          <label class="col-sm-12 control-label">Keterangan</label>
                        </div>
                        <div class="col-sm-6">
                          <textarea name="keterangan" class="form-control" rows="4"
                          placeholder="Keterangan" disabled
                          >{{@$permohonan_peralatan->keterangan}}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <br><br>
                <div class="nav-tabs2" id="tabs">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                        History Peralatan
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content bg-white">
                    <table id="my_table" class="table table-hover table-dynamic">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Alat</th>
                          <th>Merk</th>
                          <th>Type Model</th>
                          <th>Nomor Seri</th>
                          <th>Keterangan</th>
                          <th>Status</th>
                          <th>Tanggal</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  $no =1;?>
                        @foreach (@$history_peralatan as $item)
                          <tr>
                            <td>{{$no}}</td>
                            <td>{{@$item->peralatan->nama_alat}}</td>
                            <td>{{@$item->peralatan->merk_alat}}</td>
                            <td>{{@$item->peralatan->tipe_alat}}</td>
                            <td>{{@$item->peralatan->nomor_seri}}</td>
                            <td>{{@$item->keterangan}}</td>
                            <td>{{@$item->status_peralatan->nama_reference}}</td>
                            <td>{{date('d M Y H:i:s',strtotime(@$item->created_at))}}</td>
                          </tr>
                          <?php $no++?>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <br><br>
                <!-- list peralatan -->
                <div class="nav-tabs2" id="tabs">
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                        List Peralatan
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content bg-white">
                    <table id="my_table" class="table table-hover table-dynamic">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Alat</th>
                          <th>Merk</th>
                          <th>Type Model</th>
                          <th>Nomor Seri</th>
                          <th>Keterangan</th>
                          <th>Status</th>
                          <th>Pilih</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  $no =1;  ?>
                        @foreach (@$permohonan_peralatan->pekerjaan_peralatan as $item)
                          <?php
                          $optionStatus = \App\Peralatan::getOptionStatusForInspeksi(@$item->status_peralatan->nama_reference);
                          ?>
                          <tr>
                            <td>{{$no}}</td>
                            <td>{{@$item->peralatan->nama_alat}}</td>
                            <td>{{@$item->peralatan->merk_alat}}</td>
                            <td>{{@$item->peralatan->tipe_alat}}</td>
                            <td>{{@$item->peralatan->nomor_seri}}</td>
                            @if($optionStatus->count() > 0)
                              <td><input type="text" disabled id="keterangan_{{$item->id}}" name="keterangan_{{$item->id}}" class="form-control form-white" placeholder="keterangan peralatan"/></td>
                              <td>
                                <select disabled name="status_peralatan_{{$item->id}}" id="status_peralatan_{{$item->id}}" class="form-control form-white">
                                  @foreach($optionStatus as $status)
                                    <option value="{{@$status->id}}">{{@$status->nama_reference}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td>
                                <input name="peralatan[]" id="check_{{$item->id}}" type="checkbox" class="form-control" value="{{$item->id}}">
                              </td>
                            @else
                              <td>{{@$item->keterangan}}</td>
                              <td>{{@$item->status_peralatan->nama_reference}}</td>
                              <td>-</td>
                            @endif
                          </tr>
                          <?php $no++?>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <br><br>
                {{--List Surat Peminjaman & Surat Pengembalian--}}
                @if($allowManage)
                    <div class="nav-tabs2" id="tabs">
                      <ul class="nav nav-tabs">
                        <li class="active">
                          <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                            Surat Peminjaman & Pengembalian
                          </a>
                        </li>
                      </ul>
                      <div class="tab-content bg-white">
                            <div class="col-sm-3 pull-right">
                                <div class="col-sm-12 prepend-icon">
                                    <a href="{{url('/internal/print_form_peralatan/'.@$permohonan_peralatan->id)}}">
                                        <button type="button"
                                                class="col-lg-12 btn btn-success">
                                            <center><i
                                                        class="fa fa-print"></i>Download Form Peralatan
                                            </center>
                                        </button>
                                    </a>
                                </div>
                            </div>
                          <br/><br/><hr/>
                          <div class="form-group col-lg-12">
                              <div class="col-sm-3">
                                  <label class="col-sm-12 control-label">File Permohonan Peralatan</label>
                              </div>
                              <div class="col-sm-3">
                                <input type="file" class="form-control" name="file_permohonan" id="file_permohonan" />
                               </div>
                              <div class="col-sm-3">
                                  @if(@$permohonan_peralatan->file_permohonan != null)
                                      <div class="col-sm-12 prepend-icon">
                                              <a target="_blank"
                                                 href="{{url('upload/'.@$permohonan_peralatan->file_permohonan)}}">
                                                  <button type="button"
                                                          class="col-lg-12 btn btn-primary">
                                                      <center><i
                                                                  class="fa fa-download"></i>Preview
                                                          File
                                                      </center>
                                                  </button>
                                              </a>
                                      </div>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group col-lg-12">
                              <div class="col-sm-3">
                                  <label class="col-sm-12 control-label">File Pengembalian Peralatan</label>
                              </div>
                              <div class="col-sm-3">
                                  <input type="file" class="form-control" name="file_pengembalian" id="file_pengembalian" />
                              </div>
                              <div class="col-sm-3">
                                  @if(@$permohonan_peralatan->file_pengembalian != null)
                                      <div class="col-sm-12 prepend-icon">
                                          <a target="_blank"
                                             href="{{url('upload/'.@$permohonan_peralatan->file_pengembalian)}}">
                                              <button type="button"
                                                      class="col-lg-12 btn btn-primary">
                                                  <center><i
                                                              class="fa fa-download"></i>Preview
                                                      File
                                                  </center>
                                              </button>
                                          </a>
                                      </div>
                                  @endif
                              </div>
                          </div>
                      </div>
                    </div>
                    <br><br>
                @endif
                <hr/>
                <div class="panel-footer clearfix bg-white">
                  <div class="pull-right">
                    <a href="{{ url('internal/pekerjaan/peralatan') }}"
                    class="btn btn-warning btn-square btn-embossed">Batal
                    &nbsp;<i class="icon-ban"></i></a>
                    <button type="submit"
                    class="btn btn-success ladda-button btn-square btn-embossed"
                    data-style="zoom-in">Simpan &nbsp;<i
                    class="glyphicon glyphicon-floppy-saved"></i>
                  </button>
                </div>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
  @endsection


    @section('page_script')
      <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
      <!-- Tables Filtering, Sorting & Editing -->
      <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
      <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
      <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
      <!-- Buttons Loading State -->

      <script>
      // $(document).ready(function () {
      //     var oTable = $('#my_table').dataTable();
      //     // Sort immediately with columns 0 and 1
      //     oTable.fnSort([[2, 'desc']]);
      // });
      // For oncheck callback
      @if($allowManage)
      setFormPengembalian();
      $('input[name*="peralatan[]"]').on('ifChecked', function () {
        setFormPengembalian();
      });

      $('input[name*="peralatan[]"]').on('ifUnchecked', function () {
        setFormPengembalian();
      });

      function setFormPengembalian(){
        $("input[name*='keterangan_']").attr("disabled", true);
        $("select[name*='status_peralatan_']").attr("disabled", true);
        for(var i = 0 ; i < $("input[name*='peralatan[]']:checked").length ; i ++){
          var id = $("input[name*='peralatan[]']:checked")[i].value;
          $("#keterangan_" + id).removeAttr('disabled');
          $("#status_peralatan_" + id).removeAttr('disabled');
        }
      }
      @endif
      </script>
    @endsection
