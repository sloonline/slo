@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_wbs = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Pekerjaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pekerjaan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> '/internal/save_pekerjaan', 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-pekerjaan')) !!}
                            {!! Form::hidden('id', @$pekerjaan_id) !!}
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_PENUGASAN_PERSONIL))
                                        <li id="li_personil" class="disabled">
                                            <a href="#personil"><i class="icon-user" class="disabled"></i>
                                                Personil
                                            </a>
                                        </li>
                                    @endif
                                    <li style="display:none">
                                        <a href="#peralatan" data-toggle="tab"><i class="icon-user"></i>
                                            Peralatan
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">WBS/IO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select data-search="true"  name="wbs_io" class="form-control form-white" id="wbs"
                                                            required>
                                                        <option>- Pilih WBS/IO -</option>
                                                        <?php $count = 0;?>
                                                        @foreach(@$wbs as $item)
                                                            @if(sizeof($item['permohonan']) > 0)
                                                                <option value="{{$item['wbsio_id']}}">{{$item['nomor_wbsio'] ." ( " .@$item['uraian_pekerjaan']." ) "}}
                                                                    @if(@$item['kontrak'] != null)
                                                                        {{"[".@$item['kontrak']->latest_kontrak->nomor_kontrak." , ".@$item['kontrak']->perusahaan->nama_perusahaan."]"}}
                                                                    @elseif(@$item['order'] != null)
                                                                        {{"[".@$item['order']->nomor_order." , ".@$item['order']->perusahaan->nama_perusahaan."]"}}
                                                                    @endif
                                                                </option>
                                                                <?php $count++;?>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @if($count == null)
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="alert alert-danger">
                                                            <a href="#" class="close" data-dismiss="alert"
                                                               aria-label="close">&times;</a>
                                                            <i class="fa fa-exclamation-triangle"></i>
                                                            Maaf tidak ada WBS yang tersedia,
                                                            silahkan menuju <a target="_blank"
                                                                               href="{{url('/internal/show_wbs_io')}}"><b>Modul
                                                                    WBS/IO</b></a> untuk create WbS/IO terlebih dahulu
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Permohonan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="permohonan" class="form-control form-white"
                                                            id="permohonan" required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input id="pekerjaan" name="pekerjaan" type="text"
                                                           class="form-control form-white" placeholder="Pekerjaan"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Mulai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_mulai" type="text" class="form-control"
                                                           placeholder="Tanggal Mulai Pekerjaan" readonly required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Selesai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_selesai" type="text" class="form-control"
                                                           placeholder="Tanggal Selesai Pekerjaan" readonly required>
                                                </div>
                                            </div>
                                            {{--<div class="form-group col-lg-12">--}}
                                            {{--<div class="col-sm-3">--}}
                                            {{--<label class="col-sm-12 control-label">No Surat Penugasan</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-4">--}}
                                            {{--<input name="no_surat_penugasan" type="text" class="form-control form-white" placeholder="Nomor Surat Penugasan" required>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-2">--}}
                                            {{--<input name="tgl_surat_penugasan"--}}
                                            {{--type="text"--}}
                                            {{--class="form-control b-datepicker form-white"--}}
                                            {{--data-date-format="dd-mm-yyyy"--}}
                                            {{--data-lang="en"--}}
                                            {{--data-RTL="false"--}}
                                            {{--placeholder="Tanggal"required>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group col-lg-12">--}}
                                            {{--<div class="col-sm-3">--}}
                                            {{--<label class="col-sm-12 control-label">File Surat Penugasan</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-sm-6">--}}
                                            {{--<input name="file_surat_penugasan" type="file" class="form-control form-white" placeholder="Pekerjaan" required>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control form-white form-white" rows="4"
                                                              name="uraian_pekerjaan"
                                                              placeholder="Uraian Pekerjaan"
                                                              required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="personil">

                                    </div>
                                    <div class="tab-pane" id="peralatan" style="display:none">
                                        <!-- permohonan peminjaman -->
                                        <div class="nav-tabs2" id="tabs">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                        Permohonan Peminjaman
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content bg-white">
                                                <div class="tab-pane active" id="general">
                                                    <div class="panel-content pagination2 table-responsive">
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">NIP
                                                                    Pemohon</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" name="nip_pemohon"
                                                                       class="form-control form-white form-white form-white"
                                                                       placeholder="NIP Pemohon">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Nama
                                                                    Pemohon</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" name="nama_pemohon"
                                                                       class="form-control form-white form-white form-white"
                                                                       placeholder="Nama Pemohon">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Tanggal
                                                                    Pemakaian</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text"
                                                                       class="form-control form-white form-white"
                                                                       placeholder="Tanggal Pemakaian">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Lokasi
                                                                    Inspeksi</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text"
                                                                       class="form-control form-white form-white"
                                                                       placeholder="Lokasi Inspeksi">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Maksud Perjalanan
                                                                    Dinas</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text"
                                                                       class="form-control form-white form-white"
                                                                       placeholder="Maksud Perjalanan Dinas">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Keterangan</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <textarea
                                                                        class="form-control form-white form-white form-white"
                                                                        rows="4"
                                                                        placeholder="Keterangan"
                                                                ></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br><br>
                                            <!-- list peralatan -->
                                            <div class="nav-tabs2" id="tabs">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#general" data-toggle="tab"><i class="icon-plus"></i>
                                                            List Peralatan
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content bg-white">
                                                    <table id="my_table" class="table table-hover table-dynamic">
                                                        <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Nama Alat</th>
                                                            <th>Kode Alat</th>
                                                            <th>Merk</th>
                                                            <th>Type Model</th>
                                                            <th>Nomor Seri</th>
                                                            <th>Keterangan</th>
                                                            <th>Status</th>
                                                            <th>Pilih</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <br><br>
                                            <hr/>
                                            <div class="panel-footer clearfix bg-white">
                                                <div class="pull-right">
                                                    <a href="{{ url('internal/pekerjaan') }}"
                                                       class="btn btn-warning btn-square btn-embossed">Batal
                                                        &nbsp;<i class="icon-ban"></i></a>
                                                    <button type="submit"
                                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                                            data-style="zoom-in">Simpan &nbsp;<i
                                                                class="glyphicon glyphicon-floppy-saved"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="panel-footer clearfix bg-white">
                                    <div class="pull-right">
                                        <a href="{{ url('internal/pekerjaan') }}"
                                           class="btn btn-warning btn-square btn-embossed">Batal
                                            &nbsp;<i class="icon-ban"></i></a>
                                        <button type="submit"
                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                data-style="zoom-in">Simpan &nbsp;<i
                                                    class="glyphicon glyphicon-floppy-saved"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            @endsection


            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- Buttons Loading State -->

                <script>
                    // $(document).ready(function () {
                    //     var oTable = $('#my_table').dataTable();
                    //     // Sort immediately with columns 0 and 1
                    //     oTable.fnSort([[2, 'desc']]);
                    // });
                    $(function () {
                        $('.tab-pane input, .tab-pane textarea').on('invalid', function () {

                            // Find the tab-pane that this element is inside, and get the id
                            var $closest = $(this).closest('.tab-pane');
                            var id = $closest.attr('id');

                            // Find the link that corresponds to the pane and have it show
                            $('.nav a[href="#' + id + '"]').tab('show');

                        });
                    });
                    $("#wbs").change(function () {
                        generatePermohonan();
                    });

                    function generatePermohonan() {
                        $("#permohonan").html("");
                        var wbs = $("#wbs option:selected").val();
                        @foreach(@$wbs as $item)
                        if ("{{$item['wbsio_id']}}" == wbs) {
                            var permohonan_html = "<option value='0'>-</option>"
                            @if(sizeof($item['permohonan']) > 0)
                                    @foreach($item['permohonan'] as $key => $value)
                                     <?php $bayGardu = ($value->permohonan == null) ? null : \App\Permohonan::getBay($value->permohonan) ;?>
                                    @if($bayGardu == null)
                                       permohonan_html += "<option value='{{@$value->permohonan->id}}'>{{@$value->permohonan->nomor_permohonan.' - '.@$value->permohonan->instalasi->nama_instalasi .' ('.@$value->permohonan->produk->produk_layanan.')'}}</option>";
                                    @else
                                            permohonan_html += "<option value='{{@$value->permohonan->id}}'>{{@$value->permohonan->nomor_permohonan.' - '.@$bayGardu->nama_bay .' ('.@$value->permohonan->produk->produk_layanan.')'}}</option>";
                                    @endif
                        @endforeach
                        @else
                            alert('Tidak ada permohonan yang tersedia!');
                            @endif
                            <?php
                            $tmp_wbs = $item['jadwal_pekerjaan'];
                            ?>

                        }
                        $("#permohonan").html(permohonan_html);
                        @endforeach
                        $("#permohonan").val("0").change();
                    }

                    $("#permohonan").change(function () {
                        // console.log($('#permohonan option:selected').val());
                        $("#pekerjaan").val($('#permohonan option:selected').text());
                        $("#personil").html("");
                        var personil = "";
                        // check whether permohonan had selected or not
                        if ($('#permohonan option:selected').val() != 0) {

                            $('#li_personil').removeClass('disabled');
                            $('#li_personil a').attr("data-toggle", "tab");

                            // set tgl mulai & tgl selesai
                            $("input[name=tgl_mulai]").val("{{@$tmp_wbs->tanggal_mulai}}");
                            $("input[name=tgl_selesai]").val("{{@$tmp_wbs->tanggal_selesai}}");

                            @foreach (@$jabatan as $key => $item)
                            <?php
                                    $isReuired = "";
                                    //                if($item->nama_jabatan == PELAKSANA_INSPEKSI){
                                    //                    $isReuired = "required";
                                    //                }
                                    ?>
                                    personil += "<div class='nav-tabs2' id='tabs'>" +
                                    "<ul class='nav nav-tabs'>" +
                                    "<li class='active'>" +
                                    "<a href='#general' data-toggle='tab'><i class='icon-plus'></i>" +
                                    "{{@$item->nama_jabatan}}" +
                                    "</a>" +
                                    "</li>" +
                                    "</ul>" +
                                    "<div class='tab-content bg-white'>" +
                                    "<table id='{{str_replace(' ','_',$item->nama_jabatan)}}' class='table table-hover table-dynamic'>" +
                                    "<thead>" +
                                    "<tr>" +
//                                    "<th>No</th>"+
                                    "<th>NIP</th>" +
                                    "<th>Nama</th>" +
                                    "<th>Status Pekerjaan</th>" +
                                    "<th>Bidang</th>" +
                                    "<th>Sub Bidang</th>" +
                                    "<th>Grade</th>" +
                                    "<th>Kompetensi</th>" +
                                    "<th>Pilih</th>" +
                                    "</tr>" +
                                    "</thead>" +
                                    "<tbody>" +
                                    <?php $no = 1;?>
                                            @foreach ($item->personil as $key => $personil)
                                            @if($item->nama_jabatan == PELAKSANA_INSPEKSI)
                                            @if(!$personil->isBooked($personil->id, @$tmp_wbs->tanggal_mulai))
                                            "<tr>" +
                                    {{--"<td>{{$no++}}</td>"+--}}
                                            "<td>{{@$personil->nip}}</td>" +
                                    "<td>{{@$personil->nama}}</td>" +
                                    "<td>{{@$personil->status_pekerja->status}}</td>" +
                                    "<td>{{@$personil->bidang->nama_bidang}}</td>" +
                                    "<td>" +
                                    @foreach($personil->personil_bidang as $row)
                                            "{{@$row->sub_bidang->nama_sub_bidang}}<br/>" +
                                    @endforeach
                                            "</td>" +
                                    "<td>{{@$personil->grade}}</td>" +
                                    "<td>" +
                                    @foreach($personil->keahlian as $row)
                                            "{{@$row->keahlian->nama_keahlian}}<br/>" +
                                    @endforeach
                                            "</td>" +
                                    "<td>" +
                                    "<input type='checkbox' name='pilih_personil_{{$item->id}}[]' value='{{$personil->id}}' class='form-control' {{$isReuired}}>" +
                                    "</td>" +
                                    "</tr>" +
                                    @endif
                                            @else
                                            "<tr>" +
                                    {{--"<td>{{$no}}</td>"+--}}
                                            "<td>{{@$personil->nip}}</td>" +
                                    "<td>{{@$personil->nama}}</td>" +
                                    "<td>{{@$personil->status_pekerja->status}}</td>" +
                                    "<td>{{@$personil->bidang->nama_bidang}}</td>" +
                                    "<td>" +
                                    @foreach($personil->personil_bidang as $row)
                                            "{{@$row->sub_bidang->nama_sub_bidang}}<br/>" +
                                    @endforeach
                                            "</td>" +
                                    "<td>{{@$personil->grade}}</td>" +
                                    "<td>" +
                                    @foreach($personil->keahlian as $row)
                                            "{{@$row->keahlian->nama_keahlian}}<br/>" +
                                    @endforeach
                                            "</td>" +
                                    "<td>" +
                                    "<input type='checkbox' name='pilih_personil_{{$item->id}}[]' value='{{$personil->id}}' class='form-control' {{$isReuired}}>" +
                                    "</td>" +
                                    "</tr>" +
                                    @endif
                                    <?php $no++?>
                                            @endforeach
                                            "</tbody>" +
                                    "</table>" +
                                    "</div>" +
                                    "</div>" +
                                    "<br><br>"
                                $("#{{str_replace(' ','_',$item->nama_jabatan)}}").dataTable();
                            @endforeach
                        }
                        $("#personil").html(personil);

                        if ($('#permohonan option:selected').val() != 0) {
                            @foreach (@$jabatan as $key => $item)
                             $("#{{str_replace(' ','_',$item->nama_jabatan)}}").dataTable();
                            @endforeach
                        }

                    });


                    $("#form-pekerjaan").submit(function () {
                        if ($('#{{str_replace(' ','_',PELAKSANA_INSPEKSI)}}  input:checked').length == 0) {
                            alert('Salah satu pelaksana inspeksi harus dipilih');
                            return false;
                        } else {
                            return true;
                        }
                    });
                </script>
@endsection
