@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/multi-select/css/pickList.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>Approval <strong>Regitrasi Peminta Jasa</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="#">Approval</a></li>
				<li class="active">Internal Pusertif</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<p class="m-t-10 m-b-20 f-16">Silahkan inputkan data pemohon dan data trasnmisi yang hendak diajukan dalam permohonan ini.</p>
			<div class="panel">
				<div class="panel-header panel-controls bg-primary">
					<h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
				</div>

				<div class="panel-body">

					<div id="pickList"></div>

					<br><br>
					<button class="btn btn-primary" id="getSelected">Get Selected</button>
				</div>

			</div>
		</div>
	</div>
	@endsection


	@section('page_script')
	<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
	<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="{{ url('/') }}/assets/global/plugins/multi-select/js/pickList.js"></script>
	<script>

		var val = {
			01: {id: 01, text: 'Isis'},
			02: {id: 02, text: 'Sophia'},
			03: {id: 03, text: 'Alice'},
			04: {id: 04, text: 'Isabella'},
			05: {id: 05, text: 'Manuela'},
			06: {id: 06, text: 'Laura'},
			07: {id: 07, text: 'Luiza'},
			08: {id: 08, text: 'Valentina'},
			09: {id: 09, text: 'Giovanna'},
			10: {id: 10, text: 'Maria Eduarda'},
			11: {id: 11, text: 'Helena'},
			12: {id: 12, text: 'Beatriz'},
			13: {id: 13, text: 'Maria Luiza'},
			14: {id: 14, text: 'Lara'},
			15: {id: 15, text: 'Julia'}
		};

		var pick = $("#pickList").pickList({data: val});

		$("#getSelected").click(function () {
			console.log(pick.getValues());
		});

	</script>
	@endsection