@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Log <strong>Email</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Log Email</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
                    </div>
                    <div class="panel-content pagination2" style="overflow-x: scroll;overflow-y: hidden;">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Penerima</th>
                                <th>Nama Penerima</th>
                                <th>Subjek</th>
                                <th>Modul</th>
                                <th>Aksi</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th style="width: 200px;">Keterangan Status</th>
                                <th>Waktu Pengiriman</th>
                                <th>Resend</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1 ?>
                            @foreach($logs as $row)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $row->to }}</td>
                                    <td>{{ $row->to_name }}</td>
                                    <td>{{ $row->subject }}</td>
                                    <td>{{ $row->modul }}</td>
                                    <td>{{ $row->action }}</td>
                                    <td>{{ $row->description }}</td>
                                    <td>{{ $row->status }}</td>
                                    <td>{{ $row->status_message }}</td>
                                    <td>{{ $row->created_at }}</td>
                                    <td>
                                        <a href="{{url('/internal/resend_email/'.$row->id)}}" class="btn btn-sm btn-primary btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Resend">Resend</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection