@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Internal <strong>Order</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>WBS/IO</li>
                    <li class="active">Create IO</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> DETAIL <strong>IO</strong></h3>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-content ">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#realisasi" data-toggle="tab"><i class="icon-user"></i>
                                            Realisasi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#pembayaran" data-toggle="tab"><i class="fa fa-dollar"></i>
                                            Pembayaran
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            {!! Form::open(['url'=>'internal/save_wbs_io','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Tipe IO</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <select name="tipe" readonly="readonly" disabled
                                                                class="form-control form-white" required>
                                                            <option value="0">-Pilih Tipe-</option>
                                                            @foreach($ref_tipe as $item)
                                                                <option value="{{$item->id}}" {{(@$io->tipe == $item->id) ? "selected='selected'" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nomor IO</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="text" readonly class="form-control form-white"
                                                               value="{{@$io->nomor}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Deskripsi</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="text" readonly class="form-control form-white"
                                                               value="{{@$io->deskripsi}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nomor Project</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="text" readonly class="form-control form-white"
                                                               value="{{@$io->nomor_project}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Tahun</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="number" min="1900" name="tahun" required readonly
                                                               class="form-control form-white"
                                                               value="{{@$io->tahun}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(@$io->tipeWbs->nama_reference == TIPE_NORMAL)
                                                <div id="div_kontrak">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <label class="col-sm-12 control-label">Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="append-icon">
                                                                <input type="hidden" name="kontrak"
                                                                       value="{{$io->kontrak_id}}"/>
                                                                <input type="text" readonly
                                                                       class="form-control form-white"
                                                                       value="{{@$io->kontrak->latest_kontrak->nomor_kontrak}} - {{@$io->kontrak->perusahaan->nama_perusahaan}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" id="tgl_kontrak"
                                                                   class="form-control form-white"
                                                                   value="{{($io == null) ? "" : date('d M Y',strtotime(@$io->kontrak->latest_kontrak->tgl_kontrak))}}"
                                                                   readonly placeholder="Tgl. Kontrak"/>
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <a href="{{($io == null) ? "#" : url('/internal/detail_kontrak/'.@$io->kontrak_id)}}"
                                                               target="_blank" id="detail_kontrak"
                                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                        class="fa fa-eye"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="div_rab">
                                                        <table id="table-rab" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>NOMOR RAB</th>
                                                                <th>NOMOR ORDER</th>
                                                                <th>INSTALASI</th>
                                                                <th>TANGGAL RAB</th>
                                                                <th>TOTAL BIAYA</th>
                                                                <th>PILIH</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="list-rab">
                                                            @if(@$io->kontrak->latest_kontrak->kontrak_version_rab != null)
                                                                <?php
                                                                $used_rab = $io->idRab->toArray();
                                                                ?>
                                                                @foreach($io->kontrak->latest_kontrak->kontrak_version_rab as $rab)
                                                                    <tr>
                                                                        <td>{{@$rab->rab->no_dokumen}}</td>
                                                                        <td>{{@$rab->rab->order->nomor_order}}</td>
                                                                        <td>
                                                                            @foreach(\App\Kontrak::getNamaInstalasiDanLingkupByKontrak($rab) as $row) <li>{{$row['instalasi']." (".$row['layanan']." ".$row['lingkup'].")"}}</li>@endforeach
                                                                        </td>
                                                                        <td>{{date('d M Y',strtotime(@$rab->rab->created_at))}}</td>
                                                                        <td style='text-align:right;'>
                                                                            Rp. {{number_format(@$rab->rab->total_biaya, 2, ',', '.')}}</td>
                                                                        <td style="text-align:center;"><input disabled
                                                                                                              type="checkbox"
                                                                                                              name="rab[]"
                                                                                                              {{(in_array(array('rab_id' => @$rab->id_rab), $used_rab)) ? "checked='checked'" : ""}}
                                                                                                              nilai="{{@$rab->rab->total_biaya}}"
                                                                                                              value="{{@$rab->id_rab}}"/>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(@$io->tipeWbs->nama_reference == TIPE_EMERGENCY)
                                                <div id="div_order">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <label class="col-sm-12 control-label">Order</label>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="append-icon">
                                                                <input type="hidden" name="order"
                                                                       value="{{$io->order_id}}"/>
                                                                <input type="text" readonly
                                                                       class="form-control form-white"
                                                                       value="{{@$io->order->nomor_order}} - {{@$io->order->perusahaan->nama_perusahaan}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" id="tgl_order"
                                                                   class="form-control form-white"
                                                                   value="{{($io == null) ? "" : date('d M Y',strtotime(@$io->order->tanggal_order))}}"
                                                                   readonly placeholder="Tgl. Order"/>
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <a href="{{($io == null) ? "#" : url('/internal/detail_order/'.@$io->order_id)}}"
                                                               target="_blank" id="detail_order"
                                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                        class="fa fa-eye"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="div_permohonan">
                                                        <table id="table-permohonan" class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>NOMOR PERMOHONAN</th>
                                                                <th>TANGGAL PERMOHONAN</th>
                                                                <th>NAMA INSTALASI</th>
                                                                <th>JENIS INSTALASI</th>
                                                                <th>JENIS PEKERJAAN</th>
                                                                <th>JENIS LINGKUP</th>
                                                                <th style="text-align:center">PILIH</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="list-permohonan">
                                                            @if(@$io->order != null)
                                                                <?php
                                                                $used_permohonan = $io->idPermohonan->toArray();
                                                                ?>
                                                                @foreach($io->order->permohonan as $item)
                                                                    <tr>
                                                                        <td>{{ @$item->nomor_permohonan}}</td>
                                                                        <td>{{ date('d M Y',strtotime( @$item->tanggal_permohonan ))}}</td>
                                                                        <td style="white-space: nowrap;">{{ @$item->instalasi->nama_instalasi}}</td>
                                                                        <td style="white-space: nowrap;">{{ @$item->tipeInstalasi->nama_instalasi }}</td>
                                                                        <td style="white-space: nowrap;">{{ @$item->produk->produk_layanan }}</td>
                                                                        <td style="white-space: nowrap;">{{ @$item->lingkup_pekerjaan->jenis_lingkup_pekerjaan }}
                                                                            @if(@$item->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                                                <br/>
                                                                                @foreach(@$item->bayPermohonan as $bg)
                                                                                    - {{@$bg->bayGardu->nama_bay}}<br/>
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td style="text-align:center;"><input
                                                                                    type="checkbox" disabled
                                                                                    name="permohonan[]"
                                                                                    {{(in_array(array('permohonan_id' => @$item->id), $used_permohonan)) ? "checked='checked'" : ""}}
                                                                                    value="{{@$item->id}}"/>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    @if(@$io->file_landasan != null)
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <label class="col-sm-12 control-label">File
                                                                    Landasan</label>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <a target="_blank"
                                                                   href="{{url('upload/'.@$io->file_landasan)}}">
                                                                    <button type="button"
                                                                            class="col-lg-12 btn btn-primary">
                                                                        <center><i
                                                                                    class="fa fa-download"></i>Preview
                                                                            File
                                                                        </center>
                                                                    </button>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Perusahaan</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="text" readonly class="form-control form-white"
                                                               value="{{@$io->perusahaan->nama_perusahaan}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Jenis Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <select name="jenis_pekerjaan" required disabled
                                                                readonly="readonly"
                                                                class="form-control form-white">
                                                            <option value="0">-Pilih Jenis Pekerjaan-</option>
                                                            @foreach($jenis_pekerjaan as $item)
                                                                <option value="{{$item->id}}" {{(@$io->jenis_pekerjaan_wbs_id == $item->id) ? "selected='selected'" : ""}}>{{$item->jenis_pekerjaan}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                       <textarea class="form-control form-white" rows="4"
                                                 name="uraian_pekerjaan" readonly
                                                 placeholder="Uraian Pekerjaan"
                                                 required>{{@$io->uraian_pekerjaan}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nilai</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input placeholder="Nilai" type="text" name="nilai"
                                                           value="Rp. {{number_format(@$io->nilai, 2, ',', '.')}}"
                                                           readonly
                                                           class="form-control form-white" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Total Pembayaran</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input placeholder="Nilai" type="text" name="pengalihan"
                                                           value="Rp. {{number_format($pembayaran, 2, ',', '.')}}"
                                                           readonly
                                                           class="form-control form-white" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Realisasi</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input placeholder="Realisasi"
                                                           value="Rp. {{number_format(@$realisasi, 2, ',', '.')}}"
                                                           type="text" readonly
                                                           class="form-control form-white">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input placeholder="Realisasi" value="{{$persen_realisasi}}%"
                                                           type="text" readonly
                                                           class="form-control form-white">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Saldo</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input placeholder="Sisa"
                                                           value="Rp. {{number_format(@$sisa, 2, ',', '.')}}"
                                                           type="text" readonly
                                                           class="form-control form-white">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input placeholder="Sisa" value="{{$persen_sisa}}%" type="text"
                                                           readonly
                                                           class="form-control form-white">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <label class="col-sm-12 control-label">*)Realisasi dan saldo berdasarkan jumlah pembayaran</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <h2>
                                                    <div class="col-sm-6" style="padding-left:2.5%">
                                                        <b>STATUS </b>{{@$io->statusWbsIo->nama_reference}}
                                                    </div>
                                                </h2>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <hr/>
                                    </div>
                                    <div class="tab-pane" id="realisasi">
                                        <div class="panel-content pagination2 table-responsive">
                                            <table class="table table-hover table-dynamic">
                                                <thead>
                                                <tr>
                                                    <th>Tanggal Upload</th>
                                                    <th>Posting Date</th>
                                                    <th>Cost Element</th>
                                                    <th>Jumlah</th>
                                                    <th>Deskripsi</th>
                                                    <th>Uploaded By</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach(@$io->realisasi as $item)
                                                    <tr>
                                                        <td>{{date('d M Y',strtotime(@$item->tgl_upload))}}</td>
                                                        <td>{{date('d M Y',strtotime(@$item->posting_date))}}</td>
                                                        <td>{{@$item->cost_element}}</td>
                                                        <td style="text-align: right;">
                                                            Rp. {{number_format(@$item->jumlah, 2, ',', '.')}}</td>
                                                        <td>{{@$item->deskripsi}}</td>
                                                        <td>{{@$item->username}}</td>
                                                        <td>
                                                            <a onclick="return confirm('Anda yakin menghapus realisasi {{@$item->deskripsi}} ?');"
                                                               href="{{url('/internal/delete_realisasi_wbs_io/'.@$item->id)}}"
                                                               class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                                        class="fa fa-trash"></i></a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="pembayaran">
                                        <div class="panel-content pagination2 table-responsive">
                                            <table class="table table-hover table-dynamic">
                                                <thead>
                                                <tr>
                                                    <th>Termin Ke</th>
                                                    <th>Persentase</th>
                                                    <th>Nilai</th>
                                                    <th>Status</th>
                                                    <th>Keterangan</th>
                                                    <th>Bukti</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach(@$io->kontrak->pembayaran as $item)
                                                    <tr>
                                                        <td>{{@$item->termin_ke}}</td>
                                                        <td>{{@$item->persentase}}</td>
                                                        <td>{{@$item->nilai}}</td>
                                                        <td>{{@$item->status}}</td>
                                                        <td>{{@$item->keterangan}}</td>
                                                        <td>
                                                            @if(@$item->bukti != null)
                                                                <button type="button"
                                                                        onclick="window.location.href='{{url('upload/'.@$item->bukti)}}'"
                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                    <i
                                                                            class="fa fa-download"></i>
                                                                    BUKTI TRF
                                                                </button>
                                                            @endif
                                                            @if(@$item->bukti_pph != null)
                                                                <button type="button"
                                                                        onclick="window.location.href='{{url('upload/'.@$item->bukti_pph)}}'"
                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                    <i
                                                                            class="fa fa-download"></i>
                                                                    PPH 23
                                                                </button>
                                                            @endif
                                                            @if(@$item->file_kuitansi_transfer != null)
                                                                <button type="button"
                                                                        onclick="window.location.href='{{url('upload/'.@$item->file_kuitansi_transfer)}}'"
                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                    <i
                                                                            class="fa fa-download"></i>
                                                                    KUITANSI TRF
                                                                </button>
                                                            @endif
                                                            @if(@$item->file_kuitansi_pph23 != null)
                                                                <button type="button"
                                                                        onclick="window.location.href='{{url('upload/'.@$item->file_kuitansi_pph23)}}'"
                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                    <i
                                                                            class="fa fa-download"></i>
                                                                    KUITANSI PPh 23
                                                                </button>
                                                            @endif</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="panel-footer clearfix bg-white">--}}
                            {{--<input type="hidden" name="jenis" value="IO">--}}
                            {{--<div class="pull-right">--}}
                            {{--<a href="{{ url('internal/show_wbs_io') }}"--}}
                            {{--class="btn btn-warning btn-square btn-embossed">Kembali--}}
                            {{--&nbsp;</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            @include('workflow_view')
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $(document).ready(function () {
                    $('input[name*="permohonan').iCheck({
                        checkboxClass: 'icheckbox_square-red',
                        radioClass: 'iradio_square-red',
                        increaseArea: '20%' // optional
                    });
                    $('input[name*="rab').iCheck({
                        checkboxClass: 'icheckbox_square-red',
                        radioClass: 'iradio_square-red',
                        increaseArea: '20%' // optional
                    });
                });
            </script>
@endsection
