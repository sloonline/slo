@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            {{--<h2><strong>RAB</strong> Permohonan {{$permohonan->nomor_permohonan}}</h2>--}}
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/kontrak')}}">Kontrak</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        {!! Form::open(array('url'=> '/internal/save_kontrak', 'files'=> true, 'class'=> 'form-horizontal')) !!}
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Form Input <strong>Kontrak</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{-- start form pilih permohonan--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne">
                                            <i class="icon-plus"></i> Pilih Rancangan Biaya
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse">
                                    <div class="panel-body">
                                        <div class="col-md-4">
                                            <select data-search="true" name="user" class="form-control form-white" id="list_user">
                                                <?php $i = 0?>
                                                @for($i=0;$i<sizeof($peminta_jasa);$i++)
                                                    <option value="{{$peminta_jasa[$i]->id}}" {{($peminta_jasa[$i]->id == @$used_user->id || $peminta_jasa[$i]->id == @$order->user->id)?'selected':''}}>{{$peminta_jasa[$i]->perusahaan->nama_perusahaan}}</option>
                                                @endfor
                                            </select>
                                            <p>*) Pilih User terlebih dahulu</p>
                                        </div>
                                        <br><br><br><br>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table" id="table_rab">
                                                    <thead>
                                                    <tr>
                                                        <th>Nomor RAB</th>
                                                        <th>Nomor Order</th>
                                                        <th>Tanggal RAB</th>
                                                        <th>Total Biaya</th>
                                                        <th>Peminta Jasa</th>
                                                        <th>Pilih</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {{--list used rab--}}
                                                    @foreach($allRab as $item)
                                                        <tr>
                                                            <td><a class="btn btn-sm btn-primary btn-transparent" href="{{url('internal/detail_rancangan_biaya/'. @$item['rab']->order_id.'/'.@$item['rab']->id)}}" target="_blank">{{ @$item["rab"]->no_dokumen}}</a></td>
                                                            <td><a class="btn btn-sm btn-primary btn-transparent" href="{{url('internal/detail_order/'. @$item['rab']->order_id)}}" target="_blank">{{ @$item["rab"]->order->nomor_order}}</a></td>
                                                            <td>{{ date('d M Y',strtotime(@$item["rab"]->created_at)) }}</td>
                                                            <td>{{ @$item["rab"]->total_biaya}}</td>
                                                            <td>{{ @$item["rab"]->order->perusahaan->nama_perusahaan}}</td>
                                                            <td>
                                                                <input type="checkbox" name="pilih_rab[]"
                                                                       biaya="{{ @$item["rab"]->total_biaya}}"
                                                                       {{(@$item['val'] > 0 || (@$item['val']  == 0 && @$order->id == @$item["rab"]->order_id)) ? "checked='checked'" : ""}}
                                                                       value="{{@$item["rab"]->id}}"
                                                                       class="form-control pilih_rab">
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo">
                                            <i class="icon-plus"></i> Data Kontrak
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="nav-tabs2" id="tabs">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#kontrak" data-toggle="tab">
                                                                Kontrak
                                                            </a>
                                                        </li>
                                                        @if($kontrak_id != 0)
                                                            <li>
                                                                <a href="#amandemen_kontrak" data-toggle="tab">
                                                                    Amandemen Kontrak
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                    <div class="tab-content bg-white">
                                                        <div class="tab-pane active" id="kontrak">
                                                            {!! Form::hidden('id', @$kontrak_id) !!}
                                                            {!! Form::hidden('version_id', @$latest->id) !!}
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Nomor
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input type="text" required="required"
                                                                               name="nomor_kontrak"
                                                                               value="{{(@$latest != null) ? @$latest->nomor_kontrak : ''}}"
                                                                               class="form-control form-white"
                                                                               placeholder="Nomor Kontrak">
                                                                        <i class="icon-list"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Tanggal
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input id="tgl_kontrak" type="text" required="required"
                                                                               name="tanggal_kontrak"
                                                                               value="{{(@$latest != null) ? date('d-m-Y',strtotime(@$latest->tgl_kontrak)): ''}}"
                                                                               class="form-control b-datepicker form-white"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               data-date-start-date="-3d"
                                                                               data-lang="en"
                                                                               data-RTL="false"
                                                                               placeholder="Tanggal">
                                                                        <i class="icon-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Masa Berlaku
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input type="text" required="required"
                                                                               name="masa_berlaku_kontrak"
                                                                               value="{{(@$latest != null) ? date('d-m-Y',strtotime(@$latest->masa_berlaku_kontrak)): ''}}"
                                                                               class="form-control b-datepicker form-white"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               data-date-start-date="-3d"
                                                                               data-lang="en"
                                                                               data-RTL="false"
                                                                               placeholder="Masa Berlaku Kontrak">
                                                                        <i class="icon-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Deskripsi
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                            <textarea class="form-control form-white" rows="4"
                                                                      name="deskripsi_kontrak" required="required"
                                                                      placeholder="Deskripsi Kontrak">{{(@$latest != null) ? @$latest->uraian : ''}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Nilai
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input type="text" required="required"
                                                                               name="nilai_kontrak"
                                                                               class="form-control form-white"
                                                                               value="{{number_format(@$latest->nilai_kontrak, 0, '.', ',')}}"
                                                                               placeholder="Nilai Kontrak" readonly>
                                                                        <i class="glyphicon glyphicon-ok"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">File
                                                                        Kontrak *</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input type="file"
                                                                               {{(@$latest->file == null) ? "required='required'" : ""}}
                                                                               name="file_kontrak"
                                                                               class="form-control form-white"
                                                                               placeholder="File Kontrak">
                                                                        <i class="glyphicon glyphicon-paperclip"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label"></label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    @if(@$latest != null && @$latest->file != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$latest->file)}}">
                                                                            <button type="button"
                                                                                    class="col-lg-12 btn btn-primary">
                                                                                <center><i
                                                                                            class="fa fa-download"></i>Preview
                                                                                    File
                                                                                </center>
                                                                            </button>
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="amandemen_kontrak">
                                                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_AMANDEMEN_KONTRAK))
                                                                    <div class="btn-group">
                                                                        <a href="{{ url('/internal/create_amandemen/'.@$kontrak_id) }}">
                                                                            <button type="button"
                                                                                    class=" btn btn-success btn-square btn-block
                                                                        btn-embossed">
                                                                                <i class="fa fa-plus"></i> Amandemen Kontrak
                                                                            </button>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                            <table class="table table-hover table-dynamic">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nomor Kontrak</th>
                                                                    <th>Tanggal Amandemen</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Nilai Kontrak</th>
                                                                    <th>Amandemen ke</th>
                                                                    <th>Aksi</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $no = 1;?>
                                                                @foreach($kontrakVersion as $item)
                                                                    <tr>
                                                                        <td>{{$no}}</td>
                                                                        <td>{{@$item->nomor_kontrak}}</td>
                                                                        <td>{{date('d M Y',strtotime(@$item->tgl_kontrak))}}</td>
                                                                        <td>{{@$item->uraian}}</td>
                                                                        <td>{{@$item->nilai_kontrak}}</td>
                                                                        <td>{{@$item->version}}</td>
                                                                        <td>
                                                                            <a href="{{url('/internal/detail_amandemen/'.@$item->id)}}">
                                                                                <button type="button"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                    <i
                                                                                            class="fa fa-eye"></i>
                                                                                </button>
                                                                            </a></td>
                                                                    </tr>
                                                                    <?php $no++;?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <div class="pull-right">
                                <a href="{{url('internal/kontrak')}}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali <i
                                            class="icon-ban"></i></a>
                                <button type="submit" class="btn btn-primary btn-square btn-embossed">Simpan <i
                                            class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="modal fade" id="modal-aggreement" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3"><label>Termin</label></div>
                                <div class="col-md-6"><input type="text" placeholder="Termin"
                                                             class="form-control form-white"></div>
                            </div>
                            <br><br>
                            <div class="form-group">
                                <div class="col-md-3"><label>Presentase</label></div>
                                <div class="col-md-6"><input type="text" placeholder="Presentase"
                                                             class="form-control form-white"></div>
                            </div>
                            <br><br>
                            <div class="form-group">
                                <div class="col-md-3"><label>Nilai</label></div>
                                <div class="col-md-6"><input type="text" placeholder="Nilai"
                                                             class="form-control form-white"></div>
                            </div>
                            <br><br>
                            <div class="col-md-9">
                                <button class="btn btn-primary btn-square btn-embossed pull-right">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->
            <script>

//                $(document).ready(function () {
//                    var date = new Date();
//                    date.setDate(date.getDate()-1);
//
//                    $('#tgl_kontrak').datepicker({
//                        startDate: date
//                    });
//                });

                calculateNilaiKontrak();
                
                $("input[name=nilai_kontrak]").maskNumber({
                    integer: true
                });
                
                $("#btn_tambah").on('click', function () {
                    $("#modal-aggreement").modal("show");
                });

                $("input").unbind('keydown');

                oTable = $('#table_rab').DataTable();

                $(document).ready(function () {
                    oTable.search($('#list_user option:selected').text()).draw();
                });

                $('#list_user').change(function () {
                    $(":checkbox").prop('checked', false).parent().removeClass('checked');
                    oTable.search($('#list_user option:selected').text()).draw();
                });

                $('.pilih_rab').on('ifChecked', function () {
                    calculateNilaiKontrak();
                });

                $('.pilih_rab').on('ifUnchecked', function () {
                    calculateNilaiKontrak();
                });

                function calculateNilaiKontrak() {
                    var sum = 0;
                    $('#table_rab').find('tr').each(function () {
                        var row = $(this);
                        if (row.find('input[type="checkbox"]').is(':checked')) {
                            sum += parseFloat(row.find('input[type="checkbox"]').attr('biaya'));
                        }
                    });
                    $("input[name=nilai_kontrak]").val(sum.toLocaleString('en-US', {minimumFractionDigits: 0}));
                }

            </script>
@endsection