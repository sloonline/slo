@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Monitoring <strong>WBS</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>WBS/IO</li>
                    <li class="active">Monitoring WBS</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-header panel-controls bg-primary">
                            <h3><i class="fa fa-table"></i> List <strong>Data</strong></h3>
                        </div>
                        <div class="panel-content">
                            <div class="btn-group pull-right">
                                <a href="{{ url('internal/monitoring_wbs/export') }}"
                                   class="btn btn-success btn-square btn-block btn-embossed">
                                    <i class="fa fa-file-excel-o"></i> Export Data</a>
                            </div>
                            <div style="overflow-x: scroll;overflow-y: hidden;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Unit</th>
                                        <th style="white-space: nowrap">Uraian Pekerjaan</th>
                                        <th style="white-space: nowrap">No. Kontrak</th>
                                        <th style="white-space: nowrap">Tgl Kontrak</th>
                                        <th style="white-space: nowrap">No. WBS</th>
                                        <th>Tahun</th>
                                        <th>Jenis Pekerjaan</th>
                                        <th>Nilai</th>
                                        <th>Realisasi</th>
                                        <th>Sisa Nilai Penugasan</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach($wbs as $item)
                                        <?php
                                        $saldo = \App\WbsIo::getColorWarning($item);
                                        ?>
                                        <tr {!! (@$item->statusWbsIo->nama_reference == LOCKED) ? 'style="background-color: #FEFFB5;"' : "" !!}>
                                            <td>{{$no}}</td>
                                            <td style="white-space: nowrap">{{@$item->businessArea->description}}</td>
                                            <td style="white-space: nowrap">{{@$item->uraian_pekerjaan}}</td>
                                            <td style="white-space: nowrap">{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->nomor_kontrak.'<br/>'.@$item->kontrak->latest_kontrak->uraian : "" !!}</td>
                                            <td style="white-space: nowrap">{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->tgl_kontrak : "" !!}</td>
                                            <td>{{@$item->nomor}}</td>
                                            <td>{{@$item->tahun}}</td>
                                            <td style="white-space: nowrap">{{@$item->jenis_pekerjaan->jenis_pekerjaan}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format(@$item->realisasi->sum('jumlah') , 2, ',', '.')}}</td>
                                            <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                                                <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                                            </td>
                                            <td>{{@$item->statusWbsIo->nama_reference}}</td>
                                        </tr>
                                        <?php $no++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

@endsection