@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Pengalihan <strong>Biaya</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>WBS/IO</li>
                    <li class="active">Create Pengalihan Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    {!! Form::open(['url'=>'internal/save_pengalihan_biaya','id'=>'form_pengalihan', 'class'=>'form-horizontal', 'role'=>'form','files'=> true]) !!}
                    {!! Form::hidden('id', @$id) !!}
                    {!! Form::hidden('wbs', @$wbs_id) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>PENGALIHAN BIAYA</strong></h3>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-content ">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Nomor WBS</label>
                                </div>
                                <div class="col-sm-10">
                                    <select name="wbs_id" id="wbs_id" onchange="checkform()"
                                            {{(@$pengalihan->wbs_id != null) ? "readonly" : ""}}
                                            class="form-control form-white" required>
                                        <option value="0">-Pilih WBS-</option>
                                        @foreach($allWbs as $item)
                                            <option value="{{@$item->id}}" {{(@$pengalihan->wbs_id == @$item->id) ? "selected='selected'" : ""}}>{{@$item->nomor}}
                                                - {{@$item->uraian_pekerjaan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="detail_wbs">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label">Total Pengalihan</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input placeholder="Nilai" type="text" name="pengalihan" id="total_pengalihan"
                                               readonly style="text-align: right"
                                               class="form-control form-white" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input placeholder="Pengalihan"  id="persen_pengalihan"
                                               type="text" readonly style="text-align: right"
                                               class="form-control form-white">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label">Sisa Pengalihan</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input placeholder="Nilai" type="text" name="pengalihan"  id="sisa_pengalihan"
                                               readonly style="text-align: right"
                                               class="form-control form-white" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input placeholder="Pengalihan"  id="persen_sisa_pengalihan"
                                               type="text" readonly style="text-align: right"
                                               class="form-control form-white">
                                    </div>
                                </div>
                                <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Realisasi</label>
                                </div>
                                <div class="col-sm-2">
                                    <input placeholder="Realisasi" id="realisasi"
                                           type="text" readonly style="text-align: right"
                                           class="form-control form-white">
                                </div>
                                <div class="col-sm-2">
                                    <input placeholder="Realisasi" id="persen_realisasi"
                                           type="text" readonly style="text-align: right"
                                           class="form-control form-white">
                                </div>
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Saldo</label>
                                </div>
                                <div class="col-sm-2">
                                    <input placeholder="Sisa" id="saldo"
                                           type="text" readonly style="text-align: right"
                                           class="form-control form-white">
                                </div>
                                <div class="col-sm-2">
                                    <input placeholder="Sisa" id="persen_saldo" type="text"
                                           readonly style="text-align: right"
                                           class="form-control form-white">
                                </div>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Tanggal Pengalihan</label>
                                </div>
                                <div class="col-md-10">
                                    <input type="text" required="required" onchange="checkform()"
                                           name="tgl_awal" id="tgl_awal"
                                           value="{{(@$pengalihan == null) ? "":  date('d-m-Y',strtotime(@$pengalihan->tgl_pengalihan))}}"
                                           class="form-control b-datepicker form-white"
                                           data-date-format="dd-mm-yyyy"
                                           data-lang="en"
                                           data-RTL="false"
                                           placeholder="Tanggal Pengalihan Biaya">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Nilai</label>
                                </div>
                                <div class="col-sm-10">
                                    <input placeholder="Nilai" min="1"
                                           name="nilai" onkeyup="checkform()"
                                           value="{{number_format(@$pengalihan->nilai, 0, '.', ',')}}"
                                           id="nilai"
                                           class="form-control form-white" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">No Surat Pengalihan</label>
                                </div>
                                <div class="col-sm-10">
                                    <input placeholder="Nomor Surat"
                                           name="surat_pengalihan" onkeyup="checkform()"
                                           value="{{@$pengalihan->surat_pengalihan}}"
                                           id="surat_pengalihan"
                                           class="form-control form-white" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">File Surat</label>
                                </div>
                                <div class="col-md-10 prepend-icon">
                                    <input type="file" onchange="checkform()"
                                           name="file_pengalihan"
                                           class="form-control form-white"
                                           placeholder="File Surat Pengalihan">
                                    <i class="glyphicon glyphicon-paperclip"></i>
                                </div>
                            </div>
                            @if(@$pengalihan->file_pengalihan != null)
                                <div class="form-group">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-sm-12 prepend-icon">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$pengalihan->file_pengalihan )}}">
                                                <button type="button"
                                                        class="col-lg-12 btn btn-primary">
                                                    <center><i
                                                                class="fa fa-download"></i>Preview
                                                        File
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Keterangan</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="append-icon">
                                       <textarea class="form-control form-white" rows="4"
                                                 name="keterangan" onkeyup="checkform()"
                                                 placeholder="Keterangan pengalihan biaya">{{@$pengalihan->keterangan}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div id="history_biaya">
                                <h2>History Pengalihan Biaya</h2>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No WBS</th>
                                        <th>Tanggal Pengalihan</th>
                                        <th>Nilai</th>
                                        <th>No Surat Pengalihan</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    </thead>
                                    <tbody id="table_history">
                                    @if($wbs != null)
                                        <?php
                                        $no = 1;
                                        ?>
                                        @foreach(@$wbs->pengalihan_biaya as $item)
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{@$item->wbs->nomor}}</td>
                                                <td>{{date('d M Y',strtotime(@$item->tgl_pengalihan))}}</td>
                                                <td style="white-space: nowrap;text-align: right;">
                                                    Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                                <td>{{@$item->surat_pengalihan}}</td>
                                                <th>{{@$item->keterangan}}</th>
                                            </tr>
                                            <?php $no++; ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                {{--cek apakah berasal dari halaman detail wbs atau bukan--}}
                                <a href="{{($pengalihan == null && $wbs_id > 0 ) ?url('/internal/detail_wbs_io/'.$wbs_id) :  url('/internal/pengalihan_biaya')}}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;<i
                                            class="icon-ban"></i></a>
                                <button type="submit" id="submit_form"
                                        class="btn btn-success btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-disk"></i>
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $("#nilai").maskNumber({
                    integer: true
                });
                function checkform() {
                    var f = document.forms["form_pengalihan"].elements;
                    var cansubmit = true;

                    for (var i = 0; i < f.length; i++) {
                        if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                            cansubmit = false;
                        }
                    }
                    document.getElementById('submit_form').disabled = !cansubmit;
                }
                window.onload = checkform;
                @if($wbs_id > 0)
                 $("#wbs_id").val("{{$wbs_id}}").change();
                @endif
                generateHistory();
                $("#wbs_id").change(function () {
                    generateHistory();
                });
                function generateHistory() {
                    $("#history_biaya").hide('medium');
                    $("#detail_wbs").hide('medium');
                    var wbs = $("#wbs_id").find('option:selected').attr("value");
                    {!! $i = 0 !!}
                            @foreach($allWbs as $data)
                    if ("{{$data->id}}" == wbs) {
                        table = "";
                        var no = 1;
                        @foreach($data->pengalihan_biaya as $item)
                                table += "<tr>";
                        table += "<td>" + no + "</td>";
                        table += "<td>{{@$item->wbs->nomor}}</td>";
                        table += "<td>{{date('d M Y',strtotime(@$item->tgl_pengalihan))}}</td>";
                        table += "<td style='text-align:right;'>Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>";
                        table += "<td>{{@$item->surat_pengalihan}}</td>";
                        table += "<td>{{@$item->keterangan}}</td>";
                        table += "</tr>";
                        no++;
                        @endforeach
                        $("#table_history").html(table);
                        $("#history_biaya").show('medium');
                        <?php
                                $realisasi = ($data != null) ? $data->realisasi->sum('jumlah') : 0;
                                $persen_realisasi = round(($realisasi / $data->nilai) * 100, 2);
                                $sisa = $data->nilai - $realisasi;
                                $persen_sisa = 100 - $persen_realisasi;
                                $pengalihan = ((sizeof($data->pengalihan_biaya) > 0)?$data->pengalihan_biaya->sum('nilai') : 0);
                                $sisa_pengalihan = ($data->nilai - ((sizeof($data->pengalihan_biaya) > 0)?$data->pengalihan_biaya->sum('nilai') : 0));
                                $persen_pengalihan = round((($data->nilai == 0) ? 0 : $pengalihan / $data->nilai) * 100, 2);
                                $persen_sisa_pengalihan = 100 - $persen_pengalihan;
                        ?>
                        $("#total_pengalihan").val("Rp. {{number_format($pengalihan, 2, ',', '.')}}");
                        $("#sisa_pengalihan").val("Rp. {{number_format($sisa_pengalihan, 2, ',', '.')}}");
                        $("#persen_pengalihan").val("{{number_format($persen_pengalihan, 2, ',', '.')}} %");
                        $("#persen_sisa_pengalihan").val("{{number_format($persen_sisa_pengalihan, 2, ',', '.')}} %");
                        $("#realisasi").val("Rp. {{number_format($realisasi, 2, ',', '.')}}");
                        $("#persen_realisasi").val("{{number_format($persen_realisasi, 2, ',', '.')}} %");
                        $("#saldo").val("Rp. {{number_format($sisa, 2, ',', '.')}}");
                        $("#persen_saldo").val("{{number_format($persen_sisa, 2, ',', '.')}} %");
                        $("#detail_wbs").show('medium');
                    }
                    @endforeach
                }
            </script>
@endsection