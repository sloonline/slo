@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Internal <strong>Order</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>WBS/IO</li>
                    <li class="active">Create IO</li>
                </ol>
            </div>
            @if(Session::has('msg'))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <i class="glyphicon glyphicon-warning-sign"></i> {{Session::get('msg')}}
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    {!! Form::open(['url'=>'internal/save_wbs_io','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form','files'=> true]) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> INPUT <strong>IO</strong></h3>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-content ">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Tipe IO *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <select name="tipe" id="tipe_io"
                                                                {{($io != null) ? "readonly" : ""}}
                                                                class="form-control form-white" required>
                                                            <option value="0">-Pilih Tipe-</option>
                                                            @foreach($ref_tipe as $item)
                                                                <option value="{{$item->id}}" {{(@$io->tipe == $item->id) ? "selected='selected'" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nomor IO</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input placeholder="Nomor IO" type="text" name="nomor"
                                                               {{(@$io == null || @$io->flow_status->tipe_status == CREATED) ? "readonly" : ""}} value="{{@$io->nomor}}"
                                                               class="form-control form-white" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Deskripsi</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input placeholder="Deskripsi Nomor" type="text"
                                                               name="deskripsi"
                                                               {{(@$io == null || @$io->flow_status->tipe_status == CREATED) ? "readonly" : ""}} value="{{@$io->deskripsi}}"
                                                               class="form-control form-white" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nomor Project</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input placeholder="Nomor Project" type="text"
                                                               {{(@$io == null || @$io->flow_status->tipe_status == CREATED) ? "readonly" : ""}}  name="nomor_project"
                                                               value="{{@$io->nomor_project}}"
                                                               class="form-control form-white">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Tahun *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <input type="number" min="1900" name="tahun" required
                                                               class="form-control form-white"
                                                               value="{{@$io->tahun}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="div_kontrak">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">Kontrak *</label>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="append-icon">
                                                            @if($io == null || @$io->flow_status->tipe_status == CREATED || ($completed == true && $io->tipeWbs->nama_reference == TIPE_EMERGENCY))
                                                                <select name="kontrak" id="kontrak" required
                                                                        data-search="true"
                                                                        class="form-control form-white">
                                                                    <option value="0">-Pilih Kontrak-</option>
                                                                    @foreach($kontrak as $item)
                                                                        <option tgl="{{date('d M Y',strtotime(@$item->latest_kontrak->tgl_kontrak))}}"
                                                                                tgl_awal="{{date('d-m-Y',strtotime(@$item->latest_kontrak->tgl_kontrak))}}"
                                                                                masa_berlaku="{{date('d-m-Y',strtotime(@$item->latest_kontrak->masa_berlaku_kontrak))}}"
                                                                                unit="{{@$item->perusahaan->id_business_area}}"
                                                                                value="{{$item->id}}" {{(@$io->kontrak_id == $item->id) ? "selected='selected'" : ""}}>{{$item->latest_kontrak->nomor_kontrak}}
                                                                            - {{$item->perusahaan->nama_perusahaan}}
                                                                            ({{$item->latest_kontrak->uraian}})
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <input type="hidden" name="kontrak"
                                                                       value="{{$io->kontrak_id}}"/>
                                                                <input type="text" readonly
                                                                       class="form-control form-white"
                                                                       value="{{@$io->kontrak->latest_kontrak->nomor_kontrak}} - {{@$io->kontrak->perusahaan->nama_perusahaan}}"/>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="tgl_kontrak"
                                                               class="form-control form-white"
                                                               value="{{($io == null) ? "" : date('d M Y',strtotime(@$io->kontrak->latest_kontrak->tgl_kontrak))}}"
                                                               readonly placeholder="Tgl. Kontrak"/>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <a href="{{($io == null) ? "#" : url('/internal/detail_kontrak/'.@$io->kontrak_id)}}"
                                                           target="_blank" id="detail_kontrak"
                                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                    class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" id="div_rab">
                                                    <table id="table-rab" class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>NOMOR RAB</th>
                                                            <th>NOMOR ORDER</th>
                                                            <th>INSTALASI</th>
                                                            <th>TANGGAL RAB</th>
                                                            <th>TOTAL BIAYA</th>
                                                            <th>PILIH</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="list-rab">
                                                        @if(@$io->kontrak->latest_kontrak->kontrak_version_rab != null)
                                                            <?php
                                                            $used_rab = $io->idRab->toArray();
                                                            ?>
                                                            @foreach($io->kontrak->latest_kontrak->kontrak_version_rab as $rab)
                                                                <tr>
                                                                    <td>{{@$rab->rab->no_dokumen}}</td>
                                                                    <td>{{@$rab->rab->order->nomor_order}}</td>
                                                                    <td>{{date('d M Y',strtotime(@$rab->rab->created_at))}}</td>
                                                                    <td style='text-align:right;'>
                                                                        Rp. {{number_format(@$rab->rab->total_biaya, 2, ',', '.')}}</td>
                                                                    <td style="text-align:center;"><input
                                                                                type="checkbox"
                                                                                {{(in_array(array('rab_id' => @$rab->id_rab), $used_rab)) ? "checked='checked'" : ""}}
                                                                                nilai="{{@$rab->rab->total_biaya}}"
                                                                                onchange="nilaiRab()" name="rab[]"
                                                                                value="{{@$rab->id_rab}}"/></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">Tanggal Mulai *</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" required="required"
                                                               name="tgl_awal" id="tgl_awal"
                                                               value="{{(@$io == null) ? "":  date('d-m-Y',strtotime(@$io->tgl_awal))}}"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy"
                                                               data-lang="en"
                                                               data-RTL="false"
                                                               placeholder="Tanggal Awal">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">Tanggal Berakhir *</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" required="required"
                                                               name="tgl_akhir" id="tgl_akhir"
                                                               value="{{(@$io == null) ? "":  date('d-m-Y',strtotime(@$io->tgl_akhir))}}"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy"
                                                               data-lang="en"
                                                               data-RTL="false"
                                                               placeholder="Tanggal Akhir">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="div_order">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">Order *</label>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="append-icon">
                                                            @if($io == null || ($completed == true && $io->tipeWbs->nama_reference == TIPE_EMERGENCY))
                                                                <select name="order" id="order" required
                                                                        class="form-control form-white">
                                                                    <option value="0">-Pilih Order-</option>
                                                                    @foreach($order as $item)
                                                                        <option tgl="{{date('d M Y',strtotime(@$item->tanggal_order))}}"
                                                                                perusahaan="{{@$item->perusahaan->id}}"
                                                                                value="{{$item->id}}" {{(@$io->order_id == $item->id) ? "selected='selected'" : ""}}>{{$item->nomor_order}}
                                                                            - {{$item->perusahaan->nama_perusahaan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <input type="hidden" name="order"
                                                                       value="{{$io->order_id}}"/>
                                                                <input type="text" readonly
                                                                       class="form-control form-white"
                                                                       value="{{@$io->order->nomor_order}} - {{@$io->order->perusahaan->nama_perusahaan}}"/>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" id="tgl_order"
                                                               class="form-control form-white"
                                                               value="{{($io == null) ? "" : date('d M Y',strtotime(@$io->order->tanggal_order))}}"
                                                               readonly placeholder="Tgl. Order"/>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <a href="{{($io == null) ? "#" : url('/internal/detail_order/'.@$io->order_id)}}"
                                                           target="_blank" id="detail_order"
                                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                    class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" id="div_permohonan">
                                                    <table id="table-permohonan" class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>NOMOR PERMOHONAN</th>
                                                            <th>TANGGAL PERMOHONAN</th>
                                                            <th>NAMA INSTALASI</th>
                                                            <th>JENIS INSTALASI</th>
                                                            <th>JENIS PEKERJAAN</th>
                                                            <th>JENIS LINGKUP</th>
                                                            <th style="text-align:center">PILIH</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="list-permohonan">
                                                        @if(@$io->order != null)
                                                            <?php
                                                            $used_permohonan = $io->idPermohonan->toArray();
                                                            ?>
                                                            @foreach($io->order->permohonan as $item)
                                                                <tr>
                                                                    <td>{{ @$item->nomor_permohonan}}</td>
                                                                    <td>{{ date('d M Y',strtotime( @$item->tanggal_permohonan ))}}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->instalasi->nama_instalasi}}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->tipeInstalasi->nama_instalasi }}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->produk->produk_layanan }}</td>
                                                                    <td style="white-space: nowrap;">{{ @$item->lingkup_pekerjaan->jenis_lingkup_pekerjaan }}
                                                                        @if(@$item->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                                            <br/>
                                                                            @foreach(@$item->bayPermohonan as $bg)
                                                                                - {{@$bg->bayGardu->nama_bay}}<br/>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                    <td style="text-align:center;"><input
                                                                                type="checkbox" name="permohonan[]"
                                                                                {{(in_array(array('permohonan_id' => @$item->id), $used_permohonan)) ? "checked='checked'" : ""}}
                                                                                value="{{@$item->id}}"/></td>
                                                                </tr>
                                                            @endforeach
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">File Landasan</label>
                                                    </div>
                                                    <div class="col-md-10 prepend-icon">
                                                        <input type="file"
                                                               name="file_landasan"
                                                               class="form-control form-white"
                                                               placeholder="File Landasan">
                                                        <i class="glyphicon glyphicon-paperclip"></i>
                                                    </div>
                                                </div>
                                                @if(@$io->file_landasan != null)
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.@$io->file_landasan)}}">
                                                                <button type="button"
                                                                        class="col-lg-12 btn btn-primary">
                                                                    <center><i
                                                                                class="fa fa-download"></i>Preview
                                                                        File
                                                                    </center>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Perusahaan *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <select name="perusahaan" id="perusahaan" required
                                                                data-search="true"
                                                                class="form-control form-white">
                                                            <option value="0">-Pilih Perusahaan-</option>
                                                            @foreach($perusahaan as $item)
                                                                <option value="{{$item->id}}" {{(@$io->perusahaan_id == $item->id) ? "selected='selected'" : ""}}>{{$item->nama_perusahaan}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Jenis Pekerjaan *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                                        <select name="jenis_pekerjaan" required
                                                                data-search="true"
                                                                class="form-control form-white">
                                                            <option value="0">-Pilih Jenis Pekerjaan-</option>
                                                            @foreach($jenis_pekerjaan as $item)
                                                                <option value="{{$item->id}}" {{(@$io->jenis_pekerjaan_wbs_id == $item->id) ? "selected='selected'" : ""}}>{{$item->jenis_pekerjaan}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="append-icon">
                                       <textarea class="form-control form-white" rows="4"
                                                 name="uraian_pekerjaan"
                                                 placeholder="Uraian Pekerjaan"
                                                 required>{{@$io->uraian_pekerjaan}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="col-sm-12 control-label">Nilai *</label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <input placeholder="Nilai"
                                                           name="nilai"
                                                           value="{{number_format(@$io->nilai, 0, '.', ',')}}"
                                                           id="nilai"
                                                           class="form-control form-white" required>
                                                </div>
                                            </div>
                                            @if($io != null)
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <label class="col-sm-12 control-label">Status</label>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <select name="status" id="status"
                                                                {{($completed == false) ? "disabled" : ""}}
                                                                class="form-control form-white" required>
                                                            @foreach($ref_status as $item)
                                                                <option value="{{$item->id}}" {{(@$io->status == $item->id) ? "selected='selected'" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <hr/>
                                        <div class="panel-footer clearfix bg-white">
                                            <input type="hidden" name="jenis" value="{{IO}}">
                                            <input type="hidden" name="id" value="{{(@$io == null) ? 0 :  @$io->id}}">
                                            <div class="pull-right">
                                                <a href="{{ url('internal/show_wbs_io') }}"
                                                   class="btn btn-warning btn-square btn-embossed">Batal
                                                    &nbsp;<i class="icon-ban"></i></a>
                                                <button type="submit"
                                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                                        data-style="zoom-in">Simpan &nbsp;<i
                                                            class="glyphicon glyphicon-floppy-saved"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $("#nilai").maskNumber({
                    integer: true
                });
                changeTipeIo();
                $("#kontrak").removeAttr('required');
                $("#order").removeAttr('required');
                $("#tipe_io").change(function () {
                    changeTipeIo();
                });
                function changeTipeIo() {
                    var tipe = $("#tipe_io option:selected").text();
                    $("#div_kontrak").hide('medium');
                    $("#div_order").hide('medium');
                    $("#kontrak").removeAttr('required');
                    $("#order").removeAttr('required');
//                    $("#nilai").removeAttr('readonly');
                    if (tipe == "{{TIPE_NORMAL}}") {
                        $("#div_kontrak").show('medium');
                        $("#kontrak").attr("required", true);
//                        $("#nilai").attr("readonly", true);
                        @if($io != null)
                        $('#table-rab').show('medium');
                        @else
                        $("#list-rab").html("");
                        $('#table-rab').hide('medium');
                        @endif
                    } else if (tipe == "{{TIPE_EMERGENCY}}") {
                        $("#div_order").show('medium');
                        $("#order").attr("required", true);
                        @if($io != null)
                        $('#table-permohonan').show('medium');
                        @else
                        $("#list-permohonan").html("");
                        $('#table-permohonan').hide('medium');
                        @endif

                    }
                }
                $("#order").change(function () {
                    var order = $("#order option:selected");
                    var perusahaan = order.attr('perusahaan');
                    var tgl = order.attr('tgl');
                    if (perusahaan > 0) {
                        $("#perusahaan").val(perusahaan).change();
                    } else {
                        $("#perusahaan").val("0").change();
                    }
                    $("#tgl_order").val(tgl);
                    if (order.val() > 0) {
                        $("#detail_order").attr("href", "{{url('/internal/detail_order/')}}/" + order.val());
                    } else {
                        $("#detail_order").attr("href", "#");
                    }
                    generatePermohonan();
                });

                $("#kontrak").change(function () {
                    var kontrak = $("#kontrak option:selected");
                    var perusahaan = kontrak.attr('perusahaan');
                    var tgl = kontrak.attr('tgl');
                    var tgl_awal = kontrak.attr('tgl_awal');
                    var masa_berlaku = kontrak.attr('masa_berlaku');
                    if (perusahaan > 0) {
                        $("#perusahaan").val(perusahaan).change();
                    } else {
                        $("#perusahaan").val("0").change();
                    }
                    $("#tgl_kontrak").val(tgl);
                    $("#tgl_awal").val(tgl_awal);
                    $("#tgl_akhir").val(masa_berlaku);
                    if (kontrak.val() > 0) {
                        $("#detail_kontrak").attr("href", "{{url('/internal/detail_kontrak/')}}/" + kontrak.val());
                    } else {
                        $("#detail_kontrak").attr("href", "#");
                    }
                    generateRAB();
                });

                function generatePermohonan() {
                    $("#list-permohonan").html("");
                    $('#table-permohonan').hide('medium');
                    $("#div_permohonan").removeClass('force-table-responsive');
                    var order = $("#order option:selected").val();
                    @foreach($order as $item)
                    if ("{{$item->id}}" == order) {
                        var table = "";
                        @if($item->permohonan->count() != null)
                                @foreach($item->permohonan as $permohonan)
                                table += "<tr>";
                        table += "<td>{{@$permohonan->nomor_permohonan}}</td>";
                        table += "<td>{{ date('d M Y',strtotime( $permohonan->tanggal_permohonan ))}}</td>";
                        table += "<td>{{@$permohonan->instalasi->nama_instalasi}}</td>";
                        table += "<td>{{@$permohonan->tipeInstalasi->nama_instalasi}}</td>";
                        table += "<td>{{@$permohonan->produk->produk_layanan}}</td>";
                        table += "<td>{{@$permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}";
                        @if(@$permohonan->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                table += "<br/>";
                        @foreach(@$permohonan->bayPermohonan as $bg)
                                table += "- {{$bg->bayGardu->nama_bay}} <br/>";
                        @endforeach
                                @endif
                                table += "</td>";
                        table += '<td style="text-align:center;"><input type="checkbox" name="permohonan[]" value="{{@$permohonan->id}}"/></td>';
                        table += "</tr>";
                        @endforeach
                        $("#list-permohonan").html(table);
                        $('#table-permohonan').show('medium');
                        @endif
                    }
                    @endforeach
                }

                function generateRAB() {
                    $("#list-rab").html("");
                    $('#table-rab').hide('medium');
                    $("#div_rab").removeClass('force-table-responsive');
                    $("#nilai").val("0");
                    var kontrak = $("#kontrak option:selected").val();
                    @foreach($kontrak as $item)
                    if ("{{$item->id}}" == kontrak) {
                        var table = "";
                        @if($item->latest_kontrak->kontrak_version_rab->count() != null)
                                @foreach($item->latest_kontrak->kontrak_version_rab as $rab)
                                table += "<tr>";
                        table += "<td>{{@$rab->rab->no_dokumen}}</td>";
                        table += "<td>{{@$rab->rab->order->nomor_order}}</td>";
                        table += "<td><ul style='list-style-type: none;'>@foreach(\App\Kontrak::getNamaInstalasiDanLingkupByKontrak($rab) as $row) <li>{{$row['instalasi']." (".$row['layanan']." ".$row['lingkup'].")"}}</li>@endforeach</ul></td>";
                        table += "<td>{{date('d M Y',strtotime(@$rab->rab->created_at))}}</td>";
                        table += "<td style='text-align:right;'>Rp. {{number_format(@$rab->rab->total_biaya, 2, ',', '.')}}</td>";
                        table += '<td style="text-align:center;"><input type="checkbox"  nilai="{{@$rab->rab->total_biaya}}" onchange="nilaiRab()" name="rab[]" value="{{@$rab->id_rab}}"/></td>';
                        table += "</tr>";
                        @endforeach
                        $("#list-rab").html(table);
                        $('#table-rab').show('medium');
                        @endif
                    }
                    @endforeach
                }

                // For oncheck callback
                $('input[name*="rab"]').on('ifChecked', function () {
                    nilaiRab();
                });

                $('input[name*="rab"]').on('ifUnchecked', function () {
                    nilaiRab();
                });

                function nilaiRab() {
                    var nilai = 0;
                    $('#list-rab :checked').each(function () {
                        nilai += parseFloat($(this).attr('nilai'));
                    });
                    $("#nilai").val(nilai.toLocaleString('en-US', {minimumFractionDigits: 0}));
                }

                $("#div_rab").removeClass('force-table-responsive');
            </script>
@endsection