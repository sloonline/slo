@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>WBS/IO</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>Kontrak</li>
                    <li>WS/IO</li>
                    <li class="active">Pengalihan Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-header panel-controls bg-primary">
                            <h3><i class="fa fa-table"></i> List <strong>Data</strong></h3>
                        </div>
                        <div class="panel-content">
                            <div class="panel-content pagination2 table-responsive">
                                <div class="btn-group">
                                    <a href="{{ url('/') }}/internal/create_pengalihan_biaya"
                                       class="btn btn-success btn-square btn-block btn-embossed">
                                        <i class="fa fa-plus"></i> Tambah Pengalihan</a>
                                </div>
                                <table class="table table-hover table-dynamic">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No WBS</th>
                                        <th>Tanggal Pengalihan</th>
                                        <th>Nilai</th>
                                        <th>No Surat Pengalihan</th>
                                        <th>File Surat</th>
                                        <th>Keterangan</th>
                                        <th style="width: 170px;">Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach($pengalihan as $item)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{@$item->wbs->nomor}}</td>
                                            <td>{{date('d M Y',strtotime(@$item->tgl_pengalihan))}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                            <td>{{@$item->surat_pengalihan}}</td>
                                            <td>@if($item->file_pengalihan != null)
                                                    <button type="button"
                                                            onclick="window.location.href='{{url('upload/'.@$item->file_pengalihan)}}'"
                                                            class="btn btn-sm btn-primary btn-square btn-embossed">
                                                        <i
                                                                class="fa fa-download"></i>
                                                        Preview
                                                    </button>
                                                @endif</td>
                                            <th>{{@$item->keterangan}}</th>
                                            <td>
                                                <a href="{{url('/internal/create_pengalihan_biaya/'.@$item->id)}}"
                                                    class="btn btn-sm btn-warning btn-square btn-embossed"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Edit Pengaihan"><i
                                                            class="fa fa-pencil"></i></a>
                                                <a onclick="return confirm('Anda yakin menghapus pengalihan biaya {{@$item->wbs->nomor}} ?');"
                                                   href="{{url('/internal/delete_pengalihan_biaya/'.@$item->id)}}"
                                                   class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                            class="fa fa-trash"></i></a></td>
                                        </tr>
                                        <?php $no++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @endsection


            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <!-- Buttons Loading State -->

@endsection