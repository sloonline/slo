<div class="panel-body">
    <table border="1">
        <thead>
        <tr rowspan="2" style="text-align: center">
            <th colspan="12"><h1>MONITORING WBS</h1></th>
        </tr>
        <tr>
            <th colspan="12"></th>
        </tr>
        <tr style="background-color: #6db1f5;">
            <th style="text-align:center">NO</th>
            <th style="text-align:center">UNIT</th>
            <th style="text-align:center">URAIAN PEKERJAAN</th>
            <th style="text-align:center">NO. KONTRAK</th>
            <th style="text-align:center">TGL KONTRAK</th>
            <th style="text-align:center">NO. WBS</th>
            <th style="text-align:center">TAHUN</th>
            <th style="text-align:center">JENIS PEKERJAAN</th>
            <th style="text-align:center">NILAI</th>
            <th style="text-align:center">REALISASI</th>
            <th style="text-align:center">SISA NILAI PENUGASAN</th>
            <th style="text-align:center">STATUS</th>
        </tr>
        </thead>
        <tbody>
        <?php $no = 1 ?>
        @foreach($wbs as $item)
            <?php
            $saldo = \App\WbsIo::getColorWarning($item);
            ?>
            <tr {!! (@$item->statusWbsIo->nama_reference == LOCKED) ? 'style="background-color: #FEFFB5;"' : "" !!}>
                <td>{{$no}}</td>
                <td style="white-space: nowrap">{{@$item->businessArea->description}}</td>
                <td style="white-space: nowrap">{{@$item->uraian_pekerjaan}}</td>
                <td style="white-space: nowrap">{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->nomor_kontrak.'<br/>'.@$item->kontrak->latest_kontrak->uraian : "" !!}</td>
                <td style="white-space: nowrap">{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->tgl_kontrak : "" !!}</td>
                <td>{{@$item->nomor}}</td>
                <td>{{@$item->tahun}}</td>
                <td style="white-space: nowrap">{{@$item->jenis_pekerjaan->jenis_pekerjaan}}</td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format(@$item->realisasi->sum('jumlah') , 2, ',', '.')}}</td>
                <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                    <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                </td>
                <td>{{@$item->statusWbsIo->nama_reference}}</td>
            </tr>
            <?php $no++; ?>
        @endforeach
        </tbody>
    </table>
</div>