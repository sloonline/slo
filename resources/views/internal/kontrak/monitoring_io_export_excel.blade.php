<div class="panel-body">
    <table border="1">
        <thead>
        <tr rowspan="2" style="text-align: center">
            <th colspan="17"><h1>MONITORING INTERNAL ORDER</h1></th>
        </tr>
        <tr>
            <th colspan="17"></th>
        </tr>
        <tr style="background-color: #6db1f5;">
            <th style="text-align:center">NO</th>
            <th style="text-align:center">NOMOR IO</th>
            <th style="text-align:center">DESKRIPSI</th>
            <th style="text-align:center">ID PELANGGAN</th>
            <th style="text-align:center">NAMA PELANGGAN</th>
            <th style="text-align:center">JENIS PRODUK</th>
            <th style="text-align:center">NOMOR PROJECT</th>
            <th style="text-align:center">TAHUN PROJECT</th>
            <th style="text-align:center">URAIAN PEKERJAAN</th>
            <th style="text-align:center">NILAI PROJECT</th>
            <th style="text-align:center">JUMLAH PEMBAYARAN</th>
            <th style="text-align:center">JUMLAH BELUM DIBAYAR</th>
            <th style="text-align:center">KETERANGAN</th>
            <th style="text-align:center">REALISASI</th>
            <th style="text-align:center">SISA PLAFON</th>
            <th style="text-align:center">PROSENTASE</th>
            <th style="text-align:center">STATUS</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 1;
        ?>
        @foreach($io as $item)
            <?php
            $saldo = \App\WbsIo::getColorWarning($item);
            $pembayaran = ($item->kontrak == null || $item->kontrak->pembayaran == null) ? 0 : $item->kontrak->pembayaran->sum('nilai');
            ?>
            <tr {!! (@$item->statusWbsIo->nama_reference == LOCKED) ? 'style="background-color: #FEFFB5;"' : "" !!}>
                <td>{{$no}}</td>
                <td>{{@$item->nomor}}</td>
                <td>{{@$item->deskripsi}}</td>
                <td>{{@$item->perusahaan->id_pelanggan}}</td>
                <td>{{@$item->perusahaan->nama_perusahaan}}</td>
                <td style="white-space: nowrap">{{@$item->jenis_pekerjaan->jenis_pekerjaan}}</td>
                <td>{{@$item->nomor_project}}</td>
                <td>{{@$item->tahun}}</td>
                <td style="white-space: nowrap">{{@$item->uraian_pekerjaan}}</td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format($pembayaran, 2, ',', '.')}}</td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format((@$item->nilai - $pembayaran) , 2, ',', '.')}}</td>
                <td></td>
                <td style="white-space: nowrap;text-align: right;">
                    Rp. {{number_format(@$item->realisasi->sum('jumlah') , 2, ',', '.')}}</td>
                <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                    <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                </td>
                <td style="white-space: nowrap;text-align: right;">
                    {{number_format(round(($saldo['saldo'] / @$item->nilai) , 2) , 2, ',', '.')}}</td>
                <td>{{@$item->statusWbsIo->nama_reference}}</td>
            </tr>
            <?php $no++; ?>
        @endforeach
        </tbody>
    </table>
</div>