@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

<?php
$active_tab = WBS;
if (session('jenis') == IO) {
    $active_tab = IO;
}
?>
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>WBS/IO</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>Kontrak</li>
                    <li class="active">WBS/IO</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-header panel-controls bg-primary">
                            <h3><i class="fa fa-table"></i> List <strong>Data</strong></h3>
                        </div>
                        <div class="panel-content">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li {{($active_tab == WBS) ? 'class=active' : ""}}>
                                        <a href="#wbs" data-toggle="tab"><i class="icon-home"></i>
                                            WBS
                                        </a>
                                    </li>
                                    <li {{($active_tab == IO) ? 'class=active' : ""}}>
                                        <a href="#io" data-toggle="tab"><i class="icon-user"></i>
                                            IO
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane {{($active_tab == WBS) ? 'active' : ""}}" id="wbs">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="btn-group">
                                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO))
                                                    <a href="{{ url('/') }}/internal/create_wbs"
                                                       class="btn btn-success btn-square btn-block btn-embossed">
                                                        <i class="fa fa-plus"></i> Tambah WBS</a>
                                                @endif
                                            </div>
                                            <table class="table table-hover table-dynamic">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor WBS</th>
                                                    <th>Tipe WBS</th>
                                                    <th>Kontrak/Order</th>
                                                    {{-- <th>Unit</th> --}}
                                                    <th>Nilai</th>
                                                    <th>Saldo</th>
                                                    <th>Status WBS</th>
                                                    <th style="width: 170px;">Status Approval</th>
                                                    <th style="min-width: 170px;">Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $no = 1;
                                                ?>
                                                @foreach($wbs_io as $item)
                                                    @if(@$item->jenis->nama_reference == WBS)
                                                        <?php
                                                        $saldo = \App\WbsIo::getColorWarning($item);
                                                        ?>
                                                        <tr>
                                                            <td>{{$no}}</td>
                                                            <td>{{@$item->nomor}}</td>
                                                            <td>{{@$item->tipeWbs->nama_reference}}</td>
                                                            <td>{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->nomor_kontrak.'<br/>'.@$item->kontrak->latest_kontrak->uraian : @$item->order->nomor_order !!}</td>
                                                            {{-- <td style="min-width:100px;">{{@$item->businessArea->description}}</td> --}}
                                                            <td style="white-space: nowrap;text-align: right;">
                                                                Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                                            <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                                                                <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                                                            </td>
                                                            <td>{{@$item->statusWbsIo->nama_reference}}</td>
                                                            <td>{{@$item->flow_status->status }}</td>
                                                            <td style="white-space: nowrap;">
                                                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO))
                                                                    @if(\App\WbsIo::isAllowEdit($item))
                                                                        <a href="{{url('/internal/create_wbs/'.@$item->id)}}"
                                                                           class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                           data-rel="tooltip"
                                                                           data-placement="right"
                                                                           title="Edit WBS"><i
                                                                                    class="fa fa-pencil"></i></a>
                                                                    @endif
                                                                @endif
                                                                <a href="{{url('/internal/detail_wbs_io/'.@$item->id)}}"
                                                                   class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                            class="fa fa-eye"
                                                                            data-rel="tooltip"
                                                                            data-placement="right"
                                                                            title="Detail WBS"></i></a>
                                                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_ACTIVATE_WBSIO))
                                                                    @if(\App\WbsIo::isAllowActivate($item))
                                                                        <a href="{{url('/internal/update_status_wbs/'.@$item->id)}}"
                                                                           class="btn btn-sm btn-success btn-square btn-embossed"
                                                                           data-rel="tooltip"
                                                                           data-placement="right"
                                                                           title="Activate WBS"
                                                                           onclick="activate()"><i
                                                                                    class="fa fa-check"></i></a>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <?php $no++; ?>
                                                    @endif
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane {{($active_tab == IO) ? 'active' : ""}}" id="io">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="btn-group">
                                                @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO))
                                                    <a href="{{ url('/') }}/internal/create_io"
                                                       class="btn btn-success btn-square btn-block btn-embossed">
                                                        <i class="fa fa-plus"></i> Tambah IO</a>
                                                @endif
                                            </div>
                                            <div style="overflow-y:hidden;overflow-x:auto;">
                                                <table class="table table-hover table-dynamic">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor IO</th>
                                                        <th>Tipe IO</th>
                                                        <th>Kontrak/Order</th>
                                                        <th style="width: 150px;">Perusahaan</th>
                                                        <th>Nilai</th>
                                                        <th>Saldo</th>
                                                        <th>Status IO</th>
                                                        <th style="width: 170px;">Status Approval</th>
                                                        <th style="min-width: 170px;">Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $no = 1;
                                                    ?>
                                                    @foreach($wbs_io as $item)
                                                        @if(@$item->jenis->nama_reference == IO)
                                                            <?php
                                                            $saldo = \App\WbsIo::getColorWarning($item);
                                                            ?>
                                                            <tr>
                                                                <td>{{$no}}</td>
                                                                <td>{{@$item->nomor}}</td>
                                                                <td>{{@$item->tipeWbs->nama_reference}}</td>
                                                                <td>{!! (@$item->kontrak != null) ? @$item->kontrak->latest_kontrak->nomor_kontrak.'<br/>'.@$item->kontrak->latest_kontrak->uraian : @$item->order->nomor_order !!}</td>
                                                                <td>{{@$item->perusahaan->nama_perusahaan}}</td>
                                                                <td style="white-space: nowrap;text-align: right;">
                                                                    Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                                                <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                                                                    <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                                                                </td>
                                                                <td>{{@$item->statusWbsIo->nama_reference}}</td>
                                                                <td>{{@$item->flow_status->status }}</td>
                                                                <td>
                                                                    @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO))
                                                                        @if(@$item->statusWbsIo->nama_reference  != LOCKED || ($saldo['persen_realisasi'] < 90 && $saldo['nilai'] > 0))
                                                                            @if(\App\WbsIo::isAllowEdit($item))
                                                                                <a href="{{url('/internal/create_io/'.@$item->id)}}"
                                                                                   class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                                                            class="fa fa-pencil"
                                                                                            data-rel="tooltip"
                                                                                            data-placement="right"
                                                                                            title="Edit IO"></i></a>
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                    <a href="{{url('/internal/detail_wbs_io/'.@$item->id)}}"
                                                                       class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                class="fa fa-eye"
                                                                                data-rel="tooltip"
                                                                                data-placement="right"
                                                                                title="Detail IO"></i></a>

                                                                    @if(\App\User::isCurrUserAllowPermission(PERMISSION_ACTIVATE_WBSIO))
                                                                        @if(\App\WbsIo::isAllowActivate($item))
                                                                            <a href="{{url('/internal/update_status_wbs/'.@$item->id)}}"
                                                                               class="btn btn-sm btn-success btn-square btn-embossed"
                                                                               data-rel="tooltip"
                                                                               data-placement="right"
                                                                               title="Activate IO"
                                                                               onclick="activate()"><i
                                                                                        class="fa fa-check"></i></a>
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable({
                        "scrollX": true,
                    });

                });

                function activate() {
                    return confirm("Anda yakin melakukan aktivasi ?");
                }

            </script>
@endsection
