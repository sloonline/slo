@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Kontrak</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Kontrak</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_KONTRAK))
                            <div class="btn-group">
                                <a href="{{ url('/internal/create_kontrak') }}"
                                class="btn btn-success btn-square btn-block btn-embossed">
                                    <i class="fa fa-plus"></i> Tambah Kontrak</a>
                            </div>
                        @endif
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Kontrak</th>
                                <th>Tanggal Kontrak</th>
                                <th>Peminta Jasa</th>
                                <th>Instalasi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($kontrak) && (is_array($kontrak) || is_object($kontrak)))
                                <?php $no = 1; ?>
                                @foreach($kontrak as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td style="white-space: nowrap;">{{ @$item->latest_kontrak->nomor_kontrak }}</td>
                                        <td style="white-space: nowrap;">{{ date('d-M-Y',strtotime(@$item->latest_kontrak->tgl_kontrak ))}}</td>
                                        <td>{{ @$item->perusahaan->nama_perusahaan }}</td>
                                        <td>
                                            <ul style="list-style-type: none;">
                                                @foreach(@$item->nama_instalasi as $row)
                                                    <li>{{$row}}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ @$item->flow_status->status }}</td>
                                        <td width="15%">
                                            <a data-rel="tooltip" data-placement="top" title="Detail Kontrak"
                                               href="{{url('/internal/detail_kontrak/'.@$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"></i></a>
                                            @if(@\App\Kontrak::isAllowEdit($item))
                                                <a data-rel="tooltip" data-placement="top" title="Edit Kontrak"
                                                   href="{{url('/internal/create_kontrak/'.@$item->id)}}"
                                                   class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                            class="fa fa-pencil"></i></a>
                                            @endif
                                            {{--<a data-rel="tooltip" data-placement="top" title="Amandemen Kontrak"--}}
                                            {{--href="{{url('/internal/create_kontrak/'.@$item->id)}}"--}}
                                            {{--class="btn btn-sm btn-warning btn-square btn-embossed"><i--}}
                                            {{--class="fa fa-pencil"></i></a>--}}
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

@endsection