@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
    <div class="header">
        {{--<h2><strong>RAB</strong> Permohonan {{$permohonan->nomor_permohonan}}</h2>--}}
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{url('/internal')}}">Dashboard</a></li>
                <li><a href="{{url('/internal/kontrak')}}">Kontrak</a></li>
                <li class="active">New</li>
            </ol>
        </div>
    </div>
    {!! Form::open(array('url'=> '/internal/save_amandemen', 'files'=> true, 'class'=> 'form-horizontal')) !!}
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header panel-controls bg-primary">
                    <h3><i class="fa fa-table"></i> Form Input <strong>Kontrak</strong></h3>
                </div>
                <div class="panel-content">
                    {{-- start form pilih permohonan--}}
                    <div class="panel-group panel-accordion" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                    href="#collapseOne">
                                    <i class="icon-plus"></i> Pilih Rancangan Biaya
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table class="table table-hover table-dynamic" id="table_rab">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nomor RAB</th>
                                                    <th>Nomor Order</th>
                                                    <th>Tanggal RAB</th>
                                                    <th>Total Biaya</th>
                                                    <th>Peminta Jasa</th>
                                                    <th>Pilih</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1; ?>
                                                {{--list used rab--}}
                                                @foreach($allRab as $item)
                                                <tr>
                                                    <td>{{ $no}}</td>
                                                    <td>{{ @$item["rab"]->no_dokumen}}</td>
                                                    <td>{{ @$item["rab"]->order->nomor_order}}</td>
                                                    <td>{{ date('d M Y',strtotime(@$item["rab"]->created_at)) }}</td>
                                                    <td>{{ @$item["rab"]->total_biaya}}</td>
                                                    <td>{{ @$item["rab"]->order->created_by}}</td>
                                                    <td>
                                                        <input type="checkbox" name="pilih_rab[]"
                                                        biaya="{{ @$item["rab"]->total_biaya}}"
                                                        {{(@$item['val'] > 0) ? "checked='checked'" : ""}}
                                                        value="{{@$item["rab"]->id}}"
                                                        class="form-control pilih_rab">
                                                    </td>
                                                </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group panel-accordion" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseTwo">
                                <i class="icon-plus"></i> Data Kontrak
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="nav-tabs2" id="tabs">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#kontrak" data-toggle="tab">
                                                    Kontrak
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <div class="tab-pane active" id="kontrak">
                                                {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor
                                                            Kontrak</label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-sm-12 prepend-icon">
                                                                <input type="text" required="required"
                                                                name="nomor_kontrak"
                                                                class="form-control form-white"
                                                                placeholder="Nomor Kontrak">
                                                                <i class="icon-list"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Tanggal
                                                                Kontrak</label>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="col-sm-12 prepend-icon">
                                                                    <input type="text" required="required"
                                                                    name="tanggal_kontrak"
                                                                    class="form-control b-datepicker form-white"
                                                                    data-date-format="dd-mm-yyyy"
                                                                    data-lang="en"
                                                                    data-RTL="false"
                                                                    placeholder="Tanggal">
                                                                    <i class="icon-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">Deskripsi
                                                                    Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <textarea class="form-control form-white" rows="4"
                                                                        name="deskripsi_kontrak"
                                                                        placeholder="Deskripsi Kontrak"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-3">
                                                                    <label class="col-sm-12 control-label">Nilai
                                                                        Kontrak</label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="col-sm-12 prepend-icon">
                                                                            <input type="number" min="0" required="required"
                                                                            name="nilai_kontrak"
                                                                            class="form-control form-white"
                                                                            placeholder="Nilai Kontrak">
                                                                            <i class="glyphicon glyphicon-ok"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-3">
                                                                        <label class="col-sm-12 control-label">File
                                                                            Kontrak</label>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="col-sm-12 prepend-icon">
                                                                                <input type="file" required="required"
                                                                                name="file_kontrak"
                                                                                class="form-control form-white"
                                                                                placeholder="File Kontrak">
                                                                                <i class="glyphicon glyphicon-paperclip"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-3">
                                                                            <label class="col-sm-12 control-label">Keterangan Amandemen</label>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="col-sm-12 prepend-icon">
                                                                                <textarea class="form-control form-white" rows="4"
                                                                                name="keterangan_amandemen" required
                                                                                placeholder="Keterangan Amandemen"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-3">
                                                                            <label class="col-sm-12 control-label">Landasan Amandemen</label>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="col-sm-12 prepend-icon">
                                                                                <input type="file" required="required"
                                                                                name="file_amandemen"
                                                                                class="form-control form-white"
                                                                                placeholder="File Amandemen">
                                                                                <i class="glyphicon glyphicon-paperclip"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <div class="pull-right">
                                        <a onclick="return window.history.back()"
                                        class="btn btn-warning btn-square btn-embossed">Kembali <i
                                        class="icon-ban"></i></a>
                                        <button type="submit" class="btn btn-primary btn-square btn-embossed">Simpan <i
                                            class="fa fa-save"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="modal fade" id="modal-aggreement" style="margin-top: -100px;" tabindex="-1"
                    role="dialog"
                    aria-hidden="true">
                    <div class="modal-dialog" style="width: 50%;">
                        <div class="modal-content">
                            <div class="modal-header" style="background-color: teal;color:white;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                                    <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-3"><label>Termin</label></div>
                                            <div class="col-md-6"><input type="text" placeholder="Termin"
                                                class="form-control form-white"></div>
                                            </div>
                                            <br><br>
                                            <div class="form-group">
                                                <div class="col-md-3"><label>Presentase</label></div>
                                                <div class="col-md-6"><input type="text" placeholder="Presentase"
                                                    class="form-control form-white"></div>
                                                </div>
                                                <br><br>
                                                <div class="form-group">
                                                    <div class="col-md-3"><label>Nilai</label></div>
                                                    <div class="col-md-6"><input type="text" placeholder="Nilai"
                                                        class="form-control form-white"></div>
                                                    </div>
                                                    <br><br>
                                                    <div class="col-md-9">
                                                        <button class="btn btn-primary btn-square btn-embossed pull-right">
                                                            <i class="fa fa-save"></i> Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endsection

                                @section('page_script')
                                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                                <!-- Tables Filtering, Sorting & Editing -->
                                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                                <!-- Buttons Loading State -->
                                <script>

                                calculateNilaiKontrak();

                                $("#btn_tambah").on('click', function () {
                                    $("#modal-aggreement").modal("show");
                                });

                                $("input").unbind('keydown');

                                $('.pilih_rab').on('ifChecked', function () {
                                    console.log('check');
                                    calculateNilaiKontrak();
                                });

                                $('.pilih_rab').on('ifUnchecked', function () {
                                    console.log('uncheck');
                                    calculateNilaiKontrak();
                                });

                                function calculateNilaiKontrak() {
                                    console.log('calculate contract value');
                                    var sum = 0;
                                    $('#table_rab').find('tr').each(function () {
                                        var row = $(this);
                                        if (row.find('input[type="checkbox"]').is(':checked')) {
                                            sum += parseFloat(row.find('input[type="checkbox"]').attr('biaya'));
                                        }
                                    });
                                    $("input[name=nilai_kontrak]").val(sum);
                                }
                                
                                </script>
                                @endsection
