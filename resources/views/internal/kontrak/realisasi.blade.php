@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Realisasi <strong>WBS/IO</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>Kontrak</li>
                    <li class="active">Realisasi WBS/IO</li>
                </ol>
            </div>
        </div>
        <?php
        $active_tab = WBS;
        if (isset($jenis) && $jenis == IO) {
            $active_tab = IO;
        }
        ?>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel row">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i><strong>Realisasi</strong></h3>
                    </div>
                    <div class="col-md-12">
                        <div class="panel-content ">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li {{($active_tab == WBS) ? 'class=active' : ""}}>
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            Realisasi WBS
                                        </a>
                                    </li>
                                    <li {{($active_tab == IO) ? 'class=active' : ""}}>
                                        <a href="#realisasi" data-toggle="tab"><i class="icon-user"></i>
                                            Realisasi IO
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane {{($active_tab == WBS) ? 'active' : ""}}" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            {!! Form::open(['url'=>'internal/save_realisasi','files'=> true,'id'=>'form_register', 'class'=>'form-inline', 'role'=>'form']) !!}
                                            <input type="hidden" name="jenis" value={{WBS}}>
                                            <div class="col-md-2"></div>
                                            <fieldset class="cart-summary col-md-8">
                                                <legend>File SAP - WBS</legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input name="file_realisasi" type="file" class="form-control"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <button class="btn btn-success">
                                                            <i class="fa fa-paper-plane"></i> Upload
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <a href="{{url('upload/'.@$template_wbs)}}"
                                                           class="btn btn-primary">
                                                            <i class="fa fa-download"></i> Template
                                                        </a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="col-md-2"></div>
                                            {!! Form::close() !!}
                                            @if(isset($realisasi) && isset($jenis))
                                                @if($jenis==WBS)
                                                    <table class="table table-hover table-dynamic">
                                                        <thead>
                                                        <tr>
                                                            <th>Posting Date</th>
                                                            <th>Nomor WBS</th>
                                                            <th>Jumlah</th>
                                                            <th>Cost Element</th>
                                                            <th>Deskripsi</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($realisasi as $item)
                                                            <tr>
                                                                <td>{{@$item['data']->posting_date}}</td>
                                                                <td>{{@$item['data']->nomor_wbs_io}}</td>
                                                                <td>{{@$item['data']->jumlah}}</td>
                                                                <td>{{@$item['data']->cost_element}}</td>
                                                                <td>{{@$item['data']->deskripsi}}</td>
                                                                <td>{{@$item['status']}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <div class="tab-pane {{($active_tab == IO) ? 'active' : ""}}" id="realisasi">
                                        <div class="panel-content pagination2 table-responsive">
                                            {!! Form::open(['url'=>'internal/save_realisasi','files'=> true,'id'=>'form_register', 'class'=>'form-inline', 'role'=>'form']) !!}
                                            <input type="hidden" name="jenis" value={{IO}}>
                                            <div class="col-md-2"></div>
                                            <fieldset class="cart-summary col-md-8">
                                                <legend>File SAP - IO</legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input name="file_realisasi" type="file" class="form-control"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <button class="btn btn-success">
                                                            <i class="fa fa-paper-plane"></i> Upload
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group">
                                                        <a href="{{url('upload/'.@$template_io)}}"
                                                           class="btn btn-primary">
                                                            <i class="fa fa-download"></i> Template
                                                        </a>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="col-md-2"></div>
                                            {!! Form::close() !!}
                                            @if(isset($realisasi) && isset($jenis))
                                                @if($jenis == IO)
                                                    <table class="table table-hover table-dynamic">
                                                        <thead>
                                                        <tr>
                                                            <th>Posting Date</th>
                                                            <th>Nomor IO</th>
                                                            <th>Jumlah</th>
                                                            <th>Cost Element</th>
                                                            <th>Deskripsi</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($realisasi as $item)
                                                            <tr>
                                                                <td>{{@$item['data']->posting_date}}</td>
                                                                <td>{{@$item['data']->nomor_wbs_io}}</td>
                                                                <td>{{@$item['data']->jumlah}}</td>
                                                                <td>{{@$item['data']->cost_element}}</td>
                                                                <td>{{@$item['data']->deskripsi}}</td>
                                                                <td>{{@$item['status']}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer clearfix bg-white">

                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel row">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i><strong>Data Realisasi</strong></h3>
                    </div>
                    <div class="panel-content ">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <label class="col-sm-12 control-label">Filter</label>
                                </div>
                                <div class="col-sm-2">
                                    <select name="tipe" id="tipe"
                                           class="form-control form-white">
                                        <option value="">--All Tipe (WBS/IO)--</option>
                                        <option value="{{WBS}}">{{WBS}}</option>
                                        <option value="{{IO}}">{{IO}}</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="nomor" id="nomor"
                                            class="form-control form-white" placeholder="Nomor WBS/IO"/>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" required="required"
                                           name="tgl_awal" id="tgl_awal"
                                           class="form-control b-datepicker form-white"
                                           data-date-format="mm-dd-yyyy"
                                           data-lang="en"
                                           data-RTL="false"
                                           placeholder="Tanggal Awal">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" required="required"
                                           name="tgl_akhir" id="tgl_akhir"
                                           class="form-control b-datepicker form-white"
                                           data-date-format="mm-dd-yyyy"
                                           data-lang="en"
                                           data-RTL="false"
                                           placeholder="Tanggal Akhir">
                                </div>
                            </div>
                        </div>
                        <br/>
                        <hr/>
                        <table class="table table-hover table-dynamic" id="table-history">
                            <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Nomor</th>
                                <th>Tanggal Upload</th>
                                <th>Posting Date</th>
                                <th>Cost Element</th>
                                <th>Jumlah</th>
                                <th>Deskripsi</th>
                                <th>Uploaded By</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(@$data_realisasi as $item)
                                <tr>
                                    <td>{{@$item->wbs_io->jenis->nama_reference}}</td>
                                    <td>{{@$item->wbs_io->nomor}}</td>
                                    <td>{{date('d M Y',strtotime(@$item->tgl_upload))}}</td>
                                    <td>{{date('d M Y',strtotime(@$item->posting_date))}}</td>
                                    <td>{{@$item->cost_element}}</td>
                                    <td style="text-align: right;">
                                        Rp. {{number_format(@$item->jumlah, 2, ',', '.')}}</td>
                                    <td>{{@$item->deskripsi}}</td>
                                    <td>{{@$item->username}}</td>
                                    <td>
                                        <a onclick="return confirm('Anda yakin menghapus realisasi {{@$item->deskripsi}} ?');"
                                           href="{{url('/internal/delete_realisasi/'.@$item->id)}}"
                                           class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                    class="fa fa-trash"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $.fn.dataTable.ext.search.push(
                        function (settings, data, dataIndex) {
                            var min = $('#tgl_awal').val();
                            var max = $('#tgl_akhir').val();
                            var tipe = $("#tipe option:selected").val();
                            var nomor = $("#nomor").val();
                            var tgl = new Date(data[3]);
                            if (min != "") min = new Date(min);
                            if (max != "") max = new Date(max);
                            if ((tipe == "" || tipe == data[0]) && (nomor == "" || data[1].toLowerCase().indexOf(nomor.toLowerCase()) >= 0) && (( min == "" && max == "" ) ||
                                    ( min == "" && tgl <= max ) ||
                                    ( min <= tgl && max == ""  ) ||
                                    ( min <= tgl && tgl <= max )) ){
                                return true;
                            }
                            return false;
                        }
                );
                $(document).ready(function () {
                    var table = $('#table-history').DataTable({
                        'aaSorting': [],
                        'bRetrieve' : true
                    });

                    // Event listener to the two range filtering inputs to redraw on input
                    $('#tgl_awal, #tgl_akhir,#tipe,#nomor').change(function () {
                        table.draw();
                    });
                    $('#nomor').keyup(function () {
                        table.draw();
                    });
                });
            </script>
@endsection