@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            {{--<h2><strong>RAB</strong> Permohonan {{$permohonan->nomor_permohonan}}</h2>--}}
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/kontrak')}}">Kontrak</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        {!! Form::open(array('url'=> '/internal/save_kontrak', 'files'=> true, 'class'=> 'form-horizontal')) !!}
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Form Input <strong>Kontrak</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{-- start form pilih permohonan--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne">
                                            <i class="icon-plus"></i> Pilih Rancangan Biaya
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-hover table-dynamic">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor RAB</th>
                                                        <th>Nomor Order</th>
                                                        <th>Tanggal RAB</th>
                                                        <th>Total Biaya</th>
                                                        <th>Peminta Jasa</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $no = 1; ?>
                                                    {{--list used rab--}}
                                                    @foreach($allRab as $item)
                                                        @if($item['val'] > 0)
                                                            <tr>
                                                                <td>{{ $no}}</td>
                                                                <td>{{ @$item["rab"]->no_dokumen}}</td>
                                                                <td>{{ @$item["rab"]->order->nomor_order}}</td>
                                                                <td>{{ date('d M Y',strtotime(@$item["rab"]->created_at)) }}</td>
                                                                <td>{{ @$item["rab"]->total_biaya}}</td>
                                                                <td>{{ @$item["rab"]->order->created_by}}</td>
                                                            </tr>
                                                        @endif
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo">
                                            <i class="icon-plus"></i> Data Kontrak
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="nav-tabs2" id="tabs">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#kontrak" data-toggle="tab">
                                                                Kontrak
                                                            </a>
                                                        </li>
                                                        @if($kontrak_id != 0)
                                                            <li>
                                                                <a href="#amandemen_kontrak" data-toggle="tab">
                                                                    Amandemen Kontrak
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                    <div class="tab-content bg-white">
                                                        <div class="tab-pane active" id="kontrak">
                                                            {!! Form::hidden('id', $kontrak_id) !!}
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Nomor
                                                                        Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" required="required"
                                                                               name="nomor_kontrak"
                                                                               value="{{($latest != null) ? $latest->nomor_kontrak : ''}}"
                                                                               class="form-control form-white"
                                                                               placeholder="Nomor Kontrak">
                                                                        <i class="icon-list"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Tanggal
                                                                        Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" required="required"
                                                                               name="tanggal_kontrak"
                                                                               value="{{($latest != null) ? date('d-m-Y',strtotime($latest->tgl_kontrak)): ''}}"
                                                                               class="form-control b-datepicker form-white"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               data-lang="en"
                                                                               data-RTL="false"
                                                                               placeholder="Tanggal">
                                                                        <i class="icon-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Masa Berlaku
                                                                        Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" required="required"
                                                                               name="masa_berlaku_kontrak"
                                                                               value="{{($latest != null) ? date('d-m-Y',strtotime($latest->masa_berlaku_kontrak)): ''}}"
                                                                               class="form-control b-datepicker form-white"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               data-lang="en"
                                                                               data-RTL="false"
                                                                               placeholder="Masa Berlaku Kontrak">
                                                                        <i class="icon-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Deskripsi
                                                                        Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                                <textarea disabled
                                                                                          class="form-control form-white"
                                                                                          rows="4"
                                                                                          name="deskripsi_kontrak"
                                                                                          placeholder="Deskripsi Kontrak">{{($latest != null) ? $latest->uraian : ''}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">Nilai
                                                                        Kontrak</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text"
                                                                               required="required"
                                                                               name="nilai_kontrak"
                                                                               class="form-control form-white"
                                                                               value="{{number_format(@$latest->nilai_kontrak, 0, '.', ',')}}"
                                                                               placeholder="Nilai Kontrak">
                                                                        <i class="glyphicon glyphicon-ok"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-2">
                                                                    <label class="col-sm-12 control-label">File
                                                                        Kontrak</label>
                                                                </div>
                                                                @if($latest != null && $latest->file != null)
                                                                    <div class="col-sm-1">
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.$latest->file)}}"
                                                                        >
                                                                            <button type="button"
                                                                                    class="btn btn-primary"><i
                                                                                        class="fa fa-download"></i>View
                                                                            </button>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                                @if($kontrak_id == 0)
                                                                    <div class="col-md-3">
                                                                        <div class="col-sm-12 prepend-icon">
                                                                            <input type="file" required="required"
                                                                                   name="file_kontrak"
                                                                                   class="form-control form-white"
                                                                                   placeholder="File Kontrak">
                                                                            <i class="glyphicon glyphicon-paperclip"></i>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            @if(@$kontrak->sys_status != null && @$kontrak->sys_status != "")
                                                                <div class="form-group">
                                                                    <div class="col-sm-2">
                                                                        <label class="col-sm-12 control-label">Status</label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="col-sm-12">
                                                                            <input type="text" disabled
                                                                                   class="form-control form-white"
                                                                                   value="{{@$kontrak->sys_status}}"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br/><br/>
                                                            @endif
                                                        </div>
                                                        <div class="tab-pane" id="amandemen_kontrak">
                                                            <div class="btn-group">
                                                                <a href="{{ url('/internal/create_amandemen/'.$kontrak_id) }}">
                                                                    <button type="button"
                                                                            class=" btn btn-success btn-square btn-block
                                                                            btn-embossed">
                                                                        <i class="fa fa-plus"></i> Amandemen Kontrak
                                                                    </button>
                                                                </a>
                                                            </div>
                                                            <table class="table table-hover table-dynamic">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nomor Kontrak</th>
                                                                    <th>Tanggal Amandemen</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Nilai Kontrak</th>
                                                                    <th>Amandemen ke</th>
                                                                    <th>Aksi</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $no = 1;?>
                                                                @foreach($kontrakVersion as $item)
                                                                    <tr>
                                                                        <td>{{$no}}</td>
                                                                        <td>{{@$item->nomor_kontrak}}</td>
                                                                        <td>{{date('d M Y',strtotime(@$item->tgl_kontrak))}}</td>
                                                                        <td>{{@$item->uraian}}</td>
                                                                        <td>{{number_format(@$item->nilai_kontrak, 0, '.', ',')}}</td>
                                                                        <td>{{@$item->version}}</td>
                                                                        <td>
                                                                            <a href="{{url('/internal/detail_amandemen/'.@$item->id)}}">
                                                                                <button type="button"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                    <i
                                                                                            class="fa fa-eye"></i>
                                                                                </button>
                                                                            </a></td>
                                                                    </tr>
                                                                    <?php $no++;?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        @if($is_last_approval)
                            @if($needPayment)
                                <div class="panel-group panel-accordion" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseThree">
                                                    <i class="icon-plus"></i> Pembayaran
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse">
                                            <div class="panel-body" style="background-color: white">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_KONTRAK))
                                                            <button id="btn_tambah" type="button"
                                                                    class="btn btn-success btn-square btn-embossed">
                                                                <i class="fa fa-plus"></i> Tambah Termin
                                                            </button>
                                                        @endif
                                                        <table id="table_termin"
                                                               class="table table-hover table-dynamic">
                                                            <thead>
                                                            <tr>
                                                                <th>Termin</th>
                                                                <th>Persentase</th>
                                                                <th>Nilai</th>
                                                                <th>Status</th>
                                                                <th>Invoice</th>
                                                                <th>Bukti Transfer</th>
                                                                <th>Bukti PPH23</th>
                                                                <th>Kwitansi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $no = 1;?>
                                                            @foreach($pembayaran as $item)
                                                               <?php
                                                                $item->tanggal_surat = ($item->tanggal_surat != null) ? date('d-m-Y',strtotime($item->tanggal_surat)) : '';
                                                                $item->tanggal_kuitansi_pph23 = ($item->tanggal_kuitansi_pph23 != null) ? date('d-m-Y',strtotime($item->tanggal_kuitansi_pph23)) : '';
                                                                $item->tanggal_kuitansi_transfer =($item->tanggal_kuitansi_transfer != null) ?  date('d-m-Y',strtotime($item->tanggal_kuitansi_transfer)) : '';
                                                                ?>
                                                                <tr>
                                                                    <td>{{@$item->termin_ke}}</td>
                                                                    <td>{{@$item->persentase}}</td>
                                                                    <td>{{number_format(@$item->nilai, 0, '.', ',')}}</td>
                                                                    <td>{{@$item->status}}</td>
                                                                    <td>
                                                                        <input type="hidden" name="nilai"
                                                                               joss="{{@$item->nilai}}">
                                                                        @if(@$item->status == UNPAID && \App\User::isCurrUserAllowPermission(PERMISSION_REQUEST_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="window.location.href='{{url('internal/request-invoice/'.@$item->id)}}'"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-paper-plane"></i>
                                                                                REQUEST
                                                                                INVOICE
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == INVOICE_REQUESTED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="modal_invoice('{{@$item->id}}')"
                                                                                    class="btn btn-sm btn-warning btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-file"></i>
                                                                                CREATE
                                                                                INVOICE
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == INVOICE_CREATED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="modal_invoice('{{@$item->id}}','{{ json_encode(@$item) }}')"
                                                                                    class="btn btn-sm btn-warning btn-square btn-embossed">
                                                                                <i class="fa fa-pencil"></i>
                                                                                CHANGE
                                                                                INVOICE
                                                                            </button><br/>
                                                                        @endif
                                                                        @if(@$item->file_invoice != null)
                                                                            <button type="button"
                                                                                    onclick="window.open('{{url('upload/'.@$item->file_invoice)}}', '_blank')"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                INVOICE
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == INVOICE_CREATED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="return confirm('Anda yakin mengirim invoice?')? window.location.href='{{url('internal/send-invoice/'.@$item->id)}}': '';"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i class="fa fa-paper-plane"></i>
                                                                                SEND
                                                                                INVOICE
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if(@$item->bukti != null)
                                                                            <button type="button"
                                                                                    onclick="window.location.href='{{url('upload/'.@$item->bukti)}}'"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                BUKTI TRF
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if(@$item->bukti_pph != null)
                                                                            <button type="button"
                                                                                    onclick="window.location.href='{{url('upload/'.@$item->bukti_pph)}}'"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                PPH 23
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td style="white-space: nowrap;">
                                                                        @if(@$item->status == BUKTI_UPLOADED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="modal_kuitansi('{{@$item->id}}')"
                                                                                    class="btn btn-sm btn-warning btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-file"></i> CREATE
                                                                                KWITANSI
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == KUITANSI_CREATED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                                <button type="button"
                                                                                        onclick="modal_kuitansi('{{@$item->id}}','{{ json_encode(@$item) }}')"
                                                                                        class="btn btn-sm btn-warning btn-square btn-embossed">
                                                                                    <i class="fa fa-pencil"></i> CHANGE
                                                                                    KWITANSI
                                                                                </button><br/><br/>
                                                                        @endif
                                                                        @if(@$item->file_kuitansi_transfer != null)
                                                                            <button type="button"
                                                                                    onclick="window.location.href='{{url('upload/'.@$item->file_kuitansi_transfer)}}'"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                KWITANSI TRF
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->file_kuitansi_pph23 != null)
                                                                            <button type="button"
                                                                                    onclick="window.location.href='{{url('upload/'.@$item->file_kuitansi_pph23)}}'"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                KWITANSI PPh 23
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == KUITANSI_CREATED && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="return confirm('Anda yakin mengirim kwitansi ?')? window.location.href='{{url('internal/send-kuitansi/'.@$item->id)}}' : '';"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-paper-plane"></i>
                                                                                SEND
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == KUITANSI_SENT && \App\User::isCurrUserAllowPermission(PERMISSION_CREATE_INVOICE))
                                                                            <button type="button"
                                                                                    onclick="return confirm('Anda yakin mengubah status PAID ?')? window.location.href='{{url('internal/paid_pembayaran/'.@$item->id)}}' : '';"
                                                                                    class="btn btn-sm btn-success btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-check"></i> PAID
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->status == INVOICE_SENT && Auth::user()->getRolesByName("PJ"))
                                                                            <button type="button" value="{{@$item->id}}"
                                                                                    onclick="modal_pembyaran({{@$item->id}})"
                                                                                    class="btn btn-sm btn-warning btn-square btn-embossed btn_upload">
                                                                                <i
                                                                                        class="fa fa-file-text"></i>
                                                                                UPLOAD
                                                                                BUKTI
                                                                            </button>
                                                                            {{--@else--}}
                                                                            {{--<p>-</p>--}}
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <?php $no++;?>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($kontrak->jadwal != null)
                                <div class="panel-group panel-accordion" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseFour">
                                                    <i class="icon-plus"></i> Jadwal Pelaksanaan
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse">
                                            <div class="panel-body" style="background-color: white">
                                                <div class="row">
                                                    {!! Form::open(array('url'=> '/internal/save_jadwal_pelaksanaan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                                    {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-4">
                                                                <label class="col-sm-12 control-label">Tanggal
                                                                    Mulai</label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="append-icon">
                                                                    <input {{($kontrak->jadwal!=null)?'value='.$kontrak->jadwal->tanggal_mulai.' readonly':''}}
                                                                           name="tanggal_mulai" type="text" required
                                                                           class="form-control b-datepicker form-white"
                                                                           data-date-format="dd-mm-yyyy"
                                                                           data-lang="en"
                                                                           data-RTL="false"
                                                                           placeholder="Tanggal">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-4">
                                                                <label class="col-sm-12 control-label">Tanggal
                                                                    selesai</label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="append-icon">
                                                                    <input {{($kontrak->jadwal!=null)?'value='.$kontrak->jadwal->tanggal_selesai.' readonly':''}}
                                                                           name="tanggal_selesai" type="text" required
                                                                           class="form-control b-datepicker form-white"
                                                                           data-date-format="dd-mm-yyyy"
                                                                           data-lang="en"
                                                                           data-RTL="false"
                                                                           placeholder="Tanggal">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <p class="col-sm-12 control-label">*) Tanggal di atas
                                                                    tidak
                                                                    dapat melewati masa berlaku kontrak</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <div class="col-sm-4">
                                                                <label class="col-sm-12 control-label">File
                                                                    Jadwal</label>
                                                            </div>
                                                            @if($kontrak->jadwal != null && $kontrak->jadwal->file_jadwal != null)
                                                                <div class="col-sm-1">
                                                                    <a target="_blank"
                                                                       href="{{url('upload/'.$kontrak->jadwal->file_jadwal)}}"
                                                                    >
                                                                        <button type="button"
                                                                                class="btn btn-primary"><i
                                                                                    class="fa fa-download"></i>View
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                            @else
                                                                <div class="col-sm-8">
                                                                    <div class="append-icon">
                                                                        <input name="file_jadwal" type="file"
                                                                               required="required"
                                                                               class="form-control form-white"
                                                                               placeholder="File Kontrak">
                                                                        <i class="glyphicon glyphicon-paperclip"></i>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-4">
                                                                <label class="col-sm-12 control-label">Keterangan</label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="append-icon">
                                            <textarea name="keterangan" class="form-control form-white"
                                                      {{($kontrak->jadwal!=null)?'readonly':''}}
                                                      rows="4"
                                                      placeholder="Keterangan">{{($kontrak->jadwal!=null)?$kontrak->jadwal->keterangan:''}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="bg-white pull-right">
                                                            @if($kontrak->jadwal == null)
                                                                <button type="submit"
                                                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                                                        data-style="zoom-in">Simpan &nbsp;<i
                                                                            class="glyphicon glyphicon-floppy-saved"></i>
                                                                </button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                    @include('workflow_view')
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-termin" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '/internal/save_pembayaran', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        {!! Form::hidden('status', UNPAID) !!}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nilai Kontrak</label></div>
                                    <div class="col-md-3"><label id="md_nilai_kontrak">Rp. </label></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Sisa Termin</label></div>
                                    <div class="col-md-3"><label
                                                id="sisa_termin">Rp. {{number_format(@$sisa_termin, 0, '.', ',')}}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Total Termin</label></div>
                                    <div class="col-md-3"><label id="total_termin"></label></div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Termin Ke</label></div>
                                    <div class="col-md-9"><input type="number" min="1" placeholder="Termin"
                                                                 value="{{@$termin_ke}}"
                                                                 name="termin" readonly
                                                                 class="form-control form-white" required></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Persentase</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Persentase" required
                                                                 onkeyup="persentase()"
                                                                 name="persentase_termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nilai</label></div>
                                    <div class="col-md-9"><input type="number" min="0" placeholder="Nilai" required
                                                                 name="nilai_termin" readonly
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <label>Keterangan</label>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="append-icon">
                                                <textarea name="keterangan" class="form-control form-white"
                                                          required
                                                          rows="4"
                                                          placeholder="Keterangan"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-upload" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '/internal/save_bukti_pembayaran', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        <input type="hidden" name="pembayaran_id" id="pembayaran_id">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Termin</label></div>
                                    <div class="col-md-9"><input type="file"
                                                                 name="bukti"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-invoice" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Upload</strong> Invoice</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '/internal/store-invoice', 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-invoice')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        <input type="hidden" name="pembayaran_id_invoice" id="pembayaran_id_invoice" value="">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nomor Surat *</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Nomor Surat" required
                                                                 name="nomor_surat" id="nomor_surat_invc"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Tanggal Surat *</label></div>
                                    <div class="col-md-9">
                                        <div class="append-icon">
                                            <input name="tanggal_surat" type="text" required
                                                   class="form-control b-datepicker form-white"
                                                   data-date-format="dd-mm-yyyy" id="tgl_surat_invc"
                                                   data-lang="en"
                                                   data-RTL="false"
                                                   placeholder="Tanggal Surat">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Jabatan *</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Jabatan" required
                                                                 name="jabatan" id="jabatan_invc"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nama Pejabat *</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Nama Pejabat" required
                                                                 name="nama_pejabat" id="nama_pjbt_invc"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Invoice *</label></div>
                                    <div class="col-md-9"><input type="file" required
                                                                 name="file_invoice"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-kuitansi" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Upload</strong> Kuitansi</h4>
                    </div>
                    <div class="modal-body" style="margin-bottom:200px">
                        {!! Form::open(array('url'=> '/internal/store-kuitansi', 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-kwitansi')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        <input type="hidden" name="pembayaran_id_kuitansi" id="pembayaran_id_kuitansi" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="cart-summary">
                                    <legend>Bukti Transfer</legend>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Nomor Kwitansi Transfer</label></div>
                                        <div class="col-md-8"><input type="text" placeholder="Nomor Kuitansi Transfer"
                                                                     required id="nmr_kwtns_trf"
                                                                     name="nomor_kuitansi_transfer"
                                                                     class="form-control form-white"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Tanggal Kwitansi Transfer</label></div>
                                        <div class="col-md-8">
                                            <div class="append-icon">
                                                <input name="tanggal_kuitansi_transfer" type="text" required
                                                       class="form-control b-datepicker form-white"
                                                       data-date-format="dd-mm-yyyy"
                                                       data-lang="en" id="tgl_kwtns_trf"
                                                       data-RTL="false"
                                                       placeholder="Tanggal Kuitansi Transfer">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Kwitansi Transfer</label></div>
                                        <div class="col-md-8"><input type="file"
                                                                     name="file_kuitansi_transfer"
                                                                     class="form-control form-white"></div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Bukti Potong PPh Pasal 23</legend>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Nomor Kwitansi PPh 23</label></div>
                                        <div class="col-md-8"><input type="text" placeholder="Nomor Kuitansi PPh 23"
                                                                     required id="nmr_kwtns_pph"
                                                                     name="nomor_kuitansi_pph23"
                                                                     class="form-control form-white"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Tanggal Kwitansi PPh23</label></div>
                                        <div class="col-md-8">
                                            <div class="append-icon">
                                                <input name="tanggal_kuitansi_pph23" type="text" required
                                                       class="form-control b-datepicker form-white"
                                                       data-date-format="dd-mm-yyyy"
                                                       data-lang="en" id="tgl_kwtns_pph"
                                                       data-RTL="false"
                                                       placeholder="Tanggal Kuitansi Transfer">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label>Kwitansi PPh 23</label></div>
                                        <div class="col-md-8"><input type="file"
                                                                     name="file_kuitansi_pph23"
                                                                     class="form-control form-white"></div>
                                    </div>
                                </fieldset>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->
            <script>
                $("input[name=nilai_kontrak]").maskNumber({
                    integer: true
                });
                $("#btn_tambah").on('click', function () {
                    $("#modal-termin").modal("show");

                    //                    var rowCount = $('#table_termin tr').length;
                    //                    $('input[name=termin]').val(rowCount);

                    var nilai_kontrak = $('input[name=nilai_kontrak]').val();
                    $('#md_nilai_kontrak').text('Rp. ' + nilai_kontrak);
                });

                function modal_pembyaran(pembayaran_id) {
                    $("#modal-upload").modal("show");
                    $('#pembayaran_id').val(pembayaran_id);
                }

                function modal_invoice(pembayaran_id, data_invoice = null) {
                    if(data_invoice != null){
                        var data =  JSON.parse(data_invoice);
                        $('#nomor_surat_invc').val(data.nomor_surat);
                        $('#tgl_surat_invc').val(data.tanggal_surat);
                        $('#jabatan_invc').val(data.jabatan);
                        $('#nama_pjbt_invc').val(data.nama_pejabat);
                    }else{
                        $('#form-invoice')[0].reset();
                    }
                    $("#modal-invoice").modal("show");
                    $('#pembayaran_id_invoice').val(pembayaran_id);
                }

                function modal_kuitansi(pembayaran_id, data_kwitansi = null) {
                    if(data_kwitansi != null){
                        var data =  JSON.parse(data_kwitansi);
                        $('#nmr_kwtns_trf').val(data.nomor_kuitansi_transfer);
                        $('#tgl_kwtns_trf').val(data.tanggal_kuitansi_transfer);
                        $('#nmr_kwtns_pph').val(data.nomor_kuitansi_pph23);
                        $('#tgl_kwtns_pph').val(data.tanggal_kuitansi_pph23);
                    }else{
                        $('#form-kwitansi')[0].reset();
                    }
                    $("#modal-kuitansi").modal("show");
                    $('#pembayaran_id_kuitansi').val(pembayaran_id);
                }

                $("input").unbind('keydown');

                function persentase() {
                    var persentase = $('input[name=persentase_termin]').val();
                    var nilai_kontrak = $('input[name=nilai_kontrak]').val();
                    nilai_kontrak = nilai_kontrak.replace(/,/g, '');
                    var nilai_termin = +nilai_kontrak * (+persentase / 100);
                    var sisa_termin = {{$sisa_termin}};
                    var sisa_persen = {{$sisa_persen}};
                    console.log(nilai_kontrak + " " + sisa_termin);
                    if (persentase <= sisa_persen) {
                        $('input[name=nilai_termin]').val(nilai_termin);
                    } else {
                        alert('Nilai Melebihi Sisa Termin!');
                        $('input[name=nilai_termin]').val('');
                        $('input[name=persentase_termin]').val('');
                    }
                }

                $('input[name=tanggal_mulai]').change(function () {
                    compareDate('tanggal_mulai', 'masa_berlaku_kontrak');
                });

                $('input[name=tanggal_selesai]').change(function () {
                    compareDate('tanggal_selesai', 'masa_berlaku_kontrak');
                });

                function compareDate(date_a, date_b) {
                    var start_dt = $('input[name=' + date_a + ']').val().split("-");
                    var new_tgl_mulai = new Date(start_dt[2], start_dt[1] - 1, start_dt[0]);

                    var mb_kontrak = $('input[name=' + date_b + ']').val().split("-");
                    var new_masa_berlaku_kontrak = new Date(mb_kontrak[2], mb_kontrak[1] - 1, mb_kontrak[0]);

                    if (new Date(new_masa_berlaku_kontrak) < new Date(new_tgl_mulai)) {
                        $('input[name=' + date_a + ']').val('');
                    }
                }

                @if($sisa_termin > 0)
                //                     $("#btn_submit").attr('disabled', true);
                @else
                $("#btn_tambah").hide();
                @endif

                $(document).ready(function () {
                    var nilai_kontrak = $('input[name=nilai_kontrak]').val();
                    nilai_kontrak = nilai_kontrak.replace(/,/g, '');
                    var sisa_termin = {{$sisa_termin}};
                    var sisa_persen = {{$sisa_persen}};
                    var total_termin = (+nilai_kontrak - +sisa_termin) / +nilai_kontrak * 100;
                    $('#total_termin').text(total_termin.toFixed(2) + ' %');

                });
            </script>
@endsection
