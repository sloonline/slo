@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Monitoring <strong>IO</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li>WBS/IO</li>
                    <li class="active">Monitoring IO</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-header panel-controls bg-primary">
                            <h3><i class="fa fa-table"></i> List <strong>Data</strong></h3>
                        </div>
                        <div class="panel-content">
                            <div class="btn-group pull-right">
                                <a href="{{ url('internal/monitoring_io/export') }}"
                                   class="btn btn-success btn-square btn-block btn-embossed">
                                    <i class="fa fa-file-excel-o"></i> Export Data</a>
                            </div>
                            <div style="overflow-x: scroll;overflow-y: hidden;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="white-space: nowrap">Nomor IO</th>
                                        <th>Deskripsi</th>
                                        <th style="white-space: nowrap">ID Pelanggan</th>
                                        <th style="white-space: nowrap">Nama Pelanggan</th>
                                        <th style="white-space: nowrap">Jenis Produk</th>
                                        <th style="white-space: nowrap">Nomor Project</th>
                                        <th style="white-space: nowrap">Tahun Project</th>
                                        <th style="white-space: nowrap">Uraian Pekerjaan</th>
                                        <th>Nilai Project</th>
                                        <th style="white-space: nowrap">Jumlah Pembayaran</th>
                                        <th style="white-space: nowrap">Jumlah Belum Dibayar</th>
                                        <th style="white-space: nowrap">Keterangan</th>
                                        <th>Realisasi</th>
                                        <th>Sisa Plafon</th>
                                        <th>Prosentase</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $no = 1;
                                    ?>
                                    @foreach($io as $item)
                                        <?php
                                        $saldo = \App\WbsIo::getColorWarning($item);
                                        $pembayaran = ($item->kontrak == null || $item->kontrak->pembayaran == null) ? 0 : $item->kontrak->pembayaran->sum('nilai');
                                        ?>
                                        <tr {!! (@$item->statusWbsIo->nama_reference == LOCKED) ? 'style="background-color: #FEFFB5;"' : "" !!}>
                                            <td>{{$no}}</td>
                                            <td>{{@$item->nomor}}</td>
                                            <td>{{@$item->deskripsi}}</td>
                                            <td>{{@$item->perusahaan->id_pelanggan}}</td>
                                            <td>{{@$item->perusahaan->nama_perusahaan}}</td>
                                            <td style="white-space: nowrap">{{@$item->jenis_pekerjaan->jenis_pekerjaan}}</td>
                                            <td>{{@$item->nomor_project}}</td>
                                            <td>{{@$item->tahun}}</td>
                                            <td style="white-space: nowrap">{{@$item->uraian_pekerjaan}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format(@$item->nilai, 2, ',', '.')}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format($pembayaran, 2, ',', '.')}}</td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format((@$item->nilai - $pembayaran) , 2, ',', '.')}}</td>
                                            <td></td>
                                            <td style="white-space: nowrap;text-align: right;">
                                                Rp. {{number_format(@$item->realisasi->sum('jumlah') , 2, ',', '.')}}</td>
                                            <td style="white-space: nowrap;text-align: right;color:{{$saldo['color']}};">
                                                <b>Rp. {{number_format($saldo['saldo'], 2, ',', '.')}}</b>
                                            </td>
                                            <td style="white-space: nowrap;text-align: right;">
                                               {{number_format(round(($saldo['saldo'] / @$item->nilai) , 2) , 2, ',', '.')}}</td>
                                            <td>{{@$item->statusWbsIo->nama_reference}}</td>
                                        </tr>
                                        <?php $no++; ?>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

@endsection