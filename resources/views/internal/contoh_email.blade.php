<div class="body">
    <p><strong>Yth. TES,</strong></p>

    <p>Dengan ini kami sampaikan permohonan approval user baru peminta jasa dengan rincian sebagai berikut:</p>

    <div class="centered">
    <table border="0">
        <tr>
            <td>Nama Perusahaan&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>:</td>
            <td>TES</td>
        </tr>
        <tr style="height: 10px;">
            <td style="padding-bottom: 1em;">Status Perusahaan&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="padding-bottom: 1em;">:</td>
            <td style="padding-bottom: 1em;">TES</td>
        </tr>

        <tr>
            <td>Nama User&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>:</td>
            <td>TES</td>
        </tr>
        <tr>
            <td>Username&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>:</td>
            <td>TES</td>
        </tr>
    </table>
    <br>

    <a href="" class="btn">Approve</a>
    </div>

    <br>
    <p>Klik di sini atau klik tombol 'Approve' untuk melakukan approval user baru peminta jasa di atas.<br>
    Demikian pemberitahuan ini kami sampaikan. Atas perhatiannya kami ucapkan terimakasih.</p>

    <p>Salam,<br>
    pusertif.pln.co.id</p><hr>

    <p style="color:#C0C0C0">INSPECTO - Sistem Informasi nya Pusertif<br>
    PT. PLN (Persero) Kantor Pusat</p>
</div>

<style type="text/css">
.body {
    margin: 0;
    padding: 0;
}
.centered {
    padding-left: 2em;
    width: 400px;
}
.btn {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-box-shadow: 0px 1px 3px #666666;
  -moz-box-shadow: 0px 1px 3px #666666;
  box-shadow: 0px 1px 3px #666666;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 150px 10px 150px;
  border: solid #1f628d 2px;
  text-decoration: none;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
</style>