@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            {{--<h2><strong>RAB</strong> Permohonan {{$permohonan->nomor_permohonan}}</h2>--}}
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">RAB</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        {!! Form::open(array('url'=> '/internal/create_rancangan_biaya', 'files'=> true, 'class'=> 'form-horizontal')) !!}
        <input type="hidden" name="order_id" value="{{$order_id}}">
        <input type="hidden" name="rab_id" value="{{($rab == null) ? null : $rab->id}}">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Form Input <strong>RAB</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{-- start form pilih permohonan--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne">
                                            <i class="icon-plus"></i> Pilih Permohonan
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Permohonan</th>
                                                        <th>Tanggal Permohonan</th>
                                                        <th>Jenis Instalasi</th>
                                                        <th>Nama Instalasi</th>
                                                        <th>Pilih</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(is_array($permohonan) || is_object($permohonan))
                                                        <?php $no = 1; ?>
                                                        @foreach($permohonan as $item)
                                                            <?php
                                                            $status = "";
                                                            foreach ($used_perm as $p) {
                                                                if ($p->id == $item->id) {
                                                                    $status = ($p->status == "used") ? "disabled" : "checked";
                                                                    break;
                                                                }
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>{{ $no}}</td>
                                                                <td>{{ $item->nomor_permohonan}}</td>
                                                                <td>{{ date('d M Y',strtotime($item->tanggal_permohonan)) }}</td>
                                                                <td>{{ ($item->instalasi_permohonan != null) ? $item->instalasi_permohonan->instalasi->jenis_instalasi->jenis_instalasi : $item->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}</td>
                                                                <td>{{ ($item->instalasi_permohonan != null) ? $item->instalasi_permohonan->instalasi->nama_instalasi : "Distribusi"}}</td>
                                                                <td>
                                                                    <input type="checkbox" name="pilih_permohonan[]"
                                                                           {{($status != "") ? "checked='checked'" : ""}} {{($status == "disabled") ? "disabled='disabled'" : ""}}
                                                                           class="form-control permohonan"
                                                                           value="{{$item->id}}">
                                                                    {{--<a class="btn btn-sm btn-warning btn-square btn-embossed"><i--}}
                                                                    {{--class="fa fa-plus"></i> Pilih</a>--}}
                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end form pilih permohonan--}}
                        {{-- start rancangan biaya--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo">
                                            <i class="icon-plus"></i> Rancangan Biaya
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="nav-tabs2" id="tabs">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#remunerasi" data-toggle="tab"><i
                                                                        class="icon-home"></i>
                                                                Remunerasi
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#biayalangsung" data-toggle="tab"><i
                                                                        class="icon-user"></i>
                                                                Biaya Langsung
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#biayajasainspeksi" data-toggle="tab"><i
                                                                        class="icon-user"></i>
                                                                Biaya Jasa Inspeksi
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#biayamaterialpendukung" data-toggle="tab"><i
                                                                        class="icon-user"></i>
                                                                Biaya Material Pendukung
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content bg-white">
                                                        <div class="tab-pane active" id="remunerasi">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th rowspan="3" colspan="2" class="text-left">UNSUR
                                                                        BIAYA
                                                                    </th>
                                                                    <th rowspan="3" class="text-center"
                                                                        style="width:300px">TARIF
                                                                    </th>
                                                                    <th colspan="5" class="text-center">HARI ORANG
                                                                        (MD)
                                                                    </th>
                                                                    <th rowspan="3" class="text-center"
                                                                        style="width:300px">JUMLAH BIAYA
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th colspan="2" class="text-center">KANTOR</th>
                                                                    <th colspan="2" class="text-center">LAPANGAN</th>
                                                                    <th rowspan="2" class="text-center"
                                                                        style="width:50px">JUMLAH
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center" style="width:75px">HARI</th>
                                                                    <th class="text-center" style="width:75px">ORANG
                                                                    </th>
                                                                    <th class="text-center" style="width:75px">HARI</th>
                                                                    <th class="text-center" style="width:75px">ORANG
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                $no = 1;
                                                                ?>
                                                                @foreach($unsur_biaya_remunerasi as $item)
                                                                    <?php
                                                                    $remunerasi = null;
                                                                    foreach ($detail_rab as $row) {
                                                                        if ($row->id_unsur_biaya == $item->id) {
                                                                            $remunerasi = $row;
                                                                            break;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <input name="rab_detail_remunerasi_id[]"
                                                                           type="hidden"
                                                                           value="{{($remunerasi != null) ? $remunerasi->id : null}}"/>
                                                                    <input name="remunerasi_biaya_id[]" type="hidden"
                                                                           class="form-control form-white"
                                                                           value="{{$item->id}}"/>
                                                                    <tr class="item-row">
                                                                        <th>{{$item->kode}}</th>
                                                                        <th>{{$item->unsur}}</th>
                                                                        <td class="text-center"><input
                                                                                    name="tarif_remunerasi[]"
                                                                                    type="number"
                                                                                    min="0" id="tarif_remunerasi{{$no}}"
                                                                                    class="form-control form-white"
                                                                                    value="{{$item->tarif}}" readonly
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    name="kantor_hari[]" type="number"
                                                                                    min="0"
                                                                                    value="{{($remunerasi != null) ? $remunerasi->kantor_hari : ''}}"
                                                                                    id="kantor_hari{{$no}}"
                                                                                    onchange="remunerasi({{$no}})"
                                                                                    class="form-control form-white"
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    name="kantor_orang[]" type="number"
                                                                                    min="0"
                                                                                    value="{{($remunerasi != null) ? $remunerasi->kantor_orang : ''}}"
                                                                                    id="kantor_orang{{$no}}"
                                                                                    onchange="remunerasi({{$no}})"
                                                                                    class="kantor_orang{{$no}} form-control form-white"
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input name="lap_hari[]"
                                                                                                       type="number"
                                                                                                       min="0"
                                                                                                       id="lap_hari{{$no}}"
                                                                                                       value="{{($remunerasi != null) ? $remunerasi->lapangan_hari : ''}}"
                                                                                                       onchange="remunerasi({{$no}})"
                                                                                                       class="lap_hari{{$no}} form-control form-white"
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    name="lap_orang[]" type="number"
                                                                                    min="0"
                                                                                    id="lap_orang{{$no}}"
                                                                                    value="{{($remunerasi != null) ? $remunerasi->lapangan_orang : ''}}"
                                                                                    onchange="remunerasi({{$no}})"
                                                                                    class="lap_orang{{$no}} form-control form-white"
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    name="jumlah_remunerasi[]"
                                                                                    id="jumlah_remunerasi{{$no}}"
                                                                                    type="number"
                                                                                    min="0"
                                                                                    value="{{($remunerasi != null) ? $remunerasi->jumlah : ''}}"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            />
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    name="jumlah_biaya_remunerasi[]"
                                                                                    id="jumlah_biaya_remunerasi{{$no}}"
                                                                                    type="number" min="0"
                                                                                    value="{{($remunerasi != null) ? $remunerasi->jumlah_biaya : ''}}"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            />
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $no++;
                                                                    ?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="biayalangsung">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th rowspan="2" colspan="2" class="text-left">NO
                                                                    </th>
                                                                    <th rowspan="2" class="text-left">UNSUR BIAYA</th>
                                                                    <th rowspan="2" class="text-center"
                                                                        style="width:300px">TARIF
                                                                    </th>
                                                                    <th colspan="2" class="text-center">HARI ORANG
                                                                        (MD)
                                                                    </th>
                                                                    <th rowspan="2" class="text-center"
                                                                        style="width:300px">JUMLAH
                                                                    </th>
                                                                    <th rowspan="2" class="text-center"
                                                                        style="width:300px">JUMLAH BIAYA
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center" style="width:75px">HARI</th>
                                                                    <th class="text-center" style="width:75px">ORANG
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <th colspan="8">Uang Harian (SPPD)</th>
                                                                </tr>
                                                                <?php
                                                                $no = 1;
                                                                ?>
                                                                @foreach($unsur_biaya_langsung_sppd as $item)
                                                                    <?php
                                                                    $sppd = null;
                                                                    foreach ($detail_rab as $row) {
                                                                        if ($row->id_unsur_biaya == $item->id) {
                                                                            $sppd = $row;
                                                                            break;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <input name="rab_detail_sppd_id[]" type="hidden"
                                                                           value="{{($sppd != null) ? $sppd->id : null}}"/>
                                                                    <input name="sppd_biaya_id[]" type="hidden"
                                                                           class="form-control form-white"
                                                                           value="{{$item->id}}"/>
                                                                    <tr class="item-row">
                                                                        <th></th>
                                                                        <th>{{$item->kode}}</th>
                                                                        <th>{{$item->unsur}}</th>
                                                                        <td class="text-center"><input
                                                                                    id="tarif_sppd{{$no}}"
                                                                                    onchange="biaya_langsung_sppd({{$no}})"
                                                                                    name="tarif_sppd[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                                    value="{{$item->tarif}}"
                                                                                    readonly>
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="hari_sppd{{$no}}"
                                                                                    value="{{($sppd != null) ? $sppd->lapangan_hari : ''}}"
                                                                                    onchange="biaya_langsung_sppd({{$no}})"
                                                                                    name="hari_sppd[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="orang_sppd{{$no}}"
                                                                                    value="{{($sppd != null) ? $sppd->lapangan_orang : ''}}"
                                                                                    onchange="biaya_langsung_sppd({{$no}})"
                                                                                    name="orang_sppd[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_sppd{{$no}}"
                                                                                    value="{{($sppd != null) ? $sppd->jumlah : ''}}"
                                                                                    onchange="biaya_langsung_sppd({{$no}})"
                                                                                    name="jumlah_sppd[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_biaya_sppd{{$no}}"
                                                                                    value="{{($sppd != null) ? $sppd->jumlah_biaya : ''}}"
                                                                                    name="jumlah_biaya_sppd[]"
                                                                                    type="number" min="0"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            >
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $no++;
                                                                    ?>
                                                                @endforeach


                                                                <tr>
                                                                    <td>2</td>
                                                                    <th colspan="8">Biaya Harian (Akomodasi)</th>
                                                                </tr>
                                                                <?php
                                                                $no = 1;
                                                                ?>
                                                                @foreach($unsur_biaya_langsung_ako as $item)
                                                                    <?php
                                                                    $ako = null;
                                                                    foreach ($detail_rab as $row) {
                                                                        if ($row->id_unsur_biaya == $item->id) {
                                                                            $ako = $row;
                                                                            break;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <input name="rab_detail_ako_id[]" type="hidden"
                                                                           value="{{($ako != null) ? $ako->id : null}}"/>
                                                                    <input name="ako_biaya_id[]" type="hidden"
                                                                           class="form-control form-white"
                                                                           value="{{$item->id}}"/>
                                                                    <tr class="item-row">
                                                                        <th></th>
                                                                        <th>{{$item->kode}}</th>
                                                                        <th>{{$item->unsur}}</th>
                                                                        <td class="text-center"><input
                                                                                    id="tarif_ako{{$no}}"
                                                                                    onchange="biaya_langsung_ako({{$no}})"
                                                                                    name="tarif_ako[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                                    value="{{$item->tarif}}"
                                                                                    readonly>
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="hari_ako{{$no}}"
                                                                                    value="{{($ako != null) ? $ako->lapangan_hari : ''}}"
                                                                                    onchange="biaya_langsung_ako({{$no}})"
                                                                                    name="hari_ako[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="orang_ako{{$no}}"
                                                                                    value="{{($ako != null) ? $ako->lapangan_orang : ''}}"
                                                                                    onchange="biaya_langsung_ako({{$no}})"
                                                                                    name="orang_ako[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_ako{{$no}}"
                                                                                    value="{{($ako != null) ? $ako->jumlah : ''}}"
                                                                                    onchange="biaya_langsung_ako({{$no}})"
                                                                                    name="jumlah_ako[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_biaya_ako{{$no}}"
                                                                                    value="{{($ako != null) ? $ako->jumlah_biaya : ''}}"
                                                                                    name="jumlah_biaya_ako[]"
                                                                                    type="number" min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $no++;
                                                                    ?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="biayajasainspeksi">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th rowspan="3" class="text-left">NO</th>
                                                                    <th rowspan="3" class="text-left">UNSUR BIAYA</th>
                                                                    <th colspan="2" class="text-center">HARI ORANG
                                                                        (MD)
                                                                    </th>
                                                                    <th rowspan="3" class="text-center"
                                                                        style="width:75px">JUMLAH
                                                                    </th>
                                                                    <th rowspan="3" class="text-center"
                                                                        style="width:300px">JUMLAH BIAYA
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th colspan="2" class="text-center">LAPANGAN</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center" style="width:75px">HARI</th>
                                                                    <th class="text-center" style="width:75px">ORANG
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                $no = 1;
                                                                ?>
                                                                @foreach($unsur_biaya_inspeksi as $item)
                                                                    <?php
                                                                    $inspeksi = null;
                                                                    foreach ($detail_rab as $row) {
                                                                        if ($row->id_unsur_biaya == $item->id) {
                                                                            $inspeksi = $row;
                                                                            break;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <input name="rab_detail_inspeksi_id[]" type="hidden"
                                                                           value="{{($inspeksi != null) ? $inspeksi->id : null}}"/>
                                                                    <input name="inspeksi_biaya_id[]" type="hidden"
                                                                           class="form-control form-white"
                                                                           value="{{$item->id}}"/>
                                                                    <tr class="item-row">
                                                                        <td>{{$no}}</td>
                                                                        <th>{{$item->unsur}}</th>
                                                                        <td class="text-center"><input
                                                                                    id="hari_inspeksi{{$no}}"
                                                                                    value="{{($inspeksi != null) ? $inspeksi->lapangan_hari : ''}}"
                                                                                    onchange="biaya_inspeksi({{$no}})"
                                                                                    name="hari_inspeksi[]" type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="orang_inspeksi{{$no}}"
                                                                                    value="{{($inspeksi != null) ? $inspeksi->lapangan_orang : ''}}"
                                                                                    onchange="biaya_inspeksi({{$no}})"
                                                                                    name="orang_inspeksi[]"
                                                                                    type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_inspeksi{{$no}}"
                                                                                    value="{{($inspeksi != null) ? $inspeksi->jumlah : ''}}"
                                                                                    onchange="biaya_inspeksi({{$no}})"
                                                                                    name="jumlah_inspeksi[]"
                                                                                    type="number"
                                                                                    min="0"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_biaya_inspeksi{{$no}}"
                                                                                    value="{{($inspeksi != null) ? $inspeksi->jumlah_biaya : ''}}"
                                                                                    name="jumlah_biaya_inspeksi[]"
                                                                                    type="number" min="0"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            >
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $no++;
                                                                    ?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="tab-pane" id="biayamaterialpendukung">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-left">NO</th>
                                                                    <th class="text-left">UNSUR BIAYA</th>
                                                                    <th class="text-center" style="width:75px">QTY</th>
                                                                    <th class="text-center" style="width:300px">JUMLAH
                                                                    </th>
                                                                    <th class="text-center" style="width:300px">JUMLAH
                                                                        BIAYA
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                $no = 1;
                                                                ?>
                                                                @foreach($unsur_biaya_pendukung as $item)
                                                                    <?php
                                                                    $pendukung = null;
                                                                    foreach ($detail_rab as $row) {
                                                                        if ($row->id_unsur_biaya == $item->id) {
                                                                            $pendukung = $row;
                                                                            break;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <input name="rab_detail_pendukung_id[]"
                                                                           type="hidden"
                                                                           value="{{($pendukung != null) ? $pendukung->id : null}}"/>
                                                                    <input name="pendukung_biaya_id[]" type="hidden"
                                                                           class="form-control form-white"
                                                                           value="{{$item->id}}"/>
                                                                    <tr class="item-row">
                                                                        <td>{{$no}}</td>
                                                                        <th>{{$item->unsur}}</th>
                                                                        <td class="text-center"><input
                                                                                    id="qty_pendukung{{$no}}"
                                                                                    onchange="biaya_pendukung({{$no}})"
                                                                                    name="qty_pendukung[]" type="number"
                                                                                    min="0"
                                                                                    value="{{($pendukung != null) ? $pendukung->lapangan_orang : ''}}"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_pendukung{{$no}}"
                                                                                    onchange="biaya_pendukung({{$no}})"
                                                                                    name="jumlah_pendukung[]"
                                                                                    type="number"
                                                                                    min="0"
                                                                                    value="{{($pendukung != null) ? $pendukung->jumlah : ''}}"
                                                                                    class="form-control form-white"
                                                                            >
                                                                        </td>
                                                                        <td class="text-center"><input
                                                                                    id="jumlah_biaya_pendukung{{$no}}"
                                                                                    name="jumlah_biaya_pendukung[]"
                                                                                    type="number" min="0"
                                                                                    value="{{($pendukung != null) ? $pendukung->jumlah_biaya : ''}}"
                                                                                    class="form-control bg-aero"
                                                                                    readonly
                                                                            >
                                                                        </td>

                                                                    <?php
                                                                    $no++;
                                                                    ?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end rancangan biaya--}}
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Jumlah Biaya I+II+III+IV</th>
                                    <th><input name="total_biaya" id="total_biaya" class="form-control form-white"
                                               type="number" readonly min="0"
                                               value="{{($rab != null) ? $rab->jumlah_biaya : ''}}"
                                               style="width:300px"></th>
                                </tr>
                                <tr>
                                    <th>PPN 10%</th>
                                    <th><input name="total_ppn" id="total_ppn" class="form-control form-white"
                                               type="number" readonly min="0"
                                               value="{{($rab != null) ? $rab->ppn : ''}}"
                                               style="width:300px"></th>
                                </tr>
                                <tr>
                                    <th>TOTAL BIAYA JASA SETELAH PPN</th>
                                    <th><input name="total_bayar" id="total_bayar" class="form-control input-lg bg-aero"
                                               type="number" readonly min="0"
                                               value="{{($rab != null) ? $rab->total_biaya : ''}}"
                                               style="width:300px"></th>
                                </tr>
                                </thead>
                            </table>
                            <div class="pull-right">
                                <a href="{{url('internal/detail_order/'.$order_id)}}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali <i
                                            class="icon-ban"></i></a>
                                <button type="submit" onclick="return validatePermohonan();"
                                        class="btn btn-primary btn-square btn-embossed">Simpan <i
                                            class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @endsection

        @section('page_script')
            <script>
                //                total_biaya()
                //guys, these methods used to calculate field value when datas changed
                function remunerasi(id) {
                    var kantor_hari = $('#kantor_hari' + id).val();
                    var kantor_orang = $('#kantor_orang' + id).val();
                    var lap_hari = $('#lap_hari' + id).val();
                    var lap_orang = $('#lap_orang' + id).val();
                    var jumlah = $('#jumlah_remunerasi' + id).val((kantor_hari * kantor_orang) + +(lap_hari * lap_orang));
                    $('#jumlah_biaya_remunerasi' + id).val(jumlah.val() * $('#tarif_remunerasi' + id).val());

                    total_biaya();
                }

                function biaya_langsung_sppd(id) {
                    var hari = $('#hari_sppd' + id).val();
                    var orang = $('#orang_sppd' + id).val();
                    var jumlah = $('#jumlah_sppd' + id).val(hari * orang);
                    $('#jumlah_biaya_sppd' + id).val(jumlah.val() * $('#tarif_sppd' + id).val());

                    total_biaya();
                }

                function biaya_langsung_ako(id) {
                    var hari = $('#hari_ako' + id).val();
                    var orang = $('#orang_ako' + id).val();
                    var jumlah = $('#jumlah_ako' + id).val(hari * orang);
                    $('#jumlah_biaya_ako' + id).val(jumlah.val() * $('#tarif_ako' + id).val());

                    total_biaya();
                }

                function biaya_inspeksi(id) {
                    var hari = $('#hari_inspeksi' + id).val();
                    var orang = $('#orang_inspeksi' + id).val();
                    var jumlah = $('#jumlah_inspeksi' + id).val();
                    $('#jumlah_biaya_inspeksi' + id).val(hari * orang * jumlah);

                    total_biaya();
                }

                function biaya_pendukung(id) {
                    var qty = $('#qty_pendukung' + id).val();
                    var jumlah = $('#jumlah_pendukung' + id).val();
                    $('#jumlah_biaya_pendukung' + id).val(qty * jumlah);

                    total_biaya();
                }

                function hitung_total(elem_id) {
                    var id = 1;
                    var total = 0;
                    while ($('#' + elem_id + id).val() != null) {
                        var val = $('#' + elem_id + id).val();
                        total = total + +val;
                        id++;
                    }
                    console.log(total);
                    return total;
                }

                function total_biaya() {
                    var total_remunerasi = hitung_total('jumlah_biaya_remunerasi');
                    var total_biaya_sppd = hitung_total('jumlah_biaya_sppd');
                    var total_biaya_ako = hitung_total('jumlah_biaya_ako');
                    var total_biaya_inspeksi = hitung_total('jumlah_biaya_inspeksi');
                    var total_biaya_pendukung = hitung_total('jumlah_biaya_pendukung');
                    var hasil_jumlah = +total_remunerasi + +total_biaya_sppd + +total_biaya_ako + +total_biaya_inspeksi + +total_biaya_pendukung;

                    $('#total_biaya').val(hasil_jumlah); //TOTAL BIAYA I+II+III+IV
                    var ppn = $('#total_ppn').val(0.1 * hasil_jumlah); //JUMLAH PPN 10%
                    $('#total_bayar').val(hasil_jumlah + +ppn.val()); //TOTAL BAYAR

                }

                function validatePermohonan() {
                    var checked = $('.permohonan:checked');
                    var count = 0;
                    for (var i = 0; i < checked.length; i++) {
                        if (checked[i].getAttribute('disabled') == null) {
                            count++;
                        }
                    }
                    if ($("#total_bayar").val() == "") {
                        alert('Anda belum mengisi data RAB');
                        return false;
                    }
                    if (count == 0) {
                        alert('Anda belum memilih permohonan');
                        return false;
                    }
                }
            </script>
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection