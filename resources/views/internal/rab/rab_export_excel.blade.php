<div class="panel-body">
<table>
    <thead>
    <tr>
        <td rowspan="2" style="text-align:center">NO</td>
        <td rowspan="2" colspan="2" style="text-align:center">UNSUR</td>
        <td rowspan="2" style="text-align:center">BIAYA SATUAN (Rp)</td>
        <td colspan="2" style="text-align:center">JUMLAH</td>
        <td rowspan="2" style="text-align:center">SATUAN</td>
        <td rowspan="2" style="text-align:center">VOLUME</td>
        <td rowspan="2" style="text-align:center">TOTAL</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    </thead>
    <tbody>
    <?php $no = 1 ?>
    @foreach($jenis as $i => $item)
        <tr>
            <td>{{$no}}</td>
            <td colspan="8">{{$item->jenis}}</td>
        </tr>
        <?php $no++ ?>
        @foreach($unsur_arr[$item->jenis] as $j => $unsur_item)
            <?php
            $unsur = null;
            foreach ($detail_rab as $row) {
                if ($row->id_unsur_biaya == $unsur_item->id) {
                    $unsur = $row;
                    break;
                }
            }
            ?>
        <tr>
            <td></td>
            <td>{{$unsur_item->kode}}</td>
            <td>{{$unsur_item->unsur}}</td>
            <td>{{$unsur_item->tarif}}</td>
            <td>{{$unsur->kantor_hari}}</td>
            <td>{{$unsur->kantor_orang}}</td>
            <td>{{($unsur_item->satuan==0)?'HO':'BO'}}</td>
            <td>{{$unsur->jumlah}}</td>
            <td>{{$unsur->jumlah_biaya}}</td>
        </tr>
        @endforeach
    @endforeach
    <tr>
        <td colspan="8">JUMLAH</td>
        <td>{{$rab->jumlah_biaya}}</td>
    </tr>
    <tr>
        <td colspan="8">PPN 10%</td>
        <td>{{$rab->ppn}}</td>
    </tr>
    <tr>
        <td colspan="8">JUMLAH BIAYA SETELAH PPN</td>
        <td>{{$rab->total_biaya}}</td>
    </tr>
    </tbody>
</table>
</div>