@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <?php
    $allowEdit = \App\Order::isAllowEditRab($order);
    $allowPenawaran = \App\Order::isAllowEditPenawaranRab($order);
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Rancangan <strong>Biaya</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">Rancangan Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2">
                        <div class="m-b-20">
                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_RAB))
                                <div class="btn-group">
                                    <a href="{{url(($order != null) ? '/internal/create_rancangan_biaya/'.$order->id:'')}}"
                                    class="btn btn-success btn-square btn-block btn-embossed" {{($is_complete == 0) ? 'disabled' : ''}}><i
                                                class="fa fa-plus"></i> Tambah
                                        RAB</a>
                                </div>
                            @endif
                            @if($jumlah > 0)
                                <button type="button" class="btn btn-danger btn-transparent btn-square"><i
                                            class="glyphicon glyphicon-exclamation-sign"></i>{{$jumlah}} Permohonan
                                    Belum Memiliki RAB
                                </button>
                            @endif
                            {{--jika ada RAB yang belum dibikin kontrak, bikin shortcut button untuk create kontrak--}}
                            @if($flow_completed != false && sizeof($rab_kontrak_unused) > 0)
                                <div class="btn-group pull-right">
                                    <a target="_blank" href="{{url('/internal/create_kontrak_order/'.$order->id)}}"
                                       class="btn btn-success btn-square btn-block btn-embossed"><i
                                                class="fa fa-plus"></i>Create Kontrak</a>
                                </div>
                            @endif
                            {{--<div class="btn-group">--}}
                            {{--<a href="{{url('/').'/internal/rancangan_biaya_order/submit/'.$order->id}}"--}}
                            {{--class="btn btn-success btn-square btn-block btn-embossed" {{($is_complete != 0 || $order->flow_status->tipe_status == SUBMITTED) ? 'disabled' : ''}}><i--}}
                            {{--class="fa fa-money"></i>Submit RAB</a>--}}
                            {{--</div>--}}
                        </div>
                        <?php
                        $isPLN = (\App\User::isUnitPLN($order->user));
                        ?>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Dokumen</th>
                                <th>Tanggal RAB</th>
                                <th>Jumlah Biaya</th>
                                <th>PPN 10%</th>
                                <th>Total Bayar</th>
                                @if($isPLN)
                                    <th>Nomor SKKI</th>
                                    <th>Nomor SKKO</th>
                                    <th>Nomor PRK</th>
                                @endif
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(sizeof($rab)>0)
                                <?php $no = 1;$total = 0;?>
                                @foreach($rab as $item)
                                    <?php $total += $item->total_biaya;?>
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ $item->no_dokumen }}</td>
                                        <td>{{ date('d M Y',strtotime($item->created_at)) }}</td>
                                        <td style="text-align: right;">Rp. {{number_format(@$item->jumlah_biaya, 2, ',', '.')}}</td>
                                        <td style="text-align: right;">Rp. {{number_format(@$item->ppn, 2, ',', '.')}}</td>
                                        <td style="text-align: right;">
                                            Rp. {{number_format(@$item->total_biaya, 2, ',', '.')}}</td>
                                        @if($isPLN)
                                            <td>{{@$item->no_skki}}</td>
                                            <td>{{@$item->no_skko}}</td>
                                            <td>{{@$item->no_prk}}</td>
                                        @endif
                                        <td>
                                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_REVISI_RAB))
                                                @if($allowEdit)
                                                    <a href="{{url('/internal/create_rancangan_biaya/'.$item->order_id.'/'.$item->id)}}"
                                                    class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                @endif
                                            @endif
                                            
                                            <a href="{{url('/internal/detail_rancangan_biaya/'.$item->order_id.'/'.$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"></i></a>
                                            <a href="{{url('/internal/amandemen_rancangan_biaya/'.$item->order_id.'/'.$item->id)}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                        class="fa fa-pencil"></i></a>
                                            @if($allowEdit)
                                                <a href="{{url('/internal/delete_rancangan_biaya/'.$item->order_id.'/'.$item->id)}}"
                                                   class="btn btn-sm btn-danger btn-square btn-embossed"
                                                   onclick="return confirm('Apakah anda yakin menghapus RAB?')"><i
                                                            class="fa fa-trash"></i></a>
                                            @endif
                                            <a class="btn btn-success btn-sm" target="_blank"
                                               href="{{url('upload/'.@$item->file_rab)}}"><i
                                                        class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++?>
                                @endforeach
                                <tr>
                                    <td colspan="5" style="text-align: center;background-color: lightgrey"><b>Total
                                            Biaya</b></td>
                                    <td style="text-align: right;"><b>Rp. {{number_format($total, 2, ',', '.')}}</b>
                                    </td>
                                    <td @if($isPLN) colspan="4" @endif
                                    style="background-color: lightgrey"></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    @if($allowPenawaran || $order->file_spnw != null)
                        <div class="col-md-12" id="form-penawaran">
                            <div class="panel border">
                                <div class="panel-header bg-primary">
                                    <h3>SURAT PENAWARAN</h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-content ">
                                        {!! Form::open(array('url'=> url('internal/save_surat_penawaran'), 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                        {!! Form::hidden('order_id', $order_id) !!}
                                        <div class="form-group">
                                            <p>
                                                <label class="col-sm-3 control-label">Nomor Surat</label>
                                            </p>
                                            <div class="col-sm-6 prepend-icon">
                                                <input type="text" class="form-control form-white" name="no_surat"
                                                       placeholder="Nomor"
                                                       value="{{@$order->nomor_spnw}}" {{(!$allowPenawaran)?'disabled':'required'}}>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p>
                                                <label class="col-sm-3 control-label">Tanggal Surat</label>
                                            </p>
                                            <div class="col-sm-6 prepend-icon">
                                                <input type="text" data-date-format="dd-mm-yyyy"
                                                       class="b-datepicker form-control form-white" name="tgl_surat"
                                                       placeholder="Tanggal"
                                                       value="{{@$order->tanggal_spnw}}" {{(!$allowPenawaran)?'disabled':'required'}}>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p>
                                                <label class="col-sm-3 control-label">Surat
                                                    Penawaran</label>
                                            </p>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    @if($order->file_spnw != null)
                                                        <div class="col-md-3">
                                                            <a href="{{url('upload/'.$order->file_spnw)}}"
                                                               target="_blank"
                                                               class="btn btn-primary"> <i class="fa fa-download"></i>
                                                                View
                                                            </a>
                                                        </div>
                                                    @endif
                                                    @if($allowPenawaran)
                                                        <div class="col-md-{{($order->file_spnw != null)? "9" : "12"}}">
                                                            <input type="file" class="form-control"
                                                                   {{($order->file_spnw == null)?'required' : ''}}
                                                                   name="surat_penawaran">
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @if($allowPenawaran)
                                            <div class="form-group">
                                                <div class="col-sm-4 pull-right">
                                                    <button type="submit"
                                                            class="btn btn-success btn-square btn-embossed">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($order->file_penugasan != null)
                        <div class="col-md-12" id="form-penugasan">
                            <div class="panel border">
                                <div class="panel-header bg-primary">
                                    <h3>SURAT PENUGASAN</h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-content ">
                                            <div class="col-md-8">
                                                <a href="{{url('upload/'.$order->file_penugasan)}}"
                                                   target="_blank"
                                                   class="btn btn-primary"> <i class="fa fa-download"></i>
                                                    Preview file surat penugasan
                                                </a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    {{--                    @if(($order->nomor_spnw != null))--}}
                    @if($jumlah > 0)
                        {{--jika belum semua permohonan dibuat RAB maka belum b--}}
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right p-b-10">
                                <a onclick="{{url('internal/rab')}}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali <i
                                            class="icon-ban"></i></a>
                            </div>
                        </div>
                    @else
                        @include('workflow_view')
                    @endif
                    {{--@endif--}}
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <!-- Select Inputs -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- >Bootstrap Date Picker -->
            <script>
                @if($allowPenawaran && $order->file_spnw == null)
                    $("#btn_submit").attr('disabled', true);
                hideApproval();
                @endif
            </script>
@endsection