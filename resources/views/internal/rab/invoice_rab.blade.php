@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Invoice <strong>RAB</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">RAB</a></li>
					<li class="active">Invoice</li>
				</ol>
			</div>
		</div>
	    <div class="row">
			<div class="col-lg-12 portlets">
				<div class="panel">
					<div class="panel-header panel-controls bg-primary">
					  <h3><i class="fa fa-table"></i> Form Input <strong>RAB</strong></h3>
					</div>
					<div class="panel-content">
					    <div class="row">
                            <div class="col-md-12 t-center">
                                <h2>RENCANA ANGGARAN BIAYA PENAWARAN - FORMULIR 1 - A</h2>
                                <h2 class="m-t-0">BPU - PUSERTIF</h2>
                            </div>
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <table>
                                        <tr>
                                            <td><h3><strong>Peminta Jasa &nbsp;</strong></h3></td>
                                            <td><h3><strong>: &nbsp;</strong></h3></td>
                                            <td><h3><strong>Value Om</strong></h3></td>
                                        </tr>
                                        <tr>
                                            <td><h3 class="m-t-0"><strong>Lingkup / Uraian Pekerjaan &nbsp;</strong></h3></td>
                                            <td><h3 class="m-t-0"><strong>: &nbsp;</strong></h3></td>
                                            <td><h3 class="m-t-0"><strong>Value Om</strong></h3></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="pull-right">
                                    <table>
                                        <tr>
                                            <td><h3><strong>No./Tgl. Surat Peminta Jasa &nbsp;</strong></h3></td>
                                            <td><h3><strong>: &nbsp;</strong></h3></td>
                                            <td><h3><strong>Value Om</strong></h3></td>
                                        </tr>
                                        <tr>
                                            <td><h3 class="m-t-0"><strong>Lokasi &nbsp;</strong></h3></td>
                                            <td><h3 class="m-t-0"><strong>: &nbsp;</strong></h3></td>
                                            <td><h3 class="m-t-0"><strong>Value Om</strong></h3></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
					    </div>
					    <hr class="m-t-0 p-b-20">
					    <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                          <th class="text-left">NO</th>
                                          <th colspan="2" class="text-left">UNSUR BIAYA</th>
                                          <th class="text-center" style="width:250px">TARIF</th>
                                          <th colspan="5" class="text-center">HARI ORANG (MD)</th>
                                          <th class="text-center" style="width:250px">JUMLAH BIAYA</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                          <!-- START HEADER UTAMA -->
                                          <tr>
                                              <th rowspan="2">I</th>
                                              <th rowspan="2" colspan="2">Remunerasi</th>
                                              <th rowspan="2"></th>
                                              <th colspan="2" class="text-center">Kantor</th>
                                              <th colspan="2" class="text-center">Lapangan</th>
                                              <th rowspan="2" class="text-center" style="width:200px">Jumlah</th>
                                              <th rowspan="2"></th>
                                          </tr>
                                          <!-- END HEADER UTAMA -->
                                          <!-- START HEADER BIAYA I -->
                                          <tr>
                                              <th class="text-center" style="width:70px">Hari</th>
                                              <th class="text-center" style="width:70px">Orang</th>
                                              <th class="text-center" style="width:70px">Hari</th>
                                              <th class="text-center" style="width:70px">Orang</th>
                                          </tr>
                                          <!-- END HEADER BIAYA I -->
                                          <!-- START DATA BIAYA I -->
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F1</td>
                                              <td>Integration 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F2</td>
                                              <td>Advance 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F3</td>
                                              <td>Optimization 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F4</td>
                                              <td>System 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F5</td>
                                              <td>Spesific 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F6</td>
                                              <td>Basic 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr class="success">
                                              <th></th>
                                              <th colspan="8">Jumlah Biaya I</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END DATA BIAYA I -->
                                          <!-- START HEADER BIAYA II -->
                                          <tr>
                                              <th>II</th>
                                              <th colspan="2">Biaya Langsung</th>
                                              <th></th>
                                              <th colspan="2" class="text-center">Hari</th>
                                              <th colspan="2" class="text-center">Orang</th>
                                              <th></th>
                                              <th></th>
                                          </tr>
                                          <!-- START HEADER BIAYA II -->
                                          <!-- START DATA BIAYA II -->
                                          <tr>
                                              <td>1</td>
                                              <td colspan="8">Uang Harian (SPPD)</td>
                                              <td></td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F1</td>
                                              <td>Integration 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F2</td>
                                              <td>Advance 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F3</td>
                                              <td>Optimization 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F4</td>
                                              <td>System 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F5</td>
                                              <td>Spesific 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F6</td>
                                              <td>Basic 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td colspan="2">STH Spesific 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>2</td>
                                              <td colspan="8">Biaya Harian (Akomodasi)</td>
                                              <td></td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F1</td>
                                              <td>Integration 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F2</td>
                                              <td>Advance 1 - 3</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F3</td>
                                              <td>Optimization 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F4</td>
                                              <td>System 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F5</td>
                                              <td>Spesific 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td class="text-left" style="width:20px">F6</td>
                                              <td>Basic 1 - 4</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td></td>
                                              <td colspan="2">Inspektor / Senior Inspektor</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr class="success">
                                              <th></th>
                                              <th colspan="8">Jumlah Biaya II</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END DATA BIAYA II -->
                                          <!-- START HEADER BIAYA III -->
                                          <tr>
                                              <th>III</th>
                                              <th colspan="8">Biaya Jasa Inspeksi</th>
                                              <th></th>
                                          </tr>
                                          <!-- END HEADER BIAYA III -->
                                          <!-- START DATA BIAYA III -->
                                          <tr>
                                              <td>1</td>
                                              <td colspan="5">Biaya Administrasi Teknik</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>2</td>
                                              <td colspan="5">Biaya Outsourcing Inspektor</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>3</td>
                                              <td colspan="5">Biaya Outsourcing Inspektor</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr class="success">
                                              <th></th>
                                              <th colspan="8">Jumlah Biaya III</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END DATA BIAYA III -->
                                          <!-- START HEADER BIAYA IV -->
                                          <tr>
                                              <th>IV</th>
                                              <th colspan="8">Biaya Material Pendukung</th>
                                              <th></th>
                                          </tr>
                                          <!-- END HEADER BIAYA IV -->
                                          <!-- START DATA BIAYA IV -->
                                          <tr>
                                              <td>1</td>
                                              <td colspan="5">Transportasi petugas ke tempat tujuan</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>2</td>
                                              <td colspan="5">Biaya Sewa Kendaraan </td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>3</td>
                                              <td colspan="5">Biaya Laptek</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>4</td>
                                              <td colspan="5">Biaya Sewa Laptop</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>5</td>
                                              <td colspan="5">Biaya alat Pelindung Diri (APD)</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr>
                                              <td>6</td>
                                              <td colspan="5">Biaya lain yang relevan (fotocopy, komunikasi, pigura, dll)</td>
                                              <td colspan="2" class="text-center">100</td>
                                              <td class="text-center">Rp. 2.350.000,-</td>
                                              <td class="text-right">Rp. 2.350.000,-</td>
                                          </tr>
                                          <tr class="success">
                                              <th></th>
                                              <th colspan="8">Jumlah Biaya IV</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END DATA BIAYA IV -->
                                          <!-- START HEADER BIAYA V -->
                                          <tr class="success">
                                              <th>V</th>
                                              <th colspan="8">Jumlah Biaya I + II + III + IV</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END HEADER BIAYA V -->
                                          <!-- START HEADER BIAYA VI -->
                                          <tr class="danger">
                                              <th>VI</th>
                                              <th colspan="8">PPN 10%</th>
                                              <th class="text-right">Rp. 20.350.000,-</th>
                                          </tr>
                                          <!-- END HEADER BIAYA VI -->
                                          <!-- START HEADER BIAYA VII -->
                                          <tr class="active">
                                              <th>VII</th>
                                              <th colspan="8"><strong>Total Biaya Jasa Setelah PPN</strong></th>
                                              <th class="text-right">Rp. 200.350.000,-</th>
                                          </tr>
                                          <!-- END HEADER BIAYA VII -->
                                    </tbody>
                                </table>
                            </div>
					    </div>
					</div>
					<hr />
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="#" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
                        </div>
                    </div>
				</div>
			</div>
		</div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection