@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Form <strong>RAB</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">RAB</a></li>
					<li class="active">New</li>
				</ol>
			</div>
		</div>
	    <div class="row">
			<div class="col-lg-12 portlets">
				<div class="panel">
					<div class="panel-header panel-controls bg-primary">
					  <h3><i class="fa fa-table"></i> Form Input <strong>RAB</strong></h3>
					</div>
					<div class="panel-content">
						<div class="nav-tabs2" id="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#remunerasi" data-toggle="tab"><i class="icon-home"></i>
                                        Remunerasi
                                    </a>
                                </li>
                                <li>
                                    <a href="#biayalangsung" data-toggle="tab"><i class="icon-user"></i>
                                        Biaya Langsung
                                    </a>
                                </li>
                                <li>
                                    <a href="#biayajasainspeksi" data-toggle="tab"><i class="icon-user"></i>
                                        Biaya Jasa Inspeksi
                                    </a>
                                </li>
                                <li>
                                    <a href="#biayamaterialpendukung" data-toggle="tab"><i class="icon-user"></i>
                                        Biaya Material Pendukung
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active" id="remunerasi">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                              <th rowspan="3" colspan="2" class="text-left">UNSUR BIAYA</th>
                                              <th rowspan="3" class="text-center" style="width:300px">TARIF</th>
                                              <th colspan="5" class="text-center">HARI ORANG (MD)</th>
                                              <th rowspan="3" class="text-center" style="width:300px">JUMLAH BIAYA</th>
                                          </tr>
                                          <tr>
                                              <th colspan="2" class="text-center">KANTOR</th>
                                              <th colspan="2" class="text-center">LAPANGAN</th>
                                              <th rowspan="2" class="text-center" style="width:50px">JUMLAH</th>
                                          </tr>
                                          <tr>
                                              <th class="text-center" style="width:75px">HARI</th>
                                              <th class="text-center" style="width:75px">ORANG</th>
                                              <th class="text-center" style="width:75px">HARI</th>
                                              <th class="text-center" style="width:75px">ORANG</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                              <tr class="item-row">
                                                  <th>F1</th>
                                                  <th>Integration 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th>F2</th>
                                                  <th>Advance 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th>F3</th>
                                                  <th>Optimization 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th>F4</th>
                                                  <th>System 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th>F5</th>
                                                  <th>Spesific 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th>F6</th>
                                                  <th>Basic 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required/></td>
                                              </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="biayalangsung">
                                    <table class="table">
                                        <thead>
                                              <tr>
                                                  <th rowspan="2" colspan="2" class="text-left">NO</th>
                                                  <th rowspan="2" class="text-left">UNSUR BIAYA</th>
                                                  <th rowspan="2" class="text-center" style="width:300px">TARIF</th>
                                                  <th colspan="2" class="text-center">HARI ORANG (MD)</th>
                                                  <th rowspan="2" class="text-center" style="width:300px">JUMLAH BIAYA</th>
                                              </tr>
                                              <tr>
                                                  <th class="text-center" style="width:75px">HARI</th>
                                                  <th class="text-center" style="width:75px">ORANG</th>
                                              </tr>
                                        </thead>
                                        <tbody>
                                              <tr>
                                                  <td>1</td>
                                                  <th colspan="8">Uang Harian (SPPD)</th>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F1</th>
                                                  <th>Integration 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F2</th>
                                                  <th>Advance 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F3</th>
                                                  <th>Optimization 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F4</th>
                                                  <th>System 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F5</th>
                                                  <th>Spesific 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F6</th>
                                                  <th>Basic 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th colspan="2">STH Spesific 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr>
                                                  <td>2</td>
                                                  <th colspan="8">Biaya Harian (Akomodasi)</th>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F1</th>
                                                  <th>Integration 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F2</th>
                                                  <th>Advance 1 - 3</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F3</th>
                                                  <th>Optimization 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F4</th>
                                                  <th>System 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F5</th>
                                                  <th>Spesific 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th>F6</th>
                                                  <th>Basic 1 - 4</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <th></th>
                                                  <th colspan="2">Inspektor / Senior Inspektor</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="biayajasainspeksi">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                              <th rowspan="3" class="text-left">NO</th>
                                              <th rowspan="3" class="text-left">UNSUR BIAYA</th>
                                              <th colspan="2" class="text-center">HARI ORANG (MD)</th>
                                              <th rowspan="3" class="text-center" style="width:75px">JUMLAH</th>
                                              <th rowspan="3" class="text-center" style="width:300px">JUMLAH BIAYA</th>
                                          </tr>
                                          <tr>
                                              <th colspan="2" class="text-center">LAPANGAN</th>
                                          </tr>
                                          <tr>
                                              <th class="text-center" style="width:75px">HARI</th>
                                              <th class="text-center" style="width:75px">ORANG</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                              <tr class="item-row">
                                                  <td>1</td>
                                                  <th>Biaya Administrasi Teknik</th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <td>2</td>
                                                  <th>Biaya Outsourcing Inspektor </th>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                              <tr class="item-row">
                                                  <td>3</td>
                                                  <th>Biaya Witness Regulator (Transport+Akomodasi+Harian)</th>
                                                  <td colspan="2" class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                                  <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="biayamaterialpendukung">
                                <table class="table">
                                    <thead>
                                      <tr>
                                          <th class="text-left">NO</th>
                                          <th class="text-left">UNSUR BIAYA</th>
                                          <th class="text-center" style="width:75px">QTY</th>
                                          <th class="text-center" style="width:300px">JUMLAH</th>
                                          <th class="text-center" style="width:300px">JUMLAH BIAYA</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                          <tr class="item-row">
                                              <td>1</td>
                                              <th>Transportasi petugas ke tempat tujuan</th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                          <tr class="item-row">
                                              <td>2</td>
                                              <th>Biaya Sewa Kendaraan </th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                          <tr class="item-row">
                                              <td>3</td>
                                              <th>Biaya Laptek</th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                          <tr class="item-row">
                                              <td>4</td>
                                              <th>Biaya Sewa Laptop</th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                          <tr class="item-row">
                                              <td>5</td>
                                              <th>Biaya alat Pelindung Diri (APD)</th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                          <tr class="item-row">
                                              <td>6</td>
                                              <th>Biaya lain yang relevan (fotocopy, komunikasi, pigura, dll)</th>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                              <td class="text-center"><input name="x" type="text" class="form-control form-white" required=""></td>
                                          </tr>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
					</div>
					<hr />
					<div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Jumlah Biaya I+II+III+IV</th>
                                        <th><input class="form-control form-white" type="text" readonly="" style="width:300px"></th>
                                    </tr>
                                    <tr>
                                        <th>PPN 10%</th>
                                        <th><input class="form-control form-white" type="text" readonly="" style="width:300px"></th>
                                    </tr>
                                    <tr>
                                        <th>TOTAL BIAYA JASA SETELAH PPN</th>
                                        <th><input class="form-control input-lg bg-aero" type="text" readonly="" style="width:300px"></th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="pull-right">
                                <a href="#" class="btn btn-warning btn-square btn-embossed">Kembali <i class="icon-ban"></i></a>
                                <a href="#" class="btn btn-success btn-square btn-embossed">Print <i class="fa fa-print"></i></a>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection