@extends('../layout/layout_internal')

@section('page_css')

@endsection

@section('content')
    <div class="page-content page-thin">
        <div class="row">
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-shopping-cart bg-green"></i>
                            </div>
                            <div class="right">
                                @if($permohonan_baru > 0)
                                    <p class="number countup" data-from="0"
                                       data-to="{{$permohonan_baru}}">{{$permohonan_baru}}</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">Permohonan Baru</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-bolt bg-blue"></i>
                            </div>
                            <div class="right">
                                @if($jumlah_instalasi > 0)
                                    <p class="number countup" data-from="0" data-to="{{$jumlah_instalasi}}">0</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">Instalasi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-send bg-red"></i>
                            </div>
                            <div class="right">
                                @if($lhpp > 0)
                                    <p class="number countup" data-from="0" data-to="{{$lhpp}}">0</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">LHPP</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-file-text-o bg-purple"></i>
                            </div>
                            <div class="right">
                                @if($lpi > 0)
                                    <p class="number countup" data-from="0" data-to="{{$lpi}}">0</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">LPI</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Permohonan Baru -->
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="icon-list"></i> Permohonan <strong>Baru</strong></h3>
                        <div class="control-btn">
                            <span class="pull-right badge badge-primary">{{$latest_permohonan->count()}}</span>
                        </div>
                    </div>
                    <div class="panel-content widget-table">
                        <div class="withScroll" data-height="400">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Nama Instalasi</th>
                                    <th>Pekerjaan</th>
                                    <th>Perusahaan</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_permohonan as $row)
                                    <?php
                                    $badge = "primary";
                                    if ($row->order->flow_status->tipe_status == SUBMITTED) {
                                        $badge = "primary";
                                    } else if ($row->order->flow_status->tipe_status == APPROVED) {
                                        $badge = "success";
                                    } else if ($row->order->flow_status->tipe_status == REJECTED) {
                                        $badge = "danger";
                                    }
                                    ?>
                                    <tr>
                                        <td>{{date('d M Y',strtotime(@$row->updated_at))}}</td>
                                        <td>{{@$row->instalasi->nama_instalasi}}
                                            @if(@$row->bayPermohonan[0]->bayGardu != null)
                                                <br/><b>{{@$row->bayPermohonan[0]->bayGardu->nama_bay}}</b>
                                            @endif
                                        </td>
                                        <td>{{@$row->tipeInstalasi->nama_instalasi}}</td>
                                        <td>{{@$row->order->perusahaan->nama_perusahaan}}</td>
                                        <td>
                                            <div class="badge badge-{{$badge}}">{{@$row->order->flow_status->tipe_status}}</div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search Tracking -->
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-header">
                        <h3><i class="icon-list"></i> Status <strong>SLO</strong></h3>
                        <div class="control-btn">
                            <span class="pull-right badge badge-primary">{{$latest_slo->count()}}</span>
                        </div>
                    </div>
                    <div class="panel-content widget-table">
                        <div class="withScroll" data-height="400">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>No. SLO</th>
                                    <th>Pemohon</th>
                                    <th>No. Agenda</th>
                                    <th>No. Permohonan</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_slo as $row)
                                    <?php
                                    $badge = "primary";
                                    if ($row->status_permohonan_djk == SENT) {
                                        $badge = "primary";
                                    } else if ($row->status_permohonan_djk == APPROVED) {
                                        $badge = "success";
                                    } else if ($row->status_permohonan_djk == REJECTED) {
                                        $badge = "danger";
                                    }
                                    ?>
                                    <tr>
                                        <td>{{@$row->no_slo}}</td>
                                        <td>{{@$row->nama_pemohon}}</td>
                                        <td>{{@$row->no_djk_agenda}}</td>
                                        <td>{{@$row->no_djk_permohonan}}</td>
                                        <td>
                                            <div class="badge badge-{{$badge}}">{{@$row->status_permohonan_djk}}</div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <!-- Search Tracking -->
            <div class="col-md-12">
                <div class="panel">
                    {{--<div class="panel-header">--}}
                    {{--<h3><i class="icon-magnifier"></i> <strong>Tracking</strong></h3>--}}
                    {{--</div>--}}
                    {{--<div class="panel-content">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-md-12">--}}
                    {{--<form>--}}
                    {{--<div class="append-icon">--}}
                    {{--<input type="text" name="firstname" id="finder" class="form-control form-white input-lg" placeholder="Masukkan Nomor Registrasi. . ." autofocus>--}}
                    {{--<i class="icon-magnifier"></i>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                            <!-- Slider -->
                    <div class="panel-content">
                        <div class="slick" data-fade="true">
                            <div class="slide">
                                <img src="{{ url('/') }}/assets/global/images/widgets/3500.jpg" style="width: 100%">
                            </div>
                            <div class="slide">
                                <img src="{{ url('/') }}/assets/global/images/widgets/3_langkah.png"
                                     style="width: 100%">
                            </div>
                            <div class="slide">
                                <img src="{{ url('/') }}/assets/global/images/widgets/cc123.jpg" style="width: 100%">
                            </div>
                            <div class="slide">
                                <img src="{{ url('/') }}/assets/global/images/widgets/1kwh.jpg" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/slick/slick.min.js"></script> <!-- Slider -->
            <script src="{{ url('/') }}/assets/global/plugins/prettify/prettify.min.js"></script>
            <!-- Show html code -->
            <script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/countup/countUp.min.js"></script>
@endsection