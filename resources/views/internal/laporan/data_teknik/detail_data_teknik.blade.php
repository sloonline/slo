@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Teknik</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Data Teknik</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::hidden('pekerjaan_id',@$data_teknik->pekerjaan_id) !!}
                                        <input readonly name="pekerjaan" type="text" class="form-control"
                                               value="{{@$data_teknik->pekerjaan->pekerjaan}}" placeholder="Pekerjaan"
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input name="instalasi" type="text"
                                               value="{{@$data_teknik->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{($data_teknik == null) ? "#" : url('/internal/create_permohonan/'.@$data_teknik->pekerjaan->permohonan->id_orders."/".@$data_teknik->pekerjaan->permohonan_id)}}"
                                           target="_blank" id="detail_instalasi"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan"
                                               value="{{@$data_teknik->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly
                                               required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Data Teknik</label>
                                    </div>
                                    @if(@$data_teknik->hasil_data_teknik != null)
                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$data_teknik->hasil_data_teknik)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center><i class="fa fa-download"></i>Preview File</center>
                                                </button>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/data_teknik') }}"
                                       class="btn btn-warning btn-square btn-embossed">Kembali
                                        &nbsp;<i class="icon-ban"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

@endsection
