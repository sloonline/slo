<fieldset class="cart-summary">
    <legend>DATA PEMBANGKIT</legend>
    <input type="hidden" class="form-control form-white" name="pemilik_instalasi_id">
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Nama Pembangkit</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="nama_pembangkit">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Jenis Instalasi</label>
        </div>
        <div class="col-sm-6">
            <select name="jenis_instalasi_id" data-search="true" class="form-control">
                <option>-- Pilih Jenis Instalasi --</option>
                @foreach(@$jenis_instalasi as $item)
                <option value="{{$item->id}}">{{$item->jenis_instalasi}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Lokasi Instalasi</label>
        </div>
        <div class="col-sm-6">
            <textarea class="form-control  form-white" name="lokasi_instalasi"></textarea>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Provinsi</label>
        </div>
        <div class="col-sm-6">
            <select name="provinsi_id" data-search="true" class="form-control">
                <option>-- Pilih Provinsi --</option>
                @foreach(@$provinsi as $item)
                <option value="{{$item->id}}">{{$item->province}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kota</label>
        </div>
        <div class="col-sm-6">
            <select name="kota_id" data-search="true" class="form-control">
                <option>-- Pilih Kota --</option>
                @foreach(@$kota as $item)
                <option value="{{$item->id}}">{{$item->city}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Longitude</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="longitude">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Latitude</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="latitude">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Jenis Usaha</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="jenis_usaha">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="penerbit_ijin_usaha">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Berdasarkan</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="berdasarkan">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kontrak Sewa *) Jika Ada</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kontrak_sewa">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Nomor Kontrak *) Jika Ada</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="nomor_kontrak_sewa">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Tanggal Kontrak *) Jika Ada</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="tanggal_kontrak_sewa">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Masa Berlaku Sewa *) Jika Ada</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="masa_berlaku_kontrak">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kapasitas Terpasang</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kapasitas_terpasang">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kapasitas Hasil Uji</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kapasitas_hasil_uji">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Nomor Unit Pembangkit</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="no_unit_pembangkit">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Nomor Seri Generator</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="no_seri_generator">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Nomor Seri Turbin</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="no_seri_turbin">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kapasitas Modul Per Unit</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kapasitas_modul_per_unit">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Kapasitas Inverter Per Unit</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kapasitas_inverter_per_unit">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Jumlah Modul</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="kapasitas">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Jumlah Inverter</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="masa_berlaku_kontrak">
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Bahan Bakar</label>
        </div>
        <div class="col-sm-6">
            <select name="jenis_bahan_bakar_id" data-search="true" class="form-control">
                <option>-- Pilih Bahan Bakar --</option>
                @foreach(@$jenis_bahan_bakar as $item)
                <option value="{{$item->id}}">{{$item->nama_reference}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group col-lg-12">
        <div class="col-sm-3">
            <label class="col-sm-12 control-label">Net Plant Heat Rate HHV</label>
        </div>
        <div class="col-sm-6">
            <input class="form-control form-white" name="masa_berlaku_kontrak">
        </div>
    </div>
</fieldset>