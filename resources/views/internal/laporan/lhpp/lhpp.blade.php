@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Laporan Hasil <strong>Pemeriksaan dan Pengujian</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">LHPP</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('alert_type')}}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                        @if(Session::has('data_null'))
                            @foreach(Session::get('data_null') as $item)
                                {{$item.', '}}
                            @endforeach
                        @endif
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_LHPP))
                                <div class="btn-group">
                                    <a href="{{url('internal/lhpp/createLhpp')}}"
                                    class="btn btn-success"><i class="fa fa-plus"></i>
                                        LHPP Baru</a>
                                </div>
                            @endif
                            <div class="btn-group pull-right">
                                <a href="{{url('internal/api/getHasilSlo')}}"
                                   class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></a>
                            </div>
                            <br>
                            <br>
                            <ul class="nav nav-tabs nav-primary">
                                <li class="active"><a href="#baru" data-toggle="tab"><i class="icon-info"></i>
                                        PENGAJUAN BARU</a></li>
                                <li><a href="#verifikasi" data-toggle="tab"><i class="icon-pie-chart"></i>
                                        PROSES VERIFIKASI DJK</a></li>
                                <li><a href="#diterima" data-toggle="tab"><i class="icon-check"></i>
                                        DITERIMA</a></li>
                                <li><a href="#ditolak" data-toggle="tab"><i class="fa fa-ban"></i>
                                        DITOLAK</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="baru">
                                    <div class="pull-left col-sm-2" style="margin-left:0;padding-left:0">
                                        <select class="form-control form-white" data-style="btn-primary"
                                                id="filter_baru">
                                            <option value="">Tampilkan Semua</option>
                                            <option value="Pembangkit">Pembangkit</option>
                                            <option value="Transmisi">Transmisi</option>
                                            <option value="Distribusi">Distribusi</option>
                                            <option value="Pemanfaatan TT">Pemanfaatan TT</option>
                                            <option value="Pemanfaatan TM">Pemanfaatan TM</option>
                                        </select>
                                    </div>
                                    <br/><br/><br/>
                                    <table class="table table-hover table-dynamic" id="table_baru">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No SLO</th>
                                            <th>Tipe Instalasi</th>
                                            <th>Nama Instalasi</th>
                                            <th>Pemohon</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        $no = 1;
                                        ?>
                                        <tbody>
                                        @foreach($lhpp_baru as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{@$item->no_slo}}</td>
                                                <td>{{@$item->tipeInstalasi->nama_instalasi}}</td>
                                                <td>{{@$item->instalasi->nama_instalasi}}
                                                    @if(@$item->gardu != null)
                                                        ({{@$item->gardu->nama_bay}})
                                                    @endif
                                                </td>
                                                <td>{{@$item->nama_pemohon}}</td>
                                                <td width="20%">
                                                    <a href="{{url('internal/lhpp/detailLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-primary"><i
                                                                class="fa fa-eye"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Detail"></i></a>

                                                    <a href="{{url('/internal/lhpp/update/'.@$item->id)}}"
                                                       class="btn btn-sm btn-warning"><i
                                                                class="fa fa-pencil"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Update Data"></i></a>

                                                    <a href="{{url('internal/lhpp/deleteLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-danger"
                                                       onclick="return confirm('Delete Data LHPP?')"><i
                                                                class="fa fa-trash"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Delete"></i></a>
                                                    @if(@$item->no_djk_agenda != null && @$item->status_permohonan_djk == DJK_NOT_SENT)
                                                        <a href="{{url('internal/lhpp/registrasiSlo/'.@$item->id)}}"
                                                           class="btn btn-sm btn-success"
                                                           onclick="return confirm('Confirm to Send to DJK?')"><i
                                                                    class="fa fa-paper-plane"
                                                                    data-rel="tooltip"
                                                                    data-placement="right" title="Send to DJK"></i></a>
                                                    @else
                                                        <a href="#"
                                                           class="btn btn-sm btn-default"><i
                                                                    class="fa fa-paper-plane"
                                                                    data-rel="tooltip"
                                                                    data-placement="left"
                                                                    title="BELUM Memiliki No Agenda"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade in" id="verifikasi">
                                    <div class="pull-left col-sm-2" style="margin-left:0;padding-left:0">
                                        <select class="form-control form-white" data-style="btn-primary"
                                                id="filter_verifikasi">
                                            <option value="">Tampilkan Semua</option>
                                            <option value="Pembangkit">Pembangkit</option>
                                            <option value="Transmisi">Transmisi</option>
                                            <option value="Distribusi">Distribusi</option>
                                            <option value="Pemanfaatan TT">Pemanfaatan TT</option>
                                            <option value="Pemanfaatan TM">Pemanfaatan TM</option>
                                        </select>
                                    </div>
                                    <br/><br/><br/>
                                    <table class="table table-hover table-dynamic" id="table_verifikasi">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No SLO</th>
                                            <th>Tipe Instalasi</th>
                                            <th>Nama Instalasi</th>
                                            <th>Pemohon</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        $no = 1;
                                        ?>
                                        <tbody>
                                        @foreach($lhpp_verifikasi as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{@$item->no_slo}}</td>
                                                <td>{{@$item->tipeInstalasi->nama_instalasi}}</td>
                                                <td>{{@$item->instalasi->nama_instalasi}}
                                                    @if(@$item->gardu != null)
                                                        ({{@$item->gardu->nama_bay}})
                                                    @endif
                                                </td>
                                                <td>{{@$item->nama_pemohon}}</td>
                                                <td width="30%">
                                                    <a href="{{url('/internal/lhpp/update/'.@$item->id)}}"
                                                       class="btn btn-sm btn-warning"><i
                                                                class="fa fa-pencil"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Update Data"></i></a>
                                                    <a href="{{url('internal/lhpp/detailLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-primary"><i
                                                                class="fa fa-eye"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Detail"></i></a>
                                                    <a href="{{url('internal/api/getHasilSlo/'.@$item->id)}}"
                                                        class="btn btn-sm btn-success"><i
                                                                class="fa fa-refresh"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Get Hasil"></i></a>

                                                    <a class="btn btn-sm btn-primary btn-transparent">
                                                        Menunggu Verifikasi DJK
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade in" id="diterima">
                                    <div class="pull-left col-sm-2" style="margin-left:0;padding-left:0">
                                        <select class="form-control form-white" data-style="btn-primary"
                                                id="filter_diterima">
                                            <option value="">Tampilkan Semua</option>
                                            <option value="Pembangkit">Pembangkit</option>
                                            <option value="Transmisi">Transmisi</option>
                                            <option value="Distribusi">Distribusi</option>
                                            <option value="Pemanfaatan TT">Pemanfaatan TT</option>
                                            <option value="Pemanfaatan TM">Pemanfaatan TM</option>
                                        </select>
                                    </div>
                                    <br/><br/><br/>
                                    <table class="table table-hover table-dynamic" id="table_diterima">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No SLO</th>
                                            <th>Tipe Instalasi</th>
                                            <th>Nama Instalasi</th>
                                            <th>Pemohon</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        $no = 1;
                                        ?>
                                        <tbody>
                                        @foreach($lhpp_diterima as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{@$item->no_slo}}</td>
                                                <td>{{@$item->tipeInstalasi->nama_instalasi}}</td>
                                                <td>{{@$item->instalasi->nama_instalasi}}
                                                    @if(@$item->gardu != null)
                                                        ({{@$item->gardu->nama_bay}})
                                                    @endif
                                                </td>
                                                <td>{{@$item->nama_pemohon}}</td>
                                                <td width="20%">
                                                    <a href="{{url('internal/lhpp/detailLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-primary"><i
                                                                class="fa fa-eye"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Detail"></i></a>

                                                    @if(@$item->djk_hasil()->url_sertifikat != null)
                                                        <a target="_blank" href="{{@$item->djk_hasil()->url_sertifikat}}"
                                                           class="btn btn-sm btn-success"><i
                                                                    class="fa fa-file"
                                                                    data-rel="tooltip"
                                                                    data-placement="right"
                                                                    title="Download Sertifikat"></i>&nbsp Cetak
                                                            Sertifikat</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade in" id="ditolak">
                                    <div class="pull-left col-sm-2" style="margin-left:0;padding-left:0">
                                        <select class="form-control form-white" data-style="btn-primary"
                                                id="filter_ditolak">
                                            <option value="">Tampilkan Semua</option>
                                            <option value="Pembangkit">Pembangkit</option>
                                            <option value="Transmisi">Transmisi</option>
                                            <option value="Distribusi">Distribusi</option>
                                            <option value="Pemanfaatan TT">Pemanfaatan TT</option>
                                            <option value="Pemanfaatan TM">Pemanfaatan TM</option>
                                        </select>
                                    </div>
                                    <br/><br/><br/>
                                    <table class="table table-hover table-dynamic" id="table_ditolak">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tipe Instalasi</th>
                                            <th>Nama Instalasi</th>
                                            <th>Pemohon</th>
                                            <th>Alasan Penolakan</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <?php
                                        $no = 1;
                                        ?>
                                        <tbody>
                                        @foreach($lhpp_ditolak as $item)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{@$item->tipeInstalasi->nama_instalasi}}</td>
                                                <td>{{@$item->instalasi->nama_instalasi}}
                                                    @if(@$item->gardu != null)
                                                        ({{@$item->gardu->nama_bay}})
                                                    @endif
                                                </td>
                                                <td>{{@$item->nama_pemohon}}</td>
                                                <td>{{@$item->djk_hasil()->alasan_penolakan}}</td>
                                                <td width="20%">
                                                    <a href="{{url('internal/lhpp/detailLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-primary"><i
                                                                class="fa fa-eye"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Detail"></i></a>

                                                    <a href="{{url('/internal/lhpp/update/'.@$item->id)}}"
                                                       class="btn btn-sm btn-warning"><i
                                                                class="fa fa-pencil"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Update Data"></i></a>

                                                    <a href="{{url('internal/lhpp/deleteLhpp/'.@$item->id)}}"
                                                       class="btn btn-sm btn-danger"
                                                       onclick="return confirm('Delete Data LHPP?')"><i
                                                                class="fa fa-trash"
                                                                data-rel="tooltip"
                                                                data-placement="right" title="Delete"></i></a>
                                                    @if(@$item->no_djk_agenda != null && @$item->no_djk_permohonan == null)
                                                        <a href="{{url('internal/lhpp/registrasiSlo/'.@$item->id)}}"
                                                           class="btn btn-sm btn-success"><i
                                                                    class="fa fa-paper-plane"
                                                                    data-rel="tooltip"
                                                                    data-placement="right" title="Send to DJK"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    //TABLE LHPP BARU
                    var oTable1 = $('#table_baru').dataTable();
                    $('#filter_baru').each(function () {
                        oTable1.fnFilter($(this).val(), 2);
                        console.log('each | ' + $(this).val());
                    });
                    $('#filter_baru').change(function () {
                        oTable1.fnFilter($(this).val(), 2);
                        console.log('change | ' + $(this).val());
                    });

                    //TABLE LHPP VERIFIKASI
                    var oTable2 = $('#table_verifikasi').dataTable();
                    $('#filter_verifikasi').each(function () {
                        oTable2.fnFilter($(this).val(), 2);
                        console.log('each | ' + $(this).val());
                    });
                    $('#filter_verifikasi').change(function () {
                        oTable2.fnFilter($(this).val(), 2);
                        console.log('change | ' + $(this).val());
                    });

                    //TABLE LHPP DITERIMA
                    var oTable3 = $('#table_diterima').dataTable();
                    $('#filter_diterima').each(function () {
                        oTable3.fnFilter($(this).val(), 2);
                        console.log('each | ' + $(this).val());
                    });
                    $('#filter_diterima').change(function () {
                        oTable3.fnFilter($(this).val(), 2);
                        console.log('change | ' + $(this).val());
                    });

                    //TABLE LHPP DITOLAK
                    var oTable4 = $('#table_ditolak').dataTable();
                    $('#filter_ditolak').each(function () {
                        oTable4.fnFilter($(this).val(), 1);
                        console.log('each | ' + $(this).val());
                    });
                    $('#filter_ditolak').change(function () {
                        oTable4.fnFilter($(this).val(), 1);
                        console.log('change | ' + $(this).val());
                    });
                });
            </script>
@endsection
    