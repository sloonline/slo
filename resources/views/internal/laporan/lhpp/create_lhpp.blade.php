@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <style>
        textarea {
            resize: both;
        }
    </style>
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>LHPP</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Data LHPP</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/lhpp/saveLhpp', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <input type="hidden" value="{{@$lhpp->id}}" name="lhpp_id">
                            <div class="panel-content pagination2 table-responsive">

                                <ul class="nav nav-tabs nav-primary">
                                    <li class="active"><a href="#instalasi" data-toggle="tab"><i class="icon-info"></i>
                                        INSTALASI</a></li>
                                    <li><a href="#data_umum" data-toggle="tab"><i class="icon-speedometer"></i>
                                        DATA UMUM</a></li>
                                    <li><a href="#badan_usaha" data-toggle="tab"><i class="icon-user"></i> BADAN
                                            USAHA</a></li>
                                    <li><a href="#pelaksana" data-toggle="tab"><i class="icon-map"></i> PELAKSANA</a>
                                    </li>
                                    <li><a href="#pemasangan" data-toggle="tab"><i class="icon-map"></i> KONTRAKTOR
                                            PEMASANGAN</a></li>
                                    <li><a href="#perencana" data-toggle="tab"><i class="icon-map"></i> KONSULTAN
                                            PERENCANA</a></li>
                                    <li><a href="#upload" data-toggle="tab"><i class="icon-map"></i> UPLOAD DATA</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade in" id="data_umum">
                                        <fieldset class="cart-summary">
                                            <legend>Sertifikat</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor LHPP *</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="no_lhpp" required
                                                           value="{{@$lhpp->no_lhpp}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal LHPP *</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="tgl_lhpp"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd" required
                                                           value="{{@$lhpp->tgl_lhpp}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor SLO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="no_slo" readonly
                                                           value="{{@$lhpp->no_slo}}">
                                                </div>
                                                @if(@$lhpp != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-success"
                                                           href="{{url('/internal/lhpp/generateNoSlo/'.@$lhpp->id)}}"><i
                                                                    class="fa fa-check"></i> Generate No SLO</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Agenda DJK</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input value='{{@$no_agenda}}' name="no_djk_agenda"
                                                           class="form-control form-white" readonly>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="cart-summary">
                                            <legend>Data Umum</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Permohonan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="no_permohonan" readonly
                                                           value="{{@$lhpp->no_permohonan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Pemohon</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_pemohon"
                                                           value="{{@$lhpp->nama_pemohon}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Surat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nomor_surat"
                                                           value="{{@$lhpp->no_surat}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Surat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="tanggal_surat_permohonan"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd"
                                                           value="{{@$lhpp->tgl_surat}}">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane fade active in" id="instalasi">
                                        <fieldset class="cart-summary">
                                            <legend>Data Instalasi</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tipe Instalasi *</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="tipe_instalasi_id" data-search="true"
                                                            class="form-control" id="tipe_instalasi" required>
                                                        <option value=''>-- Tipe Instalasi --</option>
                                                        @foreach(@$tipe_instalasi as $item)
                                                            <option {{(@$lhpp->tipe_instalasi == $item->id) ? 'selected':''}} value="{{$item->id}}">{{$item->nama_instalasi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Instalasi *</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="instalasi_id" data-search="true" class="form-control"
                                                            id="select_instalasi">
                                                        <option {{(@$lhpp->instalasi_id == $item->id) ? 'selected':''}} value=''>
                                                            -- Nama Instalasi --
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a id="ref_detail_instalasi" target="_blank"
                                                       class="btn btn-primary"><i class="fa fa-info-circle"></i> Detail</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12" id="div_lingkup" style="display:none">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Bay Gardu *</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select name="lingkup_transmisi" data-search="true"
                                                            class="form-control" id="select_lingkup">
                                                        <option {{(@$lhpp->bay_gardu_id == $item->id) ? 'selected':''}} value=''>
                                                            -- Jenis Lingkup --
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="detail_kit" style="display: none;">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kapasitas Hasil
                                                            Uji</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white"
                                                               name="kapasitas_hasil_uji"
                                                               value="{{@$lhpp->lhpp_kit->kapasitas_hasil_uji}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kalori HHV</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white" name="kalori_hhv"
                                                               value="{{@$lhpp->lhpp_kit->kalori_hhv}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kalori LHV</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white" name="kalori_lhv"
                                                               value="{{@$lhpp->lhpp_kit->kalori_lhv}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kapasitas Modul</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white"
                                                               name="kapasitas_modul"
                                                               value="{{@$lhpp->lhpp_kit->kapasitas_modul}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kapasitas
                                                            Inverter</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white"
                                                               name="kapasitas_inverter"
                                                               value="{{@$lhpp->lhpp_kit->kapasitas_inverter}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jumlah Modul</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white"
                                                               name="jumlah_modul"
                                                               value="{{@$lhpp->lhpp_kit->jumlah_modul}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jumlah Inverter</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control form-white"
                                                               name="jumlah_inverter"
                                                               value="{{@$lhpp->lhpp_kit->jumlah_inverter}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Konsumsi Bahan
                                                            Bakar</label>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="file" class="form-control form-white"
                                                               name="file_konsumsi_bb">
                                                    </div>
                                                    @if(@$lhpp->lhpp_kit->url_file_konsumsi_bb != null)
                                                        <div class="col-sm-3">
                                                            <a class="btn btn-primary" target="_blank"
                                                               href="{{url('upload/'.@$lhpp->lhpp_kit->url_file_konsumsi_bb)}}">Preview</a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </fieldset>
                                        {{--  @include('internal.laporan.lhpp.lhpp_kit')  --}}
                                    </div>
                                    <div class="tab-pane fade in" id="badan_usaha">
                                        <fieldset class="cart-summary">
                                            <legend>Badan Usaha Jasa Pembangunan dan Pemasangan Instalasi</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_bujptl"
                                                           value="{{@$lhpp->nama_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="alamat_bujptl"
                                                           value="{{@$lhpp->alamat_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kode Badan Usaha</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kode_bujptl"
                                                           value="{{@$lhpp->kode_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="klasifikasi_bujptl"
                                                           value="{{@$lhpp->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kualifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kualifikasi_bujptl"
                                                           value="{{@$lhpp->kualifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penanggung Jawab Badan
                                                        Usaha</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="pj_bujptl"
                                                           value="{{@$lhpp->pj_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penanggung Jawab
                                                        Teknik</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="pjt_bujptl"
                                                           value="{{@$lhpp->pjt_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="masa_berlaku_bujptl"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd"
                                                           value="{{@$lhpp->masa_berlaku}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Gambar Main Single Line
                                                        Diagram</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="no_msl_diagram"
                                                           value="{{@$lhpp->no_msl_diagram}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="tgl_msl_diagram"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd"
                                                           value="{{@$lhpp->tgl_msl_diagram}}">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane fade in" id="pelaksana">
                                        <fieldset class="cart-summary">
                                            <legend>Pelaksana Uji Layak Operasi</legend>

                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Waktu Pelaksanaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="waktu_pelaksanaan"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd"
                                                           value="{{@$lhpp->waktu_pelaksanaan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Surat Pernyataan Kesesuaian
                                                        Persyaratan Pemeriksaan dan Pengujian</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="no_spkppp"
                                                           value="{{@$lhpp->no_spkppp}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"
                                                           name="tgl_spkppp"
                                                           class="form-control b-datepicker form-white"
                                                           data-date-format="yyyy-mm-dd"
                                                           data-lang="en"
                                                           data-RTL="false"
                                                           placeholder="yyyy-mm-dd"
                                                           value="{{@$lhpp->tgl_spkppp}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tim Pelaksana Uji Laik
                                                        Operasi</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Ketua Tim Pelaksana</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="ketua_nama" class="form-control form-white" id="ketua"
                                                            data-search="true">
                                                        <option value="">-- Pilih TT --</option>
                                                        @foreach($master_tt as $item)
                                                            <option {{(@$ketua->nama == $item->nama) ? 'selected':''}} data-ketua="{{$item}}" value="{{$item->nama}}">{{$item->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIK</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" id="nik_ketua"
                                                           name="nik_ketua"
                                                           value="{{@$ketua->nik}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Register
                                                        Kompetensi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="ketua_noreg_kompetensi"
                                                           value="{{@$ketua->no_reg_kompetensi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="ketua_klasifikasi"
                                                           value="{{@$ketua->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Level</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="ketua_level"
                                                           value="{{@$ketua->level}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Anggota</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="agt_nama" class="form-control form-white" id="anggota"
                                                            data-search="true">
                                                        <option value="">-- Pilih Anggota --</option>
                                                        @foreach($master_pjt_tt as $item)
                                                            <option {{(@$anggota->nama == $item->nama) ? 'selected':''}} data-anggota="{{json_encode($item)}}" value="{{$item->nama}}">{{$item->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIK</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" id="nik_anggota"
                                                           name="nik_anggota"
                                                           value="{{@$anggota->nik}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Register
                                                        Kompetensi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="agt_noreg_kompetensi"
                                                           value="{{@$anggota->no_reg_kompetensi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="agt_klasifikasi"
                                                           value="{{@$anggota->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Level</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="agt_level"
                                                           value="{{@$anggota->level}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penanggung Jawab
                                                        Teknik</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="pjt_nama" class="form-control form-white" id="pjt"
                                                            data-search="true">
                                                        <option value="">-- Pilih PJT --</option>
                                                        @foreach($master_pjt as $item)
                                                            <option {{(@$pjt->nama == $item->nama) ? 'selected':''}} data-pjt="{{$item}}" value="{{$item->nama}}">{{$item->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">NIK</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" id="nik_pjt"
                                                           name="nik_pjt"
                                                           value="{{@$pjt->nik}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Register
                                                        Kompetensi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="pjt_noreg_kompetensi"
                                                           value="{{@$pjt->no_reg_kompetensi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="pjt_klasifikasi"
                                                           value="{{@$pjt->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Level</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="pjt_level"
                                                           value="{{@$pjt->level}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12"></div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Hasil Pemeriksaan dan
                                                        Pengujian</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea name="hasil_pp" rows="10"
                                                              class="form-control form-white">{{@$lhpp->hasil_pp}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kesimpulan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea name="kesimpulan" rows="10"
                                                              class="form-control form-white">{{@$lhpp->kesimpulan}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Rekomendasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea name="rekomendasi" rows="10"
                                                              class="form-control form-white">{{@$lhpp->rekomendasi}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Laporan Pelaksanaan
                                                        Inspeksi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="no_lap_p_inspeksi"
                                                           value="{{@$lhpp->no_lap_p_inspeksi}}">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane fade in" id="pemasangan">
                                        <fieldset class="cart-summary">
                                            <legend>Izin Usaha Jasa Penunjang Tenaga Listrik (Kontraktor Pemasangan)
                                            </legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_kont_pmsg"
                                                           value="{{@$kont_pemasangan->nama}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="alamat_kont_pmsg"
                                                           value="{{@$kont_pemasangan->alamat}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Provinsi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="provinsi_kont_pmsg" id="provinsi_kont_pmsg"
                                                            data-search="true"
                                                            class="form-control">
                                                        @foreach(@$provinsi as $item)
                                                            <option {{(@$kont_pemasangan->id_provinsi == $item->id) ? 'selected':''}} value="{{$item->id}}">{{$item->province}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kabupaten/Kota</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="kota_kont_pmsg" id="kota_kont_pmsg" data-search="true"
                                                            class="form-control">
                                                        @foreach(@$kota as $item)
                                                            <option {{(@$kont_pemasangan->id_kota == $item->id) ? 'selected':''}} class="{{$item->id_province}}"
                                                                    value="{{$item->id}}">{{$item->city}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor IUJPTL</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="no_iujptl_kont_pmsg"
                                                           value="{{@$kont_pemasangan->no_iujptl}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kode Badan Usaha</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kode_kont_pmsg"
                                                           value="{{@$kont_pemasangan->kode_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="klasifikasi_kont_pmsg"
                                                           value="{{@$kont_pemasangan->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kualifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kualifikasi_kont_pmsg"
                                                           value="{{@$kont_pemasangan->kualifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="masa_berlaku_kont_pmsg"
                                                           value="{{@$kont_pemasangan->masa_berlaku}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Penanggung Jawab
                                                        Perusahaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_pj_kont_pmsg"
                                                           value="{{@$kont_pemasangan->nama_pj_perusahaan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penerbit Izin</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="penerbit_ijin_kont_pmsg"
                                                           value="{{@$kont_pemasangan->penerbit_ijin}}">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane fade in" id="perencana">
                                        <fieldset class="cart-summary">
                                            <legend>Badan Usaha / Izin Usaha Jasa Penunjang Tenaga Listrik (Konsultan
                                                Perencana)
                                            </legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_kons_prcn"
                                                           value="{{@$kons_perencana->nama}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="alamat_kons_prcn"
                                                           value="{{@$kons_perencana->alamat}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Provinsi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="provinsi_kons_prcn" id="provinsi_kons_prcn"
                                                            data-search="true"
                                                            class="form-control">
                                                        @foreach(@$provinsi as $item)
                                                            <option {{(@$kons_perencana->id_provinsi == $item->id) ? 'selected':''}} value="{{$item->id}}">{{$item->province}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kabupaten/Kota</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select name="kota_kons_prcn" id="kota_kons_prcn" data-search="true"
                                                            class="form-control">
                                                        @foreach(@$kota as $item)
                                                            <option class="{{$item->id_province}}"
                                                                    {{(@$kons_perencana->id_kota == $item->id) ? 'selected':''}} value="{{$item->id}}">{{$item->city}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor IUJPTL</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="no_iujptl_kons_prcn"
                                                           value="{{@$kons_perencana->no_iujptl}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kode Badan Usaha</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kode_kons_prcn"
                                                           value="{{@$kons_perencana->kode_badan_usaha}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Klasifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="klasifikasi_kons_prcn"
                                                           value="{{@$kons_perencana->klasifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kualifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="kualifikasi_kons_prcn"
                                                           value="{{@$kons_perencana->kualifikasi}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Masa Berlaku</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="masa_berlaku_kons_prcn"
                                                           value="{{@$kons_perencana->masa_berlaku}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Penanggung Jawab
                                                        Perusahaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white" name="nama_pj_kons_prcn"
                                                           value="{{@$kons_perencana->nama_pj_perusahaan}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Penerbit Izin</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input class="form-control form-white"
                                                           name="penerbit_ijin_kons_prcn"
                                                           value="{{@$kons_perencana->penerbit_ijin}}">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane fade in" id="upload">
                                        <fieldset class="cart-summary">
                                            <legend>Dokumen Pendukung</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Gambar Main Single Line
                                                        Diagram</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_msl_diagram" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->file_gbr_msl_diagram != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->file_gbr_msl_diagram)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Lembar LHPP</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_lembar_lhpp" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->lembar_lhpp != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->lembar_lhpp)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Laporan Final</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_laporan_final" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->laporan_final != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->laporan_final)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Draft Sertifikat SLO</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_draft_sertifikat_slo" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->draft_sertifikat_slo != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->draft_sertifikat_slo)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Sertifikat SLO (Untuk Cetak
                                                        Ulang)</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_sertifikat_slo" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->sertifikat_slo != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->sertifikat_slo)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Surat Permohonan SLO</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_srt_permohonan_slo" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->surat_permohonan != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->surat_permohonan)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Surat Pernyataan Kesesuaian
                                                        Hasil
                                                        Pemeriksaan dan Pengujian</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="file_srt_kesesuaian" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$lhpp->surat_kesesuaian != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$lhpp->surat_kesesuaian)}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </fieldset>
                                        <fieldset class="cart-summary">
                                            <legend>Foto Pelaksanaan</legend>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Papan Nama
                                                        Lokasi</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="foto[]" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$foto[0] != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$foto[0])}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Foto Instalasi Secara
                                                        Umum</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="foto[]" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$foto[1] != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$foto[1])}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Foto Pelaksanaan
                                                        Inspeksi</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="foto[]" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$foto[2] != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$foto[2])}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Foto Pelaksanaan Rapat Kick
                                                        of
                                                        Meeting</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input name="foto[]" type="file"
                                                           class="form-control form-white">
                                                </div>
                                                @if(@$foto[3] != null)
                                                    <div class="col-sm-3">
                                                        <a class="btn btn-primary" target="_blank"
                                                           href="{{url('upload/'.@$foto[3])}}">Preview</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/lhpp') }}"
                                       class="btn btn-warning btn-square btn-embossed">Batal
                                        &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                            data-style="zoom-in">Simpan &nbsp;<i
                                                class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                    
                $(document).ready(function () {
                    $("#kota_kont_pmsg").chained("#provinsi_kont_pmsg");
                    $("#kota_kons_prcn").chained("#provinsi_kons_prcn");
                    var isTrans = false;
                    var tipeInstalasi = '';
                    var pembangkit = {!!$pembangkit!!};
                    var transmisi = {!!$transmisi!!};
                    var distribusi = {!! $distribusi !!};
                    var pemanfaatan_tt = {!! $pemanfaatan_tt !!};
                    var pemanfaatan_tm = {!! $pemanfaatan_tm !!};

                    setData();
                    setLingkup();

                    $('#tipe_instalasi').change(function () {
                        setData();
                    });


                    $('#select_instalasi').change(function () {
                        setLingkup();
                    });

                    function setData() {
                        var select_instalasi = $('#select_instalasi');

                        //hapus select instalasi terlebih dahulu
                        select_instalasi.find('option').remove();

                        var selected = $("#tipe_instalasi option:selected").text();
                        tipeInstalasi = selected.toLowerCase();
                        var instalasi_id = {{(@$lhpp->instalasi_id != null)?@$lhpp->instalasi_id:'null'}};
                        isTrans = false;
                        if (selected == 'Pembangkit') {
                            //hide div lingkup
                            //console.log('asd');
                            $('#div_lingkup').hide();
                            $('#detail_kit').show();
                            //populate data instalasi pembangkit ke select box
                            $.each(pembangkit, function (key) {
                                select_instalasi.append('<option value=' + pembangkit[key]['id'] + '>' + pembangkit[key]['nama_instalasi'] + '</option>');
                            });
                        }
                        else if (selected == 'Transmisi') {
                            isTrans = true;
                            $('#detail_kit').hide();
                            //console.log(transmisi[14]['bay_gardu'][0]);
                            //populate data instalasi transmisi ke select box
                            $.each(transmisi, function (key) {
                                select_instalasi.append('<option data-lingkup=' + key + ' value=' + transmisi[key]['id'] + '>' + transmisi[key]['nama_instalasi'] + '</option>');
                            });
                        }
                        else if (selected == 'Distribusi') {
                            $('#div_lingkup').hide();
                            $('#detail_kit').hide();
                            //populate data instalasi transmisi ke select box
                            $.each(distribusi, function (key) {
                                select_instalasi.append('<option data-lingkup=' + key + ' value=' + distribusi[key]['id'] + '>' + distribusi[key]['nama_instalasi'] + '</option>');
                            });
                        }
                        else if (selected == 'Pemanfaatan TT') {
                            $('#div_lingkup').hide();
                            $('#detail_kit').hide();
                            //populate data instalasi transmisi ke select box
                            $.each(pemanfaatan_tt, function (key) {
                                select_instalasi.append('<option data-lingkup=' + key + ' value=' + pemanfaatan_tt[key]['id'] + '>' + pemanfaatan_tt[key]['nama_instalasi'] + '</option>');
                            });
                        }
                        else if (selected == 'Pemanfaatan TM') {
                            $('#div_lingkup').hide();
                            $('#detail_kit').hide();
                            //populate data instalasi transmisi ke select box
                            $.each(pemanfaatan_tm, function (key) {
                                select_instalasi.append('<option data-lingkup=' + key + ' value=' + pemanfaatan_tm[key]['id'] + '>' + pemanfaatan_tm[key]['nama_instalasi'] + '</option>');
                            });
                        }

                        if (instalasi_id != 'null') {
                            select_instalasi.val(instalasi_id);
                        }
                    }

                    function setLingkup() {
                        var instalasi_id = $('#select_instalasi').val();
                        var selected = $("#tipe_instalasi option:selected").val();
                        var url = '#';
                        if(selected == '{{ID_PEMBANGKIT}}'){
                            url = '{{url("/internal/instalasi_pembangkit/")}}/' + instalasi_id;
                        }else if(selected == '{{ID_TRANSMISI}}'){
                            url = '{{url("/internal/instalasi_transmisi/")}}/' + instalasi_id;
                        }else if(selected == '{{ID_DISTRIBUSI}}'){
                            url = '{{url("/internal/instalasi_distribusi/")}}/' + instalasi_id;
                        }else if(selected == '{{ID_PEMANFAATAN_TM}}'){
                            url = '{{url("/internal/instalasi_pemanfaatan_tm/")}}/' + instalasi_id;
                        }else if(selected == '{{ID_PEMANFAATAN_TT}}'){
                            url = '{{url("/internal/instalasi_pemanfaatan_tt/")}}/' + instalasi_id;
                        }
                        $("#ref_detail_instalasi").attr("href", url);
                        var bay_gardu_id = {{(@$lhpp->bay_gardu_id != null)?@$lhpp->bay_gardu_id:'null'}};

                        if (isTrans) {
                            var select_lingkup = $('#select_lingkup');
                            var key_transmisi = $('#select_instalasi').find(":selected").data("lingkup");
                            var transmisi = {!!$transmisi!!};
                            var lingkup = transmisi[key_transmisi]['bay_gardu'];

                            if (lingkup != null) {
                                $('#div_lingkup').show();
                                //hapus select lingkup terlebih dahulu
                                select_lingkup.find('option').remove();
                                //populate data lingkup transmisi ke select box
                                $.each(lingkup, function (key) {
                                    select_lingkup.append('<option value=' + lingkup[key]['id'] + '>' + lingkup[key]['nama_bay'] + '- ' + lingkup[key]['jenis_bay'] + '</option>');
                                });
                                if (bay_gardu_id != 'null') {
                                    select_lingkup.val(bay_gardu_id);
                                }
                            } else {
                                $('#div_lingkup').hide();
                            }
                        }
                    }

                    $('#ketua').change(function () {
                        var tmp = $('#ketua').find(":selected").data("ketua");
                        $('#nik_ketua').val(tmp.nik);
                    });

                    $('#anggota').change(function () {
                        var tmp = $('#anggota').find(":selected").data("anggota");
                        $('#nik_anggota').val(tmp.nik);
                    });

                    $('#pjt').change(function () {
                        var tmp = $('#pjt').find(":selected").data("pjt");
                        $('#nik_pjt').val(tmp.nik);
                    });

                });
            </script>
@endsection
