@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>LPI</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">LPI</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('files'=> true, 'class'=> 'form-horizontal')) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Laporan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" readonly name="nomor" placeholder="Nomor LPI"
                                               value="{{@$lpi->nomor}}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Laporan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text"
                                               name="tanggal_laporan"
                                               class="form-control" readonly
                                               disabled
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en"
                                               data-RTL="false"
                                               placeholder="Tanggal laporan"
                                               value="{{@$lpi->tanggal_laporan}}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="pekerjaan" type="text" class="form-control"
                                               value="{{@$lpi->pekerjaan->pekerjaan}}"
                                               placeholder="Pekerjaan" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" type="text"
                                               value="{{@$lpi->pekerjaan->permohonan->instalasi->nama_instalasi}} {{(@$lpi->bay != null) ? "(".@$lpi->bay->nama_bay.")" : ""}}"
                                               class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                    <div class="col-sm-1">
                                        <?php
                                        $url = '#';
                                        if ($lpi != null) {
                                            switch ($lpi->pekerjaan->permohonan->id_tipe_instalasi) {
                                                case ID_PEMBANGKIT:
                                                    $url = url("/internal/instalasi_pembangkit/" . $lpi->instalasi_id);
                                                    break;
                                                case ID_TRANSMISI:
                                                    $url = url("/internal/instalasi_transmisi/" . $lpi->instalasi_id);
                                                    break;
                                                case ID_DISTRIBUSI:
                                                    $url = url("/internal/instalasi_distribusi/" . $lpi->instalasi_id);
                                                    break;
                                                case ID_PEMANFAATAN_TM:
                                                    $url = url("/internal/instalasi_pemanfaatan_tm/" . $lpi->instalasi_id);
                                                    break;
                                                case ID_PEMANFAATAN_TT:
                                                    $url = url("/internal/instalasi_pemanfaatan_tt/" . $lpi->instalasi_id);
                                                    break;
                                            }
                                        }
                                        ?>
                                        <a href="{{($lpi == null) ? "#" : $url}}"
                                           target="_blank" id="detail_instalasi"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan"
                                               value="{{@$lpi->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly
                                               required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Hasil LPI</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if(@$lpi->file_lpi != null)
                                            <a target="_blank"
                                               href="{{url('upload/'.@$lpi->file_lpi)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center>
                                                        <i class="fa fa-download"></i>Preview File
                                                    </center>
                                                </button>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <textarea name="keterangan" rows="5" readonly
                                                  class="form-control">{{@$lpi->keterangan}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/lpi') }}"
                                       class="btn btn-warning btn-square btn-embossed">Kembali
                                        &nbsp;</a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

@endsection
