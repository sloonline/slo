@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Pending <strong>Item</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pending Item</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_pending_item', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="pekerjaan_id" class="form-control form-white" id="pekerjaan" data-search="true"required>
                                            <option>- Pilih Pekerjaan -</option>
                                            @foreach ($pekerjaan as $key => $item)
                                                <option
                                                value ="{{$item->id}}"
                                                lingkup ="{{$item->permohonan->lingkup_pekerjaan}}"
                                                instalasi = "{{$item->permohonan->instalasi}}"
                                                unit = "{{$item->permohonan->instalasi}}"
                                                hilo = "{{@$item->hilo}}"
                                                >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" type="text" class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan" type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penjelasan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <textarea class="form-control form-white" rows="4"
                                        name="penjelasan"
                                        placeholder="Penjelasan"
                                        required></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="status" class="form-control" required disabled>
                                            <option value="CREATED">Created</option>
                                            <option value="VERIFIED">Verified</option>
                                            <option value="COMPLETED">Completed</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <fieldset class="cart-summary">
                                    <legend><strong>LIST PENDING ITEM</strong></legend>
                                    <div id="div_pending_item"></div>
                                </fieldset> --}}
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/pending_item') }}"
                                    class="btn btn-warning btn-square btn-embossed">Batal
                                    &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                    class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i
                                    class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

    <script>
    $('#pekerjaan').change(function(){
        var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
        var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
        var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
        //
        $('input[name=instalasi]').val(instalasi.nama_instalasi);
        $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
        $('input[name=unit_pembangkit]').val(unit.nomor_pembangkit);

        // var hilo = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('hilo'));
        // generatePendingItem(hilo);
    });

    function generatePendingItem(hilo){
        // console.log(hilo);
        var html_pending_item = "";
        $('#div_pending_item').html("");
        for( key in hilo ) {
            console.log( "key is " + [ key ] + ", value is " + hilo[ key ]['mata_uji'] );
            html_pending_item += "<div class='form-group col-lg-12'>"+
                                "<div class='col-sm-3'></div>"+
                                "<div class='col-sm-3'>"+
                                "<label class='col-sm-12 control-label'>"+  hilo[ key ]['mata_uji'] +
                                "</div>"+
                                "<div class='col-sm-6'>"+
                                "<label class='radio-inline'><input type='checkbox' name='pending_item' value='"+ hilo[ key ]['mata_uji']+"' ></label>"+
                                "</div>"+
                                "</div>";
        }

        $('#div_pending_item').append(html_pending_item );
    }
    </script>

@endsection
