@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Pending <strong>Item</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pending Item</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pending_item->pekerjaan->pekerjaan}}" name="pekerjaan" type="text" class="form-control" placeholder="Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pending_item->pekerjaan->permohonan->instalasi->nama_instalasi}}" name="instalasi" type="text" class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pending_item->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}" name="lingkup_pekerjaan" type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penjelasan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" rows="4"
                                        name="penjelasan"
                                        placeholder="Penjelasan"
                                        required>{{$pending_item->penjelasan}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status</label>
                                    </div>
                                    {!! Form::open(array('url'=> 'internal/save_status_pending_item', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                    {!! Form::hidden('pending_item_id', $pending_item->id) !!}
                                    <div class="col-sm-2">
                                        <select name="status" class="form-control form-white" required id="status">
                                            <option value="CREATED" >Created</option>
                                            <option value="VERIFIED" >Verified</option>
                                            <option value="COMPLETED" >Completed</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                class="fa fa-save"
                                                data-rel="tooltip"
                                                data-placement="top" title="Update Status Pending Item"></i>
                                         Update Status
                                    </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tindak_lanjut" data-toggle="tab"><i class="icon-home"></i>
                                            Tindak Lanjut
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#history" data-toggle="tab"><i class="icon-user"></i>
                                            History Tindak Lanjut
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="tindak_lanjut">
                                        <div class="panel-content pagination2 table-responsive">
                                            {!! Form::open(array('url'=> 'internal/save_tindak_lanjut_pending_item', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                            {!! Form::hidden('pending_item_id', $pending_item->id) !!}
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nomor Berita Acara</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="no_ba" type="text" class="form-control form-white"
                                                           data-RTL="false" placeholder="Nomor Berita Acara">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Berita Acara</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input name="file_berita_acara" type="file" class="form-control form-white" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input name="tgl_berita_acara" type="text"
                                                    class="form-control b-datepicker form-white"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en"
                                                    data-RTL="false" placeholder="Tanggal BA" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Hasil Verifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="radio-inline"><input type="radio" name="hasil_verifikasi" value="1" >Memenuhi</label>
                                                    <label class="radio-inline"><input type="radio" name="hasil_verifikasi" value="0" >Tidak Memenuhi</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Verifikasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="tgl_verifikasi" type="text"
                                                    class="form-control b-datepicker form-white"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en"
                                                    data-RTL="false" placeholder="Tanggal Verifikasi" required>
                                                </div>
                                            </div>
                                            <div class="panel-footer clearfix bg-white">
                                                <div class="pull-right">
                                                    <a href="{{ url('internal/detail_pekerjaan_pending_item/'.$pending_item->Pekerjaan->id) }}"
                                                        class="btn btn-warning btn-square btn-embossed">Kembali
                                                        &nbsp;<i class="icon-ban"></i></a>
                                                        <button type="submit"
                                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                                        data-style="zoom-in">Simpan &nbsp;<i
                                                        class="glyphicon glyphicon-floppy-saved"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="history">
                                        <div class="panel-content pagination2 table-responsive">
                                            <table id="my_table" class="table table-hover table-dynamic">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor BA</th>
                                                        <th>Tanggal BA</th>
                                                        <th>Hasil Verifikasi</th>
                                                        <th>Tanggal Verifikasi</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;?>
                                                    @foreach ($ba_pending_item as $key => $item)
                                                        <tr>
                                                            <td>{{$no++}}</td>
                                                            <td>{{$item->no_ba}}</td>
                                                            <td>{{$item->tanggal_ba}}</td>
                                                            <td>{{($item->hasil_verifikasi==0)?'Tidak Memenuhi':'Memenuhi'}}</td>
                                                            <td>{{$item->tanggal_verifikasi}}</td>
                                                            <td>
                                                                <a href="{{url('upload/'.$item->file_ba_pending_item)}}"
                                                                    class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                    class="fa fa-download"
                                                                    data-rel="tooltip"
                                                                    data-placement="top" title="Download Berita Acara"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('page_script')
        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- Tables Filtering, Sorting & Editing -->
        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- Buttons Loading State -->
        <script>
        $(document).ready(function(){
            $("#status").val('{{$pending_item->status}}');
        });
        </script>
    @endsection
