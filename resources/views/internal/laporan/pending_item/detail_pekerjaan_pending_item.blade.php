@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Pending <strong>Item</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Pending Item</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_pending_item', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pekerjaan->pekerjaan}}" name="pekerjaan" type="text" class="form-control" placeholder="Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pekerjaan->permohonan->instalasi->nama_instalasi}}" name="instalasi" type="text" class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{$pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}" name="lingkup_pekerjaan" type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <fieldset class="cart-summary">
                                    <legend><strong>LIST PENDING ITEM</strong></legend>
                                    <table id="my_table" class="table table-hover table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Penjelasan Pending Item</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;?>
                                            @foreach ($pending_item as $key => $item)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{$item->penjelasan}}</td>
                                                    <td>{{$item->status}}</td>
                                                    <td>
                                                        <a href="{{url('internal/detail_pending_item/'.$item->id)}}"
                                                            class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                            class="fa fa-eye"
                                                            data-rel="tooltip"
                                                            data-placement="top" title="Detail Pending Item"></i>
                                                            <a href="{{url('internal/delete_pending_item/'.$item->id)}}"
                                                               onclick="return confirm('Anda yakin mengahapus Pending Item?')"
                                                               class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                                        class="fa fa-trash"
                                                                        data-rel="tooltip"
                                                                        data-placement="top" title="Delete Pending Item"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a onclick="return window.history.back()"
                                   class="btn btn-warning btn-square btn-embossed">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    @section('page_script')
        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- Tables Filtering, Sorting & Editing -->
        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- Buttons Loading State -->

        <script>

        $('#pekerjaan').change(function(){
            var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
            var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
            var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
            //
            $('input[name=instalasi]').val(instalasi.nama_instalasi);
            $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
            $('input[name=unit_pembangkit]').val(unit.nomor_pembangkit);
        });
        </script>
    @endsection
