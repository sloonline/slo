@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Rekomendasi <strong>Laik Sertifikasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">RLS</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Peminta Jasa</th>
                                    <th>Pekerjaan</th>
                                    <th>Nomor RLS</th>
                                    <th>Jenis Instalasi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 1;
                                ?>
                                @foreach ($rls as $key => $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{@$item->pekerjaan->permohonan->user->nama_user}}</td>
                                        <td>{{@$item->pekerjaan->pekerjaan}}</td>
                                        <td>{{@$item->nomor_rekomendasi}}</td>
                                        <td>{{@$item->pekerjaan->permohonan->instalasi->jenis_instalasi->jenis_instalasi}}</td>
                                        <td>
                                            <a href="{{url('internal/detail_rls/'.@$item->id)}}"
                                                class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                class="fa fa-eye"
                                                data-rel="tooltip"
                                                data-placement="right" title="Detail RLS"></i>
                                            </a>
                                            <a href="{{url('internal/create_rls/'.$item->id)}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                        class="fa fa-pencil"
                                                        data-rel="tooltip"
                                                        data-placement="right" title="Edit RLS"></i>
                                            </a>
                                            <a onclick="return confirm('Anda yakin menghapus RLS {{@$item->pekerjaan->pekerjaan}}?')"
                                               href="{{url('internal/delete_rls/'.$item->id)}}"
                                               class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                        class="fa fa-trash"
                                                        data-rel="tooltip"
                                                        data-placement="right" title="Delete RLS"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('page_script')
        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- Tables Filtering, Sorting & Editing -->
        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!-- Buttons Loading State -->

        <script>
        $(document).ready(function () {
            var oTable = $('#my_table').dataTable();

            // Sort immediately with columns 0 and 1
            oTable.fnSort([[2, 'desc']]);
        });
        </script>
    @endsection
