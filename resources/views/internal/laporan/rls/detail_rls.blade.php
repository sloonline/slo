@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
        $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Rekomendasi <strong>Laik Sertifikasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">RLS</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {{-- {!! Form::hidden('id',@$id) !!} --}}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->pekerjaan->pekerjaan}}" type="text" class="form-control  " placeholder="Nomor Rekomendasi" disabled>
                                    </div>
                                    <div class="col-sm-2">
                                        <a target="_blank" href="{{url('internal/detail_pekerjaan/'.@$rls->pekerjaan->id)}}" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Rekomendasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->nomor_rekomendasi}}" id="pekerjaan" name="no_rekomendasi" type="text" class="form-control  " placeholder="Nomor Rekomendasi" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Terbit</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->tanggal_terbit}}" name="tgl_terbit" type="text"
                                        class="form-control b-datepicker  "
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en"
                                        data-RTL="false" placeholder="Tanggal Terbit" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Peminta Jasa</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->pekerjaan->permohonan->user->nama_user}}" type="text" class="form-control  " disabled>
                                    </div>
                                    {{--<div class="col-sm-2">--}}
                                        {{--<a target="_blank" href="{{url('internal/view_user/'.@$rls->user->id)}}" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->pekerjaan->permohonan->instalasi->nama_instalasi}}" type="text" class="form-control  " disabled>
                                    </div>
                                    <div class="col-sm-2">
                                        <a id="detail_instalasi" href="{{($rls == null) ? "#" : url('/internal/create_permohonan/'.@$rls->pekerjaan->permohonan->id_orders."/".@$rls->pekerjaan->permohonan_id)}}"
                                           target="_blank" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Permohonan Peminta Jasa</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input value="{{@$rls->pekerjaan->permohonan->nomor_permohonan}}" type="text" class="form-control  " disabled>
                                    </div>
                                    <div class="col-sm-2">
                                        <input
                                        value="{{@$rls->pekerjaan->permohonan->tanggal_permohonan}}"
                                        name="tgl_surat_permohonan"
                                        type="text"
                                        class="form-control b-datepicker"
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en"
                                        data-RTL="false"
                                        placeholder="Tanggal" readonly disabled>
                                    </div>
                                    {{--<div class="col-sm-2">--}}
                                        {{--<a href="{{url('internal/create_permohonan/'.@$rls->pekerjaan->permohonan->order->id.'/'.@$rls->pekerjaan->permohonan->id)}}" target="_blank" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Permohonan Tim Inspeksi Teknik</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input value="{{@$rls->no_srt_tim_inspeksi}}" name="surat_permohonan_tim_inspeksi" type="text" class="form-control  " placeholder="Nomor Surat Permohonan Tim Inspeksi" disabled>
                                    </div>
                                    <div class="col-sm-2">
                                        <input value="{{@$rls->tgl_srt_tim_inspeksi}}" name="tgl_surat_permohonan_tim_inspeksi"
                                        type="text"
                                        class="form-control b-datepicker  "
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en"
                                        data-RTL="false"
                                        placeholder="Tanggal"disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Manajer BKI</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input value="{{@$rls->user->nama_user}}" type="text" class="form-control  " disabled>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File RLS</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="{{url('upload/'.@$rls->file_rls)}}" class="btn btn-primary"><i class="fa fa-download"></i> Download File RLS</a>
                                    </div>
                                </div>
                            </div>

                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/rls') }}"
                                    class="btn btn-warning btn-square btn-embossed">Kembali
                                    &nbsp;<i class="icon-ban"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

    <script>
        $('#pekerjaan').change(function(){
            var user = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('user'));
            var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
            var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
            // console.log(user);
            $('input[name=peminta_jasa]').val(user.nama_user);
            $('input[name=instalasi]').val(instalasi.nama_instalasi);
            $('input[name=surat_permohonan]').val(permohonan.nomor_permohonan);
            $('input[name=tgl_surat_permohonan]').val(permohonan.tanggal_permohonan);
        });
    </script>
@endsection
