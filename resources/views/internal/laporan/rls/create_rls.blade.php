@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
        $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Rekomendasi <strong>Laik Sertifikasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">RLS</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_rls', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                             {!! Form::hidden('id',@$id) !!}
                             {!! Form::hidden('permohonan_rls_id',@$permohonan_rls_id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($rls == null)
                                            <select data-search="true" name="pekerjaan_id" class="form-control form-white" id="pekerjaan" required>
                                                <option>- Pilih Pekerjaan -</option>
                                                @foreach ($pekerjaan as $key => $item)
                                                    <option
                                                            value="{{$item->id}}"
                                                            user="{{$item->permohonan->user}}"
                                                            instalasi = "{{$item->permohonan->instalasi}}"
                                                            permohonan = "{{$item->permohonan}}"
                                                    >{{$item->pekerjaan}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            {!! Form::hidden('pekerjaan_id',@$rls->pekerjaan_id) !!}
                                            <input name="pekerjaan" type="text" class="form-control"
                                                   value="{{@$rls->pekerjaan->pekerjaan}}" placeholder="Pekerjaan"
                                                   readonly>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <a id="detail_pekerjaan" target="_blank" {{($rls == null) ? '' : 'href='.url('internal/detail_pekerjaan/'.$rls->pekerjaan_id)}} class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Rekomendasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="pekerjaan" name="no_rekomendasi" value="{{@$rls->nomor_rekomendasi}}" type="text" class="form-control form-white" placeholder="Nomor Rekomendasi" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Terbit</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="tgl_terbit" type="text"
                                        class="form-control b-datepicker form-white"
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en" value="{{@$rls->tanggal_terbit}}"
                                        data-RTL="false" placeholder="Tanggal Terbit" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Peminta Jasa</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="peminta_jasa" value="{{@$rls->pekerjaan->permohonan->user->nama_user}}" type="text" class="form-control" placeholder="Peminta Jasa" readonly required>
                                    </div>
                                    {{--<div class="col-sm-2">--}}
                                        {{--<a class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" value="{{@$rls->pekerjaan->permohonan->instalasi->nama_instalasi}}" type="text" class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <a id="detail_instalasi" href="{{($rls == null) ? "#" : url('/internal/create_permohonan/'.@$rls->pekerjaan->permohonan->id_orders."/".@$rls->pekerjaan->permohonan_id)}}"
                                           target="_blank" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Permohonan Peminta Jasa</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input name="surat_permohonan" value="{{@$rls->pekerjaan->permohonan->nomor_permohonan}}" type="text" class="form-control" placeholder="Nomor Surat Permohonan" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_surat_permohonan"
                                        type="text"
                                        class="form-control b-datepicker"
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en"
                                        data-RTL="false" value="{{@$rls->pekerjaan->permohonan->tanggal_permohonan}}"
                                        placeholder="Tanggal" readonly required>
                                    </div>
                                    {{--<div class="col-sm-2">--}}
                                        {{--<a class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Permohonan Tim Inspeksi Teknik</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input name="surat_permohonan_tim_inspeksi" value="{{@$rls->no_srt_tim_inspeksi}}" type="text" class="form-control form-white" placeholder="Nomor Surat Permohonan Tim Inspeksi" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_surat_permohonan_tim_inspeksi"
                                        type="text"
                                        class="form-control b-datepicker form-white"
                                        data-date-format="dd-mm-yyyy"
                                        data-lang="en" value="{{@$rls->tgl_srt_tim_inspeksi}}"
                                        data-RTL="false"
                                        placeholder="Tanggal"required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Manajer BKI</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select data-search="true" name="manajer_bki" class="form-control form-white" required>
                                            <option>- Pilih Manajer -</option>
                                            @foreach ($mb_bki as $key => $item)
                                                <option {{(@$rls->manager_bki == $item->id) ? "selected='selected'" : ""}} value="{{$item->id}}">{{$item->nama_user}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File RLS</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="file_rls" type="file" class="form-control form-white" placeholder="File RLS">
                                    </div>
                                </div>
                                @if(@$rls->file_rls != null)
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$rls->file_rls)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center>
                                                        <i class="fa fa-download"></i>Preview File
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @if($id > 0)
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label"></label>
                                        </div>
                                        <div class="col-sm-6">
                                            <a id="file_template"
                                               href="{{url('internal/print_rls/'.@$rls->id)}}">
                                                <button type="button" class="col-lg-12 btn btn-success">
                                                    <center>
                                                        <i class="fa fa-print"></i> Cetak  RLS
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if($rls == null)
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/rls') }}"
                                    class="btn btn-warning btn-square btn-embossed">Batal
                                    &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                    class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i
                                    class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @else
                </div>
                {!! Form::close() !!}
                    @include('workflow_view')
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

    <script>
        $('#pekerjaan').change(function(){
            var user = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('user'));
            var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
            var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
            var pekerjaan = jQuery.parseJSON($('#pekerjaan').find('option:selected').val());
            // console.log(user);
            $('input[name=peminta_jasa]').val(user.nama_user);
            $('input[name=instalasi]').val(instalasi.nama_instalasi);
            $('input[name=surat_permohonan]').val(permohonan.nomor_permohonan);
            $('input[name=tgl_surat_permohonan]').val(permohonan.tanggal_permohonan);

            if (pekerjaan> 0) {
                $("#detail_pekerjaan").attr("href", "{{url('/internal/detail_pekerjaan/')}}/" + pekerjaan);
            } else {
                $("#detail_pekerjaan").attr("href", "#");
            }

            if (permohonan.id > 0) {
                $("#detail_instalasi").attr("href", "{{url('/internal/create_permohonan/')}}/" + permohonan.id_orders + "/" + permohonan.id);
            } else {
                $("#detail_instalasi").attr("href", "#");
            }
        });
    </script>
@endsection
