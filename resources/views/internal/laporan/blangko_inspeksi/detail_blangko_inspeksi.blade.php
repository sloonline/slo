@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Blangko <strong>Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Blangko Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Form::hidden('pekerjaan_id',@$blangko->pekerjaan_id) !!}
                                        <input readonly name="pekerjaan" type="text" class="form-control" value="{{@$blangko->pekerjaan->pekerjaan}}" placeholder="Pekerjaan" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" type="text" value="{{@$blangko->pekerjaan->permohonan->instalasi->nama_instalasi}}" class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan" value="{{@$blangko->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}" type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Unit Pembangkit</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="unit_pembangkit" value="{{@$blangko->pekerjaan->permohonan->instalasi->nomor_pembangkit}}" type="text" class="form-control" placeholder="Unit Pembangkit" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Pemeriksaan</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_mulai" type="text"
                                               class="form-control b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$blangko->tanggal_mulai_pemeriksaan}}"
                                               data-RTL="false" placeholder="Tanggal Mulai" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_selesai" type="text"
                                               class="form-control b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$blangko->tanggal_selesai_pemeriksaan}}"
                                               data-RTL="false" placeholder="Tanggal Selesai" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Blangko Inspeksi</label>
                                    </div>
                                    @if(@$blangko->file_blangko_inspeksi != null)
                                        <div class="col-md-3">
                                            <a target="_blank"
                                            href="{{url('upload/'.@$blangko->file_blangko_inspeksi)}}">
                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                <center><iclass="fa fa-download"></i>Preview File</center>
                                            </button>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a href="{{ url('internal/blangko_inspeksi') }}"
                                class="btn btn-warning btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-ban"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

@endsection
