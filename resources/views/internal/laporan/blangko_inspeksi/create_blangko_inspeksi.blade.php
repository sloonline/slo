@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Blangko <strong>Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Blangko Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_blangko_inspeksi', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($blangko == null)
                                            <select name="pekerjaan_id" class="form-control form-white" id="pekerjaan" data-search="true"
                                                    required>
                                                <option>- Pilih Pekerjaan -</option>
                                                @foreach ($pekerjaan as $key => $item)
                                                    <option
                                                            value="{{$item->id}}"
                                                            lingkup="{{$item->permohonan->lingkup_pekerjaan}}"
                                                            instalasi="{{$item->permohonan->instalasi}}"
                                                            permohonan="{{$item->permohonan}}"
                                                            unit="{{$item->permohonan->instalasi}}"
                                                            template="{{@$item->permohonan->lingkup_pekerjaan->template}}"
                                                    >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                                @endforeach
                                            </select>
                                        @else
                                            {!! Form::hidden('pekerjaan_id',@$blangko->pekerjaan_id) !!}
                                            <input name="pekerjaan" type="text" class="form-control"
                                                   value="{{@$blangko->pekerjaan->pekerjaan}}" placeholder="Pekerjaan"
                                                   readonly>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" type="text"
                                               value="{{@$blangko->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan"
                                               value="{{@$blangko->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly
                                               required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Unit Pembangkit</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="unit_pembangkit"
                                               value="{{@$blangko->pekerjaan->permohonan->instalasi->nomor_pembangkit}}"
                                               type="text" class="form-control" placeholder="Unit Pembangkit" readonly
                                               required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Pemeriksaan</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_mulai" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$blangko->tanggal_mulai_pemeriksaan}}"
                                               data-RTL="false" placeholder="Tanggal Mulai" required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_selesai" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$blangko->tanggal_selesai_pemeriksaan}}"
                                               data-RTL="false" placeholder="Tanggal Selesai" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Template</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <a id="file_template"
                                           href="{{($blangko == null) ? "#" : \Illuminate\Support\Facades\URL::asset('template_word/blangko_inspeksi/'.@$file_template->template_blangko_inspeksi)}}">
                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                <center>
                                                    <i class="fa fa-download"></i>Download File
                                                </center>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Blangko Inspeksi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="file_blangko_inspeksi" type="file" class="form-control form-white"
                                               placeholder="File RLB">
                                    </div>
                                </div>
                                @if(@$blangko->file_blangko_inspeksi != null)
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$blangko->file_blangko_inspeksi)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center>
                                                        <i class="fa fa-download"></i>Preview File
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/blangko_inspeksi') }}"
                                       class="btn btn-warning btn-square btn-embossed">Batal
                                        &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                            data-style="zoom-in">Simpan &nbsp;<i
                                                class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $('#pekerjaan').change(function () {
                    var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
                    var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
                    var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
                    var template = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('template'));
                    var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
                    var produk_id = permohonan.id_produk;
                    var file_template = "";
                    template.forEach(function(row){
                        if(row.id_produk == produk_id){
                            file_template = (row.template_blangko_inspeksi == null) ? "" : row.template_blangko_inspeksi;
                        }
                    });
                    //
                    $('input[name=instalasi]').val(instalasi.nama_instalasi);
                    $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
                    $('input[name=unit_pembangkit]').val(unit.nomor_pembangkit);

                    if (lingkup.id > 0) {
                        $("#file_template").attr("href", "{{\Illuminate\Support\Facades\URL::asset('template_word/blangko_inspeksi/')}}/" + file_template);
                    } else {
                        $("#detail_instalasi").attr("href", "#");
                    }
                });
            </script>
@endsection
