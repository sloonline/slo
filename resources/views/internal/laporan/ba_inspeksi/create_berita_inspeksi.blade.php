@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Berita Acara <strong>Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Berita Acara Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_berita_inspeksi', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($ba_inspeksi == null)
                                            <select name="pekerjaan_id" class="form-control form-white" id="pekerjaan" data-search="true"
                                                    required>
                                                <option>- Pilih Pekerjaan -</option>
                                                @foreach ($pekerjaan as $key => $item)
                                                    <option
                                                            value="{{$item->id}}"
                                                            lingkup="{{$item->permohonan->lingkup_pekerjaan}}"
                                                            instalasi="{{$item->permohonan->instalasi}}"
                                                            unit="{{$item->permohonan->instalasi}}"
                                                            permohonan = "{{$item->permohonan}}"
                                                            laporan_pekerjaan = "{{$item->laporan_pekerjaan}}"
                                                    >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                                @endforeach
                                            </select>
                                        @else
                                            {!! Form::hidden('pekerjaan_id',@$ba_inspeksi->pekerjaan_id) !!}
                                            <input name="pekerjaan" type="text" class="form-control"
                                                   value="{{@$ba_inspeksi->pekerjaan->pekerjaan}}"
                                                   placeholder="Pekerjaan" readonly>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input name="instalasi" type="text"
                                               value="{{@$ba_inspeksi->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               class="form-control" placeholder="Instalasi" readonly required>
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{($ba_inspeksi == null) ? "#" : url('/internal/create_permohonan/'.@$ba_inspeksi->pekerjaan->permohonan->id_orders."/".@$ba_inspeksi->pekerjaan->permohonan_id)}}"
                                           target="_blank" id="detail_instalasi"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="lingkup_pekerjaan_id" name="lingkup_pekerjaan_id" type="hidden" value="{{@$ba_inspeksi->lingkup_pekerjaan_id}}" />
                                        <input name="lingkup_pekerjaan"
                                               value="{{@$ba_inspeksi->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly
                                               required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12" style="display: none;">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Hasil Pemeriksaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Item Pemeriksaan</th>
                                                <th>Pelaksanaan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1; ?>
                                                @foreach($item_arr as $hasil)
                                                    <tr>
                                                        <td>{{$no++}}</td>
                                                        <td>{{@$hasil[0]}}</td>
                                                        <td><span id="{{@$hasil[1]}}">{{(@$hasil[2] == 1) ? 'Sesuai' : 'Tidak Sesuai'}}</span></td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input name="tanggal" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$ba_inspeksi->tanggal}}"
                                               data-RTL="false" placeholder="Tanggal" required>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">Hari</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="hari" type="text"
                                               value="{{@$ba_inspeksi->hari}}"
                                               class="form-control form-white" placeholder="Hari" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tahun Terbilang (contoh : Dua Ribu Tujuh Belas)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="tahun_terbilang" type="text"
                                               value="{{@$ba_inspeksi->tahun_terbilang}}"
                                               class="form-control form-white" placeholder="(contoh : Dua Ribu Tujuh Belas)" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Dokumen Pendukung (nama - nama dokumen yang dilampirkan, pisahkan dengan tanda koma (,) )</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="dokumen_pendukung" type="text"
                                               value="{{@$ba_inspeksi->dokumen_pendukung}}"
                                               class="form-control form-white" placeholder="(contoh : Notulen kickoff meeting, Hasil inspeksi laik operasi )" required>
                                    </div>
                                </div>
                                @if($ba_inspeksi != null)
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Berita Acara</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="file_berita_acara" type="file" class="form-control form-white"
                                               placeholder="File Berita Acara"}}>
                                    </div>
                                </div>
                                @if(@$ba_inspeksi->file_berita_acara != null)
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$ba_inspeksi->file_berita_acara)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center>
                                                        <i class ="fa fa-download"></i>Preview File
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @endif
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/berita_inspeksi') }}"
                                       class="btn btn-warning btn-square btn-embossed">Batal
                                        &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                            data-style="zoom-in">Simpan &nbsp;<i
                                                class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $('#pekerjaan').change(function () {
                    var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
                    var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
                    var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
                    var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
                    var laporan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('laporan_pekerjaan'));
                    //
                    $('input[name=instalasi]').val(instalasi.nama_instalasi);
                    $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
                    $('input[name=unit_pembangkit]').val(unit.nomor_pembangkit);
                    $('#lingkup_pekerjaan_id').val(lingkup.id);


                    if (permohonan.id > 0) {
                        $("#detail_instalasi").attr("href", "{{url('/internal/create_permohonan/')}}/" + permohonan.id_orders + "/" + permohonan.id);
                    } else {
                        $("#detail_instalasi").attr("href", "#");
                    }

                    if (lingkup.id > 0) {
                        $("#file_template").attr("href", "{{\Illuminate\Support\Facades\URL::asset('template_word/data_teknik/')}}/" + lingkup.template_data_teknik);
                    } else {
                        $("#detail_instalasi").attr("href", "#");
                    }
                    if (laporan != undefined){
                        $('#dokumen').html((laporan.pemeriksaan_design == 1) ? 'Sesuai' : 'Tidak Sesuai');
                        $('#design').html((laporan.pemeriksaan_design == 1) ? 'Sesuai' : 'Tidak Sesuai');
                        $('#visual').html((laporan.pemeriksaan_visual == 1) ? 'Sesuai' : 'Tidak Sesuai');
                        $('#komisioning').html((laporan.evaluasi_hasil_komisioning == 1) ? 'Sesuai' : 'Tidak Sesuai');
                        $('#uji_tegang').html((laporan.pengujian_sistem == 1) ? 'Sesuai' : 'Tidak Sesuai');
                        $('#periksa_dampak').html((laporan.dampak_lingkungan == 1) ? 'Sesuai' : 'Tidak Sesuai');
                    }else{
                        $('#dokumen').html('');
                        $('#design').html('');
                        $('#visual').html('');
                        $('#komisioning').html('');
                        $('#uji_tegang').html('');
                        $('#periksa_dampak').html('');
                    }
                });
            </script>
@endsection
