@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Berita Acara <strong>Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Berita Acara Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                            {!! Form::hidden('pekerjaan_id',@$ba_inspeksi->pekerjaan_id) !!}
                                            <input name="pekerjaan" type="text" class="form-control"
                                                   value="{{@$ba_inspeksi->pekerjaan->pekerjaan}}"
                                                   placeholder="Pekerjaan" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input name="instalasi" type="text"
                                               value="{{@$ba_inspeksi->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               class="form-control" placeholder="Instalasi" readonly>
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{($ba_inspeksi == null) ? "#" : url('/internal/create_permohonan/'.@$ba_inspeksi->pekerjaan->permohonan->id_orders."/".@$ba_inspeksi->pekerjaan->permohonan_id)}}"
                                           target="_blank" id="detail_instalasi"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input id="lingkup_pekerjaan_id" name="lingkup_pekerjaan_id" type="hidden" value="{{@$ba_inspeksi->lingkup_pekerjaan_id}}" />
                                        <input name="lingkup_pekerjaan"
                                               value="{{@$ba_inspeksi->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control" placeholder="Lingkup Pekerjaan" readonly
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Hasil Pemeriksaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Item Pemeriksaan</th>
                                                <th>Pelaksanaan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; ?>
                                            @foreach($item_arr as $hasil)
                                                <tr>
                                                    <td>{{$no++}}</td>
                                                    <td>{{@$hasil[0]}}</td>
                                                    <td><span id="{{@$hasil[1]}}">{{(@$hasil[2] == 1) ? 'Sesuai' : 'Tidak Sesuai'}}</span></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input name="tanggal" type="text"
                                               class="form-control b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" value="{{@$ba_inspeksi->tanggal}}"
                                               data-RTL="false" placeholder="Tanggal" readonly>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">Hari</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="hari" type="text"
                                               value="{{@$ba_inspeksi->hari}}"
                                               class="form-control" placeholder="Hari" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tahun Terbilang (contoh : Dua Ribu Tujuh Belas)</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="tahun_terbilang" type="text"
                                               value="{{@$ba_inspeksi->tahun_terbilang}}"
                                               class="form-control" placeholder="(contoh : Dua Ribu Tujuh Belas)" readonly>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Dokumen Pendukung (nama - nama dokumen yang dilampirkan, pisahkan dengan tanda koma (,) )</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="dokumen_pendukung" type="text"
                                               value="{{@$ba_inspeksi->dokumen_pendukung}}"
                                               class="form-control" placeholder="(contoh : Notulen kickoff meeting, Hasil inspeksi laik operasi )" readonly>
                                    </div>
                                </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File Berita Acara</label>
                                        </div>
                                        <div class="col-sm-3">
                                            @if(@$ba_inspeksi->file_berita_acara != null)
                                                <a target="_blank"
                                                   href="{{url('upload/'.@$ba_inspeksi->file_berita_acara)}}">
                                                    <button type="button" class="col-lg-12 btn btn-primary">
                                                        <center>
                                                            <i class ="fa fa-download"></i>Preview File
                                                        </center>
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/berita_inspeksi') }}"
                                       class="btn btn-warning btn-square btn-embossed">Kembali
                                        &nbsp;<i class="icon-ban"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

@endsection
