@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Berita Acara <strong>Inspeksi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Berita Acara Inspeksi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('internal/create_berita_inspeksi')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Berita Acara Baru</a>
                            </div>
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Pekerjaan</th>
                                <th>Lingkup Pekerjaan</th>
                                <th>Instalasi</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @foreach ($ba_inspeksi as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{date('d M Y',strtotime(@$item->tanggal))}}</td>
                                    <td>{{@$item->pekerjaan->pekerjaan}}</td>
                                    <td>{{@$item->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}</td>
                                    <td>{{@$item->pekerjaan->permohonan->instalasi->nama_instalasi}}</td>
                                    <td style="white-space: nowrap;">
                                        <a href="{{url('internal/detail_berita_inspeksi/'.$item->id)}}"
                                           class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                    class="fa fa-eye"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Detail Berita Acara"></i>
                                        </a>
                                        <a href="{{url('internal/create_berita_inspeksi/'.$item->id)}}"
                                           class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                    class="fa fa-pencil"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Edit Berita Acara"></i>
                                        </a>
                                        <a onclick="return confirm('Anda yakin menghapus berita acara inspeksi {{@$item->pekerjaan->pekerjaan}} ?')"
                                           href="{{url('internal/delete_berita_inspeksi/'.$item->id)}}"
                                           class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                    class="fa fa-trash"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Delete Berita Acara"></i>
                                        </a>
                                        <a href="{{url('internal/print_berita_inspeksi/'.$item->id)}}"
                                           class="btn btn-sm btn-success btn-square btn-embossed"><i
                                                    class="fa fa-print"
                                                    data-rel="tooltip"
                                                    data-placement="left" title="Print Berita Acara"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

@endsection
