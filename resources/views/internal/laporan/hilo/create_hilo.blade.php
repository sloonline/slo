@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Hasil Inspeksi<strong> Laik Operasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">HILO</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_hilo', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <input type="hidden" id="active_lingkup" value="{{@$hilo->pekerjaan->permohonan->lingkup_pekerjaan->id}}"/>
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    <li id="tab_hasil_inspeksi">
                                        <a href="#hasil_inspeksi" id="menu_hasil" data-toggle="tab"><i class="fa fa-file-text-o"></i>
                                            Hasil Inspeksi
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    @if($hilo == null)
                                                        <select name="pekerjaan_id" class="form-control form-white" id="pekerjaan" data-search="true" required>
                                                            <option>- Pilih Pekerjaan -</option>
                                                            @foreach ($pekerjaan as $key => $item)
                                                                <option
                                                                value ="{{$item->id}}"
                                                                lingkup ="{{$item->permohonan->lingkup_pekerjaan}}"
                                                                instalasi = "{{$item->permohonan->instalasi}}"
                                                                unit = "{{$item->permohonan->instalasi}}"
                                                                >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        {!! Form::hidden('pekerjaan_id',@$hilo->pekerjaan_id) !!}
                                                        <input name="pekerjaan" type="text" class="form-control" value="{{@$hilo->pekerjaan->pekerjaan}}" placeholder="Pekerjaan" readonly>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Instalasi</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="instalasi" type="text" class="form-control" value="{{@$hilo->pekerjaan->permohonan->instalasi->nama_instalasi}}" placeholder="Instalasi" readonly required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="lingkup_pekerjaan" type="text" value="{{@$hilo->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}" class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Pemeriksaaan</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input name="tanggal_mulai" type="text"
                                                    placeholder="tgl mulai"
                                                    class="form-control b-datepicker form-white"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en" value="{{(@$hilo->tanggal_mulai_pemeriksaan != null) ? date('d-m-Y',strtotime(@$hilo->tanggal_mulai_pemeriksaan)) : ""}}"
                                                    data-RTL="false" placeholder="Tanggal" required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input name="tanggal_selesai" type="text"
                                                    placeholder="tgl selesai"
                                                    class="form-control b-datepicker form-white"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en" value="{{(@$hilo->tanggal_selesai_pemeriksaan != null) ? date('d-m-Y',strtotime(@$hilo->tanggal_selesai_pemeriksaan)) : ""}}"
                                                    data-RTL="false" placeholder="Tanggal" required>
                                                </div>
                                            </div>
                                            @if(@$hilo != null)
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File HILO</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input name="file_hilo" type="file" class="form-control form-white"
                                                           placeholder="File HILO">
                                                </div>
                                            </div>
                                            @if(@$hilo->file_hilo != null)
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$hilo->file_hilo)}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center>
                                                                    <i class="fa fa-download"></i>Preview File
                                                                </center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            @endif
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <div class="col-sm-12 prepend-icon">
                                                        <button type="button" onclick="loadFormHasilInspeksi()"
                                                        class="col-lg-12 btn btn-warning">
                                                        <center>
                                                            <i class="fa fa-pencil"></i>Isi Hasil Inspeksi
                                                        </center>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="hasil_inspeksi">
                                    <div class="panel-content pagination2 table-responsive" id="form_generate">
                                        @if($hasil_hilo != null)
                                            <?php
                                            $mata_uji = $hasil_hilo->where('is_dokumen','0');
                                            $dokumen = $hasil_hilo->where('is_dokumen','1');
                                            ?>
                                            <h2>FORM HASIL INSPEKSI</h2><hr/>
                                            <div>
                                                @foreach ($hasil_form_hilo as $item)
                                                    <div class="row">
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label">{{@$item->nama_field}}</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input name="form_{{@$item->id}}" type="text" class="form-control form-white" value="{{@$item->hasil}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div><br/><hr/>
                                            @if($dokumen->count() > 0)
                                                <h2><strong>A.	Mata Uji Laik Operasi</strong></h2><br/>
                                            @endif
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th style="width:70px;text-align:center;">No.</th>
                                                        <th style="width:450px;text-align:center;">Mata Uji</th>
                                                        <th style="width:150px;text-align:center;">Kriteria Penilaian SLO</th>
                                                        <th style="text-align:center;">Kesesuaian</th>
                                                        <th style="text-align:center;">Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($mata_uji as $item)
                                                        @if(@$item->is_parent == "1")
                                                            <tr style='font-weight:bold;font-size:16px;background-color:#319db5;' >
                                                                <td>{{@$item->nomor}}</td>
                                                                <td colspan='4'>{{@$item->mata_uji}}</td>
                                                            </tr>
                                                        @elseif(@$item->is_parent == "2")
                                                            <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;' >
                                                                <td>{{@$item->nomor}}</td>
                                                                <td colspan='4'>{{@$item->mata_uji}}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{@$item->nomor}}</td>
                                                                <td>{{@$item->mata_uji}}</td>
                                                                <td style="text-align:center;">{{@$item->kriteria}}</td>
                                                                <td style="text-align:center;">
                                                                    <input type="radio" {{(@$item->hasil_kesesuaian == "1") ? "checked" : ""}} value="1" name="kesesuaian_{{@$item->id}}"> Ya &nbsp;
                                                                    <input type="radio" {{(@$item->hasil_kesesuaian == "0") ? "checked" : ""}} value="0" name="kesesuaian_{{@$item->id}}"> Tidak
                                                                </td>
                                                                <td><textarea cols="12" rows="2" class="form-control form-white" placeholder="Keterangan.." name="ket_{{@$item->id}}">{{@$item->hasil_keterangan}}</textarea></td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @if($dokumen->count() > 0)
                                                <h2><strong>B.	Persyaratan Kelengkapan Registrasi dan Penerbitan SLO</strong></h2><br/>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:70px;text-align:center;">Nomor</th>
                                                            <th style="width:450px;text-align:center;">Dokumen</th>
                                                            <th style="text-align:center;">Ketersediaan</th>
                                                            <th style="text-align:center;">Keterangan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($dokumen as $item)
                                                            @if(@$item->is_parent == "1")
                                                                <tr style='font-weight:bold;font-size:16px;background-color:#319db5;' >
                                                                    <td>{{@$item->nomor}}</td>
                                                                    <td colspan='4'>{{@$item->mata_uji}}</td>
                                                                </tr>
                                                            @elseif(@$item->is_parent == "2")
                                                                <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;' >
                                                                    <td>{{@$item->nomor}}</td>
                                                                    <td colspan='4'>{{@$item->mata_uji}}</td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{@$item->nomor}}</td>
                                                                    <td>{{@$item->mata_uji}}</td>
                                                                    <td style="text-align:center;">
                                                                        <input type="radio" {{(@$item->hasil_kesesuaian == "1") ? "checked" : ""}} value="1" name="dokumen_kesesuaian_{{@$item->id}}"> Ya &nbsp;
                                                                        <input type="radio" {{(@$item->hasil_kesesuaian == "0") ? "checked" : ""}} value="0" name="dokumen_kesesuaian_{{@$item->id}}"> Tidak
                                                                    </td>
                                                                    <td><textarea cols="12" rows="2" class="form-control form-white" placeholder="Keterangan.." name="dokumen_ket_{{@$item->id}}">{{@$item->hasil_keterangan}}</textarea></td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a href="{{ url('internal/hilo') }}"
                                class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit"
                                class="btn btn-success ladda-button btn-square btn-embossed"
                                data-style="zoom-in">Simpan &nbsp;<i
                                class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

    <script>
    @if(@$hilo == null)
    $("#tab_hasil_inspeksi").hide();
    @endif
    function activaTab(tab){
        $("#tab_hasil_inspeksi").show();
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
    function loadFormHasilInspeksi(){
        @if(@$hilo != null)
        activaTab("hasil_inspeksi");
        @else
        var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
        if($("#active_lingkup").val() != lingkup.id){
            $('#ajax_loader').show();
            $('#form_generate').load("/internal/generate_form_hilo/" + lingkup.id, function() {
                activaTab("hasil_inspeksi");
                $('#ajax_loader').hide();
                $("#active_lingkup").val(lingkup.id);
            });
        }else{
            activaTab("hasil_inspeksi");
        }
        @endif
    }
    $('#pekerjaan').change(function(){
        var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
        var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
        var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
        //
        $('input[name=instalasi]').val(instalasi.nama_instalasi);
        $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
        $("#tab_hasil_inspeksi").hide();
    });
    </script>
@endsection
