<?php
$mata_uji = $struktur->where('is_dokumen','0');
$dokumen = $struktur->where('is_dokumen','1');
?>
<h2>FORM HASIL INSPEKSI</h2><hr/>
<div>
@foreach ($form as $item)
  <div class="row">
    <div class="form-group col-lg-12">
      <div class="col-sm-3">
        <label class="col-sm-12 control-label">{{@$item->nama_field}}</label>
      </div>
      <div class="col-sm-6">
          <input name="form_{{@$item->id}}" type="text" class="form-control form-white">
      </div>
    </div>
  </div>
@endforeach
</div><br/><hr/>
@if($dokumen->count() > 0)
<h2><strong>A.	Mata Uji Laik Operasi</strong></h2><br/>
@endif
<table class="table table-bordered">
  <thead>
    <tr>
      <th style="width:70px;text-align:center;">No.</th>
      <th style="width:450px;text-align:center;">Mata Uji</th>
      <th style="width:150px;text-align:center;">Kriteria Penilaian SLO</th>
      <th style="text-align:center;">Kesesuaian</th>
      <th style="text-align:center;">Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach($mata_uji as $item)
      @if(@$item->is_parent == "1")
        <tr style='font-weight:bold;font-size:16px;background-color:#319db5;' >
          <td>{{@$item->nomor}}</td>
          <td colspan='4'>{{@$item->mata_uji}}</td>
        </tr>
      @elseif(@$item->is_parent == "2")
        <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;' >
          <td>{{@$item->nomor}}</td>
          <td colspan='4'>{{@$item->mata_uji}}</td>
        </tr>
      @else
        <tr>
          <td>{{@$item->nomor}}</td>
          <td>{{@$item->mata_uji}}</td>
          <td style="text-align:center;">{{@$item->kriteria}}</td>
          <td style="text-align:center;">
            <input type="radio" value="1" name="kesesuaian_{{@$item->id}}"> Ya &nbsp;
            <input type="radio" value="0" name="kesesuaian_{{@$item->id}}"> Tidak
          </td>
          <td><textarea cols="12" rows="2" class="form-control form-white" placeholder="Keterangan.." name="ket_{{@$item->id}}"></textarea></td>
        </tr>
      @endif
    @endforeach
  </tbody>
</table>
@if($dokumen->count() > 0)
<h2><strong>B.	Persyaratan Kelengkapan Registrasi dan Penerbitan SLO</strong></h2><br/>
<table class="table table-bordered">
  <thead>
    <tr>
      <th style="width:70px;text-align:center;">Nomor</th>
      <th style="width:450px;text-align:center;">Dokumen</th>
      <th style="text-align:center;">Ketersediaan</th>
      <th style="text-align:center;">Keterangan</th>
    </tr>
  </thead>
  <tbody>
    @foreach($dokumen as $item)
      @if(@$item->is_parent == "1")
        <tr style='font-weight:bold;font-size:16px;background-color:#319db5;' >
          <td>{{@$item->nomor}}</td>
          <td colspan='4'>{{@$item->mata_uji}}</td>
        </tr>
      @elseif(@$item->is_parent == "2")
        <tr style='font-weight:bold;font-size:14px;background-color:#eaecef;' >
          <td>{{@$item->nomor}}</td>
          <td colspan='4'>{{@$item->mata_uji}}</td>
        </tr>
      @else
        <tr>
          <td>{{@$item->nomor}}</td>
          <td>{{@$item->mata_uji}}</td>
          <td style="text-align:center;">
            <input type="radio" value="1" name="dokumen_kesesuaian_{{@$item->id}}"> Ya &nbsp;
            <input type="radio" value="0" name="dokumen_kesesuaian_{{@$item->id}}"> Tidak
          </td>
          <td><textarea cols="12" rows="2" class="form-control form-white" placeholder="Keterangan.." name="dokumen_ket_{{@$item->id}}"></textarea></td>
        </tr>
      @endif
    @endforeach
  </tbody>
</table>
@endif
