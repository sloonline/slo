@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <script src="{{ url('/') }}/assets/global/plugins/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({
            selector: '#agenda',
            theme: 'modern',
            // encoding: 'xml',
            // entity_encoding : "raw",
            plugins: [
                'searchreplace wordcount visualblocks visualchars code',
            ],
            toolbar1: 'undo redo | bold italic',
            image_advtab: true
        });</script>
@endsection

@section('content')
    <?php
    $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Kick Off<strong> Meeting</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Kick Off Meeting</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_kick_off', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($kick_off == null)
                                            <select name="pekerjaan_id" data-search="true" class="form-control form-white" id="pekerjaan"
                                                    required>
                                                <option>- Pilih Pekerjaan -</option>
                                                @foreach ($pekerjaan as $key => $item)
                                                    <option
                                                            value="{{$item->id}}"
                                                            lingkup="{{$item->permohonan->lingkup_pekerjaan}}"
                                                            instalasi="{{$item->permohonan->instalasi}}"
                                                            unit="{{$item->permohonan->instalasi}}"
                                                    >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                                @endforeach
                                            </select>
                                        @else
                                            {!! Form::hidden('pekerjaan_id',@$kick_off->pekerjaan_id) !!}
                                            <input name="pekerjaan" type="text" class="form-control"
                                                   value="{{@$kick_off->pekerjaan->pekerjaan}}" placeholder="Pekerjaan"
                                                   readonly>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="instalasi" type="text" class="form-control"
                                               value="{{@$kick_off->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               placeholder="Instalasi" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup_pekerjaan" type="text"
                                               value="{{@$kick_off->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               class="form-control" placeholder="Lingkup Pekerjaan" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input name="tanggal" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en"
                                               value="{{($kick_off != null) ? date('d-m-Y',strtotime(@$kick_off->tanggal)) : ''}}"
                                               data-RTL="false" placeholder="Tanggal" required>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">Waktu</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="waktu" value="{{@$kick_off->waktu}}" required
                                               class="form-control form-white input-sm" type="time">
                                        {{-- <input name="waktu" type="time" value="{{@$kick_off->waktu}}" class="form-control form-white" required> --}}
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Notulis</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="notulis" type="text" value="{{@$kick_off->notulis}}"
                                               class="form-control form-white" placeholder="Nama Notulis" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Rapat Antara</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <textarea name="rapat_antara" type="text" class="form-control form-white"
                                                  placeholder="Peserta rapat"
                                                  required>{{@$kick_off->rapat_antara}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lokasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lokasi" type="text" class="form-control form-white"
                                               value="{{@$kick_off->lokasi}}" placeholder="Lokasi Meeting" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Agenda</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea rows="20" name="agenda" id="agenda" type="text"
                                                  class="form-control form-white"
                                                  placeholder="Agenda">{{@$kick_off->agenda}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Kick Off</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="file_kick_off" type="file" class="form-control form-white"
                                               placeholder="File Kick Off">
                                    </div>
                                </div>
                                @if(@$kick_off->file_kick_off != null)
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-md-3">
                                            <a target="_blank"
                                               href="{{url('upload/'.@$kick_off->file_kick_off)}}">
                                                <button type="button" class="col-lg-12 btn btn-primary">
                                                    <center>
                                                        <i class="fa fa-download"></i>Preview File
                                                    </center>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr/>
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a href="{{ url('internal/kick_off') }}"
                                   class="btn btn-warning btn-square btn-embossed">Batal
                                    &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit"
                                        class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <!-- Buttons Loading State -->

    <script>
        $('#pekerjaan').change(function () {
            var unit = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('unit'));
            var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
            var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
            //
            $('input[name=instalasi]').val(instalasi.nama_instalasi);
            $('input[name=lingkup_pekerjaan]').val(lingkup.jenis_lingkup_pekerjaan);
        });

        $('#datetimepicker').data("DateTimePicker").function();

    </script>
@endsection
