@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Permohonan <strong>RLB</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Permohonan RLB</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_RLB_RLS))
                                <div class="btn-group">
                                    <a href="{{url('internal/create_permohonan_rlb')}}"
                                    class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Permohonan RLB</a>
                                </div>
                            @endif
                        </div>
                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_VIEW))
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor Permintaan RLB</th>
                                    <th>Pekerjaan</th>
                                    <th>Tanggal Inspeksi</th>
                                    <th>Jenis Instalasi</th>
                                    <th>Status</th>
                                    <th style="width:25%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                ?>
                                @foreach ($permohonan_rlb as $key => $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item->nomor_permintaan_rlb}}</td>
                                        <td>{{$item->pekerjaan->pekerjaan}}</td>
                                        <td>{{$item->tgl_inspeksi}}</td>
                                        <td>{{@$item->pekerjaan->permohonan->instalasi->jenis_instalasi->jenis_instalasi}}</td>
                                        <td>{{@$item->flow_status->status}}</td>
                                        <td>
                                            <a href="{{url('internal/detail_permohoanan_rlb/'.$item->id)}}"
                                                class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                class="fa fa-eye"
                                                data-rel="tooltip"
                                                data-placement="right" title="Detail RLB"></i>
                                            </a>
                                            <a href="{{url('internal/create_permohonan_rlb/'.$item->id)}}"
                                                class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                class="fa fa-pencil"
                                                data-rel="tooltip"
                                                data-placement="right" title="Edit RLB"></i>
                                            </a>
                                            <a href="{{url('internal/delete_permohonan_rlb/'.$item->id)}}"
                                                onclick="return confirm('Anda yakin mengahapus Permohonan RLB?')"
                                                class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                class="fa fa-trash"
                                                data-rel="tooltip"
                                                data-placement="right" title="Delete RLB"></i>
                                            </a>
                                            <a href="{{url('internal/print_permohonan_rlb/'.$item->id)}}"
                                               class="btn btn-sm btn-success btn-square btn-embossed"><i
                                                        class="fa fa-print"
                                                        data-rel="tooltip"
                                                        data-placement="left" title="Print Permohonan RLB"></i>
                                            </a>
                                            @if(\App\PermohonanRlb::isAllowRlb($item))
                                                <a href="{{url('internal/create_rlb/'.$item->id.'/'.@$item->rlb_id)}}"
                                                    class="btn btn-sm btn-success btn-square btn-embossed"><i
                                                    class="fa fa-file"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Create RLB"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('page_script')
        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- Tables Filtering, Sorting & Editing -->
        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!-- Buttons Loading State -->

        <script>
        $(document).ready(function () {
            var oTable = $('#my_table').dataTable();
        });
        </script>
    @endsection
