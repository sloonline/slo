@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Rekomendasi <strong>Laik Bertegangan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">RLB</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_rlb', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('permohonan_rlb_id',@$permohonan_rlb_id) !!}
                            {!! Form::hidden('id',@$id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        @if($rlb == null)
                                            <select data-search="true" name="pekerjaan_id" class="form-control form-white" id="pekerjaan"
                                            required>
                                            <option>- Pilih Pekerjaan -</option>
                                            @foreach ($pekerjaan as $key => $item)
                                                <option {{($item->id == @$rlb->pekerjaan_id)? 'selected': ''}}
                                                    value="{{$item->id}}"
                                                    user="{{$item->permohonan->user}}"
                                                    instalasi="{{$item->permohonan->instalasi}}"
                                                    permohonan="{{$item->permohonan}}"
                                                    >{{$item->pekerjaan}}</option>
                                                    {{-- <option value="{{$item->id}}" {{($item->id == @$rlb->manager_bki)? 'selected': ''}}>{{$item->nama_user}}</option> --}}
                                                @endforeach
                                            @else
                                                {!! Form::hidden('pekerjaan_id',@$rlb->pekerjaan_id) !!}
                                                <input name="pekerjaan" type="text" class="form-control"
                                                value="{{@$rlb->pekerjaan->pekerjaan}}" placeholder="Pekerjaan"
                                                readonly>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <a class="btn btn-primary btn-square btn-embossed"><i
                                            class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Rekomendasi</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value="{{@$rlb->nomor_rekomendasi}}" id="pekerjaan" name="no_rekomendasi"
                                            type="text" class="form-control form-white"
                                            placeholder="Nomor Rekomendasi" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal Terbit</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input name="tgl_terbit" type="text"
                                            value="{{@$rlb->tanggal_terbit}}"
                                            class="form-control b-datepicker form-white"
                                            data-date-format="dd-mm-yyyy"
                                            data-lang="en"
                                            data-RTL="false" placeholder="Tanggal Terbit" required>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Peminta Jasa</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <input value="{{@$rlb->pekerjaan->permohonan->user->nama_user}}"   name="peminta_jasa" type="text" class="form-control"
                                            placeholder="Peminta Jasa" readonly required>
                                        </div>
                                        <div class="col-sm-2">
                                            <a class="btn btn-primary btn-square btn-embossed"><i
                                                class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <div class="col-sm-3">
                                                <label class="col-sm-12 control-label">Instalasi</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input value="{{@$rlb->pekerjaan->permohonan->instalasi->nama_instalasi}}" name="instalasi" type="text" class="form-control" placeholder="Instalasi"
                                                readonly required>
                                            </div>
                                            <div class="col-sm-2">
                                                <a id="detail_instalasi" href="{{($rlb == null) ? "#" : url('/internal/create_permohonan/'.@$rlb->pekerjaan->permohonan->id_orders."/".@$rlb->pekerjaan->permohonan_id)}}"
                                                    target="_blank" class="btn btn-primary btn-square btn-embossed"><i class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Surat Permohonan Peminta Jasa</label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <input name="surat_permohonan" value="{{@$rlb->pekerjaan->permohonan->nomor_permohonan}}" type="text" class="form-control" placeholder="Nomor Surat Permohonan" readonly required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input name="tgl_surat_permohonan"
                                                    type="text"
                                                    class="form-control b-datepicker"
                                                    data-date-format="dd-mm-yyyy"
                                                    data-lang="en"
                                                    data-RTL="false" value="{{@$rlb->pekerjaan->permohonan->tanggal_permohonan}}"
                                                    placeholder="Tanggal" readonly required>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a class="btn btn-primary btn-square btn-embossed"><i
                                                        class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Permohonan Tim Inspeksi
                                                            Teknik</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input value="{{@$rlb->no_srt_tim_inspeksi}}"
                                                            name="surat_permohonan_tim_inspeksi" type="text"
                                                            class="form-control form-white"
                                                            placeholder="Nomor Surat Permohonan Tim Inspeksi" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input name="tgl_surat_permohonan_tim_inspeksi"
                                                            value="{{@$rlb->tgl_srt_tim_inspeksi}}"
                                                            type="text"
                                                            class="form-control b-datepicker form-white"
                                                            data-date-format="dd-mm-yyyy"
                                                            data-lang="en"
                                                            data-RTL="false"
                                                            placeholder="Tanggal" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Manajer BKI</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select data-search="true" name="manajer_bki" class="form-control form-white" required>
                                                                <option>- Pilih Manajer -</option>
                                                                @foreach ($mb_bki as $key => $item)
                                                                    <option value="{{$item->id}}" {{($item->id == @$rlb->manager_bki)? 'selected': ''}}>{{$item->nama_user}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-12">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">File RLB</label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <input name="file_rlb" type="file" class="form-control form-white"
                                                            placeholder="File RLB">
                                                        </div>
                                                    </div>
                                                    @if(@$rlb->file_rlb != "")
                                                        <div class="form-group col-lg-12">
                                                            <div class="col-sm-3">
                                                                <label class="col-sm-12 control-label"></label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <a href="{{@$rlb->file_rlb}}" class="btn btn-primary"><i class="fa fa-download"></i> Download</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if($id > 0)
                                                    <div class="form-group col-lg-12">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label"></label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <a id="file_template"
                                                               href="{{url('internal/print_rlb/'.$rlb->id)}}">
                                                                <button type="button" class="col-lg-12 btn btn-success">
                                                                    <center>
                                                                        <i class="fa fa-print"></i> Cetak  RLB
                                                                    </center>
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                @if($rlb == null)
                                                    <hr/>
                                                    <div class="panel-footer clearfix bg-white">
                                                        <div class="pull-right">
                                                            <a href="{{ url('internal/rlb') }}"
                                                            class="btn btn-warning btn-square btn-embossed">Batal
                                                            &nbsp;<i class="icon-ban"></i></a>
                                                            <button type="submit"
                                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                                            data-style="zoom-in">Simpan &nbsp;<i
                                                            class="glyphicon glyphicon-floppy-saved"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        @else
                                        </div>
                                        {!! Form::close() !!}
                                            @include('workflow_view')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endsection


                    @section('page_script')
                        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                        <!-- Tables Filtering, Sorting & Editing -->
                        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                        <!-- Buttons Loading State -->

                        <script>
                        $('#pekerjaan').change(function () {
                            var user = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('user'));
                            var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
                            var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
                            // console.log(user);
                            $('input[name=peminta_jasa]').val(user.nama_user);
                            $('input[name=instalasi]').val(instalasi.nama_instalasi);
                            $('input[name=surat_permohonan]').val(permohonan.nomor_permohonan);
                            $('input[name=tgl_surat_permohonan]').val(permohonan.tanggal_permohonan);
                        });
                        </script>
                    @endsection
