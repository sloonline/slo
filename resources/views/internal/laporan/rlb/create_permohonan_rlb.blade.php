@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <?php
    $tmp_pekerjaan = null;
    ?>
    <div class="page-content">
        <div class="header">
            <h2>Permohonan <strong>RLB</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Permohonan RLB</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            {!! Form::open(array('url'=> 'internal/save_permohonan_rlb', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                            {!! Form::hidden('id',@$permohonan_rlb->id) !!}
                            <div class="panel-content pagination2 table-responsive">
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Permintaan RLB</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="no_permintaan_rlb"
                                               value="{{@$permohonan_rlb->nomor_permintaan_rlb}}"
                                               type="text" class="form-control form-white"
                                               placeholder="Nomor Permintaan RLB" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Permintaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="tgl_permintaan_rlb"
                                               value="{{@$permohonan_rlb->tgl_permintaan}}"
                                               type="text"
                                               class="form-control form-white b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en"
                                               data-RTL="false"
                                               placeholder="Tanggal Permintaan" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Manager Bidang</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select data-search="true" name="manager" class="form-control form-white" required>
                                            <option value="">- Pilih Manager -</option>
                                            @foreach ($mb_bki as $key => $item)
                                                <option value="{{$item->id}}"
                                                        {{($item->id == @$permohonan_rlb->manager)? 'selected': ''}}
                                                >{{$item->nama_user}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Deputi Manager</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select data-search="true" name="deputi_manager" class="form-control form-white" required>
                                            <option value="">- Pilih DM -</option>
                                            @foreach ($dm_bki as $key => $item)
                                                <option value="{{$item->id}}"
                                                        {{($item->id == @$permohonan_rlb->deputi_manager)? 'selected': ''}}
                                                >{{$item->nama_user}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select name="pekerjaan_id" class="form-control form-white" data-search="true" id="pekerjaan"
                                                required>
                                            <option value="">- Pilih Pekerjaan -</option>
                                            @foreach ($pekerjaan as $key => $item)
                                                <option {{($item->id == @$permohonan_rlb->pekerjaan_id)? 'selected': ''}}
                                                        value="{{$item->id}}"
                                                        user="{{$item->permohonan->user}}"
                                                        instalasi="{{$item->permohonan->instalasi}}"
                                                        permohonan="{{$item->permohonan}}"
                                                        lingkup="{{$item->permohonan->lingkup_pekerjaan}}"
                                                >{{$item->pekerjaan}} ({{@$item->permohonan->user->perusahaan->nama_perusahaan}})</option>
                                                {{-- <option value="{{$item->id}}" {{($item->id == @$rlb->manager_bki)? 'selected': ''}}>{{$item->nama_user}}</option> --}}
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{($permohonan_rlb == null) ? "#" : url('/internal/detail_pekerjaan/'.@$permohonan_rlb->pekerjaan_id)}}"
                                           target="_blank" id="detail_pekerjaan"
                                           class="btn btn-primary btn-square btn-embossed"><i
                                                    class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Inspeksi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="tgl_inspeksi"
                                               value="{{@$permohonan_rlb->tgl_inspeksi}}"
                                               type="text"
                                               class="form-control form-white b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en"
                                               data-RTL="false"
                                               placeholder="Tanggal Inspeksi" required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Permohonan Peminta Jasa</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input value="{{@$permohonan_rlb->pekerjaan->permohonan->nomor_permohonan}}"
                                               name="surat_permohonan" type="text" class="form-control"
                                               placeholder="Nomor Surat Permohonan" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <input name="tgl_surat_permohonan"
                                               value="{{@$permohonan_rlb->pekerjaan->permohonan->tanggal_permohonan}}"
                                               type="text"
                                               class="form-control b-datepicker"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en"
                                               data-RTL="false"
                                               placeholder="Tanggal" readonly required>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Instalasi</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="nama_instalasi"
                                               value="{{@$permohonan_rlb->pekerjaan->permohonan->instalasi->nama_instalasi}}"
                                               type="text" class="form-control"
                                               placeholder="Nama Instalasi" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{($permohonan_rlb == null) ? "#" : url('/internal/create_permohonan/'.@$permohonan_rlb->pekerjaan->permohonan->id_orders."/".@$permohonan_rlb->pekerjaan->permohonan_id)}}"
                                           target="_blank" id="detail_instalasi"
                                           class="btn btn-primary btn-square btn-embossed"><i
                                                    class="glyphicon glyphicon-info-sign"></i> Detail</a>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label"> Lingkup</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="lingkup"
                                               value="{{@$permohonan_rlb->pekerjaan->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}"
                                               type="text" class="form-control"
                                               placeholder="Lingkup" readonly required>
                                    </div>
                                </div>
                            </div>

                            <hr/>
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="{{ url('internal/permohonan_rlb') }}"
                                       class="btn btn-warning btn-square btn-embossed">Batal
                                        &nbsp;<i class="icon-ban"></i></a>
                                    <button type="submit"
                                            class="btn btn-success ladda-button btn-square btn-embossed"
                                            data-style="zoom-in">Simpan &nbsp;<i
                                                class="glyphicon glyphicon-floppy-saved"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $('#pekerjaan').change(function () {
                    var user = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('user'));
                    var instalasi = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('instalasi'));
                    var permohonan = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('permohonan'));
                    var lingkup = jQuery.parseJSON($('#pekerjaan').find('option:selected').attr('lingkup'));
                    var pekerjaan_id = this.value;
                    $('input[name=peminta_jasa]').val(user.nama_user);
                    $('input[name=nama_instalasi]').val(instalasi.nama_instalasi);
                    $('input[name=surat_permohonan]').val(permohonan.nomor_permohonan);
                    $('input[name=tgl_surat_permohonan]').val(permohonan.tanggal_permohonan);
                    $('input[name=lingkup]').val(lingkup.jenis_lingkup_pekerjaan);


                    if (permohonan.id > 0) {
                        $("#detail_instalasi").attr("href", "{{url('/internal/create_permohonan/')}}/" + permohonan.id_orders + "/" + permohonan.id);
                    } else {
                        $("#detail_instalasi").attr("href", "#");
                    }

                    if (pekerjaan_id > 0) {
                        $("#detail_pekerjaan").attr("href", "{{url('/internal/detail_pekerjaan/')}}/" + pekerjaan_id);
                    } else {
                        $("#detail_pekerjaan").attr("href", "#");
                    }
                });
            </script>
@endsection
