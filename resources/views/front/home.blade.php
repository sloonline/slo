@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky transparent header-md clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left scrollTo" href="#top">
	<img src="{{ url('/') }}/assets_front/images/logo_light.png" alt="" />
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- SLIDER -->
<section id="slider" class="fullheight parallax parallax-4" style="background-image:url('{{ url('/') }}/assets_front/images/pln/1.jpg');">
	<div class="overlay dark-5"><!-- dark overlay [0 to 9 opacity] --></div>

	<div class="display-table">
		<div class="display-table-cell vertical-align-middle">
			<div class="container">
				<div class="slider-featured-text text-center">
					<h1 class="text-white wow fadeInUp" data-wow-delay="0.4s">
						PUSAT SERTIFIKASI
					</h1>
					<h2 class="text-white wow fadeInUp" data-wow-delay="0.8s">PT. PLN (PERSERO)</h2>
					<a class="btn btn-default btn-lg wow fadeInUp animated" data-wow-delay="1.5s" href="{{ url('register') }}" style="visibility: visible; animation-delay: 1.5s; animation-name: fadeIn;">&nbsp; REGISTER &nbsp;</a>
				</div>
	
			</div>
		</div>
	</div>

</section>
<!-- /SLIDER -->

<!-- CALLOUT -->
<section class="callout-dark heading-title heading-arrow-bottom" style="z-index:100;">
	<div class="container">
		<div class="text-center">
			<span class="font-lato size-30">
				<strong>Faster, Better and Competitive</strong>
			</span>
		</div>

	</div>
</section>
<!-- /CALLOUT -->

<!-- tentang -->
<section id="tentang">
	<div class="container">
		<div class="heading-title heading-dotted text-center">
			<h2>Pusat Sertifikasi</h2>
		</div>
		<!-- -->
		<article class="row">
			<div class="col-md-6">
				<!-- OWL SLIDER -->
				<div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"items": 1, "autoHeight": true, "navigation": true, "pagination": true, "transitionStyle":"backSlide", "progressBar":"true"}'>
					<div>
						<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln2.jpg" alt="">
					</div>
					<div>
						<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln1.jpg" alt="">
					</div>
					<div>
						<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln3.jpg" alt="">
					</div>
				</div>
				<!-- /OWL SLIDER -->
			</div>
			<div class="col-sm-6">
				<p class="dropcap">Pusat Sertifikasi merupakan salah satu unit penunjang <strong>PT. PLN (Persero)</strong> yang mempunyai tugas utama menjalankan kegiatan
				sertifikasi di bidang ketenagalistrikan dan telah terakreditasi yang meliputi Sertifikasi Kelaikan Instalasi Pembangkit Tenaga Listrik, Sertifikasi Kelaikan Instalasi Jaringan, Sertifikasi Produk (SPM / SNI), Sertifikasi
				Manajemen Mutu, Sertifikasi Sistem Manajemen Lingkungan, Sertifikasi Sistem Manajemen K3, Sertifikasi OHSAS 18001, Komisioning Pembangkit dan Jaringan</p>


				<a href="{{url('/pusertif')}}">
                    Read
                    <!-- /word rotator -->
                    <span class="word-rotator" data-delay="1500">
                        <span class="items">
                            <span>more</span>
                            <span>now</span>
                        </span>
                    </span><!-- /word rotator -->
                    <i class="glyphicon glyphicon-menu-right size-12"></i>
                </a>
				<hr />

				<div class="row countTo-sm text-center">

					<div class="col-xs-6 col-sm-4">
						<i class="fa fa-users size-20"></i> &nbsp; 
						<span class="countTo" data-speed="3000" style="color:#59BA41">1303</span>
						<h6>PEMINTA JASA</h6>
					</div>

					<div class="col-xs-6 col-sm-4">
						<i class="fa fa-briefcase size-20"></i> &nbsp; 
						<span class="countTo" data-speed="3000" style="color:#774F38">56000</span>
						<h6>TOTAL PEKERJAAN</h6>
					</div>

					<div class="col-xs-6 col-sm-4">
						<i class="fa fa-flag-checkered size-20"></i> &nbsp;
						<span class="countTo" data-speed="3000" style="color:#C02942">4897</span>
						<h6>PEKERJAAN SELESAI</h6>
					</div>

				</div>

			</div>
		</article>
		<!-- / -->

	</div>
</section>
<!-- /tentang -->

<section id="visimisi">
    <div class="container">
        <div class="heading-title heading-dotted text-center">
            <h2>Visi, Misi dan Nilai</h2>
        </div>
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-12 ">
                <h2>Nilai Perusahaan</h2>
                <a class="btn btn-danger btn-lg">SALING PERCAYA (MUTUAL TRUST)</a><hr>
                <a class="btn btn-primary btn-lg">INTEGRITAS (INTEGRITY)</a><hr>
                <a class="btn btn-success btn-lg">PEDULI (CARE)</a><hr>
                <a class="btn btn-warning btn-lg">PEMBELAJAR (LEARNER)</a>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Visi</h2>
                <p>Menjadi perusahaan jasa sertifikasi di bidang ketenagalistrikan dan manajemen mutu yang terbaik di regional.</p>
                <hr>
                <h2>Misi</h2>

                <ul class="list-unstyled list-icons">
                    <li><i class="fa fa-check"></i> Menjalankan bisnis sertifikasi di bidang ketenagalistrikan yang meliputi: Sertifikasi Produk, Sertifikasi Sistem Manajemen Mutu dan Sertifikasi Kelaikan Instalasi</li>
                    <li><i class="fa fa-check"></i> PLN Pusertif, sebagai bagian dari masyarakat Indonesia, terus memupuk inovasi dan kreativitas dalam mengembangkan produk dan jasa sertifikasi di bidang ketenagalistrikan dengan standar internasional dan menggunakan teknologi terbaik untuk fasilitas sertifikasi serta mempertahankan sentuhan local untuk memenuhi kebutuhan Kualitas</li>
                    <li><i class="fa fa-check"></i> Memenuhi tuntutan pasar dengan mengutamakan kepuasan pelanggan serta memberikan hasil terbaik kepada Pegawai dan stake holder</li>
                </ul>

            </div>
        </div>

    </div>
</section>

<!-- layanan -->
<section id="layanan">
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Layanan</h2>
		</div>


		<div class="row">

			<div class="col-sm-4">
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/produk/pln1.jpg" alt="img" />
				<h4 class="margin-top-10">Sertifikasi Sistem Manajemen Mutu</h4>
				<p>Berbekal pengalaman melakukan sertifikasi sistem manajemen mutu sejak tahun 2003, PT PLN (Persero) Pusat Sertifikasi (LMK Certification) berkomitmen melaksanakan proses sertifikasi secara profesional...</p>
				<a href="{{url('/s_sistem_manajemen_mutu')}}">
					Read
					<!-- /word rotator -->
					<span class="word-rotator" data-delay="2000">
						<span class="items">
							<span>more</span>
							<span>now</span>
						</span>
					</span><!-- /word rotator -->
					<i class="glyphicon glyphicon-menu-right size-12"></i>
				</a>
			</div>

			<div class="col-sm-4">
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln1.jpg" alt="img" />
				<h4 class="margin-top-10">Sertifikasi Sistem Manajemen Lingkungan</h4>
				<p>Berbekal pengalaman melakukan sertifikasi sistem manajemen mutu sejak tahun 2003, PT PLN (Persero) Pusat Sertifikasi (LMK Certification) berkomitmen melaksanakan proses sertifikasi secara profesional...</p>
				<a href="{{url('/s_sistem_manajemen_lingkungan')}}">
					Read
					<!-- /word rotator -->
					<span class="word-rotator" data-delay="2000">
						<span class="items">
							<span>more</span>
							<span>now</span>
						</span>
					</span><!-- /word rotator -->
					<i class="glyphicon glyphicon-menu-right size-12"></i>
				</a>
			</div>

			<div class="col-sm-4">
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln3.jpg" alt="img" />
				<h4 class="margin-top-10">Sertifikasi Sistem Pengawasan Mutu</h4>
				<p>Sistem Pengawasan Mutu (SPM) yaitu sertifikasi terhadap pihak kedua atau mewakili user PLN. Kegiatan ini telah dilakukan sejak tahun 1975 pada saat lembaga ini berstatus sebagai Pusat</p>
				<a href="{{url('/s_sistem_pengawasan_mutu')}}">
					Read
					<!-- /word rotator -->
					<span class="word-rotator" data-delay="2000">
						<span class="items">
							<span>more</span>
							<span>now</span>
						</span>
					</span><!-- /word rotator -->
					<i class="glyphicon glyphicon-menu-right size-12"></i>
				</a>
			</div>

		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4">
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln3.jpg" alt="img" />
                <h4 class="margin-top-10">SLO & Komisioning</h4>
                <p>PLN Pusat Sertifikasi melakukan inspeksi/komisioning dan mengeluarkan Sertifikat Laik Operasi (SLO) sebagai pengakuan secara profesional, formal dan legal untuk suatu instalasi ketenagalistrikan</p>
                <a href="{{url('/slo_&_komisioning')}}">
                    Read
                    <!-- /word rotator -->
                    <span class="word-rotator" data-delay="2000">
                        <span class="items">
                            <span>more</span>
                            <span>now</span>
                        </span>
                    </span><!-- /word rotator -->
                    <i class="glyphicon glyphicon-menu-right size-12"></i>
                </a>
            </div>
            <div class="col-sm-4">
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln3.jpg" alt="img" />
                <h4 class="margin-top-10">Sertifikasi Sistem Manajemen OHSAS</h4>
                <p>Lembaga Sertifikasi Sistem Manajemen OHSAS mempunyai ruang lingkup berupa Penyediaan Kelistrikan, Peralatan Listrik, Peralatan Optik, Teknologi Informasi, Jasa Engineering</p>
                <a href="{{url('/s_sistem_manajemen_OHSAS')}}">
                    Read
                    <!-- /word rotator -->
                    <span class="word-rotator" data-delay="2000">
                        <span class="items">
                            <span>more</span>
                            <span>now</span>
                        </span>
                    </span><!-- /word rotator -->
                    <i class="glyphicon glyphicon-menu-right size-12"></i>
                </a>
            </div>
            <div class="col-sm-4">
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/front/pusertif/pln3.jpg" alt="img" />
                <h4 class="margin-top-10">SMK3</h4>
                <p>Untuk menilai penerapan sistem manajemen keselamatan dan kesehatan kerja pada setiap tempat kerja perlu dilakukan audit sistem manajemen keselamatan dan kesehatan kerja</p>
                <a href="{{url('/smk3')}}">
                    Read
                    <!-- /word rotator -->
                    <span class="word-rotator" data-delay="2000">
                        <span class="items">
                            <span>more</span>
                            <span>now</span>
                        </span>
                    </span><!-- /word rotator -->
                    <i class="glyphicon glyphicon-menu-right size-12"></i>
                </a>
            </div>
        </div>

	</div>
</section>

<!-- akreditasi -->
<section id="akreditasi">
    <div class="container">
        <div class="heading-title heading-dotted text-center">
            <h2>Akeditasi</h2>
        </div>

        <div class="row">

            <div class="col-md-3">

                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title">
                        <i class="fa fa-tablet"></i>
                        <h2>LSSM – 011 – IDN</h2>
                    </a>
                    <p class="text-muted">Lembaga Sertifikasi Sistem Manajemen Mutu (LSSM)</p>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title">
                        <i class="fa fa-random"></i>
                        <h2>LSSML – 009 – IDN</h2>
                    </a>
                    <p class="text-muted">Lembaga Sertifikasi Sistem Manajemen Lingkungan (LSSML)</p>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title">
                        <i class="fa fa-tint"></i>
                        <h2>LSPr – 005 – IDN</h2>
                    </a>
                    <p class="text-muted">Lembaga Sertifikasi Produk (LSPr)</p>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-icon box-icon-center box-icon-transparent box-icon-large">
                    <a class="box-icon-title">
                        <i class="fa fa-cogs"></i>
                        <h2>LI – 026 – IDN</h2>
                    </a>
                    <p class="text-muted">Lembaga Inspeksi Teknik</p>
                </div>

            </div>

        </div>

    </div>
</section>
<!-- akreditasi -->

<!-- alur -->
<section id="alur">
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Alur Proses Sertifikasi</h2>
		</div>

		<!-- FEATURED BOXES 3 -->
		<div class="row">

			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/flow.png" alt="img" />

		</div>
		<!-- /FEATURED BOXES 3 -->

	</div>
</section>
<!-- /alur -->

<!-- Clients -->
<!--
<section id="pelanggan">
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Pelanggan</h2>
		</div>

		<ul class="row clients-dotted list-inline">
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/1.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/2.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/3.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/4.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/5.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/6.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/7.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/8.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/5.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/6.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/7.jpg" alt="client" />
				</a>
			</li>
			<li class="col-md-3 col-sm-3 col-xs-6">
				<a href="#">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/brands/8.jpg" alt="client" />
				</a>
			</li>
		</ul>

	</div>
</section>
-->
<!-- /Clients -->


<!-- feedback -->
<!--
<section id="feedback">
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Feedback</h2>
		</div>


		<div class="row">

			<div class="col-md-12 col-sm-12">

				<h3>Drop us a line or just say <strong><em>Hello!</em></strong></h3>

				<div id="alert_success" class="alert alert-success margin-bottom-30">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Thank You!</strong> Your message successfully sent!
				</div>

				<div id="alert_failed" class="alert alert-danger margin-bottom-30">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>[SMTP] Error!</strong> Internal server error!
				</div>

				<div id="alert_mandatory" class="alert alert-danger margin-bottom-30">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Sorry!</strong> You need to complete all mandatory (*) fields!
				</div>

				<form action="php/contact.php" method="post" enctype="multipart/form-data">
					<fieldset>
						<input type="hidden" name="action" value="contact_send" />

						<div class="row">
							<div class="form-group">
								<div class="col-md-4">
									<label for="contact:name">Full Name *</label>
									<input required type="text" value="" class="form-control" name="contact[name][required]" id="contact:name">
								</div>
								<div class="col-md-4">
									<label for="contact:email">E-mail Address *</label>
									<input required type="email" value="" class="form-control" name="contact[email][required]" id="contact:email">
								</div>
								<div class="col-md-4">
									<label for="contact:phone">Phone</label>
									<input type="text" value="" class="form-control" name="contact[phone]" id="contact:phone">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-8">
									<label for="contact:subject">Subject *</label>
									<input required type="text" value="" class="form-control" name="contact[subject][required]" id="contact:subject">
								</div>
								<div class="col-md-4">
									<label for="contact_department">Department</label>
									<select class="form-control pointer" name="contact[department]">
										<option value="">--- Select ---</option>
										<option value="Marketing">Marketing</option>
										<option value="Webdesign">Webdesign</option>
										<option value="Architecture">Architecture</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									<label for="contact:message">Message *</label>
									<textarea required maxlength="10000" rows="8" class="form-control" name="contact[message]" id="contact:message"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<div class="col-md-12">
									<label for="contact:attachment">File Attachment</label>

									<input class="custom-file-upload" type="file" id="file" name="contact[attachment]" id="contact:attachment" data-btn-text="Select a File" />
									<small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>

								</div>
							</div>
						</div>

					</fieldset>

					<div class="row">
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> SEND MESSAGE</button>
						</div>
					</div>
				</form>

			</div>

		</div>

	</div>
</section>
-->
<!-- /feedback -->
	<div style="padding-top:10%" id="message" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<!-- header modal -->
				<div class="modal-header" style="text-align: center">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title" id="myLargeModalLabel">Registrasi Berhasil</h3>
				</div>

				<!-- body modal -->
				<div class="modal-body" style="text-align: center">
					<p style="font-size: 15pt">Registrasi berhasil dilakukan<br>Mohon menunggu konfirmasi terkait verifikasi data melalui email.</p>
				</div>

			</div>
		</div>
	</div>

@endsection

@section('page_script')
		<script type="text/javascript">
			@if(Session::has('message'))
				$('#message').modal('show');
			@endif
		</script>
@endsection