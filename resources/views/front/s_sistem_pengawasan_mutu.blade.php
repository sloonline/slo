@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Sertifikasi Sistem Pengawasan Mutu Produk Peralatan Listrik (SNI/SPM)</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
            </a>
        </div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->
		<div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Sertifikasi produk merupakan merupakan salah satu lingkup bisnis PLN Pusertif, yang terdiri dari :</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sistem Pengawasan Mutu (SPM): yaitu sertifikasi terhadap pihak kedua atau mewakili user PLN. Kegiatan ini telah dilakukan sejak tahun 1975 pada saat lembaga ini berstatus sebagai Pusat Penyelidikan Masalah Kelistrikan (PPMK). Sistem Pengawasan Mutu dimaksudkan untuk menjaga agar peralatan listrik yang diproduksi/dijual kepada PLN adalah peralatan listrik yang sudah memenuhi persyaratan teknis sebagai sertifikasi pihak kedua sehingga keselamatan umum dapat terjamin.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi terhadap pihak ketiga yang mewakili user umum.
                                                                             Sertifikasi ini dilakukan berdasarkan standart yang diakui secara umum antara lain: SNI, IEC, atau standart internasional lainnya.</p></li>
            </ul>

        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Produk-produk peralatan listrik yang telah tersertifikasi oleh PLN Jaser :</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Kabel Listrik</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Transformator Distribusi</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Transformator Arus (CT) dan Transformator Tegangan (PT)</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Kubikel</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">kWh Meter</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Kotak APP</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Pemutus Tenaga Mini</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Konektor</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Isolator</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Tiang Beton Pratekan</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Kabel Listrik SNI-LMK</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sakelar SNI-LMK</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Tusuk Kontak SNI-LMK</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Kotak Kontak & Kombinasi Kotak Kontak dan Sakelar SNI-LMK</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">MCB SNI-LMK</p></li>
            </ul>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Dalam proses Sertifikasi Produk, standard yang dirujuk PLN Pusertif adalah :</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Standar PT PLN (Persero) (SPLN)</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Standard Nasional Indonesia</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Standard Internasional Electrotechnical Commission (IEC) dan Standar Nasional Negara Lainnya Sesuai Kebutuhan.</p></li>
            </ul>
        </div>

		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection