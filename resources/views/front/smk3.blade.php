@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
    @endsection

    @section('logo')
    <!-- Logo -->
    <a class="logo pull-left" href="{{url('/')}}#slider">
        <img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
    </a>
    @endsection

    @section('content')
    <!-- -->
    <section>
        <div class="container">

            <div class="heading-title heading-dotted text-center">
                <h2>Sistem Manajemen Keselamatan dan Kesehatan Kerja</h2>
            </div>
            <!-- OWL SLIDER -->
            <div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
                <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                    <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
                </a>
                <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                    <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
                </a>
                <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                    <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
                </a>
            </div>
            <!-- /OWL SLIDER -->

            <!-- IMAGE -->
            <!--
            <figure class="margin-bottom-20">
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
            </figure>
            -->
            <!-- /IMAGE -->

            <!-- VIDEO -->
            <!--
            <div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
            </div>
            -->
            <!-- /VIDEO -->


            <!-- article content -->
            <div class="divider divider-color divider-center divider-short"><!-- divider -->
                <i class="fa fa-cog"></i>
            </div>

            <div class="heading-title heading-border heading-color">
                <p class="dropcap lead">Untuk menilai penerapan sistem manajemen keselamatan dan kesehatan kerja pada setiap tempat kerja perlu dilakukan audit sistem manajemen keselamatan dan kesehatan kerja.
                </p>
                <br><p class="lead">PT PLN (Persero) Pusat Sertifikasi ditunjuk sebagai Lembaga Audit Sistem Manajemen Keselamatan dan Kesehatan Kerja (SMK3) sesuai dengan Keputusan Menteri Tenaga Kerja dan Transmigrasi R.I nomor KEP.45/M/DJPPK/VII/2013.</p>
                <br><p class="lead">Lembaga Audit Sistem Manajemen Keselamatan dan Kesehatan melaksanakan audit berdasarkan rencana kerja tahunan audit dan atau penetapan perusahaan wajib audityang telah disetujui dan hasil audit dilaporkan kepada Direktur Jenderal Pembina Pengawasan Ketenagalistrikan dan perusahaan yang bersangkutan.</p>
                <br><p class="lead">Lembaga Sertifikasi didukung oleh auditor yang kompeten dan berpengalaman untuk setiap ruang lingkup sehingga mampu mendukung dan memberi nilai tambah pada proses sertifikasi.</p>
                <br><p class="lead">Dalam melakukan kegiatan sertifikasi sistem manajemen, lembaga ini berpegang pada prinsip – prinsip profesionalisma, ketidak berpihakan, bebas dari konflik kepentingan serta menjamin objektif sehingga mampu memberi kepercayaan, keyakinan serta kepuasan bagi pelanggan.</p>
            </div>

            <div class="divider divider-dotted"><!-- divider --></div>


        </div>
    </section>
    <!-- / -->
    @endsection