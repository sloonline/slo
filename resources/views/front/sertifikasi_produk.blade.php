@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Sertifikasi Produk</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
            </a>
        </div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->

        <div class="heading-title heading-border heading-color">
            <p class="dropcap lead">Sertifikasi produk adalah salah satu kegiatan PT. PLN (Persero) Pusat Sertifikasi yang lebih dikenal sebagai kegiatan Sistem Pengawasan Mutu (SPM). Kegiatan ini telah dilakukan sejak tahun 1975 pada saat lembaga ini berstatus sebagai Pusat Penyelidikan Masalah Kelistrikan (PPMK). Semua peraltan listrik yang digunakan oleh PLN harus bersertifikat SPM.</p>
        </div>
        <div class="heading-title heading-border heading-color">
            <p class="lead">Sertifikat SPM yang diterbitkan meliputi produk Kabel Listrik, MCB, Perangkat Hubung Bagi, Tiang Besi, Tiang Beton, Isolator, Konektor, Load Breaker Switch (LBS, Arrester, Fuse Cut Out (FCO), Transformator Distribusi, Current Transformator (CT), Potential Transformer (PT), Kwh Meter, Alat Pengukur dan Pembatas (APP).</p>
        </div>
        <div class="heading-title heading-border heading-color">
            <p class="lead">Disamping sebagai Lembaga Sertifikasi Produk yang menerbitkan sertifikat SPM, <strong>PT. PLN (Persero) Pusat Sertifikasi</strong> juga menerbitkan tanda Standar Nasional Indonesia (SNI). Lembaga Sertifikasi Produk tersebut sudah diakreditasi oleh Komite Akreditasi Nasional (KAN-BSN) sesuai dengan persyaratan ISO/IEC 17065 dengan nomor akreditasi LSPr-005-IDN.</p>
        </div>
        <div class="heading-title heading-border heading-color">
            <p class="lead">Sertifikasi SNI yang diterbitkan meliputi produk Kabel Listrik, MCB, Sakelar, RCCB, Luminer, Ballast Electronic, Tusuk Kontak dan Kotak Kontak.</p>
        </div>
        <div class="heading-title heading-border heading-color">
            <p class="lead">Dalam melakukan kegiatan sertifikasi produk, lembaga ini berpegang pada prinsip - prinsip profesionalisme, ketidakberpihakan, bebas dari konflik kepentingan serta menjamin obyektif sehingga mampu memberi kepercayaan, keyakinan serta kepuasan bagi pelanggan.</p>
        </div>



		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection