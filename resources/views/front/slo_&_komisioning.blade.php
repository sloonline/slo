@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Sertifikasi Laik Operasi Dan Komisioning Instalasi Tenaga Listrik</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
            </a>
        </div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->
		<div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="dropcap lead">PLN Pusat Sertifikasi melakukan inspeksi/komisioning dan mengeluarkan Sertifikat Laik Operasi (SLO) sebagai pengakuan secara profesional, formal dan legal untuk suatu instalasi ketenagalistrikan terhadap persyaratan teknis, kontrak dan operasi sebelum dioperasikan. Pengakuan formal yang diperoleh tersebut sekaligus merupakan dukungan terhadap keamanan, keandalan instalasi serta akrab lingkungan.
            </p>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
        	<i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Kegiatan inspeksi / komisioning dilakukan antara lain terhadap :</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi pembangkit tenaga listrik yaitu Pembangkit Listrik Tenaga Uap (PLTU), Pembangkit Listrik Tenaga Gas dan Uap (PLTGU), Pembangkit Listrik Tenaga Gas (PLTG)</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Pembangkit Listrik Tenaga Mesin Gas (PLTMG), Pembangkit Listrik Tenaga Panas Bumi (PLTP),Pembangkit Listrik Tenaga Diesel (PLTD), Pembangkit Listrik Tenaga Air (PLTA), dan Pembangkit Listrik Tenaga Mikrohidro (PLTM) dan Pembangkit listrik energi baru dan terbarukan</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi jaringan yaitu Saluran Udara Tegangan Ekstra Tinggi (SUTET), Saluran Udara Tegangan Tinggi (SUTT), Gardu Induk (GI), SUTM (Saluran Udara Tegangan Menengah), GD (gardu Distribusi), Jaringan Tegangan Menengah (JTM) 20 KV, JTR (Jaringan Tegangan Rendah), dan instalasi bangunan komersial</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi mesin industri yaitu Turbin, Boiler dan Heat Exhanger</p></li>
            </ul>
        </div>

		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection