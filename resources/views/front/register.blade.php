@extends('../layout/layout_front')

@section('header')
    <div id="header" class="sticky clearfix">
@endsection

@section('logo')
    <!-- Logo -->
    <a class="logo pull-left" href="{{url('/')}}#slider">
        <img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt=""/>
    </a>
@endsection
@section('content')
    <!-- -->
    <section>
        <div class="container">
            <div class="heading-title heading-dotted text-center">
                <h2>Form Registrasi</h2>
            </div>
        @if(Session::has('error'))
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
            </div>
        @endif
        {!!Form::open(['name'=>'form_register','id'=>'form_register','url'=>'register', 'enctype'=>"multipart/form-data", 'post'])!!}
        <!-- Form Registrasi -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <h2 class="panel-title bold">Form Registrasi User Perusahaan Peminta Jasa</h2>
                </div>

                <div class="panel-body">
                    <div class="nav-tabs2" id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#data_perusahaan" data-toggle="tab"><i
                                            class="icon-home"></i> Data
                                    Perusahaan</a></li>
                            <li><a href="#data_user" data-toggle="tab" }}><i class="icon-user"></i> Data
                                    User</a></li>
                            <li><a href="#ijin_usaha" data-toggle="tab" }}><i class="icon-wrench"></i> Ijin Usaha</a></li>
                        </ul>
                        <div class="tab-content bg-white">
                            <div class="tab-pane active" id="data_perusahaan">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nama Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-building"></i>
                                            <input name="nama_perusahaan" required type="text"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Nama Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nama Pemimpin Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-user-secret"></i>
                                            <input name="nama_pemimpin_perusahaan" type="text"
                                                   class="form-control" required onkeyup="checkform()"
                                                   placeholder="Nama Pemimpin Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nomor NPWP Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-credit-card"></i>
                                            <input name="no_npwp_perusahaan" required type="text"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Nomor NPWP Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File NPWP Perusahaan *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" id="file_npwp_perusahaan"
                                                       required class="form-control"
                                                       name="file_npwp_perusahaan" accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" id="file_npwp_perusahaan_text"
                                                       class="form-control" onchange="checkform()"
                                                       placeholder="no file selected"
                                                       readonly=""/>
                                                <span class="button">Choose File</span>
                                            </div>
                                            <small class="text-muted block">Max file size: 1Mb
                                                (pdf)
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Alamat Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                                    <textarea name="alamat_perusahaan" required rows="5"
                                                              class="form-control word-count" onkeyup="checkform()"
                                                              data-maxlength="200" data-info="textarea-words-info"
                                                              placeholder="Alamat"></textarea>

                                            <i class="fa fa-location-arrow"><!-- icon --></i>

                                                    <span class="fancy-hint size-12 text-muted">
                                                        <strong>Hint:</strong> 200 words allowed!
                                                        <span class="pull-right">
                                                            <span id="textarea-words-info">0/200</span> Words
                                                        </span>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>Provinsi *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-form fancy-form-select">
                                                <select id="province" required name="provinsi" onchange="checkform()"
                                                        class="form-control select2">
                                                    <option value="">--- Pilih ---</option>
                                                    @foreach($province as $item)
                                                        <option value="{{$item->id}}">{{$item->province}}</option>
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>Kabupaten / Kota *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-form fancy-form-select">
                                                <select id="city" required name="kabupaten" onchange="checkform()"
                                                        class="form-control select2">
                                                    <option value="">--- Pilih ---</option>
                                                    @foreach($city as $item)
                                                        <option value="{{$item->id}}"
                                                                class="{{$item->id_province}}">{{$item->city}}</option>
                                                    @endforeach
                                                </select>
                                                <i class="fancy-arrow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Kode Pos *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-map-marker"></i>
                                            <input name="kode_pos_perusahaan" required type="text"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Kode Pos">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nomor Telepon Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-phone"></i>
                                            <input name="no_telepon_perusahaan" required type="text"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Nomor Telepon Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nomor Fax Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-fax"></i>
                                            <input name="no_fax_perusahaan" required type="text"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Nomor Fax Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Email Perusahaan *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-envelope"></i>
                                            <input name="email_perusahaan" required type="email"
                                                   class="form-control" onkeyup="checkform()"
                                                   placeholder="Email Perusahaan">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File Surat ijin Usaha *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_surat_ijin_usaha" id="file_surat_ijin_usaha"
                                                       accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" id="file_surat_ijin_usaha_text"
                                                       class="form-control" onchange="checkform()"
                                                       placeholder="no file selected"
                                                       readonly=""/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb(pdf)</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File Surat Keterangan Domisili Perusahaan *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_skdp" id="file_skdp" accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" id="file_skdp_text" class="form-control"
                                                       placeholder="no file selected" onchange="checkform()"
                                                       readonly=""/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb
                                                    (pdf)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File Surat ijin Tempat usaha *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_situ" id="file_situ" accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" class="form-control"
                                                       placeholder="no file selected" onchange="checkform()"
                                                       readonly="" id="file_situ_text"/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb
                                                    (pdf)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File Tanda Daftar Perusahaan *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_tdp_perusahaan" id="file_tdp_perusahaan"
                                                       accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" class="form-control"
                                                       placeholder="no file selected" onchange="checkform()"
                                                       readonly="" id="file_tdp_perusahaan_text"/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb
                                                    (pdf)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                            </div>
                            <div class="tab-pane" id="data_user">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nama User *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-user"></i>
                                            <input name="nama_user" required type="text" class="form-control"
                                                   placeholder="Nama User" onkeyup="checkform()">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Email User *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-envelope"></i>
                                            <input id="email_user"  name="email_user" required type="email" class="form-control"
                                                   placeholder="Email User" onkeyup="checkform()">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Tempat Lahir *</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="fancy-form">
                                            <i class="fa fa-legal"></i>
                                            <input name="tempat_lahir" required type="text" class="form-control"
                                                   placeholder="Tempat Lahir" onkeyup="checkform()">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <input name="tanggal_lahir" required type="text"
                                                   class="form-control datepicker" onchange="checkform()"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Tanggal Lahir">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>Gender *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-form">
                                                <label class="radio">
                                                    <input type="radio" class="form-control" name="gender_user"
                                                           value="PRIA">
                                                    <i></i> Pria
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" class="form-control" name="gender_user"
                                                           value="WANITA">
                                                    <i></i> Wanita
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Alamat *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                        <textarea name="alamat_user" required rows="5" class="form-control word-count"
                                                  data-maxlength="200" data-info="textarea-words-info"
                                                  onkeyup="checkform()"
                                                  placeholder="Alamat User"></textarea>

                                            <i class="fa fa-location-arrow"><!-- icon --></i>

								<span class="fancy-hint size-12 text-muted">
									<strong>Hint:</strong> 200 words allowed!
									<span class="pull-right">
										<span id="textarea-words-info">0/200</span> Words
									</span>
								</span>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>No KTP *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-credit-card"></i>
                                            <input name="no_ktp_user" required type="text" class="form-control"
                                                   onkeyup="checkform()"
                                                   placeholder="No KTP">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>No HP *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-phone-square"></i>
                                            <input name="no_hp_user" required type="text" class="form-control"
                                                   onkeyup="checkform()"
                                                   placeholder="No HP">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File NPWP *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_npwp_user" id="file_npwp_user"
                                                       accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" class="form-control"
                                                       placeholder="no file selected" onchange="checkform()"
                                                       readonly="" id="file_npwp_user_text"/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb
                                                    (pdf)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>File KTP *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control"
                                                       name="file_ktp_user" id="file_ktp_user"
                                                       accept="application/pdf"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" class="form-control"
                                                       placeholder="no file selected" onchange="checkform()"
                                                       readonly="" id="file_ktp_user_text"/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb
                                                    (pdf)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="fancy-form">
                                        <div class="col-sm-3">
                                            <label>Foto *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="fancy-file-upload fancy-file-info">
                                                <i class="fa fa-upload"></i>
                                                <input type="file" class="max-file" required
                                                       class="form-control" accept="image/*"
                                                       name="file_foto_user" id="file_foto_user"
                                                       onchange="jQuery(this).next('input').val(this.value);checkform();"/>
                                                <input type="text" class="form-control" onchange="checkform()"
                                                       placeholder="no file selected" id="file_foto_user_text"
                                                       readonly=""/>
                                                <span class="button">Choose File</span>
                                                <small class="text-muted block">Max file size: 1Mb</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <img id="preview_img"
                                                 src="#"
                                                 alt="foto" width="200"/>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                            </div>
                            <div class="tab-pane" id="ijin_usaha">
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Jenis Ijin Usaha *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form fancy-form-select">
                                            <select name="jenis_ijin_usaha" class="form-control" required onchange="checkform()">
                                                <option value="">--- Pilih ---</option>
                                                @foreach($jenis_ijin_usaha as $item)
                                                    <option value="{{$item->id}}">{{$item->nama_reference}}</option>
                                                @endforeach
                                            </select>
                                            <i class="fancy-arrow"></i>
                                        </div>
                                        <div class="fancy-form">

                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Penerbit Ijin Usaha *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-list-alt"></i>
                                            <input name="penerbit_ijin_usaha" required type="text" class="form-control" onkeyup="checkform()"
                                                   placeholder="Penerbit Ijin Usaha">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>No Ijin Usaha *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-th-list"></i>
                                            <input name="no_ijin_usaha" required type="text" class="form-control"
                                                   placeholder="Nomor Ijin Usaha" onkeyup="checkform()">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Masa Berlaku IU *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar-check-o"></i>
                                            <input name="masa_berlaku_iu" required type="text"
                                                   class="form-control datepicker" onkeyup="checkform()"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Masa Berlaku IU">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>File Surat Ijin Usaha *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-file-upload fancy-file-info">
                                            <i class="fa fa-upload"></i>
                                            <input type="file" class="max-file" required
                                                   class="form-control"
                                                   name="file_surat_iu" id="file_surat_iu"
                                                   accept="application/pdf"
                                                   onchange="jQuery(this).next('input').val(this.value);checkform()"/>
                                            <input type="text" class="form-control"
                                                   placeholder="no file selected" onkeyup="checkform()"
                                                   readonly="" id="file_surat_iu"/>
                                            <span class="button">Choose File</span>
                                            <small class="text-muted block">Max file size: 1Mb
                                                (pdf)
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nama Kontrak</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-list-alt"></i>
                                            <input name="nama_kontrak" type="text" class="form-control" onchange="checkform()"
                                                   placeholder="Nama Kontrak">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nomor Kontrak</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-th-list"></i>
                                            <input name="no_kontrak" type="text" class="form-control" onchange="checkform()"
                                                   placeholder="Nomor Kontrak">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Tanggal Pengesahaan Kontrak</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <input name="tgl_pengesahan_kontrak" type="text"
                                                   class="form-control datepicker" onchange="checkform()"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Tanggal Pengesahan Kontrak">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Masa Berlaku Kontrak</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar-check-o"></i>
                                            <input name="masa_berlaku_kontrak" type="text"
                                                   class="form-control datepicker" onchange="checkform()"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Masa Berlaku Kontrak">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>File Kontrak Sewa</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-file-upload fancy-file-info">
                                            <i class="fa fa-upload"></i>
                                            <input type="file" class="max-file"
                                                   class="form-control"
                                                   name="file_kontrak_sewa" id="file_kontrak_sewa"
                                                   accept="application/pdf"
                                                   onchange="jQuery(this).next('input').val(this.value);checkform()"/>
                                            <input type="text" class="form-control"
                                                   placeholder="no file selected" onchange="checkform()"
                                                   readonly="" id="file_kontrak_sewa"/>
                                            <span class="button">Choose File</span>
                                            <small class="text-muted block">Max file size: 1Mb
                                                (pdf)
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Nomor SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-th-list"></i>
                                            <input name="no_spjbtl" type="text" class="form-control" onkeyup="checkform()"
                                                   placeholder="Nomor SPJBTL">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Tanggal SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <input name="tgl_spjbtl" type="text" onchange="checkform()"
                                                   class="form-control datepicker"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Tanggal SPJBTL">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Masa Berlaku SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-form">
                                            <i class="fa fa-calendar-check-o"></i>
                                            <input name="masa_berlaku_spjbtl" type="text"
                                                   class="form-control datepicker" onchange="checkform()"
                                                   data-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                                   placeholder="Masa Berlaku SPJBTL">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>File SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="fancy-file-upload fancy-file-info">
                                            <i class="fa fa-upload"></i>
                                            <input type="file" class="max-file"
                                                   class="form-control"
                                                   name="file_spjbtl" id="file_spjbtl"
                                                   accept="application/pdf"
                                                   onchange="jQuery(this).next('input').val(this.value);checkform()"/>
                                            <input type="text" class="form-control"
                                                   placeholder="no file selected" onchange="checkform()"
                                                   readonly="" id="file_spjbtl"/>
                                            <span class="button">Choose File</span>
                                            <small class="text-muted block">Max file size: 1Mb
                                                (pdf)
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-5"></div>
                        <div class="col-sm-4">
                            <a href="#" class="btn btn-3d btn-danger"><i
                                        class="glyphicon glyphicon-remove"></i>Cancel</a>
                            <button class="btn btn-3d btn-success" value="submit" id="submit_form"><i
                                        class="glyphicon glyphicon-ok"></i>Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        {!!Form::close()!!}
        <!-- /Form Registrasi -->


            <div class="divider divider-dotted"><!-- divider --></div>
        </div>
    </section>
    <!-- / -->

    <div style="padding-top:10%" id="message" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">


                <!-- body modal -->
                <div class="modal-body" style="text-align: center">
                    Email sudah digunakan oleh user lain, mohon menggunakan email lain.
                </div>

            </div>
        </div>
    </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
    <script>
        $("#city").chained("#province");
        /*===05092016 ENABLE SUBMIT WHEN ALL INPUTS COMPLETED====*/
        function checkform() {
            var f = document.forms["form_register"].elements;
            var cansubmit = true;

            for (var i = 0; i < f.length; i++) {
                if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                    cansubmit = false;
                }
            }
            document.getElementById('submit_form').disabled = !cansubmit;
        }
        window.onload = checkform;
        /*=====ON SUBMIT CHECK EMAIL ALREADY EXIST OR NOT=====*/
        $(document).ready(function(){
            $("form").submit(function(){
                //get email user
                var email_user = $("#email_user").val();
                var valid;
                //check email already exist or not
                $.ajax({
                    url: "{{(url('/check_email'))}}" + "/" + email_user,
                    async: false,
                    success: function (data) {
                        console.log(data);
                        if(data == "invalid"){
                            valid = data;
                        }
                    }
                });
                if(valid == "invalid"){
                    $('#message').modal('show');
                    return false;
                }
            });
        });


        //preview foto user
        $('#preview_img').hide();

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('#preview_img').show();
            }
        }

        $("#file_foto_user").change(function () {
            $('#preview_img').hide();
            readURL(this);
        });
    </script>

@endsection