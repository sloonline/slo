@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Pusat Sertifikasi</h2>
              		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
			</a>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
			</a>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
			</a>
		</div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->
        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

		<div class="heading-title heading-border heading-color">
            <p class="dropcap lead">Pusat Sertifikasi merupakan salah satu unit penunjang <strong>PT. PLN (Persero)</strong> yang mempunyai tugas utama menjalankan kegiatan
                                            				sertifikasi di bidang ketenagalistrikan dan telah terakreditasi yang meliputi Sertifikasi Kelaikan Instalasi Pembangkit Tenaga Listrik, Sertifikasi Kelaikan Instalasi Jaringan, Sertifikasi Produk (SPM / SNI), Sertifikasi
                                            				Manajemen Mutu, Sertifikasi Sistem Manajemen Lingkungan, Sertifikasi Sistem Manajemen K3, Sertifikasi OHSAS 18001, Komisioning Pembangkit dan Jaringan. Kegitatan utama institusi ini didukung oleh lebih dari 300 tenaga teknik berpengalaman dari berbagai strata pendidikan dan disiplin ilmu.</p>

            <br><p class="lead">PT PLN (Persero) Pusat Sertifikasi atau yang lebih dekat di konsumen dan stakeholder dengan nama PLN Pusertif, merupakan salah satu unit penunjang yang dimiliki oleh PT PLN (Persero). Fokus kami sebagai unit bisnis dari perusahaan induk, spesifik menjalankan kegiatan sertifikasi di bidang ketenagalistrikan. Di sinilah kompetensi dan pengalaman kami. Karena itulah, PLN Pusertif menjadi pusat rujukan bagi para pemangku kepentingan (stakeholder) di bidang ketenagalistrikan, terkait dengan sertifikasi kendali mutu produk.</p>
            <br><p class="lead">Undang-Undang No.30 Tahun 2009 tentang Ketenagalistrikan, pasal 44 ayat (4) mensyaratkan bahwa setiap instalasi tenaga listrik yang beroperasi wajib memiliki Sertifikat Laik Operasi. PLN Pusertif sebagai unit penunjang yang dimiliki PT PLN (Persero) bertugas memastikan bahwa semua instalasi tenaga listrik mulai dari pembangkitan, transmisi dan distribusi, sampai akhirnya listrik dapat dinikmati oleh pelanggan, telah laik operasi. Pemberian sertifikat kelaikan instalasi bukan berdasarkan proses yang serba instan. Semua proses dijalankan secara konsisten berdasarkan persyaratan batas uji kelaikan dengan pengawasan secara detail, periodik, sistematis dan terpadu. Sehingga nantinya diakui secara profesional memenuhi persyaratan teknis dan peraturan dalam pengoperasian sebagai instalasi yang Aman, Andal dan Akrab Lingkungan.</p>
            <br><p class="lead">Pasal 54 ayat (2) menerangkan bahwa setiap orang yang memproduksi, mengedarkan, atau memperjualbelikan peralatan dan pemanfaatan tenaga listrik harus sesuai dengan Standar Nasional Indonesia (SNI). Dalam konteks ini, PLN Jaser sebagai lembaga sertifikasi memiliki peran penting dalam menjawab tantangan sekaligus memastikan sebuah industri dan produknya konsisten sesuai standar hingga berujung pada sebuah pengakuan. Pengakuan yang diberikan oleh PLN Jaser sebagai lembaga sertifikasi memiliki value lebih karena berdasarkan hasil assesment maupun audit yang dalam prosesnya bersifat independen.</p>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Keunggulan dan Keuntungan Sertifikasi PT. PLN (Persero) Pusat Sertifikasi</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">PT PLN (Persero) Pusat Sertifikasi merupakan lembaga sertifikasi sekaligus lembaga inspeksi terakreditasi oleh KAN-BSN yang dikenal mempunyai kredibilitas oleh regulator dan diakui lembaga internasional.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">PT PLN (Persero) Pusat Sertifikasi berhak langsung menerbitkan Sertifikat, dibawah pengawasan Direktorat Jenderal Ketenagalistrikan (DJK).</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">PT PLN (Persero) Pusat Sertifikasi didukung oleh tenaga ahli profesional yang bersertifikat kompetensi dari lembaga kompetensi personil yang terakreditasi.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Logo LMK telah menjadi trade mark sebagai simbol jaminan mutu produk yang telah dikenal baik di masyarakat maupun pelaku bisnis ketenagalistrikan.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Dipercaya oleh PLN Pusat sebagai penjamin mutu (Quality Assurance) peralatan dan instalasi ketenagalistrikan.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Lebih berpengalaman dan profesional, karena PT PLN (Persero) Pusat Sertifikasi spesialis di bidang ketenagalistrikan.</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Proses sertifikasi ketenagalistrikan memiliki infrastruktur knowledge dan dokumentasi yang lengkap.</p></li>
            </ul>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Ruang Lingkup</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi Kelaikan Instalasi Tenaga Listrik</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Komisioning Pembangkit dan Jaringan</li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi Produk (SPM / SNI)</li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi Sistem Manajemen Mutu (ISO 9001)</li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi Sistem Manajemen Lingkungan (ISO 14001)</li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Sertifikasi Sistem Manajemen K3</li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">OHSAS 18001</li>
            </ul>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>SDM &  Organisasi</strong></p>
            <br><p class="lead">PLN Jaser dibentuk berdasarkan Keputusan Direksi PT PLN (Persero) No. 292.K/010/DIR/2003 tanggal 19 Desember 2003 dan Keputusan Direksi PT PLN (Persero) No.018.K./010/DIR/2007 tanggal 29 Januari 2007 dan disempurnakan dengan Keputusan Direksi PT PLN (Persero) No.594.K./DIR/2012 tanggal 04 Desember 2012 tentang Organisasi PT PLN (Persero) Jasa Sertifkasi, dengan pelanggan baik dari nasional maupun internasional. Dalam tahap taat peraturan GMJS membentuk lembaga sertifikasi yang mengacu pada ISO 17020, 17021, 17025, 17065, dan 17067.</p>
            <br><p class="lead">Profil Tenaga kerja dikelompokkan kedalam dua kategori yaitu karyawan PLN Pusertif dan karyawan tidak tetap dengan jumlah total 302 tenaga kerja terdiri dari karyawan PLN Jaser sebanyak 133, dan tenaga tidak tetap sebanyak 169 (termasuk auditor, inspektor, tenaga ahli, dan tenaga penunjang lainnya) .</p>
            <br><p class="lead">Kompetensi yang dimiliki personel PLN Pusertif telah mendapat sertifikat sebagai bukti hasil assessment oleh pihak yang berkompeten seperti HAKIT, Hatekdis dan IATKI dan Lembaga Uji Kompetensi lainnya yang diakui. Pembelajaran organisasi dilakukan melalui Akreditasi Organisasi oleh KAN (Komite Akreditasi Nasional), Balance Score Card (BSC), Malcolm Baldridge, Knowledge Sharing, Community of Practice (CoP) serta internalisasi Code of Conduct (COC) PLN. Dengan kompetensi terukur melalui akreditasi itu serta didukung attitude sebagai profesional yang ahli, sumber daya manusia kami menjamin konsistensi dan kualitas output sertifikasi yang dihasilkan perusahaan.</p>

        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Keterbukaan Informasi Publik</strong></p>
            <br><p class="lead">Sejak dicanangkannya Undang-undang Nomor 14 Tahun 2008 tentang Keterbukaan Informasi Publik yang mulai berlaku berdasarkan Peraturan Pemerintah Nomor 61 Tahun 2010, mendorong PT PLN (Persero) untuk terus meningkatkan transparansi perusahaan dan membentuk perangkat pelayanan informasi publik.</p>
            <br><p class="lead">PT PLN (Persero) Pusat Sertifikasi menunjuk Manajer Bidang Pengembangan Usaha sebagai Pejabat Pengelola Informasi dan Dokumentasi (PPID).</p>
            <br><p class="lead">Melalui pelayanan informasi publik ini masyarakat bisa mendapatkan informasi informasi yang diperuntukkan bagi kepentingan publik. Pelayanan Informasi Publik PT PLN (Persero) dapat dilihat <a href="http://www.pln.co.id/keterbukaaninformasipublik">disini</a>.</p>

        </div>

        {{--<div class="heading-title heading-border heading-color">--}}
            {{--<p class="lead"><strong>Sertifikasi Sistem Manajemen Mutu, Sistem Manajemen Lingkungan, OHSAS 18001 dan Sistem Manajemen K3</strong></p>--}}
            {{--<p class="lead">Sebagai Lembaga Sertifikasi Sistem Manajemen Mutu, Sistem Manajemen Lingkungan, Sistem Manajemen K3 dan OHSAS 18001. PT. PLN (Persero) Pusat Sertifikasi melaksanakan sertifikasi khususnya bagi internal PLN, namun tidak menutup kemungkinan jika diminta oleh kalangan non-PLN</p>--}}
            {{--<ul class="list-unstyled list-icons">--}}
                {{--<li>--}}
                    {{--<i class="fa fa-check text-success"></i> <p class="lead">Sistem Manajemen Mutu (ISO 9001)</p>--}}
                    {{--<p class="lead">Lembaga Sertifikasi Sistem Manajemen Mutu (LSSM) telah diakreditasi oleh Komite Akreditasi Nasional (KAN) sesuai dengan persyaratan ISO / IEC 17021 dengan nomor akreditasi LSSM-011-IDN</p>--}}
                {{--</li>--}}
                {{--<li><i class="fa fa-check text-success"></i> <p class="lead">Sistem Manajemen Lingkungan (ISO 14001)</p>--}}
                    {{--<p class="lead">Lembaga Sertifikasi Sistem Manajemen Lingkungan (LSSML) telah terakreditasi oleh Komite Akreditasi Nasional (KAN) sesuai dengan persyaratan ISO / IEC 17021 dengan nomor akreditasi LSSM-009-IDN. Ruang lingkup sertifikasi Sistem Manejemen Lingkungan adalah Peralatan Listrik dan Peralatan Optik dan Penyediaan Ketenagalistrikan</p>--}}
                {{--</li>--}}
                {{--<li><i class="fa fa-check text-success"></i> <p class="lead">Sistem Manajemen K3 (OHSAS 18001)</p>--}}
                    {{--<p class="lead">OHSAS 18001 standar internasional yang digunakan untuk manajemen kesehatan dan keselamatan kerja. Sertifikasi OHSAS 18001 penting bagi sebuah organisasi untuk :</p>--}}
                    {{--<ol><p class="lead">- Mengendalikan dan mencegah kondisi bahaya, resiko kesehatan dan keselamatan</p></ol>--}}
                    {{--<ol><p class="lead">- Memenuhi persyaratan dan perundangan yang berkaitan dengan keselamatan kerja</p></ol>--}}
                    {{--<ol><p class="lead">- Perbaikan berkelanjutan kinerja keselamatan dan kesehatan kerja</p></ol>--}}
                    {{--<p class="lead">Ruang lingkup sertifikasi OHSAS 18001 adalah Peralatan listrik dan Peralatan Optik, Penyediaan Ketenagalistrikan, Teknologi Informasi serta Jasa Engineering.</p>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection