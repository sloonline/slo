@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Panduan</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/demo/1200x800/10-min.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="" />
			</a>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/demo/1200x800/3-min.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/3-min.jpg" alt="" />
			</a>
			<a class="lightbox" href="{{ url('/') }}/assets_front/images/demo/1200x800/21-min.jpg" data-plugin-options='{"type":"image"}'>
				<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/21-min.jpg" alt="" />
			</a>
		</div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->
		<p class="dropcap">Aliquam fringilla, sapien eget scelerisque placerat, lorem libero cursus lorem, sed sodales lorem libero eu sapien. Nunc mattis feugiat justo vel faucibus. Nulla consequat feugiat malesuada. Ut justo nulla, <strong>facilisis vel molestie id</strong>, dictum ut arcu. Nunc ipsum nulla, eleifend non blandit quis, luctus quis orci. Cras blandit turpis mattis nulla ultrices interdum. Mauris pretium pretium dictum. Nunc commodo, felis sed dictum bibendum, risus justo iaculis dui, nec euismod orci sem eget neque. Donec in metus metus, vitae eleifend lorem. Ut vestibulum gravida venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque suscipit tincidunt magna non mollis. Fusce tempus tincidunt nisi, in luctus elit pellentesque quis. Sed velit mi, ullamcorper ut tempor ut, mattis eu lacus. Morbi rhoncus aliquet tellus, id accumsan enim sollicitudin vitae.</p>
		<p>Vivamus <a href="#">magna justo</a>, lacinia eget consectetur sed, convallis at tellus. Cras ultricies ligula sed magna dictum porta. Curabitur aliquet quam id dui posuere blandit. Sed porttitor lectus nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Nulla porttitor accumsan tincidunt.</p>
		
		<!-- BLOCKQUOTE -->
		<blockquote>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
			<cite>Source Title</cite>
		</blockquote>

		<p>Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
		<p>Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		<!-- /article content -->


		<div class="divider divider-dotted"><!-- divider --></div>


		<!-- TAGS -->
		<a class="tag" href="#">
			<span class="txt">RESPONSIVE</span>
			<span class="num">12</span>
		</a>
		<a class="tag" href="#">
			<span class="txt">CSS</span>
			<span class="num">3</span>
		</a>
		<a class="tag" href="#">
			<span class="txt">HTML</span>
			<span class="num">1</span>
		</a>
		<a class="tag" href="#">
			<span class="txt">JAVASCRIPT</span>
			<span class="num">28</span>
		</a>
		<a class="tag" href="#">
			<span class="txt">DESIGN</span>
			<span class="num">6</span>
		</a>
		<a class="tag" href="#">
			<span class="txt">DEVELOPMENT</span>
			<span class="num">3</span>
		</a>
		<!-- /TAGS -->


	</div>
</section>
<!-- / -->
@endsection