@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
	@endsection
	
	@section('logo')
	<!-- Logo -->
	<a class="logo pull-left" href="{{url('/')}}#slider">
		<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
	</a>
	@endsection
	
	@section('content')
	<!-- -->
	<section>
		<div class="container">
			
			<div class="heading-title heading-dotted text-center">
				<h2>Sertifikasi Sistem Manajemen Lingkungan</h2>
			</div>
			<!-- OWL SLIDER -->
			<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
				<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
				</a>
				<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
				</a>
				<a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
				</a>
			</div>
			<!-- /OWL SLIDER -->
			
			<!-- IMAGE -->
			<!--
				<figure class="margin-bottom-20">
					<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
				</figure>
			-->
			<!-- /IMAGE -->
			
			<!-- VIDEO -->
			<!--
				<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
				</div>
			-->
			<!-- /VIDEO -->
			
			
			<!-- article content -->
			<div class="divider divider-color divider-center divider-short"><!-- divider -->
				<i class="fa fa-cog"></i>
			</div>
			
			<div class="heading-title heading-border heading-color">
				<p class="dropcap lead">Berbekal pengalaman melakukan sertifikasi sistem manajemen mutu sejak tahun 2003, PT PLN 
				</p>
				<p class="lead">(Persero) Pusat Sertifikasi (LMK Certification) berkomitmen melaksanakan proses sertifikasi secara </p>
				<p class="lead">profesional, independen, mandiri dan menjamin prinsip ketidak berpihakan. dengan teta</p>
				<p class="lead">mengutamakan mutu dan pelayanan berdasarkan standar dan ketentuan yang berlaku sehingga</p>
				<p class="lead">sertifikat yang dihasilkan tepercaya dan mempunyai legitimasi.</p>
				
				<br>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<img src="{{url('assets/global/images/mutu_lingkungan.png')}}">
					</div>
				</div>
				<br>
				
				<br><center><p class="lead">Pertanyaan mengenai proses sertifikasi, keluhan dan banding dapat disampaikan melalui
					<br>niaga@pln-jaser.co.id, +6221 7900034 ext 218, 217 (Miss Attia Ulfa)
					<br><a target="_blank" href="https://pln-jaser.co.id/client/#/login">URL Aplikasi e-Audit</a></p></center>
					
					<br>
					<br>
					
					<br><p class="lead">Dokumen Terkait:</p>
					<br>
					<br><p class="lead">- <a href="{{url('/')}}/upload/FS01-FS01.rar">Formulir aplikasi permohonan (FS01)</a></p>
					<br><p class="lead">- <a href="{{url('/')}}/upload/PP13-PP13.pdf">Prosedur pembekuan, pencabutan dan pengurangan (PP13)</a></p>
					<br><p class="lead">- <a href="{{url('/')}}/upload/PP06-PP06.pdf">Prosedur Banding Perselisihan (PP06)</a></p>
					<br><p class="lead">- <a href="{{url('/')}}/upload/PP07-PP07.pdf">Prosedur Keluhan dan umpan balik pelanggan (PP07)</a></p>
					<br><p class="lead">- <a href="{{url('/')}}/upload/DP03-DP03.pdf">Syarat dan aturan LMK-Certification (DP03)</a></p>
					<br><p class="lead">- <a href="{{url('/')}}/upload/DP11-DP11.pdf">Aturan kegunaan logo IAF (DP11)</a></p>
				</div>
				
				<div class="divider divider-dotted"><!-- divider --></div>
				
				
			</div>
		</section>
		<!-- / -->
		@endsection