@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Sertifikasi Sistem Manajemen OHSAS</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
            </a>
        </div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->
		<div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead">OHSAS 18001 adalah standar internasional yang digunakan untuk manajemen Kesehatan dan Keselamatan Kerja.</p>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead"><strong>Sertifikasi OHSAS 18001 penting bagi sebuah organisasi untuk :</strong></p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Mengendalikan dan mencegah kondisi bahaya, resiko kesehatan dan keselamatan</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Memenuhi persyaratan dan perundangan yang berkaitan dengan keselamatan kerja</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Perbaikan berkelanjutan kinerja keselamatan dan kesehatan kerja</p></li>
            </ul>
        </div>

        <div class="divider divider-color divider-center divider-short"><!-- divider -->
            <i class="fa fa-cog"></i>
        </div>

        <div class="heading-title heading-border heading-color">
            <p class="lead">Lembaga Sertifikasi Sistem Manajemen OHSAS mempunyai ruang lingkup berupa Penyediaan Kelistrikan, Peralatan Listrik, Peralatan Optik, Teknologi Informasi, Jasa Engineering.
                            Dalam melakukan kegiatan sertifikasi, lembaga ini berpegang pada prinsip-prinsip profesionalisme, ketidakberpihakan, bebas dari konflik kepentingan serta menjamin obyektivitas sehingga mampu memberi kepercayaan, keyakinan serta kepuasan bagi pelanggan.</p>
        </div>

		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection