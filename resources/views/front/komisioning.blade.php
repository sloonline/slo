@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
@endsection

@section('logo')
<!-- Logo -->
<a class="logo pull-left" href="{{url('/')}}#slider">
	<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
</a>
@endsection

@section('content')
<!-- -->
<section>
	<div class="container">

		<div class="heading-title heading-dotted text-center">
			<h2>Komisioning / Inspeksi</h2>
		</div>
		<!-- OWL SLIDER -->
		<div class="owl-carousel buttons-autohide controlls-over" data-plugin-options='{"items": 1, "autoPlay": 4500, "autoHeight": false, "navigation": true, "pagination": true, "transitionStyle":"fadeUp", "progressBar":"false"}'>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln2.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln1.jpg" alt="" />
            </a>
            <a class="lightbox" href="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" data-plugin-options='{"type":"image"}'>
                <img class="img-responsive" src="{{ url('/') }}/assets_front/images/pln/content/pusertif/pln3.jpg" alt="" />
            </a>
        </div>
		<!-- /OWL SLIDER -->

		<!-- IMAGE -->
		<!--
		<figure class="margin-bottom-20">
			<img class="img-responsive" src="{{ url('/') }}/assets_front/images/demo/content_slider/10-min.jpg" alt="img" />
		</figure>
		-->
		<!-- /IMAGE -->

		<!-- VIDEO -->
		<!--
		<div class="margin-bottom-20 embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="http://player.vimeo.com/video/8408210" width="800" height="450"></iframe>
		</div>
		-->
		<!-- /VIDEO -->


		<!-- article content -->

        <div class="heading-title heading-border heading-color">
            <p class="dropcap lead">Komisioning atau inspeksi teknik ketenagalistrikan dilaksanakan untuk memastikan kesesuaian instalasi listrik terhadap persyaratan tertentu atau persyaratan umum berdasarkan pembuktian secara profesional. Komisioning / inspeksi teknik dilakukan sejak tahun 1971 terhadap instalasi pembangkit listrik dan jaringan milik PLN maupun non-PLN yang meliputi :</p>
            <ul class="list-unstyled list-icons">
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi pembangkit tenaga listrik yaitu PLTU, PLTGU, PLTMG, PLTP, PLTD dan PLTA</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi jaringan yaitu SUTET, SUTT, GI, SUTM dan GD</p></li>
                <li><i class="fa fa-check text-success"></i> <p class="lead">Peralatan dan instalasi mesin industri yaitu Turbin, Boiler dan Heat Exhanger</p></li>
            </ul>
        </div>


		<div class="divider divider-dotted"><!-- divider --></div>


	</div>
</section>
<!-- / -->
@endsection