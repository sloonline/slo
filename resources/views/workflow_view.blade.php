<hr style="margin-top: 0px !important; margin-bottom: 0px !important;">
<div class="panel-content">
    {!! Form::open(array('url'=> @$workflow['action'], 'files'=> true, 'class'=> 'form-horizontal','id' => 'form-workflow')) !!}
    {!! Form::hidden('model', $data_workflow['model']) !!}
    {!! Form::hidden('modul', $data_workflow['modul']) !!}
    {!! Form::hidden('url_notif', $data_workflow['url_notif']) !!}
    {!! Form::hidden('url_redirect', $data_workflow['url_redirect']) !!}
    <input type="hidden" name="id" value="{{@$workflow['ref_id']}}">
    <ul class="nav nav-tabs nav-primary">
        <li id="tab_approval" class="active"><a href="#approval" data-toggle="tab"><i class="icon-check"></i>
                APPROVAL</a></li>
        <li id="tab_tracking"><a href="#tracking" data-toggle="tab"><i
                        class="fa fa-history"></i> TRACKING</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="approval">
            <div class="col-md-8">
                @if(@$workflow['keterangan'])
                    <fieldset class="cart-summary">
                        <legend>KETERANGAN</legend>
                        {{--@if(@$workflow['penolakan_rab'])--}}
                        {{--<div class="form-group">--}}
                        {{--<p>--}}
                        {{--<label class="col-sm-3 control-label">Surat Penolakan</label>--}}
                        {{--</p>--}}
                        {{--<div class="col-sm-9 prepend-icon">--}}
                        {{--<input type="file" class="form-control form-white" name="surat_tolak">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<p>--}}
                        {{--<label class="col-sm-12 control-label">*) Apabila tidak menyetujui RAB, wajib--}}
                        {{--melampirkan surat penolakan</label>--}}
                        {{--</p>--}}
                        {{--@endif--}}
                        <textarea required name="keterangan" rows="6"
                                  cols="95"></textarea>
                    </fieldset>
                @endif
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <fieldset class="cart-summary">
                        <legend>AKSI</legend>
                        <a onclick="return window.history.back()"
                           class="btn btn-warning btn-square btn-embossed">Kembali</a>
                        @if(@$workflow['btn_reject'])
                            <button type="submit" tipe="reject" id="btn_reject"
                                    name="status" value="{{@$workflow['val_reject']}}"
                                    class="btn btn-danger btn-square btn-embossed">Reject
                            </button>
                        @endif
                        @if(@$workflow['btn_approve'])
                            <button type="submit" tipe="approve" id="btn_approve"
                                    name="status" value="{{@$workflow['val_approve']}}"
                                    class="btn btn-success btn-square btn-embossed">Approve
                            </button>
                        @endif
                        @if(@$workflow['btn_revise'])
                            <button type="submit" name="status" id="btn_revise"
                                    value="{{@$workflow['val_revise']}}" tipe="submit"
                                    class="btn btn-success btn-square btn-embossed">Submit
                            </button>
                        @endif
                        @if(@$workflow['btn_submit'])
                            <button type="submit" name="status" id="btn_submit"
                                    value="{{@$workflow['val_submit']}}" tipe="submit"
                                    class="btn btn-success btn-square btn-embossed">Submit
                            </button>
                        @endif
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="tracking">
            <section id="timeline">
                @foreach(@$workflow['history'] as $row)
                    <div class="timeline-block m-b-0 p-b-0">
                        <div class="timeline-icon bg-primary">
                            <i class="glyphicon glyphicon-send"></i>
                        </div>
                        <div class="timeline-content border-imron m-b-0 p-b-0">
                            <div class="timeline-heading clearfix m-b-0 p-b-0">
                                <div class="pull-right">
                                    <div class="pull-left">
                                        <div class="timeline-day-number">
                                            {{date('d',strtotime(@$row->created_at))}}
                                        </div>
                                    </div>
                                    <div class="pull-left p-r-5">
                                        <div class="timeline-day">{{date('M Y',strtotime(@$row->created_at))}}</div>
                                        <div class="timeline-month c-gray">{{date('H:i:s',strtotime(@$row->created_at))}}</div>
                                    </div>
                                    <a class="btn btn-primary btn-square btn-sm">{{@$row->status}}</a>
                                </div>
                                <p class="article-extract">
                                    {{@$row->keterangan}}
                                </p>
                                <p class="pull-left"><strong>{{@$row->created_by}}</strong></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
        {!! Form::close() !!}
    </div>

    {{--Confirm dialog dipindah kesini agar validasi form terlihat notificationnya--}}
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        var tipe = "";
        $("#form-workflow").submit(function () {
            return confirm("Anda yakin melakukan " + tipe + " ?");
        });
        $("button[type=submit]").click(function () {
            tipe = this.getAttribute('tipe');
        });

        function hideApproval(){
            $("#tab_approval").removeClass('active');
            $("#tab_tracking").addClass('active');
            $("#approval").removeClass('active in');
            $("#tracking").addClass('active in');
            $("#tab_approval").hide();
        }

    </script>