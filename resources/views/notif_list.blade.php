@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
    <div class="header">
        <h2><strong>Notification</strong> List</h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{url('/internal')}}">Dashboard</a></li>
                <li class="active">Nofitication List</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <p class="m-t-10 m-b-20 f-16"></p>
            <div class="panel">
                <div class="panel-header panel-controls bg-primary">
                    <h3><i class="fa fa-table"></i> <strong>Notifikasi</strong></h3>
                </div>
                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Type</th>
                            <th>From</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Time</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $no = 1;
                            ?>
                            @foreach($notif as $row)
                                <tr {{$row->status == "UNREAD" ? "class=success" : ""}}>
                                    <td>@if($row->status == "UNREAD")<strong>{{$no}}</strong> @else {{$no}} @endif</td>
                                    <td><button type="button" class="btn btn-sm btn-icon btn-rounded {{$row->color}}"><i class="{{$row->icon}}"></i></button></td>
                                    <td>@if($row->status == "UNREAD")<strong>SYSTEM</strong> @else SYSTEM @endif</td>
                                    <td><a href="{{$row->url}}" type="button" class="btn btn-sm {{$row->color}}">@if($row->status == "UNREAD")<strong>{{$row->subject}}</strong> @else {{$row->subject}} @endif</a></td>
                                    <td>@if($row->status == "UNREAD")<strong>{!! $row->message !!}</strong> @else {!! $row->message !!} @endif</td>
                                    <td>@if($row->status == "UNREAD")<strong>{{App\HitungMundur::hitung_mundur($row->created_at)}}</strong> @else {{App\HitungMundur::hitung_mundur($row->created_at)}} @endif</td>
                                    <td><button type="button" class="btn btn-sm {{$row->status == "UNREAD" ? "btn-danger" : "btn-success"}}">@if($row->status == "UNREAD")<strong>{{$row->status}}</strong> @else {{$row->status}} @endif</button></td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
<script src="{{ url('/') }}/assets/global/js/pages/mailbox.js"></script>
@endsection