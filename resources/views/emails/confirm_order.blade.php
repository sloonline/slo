<?php
$data->order        = \App\Order::findOrFail($data->order->id);
$data->perusahaan   = \App\Perusahaan::findOrFail($data->perusahaan->id);
?>
<style>
    p {
        font-family: Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 400;
        line-height: 20px;
    }

    @media only screen and (max-width: 640px) {
        body {
            width: auto !important;
        }

        table[class="full-width"] {
            width: 100% !important;
            text-align: center !important;
        }

    }

    @media only screen and (max-width: 479px) {
        body {
            width: auto !important;
        }

        table[class="full-width"] {
            width: 100% !important;
            text-align: center !important;
        }
    }

</style>

<div style="width: 50%">
    <p>*** This is an automatically generated email, please do not reply *** </p>
    <p style="padding-bottom: 1em;"><strong>Yth. {{$data->user->nama_user}},</strong></p>

    @if($data->status == SUBMITTED)
        <p>Dengan ini kami sampaikan bahwa Order Pekerjaan <b>No. {{$data->order->nomor_order}} </b> telah kami terima
            dengan detail sebagai berikut : </p>
    @elseif($data->status == APPROVED)
        <p>Dengan ini kami sampaikan bahwa Order Pekerjaan <b>No. {{$data->order->nomor_order}} </b> telah
            <b>DISETUJUI</b>
            dengan detail sebagai berikut : </p>
    @else
        <p>Dengan ini kami sampaikan bahwa Order Pekerjaan <b>No. {{$data->order->nomor_order}} </b> <b>PERLU
                DIPERBAIKI</b>
            dengan detail sebagai berikut : </p>
    @endif

    <div style="padding-left: 10%;">
        <table border="0" style="padding-bottom: 1em;">
            <tr>
                <td align="left"><p>{{$data->user->jenis_user == 'PLN' ? 'Unit' : 'Nama Perusahaan'}}</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->perusahaan->nama_perusahaan}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Kategori</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->perusahaan->kategori->nama_kategori}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Alamat</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->perusahaan->alamat_perusahaan}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Tanggal Order</p></td>
                <td><p>:</p></td>
                <td><p>{{date('d M Y',strtotime($data->order->created_at))}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Nomor Surat</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->order->nomor_sp}}</p></td>
            </tr>
            <tr>
                <td colspan="3" align="center"><b>Detail Permohonan Pekerjaan</b><br/><br/></td>
            </tr>
            <tr>
                <td align="left" colspan="3">
                    <table class="full-width" align="left" border="1" cellpadding="0" cellspacing="0"
                           style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                        <thead style="background-color: #0a6aa1;">
                        <td align="left"
                            style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            No. Permohonan
                        </td>
                        <td align="left" width="150"
                            style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            Tanggal Permohonan
                        </td>
                        <td align="left" width="150"
                            style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            Jenis Instalasi
                        </td>
                        <td align="left" width="75"
                            style="white-space:nowrap;font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            Jenis Pekerjaan
                        </td>
                        <td align="left" width="100"
                            style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            Jenis Lingkup
                        </td>
                        <td align="left" width="100"
                            style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                            Instalasi
                        </td>
                        </thead>
                        <?php
                        foreach ($data->order->permohonan as $item) { ?>
                        <tr>
                            <td align="left"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ $item->nomor_permohonan}}
                            </td>
                            <td align="left" width="150"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ date('d M Y',strtotime($item->tanggal_permohonan)) }}
                            </td>
                            <td align="left" width="150"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ \App\TipeInstalasi::find($item->id_tipe_instalasi)->nama_instalasi }}
                            </td>
                            <td align="left" width="75"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ $item->produk->produk_layanan }}
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ $item->lingkup_pekerjaan->jenis_lingkup_pekerjaan }}
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                {{ $item->instalasi->nama_instalasi}}
                            </td>
                        </tr>
                        <?php
                        } ?>
                    </table>

                </td>
            </tr>
        </table>
    </div>

    <br>
    <p>Harap menunggu verifikasi order dari kami untuk tahap selanjutnya.</p>
    <p style="padding-bottom: 1em;">Demikian pemberitahuan ini kami sampaikan. Atas perhatiannya kami ucapkan
        terimakasih.</p>

    <p>Salam,<br>
        pusertif.pln.co.id</p>
    <hr>

    <p style="color:#C0C0C0">PT. PLN (Persero)<br>
        Pusat Sertifikasi</p>
</div>