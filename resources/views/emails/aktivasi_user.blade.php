<style>
p {
  font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
  font-size: 14px;
  font-style: normal;
  font-variant: normal;
  font-weight: 400;
  line-height: 20px;
}
</style>

<div style="width: 100%">
  <p>*** This is an automatically generated email, please do not reply *** </p>
  <p style="padding-bottom: 1em;"><strong>Yth. {{$user->nama_user}},</strong></p>

  <p>User Anda telah diaktivasi dengan nomor <strong>{{$user->registerID}}</strong>. Dengan detail sebagai berikut:</p>

  <div style="padding-left: 10%;">
    <table border="0" style="padding-bottom: 1em;">
      <tr>
        <td align="left"><p>Nama Perusahaan</p></td>
        <td><p>:</p></td>
        <td><p>{{$user->nama_perusahaan}}</p></td>
      </tr>
      <tr>
        <td align="left"><p>Kategori Perusahaan</p></td>
        <td><p>:</p></td>
        <td><p>{{$user->kategori_perusahaan}}</p></td>
      </tr>
      <tr>
        <td align="left"><p>Alamat Perusahaan</p></td>
        <td><p>:</p></td>
        <td><p>{{$user->alamat_perusahaan}}</p></td>
      </tr>
      <tr>
        <td align="left"><p>Username</p></td>
        <td><p>:</p></td>
        <td><p>{{$user->email}}</p></td>
      </tr>
      <tr>
        <td align="left"><p>Password</p></td>
        <td><p>:</p></td>
        <td><p>{{(isset($user->password))?  $user->password : ""}}</p></td>
      </tr>
    </table>
  </div>

  <p>Harap segera login menggunakan Username dan Password yang telah tersedia.<br/>
    Untuk keamanan dan privasi, mohon segera ganti password Anda.</p>

    <p>Salam,<br>
      pusertif.pln.co.id</p><hr>

      <p style="color:#C0C0C0">PT. PLN (Persero)<br>
        Pusat Sertifikasi</p>
      </div>
