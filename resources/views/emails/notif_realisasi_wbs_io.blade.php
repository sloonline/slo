<style>
    p {
        font-family: Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 400;
        line-height: 20px;
    }
</style>

<?php
$data->wbs_io = \App\WbsIo::findOrFail($data->wbs_io->id);
$saldo = \App\WbsIo::getColorWarning($data->wbs_io);
$total = $data->wbs_io->realisasi->sum('jumlah');
$persen_realisasi = round(($total / $data->wbs_io->nilai) * 100, 2);
?>
<div style="width: 100%">
    <p style="padding-bottom: 1em;"><strong>Yth. {{$data->user->nama_user}},</strong></p>

    <p>Dengan ini kami sampaikan bahwa {{$data->jenis}} No. {{$data->wbs_io->nomor}} telah mencapai realisasi sebesar
        <b>{{$persen_realisasi}}%</b> , dengan detail sebagai berikut :</p>

    <div style="padding-left: 10%;">
        <table border="0" style="padding-bottom: 1em;">
            <tr>
                <td align="left"><p>Nomor {{$data->jenis}} </p></td>
                <td><p>:</p></td>
                <td colspan="2"><p>{{$data->wbs_io->nomor}}</p></td>
            </tr>
            @if($data->jenis == WBS)
                <tr>
                    <td align="left"><p>Unit</p></td>
                    <td><p>:</p></td>
                    <td colspan="2"><p>{{@$data->wbs_io->businessArea->description}}</p></td>
                </tr>
            @else
                <tr>
                    <td align="left"><p>Perusahaan</p></td>
                    <td><p>:</p></td>
                    <td colspan="2"><p>{{@$data->wbs_io->perusahaan->nama_perusahaan}}</p></td>
                </tr>
            @endif
            <tr>
                <td align="left"><p>Uraian Pekerjaan</p></td>
                <td><p>:</p></td>
                <td colspan="2"><p>{{@$data->wbs_io->uraian_pekerjaan}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Nilai</p></td>
                <td><p>:</p></td>
                <td><p>Rp. </p></td>
                <td style="text-align: right;"><p>{{number_format(@$data->wbs_io->nilai, 2, ',', '.')}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Realisasi</p></td>
                <td><p>:</p></td>
                <td><p>Rp. </p></td>
                <td style="text-align: right;"><p>{{number_format($total, 2, ',', '.')}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Saldo</p></td>
                <td><p>:</p></td>
                <td><p>Rp. </p></td>
                <td style="text-align: right;"><p>{{number_format($saldo['saldo'], 2, ',', '.')}}</p></td>
            </tr>
        </table>
        <div style="padding: 5px 100px 5px 100px; width: 50%; background-color: #3498db; text-align: center; text-decoration: none; font-size: 20px; vertical-align: middle; align-items: center">
            <strong><a href="{{url('/'.$data->notif->url)}}"
                       style="text-decoration:none; color:#ffffff">Review &raquo;</a></strong>
        </div>
    </div>

    <br>
    <p><a href="{{url('/'.$data->notif->url)}}">Klik di sini</a> atau klik tombol 'Review' untuk melihat detail.</p>
    <p style="padding-bottom: 1em;">Demikian pemberitahuan ini kami sampaikan. Atas perhatiannya kami ucapkan
        terimakasih.</p>

    <p>Salam,<br>
        pusertif.pln.co.id</p>
    <hr>

    <p style="color:#C0C0C0">PT. PLN (Persero)<br>
        Pusat Sertifikasi</p>
</div>