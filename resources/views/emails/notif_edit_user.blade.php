<style>
    p {
    	font-family: Segoe UI,Frutiger,Frutiger Linotype,Dejavu Sans,Helvetica Neue,Arial,sans-serif;
    	font-size: 14px;
    	font-style: normal;
    	font-variant: normal;
    	font-weight: 400;
    	line-height: 20px;
    }
</style>

<div style="width: 100%">
    <p style="padding-bottom: 1em;"><strong>Yth. {{$notif->userrole_name}},</strong></p>

    <p>Dengan ini kami sampaikan bahwa terdapat update user peminta jasa dengan rincian sebagai berikut:</p>

    <div style="padding-left: 10%;">
    <table border="0" style="padding-bottom: 1em;">
        <tr>
            <td align="left"><p>{{$notif->jenis_user == 'PLN' ? 'Unit' : 'Nama Perusahaan'}}</p></td>
            <td><p>:</p></td>
            <td><p>{{$notif->nama_perusahaan}}</p></td>
        </tr>
        <tr style="height: 10px;">
            <td style="padding-bottom: 1em;" align="left"><p>{{$notif->jenis_user == 'PLN' ? 'Alamat Unit' : 'Alamat Perusahaan'}}</p></td>
            <td style="padding-bottom: 1em;"><p>:</p></td>
            <td style="padding-bottom: 1em;"><p>{{$notif->alamat_perusahaan}}</p></td>
        </tr>

        <tr>
            <td align="left"><p>Nama User</p></td>
            <td><p>:</p></td>
            <td><p>{{$notif->nama_user}}</p></td>
        </tr>
        <tr>
            <td align="left"><p>Email</p></td>
            <td><p>:</p></td>
            <td><p>{{$notif->username}}</p></td>
        </tr>
    </table>
    </div>

    <br>
    <p style="padding-bottom: 1em;">Demikian pemberitahuan ini kami sampaikan. Atas perhatiannya kami ucapkan terimakasih.</p>

    <p>Salam,<br>
    pusertif.pln.co.id</p><hr>

    <p style="color:#C0C0C0">PT. PLN (Persero)<br>
    Pusat Sertifikasi</p>
</div>