<?php
$data->wbs_io = \App\WbsIo::findOrFail($data->wbs_io->id);
?>
<style>
    p {
        font-family: Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 400;
        line-height: 20px;
    }

    @media only screen and (max-width: 640px) {
        body {
            width: auto !important;
        }

        table[class="full-width"] {
            width: 100% !important;
            text-align: center !important;
        }

    }

    @media only screen and (max-width: 479px) {
        body {
            width: auto !important;
        }

        table[class="full-width"] {
            width: 100% !important;
            text-align: center !important;
        }
    }

</style>


<div style="width: 50%">
    <p>*** This is an automatically generated email, please do not reply *** </p>
    <p style="padding-bottom: 1em;"><strong>Yth. {{@$data->user->nama_user}},</strong></p>

    <p>Dengan ini kami sampaikan bahwa <b>
            status </b> WBS {{(@$data->wbs_io->nomor != "")? "No.".@$data->wbs_io->nomor : ""}} adalah <b>{{@$data->wbs_io->flow_status->status}}</b>
        dengan detail sebagai berikut : </p>

    <div style="padding-left: 10%;">
        <table border="0" style="padding-bottom: 1em;">
            <tr>
                <td align="left"><p>Tipe WBS</p></td>
                <td><p>:</p></td>
                <td><p>{{@$data->wbs_io->tipeWbs->nama_reference}}</p></td>
            </tr>
            @if(@$data->wbs_io->nomor != "")
                <tr>
                    <td align="left"><p>Nomor WBS</p></td>
                    <td><p>:</p></td>
                    <td><p>{{$data->wbs_io->nomor}}</p></td>
                </tr>
            @endif
            <tr>
                <td align="left"><p>Tahun</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->wbs_io->tahun}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Unit</p></td>
                <td><p>:</p></td>
                <td><p>{{$data->wbs_io->businessArea->description}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Jenis Pekerjaan</p></td>
                <td><p>:</p></td>
                <td><p>{{@$data->wbs_io->jenis_pekerjaan->jenis_pekerjaan}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Uraian Pekerjaan</p></td>
                <td><p>:</p></td>
                <td><p>{{@$data->wbs_io->uraian_pekerjaan}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Nilai WBS</p></td>
                <td><p>:</p></td>
                <td><p>Rp. {{number_format(@$data->wbs_io->nilai, 2, ',', '.')}}</p></td>
            </tr>
            <tr>
                <td align="left"><p>Status WBS</p></td>
                <td><p>:</p></td>
                <td><p>{{@$data->wbs_io->statusWbsIo->nama_reference}}</p></td>
            </tr>
            @if(@$data->wbs_io->tipeWbs->nama_reference == TIPE_NORMAL)
                <tr>
                    <td align="left"><p>Nomor Kontrak</p></td>
                    <td><p>:</p></td>
                    <td><p>{{@$data->wbs_io->kontrak->latest_kontrak->nomor_kontrak}}</p></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><br/><br/><b>Data RAB</b><br/><br/></td>
                </tr>
                <tr>
                    <td align="left" colspan="3">
                        <table class="full-width" align="left" border="1" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                            <thead style="background-color: #0a6aa1;">
                            <td align="left"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                No. Rab
                            </td>
                            <td align="left"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                No. Order
                            </td>
                            <td align="left" width="150"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Tanggal RAB
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Total Biaya
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Peminta Jasa
                            </td>
                            </thead>
                            @foreach (@$data->wbs_io->rab as $item)
                                <tr>
                                    <td align="left"
                                        style="white-space: nowrap;font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->rab->no_dokumen}}
                                    </td>
                                    <td align="left"
                                        style="white-space: nowrap;font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->rab->order->nomor_order}}
                                    </td>
                                    <td align="left" width="150"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ date('d M Y',strtotime(@$item->rab->created_at)) }}
                                    </td>
                                    <td align="right" width="150"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        Rp. {{number_format(@$item->rab->total_biaya, 2, ',', '.')}}
                                    </td>
                                    <td align="left"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->rab->order->user->username}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </td>
                </tr>
            @else
                <tr>
                    <td align="left"><p>Nomor Order</p></td>
                    <td><p>:</p></td>
                    <td><p>{{@$data->wbs_io->order->nomor_order}}</p></td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><br/><br/><b>Data Permohonan</b><br/><br/></td>
                </tr>
                <tr>
                    <td align="left" colspan="3">
                        <table class="full-width" align="left" border="1" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                            <thead style="background-color: #0a6aa1;">
                            <td align="left"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                No. Permohonan
                            </td>
                            <td align="left" width="150"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Tanggal Permohonan
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Nama Instalasi
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Jenis Instalasi
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Jenis Pekerjaan
                            </td>
                            <td align="left" width="100"
                                style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; color: white; line-height: 24px;padding: 5px;text-align: center;">
                                Jenis Lingkup
                            </td>
                            </thead>
                            @foreach (@$data->wbs_io->permohonan as $item)
                                <tr>
                                    <td align="left"
                                        style="white-space: nowrap;font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->permohonan->nomor_permohonan}}
                                    </td>
                                    <td align="left" width="150"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ date('d M Y',strtotime(@$item->permohonan->tanggal_permohonan)) }}
                                    </td>
                                    <td align="left"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->permohonan->instalasi->nama_instalasi}}
                                    </td>
                                    <td align="left"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->permohonan->tipeInstalasi->nama_instalasi}}
                                    </td>
                                    <td align="left"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->permohonan->produk->produk_layanan}}
                                    </td>
                                    <td align="left"
                                        style="font-family: 'Open Sans', sans-serif; font-size: 12px; font-weight: 400; line-height: 24px;padding: 5px;">
                                        {{ @$item->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}
                                        @if(@$item->permohonan->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                            <br/>
                                            @foreach(@$item->permohonan->bayPermohonan as $bg)
                                                - {{@$bg->bayGardu->nama_bay}}<br/>
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </td>
                </tr>
            @endif
            <tr>
                <td colspan="3" align="center">
                    <div style="padding: 5px;background-color: #3498db; text-align: center; text-decoration: none; font-size: 20px; vertical-align: middle; align-items: center">
                        <strong><a href="{{url('/'.@$data->notif->url)}}"
                                   style="text-decoration:none; color:#ffffff">Review &raquo;</a></strong>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <br>
    <p><a href="{{url('/'.@$data->notif->url)}}">Klik di sini</a> atau klik tombol 'Review' untuk melakukan verifikasi
        order
        pekerjaan di atas.</p>
    <p style="padding-bottom: 1em;">Demikian pemberitahuan ini kami sampaikan. Atas perhatiannya kami ucapkan
        terimakasih.</p>

    <p>Salam,<br>
        pusertif.pln.co.id</p>
    <hr>

    <p style="color:#C0C0C0">PT. PLN (Persero)<br>
        Pusat Sertifikasi</p>
</div>