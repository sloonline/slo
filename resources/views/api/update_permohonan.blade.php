@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Permohonan <strong>Nomor Agenda</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Peralatan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    {!! Form::open(['url'=>'internal/api/saveUpdatePermohonan', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
                    <input type="hidden" value="{{$djk_agenda->id}}" name="djk_agenda_id">
                    <div class="panel-header bg-primary">
                        <h3><i class="icon-bulb"></i> Form <strong>Update Status</strong></h3>

                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Nomor Agenda</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <input name="no_agenda" value="{{$djk_agenda->no_agenda}}"
                                           class="form form-control disable" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Status</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <select name="status" class="form-control form-white">
                                        <option value="">-- Pilih Status --</option>
                                        @if($status_djk != null)
                                            @foreach($status_djk as $item)
                                                <option {{(@$djk_agenda->status_id == $item->id)? 'selected':''}} value="{{@$item->id}}">{{@$item->nama_reference}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Keterangan</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <textarea name="keterangan" class="form-control form-white"
                                              placeholder="Keterangan (Optional)"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-content">
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne">
                                            <i class="icon-plus"></i> Tracking
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <section id="timeline">
                                        @foreach(@$djk_agenda->history as $row)
                                            <div class="timeline-block m-b-0 p-b-0">
                                                <div class="timeline-icon bg-primary">
                                                    <i class="glyphicon glyphicon-send"></i>
                                                </div>
                                                <div class="timeline-content border-imron m-b-0 p-b-0">
                                                    <div class="timeline-heading clearfix m-b-0 p-b-0">
                                                        <div class="pull-right">
                                                            <div class="pull-left">
                                                                <div class="timeline-day-number">
                                                                    {{date('d',strtotime(@$row->created_at))}}
                                                                </div>
                                                            </div>
                                                            <div class="pull-left p-r-5">
                                                                <div class="timeline-day">{{date('M Y',strtotime(@$row->created_at))}}</div>
                                                                <div class="timeline-month c-gray">{{date('H:i:s',strtotime(@$row->created_at))}}</div>
                                                            </div>
                                                        </div>
                                                        <p class="pull-left"><strong>{{@$row->user->nama_user}}</strong>
                                                        </p>
                                                        <br>
                                                        <p>
                                                            <button type="button"
                                                                    class="btn btn-primary btn-square btn-sm">{{@$row->status->nama_reference}}</button>
                                                        </p>
                                                        <p>
                                                            {{@$row->keterangan}}
                                                        </p>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i
                                        class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')

            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
@endsection
