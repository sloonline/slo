@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Permohonan <strong>Nomor Agenda</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">API</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    {!! Form::open(['url'=>'internal/api/savePermohonan', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="icon-bulb"></i> Form <strong>Permohonan - DJK</strong></h3>

                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12 prepend-icon">
                                    <input type="text" required
                                           name="tanggal"
                                           class="form-control b-datepicker form-white"
                                           data-date-format="yyyy-mm-dd"
                                           data-lang="en"
                                           data-RTL="false"
                                           placeholder="Tanggal">
                                    <i class="icon-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Tipe Instalasi</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <select required name="tipe_instalasi" class="form-control form-white"
                                            id="tipe_instalasi">
                                        @foreach($tipe_instalasi as $row)
                                            <option value="{{$row->id}}">{{$row->nama_instalasi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="list_kit">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Instalasi Pembangkit</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <select name="pembangkit_id" class="form-control form-white" id="pembangkit" data-search='true'>
                                            @foreach($pembangkit as $kit)
                                                <option data-jenis_instalasi="{{$kit->jenis_instalasi}}"
                                                        data-pemilik="{{$kit->pemilik}}" data-instalasi="{{$kit}}"
                                                        value="{{$kit->id}}">{{$kit->nama_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="list_trans" style="display:none">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Instalasi Transmisi</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <select name="transmisi_id" class="form-control form-white" id="transmisi"
                                                data-search="true">
                                            @foreach($transmisi as $trans)
                                                <option data-jenis_instalasi="{{$trans->jenis_instalasi}}"
                                                        data-pemilik="{{$trans->pemilik}}" data-instalasi="{{$trans}}"
                                                        value="{{$trans->id}}">{{$trans->nama_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="list_dist" style="display:none">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Instalasi Distribusi</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <select name="distribusi_id" class="form-control form-white" id="distribusi"
                                                data-search="true">
                                            @foreach($distribusi as $dist)
                                                <option data-jenis_instalasi="{{$dist->jenis_instalasi}}"
                                                        data-pemilik="{{$dist->pemilik}}" data-instalasi="{{$dist}}"
                                                        value="{{$dist->id}}">{{$dist->nama_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="list_pmtm" style="display:none">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Instalasi Pemanfaatan TM</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <select name="pemanfaatan_tm_id" class="form-control form-white" id="pemanfaatan_tm"
                                                data-search="true">
                                            @foreach($pemanfaatan_tm as $tm)
                                                <option data-jenis_instalasi="{{$tm->jenis_instalasi}}"
                                                        data-pemilik="{{$tm->pemilik}}" data-instalasi="{{$tm}}"
                                                        value="{{$tm->id}}">{{$tm->nama_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="list_pmtt" style="display:none">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Instalasi Pemanfaatan TT</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <select name="pemanfaatan_tt_id" class="form-control form-white" id="pemanfaatan_tt"
                                                data-search="true">
                                            @foreach($pemanfaatan_tt as $tt)
                                                <option data-jenis_instalasi="{{$tt->jenis_instalasi}}"
                                                        data-pemilik="{{$tt->pemilik}}" data-instalasi="{{$tt}}"
                                                        value="{{$tt->id}}">{{$tt->nama_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a id="ref_detail" target="_blank" class="btn btn-primary btn-square btn-embossed"><i
                                            class="fa fa-info-circle"></i>Detail Instalasi</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Jenis Instalasi</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <input type="text" required class="form-control" name="jenis_instalasi"
                                           placeholder="Jenis Instalasi"
                                           id="jenis_instalasi" readonly/>
                                    <input type="hidden" class="form-control" name="jenis_instalasi_id"
                                           id="jenis_instalasi_id"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label class="col-sm-12 control-label">Lokasi</label>
                            </div>
                            <div class="col-md-4">
                                <div class="col-sm-12">
                                    <input required class="form-control" placeholder="Lokasi Instalasi"
                                           id="lokasi_instalasi" readonly>
                                </div>
                            </div>
                        </div>

                        <fieldset class="cart-summary">
                            <legend>Detail Pemilik</legend>
                            <div class="form-group" id="list_kit">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Nama</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <input class="form-control" placeholder="Nama Pemilik" id="nama_pemilik"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="list_kit">
                                <div class="col-sm-2">
                                    <label class="col-sm-12 control-label">Alamat</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <input required class="form-control" placeholder="Alamat Pemilik"
                                               id="alamat_pemilik" readonly>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i
                                        class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    onclick="return confirm('Simpan dan Request No Agenda ke DJK ?')"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script>
                $(function () {
                    set_data('pembangkit');
                });

                $('#tipe_instalasi').change(function () {
                    console.log('tipe inslasi changed');

                    if ($('#tipe_instalasi').val() == "{{ID_PEMBANGKIT}}") {
                        hideAll('list_kit');
                        set_data('pembangkit');
                    }else if ($('#tipe_instalasi').val() == "{{ID_TRANSMISI}}") {
                        hideAll('list_trans');
                        set_data('transmisi');
                    }else if ($('#tipe_instalasi').val() == "{{ID_DISTRIBUSI}}") {
                        hideAll('list_dist');
                        set_data('distribusi');
                    }else if ($('#tipe_instalasi').val() == "{{ID_PEMANFAATAN_TM}}") {
                        hideAll('list_pmtm');
                        set_data('pemanfaatan_tm');
                    }else if ($('#tipe_instalasi').val() == "{{ID_PEMANFAATAN_TT}}") {
                        hideAll('list_pmtt');
                        set_data('pemanfaatan_tt');
                    }
                });

                function hideAll(list_show) {
                    $('#list_kit').hide();
                    $('#list_trans').hide();
                    $('#list_dist').hide();
                    $('#list_pmtm').hide();
                    $('#list_pmtt').hide();
                    $('#' + list_show).show();
                }

                $('#pembangkit').change(function () {
                    console.log('pembangkit changed');
                    set_data('pembangkit');
                });

                $('#transmisi').change(function () {
                    console.log('transmisi changed');
                    set_data('transmisi');
                });

                $('#distribusi').change(function () {
                    set_data('distribusi');
                });

                $('#pemanfaatan_tm').change(function () {
                    set_data('pemanfaatan_tm');
                });

                $('#pemanfaatan_tt').change(function () {
                    set_data('pemanfaatan_tt');
                });

                function set_data(param) {
                    //get data instalasi dan pemilik
                    var pemilik = $('#' + param).find(":selected").data("pemilik");
                    console.log(pemilik);
                    var instalasi = $('#' + param).find(":selected").data("instalasi");
                    var jenis_instalasi = $('#' + param).find(":selected").data("jenis_instalasi");

                    //set data pemilik pada tampilan
                    $('#nama_pemilik').val(pemilik['nama_pemilik']);
                    $('#alamat_pemilik').val((pemilik['alamat_pemilik'] == null) ? "-" : pemilik['alamat_pemilik']);
                    if (jenis_instalasi != "") {
                        $('#jenis_instalasi').val(jenis_instalasi['kode_instalasi'] + " : " + jenis_instalasi['jenis_instalasi']);
                        $('#jenis_instalasi_id').val(jenis_instalasi['id']);
                    }

                    //set lokasi insalasi
                    $('#lokasi_instalasi').val((instalasi['alamat_instalasi'] == null) ? "-" : instalasi['alamat_instalasi']);

                    //set url button detail instalasi
                    var url = "/eksternal/instalasi-" + param + "/" + instalasi['id'];

                    var selected = $('#tipe_instalasi').val();
                    var url = '#';
                    var instalasi_id = instalasi['id'];
                    if(selected == '{{ID_PEMBANGKIT}}'){
                        url = '{{url("/internal/instalasi_pembangkit/")}}/' + instalasi_id;
                    }else if(selected == '{{ID_TRANSMISI}}'){
                        instalasi_id = (instalasi['transmisi_id'] > 0) ? instalasi['transmisi_id'] : instalasi['id'];
                        url = '{{url("/internal/instalasi_transmisi/")}}/' + instalasi_id;
                    }else if(selected == '{{ID_DISTRIBUSI}}'){
                        url = '{{url("/internal/instalasi_distribusi/")}}/' + instalasi_id;
                    }else if(selected == '{{ID_PEMANFAATAN_TM}}'){
                        url = '{{url("/internal/instalasi_pemanfaatan_tm/")}}/' + instalasi_id;
                    }else if(selected == '{{ID_PEMANFAATAN_TT}}'){
                        url = '{{url("/internal/instalasi_pemanfaatan_tt/")}}/' + instalasi_id;
                    }
                    $("#ref_detail").attr("href", url);
                }
            </script>
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
@endsection
			