@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>Detail <strong>Permohonan</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li class="active">API</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<div class="panel">
				{!! Form::open(['url'=>'#', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Data <strong>Detail</strong></h3>
				</div>
				<div class="panel-content">
					<fieldset class="cart-summary">
						<legend>Data Registrasi</legend>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Agenda</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->no_agenda}}" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Permohonan</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->no_permohonan}}" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Registrasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->no_registrasi}}" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor LHPP</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor SLO</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						
					</fieldset>
					
					<fieldset class="cart-summary">
						<legend>Data Instalasi</legend>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nama Instalasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->instalasi->nama_instalasi}}" class="form form-control disable" readonly>
								</div>
							</div>
							<div class="col-md-4">
								<a href="{{'/eksternal/instalasi-'.$tipe_instalasi.'/'.$permohonan->instalasi_id}}" class="btn btn-info"><i class="fa fa-info-circle"></i>Detail Instalasi</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Tipe Instalasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->tipeInstalasi->nama_instalasi}}" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Pemilik Instalasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<input name="no_agenda" value="{{$permohonan->instalasi->pemilik->nama_pemilik}}" class="form form-control disable" readonly>
								</div>
							</div>
						</div>
					</fieldset>
					@if($permohonan->alasan_penolakan != null)
					<fieldset class="cart-summary">
						<legend>Alasan Penolakan</legend>
						<div class="form-group">
							<div class="col-sm-8">
								<textarea class="form-control">{{$permohonan->alasan_penolakan}}</textarea>
							</div>
						</div>
					</fieldset>
					@endif
				</div>
				<hr/>
				<div class="panel-footer clearfix bg-white">
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endsection
	
	@section('page_script')
	
	<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
	<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
	<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	@endsection
	