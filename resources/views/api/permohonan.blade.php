@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet" >
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Permohonan - DJK</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">API</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert {{Session::get('alert_type')}}">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('internal/api/createPermohonan')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Permohonan Baru</a>
                            </div>
                        </div>
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Agenda</th>
                                <th>Nama Instalasi</th>
                                <th>Tipe Instalasi</th>
                                <th>Status</th>
                                <th style="width:15%">Aksi</th>
                            </tr>
                            </thead>
                            <?php
                            $no = 1;
                            ?>
                            <tbody>
                            @foreach($djk_agenda as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$item->no_agenda}}</td>
                                    <td>{{@$item->instalasi->nama_instalasi}}</td>
                                    <td>{{@$item->tipeInstalasi->nama_instalasi}}</td>
                                    <td>{{(@$item->status_id == null) ? 'Inisiasi': @$item->status->nama_reference }}</td>
                                    <td>
                                        <a href="{{url('internal/api/updatePermohonan/'.$item->id)}}"
                                           class="btn btn-sm btn-success btn-square btn-embossed"><i
                                                    class="fa fa-pencil"
                                                    data-rel="tooltip"
                                                    data-placement="right" title="Update Status"></i></a>

                                        {{--<a href="{{url('internal/api/pengajuanSlo/'.$item->id)}}"--}}
                                        {{--class="btn btn-sm btn-success btn-square btn-embossed"><i--}}
                                        {{--class="fa fa-file-text"--}}
                                        {{--data-rel="tooltip"--}}
                                        {{--data-placement="right" title="Pengajuan SLO"></i></a>--}}
                                        {{--@if(@$item->hasil_registrasi != null)--}}
                                        {{--<a href="{{url('internal/api/showDetail/'.$item->id)}}"--}}
                                        {{--class="btn btn-sm btn-info btn-square btn-embossed"><i--}}
                                        {{--class="fa fa-info-circle"--}}
                                        {{--data-rel="tooltip"--}}
                                        {{--data-placement="right" title="Hasil Registrasi"></i></a>--}}
                                        {{--@endif--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->

            <script>
                $(document).ready(function () {
                    var oTable = $('#my_table').dataTable();
                });
            </script>
@endsection
            