@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>Permohonan <strong>SLO Pembangkit</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li class="active">API</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<div class="panel">
				{!! Form::open(['url'=>'internal/api/saveSloKit', 'class'=>'form-horizontal', 'role'=>'form', 'enctype'=>"multipart/form-data"]) !!}
				<input type="hidden" name="permohonan_djk_id" value="{{$permohonan_djk->id}}">
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>Permohonan - DJK</strong></h3>
					
				</div>
				<div class="panel-content">
					<fieldset class="cart-summary">
						<legend>SECTION I</legend>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Agenda</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_agenda"
									class="form-control form-white"
									value="{{$permohonan_djk->no_agenda}}">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor LHPP</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_lhpp"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor SLO</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_slo"
									value="{{@$permohonan_djk->no_slo}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset class="cart-summary">
						<legend>SECTION II</legend>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Kode SBU</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<select name="kode_sbu" class="form-control" data-search="true">
										@foreach($master_sbu as $sbu)
										<option value="{{$sbu->kode}}">{{$sbu->nama}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">NIK PJT</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<select name="nik_pjt" class="form-control" data-search="true">
										@foreach($master_pjt as $pjt)
										<option value="{{$pjt->nik}}">{{$pjt->nama}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">NIK TT</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12">
									<select name="nik_tt" class="form-control" data-search="true">
										@foreach($master_tt as $tt)
										<option value="{{$tt->nik}}">{{$tt->nama}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset class="cart-summary">
						<legend>SECTION III</legend>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">URL Detail LHPP</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="file"
										name="url_detail_lhpp"
										class="form-control form-white"
										placeholder="File Landasan">
									<i class="glyphicon glyphicon-paperclip"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">URL Koordinat Lokasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="url_koordinat_lokasi" value="{{url('/eksternal/instalasi-pembangkit/'.$instalasi->id)}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">URL Foto Pelaksanaan</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
										<input type="file"
										name="url_foto_pelaksanaan"
										class="form-control form-white"
										placeholder="File Landasan">
									<i class="glyphicon glyphicon-paperclip"></i>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">URL File Konsumsi Bahan Bakar</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="file"
										name="url_file_konsumsi_bahan_bakar"
										class="form-control form-white"
										placeholder="File Landasan">
									<i class="glyphicon glyphicon-paperclip"></i>
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset class="cart-summary">
						<legend>SECTION IV</legend>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Jenis Instalasi</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required" readonly
										   class="form-control" value="{{$instalasi->jenis_instalasi->jenis_instalasi}}">
									<input name="jenis_instalasi" type="hidden" value="{{$instalasi->jenis_instalasi->kode_instalasi}}">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Longitude</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="longitude" value="{{@$instalasi->longitude}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Latitude</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="latitude" value="{{@$instalasi->latitude}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Kapasitas Terpasang</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="kapasitas_terpasang"
									value="{{$instalasi->kapasitas_terpasang}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Kapasitas Hasil Uji</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="kapasitas_hasil_uji"
									value="{{$instalasi->kapasitas_hasil_uji}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Unit Pembangkit</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_unit_pembangkit"
									value="{{$instalasi->nomor_pembangkit}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Seri Generator</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_seri_generator"
									value="{{$instalasi->nomor_generator}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Nomor Seri Turbin</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="no_seri_turbin"
									value="{{$instalasi->nomor_turbin}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Kapasitas Modul Per Unit</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="kapasitas_modul_per_unit"
									value="{{$instalasi->kapasitas_modul}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Kapasitas Inverter Per Unit</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="kapasitas_inverter_per_unit"
									value="{{$instalasi->kapasitas_inverter}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Jumlah Modul</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="jumlah_modul"
									value="{{$instalasi->jumlah_modul}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Jumlah Inverter</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="jumlah_inverter"
									value="{{$instalasi->jumlah_inverter}}"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Bahan Bakar</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									class="form-control form-white" value="{{$instalasi->bahanBakar->nama_reference}}">
									<input name="bahan_bakar" type="hidden" value="{{$instalasi->bahanBakar->value}}">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">Net Plant Heat Rate HHV</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="net_plant_heat_rate_hhv"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-2">
								<label class="col-sm-12 control-label">SFC</label>
							</div>
							<div class="col-md-4">
								<div class="col-sm-12 prepend-icon">
									<input type="text" required="required"
									name="sfc"
									class="form-control form-white">
									<i class="icon-calendar"></i>
								</div>
							</div>
						</div>
					</fieldset>
					
					<hr/>
					<div class="panel-footer clearfix bg-white">
						<div class="pull-right">
							<a href="" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
							<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		@endsection
		
		@section('page_script')
		<script>
			
		</script>
		<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
		<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
		<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
		<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
		<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		@endsection
		