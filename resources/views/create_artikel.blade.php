@extends('layout')

@section('content')

    {!! Form::open(['url'=>'artikel/create','post']) !!}
        Judul : {!! Form::text('judul', null, ['class'=>'form-control']) !!}
        @if ($errors->has('judul')) <p class="help-block">{{ $errors->first('judul') }}</p> @endif<br>
        Lokasi : {!! Form::text('lokasi', null, ['class'=>'form-control']) !!}
        @if ($errors->has('lokasi')) <p class="help-block">{{ $errors->first('lokasi') }}</p> @endif<br>
        Views : {!! Form::text('views', null, ['class'=>'form-control', 'readonly'=>'']) !!}<br>
        Isi : {!! Form::textarea('isi', null, ['class'=>'form-control']) !!}
        @if ($errors->has('isi')) <p class="help-block">{{ $errors->first('isi') }}</p> @endif<br>
        {!! Form::submit('Save') !!}<br>
    {!! Form::close() !!}

@stop