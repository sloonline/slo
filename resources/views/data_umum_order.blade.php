@extends('layout/layout_eksternal')

@section('page_css')
<!-- BEGIN PAGE STYLE -->
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/rateit/rateit.css" rel="stylesheet">
<!-- END PAGE STYLE -->
@endsection

@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="header">
		<h2>Data <strong>Umum Order</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/eksternal')}}">Dashboard</a></li>
				<li><a href="#">Order</a></li>
				<li class="active">Data Umum</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<div class="panel">
				{!! Form::open(array('url'=> 'transmisi', 'files'=> true, 'class'=> 'form-horizontal')) !!}
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>Input</strong></h3>
				</div>
				<div class="panel-content">
					<div class="row">
						<div class="col-md-12">
							<div class="panel-group panel-accordion dark-accordion" id="accordion2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>
											<a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
												Data Umum Order
											</a>
										</h4>
									</div>
									<div id="collapseOne2" class="panel-collapse collapse in">
										<div class="panel-body bg-white">
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Nomor Order</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<h1>122016010001</h1>
													<input type="hidden" name="no_order">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Nama Perusahaan</label>
												</div>
												<div class="col-sm-6 prepend-icon">
													<input name="nama_perusahaan" type="text" class="form-control form-white" placeholder="Nama Perusahaan" required>
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Alamat Perusahaan</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<textarea name="alamat_perusahaan" class="form-control form-white" placeholder="Alamat" required></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Kategori Perusahaan</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="kategori_perusahaan" type="text" class="form-control form-white" placeholder="Kategori" required>
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Nomor Surat Permintaan</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="no_surat_permintaan" type="text" class="form-control form-white" placeholder="Nomor Surat" required/>
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Tanggal Surat Permintaan</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="tgl_surat_permintaan" type="text" class="date-picker form-control form-white" placeholder="Tanggal" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">File Surat Permintaan</label>
												</div>
												<div class="col-sm-9">
													<div class="file">
														<div class="option-group">
															<span class="file-button btn-primary">Choose File</span>
															<input type="file" class="custom-file" name="file_surat_permintaan" onchange="document.getElementById('uploader').value = this.value;" required>
															<input type="text" class="form-control form-white" id="uploader" placeholder="no file selected" readonly="">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2">
												Data Pemilik Instalasi
											</a>
										</h4>
									</div>
									<div id="collapseTwo2" class="panel-collapse collapse">
										<div class="panel-body bg-white">
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Nama Pemilik</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="nama_pemilik_instalasi" type="text" class="form-control form-white" placeholder="Nama Pemilik" required>
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Alamat</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<textarea name="alamat_pemilik_instalasi" class="form-control form-white" placeholder="Alamat Pemilik" required></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Negara</label>
												</div>
												<div class="col-sm-9">
													<select name="negara_pemilik_instalasi" class="form-control form-white" data-style="white" data-placeholder="Select a country...">
														<option value="1">Indonesia</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Provinsi</label>
												</div>
												<div class="col-sm-9">
													<select name="provinsi_pemilik_instalasi" class="form-control form-white" data-style="white" data-placeholder="Select a country...">
														<option value="1">DKI Jakarta</option>
														<option value="2">Jawa Barat</option>
														<option value="3">Jawa Timur</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Kabupaten</label>
												</div>
												<div class="col-sm-9">
													<select name="kabupaten_pemilik_instalasi" class="form-control form-white" data-style="white" data-placeholder="Select a country...">
														<option value="1">Kabupaten Bandung</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Kode Pos</label>
												</div>
												<div class="col-sm-9">
													<input name="kode_pos" type="text" class="form-control form-white" placeholder="Kode Pos" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Telepon</label>
												</div>
												<div class="col-sm-9">
													<input name="telepon" type="text" class="form-control form-white" placeholder="No Telepon" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Fax</label>
												</div>
												<div class="col-sm-9">
													<input name="no_fax" type="text" class="form-control form-white" placeholder="No Fax">
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label class="col-sm-12 control-label">Email</label>
												</div>
												<div class="col-sm-9">
													<input name="email_pemilik_instalasi" type="email" class="form-control form-white" placeholder="Email" required >
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4>
											<a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2">
												Data Umum Permohonan
											</a>
										</h4>
									</div>
									<div id="collapseThree2" class="panel-collapse collapse">
										<div class="panel-body bg-white">
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Tipe Instalasi</label>
												</div>
												<div class="col-sm-8">
													<select name="tipe_instalasi" class="form-control form-white" data-style="white" >
														<option value="1">Pembangkit</option>
														<option value="2">Transmisi</option>
														<option value="3">Gardu Induk</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Jenis Pekerjaan</label>
												</div>
												<div class="col-sm-8">
													<select id="jenis_pekerjaan" name="jenis_pekerjaan" class="form-control form-white" data-style="white" >
														<option value="">--Pilih--</option>
														<option value="1">Supervisi Komisioning</option>
														<option value="2">Rekomendasi Teknik Laik Bertegangan</option>
														<option value="2">Rekomendasi Teknik Laik Sinkron</option>
														<option value="2">SLO</option>
														<option value="2">Re-SLO</option>
														<option value="2">Supervisi NDC</option>
														<option value="2">Supervisi Performance</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
												</div>
												<div class="col-sm-8">
													<div class="icheck-list">
														<label id="a" style="display:none">
															<input id="a1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Supervisi Komisioning 
														</label>
														<label id="b" >
															<input id="b1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Rekomendasi Teknik Laik Operasi
														</label>
														<label id="c" style="display:none" >
															<input id="c1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Rekomendasi Teknik Laik Sinkron
														</label>
														<label id="d" style="display:none" >
															<input id="d1" type="checkbox" data-checkbox="icheckbox_flat-blue"> SLO
														</label>
														<label id="e" style="display:none" >
															<input id="e1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Re-SLO
														</label>
														<label id="f" style="display:none" >
															<input id="f1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Supervisi NDC
														</label>
														<label id="g" style="display:none" >
															<input id="g1" type="checkbox" data-checkbox="icheckbox_flat-blue"> Supervisi Performance Test
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Jenis Lingkup Pekerjaan</label>
												</div>
												<div class="col-sm-8">
													<select name="jenis_lingkup_pekerjaan" class="form-control form-white" data-style="white" >
														<option value="1">PLTA</option>
														<option value="2">PLTG</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
												</div>
												<div class="col-sm-8">
													<select name="jenis_ijin_usaha" class="form-control form-white" data-style="white" >
														<option value="1">IUPTL</option>
														<option value="2">IO</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
												</div>
												<div class="col-sm-8">
													<input name="penerbit_ijin_usaha" type="text" class="form-control form-white" placeholder="Penerbit Ijin Usaha" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nomor Ijin Usaha</label>
												</div>
												<div class="col-sm-8">
													<input name="no_ijin_usaha" type="text" class="form-control form-white" placeholder="Nomor Ijin Usaha" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3 prepend-icon">
													<label class="col-sm-12 control-label">Masa Berlaku Ijin Usaha</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="masa_berlaku_ijin_usaha" type="text" class="date-picker form-control form-white" placeholder="Masa Berlaku Ijin Usaha" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">File Ijin Usaha</label>
												</div>
												<div class="col-sm-8">
													<div class="file">
														<div class="option-group">
															<span class="file-button btn-primary">Choose File</span>
															<input type="file" class="custom-file" name="file_ijin_usaha" onchange="document.getElementById('uploader').value = this.value;" required>
															<input type="text" class="form-control form-white" id="uploader" placeholder="no file selected" readonly="">
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nama/Judul Kontrak Sewa</label>
												</div>
												<div class="col-sm-8">
													<input name="judul_kontrak_sewa" type="text" class="form-control form-white" placeholder="Judul Kontrak Sewa" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nomor Kontrak Sewa</label>
												</div>
												<div class="col-sm-8">
													<input name="no_kontrak_sewa" type="text" class="form-control form-white" placeholder="Nomor Kontrak Sewa" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Tanggal Pengesahan Kontrak Sewa</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="tgl_pengesahaan_kontrak_sewa" type="text" class="date-picker form-control form-white" placeholder="Tanggal Pengesahaan Kontrak Sewa" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Masa Berlaku Sewa</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="masa_berlaku_sewa" type="text" class="date-picker form-control form-white" placeholder="Masa Berlaku Sewa" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">File Cover Kontrak Sewa</label>
												</div>
												<div class="col-sm-8">
													<div class="file">
														<div class="option-group">
															<span class="file-button btn-primary">Choose File</span>
															<input type="file" class="custom-file" name="file_cover_kontrak_sewa" onchange="document.getElementById('uploader').value = this.value;" required>
															<input type="text" class="form-control form-white" id="uploader" placeholder="no file selected" readonly="">
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nomor Surat Perjanjian Jual Beli Tenaga Listrik</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="no_surat_pjbtl" type="text" class="form-control form-white" placeholder="Nomor Surat">
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Tanggal Surat Perjanjian Jual Beli Tenaga Listrik</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="tgl_surat_pjbtl" type="text" class="date-picker form-control form-white" placeholder="Tanggal Surat PJBTL" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Masa Berlaku  Surat Perjanjian Jual Beli Tenaga Listrik</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="masa_berlaku_surat_pjbtl" type="text" class="date-picker form-control form-white" placeholder="Masa Berlaku Surat PJBTL" required>
													<i class="icon-calendar"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">File Cover Surat Perjanjian Jual Beli Tenaga Listrik</label>
												</div>
												<div class="col-sm-8">
													<div class="file">
														<div class="option-group">
															<span class="file-button btn-primary">Choose File</span>
															<input type="file" class="custom-file" name="file_cover_surat_pjbtl" onchange="document.getElementById('uploader').value = this.value;" required>
															<input type="text" class="form-control form-white" id="uploader" placeholder="no file selected" readonly="">
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nama Instalasi</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<input name="nama_instalasi" type="text" class="form-control form-white" placeholder="Nama Instalasi">
													<i class="icon-user"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Alamat Lokasi Instalasi</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<textarea name="alamat_lokasi_instalasi" class="form-control form-white" placeholder="Alamat Lokasi Instalasi" required></textarea>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Provinsi Lokasi Instalasi</label>
												</div>
												<div class="col-sm-8">
													<select name="provinsi_lokasi_instalasi" class="form-control form-white" data-style="white" data-placeholder="Select a country...">
														<option value="1">DKI Jakarta</option>
														<option value="2">Jawa Barat</option>
														<option value="3">Jawa Timur</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Kabupaten Lokasi Instalasi</label>
												</div>
												<div class="col-sm-8 prepend-icon">
													<select name="kabupaten_lokasi_instalasi" class="form-control form-white" data-style="white" data-placeholder="Select a country...">
														<option value="1">Kabupaten Bandung</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer clearfix">
					<div class="pull-right">
						<a class="btn btn-warning btn-square" href="{{url('eksternal/list_order')}}">Batal</a>
						<a class="btn btn-success btn-square" href="{{url('eksternal/list_order')}}">Simpan</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="copyright">
			<p class="pull-left sm-pull-reset">
				<span>Copyright <span class="copyright">©</span> 2015 </span>
				<span>THEMES LAB</span>.
				<span>All rights reserved. </span>
			</p>
			<p class="pull-right sm-pull-reset">
				<span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
			</p>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END MAIN CONTENT -->


<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
	<div class="spinner">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
</div>
@endsection

@section('page_script')
<!-- BEGIN PAGE SCRIPT -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
<script type="text/javascript">
	$("#jenis_pekerjaan").change(function(){
		switch($("#jenis_pekerjaan").val()){
			case "1": $(".icheckbox_flat-blue").addClass("checked"); console.log("joss");
		}
	});
</script>
<!-- END PAGE SCRIPTS -->
@endsection