@extends('../layout/layout_eksternal')

@section('content')

    {!! Form::open(['url'=>'djk/create/']) !!}
    <div class="page-content page-wizard">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header md-panel-controls">
                        <h3><i class="icon-bulb"></i> Input <strong>Distribusi</strong></h3>
                    </div>
                    <div class="panel-content">
                        <p>Silahkan masukan data pemohon dan detail distribusi yang akan diinspeksi.</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label"><h2><b>Data Pemilik Instalasi</b></h2></label><br/>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Nama Pemilik Instalasi</label>
                                    <input type="text" name="NamaPemilik" class="form-control" placeholder="Nama pemilik">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Alamat</label>
                                    <textarea name="AlamatPemilik" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Lokasi</label>
                                    <input name="LokasiInstalasi" type="text" class="form-control" placeholder="Lokasi">
                                </div>
                                <div class="form-group">
                                    <label class="form-label"><h2><b>Data Distribusi</b></h2></label><br/>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Jenis Instalasi Distribusi</label>
                                    <select name="JenisInstalasi" class="form-control" placeholder="Jenis instalasi">
                                        <option value="311">Jaringan distribusi tenaga listri tegangan menengah</option>
                                        <option value="321">Jaringan distribusi tenaga listri tegangan rendah</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Panjang Saluran</label>
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <input name="PanjangSaluran" type="text" class="form-control" placeholder="Panjang Saluran"/>
                                        </div>
                                        <div class="col-lg-3">kms</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Kapasitas Gardu Distribusi</label>
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <input type="text" name="KapasitasGarduDistribusi" class="form-control"
                                                   placeholder="Kapasitas Gardu Distribusi"/>
                                        </div>
                                        <div class="col-lg-3">kVA</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Nomor LHPP</label>
                                    <input name="NomorLHPP" type="text" class="form-control" placeholder="Nomor LHPP">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Nomor SLO</label>
                                    <input type="text" name="NomorSLO" class="form-control" placeholder="Nomor SLO">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Kode SBU</label>
                                    <input type="text" name="KodeSBU" class="form-control" placeholder="Kode SBU">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Url Koordinat Lokasi</label>
                                    <input type="text" name="UrlKoordinatLokasi" class="form-control" placeholder="Url Koordinat Lokasi">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">UrlFoto Pelaksanaan</label>
                                    <input type="text" name="UrlFotoPelaksanaan" class="form-control" placeholder="Url Foto Pelaksanaan">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">
                                <button class="btn btn-success" type="submit" name="save"><i class="fa fa-save"></i>
                                    Simpan
                                </button>
                            </div>
                            </br> </br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@stop