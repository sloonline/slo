@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
<?php
$summary = \App\Order::calculateSummary($order);
?>

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data Order <strong>Perusahaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Order</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="col-lg-12 clearfix">
                            <div class="row">
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-success btn-transparent btn-square"><i
                                                class="glyphicon glyphicon-exclamation-sign"></i><b>{{@$summary[SUBMITTED]}}
                                            ORDER CREATED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-primary btn-transparent btn-square"><i
                                                class="fa fa-thumbs-up"></i><b>{{@$summary[APPROVED]}} ORDER
                                            APPROVED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-danger btn-transparent btn-square"><i
                                                class="fa fa-thumbs-down"></i><b>{{@$summary[REJECTED]}} ORDER
                                            REJECTED</b>
                                    </button>
                                </div>
                                <div class="col-lg-3">
                                    <button type="button" class="btn btn-warning btn-transparent btn-square"><i
                                                class="fa fa-pencil"></i><b>{{@$summary[REVISED]}} ORDER REVISED</b>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br/><br/>
                        <hr/>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Order</th>
                                <th>Instalasi</th>
                                <th>Tanggal Order</th>
                                <th>Nomor Surat</th>
                                <th>Status</th>
                                <th style="width: auto; ">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(is_array($order) || is_object($order))
                                <?php $no = 1; ?>
                                @foreach($order as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td style="white-space: nowrap;">{{ $item->nomor_order }}</td>
                                        <td>
                                            <ul style="list-style-type: none;">
                                                @foreach(@$item->nama_instalasi as $row)
                                                    <li>{{$row}}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ date('d-M-Y',strtotime($item->tanggal_order ))}}</td>
                                        <td>{{ $item->nomor_sp }}</td>
                                        <td>{{ @$item->flow_status->status_eksternal }}</td>
                                        <td style="white-space: nowrap">
                                            @if(\App\Order::isEksternalAllowEdit($item))
                                                <a href="{{url('/eksternal/create_order/'.$item->id)}}"
                                                   class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                            class="fa fa-pencil"></i></a>
                                            @endif
                                            <a href="{{url('/eksternal/detail_order/'.@$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"></i></a>
                                            @if(@$item->flow_status->modul == MODUL_RAB)
                                                @if(@$item->flow_status->allow_seen == TIPE_ALL)
                                                    <a href="{{url('eksternal/rancangan_biaya_order/'.@$item->id)}}"
                                                       class="btn btn-sm btn-success btn-square btn-embossed">RAB</a>
                                                    @if(\App\User::isUnitPLN(Auth::user()) && \App\Order::isEksternalAllowEditRab(@$item))
                                                        <a href="{{url('eksternal/edit_rancangan_biaya_order/'.@$item->id)}}"
                                                           class="btn btn-sm btn-warning btn-square btn-embossed">SKKI/SKKO</a>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection