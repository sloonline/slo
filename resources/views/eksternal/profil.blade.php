@extends('../layout/layout_eksternal')

@section('page_css')

@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>My <strong>Profile</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">My Profile</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif

                <div class="panel">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#user" data-toggle="tab"><i class="icon-user"></i> USER</a></li>
                            @if($user->perusahaan == null)
                                <li><a href="#pusertif" data-toggle="tab"><i class="fa fa-building-o"></i> PUSERTIF</a>
                                </li>
                            @else
                                <li><a href="#perusahaan" data-toggle="tab"><i class="fa fa-building-o"></i> PERUSAHAAN</a>
                                </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="user">
                                <div class="row column-seperation">
                                    <br>
                                    <div class="col-md-2 line-separator">
                                        @if($user->file_foto_user != null)
                                            <img id="preview_img" class="img-thumbnail"
                                                 src="{{url('upload/'.$user->file_foto_user)}}"
                                                 alt="foto" width="200"/>
                                        @else
                                            <img id="preview_img" class="img-thumbnail"
                                                 src="{{url('assets/global/images/avatars/avatar12_big.png')}}"
                                                 alt="foto" width="200"/>
                                        @endif
                                        <hr>
                                        {{--<a href="{{url('#')}}"--}}
                                        {{--class="btn btn-primary btn-square btn-embossed btn-block"><i--}}
                                        {{--class="icon-camera pull-left"></i> Ganti Foto</a>--}}
                                        {{--<a href="{{url('#')}}"--}}
                                        {{--class="btn btn-success btn-square btn-embossed btn-block"><i--}}
                                        {{--class="icon-lock pull-left"></i> Ganti Password</a>--}}
                                        {{--<a href="{{url('eksternal/profile/edit/'.Auth::user()->id)}}"--}}
                                        {{--class="btn btn-warning btn-square btn-embossed btn-block"><i--}}
                                        {{--class="icon-pencil pull-left"></i> Edit Profile</a>--}}
                                    </div>
                                    <div class="col-md-10">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Lengkap</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->nama_user != null ? $user->nama_user : ""}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Username</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->username != null ? $user->username : ""}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Email</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->email != null ? $user->email : ""}}">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tempat, Tanggal Lahir</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->tanggal_lahir != null ? $user->tempat_lahir_user. ", " .@$user->tanggal_lahir : ""}}">
                                                        <i class="fa fa-legal"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Jenis Kelamin</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->gender_user != null ? $user->gender_user : ""}}">
                                                        <i class="fa fa-male"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">No Handphone</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white" readonly=""
                                                               value="{{$user->no_hp_user != null ? $user->no_hp_user : ""}}">
                                                        <i class="fa fa-mobile"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Alamat</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <textarea rows="3" class="form-control form-white"
                                                                  readonly="">{{$user->alamat_user != null ? $user->alamat_user : ""}}</textarea>
                                                        <i class="icon-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($user->perusahaan != null)
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">KTP</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->file_ktp_user != null ? $user->file_ktp_user : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->no_ktp_user != null ? $user->no_ktp_user : ""}}">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">NPWP</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{$user->file_npwp_user != null ? $user->file_npwp_user : ""}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">NIP / User ID</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->nip_user != null ? $user->nip_user : ""}}">
                                                            <i class="icon-tag"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Grade</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->grade_user != null ? $user->grade_user : ""}}">
                                                            <i class="icon-briefcase"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jabatan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->jabatan_user != null ? $user->jabatan_user : ""}}">
                                                            <i class="icon-briefcase"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Pendidikan
                                                            Terakhir</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->pendidikan_user != null ? $user->pendidikan_user : ""}}">
                                                            <i class="icon-graduation"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Status Pernikahan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->status_pernikahan != null ? $user->status_pernikahan : ""}}">
                                                            <i class="icon-heart"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @if($user->perusahaan != null)
                                <div class="tab-pane fade" id="perusahaan">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <br>
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kategori
                                                            Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$kategori != null ? $kategori->nama_kategori : ""}}">
                                                            <i class="fa fa-building-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   name="nama_pemimpin_perusahaan" readonly=""
                                                                   value="{{$user->perusahaan->nama_perusahaan}}"
                                                            >
                                                            <i class="fa fa-building-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($kategori->id == 1)
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Unit</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div class="append-icon">
                                                                <input type="text" class="form-control form-white"
                                                                       readonly=""
                                                                       value="{{$user->perusahaan->company_code->company_code." - ".$user->perusahaan->company_code->description}}">
                                                                <i class="icon-graduation"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Area</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <div class="append-icon">
                                                                <input type="text" class="form-control form-white"
                                                                       readonly=""
                                                                       value="{{@$user->perusahaan->business_area->business_area." - ".@$user->perusahaan->business_area->description}}">
                                                                <i class="icon-graduation"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama
                                                            Pemimpin {{$kategori->id == 1 ? "Unit" : "Perusahaan"}}</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->nama_pemimpin_perusahaan != null ? $user->perusahaan->nama_pemimpin_perusahaan : ""}}">
                                                            <i class="fa fa-user-secret"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Telepon
                                                            Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->no_telepon_perusahaan != null ? $user->perusahaan->no_telepon_perusahaan : ""}}">
                                                            <i class="fa fa-phone"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Fax
                                                            Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->no_fax_perusahaan != null ? $user->perusahaan->no_fax_perusahaan : ""}}">
                                                            <i class="fa fa-fax"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->email_perusahaan != null ? $user->perusahaan->email_perusahaan : ""}}">
                                                            <i class="fa fa-envelope"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <textarea rows="3" class="form-control form-white"
                                                                      readonly="">{{$user->perusahaan->alamat_perusahaan != null ? $user->perusahaan->alamat_perusahaan : ""}}</textarea>
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->kode_pos_perusahaan != null ? $user->perusahaan->kode_pos_perusahaan : ""}}">
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->city != null ? $user->perusahaan->city->city : ""}}">
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->province != null ? $user->perusahaan->province->province : ""}}">
                                                            <i class="icon-pointer"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">NPWP Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_npwp_perusahaan != null ? $user->perusahaan->file_npwp_perusahaan : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <div class="append-icon">
                                                            <input type="text" class="form-control form-white"
                                                                   readonly=""
                                                                   value="{{$user->perusahaan->no_npwp_perusahaan != null ? $user->perusahaan->no_npwp_perusahaan : ""}}">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_siup_perusahaan != null ? $user->perusahaan->file_siup_perusahaan : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Keterangan Domisili
                                                            Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_skdomisili_perusahaan != null ? $user->perusahaan->file_skdomisili_perusahaan : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Surat Ijin Tempat
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_situ_perusahaan != null ? $user->perusahaan->file_situ_perusahaan : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanda Daftar
                                                            Perusahaan</label>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="#" target="_blank"
                                                           onclick="window.open('{{url('upload/'.$user->perusahaan->file_tdp_perusahaan != null ? $user->perusahaan->file_tdp_perusahaan : "")}}')"
                                                           class="btn btn-info btn-square btn-embossed"><i
                                                                    class="glyphicon glyphicon-search"></i>View File</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_script')
@endsection