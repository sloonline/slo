@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Rancangan <strong>Biaya</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">Rancangan Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif

                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel border">
                                    <div class="panel-header bg-primary">
                                        <h2 class="panel-title">Data <strong>RAB</strong></h2>
                                    </div>
                                    <div class="panel-body bg-white">
                                        <div class="row">
                                            <div class="col-lg-2 pull-right">
                                                <a href="{{url('upload/'.$order->file_spnw)}}"
                                                   class="btn btn-primary"><i
                                                            class="fa fa-download"></i> Download Surat</a>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                @if($validSkkiSkko == false)
                                                    <span style="color: red"><b><i class="fa fa-info-circle"></i> Anda
                                                            belum melengkapi Nomor SKKI/SKKO
                                                            untuk RAB <a
                                                                    href="{{url('eksternal/edit_rancangan_biaya_order/'.$order->id)}}">(LENGKAPI
                                                                DISINI)</a></b></span>
                                                    <br/><br/>
                                                @endif
                                                <?php
                                                $isPLN = (\App\User::isUnitPLN(Auth::user()));
                                                ?>
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Dokumen</th>
                                                        <th>Tanggal RAB</th>
                                                        <th>Instalasi</th>
                                                        <th>Jumlah Biaya</th>
                                                        <th>PPN 10%</th>
                                                        <th>Total Bayar</th>
                                                        @if($isPLN)
                                                            <th>Nomor SKKI</th>
                                                            <th>Nomor SKKO</th>
                                                            <th>Nomor PRK</th>
                                                        @endif
                                                        {{--<th>Aksi</th>--}}
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($rab)>0)
                                                        <?php $no = 1;$total = 0;?>
                                                        @foreach($rab as $item)
                                                            <?php $total += $item->total_biaya;?>
                                                            <tr>
                                                                <td>{{ $no }}</td>
                                                                <td style="white-space: nowrap;">{{ $item->no_dokumen }}</td>
                                                                <td style="white-space: nowrap;">{{ date('d M Y',strtotime($item->created_at)) }}</td>
                                                                <td>
                                                                    <ul style="list-style-type: none;">
                                                                        @foreach(@$item->nama_instalasi as $row)
                                                                            <li>{{$row}}</li>
                                                                        @endforeach
                                                                    </ul>
                                                                </td>
                                                                <td style="white-space: nowrap;text-align: right;">
                                                                    Rp. {{ number_format(@$item->jumlah_biaya, 2, ',', '.') }}</td>
                                                                <td style="white-space: nowrap;text-align: right;">
                                                                    Rp. {{ number_format(@$item->ppn, 2, ',', '.') }}</td>
                                                                <td style="white-space: nowrap;text-align: right;">
                                                                    Rp. {{number_format(@$item->total_biaya, 2, ',', '.')}}</td>
                                                                @if($isPLN)
                                                                    <td>{{@$item->no_skki}}</td>
                                                                    <td>{{@$item->no_skko}}</td>
                                                                    <td>{{@$item->no_prk}}</td>
                                                                @endif
                                                                {{--<td>--}}
                                                                {{--<a href="{{url('/eksternal/rancangan_biaya/'.$item->order_id.'/'.$item->id)}}"--}}
                                                                {{--class="btn btn-sm btn-primary btn-square btn-embossed"><i--}}
                                                                {{--class="fa fa-eye"></i></a>--}}
                                                                {{--</td>--}}
                                                            </tr>
                                                            <?php $no++?>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="6"
                                                                style="text-align: center;background-color: lightgrey">
                                                                <b>Total Biaya</b></td>
                                                            <td style="white-space: nowrap;text-align: right;">
                                                                <b>Rp. {{number_format($total, 2, ',', '.')}}</b></td>
                                                            {{--<td @if($isPLN) colspan="4" @endif--}}
                                                            {{--style="background-color: lightgrey"></td>--}}
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel border" id="form-penugasan">
                            <div class="panel-header bg-primary">
                                <h2 class="panel-title">Surat <strong>Penugasan</strong></h2>
                            </div>
                            <div class="panel-body bg-white">
                                <div class="col-lg-12">
                                    {!! Form::open(['url'=>'eksternal/upload_surat_penugasan', 'files'=> true,'id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form'])  !!}
                                    <input type="hidden" name="order_id" value="{{@$order->id}}"/>
                                    @if(\App\Order::isEksternalAllowEditRab($order) || $flow_completed != false)
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Surat Penugasan</label>
                                            @if($flow_completed == true && $order->file_penugasan  != null)
                                                <div class="col-lg-10"><a target="_blank"
                                                                          href="{{url('upload/'.@$order->file_penugasan)}}">
                                                <span class="btn btn-primary col-lg-3"><b><i
                                                                class="fa fa-download"></i> Preview surat
                                                        penugasan</b></span></a>
                                                </div>
                                            @else
                                                <div class="col-sm-5 prepend-icon">
                                                    <input type="file" required class="form-control form-white"
                                                           name="surat_penugasan" id="surat_penugasan">
                                                </div>
                                                <div class="col-lg-2">
                                                    <button type="submit" id="btn-upload"
                                                            class="btn btn-success ladda-button btn-square btn-embossed">
                                                        Upload
                                                        &nbsp;<i
                                                                class="fa fa-upload"></i>
                                                    </button>
                                                </div>
                                                <span class="col-sm-3" style="color: red">*) Apabila menyetujui dan melanjutkan ke kontrak</span>
                                            @endif
                                        </div>
                                    @endif
                                    @if($flow_completed == false && $order->file_penugasan  != null)
                                        <div class="form-group">
                                            <div class="col-lg-12"><a target="_blank"
                                                                      href="{{url('upload/'.@$order->file_penugasan)}}">
                                            <span class="btn btn-primary col-lg-3"><b><i
                                                            class="fa fa-download"></i> Preview surat
                                                    penugasan</b></span></a>
                                            </div>
                                        </div>
                                    @endif
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel border" id="form-penolakan">
                            <div class="panel-header bg-primary">
                                <h2 class="panel-title">Surat <strong>Penolakan</strong></h2>
                            </div>
                            <div class="panel-body bg-white">
                                <div class="col-lg-12">
                                    {!! Form::open(['url'=>'eksternal/upload_surat_penolakan', 'files'=> true,'id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form'])  !!}
                                    <input type="hidden" name="order_id" value="{{@$order->id}}"/>
                                    @if(\App\Order::isEksternalAllowEditRab($order))
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Surat Penolakan</label>
                                            <div class="col-sm-5 prepend-icon">
                                                <input type="file" required class="form-control form-white"
                                                       name="surat_penolakan" id="surat_penolakan">
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="submit" id="btn-upload"
                                                        class="btn btn-success ladda-button btn-square btn-embossed">
                                                    Upload
                                                    &nbsp;<i
                                                            class="fa fa-upload"></i>
                                                </button>
                                            </div>
                                        <span class="col-sm-3" style="color: red">*) Apabila tidak
                                            menyetujui RAB,wajib  melampirkan surat penolakan</span>
                                        </div>
                                    @endif
                                    @if($order->file_stlkrab  != null)
                                        <div class="form-group">
                                            <div class="col-lg-12"><a target="_blank"
                                                                      href="{{url('upload/'.@$order->file_stlkrab)}}">
                                            <span class="btn btn-primary col-lg-3"><b><i
                                                            class="fa fa-download"></i> Preview surat penolakan RAB</b></span></a>
                                            </div>
                                        </div>
                                    @endif
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <hr>
                        @include('workflow_view')
                    </div>
                </div>
            </div>
            @endsection


            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <!-- Buttons Loading State -->
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
                <!-- Select Inputs -->
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- >Bootstrap Date Picker -->
                <script type="text/javascript">
                    @if($order->file_stlkrab == null)
                        $("#form-penolakan").hide();
                    @endif
                    @if($validSkkiSkko == false)
                        $("#btn_approve").attr('disabled', true);
                    @endif
                    $("#btn_reject").click(function () {
                        @if($order->file_stlkrab == null)
                            $("#form-penolakan").show();
                        $("#btn-upload").click();
                        return false;
                        @endif
                    });
                    $("#surat_penolakan").change(function () {
                        $("#btn_reject").removeAttr('disabled');
                        @if($order->file_stlkrab == null)
                            $("#btn_reject").attr('disabled', true);
                        @endif
                    });
                </script>
@endsection