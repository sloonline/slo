@if(sizeof($fields) > 0)
    <div class="form-group">
        <div class="col-sm-9">
            <h4 class="col-sm-12 control-label"><strong>Pemeriksaan Dokumen</strong></h4>
        </div>
    </div>
    <div style="height: 350px;overflow-x: hidden;overflow-y: scroll">
        @foreach($fields as $field)
            <?php $nama_field = $field['name']; ?>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">{{$field['label']}}</label>
                </div>
                <div class="col-sm-9">
                    @if(!(isset($files['valid_'.$nama_field]) && $files['valid_'.$nama_field] == true))
                        <div class="file">
                            <div class="option-group">
                                <span class="file-button btn-primary">Choose File</span>
                                <input type="file" class="{{$field['class']}}" name="{{$field['name']}}"
                                       onchange="document.getElementById('{{$field['id']}}').value = this.value;">
                                <input type="text" class="form-control form-white" id="{{$field['id']}}"
                                       placeholder="no file selected" readonly="">
                            </div>
                        </div>
                    @endif
                    @if($field['value'] != "")
                        <a href="{{url('/upload/'.$field['value'])}}" style="color:blue">{{$field['value']}}</a>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endif