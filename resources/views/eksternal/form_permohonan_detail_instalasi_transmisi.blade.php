<br>
<div class="form-group">
    <div class="col-sm-3">
        <h4 class="col-sm-12 control-label"><strong>Data Pemilik Instalasi</strong></h4>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Pemilik</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($transmisi != null)? $transmisi->pemilik->nama_pemilik : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Instalasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($transmisi != null)? $transmisi->jenis_instalasi->jenis_instalasi : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Alamat Instalasi</label>
    </div>
    <div class="col-sm-9">
        <textarea rows="3" readonly class="form-control form-white" name="alamat_instalasi" placeholder="Alamat Instalasi">{{($transmisi != null)? $transmisi->alamat_instalasi : ""}}</textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Provinsi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($transmisi != null)? $transmisi->provinsi->province : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($transmisi != null)? $transmisi->kota->city : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($transmisi != null)? $transmisi->longitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="latitude_awal" value="{{($transmisi != null)? $transmisi->latitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_akhir" value="{{($transmisi != null)? $transmisi->longitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="latitude_akhir" value="{{($transmisi != null)? $transmisi->latitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Total Gardu Induk</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="kapasitas_gardu_induk" value="{{($transmisi != null)? $transmisi->kapasitas_gi : ""}}" >
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Bay Line</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="bay_line" value="{{($transmisi != null)? $transmisi->bay_line : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Bay Bus Coupler</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="bay_bus_coupler" value="{{($transmisi != null)? $transmisi->bay_bus_coupler : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Bay Transformator Tenaga</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="bay_transformator_tenaga" value="{{($transmisi != null)? $transmisi->bay_tf_tenaga : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Bay Reaktor</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="bay_reaktor" value="{{($transmisi != null)? $transmisi->bay_reaktor : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Bay Kapasitor</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="bay_kapasitor" value="{{($transmisi != null)? $transmisi->bay_kapasitor : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">SUTET / SUTT</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="sutet" value="{{($transmisi != null)? $transmisi->sutet : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">SKTT</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="sktt" value="{{($transmisi != null)? $transmisi->sktt : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">SKLT</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="sklt" value="{{($transmisi != null)? $transmisi->sklt : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Panjang Saluran TET / TT</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="panjang_saluran" value="{{($transmisi != null)? $transmisi->panjang_tt : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jumlah Tower</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="jumlah_tower" value="{{($transmisi != null)? $transmisi->jml_tower : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Sistem Jaringan Transmisi</label>
    </div>
    <div class="col-sm-9">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Pemutus Tenaga</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="kapasitas_pemutus_tenaga" value="{{($transmisi != null)? $transmisi->kap_pemutus_tenaga : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Transformator Tenaga</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="kapasitas_transformator_tenaga" value="{{($transmisi != null)? $transmisi->kap_tf_tenaga : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Bank Kapasitor</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="kapasitas_bank_kapasitor" value="{{($transmisi != null)? $transmisi->kap_bank_kapasitor : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Bank Reaktor</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="kapasitas_bank_reaktor" value="{{($transmisi != null)? $transmisi->kap_bank_reaktor : ""}}">
       
    </div>
</div>