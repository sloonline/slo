@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
          rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Rancangan <strong>Biaya</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">Rancangan Biaya</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif

                <div class="panel">
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel border">
                                    <div class="panel-header bg-primary">
                                        <h2 class="panel-title">Data <strong>RAB</strong></h2>
                                    </div>
                                    {!! Form::open(['url'=>'eksternal/edit_rancangan_biaya_order','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                                    <div class="panel-body bg-white">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Dokumen</th>
                                                        <th>Tanggal RAB</th>
                                                        <th>Jumlah Biaya</th>
                                                        <th>PPN 10%</th>
                                                        <th>Total Bayar</th>
                                                        <th style="width: 150px;">No. SKKI</th>
                                                        <th style="width: 150px;">No. SKKO</th>
                                                        <th style="width: 150px;">No. PRK</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($rab)>0)
                                                        <?php $no = 1;$total = 0;?>
                                                        @foreach($rab as $item)
                                                            <?php $total += $item->total_biaya;?>
                                                            <tr>
                                                                <td>{{ $no }}</td>
                                                                <td>{{ $item->no_dokumen }}</td>
                                                                <td>{{ date('d M Y',strtotime($item->created_at)) }}</td>
                                                                <td>{{ $item->jumlah_biaya }}</td>
                                                                <td>{{ $item->ppn }}</td>
                                                                <td style="text-align: right;">
                                                                    Rp. {{number_format(@$item->total_biaya, 2, ',', '.')}}</td>
                                                                <td><input class="form-control form-white" type="text"
                                                                           placeholder="Nomor SKKI" name="skki[]"
                                                                           id="skki_{{$item->id}}" {{(@$item->no_skko == "") ? "required" : ""}}
                                                                           value="{{@$item->no_skki}}"
                                                                           rab="{{$item->id}}"/></td>
                                                                <td><input class="form-control form-white" type="text"
                                                                           placeholder="Nomor SKKO" name="skko[]"
                                                                           id="skko_{{$item->id}}"
                                                                           value="{{@$item->no_skko}}"
                                                                           rab="{{$item->id}}"/></td>
                                                                <td><input class="form-control form-white" type="text"
                                                                           placeholder="Nomor PRK"
                                                                           name="prk[]" id="prk_{{$item->id}}"
                                                                           value="{{@$item->no_prk}}"
                                                                           rab="{{$item->id}}"/>
                                                                </td>
                                                            </tr>
                                                            <?php $no++?>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="5"
                                                                style="text-align: center;background-color: lightgrey">
                                                                <b>Total Biaya</b></td>
                                                            <td style="text-align: right;">
                                                                <b>Rp. {{number_format($total, 2, ',', '.')}}</b></td>
                                                            <td colspan="4" style="background-color: lightgrey"></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                                <div class="col-lg-3 pull-right">
                                                    <span style="color: red">*) Jika mengisi SKKI, Nomor PRK wajib diisi</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer clearfix bg-white">
                                        <div class="col-lg-8">
                                            <a href="{{url('upload/'.$order->file_spnw)}}" class="btn btn-primary"><i
                                                        class="fa fa-download"></i> Download Surat</a>
                                        </div>
                                        <div class="pull-right">
                                            <input type="hidden" name="order_id" value="{{@$order->id}}">
                                            <a href="{{ url('eksternal/view_order') }}"
                                               class="btn btn-warning btn-square btn-embossed">Kembali
                                                &nbsp;<i class="icon-ban"></i></a>
                                            <button type="submit"
                                                    class="btn btn-success ladda-button btn-square btn-embossed"
                                                    data-style="zoom-in">Simpan &nbsp;<i
                                                        class="glyphicon glyphicon-floppy-saved"></i>
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <!-- Select Inputs -->
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- >Bootstrap Date Picker -->
            <script type="text/javascript">
                $('input[name^="skki"]').change(function () {
                    var id = this.id;
                    validateSkki(id);
                });
                $('input[name^="skko"]').change(function () {
                    var id = this.id;
                    validateSkko(id);
                });
                function validateSkki(id){
                    console.log("a");
                    var val = $("#" + id).val();
                    var rab = $("#" + id).attr('rab');
                    var skko_val =  $("#skki_" + rab).val();
                    if(val.trim() != ""){
                        $("#prk_" + rab).attr("required", true);
                    }else{
                        $("#prk_" + rab).removeAttr("required");
                        if(skko_val.trim() != "") {
                            $("#skki_" + rab).removeAttr("required");
                        }
                    }
                }
                function validateSkko(id){
                    var val = $("#" + id).val();
                    var rab = $("#" + id).attr('rab');
                    var skki_val =  $("#skki_" + rab).val();
                    if(val.trim() != "" && skki_val.trim() == ""){
                        $("#prk_" + rab).removeAttr("required");
                        $("#skki_" + rab).removeAttr("required");
                    }else{
                        $("#prk_" + rab).attr("required", true);
                        $("#skki_" + rab).attr("required", true);
                    }
                }
            </script>
@endsection