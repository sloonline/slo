
@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/magnific/magnific-popup.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content page-thin">
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-header bg-primary">
          <h3><i class="icon-home"></i> <strong>Dashboard</strong></h3>
        </div>
        <div class="panel-content">
          <div class="slick" data-fade="true">
            <div class="slide">
              <img src="{{ url('/') }}/assets/global/images/widgets/login2.jpg" style="width: 100%" height="300">
            </div>
            <div class="slide">
              <img src="{{ url('/') }}/assets/global/images/widgets/login3.jpg" style="width: 100%" height="300">
            </div>
            <div class="slide">
              <img src="{{ url('/') }}/assets/global/images/widgets/login4.jpg" style="width: 100%" height="300">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="panel">
        <div class="panel-content widget-info">
          <div class="row">
            <div class="left">
              <i class="fa fa-umbrella bg-green"></i>
            </div>
            <div class="right">
              <p class="number countup" data-from="0" data-to="5200">0</p>
              <p class="text">Permohonan Baru</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="panel">
        <div class="panel-content widget-info">
          <div class="row">
            <div class="left">
              <i class="fa fa-bug bg-blue"></i>
            </div>
            <div class="right">
              <p class="number countup" data-from="0" data-to="575">0</p>
              <p class="text">Proses SLO</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="panel">
        <div class="panel-content widget-info">
          <div class="row">
            <div class="left">
              <i class="fa fa-fire-extinguisher bg-red"></i>
            </div>
            <div class="right">
              <p class="number countup" data-from="0" data-to="463">0</p>
              <p class="text">Menunggu Sertifikat</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
      <div class="panel">
        <div class="panel-content widget-info">
          <div class="row">
            <div class="left">
              <i class="icon-microphone bg-purple"></i>
            </div>
            <div class="right">
              <p class="number countup" data-from="0" data-to="1210">0</p>
              <p class="text">Proses Selesai</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-header bg-primary">
          <h3><i class="icon-energy"></i> <strong>Instalasi</strong></h3>
          <div class="control-btn">
            <a href="#">SEMUA INSTALASI&nbsp; <i class="icon-layers"></i></a>
          </div>
        </div>
        <div class="panel-content p-t-5">
          <div class="widget widget_slider p-b-0">
            <div class="slick" data-arrows="true" data-num-slides="4" data-num-scroll="1" data-autoplay="false">
              <?php
              for($i=0;$i<8;$i++){
                $r = rand(1,3);
                ?>
                <div class="slide text-center">
                  <img style="margin: 0 auto;" src="{{ url('/') }}/assets/global/images/widgets/square{{$r}}.jpg" width="200" class="img-thumbnail">
                  <h4><strong>NAMA INSTALASI</strong></h4>
                  <h4><strong>JENIS INSTALASI</strong></h4>
                  <br>
                </div>
                <?php }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="panel">
          <div class="panel-header bg-primary">
            <h3><i class="icon-book-open"></i> <strong>Layanan</strong></h3>
          </div>
          <div class="panel-content">
            <div class="portfolioFilter text-center">
              <a href="#" data-filter="*" class="current btn btn-sm btn-blue btn-square btn-embossed"><h3 class="c-white m-10">Semua Layanan</h3></a>
              <a href="#" data-filter=".pembangkit" class="btn btn-sm btn-danger btn-square btn-embossed"><h3 class="c-white m-10">Pembangkit</h3></a>
              <a href="#" data-filter=".transmisi" class="btn btn-sm btn-success btn-square btn-embossed"><h3 class="c-white m-10">Transmisi</h3></a>
              <a href="#" data-filter=".distribusi" class="btn btn-sm btn-info btn-square btn-embossed"><h3 class="c-white m-10">Distribusi</h3></a>
            </div>
            <hr>
            <div class="portfolioContainer grid">
                <div class="col-md-3 pembangkit p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/14.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 pembangkit p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/3.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 pembangkit p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/2.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 pembangkit p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/1.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/6.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/8.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/13.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/11.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 distribusi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/9.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 distribusi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/7.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 distribusi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/12.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 pembangkit p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/5.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/4.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
                <div class="col-md-3 transmisi p-b-20 text-center">
                  <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/10.jpg" class="img-thumbnail"/>
                  <h3 class="m-0 m-t-10"><strong>NAMA LAYANAN</strong></h3>
                  <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection

    @section('page_script')
    <script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/slick/slick.min.js"></script> <!-- Slider -->
    <script src="{{ url('/') }}/assets/global/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/isotope/isotope.pkgd.min.js"></script>
    <script>
    $(window).load(function(){
      var $container = $('.portfolioContainer');
      $container.isotope();
      $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
        var selector = $(this).attr('data-filter');
        $container.isotope({
          filter: selector
        });
        return false;
      });
    });
    </script>
    @endsection
