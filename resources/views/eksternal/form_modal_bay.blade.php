{!! Form::open(array('name'=>'form_bay','id'=>'form_bay','url'=> 'eksternal/create_instalasi_transmisi', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
<input type="hidden" id="form_hidden_jenis_bay">
<input type="hidden" id="form_id_bay">
<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Jenis Bay *</label>
    </div>
    <div class="col-sm-8">
        <input type="text" class="form-control form-white" readonly required
               name="form_jenis_bay" id="form_jenis_bay">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Nama *</label>
    </div>
    <div class="col-sm-8 prepend-icon">
        <input type="text" class="form-control form-white" required
               name="form_nama_bay" id="form_nama_bay">
        <i class="fa fa-gear"></i>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Pemilik Instalasi *</label>
    </div>
    <div class="col-sm-8">
        <select class="form-control form-white" data-search="true" required
                name="form_tipe_pemilik" id="form_jenis_pemilik">
            <option value="{{MILIK_SENDIRI}}">
                Milik sendiri
            </option>
            <option value="{{TERDAFTAR}}">
                Milik perusahaan lain yang terdaftar
            </option>
        </select>
    </div>
</div>

<div class="form-group" id="form_perusahaan_terdaftar">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Nama Pemilik Instalasi</label>
    </div>
    <div class="col-sm-8">
        <select class="form-control form-white" data-search="true"
                name="form_pemilik_instalasi_lain"
                id="form_pemilik_instalasi_lain">
            @foreach($pemilik as $item)
                <option value="{{$item->id}}">{{$item->nama_pemilik}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Kontraktor (SBU)*</label>
        <small class="text-muted">(Jika tidak ada gunakan '0000 - TIDAK ADA PEMBANGUNAN')</small>
    </div>
    <div class="col-sm-8">
        {{--<select class="form-control form-white" data-search="true"--}}
                {{--name="form_kode_kontraktor" id="form_kode_kontraktor">--}}
            {{--<option value="">--- Pilih ---</option>--}}
            {{--@foreach($list_kontraktor as $item)--}}
                {{--<option value="{{$item->kode}}" {{($transmisi != null && $transmisi->kode_kontraktor == $item->kode) ? "selected" : ""}} nama_kontraktor="{{$item->nama}}">{{$item->kode." - ".$item->nama}}</option>--}}
            {{--@endforeach--}}
        {{--</select>--}}
        <input type="text" required
               value="{{(@$transmisi->kontraktor != null) ? @$transmisi->kode_kontraktor . " - " .@$transmisi->kontraktor : ""}}"
               class="form-control form-white typeahead" autocomplete="off"
               typeahead-editable="false"  name="form_kode_kontraktor" id="form_kode_kontraktor"
               spellcheck="false">
    </div>
    
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Kapasitas Pemutus Tenaga *</label>
    </div>
    <div class="col-sm-7 prepend-icon">
        <input type="text" class="form-control form-white" required
               name="form_kapasitas_pemutus" id="form_kapasitas_pemutus">
        <i class="fa fa-gear"></i>
    </div>
    <div class="col-sm-1">
        <label class="col-sm-12 control-label">kA</label>
    </div>
    {{--<div class="col-sm-1">--}}
        {{--kA--}}
    {{--</div>--}}
</div>
<div class="form-group" id="div_kapasitas_trafo">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Kapasitas trafo tenaga/kapasitor/reaktor *</label>
    </div>
    <div class="col-sm-7 prepend-icon">
        <input type="text" class="form-control form-white" required
               name="form_kapasitas_trafo" id="form_kapasitas_trafo">
        <i class="fa fa-gear"></i>
    </div>
    <div class="col-sm-1">
        <label class="col-sm-12 control-label">MVA</label>
    </div>
    {{--<div class="col-sm-1">--}}
    {{--kA--}}
    {{--</div>--}}
</div>
<div class="form-group">
    <div class="col-sm-4">
        <label class="col-sm-12 control-label">Tegangan Pengenal *</label>
    </div>
    <div class="col-sm-8">
        <select class="form-control form-white" data-search="true"
                name="form_tegangan_pengenal" id="form_tegangan_pengenal">
            <option value="">--Pilih Tegangan Pengenal--</option>
            @foreach($tegangan_pengenal as $item)
                <option value="{{$item->id}}">{{$item->nama_reference}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <div class="pull-right">
            <button type="submit" id="submit_form"
                    class="btn btn-success btn-square btn-embossed"
                    data-style="zoom-in">Simpan
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}