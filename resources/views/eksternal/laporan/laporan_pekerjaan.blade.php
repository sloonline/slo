@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Laporan <strong>Pekerjaan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li class="active">Laporan pekerjaan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel">
                        <div class="panel-content">
                            <div class="nav-tabs2" id="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#general" data-toggle="tab"><i class="icon-home"></i>
                                            General
                                        </a>
                                    </li>
                                    <li id="tab_hasil_inspeksi">
                                        <a href="#laporan" id="menu_laporan" data-toggle="tab"><i
                                                    class="fa fa-file-text-o"></i>
                                            Laporan
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content bg-white">
                                    <div class="tab-pane active" id="general">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Mulai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->tgl_mulai_pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Tanggal Selesai</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control"
                                                           value="{{$pekerjaan->tgl_selesai_pekerjaan}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Uraian Pekerjaan</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control" rows="4"
                                                              name="uraian_pekerjaan"
                                                              placeholder="Uraian Pekerjaan"
                                                              disabled="">{{$pekerjaan->uraian_pekerjaan}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="laporan">
                                        <div class="panel-content pagination2 table-responsive">
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Kick Off Meeting</label>
                                                </div>
                                                @if(@$laporan['kick_off']->file_kick_off != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['kick_off']->file_kick_off )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Data Teknik</label>
                                                </div>
                                                @if(@$laporan['data_teknik']->hasil_data_teknik != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['data_teknik']->hasil_data_teknik )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Blangko Inspeksi</label>
                                                </div>
                                                @if(@$laporan['blangko_inspeksi']->file_blangko_inspeksi != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['blangko_inspeksi']->file_blangko_inspeksi)}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Berita Acara Inspeksi</label>
                                                </div>
                                                @if(@$laporan['berita_inspeksi']->file_berita_acara != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['berita_inspeksi']->file_berita_acara )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Hasil Inspeksi Laik Operasi (HILO)</label>
                                                </div>
                                                @if(@$laporan['hilo']->file_hilo != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['hilo']->file_hilo )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File RLB</label>
                                                </div>
                                                @if(@$laporan['rlb']->file_rlb != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['rlb']->file_rlb )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">File RLS</label>
                                                </div>
                                                @if(@$laporan['rls']->file_rls != null)
                                                    <div class="col-md-3">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$laporan['rls']->file_rls )}}">
                                                            <button type="button" class="col-lg-12 btn btn-primary">
                                                                <center><i class="fa fa-download"></i>Preview File</center>
                                                            </button>
                                                        </a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix bg-white" id="laporan_save">
                            <div class="pull-right">
                                <a href="{{ url('eksternal/laporan') }}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali
                                    &nbsp;<i class="icon-ban"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection
            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- Buttons Loading State -->

@endsection
