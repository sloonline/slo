@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="page-content">
    <div class="header">
        <h2>Data Laporan<strong>Pekerjaan</strong></h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                <li class="active">Laporan Pekerjaan</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel border">
                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-dynamic">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Surat Permohonan</th>
                                <th>Instalasi</th>
                                <th>Pekerjaan</th>
                                <th>Lingkup Pekerjaan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @if($pekerjaan != null)
                                @foreach($pekerjaan as $item)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ @$item->permohonan->nomor_permohonan}}</td>
                                    <td>{{ @$item->permohonan->instalasi->nama_instalasi}}</td>
                                    <td>{{ @$item->pekerjaan }}</td>
                                    <td>{{ @$item->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}</td>
                                    <td>
                                        <a href="{{url('eksternal/laporan_pekerjaan/'.@$item->id)}}"
                                            class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                            class="fa fa-eye"
                                            data-rel="tooltip"
                                            data-placement="right" title="Detail Laporan"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                    @endforeach
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- Tables Filtering, Sorting & Editing -->
        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <!-- Buttons Loading State -->
        @endsection