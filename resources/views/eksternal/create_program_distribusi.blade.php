@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>{{($program != null) ? "Ubah" : "Tambah"}} Data <strong>Program Distribusi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/program_distribusi')}}">Program Distribusi</a></li>
                    <li class="active">{{($program != null) ? "Ubah" : "Tambah"}} Program Distribusi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(array('name'=>'form_instalasi','url'=> 'eksternal/create_program', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    PROGRAM</a></li>
                            @if($program != null)
                                <li><a href="#area_kontrak" data-toggle="tab"><i
                                                class="icon-speedometer"></i> {{@$program->jenisProgram->acuan_1}}</a></li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Tugas</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{$surat->nomor_surat}}"
                                               name="surat_tugas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Program</label>
                                    </div>
                                    <input type="hidden" name="id" value="{{$id}}"/>
                                    <input type="hidden" name="id_surat_tugas" value="{{$id_surat_tugas}}"/>
                                    <div class="col-sm-9">
                                        @if($program == null)
                                            <select class="form-control form-white" data-search="true"
                                                    name="id_jenis_program" required id="id_jenis_program">
                                                <option value="">--- Pilih ---</option>
                                                @foreach($jenis_program as $item)
                                                    <option value="{{$item->id}}" {{($program != null && $program->id_jenis_program == $item->id) ? "selected" : ""}}>{{$item->jenis_program}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="hidden" name="id_jenis_program"
                                                   value="{{$program->id_jenis_program }}"/>
                                            <input type="text" class="form-control form-white" readonly
                                                   value="{{$program->jenisProgram->jenis_program}}" required
                                                   name="jenis_program">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Program</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               value="{{@$program->deskripsi}}" required name="deskripsi">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Periode</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="number" min="1900" minlength="4" maxlength="4"
                                               class="form-control form-white" value="{{@$program->periode}}" required
                                               name="periode">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            @if($program != null)
                                <div class="tab-pane fade" id="area_kontrak">
                                    <div class="m-b-20 border-bottom">
                                        <div class="btn-group">
                                            <a href="{{url('eksternal/'.(($program->jenisProgram->acuan_1 == TIPE_AREA)?"create_area_program": "create_kontrak_program")."/".$id)}}"
                                               class="btn btn-success btn-square btn-block btn-embossed"><i
                                                        class="fa fa-plus"></i> Tambah Area / Kontrak</a>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-dynamic">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            @if($program->jenisProgram->acuan_1 == TIPE_AREA)
                                                <th>Area</th>
                                            @else
                                                <th>Periode</th>
                                                <th>Nomor Kontrak</th>
                                                <th>Tanggal Kontrak</th>
                                                <th>Vendor</th>
                                            @endif
                                            <th style="width: auto; ">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1;?>
                                        <?php $data = ($program->jenisProgram->acuan_1 == TIPE_AREA) ? $program->areaProgram : $program->kontrakProgram;?>
                                        @foreach($data as $dt)
                                            <tr>
                                                <td>{{$no}}</td>
                                                @if($program->jenisProgram->acuan_1 == TIPE_AREA)
                                                    <td>{{$dt->businessArea->description}}</td>
                                                @else
                                                    <td>{{$dt->periode}}</td>
                                                    <td>{{$dt->nomor_kontrak}}</td>
                                                    <td>{{$dt->tanggal_kontrak}}</td>
                                                    <td>{{$dt->vendor}}</td>
                                                @endif
                                                <td>
                                                    <a href="{{url('/eksternal/'.(($program->jenisProgram->acuan_1 == TIPE_AREA)?"create_area_program": "create_kontrak_program")."/".$id."/".$dt->id)}}"
                                                       class="btn btn-sm btn-primary btn-square btn-embossed" data-toggle="tooltip" title="{{$program->jenisProgram->acuan_2}}"><i
                                                                class="fa fa-eye"></i></a>
                                                    <a href="{{url('/eksternal/delete_program/'.$dt->id)}}"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       onclick="return confirm('Apakah anda yakin untuk menghapus?')"><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php $no++;?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr class="p-t-0 m-t-0">
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="{{url('/eksternal/create_surat_tugas/'.@$id_surat_tugas)}}"
                               class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" id="submit_form"
                                    class="btn btn-success btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
            </script>
@endsection