@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Master <strong>Penyulang</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Penyulang</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 portlets">
				<p class="m-t-10 m-b-20 f-16">List Penyulang</p>
				<div class="panel">
					<div class="panel-header panel-controls bg-primary">
					<h3><i class="fa fa-table"></i> LIST <strong>DATA</strong></h3>
					</div>
					<div class="panel-content pagination2 table-responsive">
						<div class="m-b-20 border-bottom">
							<div class="btn-group">
								<a href="{{ url('/') }}/master/penyulang/input" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah Penyulang</a>
							</div>
						</div>
						<table class="table table-hover table-dynamic">
							<thead>
								<tr>
									<th>No</th>
									<th>Penyulang</th>
									<th>Area</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1 ?>
							@foreach($penyulang as $item)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $item->penyulang }}</td>
								<td>{{ $item->description." - ".$item->business_area }}</td>
								<td>
									<a href="{{ url('/') }}/master/penyulang/input/{{$item->id}}" class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></a>
									<a href="{{ url('/') }}/master/penyulang/delete/{{$item->id}}" class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus gardu induk ini?')"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection