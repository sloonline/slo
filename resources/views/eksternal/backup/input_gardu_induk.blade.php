@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
			<h2>Master <strong>Gardu Induk</strong></h2>
			<div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="#">Master</a></li>
					<li class="active">Gardu Induk</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 portlets">
				<p class="m-t-10 m-b-20 f-16">List Bidang Internal Pusertif.</p>
				<div class="panel">
					{!! Form::open(['url'=>'master/gardu_induk/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
					<div class="panel-header bg-primary">
						<h3><i class="fa fa-table"></i> <?php echo ($gi != null) ? "Update" : "Input"; ?> <strong>Gardu Induk</strong></h3>
					</div>
					<div class="panel-content">
					    <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Area</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control form-white" data-search="true" name="area">
                                    @if($gi != null)
                                    <option value="{{$gi->business_area}}">{{$gi->businessArea->description}}</option>
                                    @endif
                                    @foreach($ba as $item)
                                    <option value="{{$item->id}}">{{$item->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="col-sm-12 control-label">Nama Gardu Induk</label>
							</div>
							<div class="col-sm-9">
								<div class="append-icon">
									<input type="text" required="required" name="gardu_induk"
												class="form-control form-white"
												value="<?php echo ($gi != null) ? $gi->gardu_induk : ""; ?>" >
									<i class="fa fa-building"></i>
								</div>
							</div>
						</div>
						<div class="form-group">
						    <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Jenis Gardu</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="icheck-inline">
                                        <label><input type="radio" name="jenis_gardu" data-radio="iradio_square-blue" value="GI" {{($gi != null && $gi->jenis_gardu == "GI")? "checked": ""}}/> GI</label>
                                        <label><input type="radio" name="jenis_gardu" data-radio="iradio_square-blue" value="GH" {{($gi != null && $gi->jenis_gardu == "GH")? "checked": ""}}/> GH</label>
                                    </div>
                                </div>
                          </div>
                        </div>
					</div>
					<hr />
					<div class="panel-footer clearfix bg-white">
						<div class="pull-right">
							<input type="hidden" name="id" value="<?php echo ($gi != null) ? $gi->id : 0; ?>">
							<a href="{{ url('master/gardu_induk') }}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
							<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
@endsection

@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection