@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Lokasi <strong>Area Program</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Area Program</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">Input lokasi area untuk program.</p>
                <div class="panel">
                    {!! Form::open(['url'=>'master/lokasi_area/store','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> <?php echo ($lokasi_area != null) ? "Update" : "Input"; ?>
                            <strong>Lokasi Area Program</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Lokasi</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control form-white" data-search="true" name="id_lokasi">
                                    @if($lokasi_area != null)
                                        <option value="{{$lokasi_area->id_lokasi}}">{{$lokasi_area->lokasi->lokasi}}</option>
                                    @endif
                                    @foreach($lokasi as $item)
                                        <option value="{{$item->id}}">{{$item->lokasi}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($area != null)
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="hidden" name="id_area_program" value="{{$area->id}}"/>
                                    <input type="text" class="form-control form-white" readonly name="business_area"
                                           value="{{$area->businessArea->description}}"/>
                                </div>
                            </div>
                        @endif
                        @if($kontrak != null)
                            <input type="hidden" name="id_kontrak" value="{{$kontrak->id}}"/>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="vendor"
                                           value="{{$kontrak->nomor_kontrak}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Tanggal Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="vendor"
                                           value="{{$kontrak->tanggal_kontrak}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Vendor Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="vendor"
                                           value="{{$kontrak->vendor}}"/>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Program</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="hidden" name="id_program" value="{{$program->id}}"/>
                                <input type="text" class="form-control form-white" readonly name="jenis_program"
                                       value="{{$program->jenisProgram->jenis_program}}"/>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id"
                                   value="<?php echo ($lokasi_area != null) ? $lokasi_area->id : 0; ?>">
                            <a href="{{url('/eksternal/create_area_program/'.$area->id_program."/".$area->id) }}" class="btn btn-warning btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection