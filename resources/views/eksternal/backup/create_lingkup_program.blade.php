@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Master <strong>Area Program</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Area Program</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16">Input lokasi area untuk program.</p>
                <div class="panel">
                    {!! Form::open(['url'=>'master/lingkup_program/store','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <input type="hidden" name="id_acuan" value="{{$id_acuan}}">
                    <div class="panel-header bg-primary">
                        <h3><i class="fa fa-table"></i> <?php echo ($lokasi_area != null) ? "Update" : "Input"; ?>
                            <strong>Area Program</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="icheck-list">
                                    @foreach($lingkup_kerja as $row)
                                        <input type="checkbox"  data-checkbox="icheckbox_square-blue" name="id_lingkup_pekerjaan[]" class="roles"
                                               value="{{$row->id}}"
                                        <?php
                                                $stat = 0;
                                                $i = 0;
                                                while ($stat == 0 && $i < sizeof($lingkup_program)) {
                                                    if ($lingkup_program[$i]->id_lingkup_pekerjaan == $row->id) {
                                                        $stat = 1;
                                                        echo "checked";
                                                    } else {
                                                        $i++;
                                                    }
                                                }
                                                ?>
                                        />
                                        {{$row->jenis_lingkup_pekerjaan}}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if($penyulang_area != null)
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Penyulang</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{$penyulang_area->penyulang->penyulang}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{$penyulang_area->areaProgram->businessArea->description}}"/>
                                </div>
                            </div>
                        @endif
                        @if($gardu_area != null)
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Gardu Induk</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{$gardu_area->garduInduk->gardu_induk}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{$gardu_area->areaProgram->businessArea->business_area}}"/>
                                </div>
                            </div>
                        @endif
                        @if($lokasi_area != null)
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Lokasi</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{$lokasi_area->lokasi->lokasi}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area/Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white" readonly name="gardu"
                                           value="{{($lokasi_area->id_area_program != null) ? $lokasi_area->areaProgram->businessArea->description : $lokasi_area->kontrak->nomor_kontrak}}"/>
                                </div>
                            </div>
                            @if($lokasi_area->id_kontrak != null)
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Vendor</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly name="gardu"
                                               value="{{$lokasi_area->kontrak->vendor}}"/>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Program</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="hidden" name="id_program" value="{{$program->id}}"/>
                                <input type="text" class="form-control form-white" readonly name="jenis_program"
                                       value="{{$program->jenisProgram->jenis_program}}"/>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <input type="hidden" name="id"
                                   value="<?php echo ($lokasi_area != null) ? $lokasi_area->id : 0; ?>">
                            <a href="{{ url('/eksternal/program_tree/'.$program->id) }}" class="btn btn-warning btn-square btn-embossed">Batal
                                &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection