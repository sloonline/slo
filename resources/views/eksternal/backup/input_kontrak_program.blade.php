@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>{{($kontrak != null) ? "Ubah" : "Tambah"}} Data <strong>Kontrak</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/create_program/'.$program->id)}}">Program Distribusi</a></li>
                    <li class="active">{{($kontrak != null) ? "Ubah" : "Tambah"}} Kontrak</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(['url'=>'/eksternal/create_kontrak_program','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    KONTRAK</a></li>
                            @if($kontrak != null)
                                <li><a href="#lokasi" data-toggle="tab"><i
                                                class="icon-speedometer"></i> {{$program->jenisProgram->acuan_2}}</a></li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <input type="hidden" name="id" value="{{($kontrak != null) ? $kontrak->id : 0}}">
                                <div class="panel-content">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Program</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="id_program" value="{{$program->id}}"/>
                                            <input type="text" class="form-control form-white" readonly
                                                   name="jenis_program"
                                                   value="{{$program->jenisProgram->jenis_program}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Periode</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="periode" placeholder="Periode kontrak (tahun)" type="number"
                                                   required
                                                   class="form-control form-white" min="1000"
                                                   value="{{($kontrak != null) ? $kontrak->periode : null}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Vendor</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="vendor" placeholder="Vendor" required
                                                   class="form-control form-white"
                                                   value="{{($kontrak != null) ? $kontrak->vendor : null}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input name="nomor_kontrak" placeholder="Nomor kontrak" required
                                                   class="form-control form-white"
                                                   value="{{($kontrak != null) ? $kontrak->nomor_kontrak : null}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal Kontrak</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input id="tanggal_kontrak"
                                                   name="tanggal_kontrak"
                                                   type="text"
                                                   class="form-control b-datepicker form-white"
                                                   data-date-format="dd-mm-yyyy"
                                                   data-lang="en" data-RTL="false"
                                                   value="{{($kontrak != null) ? date('d-m-Y',strtotime($kontrak->tanggal_kontrak)) : null}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($kontrak != null)
                                <div class="tab-pane fade" id="lokasi">
                                    <div class="m-b-20">
                                        <div class="btn-group">
                                            <a href="{{url('eksternal/')}}"
                                               class="btn btn-success btn-square btn-block btn-embossed"><i
                                                        class="fa fa-plus"></i> Tambah Lokasi</a>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-dynamic">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Lokasi</th>
                                            <th>Jenis Lokasi</th>
                                            <th style="width: auto; ">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1;?>
                                        @foreach($lokasi as $dt)
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <a href="{{url('/eksternal/'.(($program->jenisProgram->acuan_1 == TIPE_AREA)?"create_area_program": "create_kontrak_program")."/".$id."/".$dt->id)}}"
                                                       class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                    <a href="{{url('/eksternal/delete_program/'.$dt->id)}}"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       onclick="return confirm('Apakah anda yakin untuk menghapus?')"><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php $no++;?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                        <hr class="p-t-0 m-t-0">
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <a href="{{url('/eksternal/create_program/'.$program->id)}}"
                                   class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
                                <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @endsection

            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                <!-- Buttons Loading State -->
                <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection