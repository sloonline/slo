@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/admin/md-layout4/material-design/css/material.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>{{($ap != null) ? "Ubah" : "Tambah"}} Data <strong>Area Program</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/create_program/'.$program->id)}}">Program Distribusi</a></li>
                    <li class="active">{{($ap != null) ? "Ubah" : "Tambah"}} Area</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(['url'=>'/eksternal/create_area_program','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    AREA</a></li>
                            @if($ap != null)
                                <li><a href="#child" data-toggle="tab"><i
                                                class="icon-speedometer"></i> {{$program->jenisProgram->acuan_2}}</a>
                                </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <input type="hidden" name="id" value="{{($ap != null) ? $ap->id : 0}}">
                                <div class="panel-content">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Area</label>
                                        </div>
                                        <div class="col-sm-9">
                                            @if($ap == null)
                                                <select class="form-control form-white" data-search="true" name="ba">
                                                    @foreach($ba as $item)
                                                        <option value="{{$item->id}}">{{$item->description}}</option>
                                                    @endforeach
                                                </select>
                                            @else

                                                <input type="hidden" name="ba"
                                                       value="{{$ap->business_area}}"/>
                                                <input type="text" class="form-control form-white" readonly
                                                       value="{{$ap->businessArea->description}}"
                                                       name="business_area">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Program</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="id_program" value="{{$program->id}}"/>
                                            <input type="text" class="form-control form-white" readonly
                                                   name="jenis_program"
                                                   value="{{$program->jenisProgram->jenis_program}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($ap != null)
                                <div class="tab-pane fade" id="child">
                                    <div class="m-b-20">
                                        <div class="btn-group">
                                            <div class="btn-group">
                                                <a href="{{url('eksternal/'.(($program->jenisProgram->acuan_2 == TIPE_LOKASI)?"create_lokasi_area": "create_penyulang_area")."/".$program->id."/".$id)}}"
                                                   class="btn btn-success btn-square btn-block btn-embossed"><i
                                                            class="fa fa-plus"></i> Tambah lokasi / Penyulang</a>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-dynamic">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            @if($program->jenisProgram->acuan_2 == TIPE_LOKASI)
                                                <th>Lokasi</th>
                                                <th>Jenis Lokasi</th>
                                            @else
                                                <th>Penyulang</th>
                                            @endif
                                            <th style="width: auto; ">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1;?>
                                        @foreach($lokasi_penyulang as $dt)
                                            <tr>
                                                <td>{{$no}}</td>
                                                @if($program->jenisProgram->acuan_2 == TIPE_LOKASI)
                                                    <td></td>
                                                    <td></td>
                                                @else
                                                    <td></td>
                                                @endif
                                                <td>
                                                    <a href="{{url('/eksternal/')}}"
                                                       class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                    <a href="{{url('/eksternal/delete_program/'.$dt->id)}}"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       onclick="return confirm('Apakah anda yakin untuk menghapus?')"><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php $no++;?>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                        <hr class="p-t-0 m-t-0">
                        <div class="panel-footer clearfix bg-white">
                            <div class="pull-right">
                                <input type="hidden" name="id" value="<?php echo ($ap != null) ? $ap->id : 0; ?>">
                                <a href="{{ url('/eksternal/create_program/'.$program->id_surat_tugas."/".$program->id) }}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;<i
                                            class="icon-ban"></i></a>
                                <button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"
                                        data-style="zoom-in">Simpan &nbsp;<i
                                            class="glyphicon glyphicon-floppy-saved"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @endsection

            @section('page_script')
                <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                <!-- Tables Filtering, Sorting & Editing -->
                <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                <!-- Buttons Loading State -->
                <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
@endsection