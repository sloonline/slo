@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/magnific/magnific-popup.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/hover-effects/hover-effects.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('/assets/global/plugins/slick/slick.css')}}"/>
    {{--<link rel="stylesheet" type="text/css" href="{{url('/assets/global/plugins/slick/slick-theme.css')}}"/>--}}
    <style>
        /*.slide {*/
        /*overflow: hidden;*/
        /*}*/

        /*.slick:nth-of-child(n+1) {*/
        /*display: none;*/
        /*}*/

        /*.slick:first-child {*/
        /*display: block;*/
        /*}*/
        /*.slider { display: none; }*/
        /*.slider.slick-initialized { display: block; }*/
    </style>
@endsection

@section('content')
    <div class="page-content page-thin">
        {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
        {{--<div class="panel">--}}
        {{--<div class="panel-header bg-primary">--}}
        {{--<h3><i class="icon-home"></i> <strong>Dashboard</strong></h3>--}}
        {{--</div>--}}
        {{--<div class="panel-content">--}}
        {{--<div class="slick" data-fade="true">--}}
        {{--<div class="slide">--}}
        {{--<img src="{{ url('/') }}/assets/global/images/widgets/login2.jpg" style="width: 100%" height="300">--}}
        {{--</div>--}}
        {{--<div class="slide">--}}
        {{--<img src="{{ url('/') }}/assets/global/images/widgets/login3.jpg" style="width: 100%" height="300">--}}
        {{--</div>--}}
        {{--<div class="slide">--}}
        {{--<img src="{{ url('/') }}/assets/global/images/widgets/login4.jpg" style="width: 100%" height="300">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-shopping-cart bg-green"></i>
                            </div>
                            <div class="right">
                                @if($permohonan_baru > 0)
                                    <p class="number countup" data-from="0"
                                       data-to="{{$permohonan_baru}}">{{$permohonan_baru}}</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">Permohonan Baru</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-bolt bg-blue"></i>
                            </div>
                            <div class="right">
                                @if($jumlah_instalasi > 0)
                                    <p class="number countup" data-from="0" data-to="{{$jumlah_instalasi}}">0</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">Instalasi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-send bg-red"></i>
                            </div>
                            <div class="right">
                                @if($lhpp > 0)
                                    <p class="number countup" data-from="0" data-to="{{$lhpp}}">0</p>
                                @else
                                    <p class="number">0</p>
                                @endif
                                <p class="text">LHPP</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="panel">
                    <div class="panel-content widget-info">
                        <div class="row">
                            <div class="left">
                                <i class="fa fa-file-text-o bg-purple"></i>
                            </div>
                            <div class="right">
                                <p class="number countup" data-from="0" data-to="6">0</p>
                                <p class="text">LPI</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header bg-primary">
                        <h3><i class="icon-energy"></i> <strong>Instalasi</strong></h3>
                        <div class="control-btn">
                            {{--<a href="{{url('eksternal/view_instalasi_pembangkit')}}">Semua Instalasi&nbsp;&raquo;</a>--}}
                        </div>
                    </div>
                    {{--<div class="panel-content p-t-5">--}}

                    <div class="panel-content">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1_1" data-toggle="tab">Pembangkit</a></li>
                            <li class=""><a href="#tab1_2" data-toggle="tab">Transmisi</a></li>
                            @if(Auth::user()->jenis_user == "PLN")
                                <li class=""><a href="#tab1_5" data-toggle="tab">Distribusi</a></li>
                            @endif
                            @if(Auth::user()->jenis_user != TIPE_PLN)
                                <li><a href="#tab1_3" data-toggle="tab">Pemanfaatan TT</a></li>
                                <li><a href="#tab1_4" data-toggle="tab">Pemanfaatan TM</a></li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1_1">
                                @if($pembangkit->count()!=0)

                                    <div class="widget widget_slider p-b-0">
                                        <div class="slick" data-arrows="true" data-num-slides="4" data-num-scroll="4"
                                             data-autoplay="false">
                                            @foreach($pembangkit as $wa)
                                                <div class="slide text-center" style="margin: 10px;">
                                                    <img style="margin: 0 auto; width: 200px;height: 150px;"
                                                         src="{{(@$wa->foto_1 != null) ?  url('upload/instalasi/'.@$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                                         class="img-thumbnail"
                                                         onmouseover="this.style.cursor='pointer';"
                                                         onclick="window.location.href='{{url('eksternal/instalasi-pembangkit/'.@$wa->id)}}';">
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pembangkit/'.@$wa->id)}}';">
                                                        <strong>{{@$wa->nama_instalasi}}</strong></h5>
                                                    {{--                          <h5 onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/instalasi-pembangkit/'.@$wa->id)}}';">{{@$wa->jenis_instalasi->jenis_instalasi}}</h5>--}}
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pembangkit/'.@$wa->id)}}';">{{@$wa->pemilik->nama_pemilik}}</h5>
                                                    {{--<br>--}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div style="text-align: right;">
                                        <a href="{{url('eksternal/view_instalasi_pembangkit')}}">Semua
                                            Instalasi&nbsp;&raquo;</a>
                                    </div>

                                @else
                                    <div class="" style="text-align: center;">
                                        <br/>
                                        <div style="text-align: center;">Anda belum memiliki instalasi pembangkit. Klik
                                            tombol <strong>Tambah Instalasi</strong> untuk membuat instalasi baru.
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-success"
                                                onclick="window.location.href='{{url('eksternal/create_instalasi_pembangkit')}}'">
                                            <i class="fa fa-plus"></i> Tambah Instalasi
                                        </button>
                                        {{--<div class="btn-group">--}}
                                        {{--<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus"></i> Tambah Instalasi <span class="caret"></span>--}}
                                        {{--</button>--}}
                                        {{--<span class="dropdown-arrow"></span>--}}
                                        {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pembangkit')}}">Pembangkit</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_transmisi')}}">Transmisi</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tt')}}">Pemanfaatan TT</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tm')}}">Pemanfaatan TM</a>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                        <br/>
                                        <br/>
                                    </div>
                                @endif
                            </div>

                            <div class="tab-pane fade" id="tab1_2">
                                @if($transmisi->count()!=0)

                                    <div class="widget widget_slider p-b-0">
                                        <div class="slick" data-arrows="true" data-num-slides="4" data-num-scroll="4"
                                             data-autoplay="false">
                                            @foreach($transmisi as $wa)
                                                <div class="slide text-center" style="margin: 10px;">
                                                    <img style="margin: 0 auto; width: 200px;height: 150px;"
                                                         src="{{(@$wa->foto_1 != null) ?  url('upload/instalasi/'.@$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                                         class="img-thumbnail"
                                                         onmouseover="this.style.cursor='pointer';"
                                                         onclick="window.location.href='{{url('eksternal/instalasi-transmisi/'.@$wa->id)}}';">
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-transmisi/'.@$wa->id)}}';">
                                                        <strong>{{@$wa->nama_instalasi}}</strong></h5>
                                                    {{--                          <h5 onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/instalasi-transmisi/'.@$wa->id)}}';">{{@$wa->jenis_instalasi->jenis_instalasi}}</h5>--}}
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-transmisi/'.@$wa->id)}}';">{{@$wa->pemilik->nama_pemilik}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div style="text-align: right;">
                                        <a href="{{url('eksternal/view_instalasi_traansmisi')}}">Semua
                                            Instalasi&nbsp;&raquo;</a>
                                    </div>

                                @else
                                    <div class="" style="text-align: center;">
                                        <br/>
                                        <div style="text-align: center;">Anda belum memiliki instalasi transmisi. Klik
                                            tombol <strong>Tambah Instalasi</strong> untuk membuat instalasi baru.
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-success"
                                                onclick="window.location.href='{{url('eksternal/create_instalasi_transmisi')}}'">
                                            <i class="fa fa-plus"></i> Tambah Instalasi
                                        </button>
                                        {{--<div class="btn-group">--}}
                                        {{--<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus"></i> Tambah Instalasi <span class="caret"></span>--}}
                                        {{--</button>--}}
                                        {{--<span class="dropdown-arrow"></span>--}}
                                        {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pembangkit')}}">Pembangkit</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_transmisi')}}">Transmisi</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tt')}}">Pemanfaatan TT</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tm')}}">Pemanfaatan TM</a>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                        <br/>
                                        <br/>
                                    </div>
                                @endif
                            </div>

                            <div class="tab-pane fade" id="tab1_3">

                                @if($pemanfaatan_tt->count()!=0)

                                    <div class="widget widget_slider p-b-0">
                                        <div class="slick" data-arrows="true" data-num-slides="4" data-num-scroll="4"
                                             data-autoplay="false">
                                            @foreach($pemanfaatan_tt as $wa)
                                                <div class="slide text-center" style="margin: 10px;">
                                                    <img style="margin: 0 auto; width: 200px;height: 150px;"
                                                         src="{{(@$wa->foto_1 != null) ?  url('upload/instalasi/'.@$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                                         class="img-thumbnail"
                                                         onmouseover="this.style.cursor='pointer';"
                                                         onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tt/'.@$wa->id)}}';">
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tt/'.@$wa->id)}}';">
                                                        <strong>{{@$wa->nama_instalasi}}</strong></h5>
                                                    {{--                          <h5 onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tt/'.@$wa->id)}}';">{{@$wa->jenis_instalasi->jenis_instalasi}}</h5>--}}
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tt/'.@$wa->id)}}';">{{@$wa->pemilik->nama_pemilik}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div style="text-align: right;">
                                        <a href="{{url('eksternal/view_instalasi_pemanfaatan_tt')}}">Semua Instalasi&nbsp;&raquo;</a>
                                    </div>

                                @else
                                    <div class="" style="text-align: center;">
                                        <br/>
                                        <div style="text-align: center;">Anda belum memiliki instalasi pemanfaatan
                                            tegangan tinggi. Klik tombol <strong>Tambah Instalasi</strong> untuk membuat
                                            instalasi baru.
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-success"
                                                onclick="window.location.href='{{url('eksternal/create_instalasi_pemanfaatan_tt')}}'">
                                            <i class="fa fa-plus"></i> Tambah Instalasi
                                        </button>
                                        {{--<div class="btn-group">--}}
                                        {{--<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus"></i> Tambah Instalasi <span class="caret"></span>--}}
                                        {{--</button>--}}
                                        {{--<span class="dropdown-arrow"></span>--}}
                                        {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pembangkit')}}">Pembangkit</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_transmisi')}}">Transmisi</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tt')}}">Pemanfaatan TT</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tm')}}">Pemanfaatan TM</a>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                        <br/>
                                        <br/>
                                    </div>
                                @endif
                            </div>

                            <div class="tab-pane fade" id="tab1_4">

                                @if($pemanfaatan_tm->count()!=0)

                                    <div class="widget widget_slider p-b-0">
                                        <div class="slick" data-arrows="true" data-num-slides="4" data-num-scroll="4"
                                             data-autoplay="false">
                                            @foreach($pemanfaatan_tm as $wa)
                                                <div class="slide text-center" style="margin: 10px;">
                                                    <img style="margin: 0 auto; width: 200px;height: 150px;"
                                                         src="{{(@$wa->foto_1 != null) ?  url('upload/instalasi/'.@$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                                         class="img-thumbnail"
                                                         onmouseover="this.style.cursor='pointer';"
                                                         onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tm/'.@$wa->id)}}';">
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tm/'.@$wa->id)}}';">
                                                        <strong>{{@$wa->nama_instalasi}}</strong></h5>
                                                    {{--                          <h5 onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tm/'.@$wa->id)}}';">{{@$wa->jenis_instalasi->jenis_instalasi}}</h5>--}}
                                                    <h5 onmouseover="this.style.cursor='pointer';"
                                                        onclick="window.location.href='{{url('eksternal/instalasi-pemanfaatan-tm/'.@$wa->id)}}';">{{@$wa->pemilik->nama_pemilik}}</h5>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div style="text-align: right;">
                                        <a href="{{url('eksternal/view_instalasi_pemanfaatan_tm')}}">Semua Instalasi&nbsp;&raquo;</a>
                                    </div>

                                @else
                                    <div class="" style="text-align: center;">
                                        <br/>
                                        <div style="text-align: center;">Anda belum memiliki instalasi pemanfaatan
                                            tegangan menengah. Klik tombol <strong>Tambah Instalasi</strong> untuk
                                            membuat instalasi baru.
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-success"
                                                onclick="window.location.href='{{url('eksternal/create_instalasi_pemanfaatan_tm')}}'">
                                            <i class="fa fa-plus"></i> Tambah Instalasi
                                        </button>
                                        {{--<div class="btn-group">--}}
                                        {{--<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus"></i> Tambah Instalasi <span class="caret"></span>--}}
                                        {{--</button>--}}
                                        {{--<span class="dropdown-arrow"></span>--}}
                                        {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pembangkit')}}">Pembangkit</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_transmisi')}}">Transmisi</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tt')}}">Pemanfaatan TT</a>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="{{url('eksternal/create_instalasi_pemanfaatan_tm')}}">Pemanfaatan TM</a>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--</div>--}}
                                        <br/>
                                        <br/>
                                    </div>
                                @endif
                            </div>
                            @if(Auth::user()->jenis_user == "PLN")
                                <div class="tab-pane fade" id="tab1_5">

                                    @if($distribusi->count()!=0)

                                        <div class="widget widget_slider p-b-0">
                                            <div class="slick" data-arrows="true" data-num-slides="4"
                                                 data-num-scroll="4"
                                                 data-autoplay="false">
                                                @foreach($distribusi as $wa)
                                                    <div class="slide text-center" style="margin: 10px;">
                                                        <img style="margin: 0 auto; width: 200px;height: 150px;"
                                                             src="{{(@$wa->foto_1 != null) ?  url('upload/instalasi/'.@$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                                             class="img-thumbnail"
                                                             onmouseover="this.style.cursor='pointer';"
                                                             onclick="window.location.href='{{url('eksternal/instalasi-distribusi/'.@$wa->id)}}';">
                                                        <h5 onmouseover="this.style.cursor='pointer';"
                                                            onclick="window.location.href='{{url('eksternal/instalasi-distribusi/'.@$wa->id)}}';">
                                                            <strong>{{@$wa->nama_instalasi}}</strong></h5>
                                                        {{--                          <h5 onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/instalasi-pembangkit/'.@$wa->id)}}';">{{@$wa->jenis_instalasi->jenis_instalasi}}</h5>--}}
                                                        <h5 onmouseover="this.style.cursor='pointer';"
                                                            onclick="window.location.href='{{url('eksternal/instalasi-distribusi/'.@$wa->id)}}';">{{@$wa->pemilik->nama_pemilik}}</h5>
                                                        {{--<br>--}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div style="text-align: right;">
                                            <a href="{{url('eksternal/view_instalasi_distribusi')}}">Semua
                                                Instalasi&nbsp;&raquo;</a>
                                        </div>

                                    @else
                                        <div class="" style="text-align: center;">
                                            <br/>
                                            <div style="text-align: center;">Anda belum memiliki instalasi Distribusi.
                                                Klik
                                                tombol <strong>Tambah Instalasi</strong> untuk membuat instalasi baru.
                                            </div>
                                            <br>
                                            <button type="button" class="btn btn-success"
                                                    onclick="window.location.href='{{url('eksternal/create_instalasi_distribusi')}}'">
                                                <i class="fa fa-plus"></i> Tambah Instalasi
                                            </button>
                                            <br/>
                                            <br/>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>

                    {{--</div>--}}
                </div>


            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="slick" data-fade="true">
                    <div class="slide">
                        <img src="{{ url('/') }}/assets/global/images/widgets/3500.jpg" style="width: 100%">
                    </div>
                    <div class="slide">
                        <img src="{{ url('/') }}/assets/global/images/widgets/3_langkah.png" style="width: 100%">
                    </div>
                    <div class="slide">
                        <img src="{{ url('/') }}/assets/global/images/widgets/cc123.jpg" style="width: 100%">
                    </div>
                    <div class="slide">
                        <img src="{{ url('/') }}/assets/global/images/widgets/1kwh.jpg" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
        {{--<div class="panel">--}}
        {{--<div class="panel-header bg-primary">--}}
        {{--<h3><i class="icon-book-open"></i> <strong>Layanan</strong></h3>--}}
        {{--</div>--}}
        {{--<div class="panel-content">--}}
        {{--<div class="portfolioFilter text-center">--}}
        {{--<a href="#" data-filter="*" class="current btn btn-sm btn-blue btn-square btn-embossed"><h3--}}
        {{--class="c-white m-10">Semua Layanan</h3></a>--}}
        {{--<a href="#" data-filter=".pembangkit" class="btn btn-sm btn-danger btn-square btn-embossed">--}}
        {{--<h3 class="c-white m-10">Pembangkit</h3></a>--}}
        {{--<a href="#" data-filter=".transmisi" class="btn btn-sm btn-success btn-square btn-embossed">--}}
        {{--<h3 class="c-white m-10">Transmisi</h3></a>--}}
        {{--@if(Auth::user()->jenis_user == "PLN")--}}
        {{--<a href="#" data-filter=".distribusi"--}}
        {{--class="btn btn-sm btn-info btn-square btn-embossed">--}}
        {{--<h3 class="c-white m-10">Distribusi</h3></a>--}}
        {{--@endif--}}
        {{--<a href="#" data-filter=".pemanfaatan_tt"--}}
        {{--class="btn btn-sm btn-warning btn-square btn-embossed"><h3 class="c-white m-10">--}}
        {{--Pemanfaatan TT</h3></a>--}}
        {{--<a href="#" data-filter=".pemanfaatan_tm"--}}
        {{--class="btn btn-sm btn-primary btn-square btn-embossed"><h3 class="c-white m-10">--}}
        {{--Pemanfaatan TM</h3></a>--}}
        {{--</div>--}}
        {{--<hr style="margin-top: -10px;">--}}
        {{--<div class="portfolioContainer grid">--}}
        {{--@foreach($layanan_kit as $wa)--}}
        <?php //$url = url('/eksternal/layanan/' . $wa->id . '/1');?>
        {{--
        <div class="col-md-3 pembangkit p-b-20 text-center">--}}
            {{--<img width="100%" src="{{ url('/assets/global/images/produk/'.@$wa->foto) }}" --}}
                     {{--class="img-thumbnail" onmouseover="this.style.cursor='pointer';" --}}
                     {{--onclick="window.location.href='{{$url}}';"/>--}}
            {{--<h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">--}}
                {{--<strong>{{@$wa->produk_layanan}}</strong></h5>--}}
            {{--<h5 class="m-0" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">Pembangkit</h5>--}}
            {{--
        </div>
        --}}
        {{--@endforeach--}}

        {{--@foreach($layanan_trs as $wa)--}}
        <?php //$url = url('/eksternal/layanan/' . $wa->id . '/2');?>
        {{--
        <div class="col-md-3 transmisi p-b-20 text-center">--}}
            {{--<img width="100%" src="{{ url('/assets/global/images/produk/'.@$wa->foto) }}" --}}
                     {{--class="img-thumbnail" onmouseover="this.style.cursor='pointer';" --}}
                     {{--onclick="window.location.href='{{$url}}';"/>--}}
            {{--<h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">--}}
                {{--<strong>{{@$wa->produk_layanan}}</strong></h5>--}}
            {{--<h5 class="m-0" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">Transmisi</h5>--}}
            {{--
        </div>
        --}}
        {{--@endforeach--}}

        {{--@if(Auth::user()->jenis_user == "PLN")--}}
        {{--@foreach($layanan_dis as $wa)--}}
        <?php //$url = url('/eksternal/layanan/' . $wa->id . '/3');?>
        {{--
        <div class="col-md-3 distribusi p-b-20 text-center">--}}
            {{--<img width="100%" src="{{ url('/assets/global/images/produk/'.@$wa->foto) }}" --}}
                     {{--class="img-thumbnail" onmouseover="this.style.cursor='pointer';" --}}
                     {{--onclick="window.location.href='{{$url}}';"/>--}}
            {{--<h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">--}}
                {{--<strong>{{@$wa->produk_layanan}}</strong></h5>--}}
            {{--<h5 class="m-0" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">Distribusi</h5>--}}
            {{--
        </div>
        --}}
        {{--@endforeach--}}
        {{--@endif--}}

        {{--@foreach($layanan_ptt as $wa)--}}
        <?php //$url = url('/eksternal/layanan/' . $wa->id . '/4');?>
        {{--
        <div class="col-md-3 pemanfaatan_tt p-b-20 text-center">--}}
            {{--<img width="100%" src="{{ url('/assets/global/images/produk/'.@$wa->foto) }}" --}}
                     {{--class="img-thumbnail" onmouseover="this.style.cursor='pointer';" --}}
                     {{--onclick="window.location.href='{{$url}}';"/>--}}
            {{--<h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">--}}
                {{--<strong>{{@$wa->produk_layanan}}</strong></h5>--}}
            {{--<h5 class="m-0" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">Pemanfaatan TT</h5>--}}
            {{--
        </div>
        --}}
        {{--@endforeach--}}

        {{--@foreach($layanan_ptm as $wa)--}}
        <?php //$url = url('/eksternal/layanan/' . $wa->id . '/5');?>
        {{--
        <div class="col-md-3 pemanfaatan_tm p-b-20 text-center">--}}
            {{--<img width="100%" src="{{ url('/assets/global/images/produk/'.@$wa->foto) }}" --}}
                     {{--class="img-thumbnail" onmouseover="this.style.cursor='pointer';" --}}
                     {{--onclick="window.location.href='{{$url}}';"/>--}}
            {{--<h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">--}}
                {{--<strong>{{@$wa->produk_layanan}}</strong></h5>--}}
            {{--<h5 class="m-0" onmouseover="this.style.cursor='pointer';" --}}
                    {{--onclick="window.location.href='{{$url}}';">Pemanfaatan TM</h5>--}}
            {{--
        </div>
        --}}
        {{--@endforeach--}}
        {{--
    </div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    @endsection

    @section('page_script')
    <script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/slick/slick.min.js"></script> <!-- Slider -->
    <script src="{{ url('/') }}/assets/global/plugins/magnific/jquery.magnific-popup.min.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/isotope/isotope.pkgd.min.js"></script>
    <script>
        $(window).load(function () {
            var $container = $('.portfolioContainer');
            $container.isotope();
            $('.portfolioFilter a').click(function () {
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');
                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector
                });
                return false;
            });
        });

        $(document).ready(function () {
            $('.slick3').slick({});
        });

    </script>
    @endsection
