@extends('../layout/layout_eksternal')

@section('page_css')
<!-- BEGIN PAGE STYLE -->
<link href="../assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
<link href="../assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<!-- END PAGE STYLE -->
@endsection

@section('content')
        <!-- BEGIN PAGE CONTENT -->
<div class="page-content">
    <div class="header">
        <h2>Permohonan <strong>Transmisi</strong></h2>
        <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="#">Produk</a>
                </li>
                <li><a href="#">Input</a>
                </li>
                <li class="active">Pembangkit</li>
            </ol>
        </div>
    </div>
    {{--Begin form input--}}
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header md-panel-controls">
                    <h3><i class="icon-bulb"></i> Form <strong>Input</strong></h3>
                </div>
                <div class="panel-content">
                    <p>Silahkan inputkan data pemohon dan data pembangkit yang hendak diajukan dalam permohonan ini</p>
                    {!! Form::open(['url'=>'pembangkit','post']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <h1><b>Data Pemohon</b></h1>
                            <div class="form-group">
                                <label class="form-label">Tanggal Surat</label>
                                <input type="text" class="form-control" placeholder="Tanggal Surat">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama Pemohon</label>
                                <input type="text" class="form-control" placeholder="Nama pemohon">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jabatan</label>
                                <input type="text" class="form-control" placeholder="Jabatan">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Email</label>
                                <input type="text" class="form-control" placeholder="Masukan email">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Telepon</label>
                                <input type="text" class="form-control" placeholder="Masukan telepon">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control" placeholder="Masukan alamat"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Permohonan</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h1><b>Data Pembangkit</b></h1>
                            <div class="form-group">
                                <label>Pilih Provinsi</label>
                                <select class="form-control form-white" data-style="white"
                                        data-placeholder="Select a country...">
                                    <option value="1">Jawa Timur</option>
                                    <option value="2">Jawa Barat</option>
                                    <option value="3">Bali</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Pilih Kota/Kab</label>
                                <select class="form-control form-white" data-style="white"
                                        data-placeholder="Select a country...">
                                    <option value="1">Pilih Kota</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Lokasi Pembangkit</label>
                                <input type="text" class="form-control" placeholder="Masukan lokasi">
                            </div>
                            <div class="form-group">
                                <label>Jenis Pembangkit</label>
                                <select class="form-control form-white" data-style="white"
                                        data-placeholder="Select a country...">
                                    <option value="1">Pembangkit listrik tenaga uap</option>
                                    <option value="2">Pembangkit listrik tenaga air</option>
                                    <option value="3">Pembangkit listrik tenaga diesel</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jenis Usaha Penyedia Tenaga Listrik</label>
                                <select class="form-control form-white" data-style="white"
                                        data-placeholder="Select a country...">
                                    <option value="1">IO</option>
                                    <option value="2">Pelanggan</option>
                                    <option value="3">PIUPTL</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Bahan Bakar</label>
                                <select class="form-control form-white" data-style="white"
                                        data-placeholder="Select a country...">
                                    <option value="1">Batubara</option>
                                    <option value="2">Air</option>
                                    <option value="3">Gas</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kapasitas</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" placeholder="Kapasitas">
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control form-white col-sm-6" data-style="white"
                                                data-placeholder="Select a country...">
                                            <option value="1">-Pilih satuan-</option>
                                            <option value="2">kVA</option>
                                            <option value="3">kW</option>
                                            <option value="3">kWp</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <h1><b>Surat Perjanjian Jual Beli Tenaga Listrik</b></h1>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Nomor</label>
                                <input type="text" class="form-control" placeholder="Nomor Surat">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal</label>
                                <input type="text" class="form-control" placeholder="Tanggal Surat">
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jual Beli/SIT/ID Pelanggan</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">File Single Line Diagram</label>
                                <input type="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Gambar Instalasi dan Tata Letak</label>
                                <input type="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jenis dan Kapasitas Instalasi</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">File Spesifikasi Peralatan Utama Instalasi</label>
                                <input type="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Ijin Operasi</label>
                                <input type="file" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-label">File NPWP</label>
                                <input type="file" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-8">
                            <button type="button" class="btn btn-warning">Reset</button>
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    {{--End form input--}}
    <div class="footer">
        <div class="copyright">
            <p class="pull-left sm-pull-reset">
                <span>Copyright <span class="copyright">©</span> 2015 </span>
                <span>THEMES LAB</span>.
                <span>All rights reserved. </span>
            </p>
            <p class="pull-right sm-pull-reset">
                        <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of
                                use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
            </p>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END MAIN CONTENT -->

<!-- BEGIN MODAL SELECT -->
<div class="modal fade" id="modal-select" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="icons-office-52"></i></button>
                <h4 class="modal-title"><strong>Select Input</strong> inside modal</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select with search</label>
                                        <select class="form-control" data-search="true">
                                            <option value="United States">United States</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Bouvet Island">Bouvet Island</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Territory">British Indian Ocean
                                                Territory
                                            </option>
                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic
                                            </option>
                                            <option value="Chad">Chad</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Congo, The Democratic Republic of The">Congo, The
                                                Democratic Republic of The
                                            </option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote D'ivoire">Cote D'ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands (Malvinas)">Falkland Islands
                                                (Malvinas)
                                            </option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Territories">French Southern
                                                Territories
                                            </option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea-bissau">Guinea-bissau</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Heard Island and Mcdonald Islands">Heard Island and
                                                Mcdonald Islands
                                            </option>
                                            <option value="Holy See (Vatican City State)">Holy See (Vatican City
                                                State)
                                            </option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="India">India</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of
                                            </option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea, Democratic People's Republic of">Korea, Democratic
                                                People's Republic of
                                            </option>
                                            <option value="Korea, Republic of">Korea, Republic of</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Lao People's Democratic Republic">Lao People's Democratic
                                                Republic
                                            </option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macao">Macao</option>
                                            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia,
                                                The Former Yugoslav Republic of
                                            </option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Micronesia, Federated States of">Micronesia, Federated
                                                States of
                                            </option>
                                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherlands">Netherlands</option>
                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Northern Mariana Islands">Northern Mariana Islands
                                            </option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau">Palau</option>
                                            <option value="Palestinian Territory, Occupied">Palestinian Territory,
                                                Occupied
                                            </option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Philippines">Philippines</option>
                                            <option value="Pitcairn">Pitcairn</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russian Federation">Russian Federation</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Saint Helena">Saint Helena</option>
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                            <option value="Saint Lucia">Saint Lucia</option>
                                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon
                                            </option>
                                            <option value="Saint Vincent and The Grenadines">Saint Vincent and The
                                                Grenadines
                                            </option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Georgia and The South Sandwich Islands">South
                                                Georgia and The South Sandwich Islands
                                            </option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                            <option value="Taiwan, Province of China">Taiwan, Province of China
                                            </option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania, United Republic of">Tanzania, United Republic
                                                of
                                            </option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Timor-leste">Timor-leste</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands
                                            </option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="United States">United States</option>
                                            <option value="United States Minor Outlying Islands">United States Minor
                                                Outlying Islands
                                            </option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Viet Nam">Viet Nam</option>
                                            <option value="Virgin Islands, British">Virgin Islands, British</option>
                                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                            <option value="Wallis and Futuna">Wallis and Futuna</option>
                                            <option value="Western Sahara">Western Sahara</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Multiple Select</label>
                                        <select multiple class="form-control"
                                                data-placeholder="Choose one or various country...">
                                            <optgroup label="North America">
                                                <option value="USA">USA</option>
                                                <option value="CANADA">Canada</option>
                                                <option value="MEXICO">Mexico</option>
                                            </optgroup>
                                            <optgroup label="South America">
                                                <option value="USA">Brazil</option>
                                                <option value="peru">Peru</option>
                                                <option value="argentina">Argentina</option>
                                            </optgroup>
                                            <optgroup label="Europe">
                                                <option value="SPANISH">Spain</option>
                                                <option value="FRENCH">France</option>
                                                <option value="UNITED-KINGDOM">United Kingdom</option>
                                                <option value="ITALY">Italy</option>
                                                <option value="SPANISH">Deutshland</option>
                                                <option value="FRENCH">France</option>
                                                <option value="Belgium">Belgium</option>
                                            </optgroup>
                                            <optgroup label="Africa">
                                                <option value="Angola">Angola</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Ghana">Ghana</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tags Input</label>
                                        <div>
                                            <input class="select-tags form-control" value="brown,red,green">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>White Look</label>
                                        <select class="form-control form-white" data-style="white"
                                                data-placeholder="Select a country...">
                                            <option value="USA">USA</option>
                                            <option value="CANADA">Canada</option>
                                            <option value="MEXICO">Mexico</option>
                                            <option value="SPANISH">Spain</option>
                                            <option value="FRENCH">France</option>
                                            <option value="UNITED-KINGDOM">United Kingdom</option>
                                            <option value="ITALY">Italy</option>
                                            <option value="SPANISH">Deutshland</option>
                                            <option value="FRENCH">France</option>
                                            <option value="Belgium">Belgium</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-gray-light">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Save changes
                </button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL SELECT -->

<!-- BEGIN BUILDER -->
<div class="builder hidden-sm hidden-xs" id="builder">
    <a class="builder-toggle"><i class="icon-wrench"></i></a>
    <div class="inner">
        <div class="builder-container">
            <a href="#" class="btn btn-sm btn-default" id="reset-style">reset default style</a>
            <h4>Layout options</h4>
            <div class="layout-option">
                <span> Fixed Sidebar</span>
                <label class="switch pull-right">
                    <input data-layout="sidebar" id="switch-sidebar" type="checkbox" class="switch-input" checked>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <div class="layout-option">
                <span> Sidebar on Hover</span>
                <label class="switch pull-right">
                    <input data-layout="sidebar-hover" id="switch-sidebar-hover" type="checkbox"
                           class="switch-input">
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <div class="layout-option">
                <span> Submenu on Hover</span>
                <label class="switch pull-right">
                    <input data-layout="submenu-hover" id="switch-submenu-hover" type="checkbox"
                           class="switch-input">
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <div class="layout-option">
                <span>Fixed Topbar</span>
                <label class="switch pull-right">
                    <input data-layout="topbar" id="switch-topbar" type="checkbox" class="switch-input" checked>
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <div class="layout-option">
                <span>Boxed Layout</span>
                <label class="switch pull-right">
                    <input data-layout="boxed" id="switch-boxed" type="checkbox" class="switch-input">
                    <span class="switch-label" data-on="On" data-off="Off"></span>
                    <span class="switch-handle"></span>
                </label>
            </div>
            <h4 class="border-top">Color</h4>
            <div class="row">
                <div class="col-xs-12">
                    <div class="theme-color bg-dark" data-main="default" data-color="#2B2E33"></div>
                    <div class="theme-color background-primary" data-main="primary" data-color="#319DB5"></div>
                    <div class="theme-color bg-red" data-main="red" data-color="#C75757"></div>
                    <div class="theme-color bg-green" data-main="green" data-color="#1DA079"></div>
                    <div class="theme-color bg-orange" data-main="orange" data-color="#D28857"></div>
                    <div class="theme-color bg-purple" data-main="purple" data-color="#B179D7"></div>
                    <div class="theme-color bg-blue" data-main="blue" data-color="#4A89DC"></div>
                </div>
            </div>
            <h4 class="border-top">Theme</h4>
            <div class="row row-sm">
                <div class="col-xs-6">
                    <div class="theme clearfix sdtl" data-theme="sdtl">
                        <div class="header theme-left"></div>
                        <div class="header theme-right-light"></div>
                        <div class="theme-sidebar-dark"></div>
                        <div class="bg-light"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="theme clearfix sltd" data-theme="sltd">
                        <div class="header theme-left"></div>
                        <div class="header theme-right-dark"></div>
                        <div class="theme-sidebar-light"></div>
                        <div class="bg-light"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="theme clearfix sdtd" data-theme="sdtd">
                        <div class="header theme-left"></div>
                        <div class="header theme-right-dark"></div>
                        <div class="theme-sidebar-dark"></div>
                        <div class="bg-light"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="theme clearfix sltl" data-theme="sltl">
                        <div class="header theme-left"></div>
                        <div class="header theme-right-light"></div>
                        <div class="theme-sidebar-light"></div>
                        <div class="bg-light"></div>
                    </div>
                </div>
            </div>
            <h4 class="border-top">Background</h4>
            <div class="row">
                <div class="col-xs-12">
                    <div class="bg-color bg-clean" data-bg="clean" data-color="#F8F8F8"></div>
                    <div class="bg-color bg-lighter" data-bg="lighter" data-color="#EFEFEF"></div>
                    <div class="bg-color bg-light-default" data-bg="light-default" data-color="#E9E9E9"></div>
                    <div class="bg-color bg-light-blue" data-bg="light-blue" data-color="#E2EBEF"></div>
                    <div class="bg-color bg-light-purple" data-bg="light-purple" data-color="#E9ECF5"></div>
                    <div class="bg-color bg-light-dark" data-bg="light-dark" data-color="#DCE1E4"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END BUILDER -->
</section>
<!-- BEGIN QUICKVIEW SIDEBAR -->
<div id="quickview-sidebar">
    <div class="quickview-header">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#chat" data-toggle="tab">Chat</a></li>
            <li><a href="#notes" data-toggle="tab">Notes</a></li>
            <li><a href="#settings" data-toggle="tab" class="settings-tab">Settings</a></li>
        </ul>
    </div>
    <div class="quickview">
        <div class="tab-content">
            <div class="tab-pane fade active in" id="chat">
                <div class="chat-body current">
                    <div class="chat-search">
                        <form class="form-inverse" action="#" role="search">
                            <div class="append-icon">
                                <input type="text" class="form-control" placeholder="Search contact...">
                                <i class="icon-magnifier"></i>
                            </div>
                        </form>
                    </div>
                    <div class="chat-groups">
                        <div class="title">GROUP CHATS</div>
                        <ul>
                            <li><i class="turquoise"></i> Favorites</li>
                            <li><i class="turquoise"></i> Office Work</li>
                            <li><i class="turquoise"></i> Friends</li>
                        </ul>
                    </div>
                    <div class="chat-list">
                        <div class="title">FAVORITES</div>
                        <ul>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar13.png" alt="avatar"/>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">Bobby Brown</div>
                                    <div class="user-txt">On the road again...</div>
                                </div>
                                <div class="user-status">
                                    <i class="online"></i>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar5.png" alt="avatar"/>
                                    <div class="pull-right badge badge-danger">3</div>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">Alexa Johnson</div>
                                    <div class="user-txt">Still at the beach</div>
                                </div>
                                <div class="user-status">
                                    <i class="away"></i>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar10.png" alt="avatar"/>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">Bobby Brown</div>
                                    <div class="user-txt">On stage...</div>
                                </div>
                                <div class="user-status">
                                    <i class="busy"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="chat-list">
                        <div class="title">FRIENDS</div>
                        <ul>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar7.png" alt="avatar"/>
                                    <div class="pull-right badge badge-danger">3</div>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">James Miller</div>
                                    <div class="user-txt">At work...</div>
                                </div>
                                <div class="user-status">
                                    <i class="online"></i>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar11.png" alt="avatar"/>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">Fred Smith</div>
                                    <div class="user-txt">Waiting for tonight</div>
                                </div>
                                <div class="user-status">
                                    <i class="offline"></i>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="user-img">
                                    <img src="../assets/global/images/avatars/avatar8.png" alt="avatar"/>
                                </div>
                                <div class="user-details">
                                    <div class="user-name">Ben Addams</div>
                                    <div class="user-txt">On my way to NYC</div>
                                </div>
                                <div class="user-status">
                                    <i class="offline"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="chat-conversation">
                    <div class="conversation-header">
                        <div class="user clearfix">
                            <div class="chat-back">
                                <i class="icon-action-undo"></i>
                            </div>
                            <div class="user-details">
                                <div class="user-name">James Miller</div>
                                <div class="user-txt">On the road again...</div>
                            </div>
                        </div>
                    </div>
                    <div class="conversation-body">
                        <ul>
                            <li class="img">
                                <div class="chat-detail">
                                    <span class="chat-date">today, 10:38pm</span>
                                    <div class="conversation-img">
                                        <img src="../assets/global/images/avatars/avatar4.png" alt="avatar 4"/>
                                    </div>
                                    <div class="chat-bubble">
                                        <span>Hi you!</span>
                                    </div>
                                </div>
                            </li>
                            <li class="img">
                                <div class="chat-detail">
                                    <span class="chat-date">today, 10:45pm</span>
                                    <div class="conversation-img">
                                        <img src="../assets/global/images/avatars/avatar4.png" alt="avatar 4"/>
                                    </div>
                                    <div class="chat-bubble">
                                        <span>Are you there?</span>
                                    </div>
                                </div>
                            </li>
                            <li class="img">
                                <div class="chat-detail">
                                    <span class="chat-date">today, 10:51pm</span>
                                    <div class="conversation-img">
                                        <img src="../assets/global/images/avatars/avatar4.png" alt="avatar 4"/>
                                    </div>
                                    <div class="chat-bubble">
                                        <span>Send me a message when you come back.</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="conversation-message">
                        <input type="text" placeholder="Your message..." class="form-control form-white send-message"/>
                        <div class="item-footer clearfix">
                            <div class="footer-actions">
                                <i class="icon-rounded-marker"></i>
                                <i class="icon-rounded-camera"></i>
                                <i class="icon-rounded-paperclip-oblique"></i>
                                <i class="icon-rounded-alarm-clock"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="notes">
                <div class="list-notes current withScroll">
                    <div class="notes ">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="add-note">
                                    <i class="fa fa-plus"></i>ADD A NEW NOTE
                                </div>
                            </div>
                        </div>
                        <div id="notes-list">
                            <div class="note-item media current fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Reset my account password</p>
                                    </div>
                                    <p class="note-desc hidden">Break security reasons.</p>
                                    <p>
                                        <small>Tuesday 6 May, 3:52 pm</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Call John</p>
                                    </div>
                                    <p class="note-desc hidden">He have my laptop!</p>
                                    <p>
                                        <small>Thursday 8 May, 2:28 pm</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Buy a car</p>
                                    </div>
                                    <p class="note-desc hidden">I'm done with the bus</p>
                                    <p>
                                        <small>Monday 12 May, 3:43 am</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Don't forget my notes</p>
                                    </div>
                                    <p class="note-desc hidden">I have to read them...</p>
                                    <p>
                                        <small>Wednesday 5 May, 6:15 pm</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media current fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Reset my account password</p>
                                    </div>
                                    <p class="note-desc hidden">Break security reasons.</p>
                                    <p>
                                        <small>Tuesday 6 May, 3:52 pm</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Call John</p>
                                    </div>
                                    <p class="note-desc hidden">He have my laptop!</p>
                                    <p>
                                        <small>Thursday 8 May, 2:28 pm</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Buy a car</p>
                                    </div>
                                    <p class="note-desc hidden">I'm done with the bus</p>
                                    <p>
                                        <small>Monday 12 May, 3:43 am</small>
                                    </p>
                                </div>
                            </div>
                            <div class="note-item media fade in">
                                <button class="close">×</button>
                                <div>
                                    <div>
                                        <p class="note-name">Don't forget my notes</p>
                                    </div>
                                    <p class="note-desc hidden">I have to read them...</p>
                                    <p>
                                        <small>Wednesday 5 May, 6:15 pm</small>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="detail-note note-hidden-sm">
                    <div class="note-header clearfix">
                        <div class="note-back">
                            <i class="icon-action-undo"></i>
                        </div>
                        <div class="note-edit">Edit Note</div>
                        <div class="note-subtitle">title on first line</div>
                    </div>
                    <div id="note-detail">
                        <div class="note-write">
                            <textarea class="form-control" placeholder="Type your note here"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="settings">
                <div class="settings">
                    <div class="title">ACCOUNT SETTINGS</div>
                    <div class="setting">
                        <span> Show Personal Statut</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked>
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <p class="setting-info">Lorem ipsum dolor sit amet consectetuer.</p>
                    </div>
                    <div class="setting">
                        <span> Show my Picture</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked>
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <p class="setting-info">Lorem ipsum dolor sit amet consectetuer.</p>
                    </div>
                    <div class="setting">
                        <span> Show my Location</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input">
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <p class="setting-info">Lorem ipsum dolor sit amet consectetuer.</p>
                    </div>
                    <div class="title">CHAT</div>
                    <div class="setting">
                        <span> Show User Image</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked>
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div class="setting">
                        <span> Show Fullname</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked>
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div class="setting">
                        <span> Show Location</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input">
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div class="setting">
                        <span> Show Unread Count</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked>
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div>
                    <div class="title">STATISTICS</div>
                    <div class="settings-chart">
                        <div class="clearfix">
                            <div class="chart-title">Stat 1</div>
                            <div class="chart-number">82%</div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary setting1" data-transitiongoal="82"></div>
                        </div>
                    </div>
                    <div class="settings-chart">
                        <div class="clearfix">
                            <div class="chart-title">Stat 2</div>
                            <div class="chart-number">43%</div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary setting2" data-transitiongoal="43"></div>
                        </div>
                    </div>
                    <div class="m-t-30" style="width:100%">
                        <canvas id="setting-chart" height="300"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END QUICKVIEW SIDEBAR -->
<!-- BEGIN SEARCH -->
<div id="morphsearch" class="morphsearch">
    <form class="morphsearch-form">
        <input class="morphsearch-input" type="search" placeholder="Search..."/>
        <button class="morphsearch-submit" type="submit">Search</button>
    </form>
    <div class="morphsearch-content withScroll">
        <div class="dummy-column user-column">
            <h2>Users</h2>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar1_big.png" alt="Avatar 1"/>
                <h3>John Smith</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar2_big.png" alt="Avatar 2"/>
                <h3>Bod Dylan</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar3_big.png" alt="Avatar 3"/>
                <h3>Jenny Finlan</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar4_big.png" alt="Avatar 4"/>
                <h3>Harold Fox</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar5_big.png" alt="Avatar 5"/>
                <h3>Martin Hendrix</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/avatars/avatar6_big.png" alt="Avatar 6"/>
                <h3>Paul Ferguson</h3>
            </a>
        </div>
        <div class="dummy-column">
            <h2>Articles</h2>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/1.jpg" alt="1"/>
                <h3>How to change webdesign?</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/2.jpg" alt="2"/>
                <h3>News From the sky</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/3.jpg" alt="3"/>
                <h3>Where is the cat?</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/4.jpg" alt="4"/>
                <h3>Just another funny story</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/5.jpg" alt="5"/>
                <h3>How many water we drink every day?</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/6.jpg" alt="6"/>
                <h3>Drag and drop tutorials</h3>
            </a>
        </div>
        <div class="dummy-column">
            <h2>Recent</h2>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/7.jpg" alt="7"/>
                <h3>Design Inspiration</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/8.jpg" alt="8"/>
                <h3>Animals drawing</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/9.jpg" alt="9"/>
                <h3>Cup of tea please</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/10.jpg" alt="10"/>
                <h3>New application arrive</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/11.jpg" alt="11"/>
                <h3>Notification prettify</h3>
            </a>
            <a class="dummy-media-object" href="#">
                <img src="../assets/global/images/gallery/12.jpg" alt="12"/>
                <h3>My article is the last recent</h3>
            </a>
        </div>
    </div>
    <!-- /morphsearch-content -->
    <span class="morphsearch-close"></span>
</div>
<!-- END SEARCH -->
<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
@endsection

@section('page_script')
        <!-- BEGIN PAGE SCRIPT -->
<script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
<script src="../assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="../assets/global/plugins/dropzone/dropzone.min.js"></script>  <!-- Upload Image & File in dropzone -->
<script src="../assets/global/js/pages/form_icheck.js"></script>  <!-- Change Icheck Color - DEMO PURPOSE - OPTIONAL -->
<!-- END PAGE SCRIPTS -->
@endsection