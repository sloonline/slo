@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet"/>
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>DATA BAY <strong>GARDU INDUK</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    <div class="panel-header bg-primary">
                        <h2 class="panel-title">Data <strong>Transmisi</strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                        {!! Form::open(array( 'class'=> 'form-horizontal')) !!}
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Nama Instalasi</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white"
                                       value="{{(@$transmisi != null)? @$transmisi->nama_instalasi : ""}}"
                                       name="nama_instalasi" readonly="">
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Sistem Jaringan Transmisi</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white"
                                       value="{{(@$transmisi->sistemJaringanTransmisi != null)? @$transmisi->sistemJaringanTransmisi->nama_reference : ""}}"
                                       name="sistem_jaringan_transmisi" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Kapasitas Total Gardu Induk (MVA)</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white" name="kapasitas_gardu_induk"
                                       value="{{(@$transmisi != null)? @$transmisi->kapasitas_gi : ""}}"
                                       readonly="">
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                       value="{{(@$transmisi->teganganPengenal != null)? @$transmisi->teganganPengenal->nama_reference : ""}}"
                                       readonly="">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="panel border">
                    <div class="panel-content">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <i class="icon-plus"></i> <strong>DATA BAY</strong>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in in">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs nav-primary">
                                        @foreach($all_bay as $key => $row)
                                            <li {{($key == BAY_LINE) ? 'class=active' : ""}}><a
                                                        href="#{{str_replace(" ","_",$key)}}"
                                                        data-toggle="tab">{{strtoupper($key)}}</a></li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_bay as $key => $row)
                                            <div class="tab-pane fade {{($key == BAY_LINE) ? "active in" : ""}}"
                                                 id="{{str_replace(" ","_",$key)}}">
                                                <div class="form-group">
                                                    <div class="col-lg-2 pull-right">
                                                        <a href="{{url('/eksternal/create_bay_gardu/'.@$transmisi->id.'/'.$key)}}"
                                                           style="width: 150px;"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-plus"></i>
                                                            ADD
                                                        </a>
                                                    </div>
                                                </div>
                                                <br/>
                                                <br/>
                                                <div id="data_{{str_replace(" ","_",$key)}}" style="overflow-x:auto;">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <th>NO.</th>
                                                        <th>NAMA</th>
                                                        <th>JENIS</th>
                                                        <th>KEPEMILIKAN</th>
                                                        <th>PEMILIK</th>
                                                        <th>KAPASITAS PEMUTUS TENAGA (kA)</th>
                                                        <th>KAPASITAS TRAFO TENAGA/KAPASITOR/REAKTOR (MVA)</th>
                                                        <th>TEGANGAN PENGENAL</th>
                                                        <th>FILE LAMPIRAN</th>
                                                        <th>AKSI</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i = 1;?>
                                                        @foreach(@$row as $bg)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td style="white-space: nowrap;">{{$bg->nama_bay}}</td>
                                                                <td style="white-space: nowrap;">{{$bg->jenis_bay}}</td>
                                                                <td>{{$bg->tipe_pemilik}}</td>
                                                                <td>{{($bg->tipe_pemilik == MILIK_SENDIRI) ? MILIK_SENDIRI : $bg->pemilik->nama_pemilik}}</td>
                                                                <td>{{@$bg->kapasitas_pemutus}}</td>
                                                                <td>{{@$bg->kapasitas_trafo}}</td>
                                                                <td>{{($bg->pengenal != null) ? $bg->pengenal->nama_reference : ""}}</td>
                                                                <td style="white-space: nowrap;">
                                                                    @if(@$bg->file_sbujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sbujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SBUJPTL</a>
                                                                    @endif
                                                                    @if(@$bg->file_iujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_iujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>IUJPTL</a>
                                                                    @endif
                                                                    @if(@$bg->file_sld != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sld)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SLD</a>
                                                                    @endif
                                                                </td>
                                                                <td style="white-space: nowrap;">
                                                                    <?php
                                                                    $isRejected = false;
                                                                    if ($bg->permohonan != null) {
                                                                        if (sizeof($bg->permohonan) != 0) {
                                                                            $isRejected = \App\BayGardu::isPermohonanRej($bg->permohonan->pluck('permohonan_id'));
                                                                        }
                                                                    }
                                                                    ?>
                                                                    @if(@$bg->permohonan == null || sizeof(@$bg->permohonan) == 0 || $isRejected)
                                                                        <a href="{{url('/eksternal/edit_bay_gardu/'.@$bg->instalasi_id.'/'.@$bg->id)}}"
                                                                           class="btn btn-sm btn-warning btn-square
                                                                        btn-embossed"><i
                                                                                    class="fa fa-pencil-square-o"></i></a>
                                                                    @endif
                                                                    <a onclick="return confirm('Apakah anda yakin untuk menghapus {{@$bg->nama_bay}} ?')"
                                                                       href="{{url('/eksternal/delete_bay_gardu/'.@$bg->id)}}"
                                                                       class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                                                class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php $i++;?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="panel-footer clearfix bg-white">
                                    <div class="pull-right">
                                        <a href="{{url('/eksternal/view_instalasi_transmisi/')}}"
                                           class="btn btn-warning btn-square btn-embossed">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
@endsection
