@extends('../layout/layout_eksternal')


@section('content')

        <!-- BEGIN PAGE CONTENT -->
<div class="page-content page-wizard">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header md-panel-controls">
                    <h3><i class="icon-bulb"></i> Input <strong>Distribusi</strong></h3>
                </div>
                <div class="panel-content">
                    <p>Silahkan masukan data pemohon dan detail distribusi yang akan diinspeksi.</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label"><h2><b>Data Pemilik Instalasi</b></h2></label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama Pemilik Instalasi</label>
                                <input type="text" class="form-control" placeholder="Nama pemilik">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Lokasi</label>
                                <input type="text" class="form-control" placeholder="Lokasi">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Wilayah Kerja</label>
                                <input type="text" class="form-control" placeholder="Wilayah kerja">
                            </div>
                            <div class="form-group">
                                <label class="form-label"><h2><b>Data Distribusi</b></h2></label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tujuan Pemasangan</label>
                                <input type="text" class="form-control" placeholder="Tujuan pemasangan">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jenis Instalasi Distribusi</label>
                                <select class="form-control" placeholder="Jenis instalasi">
                                    <option>Jaringan distribusi tenaga listri tegangan menengah</option>
                                    <option>Jaringan distribusi tenaga listri tegangan rendah</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jenis Usaha Penyedia Listrik</label>
                                <select class="form-control" placeholder="Jenis instalasi">
                                    <option>IO</option>
                                    <option>Pelanggan</option>
                                    <option>PIUPTL</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kepemilikan Sistem Jaringan</label>
                                <select class="form-control" placeholder="Jenis instalasi">
                                    <option>Milik BUMN</option>
                                    <option>Milik Swasta</option>
                                    <option>Menyewa Jaringan BUMN</option>
                                    <option>Menyewa Jaringan Swasta</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Sistem Jaringan</label>
                                <select class="form-control" placeholder="Jenis instalasi">
                                    <option>Wilayah Aceh</option>
                                    <option>Wilayah Sumatera Utara</option>
                                    <option>Wilayah Sumatera Barat</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tegangan Pengenal</label>
                                <select class="form-control" placeholder="Jenis instalasi">
                                    <option>220 V</option>
                                    <option>380 V</option>
                                    <option>6 kV</option>
                                    <option>12 kV</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jaringan SUTM</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="Jaringan SUTM"/>
                                    </div>
                                    <div class="col-lg-3">kms</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jaringan SUTR</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="Jaringan SUTR"/>
                                    </div>
                                    <div class="col-lg-3">kms</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">SKTM</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="SKTM"/>
                                    </div>
                                    <div class="col-lg-3">kms</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Cubicle</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="Cubicle"/>
                                    </div>
                                    <div class="col-lg-3">unit</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label"><h2><b>Trafo</b></h2></label><br/>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">Daya</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">unit</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">Daya</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">unit</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">Daya</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">unit</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">Daya</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">unit</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">Daya</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">unit</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label"><h2><b>Surat Perjanjian Jual Beli Tenaga Listrik</b></h2>
                                </label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nomor</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="Nomor"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="date" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jual Beli/SIT/ID Pelanggan</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Single Line Diagram</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Gambar Instalasi dan Tata Letak</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jenis dan Kapasitas Instalasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Spesifikasi Peralatan Utama Instalasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Ijin Operasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File NPWP</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File PKP</label>
                                <input type="file" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                        <button class="btn btn-success" type="submit" name="save"><i class="fa fa-save"></i> Simpan
                        </button>
                    </div>
                    </br> </br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')


@endsection