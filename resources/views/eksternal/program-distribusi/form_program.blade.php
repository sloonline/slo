{!! Form::open(array('name'=>'form_instalasi','url'=> 'eksternal/create_program', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}

<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Program</label>
    </div>
    <input type="hidden" name="id" value="{{$id}}"/>
    <input type="hidden" name="id_surat_tugas" value="{{$id_surat_tugas}}"/>
    <div class="col-sm-9">
        @if($program == null)
            <select class="form-control form-white" data-search="true"
                    name="id_jenis_program" required id="id_jenis_program">
                <option value="">--- Pilih ---</option>
                @foreach($jenis_program as $item)
                    <option value="{{$item->id}}" {{($program != null && $program->id_jenis_program == $item->id) ? "selected" : ""}}>{{$item->jenis_program}}</option>
                @endforeach
            </select>
        @else
            <input type="hidden" name="id_jenis_program"
                   value="{{$program->id_jenis_program }}"/>
            <input type="text" class="form-control form-white" readonly
                   value="{{$program->jenisProgram->jenis_program}}" required
                   name="jenis_program">
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Program</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" class="form-control form-white"
               value="{{@$program->deskripsi}}" required name="deskripsi">
        <i class="fa fa-building"></i>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Periode</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="number" min="1900" minlength="4" maxlength="4"
               class="form-control form-white" value="{{@$program->periode}}" required
               name="periode">
        <i class="fa fa-building"></i>
    </div>
</div>
<div class="panel-footer clearfix bg-white">
    <div class="pull-right">
        <button type="submit" id="submit_form"
                class="btn btn-success btn-square btn-embossed"
                data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
        </button>
    </div>
</div>
{!! Form::close() !!}