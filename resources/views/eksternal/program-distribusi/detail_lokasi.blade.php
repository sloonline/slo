{!! Form::open(['url'=>'/eksternal/create_lokasi_area','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id_area_program" value="{{$id_area}}"/>
<input type="hidden" name="id_kontrak" value="{{$id_kontrak}}"/>
<input type="hidden" name="id" value="{{($lokasi_area != null) ? $lokasi_area->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Lokasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="lokasi"
               value="{{($lokasi_area != null) ? $lokasi_area->lokasi->lokasi : ""}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">{{($program->jenisProgram->acuan_1 == TIPE_AREA) ? "Area" : "Kontrak"}}</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="area_kontrak"
               value="{{$parent}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->jenisProgram->jenis_program}}"/>
    </div>
</div>
{!! Form::close() !!}