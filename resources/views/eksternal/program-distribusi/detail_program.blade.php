{!! Form::open(array('name'=>'form_instalasi','url'=> 'eksternal/create_program', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}

<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Program</label>
    </div>
    <input type="hidden" name="id" value="{{$id}}"/>
    <input type="hidden" name="id_surat_tugas" value="{{$id_surat_tugas}}"/>
    <div class="col-sm-9">
        <input type="hidden" name="id_jenis_program"
               value="{{$program->id_jenis_program }}"/>
        <input type="text" class="form-control form-white" readonly
               value="{{$program->jenisProgram->jenis_program}}" required
               name="jenis_program">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Program</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" class="form-control form-white"
               value="{{@$program->deskripsi}}" readonly name="deskripsi">
        <i class="fa fa-building"></i>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Periode</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="number" min="1900" minlength="4" maxlength="4"
               class="form-control form-white" value="{{@$program->periode}}" required
               name="periode">
        <i class="fa fa-building"></i>
    </div>
</div>
{!! Form::close() !!}