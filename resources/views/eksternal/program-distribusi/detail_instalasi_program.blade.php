@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Detail Instalasi <strong>Distribusi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/create_surat_tugas/'.$program->id_surat_tugas)}}">Surat Tugas</a>
                    </li>
                    <li class="active">{{($distribusi != null) ? "Ubah" : "Tambah"}} Instalasi Program Distribusi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    {!! Form::open(array('name' => 'form_instalasi','url'=> 'eksternal/create_instalasi_program', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    <input type="hidden" name="id" value="{{($distribusi != null) ? $distribusi->id : 0}}"/>
                    <input type="hidden" name="id_program" value="{{$id_program}}"/>
                    <input type="hidden" name="id_area_program" value="{{$id_area_program}}"/>
                    <input type="hidden" name="id_parent" value="{{$id_parent}}"/>
                    <input type="hidden" name="pemilik_instalasi_baru_id"
                           value="{{($distribusi != null) ? $distribusi->pemilik_instalasi_id : 0}}"/>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li><a href="#program" data-toggle="tab"><i class="fa fa-file"></i>
                                    PROGRAM</a></li>
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#pemilik" data-toggle="tab"><i class="icon-user"></i> PEMILIK</a></li>
                            <li><a href="#lokasi" data-toggle="tab"><i class="icon-map"></i> LOKASI</a></li>
                            <li><a href="#lingkup_pekerjaan" data-toggle="tab"><i class="fa fa-tasks"></i> LINGKUP
                                    PEKERJAAN</a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="tab-pane fade" id="program">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Surat</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$program->suratTugas->nomor_surat}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Program</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{$program->deskripsi}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Program</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$program->jenisProgram->jenis_program}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Periode</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$program->periode}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$distribusi->jenis_instalasi->jenis_instalasi}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">{{ucwords(strtolower($program->jenisProgram->acuan_2))}}</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{$parent}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{($distribusi->status_baru == 1 ) ? "INSTALASI BARU" : "INSTALASI LAMA"}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$distribusi->jenis->nama_reference}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Instalasi</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="text" class="form-control form-white" readonly

                                               value="{{($distribusi != null)? $distribusi->nama_instalasi : ""}}"
                                               name="nama_instalasi">
                                        <i class="glyphicon glyphicon-flash"></i>
                                    </div>
                                </div>
                                <div id="jtm_jtr">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Panjang Saluran (kms)</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="number" class="form-control form-white" readonly

                                                   name="panjang_kms" id="panjang_saluran"
                                                   value="{{($distribusi != null)? $distribusi->panjang_kms : ""}}">
                                            <i class="fa fa-wrench"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="gardu">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Jumlah Gardu</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="number" class="form-control form-white" readonly

                                                   name="jml_gardu" id="jml_gardu"
                                                   value="{{($distribusi != null)? $distribusi->jml_gardu : ""}}">
                                            <i class="fa fa-wrench"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kapasitas Gardu
                                                Distribusi</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="number" class="form-control form-white" readonly

                                                   name="kapasitas_gardu" id="kapasitas_gardu"
                                                   value="{{($distribusi != null)? $distribusi->kapasitas_gardu : ""}}">
                                            <i class="fa fa-tasks"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="phb_phbtm">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Jumlah Cubicle</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="number" class="form-control form-white" readonly

                                                   name="jml_cubicle" id="jml_cubicle"
                                                   value="{{($distribusi != null)? $distribusi->jml_cubicle : ""}}">
                                            <i class="fa fa-wrench"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kapasitas Arus Hubung
                                                Singkat</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input type="number" class="form-control form-white" readonly

                                                   name="kapasitas_arus_hubung_singkat"
                                                   id="kapasitas_arus_hubung_singkat"
                                                   value="{{($distribusi != null)? $distribusi->kapasitas_arus_hubung_singkat : ""}}">
                                            <i class="fa fa-tasks"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="teg_pengenal">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="number" step="any" class="form-control form-white" readonly

                                               name="tegangan_pengenal"
                                               value="{{($distribusi != null)? $distribusi->tegangan_pengenal : ""}}">
                                        <i class="fa fa-tasks"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea id="keterangan" name="keterangan" rows="3" readonly
                                                  class="form-control form-white"
                                                  placeholder="Keterangan Instalasi...">{{($distribusi != null && $distribusi->keterangan != null) ? trim($distribusi->keterangan) : null}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pemilik">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kepemilikan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input id="kepemilikan" name="kepemilikan"
                                               placeholder="Kepemilikan" type="text"
                                               class="form-control form-white"
                                               value="{{($distribusi->perusahaan != null) ? $distribusi->perusahaan->kategori->nama_kategori : null}}"
                                               readonly="">
                                        {{--<i class="fa fa-building"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kepemilikan Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true" required

                                                name="tipe_pemilik" id="jenis_pemilik_instalasi" disabled>
                                            {{--<option value="">--- Pilih ---</option>--}}
                                            <option value="{{MILIK_SENDIRI}}" {{($distribusi != null && $distribusi->tipe_pemilik == MILIK_SENDIRI) ? "selected" : ""}}>
                                                Milik sendiri
                                            </option>
                                            <option value="{{TERDAFTAR}}" {{($distribusi != null && $distribusi->tipe_pemilik == TERDAFTAR) ? "selected" : ""}}>
                                                Milik perusahaan lain yang terdaftar
                                            </option>
                                            <option value="{{BELUM_TERDAFTAR}}" {{($distribusi != null && $distribusi->tipe_pemilik == BELUM_TERDAFTAR) ? "selected" : ""}}>
                                                Milik perusahaan lain yang belum terdaftar
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::hidden('pemilik_instalasi_id', @$pemilik_instalasi->id) !!}
                                        <div class="panel hidden border" id="pemilik_instalasi_baru">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">PEMILIK <strong>INSTALASI</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="nama_pemilik" name="nama_pemilik"
                                                               placeholder="Nama Pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik != null) ? $distribusi->pemilik->nama_pemilik : null}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                    <textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
                                                              class="form-control form-white"
                                                              placeholder="Alamat Instalasi...">{{($distribusi != null && $distribusi->pemilik != null) ? trim($distribusi->pemilik->alamat_pemilik) : null}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select name="provinsi_pemilik" class="form-control form-white"
                                                                data-search="true"
                                                                id="province_pemilik">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($province as $item)
                                                                <option value="{{$item->id}}"
                                                                        {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->id_province == $item->id) ? "selected" : ""}}
                                                                >{{$item->province}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select name="kabupaten_pemilik" id="city_pemilik"
                                                                class="form-control form-white" data-search="true">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($city as $item)
                                                                <option value="{{$item->id}}"
                                                                        class="{{$item->id_province}}"
                                                                        {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->id_city == $item->id) ? "selected" : ""}}
                                                                >{{$item->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="kode_pos" name="kode_pos" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->kode_pos_pemilik : null}}">
                                                        <i class="fa fa-map-marker"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Telepon</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="telepon" name="telepon" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->telepon_pemilik : null}}">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Fax</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_fax" name="no_fax" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_fax_pemilik : null}}">
                                                        <i class="fa fa-fax"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="email_pemilik" name="email_pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->email_pemilik : null}}">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel hidden border" id="ijin_usaha_baru">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">IJIN <strong>USAHA</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select id="jenis_ijin_usaha" name="jenis_ijin_usaha"
                                                                class="form-control form-white">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($jenis_ijin_usaha as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->pemilik && $distribusi->pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Penerbit Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="penerbit_ijin_usaha" name="penerbit_ijin_usaha"
                                                               type="text" class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->penerbit_ijin_usaha : null}}">
                                                        <i class="fa fa-list-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_ijin_usaha" name="no_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_ijin_usaha : null}}">
                                                        <i class="fa fa-th-list"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="masa_berlaku_iu" name="masa_berlaku_iu" type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy" data-lang="en"
                                                               data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_iu)) : null}}">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Surat Ijin
                                                            Usaha</label>
                                                    </div>
                                                    @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_siup != null)
                                                        <div class="col-sm-2">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.$distribusi->pemilik->file_siup)}}"
                                                               class="btn btn-primary"><i
                                                                        class="fa fa-download"></i>View</a>
                                                        </div>
                                                    @endif
                                                    <div class="col-sm-7">
                                                        <div class="file">
                                                            <div class="option-group">
                                                                <span class="file-button btn-primary">
                                                                    {{--                                                            {{($pemilik != null && $pemilik->file_siup != null) ? "Ganti File" : "Choose File"}}--}}
                                                                    Choose File
                                                                </span>
                                                                <input type="file" class="custom-file"
                                                                       name="file_surat_iu"
                                                                       onchange="document.getElementById('uploader').value = this.value;">
                                                                <input type="text" class="form-control form-white"
                                                                       id="uploader"
                                                                       placeholder="no file selected"
                                                                       {{--                                                                       value="{{($pemilik != null && $pemilik->file_siup != null) ? $pemilik->file_siup : null}}">--}}
                                                                       value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Sewa (Jika Ada)</label>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <input name="has_sewa" type="radio" id="sewa_true"
                                                               class="form-control"
                                                               {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->nama_kontrak != "" ) ? "checked" : ""}}
                                                               value="1"> Ya
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <input name="has_sewa"
                                                               {{ ($distribusi == null) ? "checked": (($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->nama_kontrak == "") ? "checked" : "")}} type="radio"
                                                               id="sewa_false" class="form-control" value="0"> Tidak
                                                    </div>
                                                </div>
                                                <div id="kontrak">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Nama Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="nama_kontrak" name="nama_kontrak" type="text"
                                                                   class="form-control form-white"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->nama_kontrak : null}}">
                                                            <i class="fa fa-list-alt"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="no_kontrak" name="no_kontrak" type="text"
                                                                   class="form-control form-white"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_kontrak : null}}">
                                                            <i class="fa fa-th-list"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Tanggal Pengesahaan
                                                                Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="tgl_pengesahan_kontrak"
                                                                   name="tgl_pengesahan_kontrak"
                                                                   type="text"
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en" data-RTL="false"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->tgl_pengesahan_kontrak)) : null}}">
                                                            <i class="fa fa-th-list"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Masa Berlaku
                                                                Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="masa_berlaku_kontrak" name="masa_berlaku_kontrak"
                                                                   type="text"
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en" data-RTL="false"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_kontrak)) : null}}">
                                                            <i class="fa fa-calendar-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">File Kontrak
                                                                Sewa</label>
                                                        </div>
                                                        @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_kontrak != null)
                                                            <div class="col-sm-2">
                                                                <a target="_blank"
                                                                   href="{{url('upload/'.$distribusi->pemilik->file_kontrak)}}"
                                                                   class="btn btn-primary"><i
                                                                            class="fa fa-download"></i>View</a>
                                                            </div>
                                                        @endif
                                                        <div class="col-sm-7">
                                                            <div class="file">
                                                                <div class="option-group">
                                                                    <span class="file-button btn-primary">Choose File</span>
                                                                    <input type="file" class="custom-file"
                                                                           name="file_kontrak_sewa"
                                                                           onchange="document.getElementById('uploader2').value = this.value;">
                                                                    <input type="text" class="form-control form-white"
                                                                           id="uploader2"
                                                                           placeholder="no file selected"
                                                                           {{--value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">--}}
                                                                           value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label class="col-sm-12 control-label">*) SPJBTL di-isikan
                                                            sesuai dengan lokasi instalasi</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_spjbtl" name="no_spjbtl" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_spjbtl : null}}">
                                                        <i class="fa fa-th-list"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="tgl_spjbtl" name="tgl_spjbtl" type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy" data-lang="en"
                                                               data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->tgl_spjbtl)) : null}}">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="masa_berlaku_spjbtl" name="masa_berlaku_spjbtl"
                                                               type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_spjbtl)) : null}}">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                                    </div>
                                                    @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_spjbtl != null)
                                                        <div class="col-sm-2">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.$distribusi->pemilik->file_spjbtl)}}"
                                                               class="btn btn-primary"><i
                                                                        class="fa fa-download"></i>View</a>
                                                        </div>
                                                    @endif
                                                    <div class="col-sm-7">
                                                        <div class="file">
                                                            <div class="option-group">
                                                                <span class="file-button btn-primary">Choose File</span>
                                                                <input type="file" class="custom-file"
                                                                       name="file_spjbtl"
                                                                       onchange="document.getElementById('uploader3').value = this.value;">
                                                                <input type="text" class="form-control form-white"
                                                                       id="uploader3"
                                                                       placeholder="no file selected"
                                                                       {{--                                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">--}}
                                                                       value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel hidden border" id="pemilik_instalasi">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">PEMILIK <strong>INSTALASI</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Pemilik Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select class="form-control form-white" data-search="true"
                                                                name="pemilik_instalasi_lain"
                                                                id="pemilik_instalasi_lain">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($pemilik as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->pemilik_instalasi_id == $item->id) ? "selected" : ""}}>{{$item->nama_pemilik}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lokasi">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea rows="3"
                                                  class="form-control form-white" readonly
                                                  name="lokasi"
                                                  placeholder="Alamat Instalasi">{{($distribusi != null)? $distribusi->lokasi : ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Provinsi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$distribusi->provinsi->province}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" readonly
                                               value="{{@$distribusi->kota->city}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong> (AWAL)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>

                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="deg" maxlength="3"
                                                                   class="form-control form-white"
                                                                   name="longitude_awal_deg" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_awal_deg : ""}}">
                                                            <span class="input-group-addon">°</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="min" maxlength="2"
                                                                   class="form-control form-white"
                                                                   name="longitude_awal_min" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_awal_min : ""}}">
                                                            <span class="input-group-addon">'</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="sec" maxlength="4"
                                                                   class="form-control form-white"
                                                                   name="longitude_awal_sec" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_awal_sec : ""}}">
                                                            <span class="input-group-addon">"</span>
                                                        </div>
                                                        <div class="input-group col-sm-2 col-md-2 col-lg-2">
                                                            <select name="longitude_awal_dir" readonly
                                                                    class="form-control form-white">
                                                                <option value="e" <?php echo ($distribusi != null) ? ($distribusi->longitude_awal_dir == "e" ? "selected='selected'" : "") : ""; ?>>
                                                                    E
                                                                </option>
                                                                <option value="w" <?php echo ($distribusi != null) ? ($distribusi->longitude_awal_dir == "w" ? "selected='selected'" : "") : ""; ?>>
                                                                    W
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <!-- <input type="text" placeholder="Titik Koordinat Longitude" id="koordinat_awal_long" name="field_pmb[KoordinatAwalLong]" class="form-control"> -->
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="deg" maxlength="3"
                                                                   class="form-control form-white"
                                                                   name="latitude_awal_deg" readonly
                                                                   value="{{($distribusi != null)? $distribusi->latitude_awal_deg : ""}}">
                                                            <span class="input-group-addon">°</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="min" maxlength="2"
                                                                   class="form-control form-white"
                                                                   name="latitude_awal_min" readonly
                                                                   value="{{($distribusi != null)? $distribusi->latitude_awal_min : ""}}">
                                                            <span class="input-group-addon">'</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="sec" maxlength="4"
                                                                   class="form-control form-white"
                                                                   name="latitude_awal_sec" readonly
                                                                   value="{{($distribusi != null)? $distribusi->latitude_awal_sec : ""}}">
                                                            <span class="input-group-addon">"</span>
                                                        </div>
                                                        <div class="input-group col-sm-2 col-md-2 col-lg-2">
                                                            <select name="latitude_awal_dir"
                                                                    class="form-control form-white">
                                                                <option value="n" <?php echo ($distribusi != null) ? ($distribusi->latitude_awal_dir == "n" ? "selected='selected'" : "") : ""; ?>>
                                                                    N
                                                                </option>
                                                                <option value="s" <?php echo ($distribusi != null) ? ($distribusi->latitude_awal_dir == "s" ? "selected='selected'" : "") : ""; ?>>
                                                                    S
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <!-- <input type="text" placeholder="Titik Koordinat Longitude" id="koordinat_awal_long" name="field_pmb[KoordinatAwalLong]" class="form-control"> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong> (AKHIR)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="deg" maxlength="3"
                                                                   class="form-control form-white"
                                                                   name="longitude_akhir_deg" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_akhir_deg : ""}}">
                                                            <span class="input-group-addon">°</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="min" maxlength="2"
                                                                   class="form-control form-white"
                                                                   name="longitude_akhir_min" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_akhir_min : ""}}">
                                                            <span class="input-group-addon">'</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="sec" maxlength="4"
                                                                   class="form-control form-white"
                                                                   name="longitude_akhir_sec" readonly
                                                                   value="{{($distribusi != null)? $distribusi->longitude_akhir_sec : ""}}">
                                                            <span class="input-group-addon">"</span>
                                                        </div>
                                                        <div class="input-group col-sm-2 col-md-2 col-lg-2">
                                                            <select name="longitude_akhir_dir" readonly
                                                                    class="form-control form-white">
                                                                <option value="w" <?php echo ($distribusi != null) ? ($distribusi->longitude_akhir_dir == "w" ? "selected='selected'" : "") : ""; ?>>
                                                                    W
                                                                </option>
                                                                <option value="e" <?php echo ($distribusi != null) ? ($distribusi->longitude_akhir_dir == "e" ? "selected='selected'" : "") : ""; ?>>
                                                                    E
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <!-- <input type="text" placeholder="Titik Koordinat Longitude" id="koordinat_awal_long" name="field_pmb[KoordinatAwalLong]" class="form-control"> -->
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="deg" maxlength="3"
                                                                   class="form-control form-white" readonly
                                                                   name="latitude_akhir_deg"
                                                                   value="{{($distribusi != null)? $distribusi->latitude_akhir_deg : ""}}">
                                                            <span class="input-group-addon">°</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="min" maxlength="2"
                                                                   class="form-control form-white" readonly
                                                                   name="latitude_akhir_min"
                                                                   value="{{($distribusi != null)? $distribusi->latitude_akhir_min : ""}}">
                                                            <span class="input-group-addon">'</span>
                                                        </div>
                                                        <div class="input-group col-sm-3 col-md-3 col-lg-3">
                                                            <input type="text" placeholder="sec" maxlength="4"
                                                                   class="form-control form-white" readonly
                                                                   name="latitude_akhir_sec"
                                                                   value="{{($distribusi != null)? $distribusi->latitude_akhir_sec : ""}}">
                                                            <span class="input-group-addon">"</span>
                                                        </div>
                                                        <div class="input-group col-sm-2 col-md-2 col-lg-2">
                                                            <select name="latitude_akhir_dir" readonly
                                                                    class="form-control form-white">
                                                                <option value="n" <?php echo ($distribusi != null) ? ($distribusi->latitude_akhir_dir == "n" ? "selected='selected'" : "") : ""; ?>>
                                                                    N
                                                                </option>
                                                                <option value="s" <?php echo ($distribusi != null) ? ($distribusi->latitude_akhir_dir == "s" ? "selected='selected'" : "") : ""; ?>>
                                                                    S
                                                                </option>

                                                            </select>
                                                        </div>
                                                        <!-- <input type="text" placeholder="Titik Koordinat Longitude" id="koordinat_awal_long" name="field_pmb[KoordinatAwalLong]" class="form-control"> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lingkup_pekerjaan">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        @foreach($lingkup as $row)
                                            <div class="col-sm-12" style="padding:10px;">
                                                <input type="checkbox" name="lingkup_pekerjaan[]"  readonly disabled
                                                       {{(in_array(array('lingkup_id' => $row->id), $used_lingkup)) ? "checked='checked'" : ""}}
                                                       value="{{$row->id}}"/> {{$row->jenis_lingkup_pekerjaan}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr class="p-t-0 m-t-0">
                            <div class="panel-footer clearfix bg-white">
                                <div class="pull-right">
                                    <a href="javascript:history.back()"
                                       class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                @endsection

                @section('page_script')
                    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
                    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
                    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                    <script>
                        changeJenisInstalasi();
                        $("#city_instalasi").chained("#province_instalasi");
                        $("#city_pemilik").chained("#province_pemilik");

                        panelControl();
                        @if($distribusi != null)
                             setPemilik();
                        @if($distribusi->tipe_pemilik == TERDAFTAR)
                            $("#pemilik_instalasi_lain").val('{{@$distribusi->pemilik_instalasi_id}}');
                        @endif
                    @endif
                $('#jenis_pemilik_instalasi').change(function () {
                            panelControl();
                        });
                        function panelControl() {
                            var val = ($('#jenis_pemilik_instalasi').val());
                            if (val == '{{MILIK_SENDIRI}}') { //milik sendiri
                                $('#panel_pemilik_instalasi').hide();
                                $('#panel_ijin_usaha').show();
                            } else if (val == '{{TERDAFTAR}}') { //lainnya
                                $('#panel_pemilik_instalasi').show();
                                $('#panel_ijin_usaha').show();
                            } else { //belum dipilih
                                $('#panel_pemilik_instalasi').hide();
                                $('#panel_ijin_usaha').hide();
                            }
                        }


                        $('#jenis_pemilik_instalasi').change(function () {
                            setPemilik();
                        });

                        function setPemilik() {

                            if ($("#jenis_pemilik_instalasi").val() == '{{MILIK_SENDIRI}}') {
                                $("#pemilik_instalasi_baru").addClass('hidden');
                                $("#ijin_usaha_baru").addClass('hidden');
                                $("#pemilik_instalasi").addClass('hidden');
                                $("#nama_pemilik").val('{{@$pemilik_instalasi->nama_pemilik}}');
                                $("#alamat_pemilik").val('{{@$pemilik_instalasi->alamat_pemilik}}');
                                $("#province").val('{{@$pemilik_instalasi->id_province}}');
                                $('#province').change();
                                $("#city").val('{{@$pemilik_instalasi->id_city}}');
                                $('#city').change();
                                $("#kode_pos").val('{{@$pemilik_instalasi->kode_pos_pemilik}}');
                                $("#telepon").val('{{@$pemilik_instalasi->telepon_pemilik}}');
                                $("#no_fax").val('{{@$pemilik_instalasi->no_fax_pemilik}}');
                                $("#email_pemilik").val('{{@$pemilik_instalasi->email_pemilik}}');
                                $("#jenis_ijin_usaha").val('{{@$pemilik_instalasi->jenis_ijin_usaha}}');
                                $('#jenis_ijin_usaha').change();
                                $("#penerbit_ijin_usaha").val('{{@$pemilik_instalasi->penerbit_ijin_usaha}}');
                                $("#no_ijin_usaha").val('{{@$pemilik_instalasi->no_ijin_usaha}}');
                                $("#masa_berlaku_iu").val('{{@$pemilik_instalasi->masa_berlaku_iu}}');
                                $("#uploader").val('{{@$pemilik_instalasi->file_siup}}');
                                $("#nama_kontrak").val('{{@$pemilik_instalasi->nama_kontrak}}');
                                $("#no_kontrak").val('{{@$pemilik_instalasi->no_kontrak}}');
                                $("#tgl_pengesahan_kontrak").val('{{@$pemilik_instalasi->tgl_pengesahan_kontrak}}');
                                $("#masa_berlaku_kontrak").val('{{@$pemilik_instalasi->masa_berlaku_kontrak}}');
                                $("#uploader2").val('{{@$pemilik_instalasi->file_kontrak}}');
                                $("#no_spjbtl").val('{{@$pemilik_instalasi->no_spjbtl}}');
                                $("#tgl_spjbtl").val('{{@$pemilik_instalasi->tgl_spjbtl}}');
                                $("#masa_berlaku_spjbtl").val('{{@$pemilik_instalasi->masa_berlaku_spjbtl}}');
                                $("#uploader3").val('{{@$pemilik_instalasi->file_spjbtl}}');
                                $("#pemilik_instalasi_lain").val('');
                            }
                            else if ($("#jenis_pemilik_instalasi").val() == '{{TERDAFTAR}}') {
                                $("#pemilik_instalasi_baru").addClass('hidden');
                                $("#ijin_usaha_baru").addClass('hidden');
                                $("#pemilik_instalasi").removeClass('hidden');
                                $("#pemilik_instalasi_lain").val('');
                            }
                            else {
                                $("#pemilik_instalasi_baru").removeClass('hidden');
                                $("#ijin_usaha_baru").removeClass('hidden');
                                $("#pemilik_instalasi").addClass('hidden');
                                @if($distribusi == null || $distribusi->tipe_pemilik != BELUM_TERDAFTAR)
                                    $("#nama_pemilik").val('');
                                $("#alamat_pemilik").val('');
                                $("#province").val('');
                                $('#province').change();
                                $("#city").val('');
                                $('#city').change();
                                $("#kode_pos").val('');
                                $("#telepon").val('');
                                $("#no_fax").val('');
                                $("#email_pemilik").val('');
                                $("#jenis_ijin_usaha").val('');
                                $('#jenis_ijin_usaha').change();
                                $("#penerbit_ijin_usaha").val('');
                                $("#no_ijin_usaha").val('');
                                $("#masa_berlaku_iu").val('');
                                $("#uploader").val('');
                                $("#nama_kontrak").val('');
                                $("#no_kontrak").val('');
                                $("#tgl_pengesahan_kontrak").val('');
                                $("#masa_berlaku_kontrak").val('');
                                $("#uploader2").val('');
                                $("#no_spjbtl").val('');
                                $("#tgl_spjbtl").val('');
                                $("#masa_berlaku_spjbtl").val('');
                                $("#uploader3").val('');
                                $("#pemilik_instalasi_lain").val('');
                                @endif
                            }

                        }
                        function changeJenisInstalasi() {
                            $("#jtm_jtr").hide();
                            $("#phb_phbtm").hide();
                            $("#gardu").hide();
                            $("#panjang_saluran").removeAttr('required');
                            $("#jml_gardu").removeAttr('required');
                            $("#jml_cubicle").removeAttr('required');
                            $("#kapasitas_gardu").removeAttr('required');
                            $("#kapasitas_arus_hubung_singkat").removeAttr('required');
                            var jenis = $("#jenis_instalasi").find('option:selected').attr("jenis");
                            if (jenis == "JTM" || jenis == "JTR") {
                                $("#jtm_jtr").show('medium');
                                $("#panjang_saluran").attr("required", true);
                            } else if (jenis == "PHB" || jenis == "PHBTM") {
                                $("#phb_phbtm").show('medium');
                                $("#jml_cubicle").attr("required", true);
                                $("#kapasitas_arus_hubung_singkat").attr("required", true);
                            } else if (jenis == "Gardu") {
                                $("#jml_gardu").attr("required", true);
                                $("#kapasitas_gardu").attr("required", true);
                                $("#gardu").show('medium');
                            }
                        }
                    </script>
@endsection
