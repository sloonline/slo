{!! Form::open(['url'=>'/eksternal/create_kontrak_program','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id" value="{{($kontrak != null) ? $kontrak->id : 0}}">
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->jenisProgram->jenis_program}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Periode</label>
    </div>
    <div class="col-sm-9">
        <input name="periode" placeholder="Periode kontrak (tahun)" type="number"
               readonly
               class="form-control form-white" min="1000"
               value="{{($kontrak != null) ? $kontrak->periode : null}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Vendor</label>
    </div>
    <div class="col-sm-9">
        <input name="vendor" placeholder="Vendor" readonly
               class="form-control form-white"
               value="{{($kontrak != null) ? $kontrak->vendor : null}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nomor Kontrak</label>
    </div>
    <div class="col-sm-9">
        <input name="nomor_kontrak" placeholder="Nomor kontrak" readonly
               class="form-control form-white"
               value="{{($kontrak != null) ? $kontrak->nomor_kontrak : null}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Tanggal Kontrak</label>
    </div>
    <div class="col-sm-9">
        <input id="tanggal_kontrak" readonly
               name="tanggal_kontrak"
               type="date"
               class="form-control b-datepicker form-white"
               data-lang="en" data-RTL="false"
               value="{{($kontrak != null) ? $kontrak->tanggal_kontrak : null}}">
    </div>
</div>
{!! Form::close() !!}
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>