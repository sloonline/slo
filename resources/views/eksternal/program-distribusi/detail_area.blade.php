{!! Form::open(['url'=>'/eksternal/create_area_program','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id" value="{{($ap != null) ? $ap->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Area</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               value="{{@$ap->businessArea->description}}"/>
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->deskripsi}}"/>
    </div>
</div>
{!! Form::close() !!}