{!! Form::open(['url'=>'/eksternal/create_gardu_area','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id_area_program" value="{{$id_area_program}}"/>
<input type="hidden" name="id" value="{{($gardu_area != null) ? $gardu_area->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Gardu</label>
    </div>
    <div class="col-sm-9">
        <div class="append-icon">
            <input type="text" required="required" name="gardu" readonly
                   class="form-control form-white"
                   value="{{($gardu_area != null) ? $gardu_area->garduInduk->gardu_induk : ""}}" >
            <i class="fa fa-building"></i>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Area</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="area_kontrak"
               value="{{$area->businessArea->description}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->jenisProgram->jenis_program}}"/>
    </div>
</div>
{!! Form::close() !!}