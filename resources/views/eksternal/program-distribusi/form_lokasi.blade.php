{!! Form::open(['url'=>'/eksternal/create_lokasi_area','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id_area_program" value="{{$id_area}}"/>
<input type="hidden" name="id_kontrak" value="{{$id_kontrak}}"/>
<input type="hidden" name="id" value="{{($lokasi_area != null) ? $lokasi_area->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Lokasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" required
               name="lokasi" id="lokasi"
               class="form-control form-white autocomplete"
               value="{{($lokasi_area != null) ? $lokasi_area->lokasi->lokasi : ""}}"/>
        <input type="hidden" name="id_lokasi" id="id_lokasi" value="{{($lokasi_area != null) ? $lokasi_area->id_lokasi : 0}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">{{($program->jenisProgram->acuan_1 == TIPE_AREA) ? "Area" : "Kontrak"}}</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="area_kontrak"
               value="{{$parent}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->jenisProgram->jenis_program}}"/>
    </div>
</div>
<div class="panel-footer clearfix bg-white">
    <div class="pull-right">
        <button type="submit" id="submit_form"
                class="btn btn-success btn-square btn-embossed"
                data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
        </button>
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">

    $("#lokasi").autocomplete({
        minLength: 3,
        source: function (req, add) {
            $.ajax({
                url: "{{ url('/') }}/eksternal/get_lokasi",
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {'lokasi': req.term, '_token': "{{csrf_token()}}"},
                success: function (data) {
                    if (data.response == 'true') {
                        console.log(data.message);
                        add(data.message);
                    }
                }
            });
        },
        select: function (event, ui) {
            $("#id_lokasi").val(ui.item.id);
        }
    });
    $('#lokasi').change(function(){
        if($("#lokasi").val() != ""){
            $.ajax({
                url: "{{ url('/') }}/eksternal/get_lokasi",
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {'lokasi': $("#lokasi").val(), '_token': "{{csrf_token()}}"},
                success: function (data) {
                    if (data.response == 'true') {
                        $("#id_lokasi").val(data.message[0].id);
                    }else{
                        $("#id_lokasi").val("0");
                    }
                }
            });
        }else{
            $("#id_lokasi").val("0");
        }
    });
</script>