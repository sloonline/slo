{!! Form::open(['url'=>'/eksternal/create_gardu_area','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id_area_program" value="{{$id_area_program}}"/>
<input type="hidden" name="id" value="{{($gardu_area != null) ? $gardu_area->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Gardu</label>
    </div>
    <div class="col-sm-9">
        <div class="append-icon">
            <input type="text" required="required" name="gardu" id="gardu"
                   class="form-control form-white autocomplete"
                   value="<?php echo ($gardu_area != null) ? $gardu_area->garduInduk->gardu_induk : ""; ?>" >
            <i class="fa fa-building"></i>
            <input type="hidden" name="id_gardu" id="id_gardu"
                   value="{{($gardu_area != null) ? $gardu_area->id_gardu_induk : 0}}"/>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Area</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="area_kontrak"
               value="{{$area->businessArea->description}}"/>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->jenisProgram->jenis_program}}"/>
    </div>
</div>
<div class="panel-footer clearfix bg-white">
    <div class="pull-right">
        <button type="submit" id="submit_form"
                class="btn btn-success btn-square btn-embossed"
                data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
        </button>
    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">

    $("#gardu").autocomplete({
        minLength: 3,
        source: function (req, add) {
            $.ajax({
                url: "{{ url('/') }}/eksternal/get_gardu",
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {'gardu_induk': req.term, '_token': "{{csrf_token()}}"},
                success: function (data) {
                    if (data.response == 'true') {
                        add(data.message);
                    }
                }
            });
        },
        select: function (event, ui) {
            $("#id_gardu").val(ui.item.id);
        }
    });
    $('#gardu').change(function(){
        if($("#gardu").val() != ""){
            $.ajax({
                url: "{{ url('/') }}/eksternal/get_gardu",
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {'gardu_induk': $("#gardu").val(), '_token': "{{csrf_token()}}"},
                success: function (data) {
                    if (data.response == 'true') {
                        $("#id_gardu").val(data.message[0].id);
                    }else{
                        $("#id_gardu").val("0");
                    }
                }
            });
        }else{
            $("#id_gardu").val("0");
        }
    });
</script>