{!! Form::open(['url'=>'/eksternal/create_area_program','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
<input type="hidden" name="id_program" value="{{$program->id}}"/>
<input type="hidden" name="id" value="{{($ap != null) ? $ap->id : 0}}">
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Area</label>
    </div>
    <div class="col-sm-9">
        <select class="form-control form-white" data-search="true" name="ba">
            @foreach($ba as $item)
                <option value="{{$item->id}}" {{(@$ap->business_area == $item->id) ? "selected='selected'" : ""}}>{{$item->description}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Program</label>
    </div>
    <div class="col-sm-9">
        <input type="text" class="form-control form-white" readonly
               name="jenis_program"
               value="{{$program->deskripsi}}"/>
    </div>
</div>
<div class="panel-footer clearfix bg-white">
    <div class="pull-right">
        <button type="submit" id="submit_form"
                class="btn btn-success btn-square btn-embossed"
                data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
        </button>
    </div>
</div>
{!! Form::close() !!}