@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet"/>
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>DATA BAY <strong>GARDU INDUK</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    <div class="panel-header bg-primary">
                        <h2 class="panel-title">Data <strong>Transmisi</strong></h2>
                    </div>
                    <div class="panel-body bg-white">
                        {!! Form::open(array( 'class'=> 'form-horizontal')) !!}
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Nama Instalasi</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white"
                                       value="{{(@$transmisi != null)? @$transmisi->nama_instalasi : ""}}"
                                       name="nama_instalasi" readonly="">
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Sistem Jaringan Transmisi</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white"
                                       value="{{(@$transmisi->sistemJaringanTransmisi != null)? @$transmisi->sistemJaringanTransmisi->nama_reference : ""}}"
                                       name="sistem_jaringan_transmisi" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Kapasitas Total Gardu Induk (MVA)</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white" name="kapasitas_gardu_induk"
                                       value="{{(@$transmisi != null)? @$transmisi->kapasitas_gi : ""}}"
                                       readonly="">
                            </div>
                            <div class="col-sm-3">
                                <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                       value="{{(@$transmisi->teganganPengenal != null)? @$transmisi->teganganPengenal->nama_reference : ""}}"
                                       readonly="">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="panel border">
                    <div class="panel-content">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <i class="icon-plus"></i> <strong>DATA BAY</strong>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in in">
                                <div class="panel-body">
                                    <ul class="nav nav-tabs nav-primary">
                                        @foreach($all_bay as $key => $row)
                                            <li {{($key == BAY_LINE) ? 'class=active' : ""}}><a
                                                        href="#{{str_replace(" ","_",$key)}}"
                                                        data-toggle="tab">{{strtoupper($key)}}</a></li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach($all_bay as $key => $row)
                                            <div class="tab-pane fade {{($key == BAY_LINE) ? "active in" : ""}}"
                                                 id="{{str_replace(" ","_",$key)}}">
                                                <div class="form-group">
                                                    <div class="col-lg-2 pull-right">
                                                        <a href="{{url('/eksternal/create_bay_gardu/'.@$transmisi->id.'/'.$key)}}"
                                                           style="width: 150px;"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-plus"></i>
                                                            ADD
                                                        </a>
                                                    </div>
                                                </div>
                                                <div id="data_{{str_replace(" ","_",$key)}}">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <th>NO.</th>
                                                        <th>NAMA</th>
                                                        <th>JENIS</th>
                                                        <th>KEPEMILIKAN</th>
                                                        <th>PEMILIK</th>
                                                        <th>KAPASITAS PEMUTUS TENAGA (kA)</th>
                                                        <th>KAPASITAS TRAFO TENAGA/KAPASITOR/REAKTOR (MVA)</th>
                                                        <th>TEGANGAN PENGENAL</th>
                                                        <th>FILE LAMPIRAN</th>
                                                        <th>AKSI</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php $i = 1;?>
                                                        @foreach(@$row as $bg)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td style="white-space: nowrap;">{{$bg->nama_bay}}</td>
                                                                <td style="white-space: nowrap;">{{$bg->jenis_bay}}</td>
                                                                <td>{{$bg->tipe_pemilik}}</td>
                                                                <td>{{($bg->tipe_pemilik == MILIK_SENDIRI) ? MILIK_SENDIRI : $bg->pemilik->nama_pemilik}}</td>
                                                                <td>{{@$bg->kapasitas_pemutus}}</td>
                                                                <td>{{@$bg->kapasitas_trafo}}</td>
                                                                <td>{{($bg->pengenal != null) ? $bg->pengenal->nama_reference : ""}}</td>
                                                                <td style="white-space: nowrap;">
                                                                    @if(@$bg->file_sbujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sbujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SBUJPTL</a>
                                                                    @endif
                                                                    @if(@$bg->file_iujk != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_iujk)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>IUJPTL</a>
                                                                    @endif
                                                                    @if(@$bg->file_sld != null)
                                                                        <a target="_blank"
                                                                           href="{{url('upload/'.@$bg->file_sld)}}"
                                                                           class="btn-sm btn-primary"><i
                                                                                    class="fa fa-download"></i>SLD</a>
                                                                    @endif
                                                                </td>
                                                                <td style="white-space: nowrap;">
                                                                    <a href="{{url('/eksternal/edit_bay_gardu/'.@$bg->instalasi_id.'/'.@$bg->id)}}"
                                                                       class="btn btn-sm btn-warning btn-square
                                                                        btn-embossed"><i
                                                                                class="fa fa-pencil-square-o"></i></a>
                                                                    <a onclick="return confirm('Apakah anda yakin untuk menghapus {{@$bg->nama_bay}} ?')"
                                                                       href="{{url('/eksternal/delete_bay_gardu/'.@$bg->id)}}"
                                                                       class="btn btn-sm btn-danger btn-square btn-embossed"><i
                                                                                class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php $i++;?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel border">
                    <div class="panel-content">
                        <div class="panel" id="panel-form-bay">
                            <div class="panel-heading">
                                <h4>
                                    <a data-toggle="collapse" data-parent="#div-form">
                                        <i class="icon-plus"></i> <strong>FORM BAY</strong>
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse in in">
                                <div class="panel-body">
                                    {!! Form::open(array('name'=>'form_bay','id'=>'form_bay','url'=> 'eksternal/create_bay_gardu', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                                    <input type="hidden" name="transmisi_id" value="{{@$transmisi->id}}"/>
                                    <input type="hidden" name="id" value="{{@$id}}"/>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Jenis Bay *</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @if(@$bay == null)
                                                <select class="form-control form-white" data-search="true"
                                                        name="form_jenis_bay" required
                                                        id="form_jenis_bay">
                                                    <option {{(@$jenis == BAY_LINE) ? "selected='selected'" : ""}} value="{{BAY_LINE}}">{{BAY_LINE}}</option>
                                                    <option {{(@$jenis == BAY_KAPASITOR) ? "selected='selected'" : ""}} value="{{BAY_KAPASITOR}}">{{BAY_KAPASITOR}}</option>
                                                    <option {{(@$jenis == BAY_REAKTOR) ? "selected='selected'" : ""}} value="{{BAY_REAKTOR}}">{{BAY_REAKTOR}}</option>
                                                    <option {{(@$jenis == BAY_TRANSFORMATOR) ? "selected='selected'" : ""}} value="{{BAY_TRANSFORMATOR}}">{{BAY_TRANSFORMATOR}}</option>
                                                    <option {{(@$jenis == BAY_COUPLER) ? "selected='selected'" : ""}} value="{{BAY_COUPLER}}">{{BAY_COUPLER}}</option>
                                                    {{-- <option {{(@$jenis == PHB) ? "selected='selected'" : ""}} value="{{PHB}}">{{PHB}}</option> --}}
                                                    <option {{(@$jenis == BAY_CUSTOM) ? "selected='selected'" : ""}} value="{{BAY_CUSTOM}}">{{BAY_CUSTOM}}</option>
                                                </select>
                                            @else
                                                <input type="text" class="form-control form-white" readonly
                                                       value="{{@$bay->jenis_bay}}">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Nama *</label>
                                        </div>
                                        <div class="col-sm-8 prepend-icon">
                                            <input type="text" class="form-control form-white" required
                                                   name="form_nama_bay" id="form_nama_bay" value="{{@$bay->nama_bay}}">
                                            <i class="fa fa-gear"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Status Instalasi *</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control form-white" data-search="true" required
                                                    onchange="isiSuratPernyataan()"
                                                    name="status_baru" id="status_baru">
                                                <option value="">--- Pilih ---</option>
                                                <option value="1" {{(@$bay->status_baru == "1" || @$bay->status_baru == null) ? "selected='selected'":""}}>
                                                    Instalasi Baru
                                                </option>
                                                <option value="0" {{(@$bay->status_baru == "0") ? "selected='selected'":""}}>
                                                    Instalasi Lama
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="status_slo_div">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Status SLO *</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control form-white" data-search="true"
                                                    onchange="changeStatusSlo();"
                                                    name="status_slo" id="status_slo">
                                                <option value="">--- Pilih ---</option>
                                                <option value="1" {{($bay != null && $bay->status_slo == 1) ? "selected='selected'":""}}>
                                                    Perpanjangan SLO
                                                </option>
                                                <option value="0" {{($bay != null && $bay->status_slo == 0) ? "selected='selected'":""}}>
                                                    Belum Ada SLO
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Pemilik Instalasi *</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control form-white" data-search="true" required
                                                    name="form_tipe_pemilik" id="form_jenis_pemilik">
                                                <option {{(@$bay->tipe_pemilik == MILIK_SENDIRI) ? "selected" : ""}} value="{{MILIK_SENDIRI}}">
                                                    Milik sendiri
                                                </option>
                                                <option {{(@$bay->tipe_pemilik == TERDAFTAR) ? "selected" : ""}} value="{{TERDAFTAR}}">
                                                    Milik perusahaan lain yang terdaftar
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="form_perusahaan_terdaftar">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Nama Pemilik Instalasi</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control form-white" data-search="true"
                                                    name="form_pemilik_instalasi_lain"
                                                    id="form_pemilik_instalasi_lain">
                                                @foreach($pemilik as $item)
                                                    <option value="{{$item->id}}" {{(@$bay->tipe_pemilik == TERDAFTAR && @$bay->pemilik_id == $item->id) ? "selected='selected'" : ""}}>{{$item->nama_pemilik}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="kontraktor_div">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Kontraktor (SBU)*</label>
                                            <small class="text-muted">(Jika tidak ada gunakan '0000 - TIDAK ADA
                                                PEMBANGUNAN')
                                            </small>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="hidden" name="kode_kontraktor"
                                                   value="{{@$bay->kode_kontraktor}}" id="kode_kontraktor">
                                            <input type="text"
                                                   value="{{(@$bay->kontraktor != null) ? @$bay->kode_kontraktor . " - " .@$bay->kontraktor : ""}}"
                                                   class="form-control form-white typeahead" autocomplete="off"
                                                   typeahead-editable="false" id="nama_kontraktor"
                                                   name="nama_kontraktor"
                                                   spellcheck="false">
                                        </div>

                                    </div>
                                    <div class="form-group" id="suratpernyataan">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label"><span
                                                        id="label_sp">Surat Pernyataan</span></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" name="file_sp"
                                                           id="file_sp"
                                                           class="custom-file max-file"
                                                           name="avatar"
                                                           onchange="document.getElementById('file_sp_text').value = this.value;">
                                                    <input type="text" id="file_sp_text"
                                                           class="form-control form-white"
                                                           placeholder="no file selected" readonly="">
                                                    <small class="text-muted block"><i
                                                                class="icon-paper-clip"></i> Max
                                                        file size: 1Mb
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                        @if(@$bay->surat_pernyataan_status != null)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$bay->surat_pernyataan_status)}}"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-download"></i>View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Kapasitas Pemutus Tenaga
                                                *</label>
                                        </div>
                                        <div class="col-sm-7 prepend-icon">
                                            <input type="text" class="form-control form-white" required
                                                   value="{{@$bay->kapasitas_pemutus}}"
                                                   name="form_kapasitas_pemutus" id="form_kapasitas_pemutus">
                                            <i class="fa fa-gear"></i>
                                        </div>
                                        <div class="col-sm-1">
                                            <label class="col-sm-12 control-label">kA</label>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div_kapasitas_trafo">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Kapasitas trafo
                                                tenaga/kapasitor/reaktor *</label>
                                        </div>
                                        <div class="col-sm-7 prepend-icon">
                                            <input type="text" class="form-control form-white" required
                                                   value="{{@$bay->kapasitas_trafo}}"
                                                   name="form_kapasitas_trafo" id="form_kapasitas_trafo">
                                            <i class="fa fa-gear"></i>
                                        </div>
                                        <div class="col-sm-1">
                                            <label class="col-sm-12 control-label">MVA</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">Tegangan Pengenal *</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control form-white" data-search="true"
                                                    name="form_tegangan_pengenal" id="form_tegangan_pengenal">
                                                <option value="">--Pilih Tegangan Pengenal--</option>
                                                @foreach($tegangan_pengenal as $item)
                                                    <option {{(@$bay->tegangan_pengenal == $item->id) ? "selected" : ""}} value="{{$item->id}}">{{$item->nama_reference}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="file_sbujk">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">File SBUJPTL</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file"
                                                           name="file_sbujk"
                                                           onchange="document.getElementById('uploader_sbujk').value = this.value;checkform();">
                                                    <input type="text" class="form-control form-white"
                                                           id="uploader_sbujk"
                                                           placeholder="no file selected"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        @if(@$bay->file_sbujk != null)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.@$bay->file_sbujk)}}"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-download"></i>View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group" id="file_iujk">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">File IUJPTL</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file"
                                                           name="file_iujk" id="file_iujk"
                                                           onchange="document.getElementById('uploader_iujk').value = this.value;checkform();">
                                                    <input type="text" class="form-control form-white"
                                                           id="uploader_iujk"
                                                           placeholder="no file selected"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        @if(@$bay->file_iujk!= null)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <a target="_blank"
                                                           href="{{url('upload/'.$bay->file_iujk)}}"
                                                           class="btn btn-primary"><i
                                                                    class="fa fa-download"></i>View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group" id="file_sld">
                                        <div class="col-sm-4">
                                            <label class="col-sm-12 control-label">File Single Line Diagram</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file"
                                                           name="file_sld" id="file_sld"
                                                           onchange="document.getElementById('uploader_sld').value = this.value;checkform();">
                                                    <input type="text" class="form-control form-white"
                                                           id="uploader_sld"
                                                           placeholder="no file selected"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        @if(@$bay->file_sld!= null)
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                </div>
                                                <div class="col-sm-4">
                                                    <a target="_blank"
                                                       href="{{url('upload/'.$bay->file_sld)}}"
                                                       class="btn btn-primary"><i
                                                                class="fa fa-download"></i>View</a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <button type="button"
                                                        onclick="window.location='{{url('/eksternal/view_instalasi_transmisi')}}'"
                                                        class="btn btn-warning btn-square btn-embossed">Batal <i
                                                            class="icon-ban"></i></button>
                                                <button type="submit" id="submit_form"
                                                        class="btn btn-success btn-square btn-embossed"
                                                        data-style="zoom-in">Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script>
                $('html, body').animate({
                    scrollTop: eval($("#panel-form-bay").offset().top - 130)
                }, 2000);
                isiSuratPernyataan();
                changeStatusSlo();
                changeKontraktor('{{@$bay->kode_kontraktor}}');
                $("#form_perusahaan_terdaftar").hide();
                // Initializing the typeahead
                var namaKontraktor = new Array();
                var kodeKontraktor = new Object();
                var idSbuElement = "nama_kontraktor";
                var textInputSbu = document.getElementById(idSbuElement);

                // Init a timeout variable to be used below
                var timeout = null;

                // Listen for keystroke events
                textInputSbu.onkeypress = function (e) {
                    clearTimeout(timeout);
                    namaKontraktor = new Array();
                    kodeKontraktor = new Object();

                    // Make a new timeout set to go off in 800ms
                    timeout = setTimeout(function () {
                        var result;
                        namaKontraktor = new Array();
                        kodeKontraktor = new Object();
                        var query = textInputSbu.value;
                        if (query.length >= 4) {
                            $('#ajax_loader').show();
                            setTimeout(function () {
                                $.ajax({
                                    url: "{{url('/')}}/kontraktor/search/" + query,
                                    type: 'GET',
                                    dataType: 'json',
                                    async: false,
                                    beforeSend: function () {
                                        $('#ajax_loader').show();
                                    },
                                    success: function (data) {
                                        namaKontraktor = new Array();
                                        kodeKontraktor = new Object();
                                        data = JSON.parse(JSON.stringify(data));
                                        var allKontraktor = data.kontraktor;
                                        // result = data.kontraktor;
                                        $.each(allKontraktor, function (index, kontraktor) {
                                            namaKontraktor.push(kontraktor.kode + " - " + kontraktor.nama);
                                            kodeKontraktor[kontraktor.kode + " - " + kontraktor.nama] = kontraktor.kode;
                                        });
                                        $('#nama_kontraktor').typeahead('val', '')
                                        $('#nama_kontraktor').focus().typeahead('val', query).focus();
                                    }
                                });
                                $('#ajax_loader').hide();
                            }, 500);
                        }
                    }, 1000);
                };
                $('.typeahead').typeahead({
                            hint: true,
                            highlight: true, /* Enable substring highlighting */
                            minLength: 4, /* Specify minimum characters required for showing result */
                        },
                        {
                            name: 'kontraktor',
                            source: function (query, process) {
                                return process(namaKontraktor);
                            }
                        }
                );
                $("#nama_kontraktor").on('typeahead:selected', function (e, datum) {
                    var kode = kodeKontraktor[datum];
                    $("#kode_kontraktor").val(kode);
                    $("#file_sp").removeAttr('required');
                });

                $("#nama_kontraktor").change(function () {
                    var nama = $("#nama_kontraktor").val();
                    var kode = kodeKontraktor[nama];
                    changeKontraktor(kode);
                });
                @if(@$bay != null)
                    setPemilik();
                @if(@$bay->tipe_pemilik == TERDAFTAR)
                    $("#form_perusahaan_terdaftar").val('{{@$bay->pemilik_id}}');
                @endif
            @endif

            $("#form_jenis_pemilik").change(function () {
                    setPemilik();
                });
                function setPemilik() {
                    if ($("#form_jenis_pemilik").val() == '{{TERDAFTAR}}') {
                        $("#form_perusahaan_terdaftar").show();
                    } else {
                        $("#form_perusahaan_terdaftar").hide();
                    }
                }
                function isiSuratPernyataan() {

                    var jenis = $("#status_baru").find('option:selected').attr("value");
                    $("#suratpernyataan").hide();

                    if (jenis == "0") {
                        //instalasi lama
                        $("#suratpernyataan").show();
                        @if(@$bay->surat_pernyataan_status == null)
                            $("#file_sp").attr('required', true);
                        @endif
                        $("#file_sbujk").hide();
                        $("#file_iujk").hide();
                        $("#status_slo_div").show();
                        $("#status_slo").attr('required', true);
                        $("#kontraktor_div").hide();
                    } else {
                        $("#status_slo_div").hide();
                        $("#status_slo").removeAttr('required');
                        $("#suratpernyataan").hide();
                        $("#file_sp").removeAttr('required');
                        $("#file_sbujk").show();
                        $("#file_iujk").show();
                        $("#kontraktor_div").show();
                    }
                }
                function changeKontraktor(kode){
                    $("#kode_kontraktor").val(kode);
                    $("#suratpernyataan").hide();
                    $("#file_sp").removeAttr('required');
                    if (typeof kode != 'undefined') {
                        $("#file_sp").removeAttr('required');
                        if (kode == '0000') {
                            //upload filepernyataan
                            //upload file JUPTL/SBUJPTL
                            $("#suratpernyataan").show();
                            $("#file_sp").attr("required", true);
                        }
                    } else {
                        $("#nama_kontraktor").typeahead('val', "");
                        $("#nama_kontraktor").val('');
                        @if(@$bay->surat_pernyataan_status == null)
                            $("#suratpernyataan").show();
                        $("#file_sp").attr("required", true);
                        @endif
                    }
                }
                function changeStatusSlo() {
                    //jika status perpanjangan SLO, label surat pernyataan <-> file SLO lama
                    var status = $("#status_slo").find('option:selected').attr("value");
                    console.log(status);
                    if (status == 1) {
                        //perpanjangan
                        $("#label_sp").html('File SLO Lama');
                    } else {
                        $("#label_sp").html('Surat Pernyataan');
                    }
                }
            </script>
@endsection
