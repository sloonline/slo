@extends('../layout/layout_eksternal')

@section('content')
<div class="page-content page-thin">
          <div class="row">
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="panel">
                <div class="panel-content widget-info">
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-umbrella bg-green"></i>
                    </div>
                    <div class="right">
                      <p class="number countup" data-from="0" data-to="5200">0</p>
                      <p class="text">Permohonan Baru</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="panel">
                <div class="panel-content widget-info">
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-bug bg-blue"></i>
                    </div>
                    <div class="right">
                      <p class="number countup" data-from="0" data-to="575">0</p>
                      <p class="text">Proses SLO</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="panel">
                <div class="panel-content widget-info">
                  <div class="row">
                    <div class="left">
                      <i class="fa fa-fire-extinguisher bg-red"></i>
                    </div>
                    <div class="right">
                      <p class="number countup" data-from="0" data-to="463">0</p>
                      <p class="text">Menunggu Sertifikat</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xlg-2 col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <div class="panel">
                <div class="panel-content widget-info">
                  <div class="row">
                    <div class="left">
                      <i class="icon-microphone bg-purple"></i>
                    </div>
                    <div class="right">
                      <p class="number countup" data-from="0" data-to="1210">0</p>
                      <p class="text">Proses Selesai</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- Search Tracking -->
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-header bg-primary">
                        <h3><i class="icon-magnifier"></i> <strong>Tracking</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="row">
                            <div class="col-md-12">
                                <form>
                                    <div class="append-icon">
                                        <input type="text" name="firstname" id="finder" class="form-control form-white input-lg" placeholder="Masukkan Nomor Registrasi. . ." autofocus>
                                            <i class="icon-magnifier"></i>
                                     </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Slider -->
                    <div class="panel-content">
                        <div class="slick" data-fade="true">
                          <div class="slide">
                            <img src="{{ url('/') }}/assets/global/images/widgets/1.jpg">
                          </div>
                          <div class="slide">
                            <img src="{{ url('/') }}/assets/global/images/widgets/2.jpg">
                          </div>
                          <div class="slide">
                            <img src="{{ url('/') }}/assets/global/images/widgets/3.jpg">
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Permohonan Baru -->
			<div class="col-md-4">
				<a href="{{url('/eksternal/agreement')}}" class="btn btn-hg btn-danger btn-embossed btn-block border"><h2><strong>INPUT ORDER</strong></h2></a>
			</div>
            <div class="col-md-4">
              <div class="panel">
                <div class="panel-header bg-primary">
                  <h3><i class="icon-list"></i> Permohonan <strong>Baru</strong></h3>
                  <div class="control-btn">
                    <span class="pull-right badge badge-white"><strong>{{$latest_permohonan->count()}}</strong></span>
                  </div>
                </div>
                <div class="panel-content widget-news">
                  <div class="withScroll" data-height="307">
                    <a href="#" class="message-item media">
                      <div class="media">
                        <div class="media-body">
                          <div>
                            <small class="pull-right">28 Feb</small>
                            <h4 class="c-dark">Reset your account password</h4>
                            <p class="f-14 c-gray">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a href="#" class="message-item media">
                      <div class="media">
                        <div class="media-body">
                          <div>
                            <small class="pull-right">27 Feb</small>
                            <h4 class="c-dark">Check Dropbox</h4>
                            <p class="f-14 c-gray">Hello Steve, I have added new files in your Dropbox in order to show you how to...</p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a href="#" class="message-item media">
                      <div class="media">
                        <div class="media-body">
                          <div>
                            <small class="pull-right">27 Feb</small>
                            <h4 class="c-dark">New document added</h4>
                            <p class="f-14 c-gray">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a href="#" class="message-item media">
                      <div class="media">
                        <div class="media-body">
                          <div>
                            <small class="pull-right">26 Feb</small>
                            <h4 class="c-dark">You receive a gift</h4>
                            <p class="f-14 c-gray">Hello Steve, I have added new files in your Dropbox in order to show you how to...</p>
                          </div>
                        </div>
                      </div>
                    </a>
                    <a href="#" class="message-item media">
                      <div class="media">
                        <div class="media-body">
                          <div>
                            <small class="pull-right">25 Feb</small>
                            <h4 class="c-dark">Call Alfred for business</h4>
                            <p class="f-14 c-gray">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>

          </div>
@stop