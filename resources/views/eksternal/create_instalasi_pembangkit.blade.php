@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>{{($pembangkit != null) ? "Ubah" : "Tambah"}}  Instalasi <strong>Pembangkit</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">Data Instalasi</a></li>
				<li class="active">{{($pembangkit != null) ? "Ubah" : "Tambah"}} Instalasi Pembangkit</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<p class="m-t-10 m-b-20 f-16">Silahkan inputkan data instalasi pembangkit.</p>
			<div class="panel">
				{!! Form::open(array('url'=> '/eksternal/create_instalasi_pembangkit', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
				<input type="hidden" name="id" value="{{($pembangkit != null) ? $pembangkit->id : 0}}"/>
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>{{($pembangkit != null) ? "Ubah" : "Tambah"}} Instalasi</strong></h3>
				</div>
				<div class="panel-content">
					<div class="form-group">
						<div class="col-sm-3">
						<label class="col-sm-12 control-label">Pemilik Instalasi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="pemilik_instalasi" id="jenis_instalasi">
								<option value="">--- Pilih ---</option>
								@foreach($pemilik as $item)
								<option value="{{$item->id}}" {{($pembangkit != null && $pembangkit->pemilik_instalasi_id == $item->id) ? "selected" : ""}}>{{$item->nama_pemilik}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Jenis Instalasi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="jenis_instalasi" id="jenis_instalasi">
								<option value="">--- Pilih ---</option>
								@foreach($jenis_instalasi as $item)
								<option value="{{$item->id}}" {{($pembangkit != null && $pembangkit->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nama Instalasi</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->nama_instalasi : ""}}" name="nama_instalasi">
							<i class="fa fa-building"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Alamat Instalasi</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<textarea rows="3" class="form-control form-white" name="alamat_instalasi" placeholder="Alamat Instalasi..."> {{($pembangkit != null)? $pembangkit->alamat_instalasi : ""}}</textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Provinsi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="provinsi" id="province">
								<option value="">--- Pilih ---</option>
								@foreach($province as $item)
								<option value="{{$item->id}}" {{($pembangkit != null && $pembangkit->id_provinsi == $item->id) ? "selected" : ""}}>{{$item->province}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kabupaten / Kota</label>
						</div>
						<div class="col-sm-9">
							<select id="city" name="kabupaten" class="form-control form-white" data-search="true">
								<option value="">--- Pilih ---</option>
								@foreach($city as $item)
								<option value="{{$item->id}}" class="{{$item->id_province}}" {{($pembangkit != null && $pembangkit->id_kota == $item->id) ? "selected" : ""}} >{{$item->city}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Longitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->longitude_awal : ""}}" name="longitude_awal">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Latitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->latitude_awal : ""}}" name="latitude_awal">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Longitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->longitude_akhir : ""}}" name="longitude_akhir">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Latitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->latitude_akhir : ""}}" name="latitude_akhir">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kapasitas Terpasang</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->kapasitas_terpasang : ""}}" name="kapasitas_terpasang">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kapasitas Hasil Uji</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->kapasitas_hasil_uji : ""}}" name="kapasitas_hasil_uji">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">No Unit Pembangkit</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->no_unit_pembangkit : ""}}" name="no_unit_pembangkit">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">No Seri Generator</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->no_seri_generator : ""}}" name="no_seri_generator">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">No Seri Turbin / Mesin</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->no_seri_turbin : ""}}" name="no_seri_turbin">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kapasitas Modul Per Unit</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->kapasitas_modul : ""}}" name="kapasitas_modul_per_unit">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kapasitas Inverter Per Unit</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->kapasitas_inverter : ""}}" name="kapasitas_inverter_per_unit">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Jumlah Modul</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->jumlah_modul : ""}}" name="jumlah_modul">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Jumlah Inverter</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->jumlah_inverter : ""}}" name="jumlah_inverter">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Jenis Bahan Bakar</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" name="jenis_bahan_bakar">
								<option value="">--- Pilih ---</option>
								@foreach($jenis_bahan_bakar as $item)
								<option value="{{$item->id}}" {{($pembangkit != null && $pembangkit->id_bahan_bakar == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<hr />
				<div class="panel-footer clearfix bg-white">
					<div class="pull-right">
						<a href="{{url('/eksternal/view_instalasi_pembangkit')}}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
						<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endsection

	@section('page_script')
	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
	<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
	<script>$("#city").chained("#province"); </script>
	@endsection