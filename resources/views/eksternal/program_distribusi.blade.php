@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data Program <strong>Distribusi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li class="active">Program Distribusi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Program</th>
                                <th>Jenis Program</th>
                                <th>Periode</th>
                                <th style="width: auto; ">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;?>
                            @foreach($program as $item)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{@$item->deskripsi}}</td>
                                    <td>{{@$item->jenisProgram->jenis_program}}</td>
                                    <td>{{@$item->periode}}</td>
                                    <td><a href="{{url('/eksternal/create_program/'.$item->id)}}"
                                           class="btn btn-sm btn-warning btn-square btn-embossed"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a href="{{url('/eksternal/delete_program/'.$item->id)}}"
                                           class="btn btn-sm btn-danger btn-square btn-embossed"
                                           onclick="return confirm('Apakah anda yakin untuk menghapus program ini?')"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $no++;?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection