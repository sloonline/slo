@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Daftar</strong> Permohonan</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li class="active"><a href="{{url('/eksternal/view_instalasi_pembangkit')}}">Daftar Permohonan</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    {!! Form::open(array('id'=>'form_order','name'=>'form_order','url'=> '/eksternal/checkout/', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                    <input type="hidden" name="perusahaan" value="{{@$perusahaan->id}}"/>
                    <div class="panel-content p-t-0">
                        <div class="row">
                            <div class="col-md-8">
                                <h3 class="alert bg-primary text-center">DATA <strong>DETAIL ORDER</strong></h3>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <label class="control-label">Nomor
                                            Order</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <h1>
                                            <strong>-</strong>
                                        </h1>
                                        <input type="hidden" name="nomor_order"
                                               value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <label class="control-label">Nomor
                                            Surat
                                            Permintaan *</label>
                                    </div>
                                    <div class="col-sm-6 prepend-icon">
                                        <input name="nomor_sp"
                                               type="text" onkeyup="checkform()"
                                               class="form-control form-white"
                                               placeholder="Nomor Surat" required/>
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <label class="control-label">Tanggal
                                            Surat
                                            Permintaan *</label>
                                    </div>
                                    <div class="col-sm-6 prepend-icon">
                                        <input name="tanggal_sp"
                                               type="text" onchange="checkform()"
                                               data-date-format="dd-mm-yyyy"
                                               class="b-datepicker form-control form-white"
                                               placeholder="Tanggal" required>
                                        <i class="icon-calendar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <label class="control-label">File
                                            Surat
                                            Permintaan *</label><br>
                                        <small class="text-muted">Format file pdf maksimal 1MB.</small>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input id="surat_permintaan" type="file"
                                                       class="custom-file reset"
                                                       name="file_sp" required
                                                       onchange="document.getElementById('uploader').value = this.value;checkform()">
                                                <input type="text"
                                                       class="form-control form-white reset"
                                                       id="uploader"
                                                       placeholder="no file selected"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h3 class="alert bg-primary text-center">DATA <strong>PEMINTA JASA</strong></h3>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-1 prepend-icon">
                                        <input name="nama_perusahaan" type="text"
                                               class="form-control form-white"
                                               placeholder="Nama Perusahaan"
                                               readonly
                                               value="{{$perusahaan->nama_perusahaan}}">
                                        <i class="fa fa-building-o"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-1 prepend-icon">
                                        <input name="nama_perusahaan" type="text"
                                               class="form-control form-white"
                                               placeholder="Kategori Perusahaan"
                                               readonly
                                               value="{{@$perusahaan->kategori->nama_kategori}}">
                                        <i class="fa fa-building-o"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-1 prepend-icon">
                                        <input name="nama_perusahaan" type="text"
                                               class="form-control form-white"
                                               placeholder="Nama Perusahaan"
                                               readonly
                                               value="{{$perusahaan->city->city}}">
                                        <i class="icon-pointer"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-1 prepend-icon">
                                                                <textarea name="alamat_perusahaan"
                                                                          class="form-control form-white"
                                                                          placeholder="Alamat"
                                                                          readonly>{{$perusahaan->alamat_perusahaan}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="m-b-0 p-b-0">
                    <div class="panel-content p-t-0 m-t-0">
                        <h3 class="alert bg-primary text-center">DATA <strong>PERMOHONAN</strong></h3>
                        <fieldset class="cart-summary">
                            @if($permohonan->count() > 0)
                                <div class="table-responsive shopping-cart-table">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td class="text-info w-700">FOTO</td>
                                            <td class="text-info w-700">TANGGAL</td>
                                            <td class="text-info w-700">JENIS INSTALASI</td>
                                            <td class="text-info w-700">JENIS PEKERJAAN</td>
                                            <td class="text-info w-700">JENIS LINGKUP</td>
                                            <td class="text-info w-700">INSTALASI</td>
                                            <td class="text-info w-700">AKSI</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($permohonan as $wa)
                                            <tr>
                                                <td>
                                                    <img style="margin: 0 auto;"
                                                         src="{{($wa->instalasi->foto_1 != null) ?  url('upload/instalasi/'.$wa->instalasi->foto_1) : url('/assets/global/images/picture.png')}}"
                                                         class="img-thumbnail" width="100">
                                                </td>
                                                <td>
                                                    {{$wa->tanggal_permohonan->format('d F Y')}}
                                                </td>
                                                <td>
                                                    {{$wa->tipeInstalasi->nama_instalasi}}
                                                </td>
                                                <td>
                                                    {{$wa->produk->produk_layanan}}
                                                </td>
                                                <td>
                                                    {{@$wa->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}
                                                    @if(@$wa->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                        <br/>
                                                        @foreach($wa->bayPermohonan as $bg)
                                                            - {{$bg->bayGardu->nama_bay}}<br/>
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$wa->instalasi->nama_instalasi}}
                                                </td>
                                                <td>
                                                    <a href="{{url('/eksternal/keranjang/delete/'.$wa->id)}}"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       onclick="return confirm('Apakah anda yakin menghapus permohonan tersebut?')"><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif

                            @if($permohonan_distribusi->count() > 0)
                                <h2>PERMOHONAN INSTALASI DISTRIBUSI</h2>
                                <hr/>
                                <div class="table-responsive shopping-cart-table">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <td class="text-info w-700">NOMOR SURAT</td>
                                            <td class="text-info w-700">TANGGAL SURAT</td>
                                            <td class="text-info w-700">PERIODE</td>
                                            <td class="text-info w-700">JENIS PEKERJAAN</td>
                                            <td class="text-info w-700">AKSI</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($permohonan_distribusi as $wa)
                                            <tr>
                                                <td>
                                                    {{$wa->nomor_surat}}
                                                </td>
                                                <td>
                                                    {{date('d F Y',strtotime($wa->tanggal_surat))}}
                                                </td>
                                                <td>
                                                    {{$wa->periode}}
                                                </td>
                                                <td>
                                                    <?php $layanan = $wa->produk ?>
                                                    @foreach(@$layanan as $row)
                                                        <p>{{@$row->produk->produk_layanan}}</p>
                                                    @endforeach
                                                </td>
                                                <td>
                                                    <a href="{{url('/eksternal/keranjang/delete_surat_tugas/'.$wa->id)}}"
                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                       onclick="return confirm('Apakah anda yakin menghapus permohonan {{$wa->nomor_surat}} ?')"><i
                                                                class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </fieldset>
                    </div>
                    <hr class="m-b-0 p-b-0">
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="{{url('/eksternal/view_order')}}" class="btn btn-warning btn-square btn-embossed">Kembali
                                &nbsp;<i class="icon-arrow-left"></i></a>
                            <a class="btn btn-primary btn-square btn-embossed" onClick="window.location.reload()">Refresh
                                &nbsp;<i class="fa fa-refresh"></i></a>
                            @if(($permohonan->count() + $permohonan_distribusi->count()) > 0)
                                <button type="button" id="btn_checkout" class="btn btn-success btn-square btn-embossed">
                                    Checkout &nbsp;<i class="fa fa-shopping-cart"></i></button>
                            @endif
                        </div>
                    </div>
                    <!-- BEGIN MODAL AGGREEMENT -->
                    <div class="modal fade" id="modal-aggreement" style="margin-left: -150px; margin-top: -100px;"
                         tabindex="-1"
                         role="dialog"
                         aria-hidden="true">
                        <div class="modal-dialog" style="width: 1000px;">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color: teal;color:white;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                                class="icons-office-52"></i></button>
                                    <h4 class="modal-title"><strong>Persetujuan</strong> Pengguna</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            @include('eksternal/syarat')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END MODAL AGGREEMENT-->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                function checkform() {
                    var f = document.forms["form_order"].elements;
                    var cansubmit = true;

                    for (var i = 0; i < f.length; i++) {
                        if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                            cansubmit = false;
                        }
                    }
                    document.getElementById('btn_checkout').disabled = !cansubmit;
                }
                window.onload = checkform();

                $("#btn_checkout").on('click', function () {
                    $("#modal-aggreement").modal("show");
                });
                $("#btn_setuju").on('click', function () {
                    $("#btn_submit").click();
                });
            </script>
            <!-- Buttons Loading State -->
@endsection