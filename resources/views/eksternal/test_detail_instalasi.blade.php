@extends('../layout/layout_eksternal')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.demo2.css" />
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Detail</strong> Instalasi</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/instalasi')}}">Instalasi</a></li>
                    <li class="active">Detail Instalasi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <br/>
                    <div class="panel-content p-b-0">
                        <div class="row column-seperation">
                            <div class="col-md-4 line-separator">
                                <section id="gallery" class="simplegallery">
                                    <div class="content">
                                        <img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" class="image_1 img-thumbnail" alt="" />
                                        <img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" class="image_2 img-thumbnail" style="display:none" alt="" />
                                        <img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" class="image_3 img-thumbnail" style="display:none" alt="" />
                                    </div>
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <a href="#" rel="1">
                                                <img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" id="thumb_1" alt="" />
                                            </a>
                                        </div>
                                        <div class="thumb">
                                            <a href="#" rel="2">
                                                <img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" id="thumb_2" alt="" />
                                            </a>
                                        </div>
                                        <div class="thumb last">
                                            <a href="#" rel="3">
                                                <img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" id="thumb_3" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </section>
                                {{--<div class="widget widget_slider p-0">--}}
                                    {{--<div class="slick" data-arrows="true" data-fade="true">--}}
                                        {{--<div class="slide">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg"--}}
                                                 {{--width="500" class="img-thumbnail">--}}
                                        {{--</div>--}}
                                        {{--<div class="slide">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg"--}}
                                                 {{--width="500" class="img-thumbnail">--}}
                                        {{--</div>--}}
                                        {{--<div class="slide">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg"--}}
                                                 {{--width="500" class="img-thumbnail">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div id="sliderFrame" class="p-0">--}}
                                        {{--<div id="slider">--}}
                                            {{--<a class="lazyImage" href="{{ url('/') }}/assets/global/images/widgets/square1.jpg" title=""></a>--}}
                                            {{--<a class="lazyImage" href="{{ url('/') }}/assets/global/images/widgets/square2.jpg" title=""></a>--}}
                                            {{--<a class="lazyImage" href="{{ url('/') }}/assets/global/images/widgets/square3.jpg" title=""></a>--}}
                                        {{--</div>--}}
                                        {{--<div id="thumbs">--}}
                                            {{--<div class="thumb"><img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" /></div>--}}
                                            {{--<div class="thumb"><img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" /></div>--}}
                                            {{--<div class="thumb"><img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" /></div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <h1 class="f-32">Nama <strong>Instalasi</strong></h1>
                                        <hr/>
                                        <br/>
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label><input type="checkbox" data-checkbox="icheckbox_square-blue">
                                                        Supervisi Komisioning</label>
                                                    <label><input type="checkbox" checked
                                                                  data-checkbox="icheckbox_square-blue"> Rekomendasi
                                                        Teknik Laik Bertegangan </label>
                                                    <label><input type="checkbox" data-checkbox="icheckbox_square-blue">
                                                        Rekomendasi Teknik Laik Sinkron</label>
                                                    <label><input type="checkbox" data-checkbox="icheckbox_square-blue">
                                                        SLO</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label><input type="checkbox" data-checkbox="icheckbox_square-blue">
                                                        Re SLO</label>
                                                    <label><input type="checkbox" checked
                                                                  data-checkbox="icheckbox_square-blue"> Supervisi
                                                        NDC</label>
                                                    <label><input type="checkbox" data-checkbox="icheckbox_square-blue">
                                                        Supervisi Performance Test</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 p-t-30">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary btn-square btn-embossed">Add To
                                                Cart &nbsp;<i class="icon-basket"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-content p-t-0">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-user"></i>
                                    General</a></li>
                            <li><a href="#lokasi" data-toggle="tab"><i class="fa fa-building-o"></i> Lokasi</a></li>
                            <li><a href="#ijinusaha" data-toggle="tab"><i class="fa fa-building-o"></i> Ijin Usaha</a>
                            </li>
                            <li><a href="#dokumen" data-toggle="tab"><i class="fa fa-building-o"></i> Dokumen</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Nama Instalasi</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lokasi">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Lokasi</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="ijinusaha">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Ijin Usaha</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="dokumen">
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <label class="col-sm-12 control-label">Dokumen</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="append-icon">
                                                        <input type="text" class="form-control form-white">
                                                        <i class="fa fa-building-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.min.js"></script>
    <script>
        $('#gallery').simplegallery({
                galltime : 400,
                gallcontent: '.content',
                gallthumbnail: '.thumbnail',
                gallthumb: '.thumb'
            });
    </script>
@endsection