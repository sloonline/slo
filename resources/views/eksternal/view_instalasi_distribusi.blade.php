@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Instalasi</strong> Distribusi</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/view_instalasi_distribusi')}}">Data Instalasi</a></li>
                    <li class="active">Distribusi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('eksternal/create_instalasi_distribusi')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Tambah Instalasi</a>
                            </div>
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>Foto</th>
                                <th>Nama Instalasi</th>
                                <th>Jenis Instalasi</th>
                                <th>Jenis Program</th>
                                <th>Provinsi</th>
                                <th>Kabupaten</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; ?>
                            @if(is_array($distribusi) || is_object($distribusi))
                                @foreach($distribusi as $item)
                                    <tr>
                                        {{--<td>{{ $no }}</td>--}}
                                        <td><a href="{{url('eksternal/instalasi-distribusi/'.$item->id)}}"><img
                                                        style="margin: 0 auto;"
                                                        src="{{($item->foto_1 != null) ?  url('upload/instalasi/'.$item->foto_1) : url('/assets/global/images/picture.png')}}"
                                                        class="img-thumbnail" width="100"></a></td>
                                        <td>
                                            <a href="{{url('eksternal/instalasi-distribusi/'.$item->id)}}">{{ @$item->nama_instalasi }}</a>
                                        </td>
                                        <td>{{ @$item->jenis_instalasi->jenis_instalasi }}</td>
                                        <td>{{ @$item->jenis_program->jenis_program }}</td>
                                        <td>{{ @$item->provinsi->province }}</td>
                                        <td>{{ @$item->kota->city }}</td>
                                        <td style="white-space: nowrap;">
                                            @if(@$item->isAllowEdit() || @$item->isOrderRej())
                                                <a href="{{url('/eksternal/create_instalasi_distribusi/'.$item->id)}}"
                                                   class="btn btn-sm btn-warning btn-square btn-embossed"
                                                   data-toggle="tooltip"
                                                   title="EDIT INSTALASI"><i
                                                            class="fa fa-pencil"></i></a>
                                            @endif
                                            <a href="{{url('/eksternal/delete_instalasi_distribusi/'.$item->id)}}"
                                               class="btn btn-sm btn-danger btn-square btn-embossed"
                                               onclick="return confirm('Apakah anda yakin untuk menghapus instalasi ini?')">
                                                <i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
@endsection