@extends('layout/layout_eksternal')

@section('page_css')
<!-- BEGIN PAGE STYLE -->
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/rateit/rateit.css" rel="stylesheet">
<!-- END PAGE STYLE -->
@endsection

@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="header">
		{{-- <h2>Permohonan <strong>{{$tipe->nama_instalasi}}</strong></h2> --}}
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="#">Order</a></li>
				<li class="active">Permohonan</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<p class="m-t-10 m-b-20 f-16">{{-- Silahkan inputkan detail permohonan anda. --}}</p>
			<div class="panel">
				<div class="panel-body bg-white">
					{!! Form::open(['url'=>'/eksternal/custom_field', 'post', 'files'=> true]) !!}
					@foreach ($formField->form($lingkup_id) as $field) 
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Turbin</label>
						</div>
						<div class="col-sm-9">
							<div class="file">
								<div class="option-group">
									<span class="file-button btn-primary">Choose File</span>
									{!!Form::file($field['name'], $field)!!}
									<input type="text" class="form-control form-white" id="file_turbin"
									placeholder="no file selected" readonly="">
								</div>
							</div>
						</div>
					</div>
					@endforeach
					{!! Form::submit('Save') !!}
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('page_script')
<!-- BEGIN PAGE SCRIPT -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
<!-- END PAGE SCRIPTS -->
@endsection