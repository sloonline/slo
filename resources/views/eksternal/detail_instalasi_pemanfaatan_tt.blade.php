@extends('../layout/layout_eksternal')

@section('page_css')
    <link rel="stylesheet" type="text/css"
          href="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.demo2.css"/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Instalasi</strong> Pemanfaatan TT</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/view_instalasi_pemanfaatan_tt')}}">Instalasi Pemanfaatan TT</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <br/>
                    <div class="panel-content p-b-0">
                        <div class="row column-seperation">
                            <div class="col-md-4">
                                {{--<section id="gallery" class="simplegallery">--}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <img id="img-instalasi"
                                             src="{{($instalasi->foto_1 != null) ?  url('upload/instalasi/'.$instalasi->foto_1) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail">
                                        {{--<img src="{{ url('/assets/global/images/widgets/square1.jpg') }}" class="img-thumbnail" alt="" />--}}
                                        {{--<img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" class="image_2 img-thumbnail" style="display:none" alt="" />--}}
                                        {{--<img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" class="image_3 img-thumbnail" style="display:none" alt="" />--}}
                                    </div>
                                </div>
                                {{--<div class="row" style="margin-top: 10px;">--}}
                                {{--@if($instalasi->foto_1 != null)--}}
                                {{--<div class="col-md-3">--}}
                                {{--<a href="#">--}}
                                {{--<img onclick="document.getElementById('img-instalasi').src = this.getAttribute('src')"--}}
                                {{--src="{{($instalasi->foto_1 != null) ?  url('upload/instalasi/'.$instalasi->foto_1) : url('/assets/global/images/picture.png')}}"--}}
                                {{--id="thumb_1" alt="" class="img-thumbnail"/>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--@if($instalasi->foto_2 != null)--}}
                                {{--<div class="col-md-3">--}}
                                {{--<a href="#">--}}
                                {{--<img onclick="document.getElementById('img-instalasi').src = this.getAttribute('src')"--}}
                                {{--src="{{($instalasi->foto_2 != null) ?  url('upload/instalasi/'.$instalasi->foto_2) : url('/assets/global/images/picture.png')}}"--}}
                                {{--id="thumb_2" alt="" class="img-thumbnail"/>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--@if($instalasi->foto_3 != null)--}}
                                {{--<div class="col-md-3">--}}
                                {{--<a href="#">--}}
                                {{--<img onclick="document.getElementById('img-instalasi').src = this.getAttribute('src')"--}}
                                {{--src="{{($instalasi->foto_3 != null) ?  url('upload/instalasi/'.$instalasi->foto_3) : url('/assets/global/images/picture.png')}}"--}}
                                {{--id="thumb_3" alt="" class="img-thumbnail"/>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--@if($instalasi->foto_4 != null)--}}
                                {{--<div class="col-md-3">--}}
                                {{--<a href="#">--}}
                                {{--<img onclick="document.getElementById('img-instalasi').src = this.getAttribute('src')"--}}
                                {{--src="{{($instalasi->foto_4 != null) ?  url('upload/instalasi/'.$instalasi->foto_4) : url('/assets/global/images/picture.png')}}"--}}
                                {{--id="thumb_4" alt="" class="img-thumbnail"/>--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                <div class="row">
                                    <div class="col-md-12"></div>
                                    <div id="mapdiv" class="col-md-12">
                                        <fieldset class="cart-summary">
                                            <legend>Lokasi Instalasi</legend>
                                            <div id="mapid"
                                                 style="width: 100%; height: 200px; z-index: 1; position:relative"></div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 border-left">
                                {!! Form::open(array('url'=> '/eksternal/add-to-cart/','id' => 'form_order' ,'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                                {!! Form::hidden('id_instalasi',$instalasi->id) !!}
                                {!! Form::hidden('id_tipe_instalasi',$tipe_instalasi->id) !!}
                                {!! Form::hidden('status_baru',$instalasi->status_baru) !!}
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <h4 class="f-20"><strong>{{$instalasi->nama_instalasi}}</strong></h4>
                                            </div>
                                            {{--@if(@$instalasi->isAllowEdit())--}}
                                                <div class="col-md-2" style="text-align: right;">
                                                    <a href="{{url('/eksternal/edit-instalasi-pemanfaatan-tt/'.$instalasi->id)}}"
                                                       class="btn btn-warning btn-square btn-embossed btn-block">Edit
                                                        &nbsp;<i
                                                                class="fa fa-pencil-square-o"></i></a>
                                                </div>
                                            {{--@endif--}}
                                        </div>
                                        <hr/>
                                        @if($instalasi->lhpp != null)
                                            <strong>Instalasi sudah masuk dalam proses LHPP</strong>
                                        @else
                                            Silakan pilih lingkup pekerjaan dan layanan yang diinginkan. Klik <strong>Add
                                                to
                                                cart</strong> untuk memasukkan ke dalam order.
                                            <br/>
                                            <br/>
                                            <label for="lingkup_pekerjaan">Lingkup Pekerjaan</label>
                                            <select class="form-control form-white" data-search="true"
                                                    onchange="checkform();checkOrdered();"
                                                    name="lingkup_pekerjaan" id="lingkup_pekerjaan">
                                                <option value="">--Pilih Lingkup--</option>
                                                @foreach($lingkup_pekerjaan as $item)
                                                    <option value="{{$item->id}}">{{$item->jenis_lingkup_pekerjaan}}</option>
                                                @endforeach
                                            </select>
                                            <div id="form_upload">
                                                {{--Form upload file digenerate di sini--}}
                                            </div>
                                            <div class="panel border m-t-30">
                                                <div class="panel-header bg-primary">
                                                    <h2 class="panel-title"><strong>LAYANAN</strong></h2>
                                                </div>
                                                <div class="panel-body bg-white">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            @foreach($layanan as $wa)
                                                                <label class="col-md-6">
                                                                    <input id="layanan_{{$wa->id}}"
                                                                           name="layanan_{{$wa->id}}"
                                                                           value="{{$wa->id}}" type="checkbox"
                                                                           @if($selected_layanan!=null){{($wa->id==$selected_layanan->id) ? 'checked' : ''}}@endif data-checkbox="icheckbox_square-blue">
                                                                    {{$wa->produk_layanan}}
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    @endif

                                    @if($instalasi->lhpp == null)
                                        <div class="col-sm-12 p-t-30">
                                            <div class="pull-right">
                                                <button type="submit" id="submit_form"
                                                        class="btn btn-primary btn-square btn-embossed">Add to
                                                    cart &nbsp;<i class="icon-basket"></i>
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel-content p-t-0">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#kapasitas" data-toggle="tab"><i
                                            class="icon-speedometer"></i> KAPASITAS</a></li>
                            <li><a href="#pemilik" data-toggle="tab"><i class="icon-user"></i> PEMILIK</a></li>
                            <li><a href="#lokasi" data-toggle="tab"><i class="icon-map"></i> LOKASI</a></li>
                            <li><a href="#history" data-toggle="tab"><i
                                            class="fa fa-history"></i> RIWAYAT</a>
                            </li>
                            {{--<li class="active"><a href="#general" data-toggle="tab"><i class="icon-user"></i>--}}
                            {{--General</a></li>--}}
                            {{--<li><a href="#lokasi" data-toggle="tab"><i class="fa fa-building-o"></i> Lokasi</a></li>--}}
                            {{--<li><a href="#ijinusaha" data-toggle="tab"><i class="fa fa-building-o"></i> Ijin Usaha</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#dokumen" data-toggle="tab"><i class="fa fa-building-o"></i> Dokumen</a></li>--}}
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="control-label">Status Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               value="{{($instalasi != null)? (($instalasi->status_baru == 1) ? "INSTALASI BARU" : "INSTALASI LAMA") :""}}"
                                               name="nama_instalasi" readonly="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        {{--{{$instalasi->jenis_instalasi->jenis_instalasi}}--}}
                                        {{--<i class="fa fa-building" style="margin-left: 15px;"></i>--}}
                                        <input type="text" class="form-control form-white"
                                               value="{{($instalasi != null)? $instalasi->jenis_instalasi->jenis_instalasi : ""}}"
                                               name="jenis_instalasi" readonly="">

                                        {{--<select class="form-control form-white" data-search="true" name="jenis_instalasi" id="jenis_instalasi">--}}
                                        {{--<option value="">--- Pilih ---</option>--}}
                                        {{--@foreach($jenis_instalasi as $item)--}}
                                        {{--<option value="{{$item->id}}" {{($instalasi != null && $instalasi->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               value="{{($instalasi != null)? $instalasi->nama_instalasi : ""}}"
                                               name="nama_instalasi" readonly="">
                                        {{--<i class="fa fa-building" style="margin-left: 15px;"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">PHB TM</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="phb_tm"
                                               value="{{($instalasi != null)? $instalasi->phb_tm : ""}}" readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">PHB TR</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="phb_tr"
                                               value="{{($instalasi != null)? $instalasi->phb_tr : ""}}" readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penyedia Tenaga Listrik</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               name="penyedia_tenaga_listrik"
                                               value="{{($instalasi != null)? $instalasi->penyedia_tl : ""}}"
                                               readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                               value="{{($instalasi->teganganPengenal != null)? $instalasi->teganganPengenal->nama_reference : ""}}"
                                               readonly="">
                                        {{--<select class="form-control form-white" data-search="true" name="tegangan_pengenal">--}}
                                        {{--<option value="">--- Pilih ---</option>--}}
                                        {{--@foreach($tegangan_pengenal as $item)--}}
                                        {{--<option value="{{$item->id}}" {{($instalasi != null && $instalasi->tegangan_pengenal == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kontraktor</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="tegangan_pengenal"
                                               value="{{(@$instalasi->kode_kontraktor != null)? @$instalasi->kode_kontraktor ." - ".@$instalasi->kontraktor: ""}}"
                                               readonly="">
                                    </div>
                                </div>
                                @if(@$instalasi->file_pernyataan != null)
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File Pernyataan</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <a target="_blank"
                                               href="{{url('upload/'.$instalasi->file_pernyataan)}}"
                                               class="btn btn-primary"><i
                                                        class="fa fa-download"></i>View</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="kapasitas">
                                <!-- kapasitas instalasi -->
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kapasitas Terpasang (kVA)</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white"
                                               name="kapasitas_trafo_terpasang"
                                               value="{{($instalasi != null)? $instalasi->kapasitas_trafo : ""}}"
                                               readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Daya Tersambung (kVA)</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="daya_tersambung"
                                               value="{{($instalasi != null)? $instalasi->daya_sambung : ""}}"
                                               readonly="">
                                        {{--<i class="fa fa-credit-card"></i>--}}
                                    </div>
                                </div>
                                <!-- end kapasitas instalasi -->
                            </div>
                            <div class="tab-pane fade" id="pemilik">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Pemilik <strong>Instalasi</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kepemilikan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kepemilikan" name="kepemilikan"
                                                               placeholder="Kepemilikan" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->perusahaan->kategori->nama_kategori : null}}"
                                                               readonly="">
                                                        {{--<i class="fa fa-building"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="nama_pemilik" name="nama_pemilik"
                                                               placeholder="Nama Pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->nama_pemilik : null}}"
                                                               readonly="">
                                                        {{--<i class="fa fa-building"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                              <textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
                                                        class="form-control form-white"
                                                        placeholder="Alamat Instalasi..."
                                                        readonly="">{{($instalasi->pemilik != null) ? $instalasi->pemilik->alamat_pemilik : null}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi" name="provinsi" placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik->province != null) ? $instalasi->pemilik->province->province : null}}"
                                                               readonly="">

                                                        {{--<select name="provinsi" class="form-control form-white" data-search="true"--}}
                                                        {{--id="province">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($province as $item)--}}
                                                        {{--<option value="{{$item->id}}"--}}
                                                        {{--{{($pemilik != null && $pemilik->id_province == $item->id) ? "selected" : ""}}--}}
                                                        {{-->{{$item->province}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kabupaten" name="kabupaten"
                                                               placeholder="Kabupaten / Kota" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik->city != null) ? $instalasi->pemilik->city->city : null}}"
                                                               readonly="">

                                                        {{--<select name="kabupaten" id="city" class="form-control form-white" data-search="true">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($city as $item)--}}
                                                        {{--<option value="{{$item->id}}"--}}
                                                        {{--class="{{$item->id_province}}"--}}
                                                        {{--{{($pemilik != null && $pemilik->id_city == $item->id) ? "selected" : ""}}--}}
                                                        {{-->{{$item->city}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="kode_pos" name="kode_pos" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->kode_pos_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Telepon</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="telepon" name="telepon" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->telepon_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Fax</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_fax" name="no_fax" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_fax_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="email_pemilik" name="email_pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->email_pemilik : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">Ijin <strong>Usaha</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="jenis_ijin_usaha" name="jenis_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->jenisIjinUsaha->nama_reference : null}}"
                                                               readonly="">
                                                        {{--<select id="jenis_ijin_usaha" name="jenis_ijin_usaha" class="form-control form-white">--}}
                                                        {{--<option value="">--- Pilih ---</option>--}}
                                                        {{--@foreach($jenis_ijin_usaha as $item)--}}
                                                        {{--                                                                <option value="{{$item->id}}" {{($pemilik != null && $pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
                                                        {{--<option value="{{$item->id}}" >{{$item->nama_reference}}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Penerbit Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="penerbit_ijin_usaha" name="penerbit_ijin_usaha"
                                                               type="text" class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->penerbit_ijin_usaha : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_ijin_usaha" name="no_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_ijin_usaha : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_iu" name="masa_berlaku_iu" type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                                               value="{{($instalasi->pemilik->masa_berlaku_iu != null) ? $instalasi->pemilik->masa_berlaku_iu->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Surat Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if($instalasi->pemilik->file_siup != null)
                                                            <a href="{{url('upload/'.$instalasi->pemilik->file_siup)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{$instalasi->pemilik->file_siup}}
                                                            </a>
                                                        @endif
                                                        {{--<div class="file">--}}
                                                        {{--<div class="option-group">--}}
                                                        {{--<span class="file-button btn-primary">--}}
                                                        {{--                                                            {{($pemilik != null && $pemilik->file_siup != null) ? "Ganti File" : "Choose File"}}--}}
                                                        {{--Choose File--}}
                                                        {{--</span>--}}
                                                        {{--<input type="file" class="custom-file" name="file_surat_iu"--}}
                                                        {{--onchange="document.getElementById('uploader').value = this.value;">--}}
                                                        {{--<input type="text" class="form-control form-white" id="uploader"--}}
                                                        {{--placeholder="no file selected"--}}
                                                        {{--                                                                       value="{{($pemilik != null && $pemilik->file_siup != null) ? $pemilik->file_siup : null}}">--}}
                                                        {{--value="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="nama_kontrak" name="nama_kontrak" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->nama_kontrak : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_kontrak" name="no_kontrak" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_kontrak : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal Pengesahaan
                                                            Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="tgl_pengesahan_kontrak" name="tgl_pengesahan_kontrak"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{($instalasi->pemilik->tgl_pengesahan_kontrak != null) ? $instalasi->pemilik->tgl_pengesahan_kontrak->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            Kontrak</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_kontrak" name="masa_berlaku_kontrak"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{($instalasi->pemilik->masa_berlaku_kontrak != null) ? $instalasi->pemilik->masa_berlaku_kontrak->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if($instalasi->pemilik->file_kontrak != null)
                                                            <a href="{{url('upload/'.$instalasi->pemilik->file_kontrak)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{$instalasi->pemilik->file_kontrak}}
                                                            </a>
                                                        @endif

                                                        {{--<div class="file">--}}
                                                        {{--<div class="option-group">--}}
                                                        {{--<span class="file-button btn-primary">Choose File</span>--}}
                                                        {{--<input type="file" class="custom-file" name="file_kontrak_sewa"--}}
                                                        {{--onchange="document.getElementById('uploader2').value = this.value;">--}}
                                                        {{--<input type="text" class="form-control form-white" id="uploader2"--}}
                                                        {{--placeholder="no file selected"--}}
                                                        {{--value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">--}}
                                                        {{--value="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="no_spjbtl" name="no_spjbtl" type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_spjbtl : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="tgl_spjbtl" name="tgl_spjbtl" type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                                               value="{{($instalasi->pemilik->tgl_spjbtl != null) ? $instalasi->pemilik->tgl_spjbtl->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="masa_berlaku_spjbtl" name="masa_berlaku_spjbtl"
                                                               type="text"
                                                               class="form-control date-picker form-white"
                                                               data-format="yyyy-mm-dd"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{($instalasi->pemilik->masa_berlaku_spjbtl != null) ? $instalasi->pemilik->masa_berlaku_spjbtl->format('d F Y') : null}}"
                                                               readonly="">
                                                        {{--value="">--}}
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        @if($instalasi->pemilik->file_spjbtl != null)
                                                            <a href="{{url('upload/'.$instalasi->pemilik->file_spjbtl)}}"
                                                               target="_blank">
                                                                <i class="fa fa-download"></i> {{$instalasi->pemilik->file_spjbtl}}
                                                            </a>
                                                        @endif

                                                        {{--<div class="file">--}}
                                                        {{--<div class="option-group">--}}
                                                        {{--<span class="file-button btn-primary">Choose File</span>--}}
                                                        {{--<input type="file" class="custom-file" name="file_spjbtl"--}}
                                                        {{--onchange="document.getElementById('uploader3').value = this.value;">--}}
                                                        {{--<input type="text" class="form-control form-white" id="uploader3"--}}
                                                        {{--placeholder="no file selected"--}}
                                                        {{--                                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">--}}
                                                        {{--value="">--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lokasi">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong>(AWAL)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <textarea rows="3" class="form-control form-white"
                                                                  name="alamat_instalasi"
                                                                  placeholder="Alamat Instalasi..."
                                                                  readonly=""> {{($instalasi != null)? $instalasi->alamat_instalasi : ""}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi_instalasi" name="provinsi_instalasi"
                                                               placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->provinsi != null) ? $instalasi->provinsi->province : null}}"
                                                               readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi_instalasi" name="provinsi_instalasi"
                                                               placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->kota != null) ? $instalasi->kota->city : null}}"
                                                               readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{($instalasi != null)? $instalasi->longitude_awal : ""}}"
                                                               name="longitude_awal" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{($instalasi != null)? $instalasi->latitude_awal : ""}}"
                                                               name="latitude_awal" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong>(AKHIR)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">

                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <textarea rows="3" class="form-control form-white"
                                                                  name="alamat_instalasi"
                                                                  placeholder="Alamat Instalasi..."
                                                                  readonly=""> {{($instalasi != null)? $instalasi->alamat_akhir_instalasi : ""}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi_instalasi" name="provinsi_instalasi"
                                                               placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->provinsi_akhir != null) ? $instalasi->provinsi_akhir->province : null}}"
                                                               readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input id="provinsi_instalasi" name="provinsi_instalasi"
                                                               placeholder="Provinsi"
                                                               type="text"
                                                               class="form-control form-white"
                                                               value="{{($instalasi->kota_akhir != null) ? $instalasi->kota_akhir->city : null}}"
                                                               readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{($instalasi != null)? $instalasi->longitude_akhir : ""}}"
                                                               name="longitude_akhir" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control form-white"
                                                               value="{{($instalasi != null)? $instalasi->latitude_akhir : ""}}"
                                                               name="latitude_akhir" readonly="">
                                                        {{--<i class="fa fa-credit-card"></i>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="history">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-dynamic">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nomor Order</th>
                                                <th>Nomor Permohonan</th>
                                                <th>Tanggal Order</th>
                                                <th>Nama Layanan</th>
                                                <th>Aksi</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(is_array($permohonan) || is_object($permohonan))
                                                <?php $no = 1; ?>
                                                @foreach($permohonan as $item)

                                                    <tr>
                                                        <td>{{ $no}}</td>
                                                        <td>{{ $item->nomor_order}}</td>
                                                        <td>{{ $item->nomor_permohonan}}</td>
                                                        <td>{{ date('d M Y',strtotime($item->tanggal_order)) }}</td>
                                                        <td>{{ $item->produk_layanan}}</td>

                                                        <td>
                                                            <a href="{{url('/eksternal/detail_order/'.$item->id_orders)}}"
                                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                        class="fa fa-eye"></i></a></td>

                                                    </tr>
                                                    <?php $no++; ?>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.min.js"></script>
            <script>
                //generateForm();

                $('#gallery').simplegallery({
                    galltime: 400,
                    gallcontent: '.content',
                    gallthumbnail: '.thumbnail',
                    gallthumb: '.thumb'
                });

                $('#lingkup_pekerjaan').change(function () {
                    generateForm();
                });

                function generateForm() {
                    if ($('#lingkup_pekerjaan').val() != "") {
                        $('#form_upload').load("/eksternal/custom_field/" + $('#lingkup_pekerjaan').val() + "/0/{{($instalasi != null)  ? $instalasi->status_baru : 1}}");
                        //                        console.log($('#lingkup_pekerjaan').val());
                        $('#form_upload').show();
                    } else {
                        $('#form_upload').hide();
                    }
                }

                // For oncheck callback
                $('input[name*="layanan"]').on('ifChecked', function () {
                    checkform();
                });

                $('input[name*="layanan"]').on('ifUnchecked', function () {
                    checkform();
                });

                function checkform() {
                    var f = document.forms["form_order"].elements;
                    var cansubmit = false;
                    var validLingkup = true;
                    var validLayanan = true;

                    //cek pilih lingkup pekerjaan
                    validLingkup = ($("#lingkup_pekerjaan option:selected").val() > 0) ? true : false;

                    //cek pilih layanan
                    var layanan = $("input[name*='layanan']:checked").length - $("input[name*='layanan']:disabled").length;
                    validLayanan = (layanan > 0) ? true : false;

                    if (validLingkup && validLayanan) {
                        cansubmit = true;
                    }
                    document.getElementById('submit_form').disabled = !cansubmit;
                }
                window.onload = checkform;

                //Jika layanan sudah diplih, tidak bisa dipilih kembali di detail instalasi

                function checkOrdered() {
                    var lingkup = $("#lingkup_pekerjaan option:selected").val();
                    var ordered = '{!!json_encode($used,JSON_NUMERIC_CHECK)!!}';
                    ordered = JSON.parse(ordered);
                    var used = ordered[lingkup];
                    console.log(ordered);
                    $('input[name*="layanan"]').iCheck('enable');
                    $('input[name*="layanan"]').iCheck('uncheck');
                    if (Array.isArray(used)) {
                        for (var i = 0; i < used.length; i++) {
                            var dt = used[i];
                            $("#layanan_" + dt.id_produk).iCheck('disable');
                            $("#layanan_" + dt.id_produk).iCheck('check');
                        }
                    }
                }

                generateMap();

                function generateMap() {
                    //First, specify your Mapbox API access token
                    var mymap = L.map('mapid');
                    var ltlng = [-6.1751, 106.8650];
                    var ltlng_line = [];
                    var isMark = false;
                    var isPoly = false;

                    @if($instalasi->longitude_awal != null && $instalasi->latitude_awal != null)
                            ltlng = [{{$instalasi->latitude_awal}}, {{$instalasi->longitude_awal}}];
                    isMark = true;
                    @endif

                            @if($instalasi->longitude_akhir != null && $instalasi->latitude_akhir != null)
                            ltlng_line = [[{{$instalasi->latitude_awal}}, {{$instalasi->longitude_awal}}],
                        [{{$instalasi->latitude_akhir}}, {{$instalasi->longitude_akhir}}]];
                    isPoly = true;
                    @endif

                    mymap.setView(ltlng, 5);

                    if (isPoly) {
                        var polyline = L.polyline(ltlng_line, {color: 'blue'}).addTo(mymap);
                        mymap.fitBounds(polyline.getBounds());
                    } else if (isMark) {
                        var marker = L.marker(ltlng).addTo(mymap);
                    }


                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                        maxZoom: 18,
                        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Imagery � <a href="http://mapbox.com">Mapbox</a>',
                        id: 'mapbox.streets'
                    }).addTo(mymap);
                }
            </script>
@endsection
