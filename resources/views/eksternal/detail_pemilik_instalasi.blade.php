@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Detail Pemilik <strong>Instalasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/pemilik_instalasi')}}">Pemilik Instalasi</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(array('url'=> '', 'class'=> 'form-horizontal')) !!}
                    <input type="hidden" name="id" value="{{($pemilik != null) ? $pemilik->id : null}}">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#kontrak" data-toggle="tab"><i
                                            class="icon-speedometer"></i> KONTRAK</a></li>
                            <li><a href="#spjbtl" data-toggle="tab"><i class="icon-user"></i> SPJBTL</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->nama_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->alamat_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Provinsi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$pemilik->province->province}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$pemilik->city->city}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->kode_pos_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Telepon</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->telepon_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">No Fax</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->no_fax_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Email</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->email_pemilik}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9">
                                        {{$pemilik->jenisIjinUsaha->nama_reference}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->penerbit_ijin_usaha}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->no_ijin_usaha}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{date('d-M-Y',strtotime($pemilik->masa_berlaku_iu))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Surat Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <?php if($pemilik != null && $pemilik->file_siup != null){
                                        ?>
                                        <a href="{{url('upload/'.$pemilik->file_siup)}}" class="btn btn-primary"><i
                                                    class="fa fa-download"></i>View</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kontrak">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Kontrak</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->nama_kontrak}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->no_kontrak}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal Pengesahaan Kontrak</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{date('d-M-Y',strtotime($pemilik->tgl_pengesahan_kontrak))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Masa Berlaku Kontrak</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{date('d-M-Y',strtotime($pemilik->masa_berlaku_kontrak))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <?php if($pemilik != null && $pemilik->file_kontrak != null){
                                        ?>
                                        <a href="{{url('upload/'.$pemilik->file_kontrak)}}" class="btn btn-primary"><i
                                                    class="fa fa-download"></i>View</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="spjbtl">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{$pemilik->no_spjbtl}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{date('d-M-Y',strtotime($pemilik->tgl_spjbtl))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Masa Berlaku SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        {{date('d-M-Y',strtotime($pemilik->masa_berlaku_spjbtl))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <?php if($pemilik != null && $pemilik->file_spjbtl != null){
                                        ?>
                                        <a href="{{url('upload/'.$pemilik->file_spjbtl)}}"
                                           class="btn btn-primary"><i
                                                    class="fa fa-download"></i>View</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="p-t-0 m-t-0">
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="{{url('/eksternal/pemilik_instalasi')}}"
                               class="btn btn-warning btn-square btn-embossed">Kembali &nbsp;<i class="icon-arrow-left"></i></a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
    <script>$("#city").chained("#province"); </script>
@endsection