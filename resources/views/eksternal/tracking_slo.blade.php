 @extends('../layout/layout_eksternal')
 
 @section('content')
<div class="page-content page-thin">
    <div class="row">
            <div class="col-lg-12 portlets">
            <h2>Tracking Permohonan SLO</h2>
              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="fa fa-table"></i> Pembangkit</h3>
                </div>
                <div class="panel-content">
                  <table class="table dataTable" id="table_pembangkit">
                    <thead>
                      <tr>
                        <th class="no_sort" tabindex="0" rowspan="1" colspan="1" style="width: 42px;"></th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Nomor
                        </th>
                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 279px;">
                          Lokasi Pembangkit
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 350px;">
                          Jenis Pembangkit
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 322px;">
                          Jenis Usaha Penyedia Tenaga Listrik
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 241px;">
                          Bahan Bakar
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Kapasitas
                        </th>
                        <th tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <tr class="gradeA odd">
                        <td class="center "></td>
                        <td class=" sorting_1">1</td>
                        <td class=" sorting_1">PLTU Wil. A</td>
                        <td class=" ">PLTU</td>
                        <td class=" ">Pelanggan</td>
                        <td class="center ">Batu Bara</td>
                        <td class="center ">... kVA</td>
                        <td class="center "><button class="btn btn-primary" data-toggle="modal" data-target="#colored-header">Tracking</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="fa fa-table"></i> Transmisi</h3>
                </div>
                <div class="panel-content">
                  <table class="table dataTable" id="table_transmisi">
                    <thead>
                      <tr>
                        <th class="no_sort" tabindex="0" rowspan="1" colspan="1" style="width: 42px;"></th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Nomor
                        </th>
                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 279px;">
                          Jenis Transmisi
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 350px;">
                          Jenis Usaha Penyedia Listrik
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 322px;">
                          Kepemilikan Sistem Jaringan
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 241px;">
                          Sistem Jaringan
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Tegangan Pengenal
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Jumlah Tower
                        </th>
                        <th tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <tr class="gradeA odd">
                        <td class="center "></td>
                        <td class=" sorting_1">1</td>
                        <td class=" sorting_1">Gardu Induk</td>
                        <td class=" ">PIUPTL</td>
                        <td class=" ">Milik BUMN</td>
                        <td class="center ">Sumatera</td>
                        <td class="center ">.. kV</td>
                        <td class="center ">.. Tower</td>
                        <td class="center "><button class="btn btn-primary">Tracking</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="panel">
                <div class="panel-header panel-controls">
                  <h3><i class="fa fa-table"></i> Distribusi</h3>
                </div>
                <div class="panel-content">
                  <table class="table dataTable" id="table_distribusi">
                    <thead>
                      <tr>
                        <th class="no_sort" tabindex="0" rowspan="1" colspan="1" style="width: 42px;"></th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Nomor
                        </th>
                        <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 279px;">
                          Jenis Instalasi
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 350px;">
                          Jenis Usaha Penyedia Tenaga Listrik
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 322px;">
                          Kepemilikan Sistem Jaringan
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 241px;">
                          Sistem Jaringan
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 241px;">
                          Tegangan Pengenal
                        </th>
                        <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                          Aksi
                        </th>
                      </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                      <tr class="gradeA odd">
                        <td class="center "></td>
                        <td class=" sorting_1">1</td>
                        <td class=" sorting_1">Jaringan Distribusi Tegangan Menengah</td>
                        <td class=" ">IO</td>
                        <td class=" ">Milik Swasta</td>
                        <td class="center ">Wilayah Distribusi Jawa Barat</td>
                        <td class="center ">.. kV</td>
                        <td class="center "><button class="btn btn-primary">Tracking</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>
    <div class="modal fade" id="colored-header" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header bg-primary">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                      <h4 class="modal-title"><strong>Tracking </strong> Permohonan</h4>
                    </div>
                    <div class="modal-body">
                        <div style="padding: 20px;">
                            Lokasi : PLTU Wil.A<br/>
                            Jenis Pembangkit : Tenga Uap<br/>
                            Jenis Usaha Penyedia Listrik : Pelanggan<br/>
                            Bahan Bakar : Batu Bara<br/>
                            Kapasitas : ..kVA<br/>
                        </div>
                        <div style="padding: 20px;">
                        <table class="table dataTable" id="table_tracking">
                            <thead>
                              <tr>
                                <th class="no_sort" tabindex="0" rowspan="1" colspan="1" style="width: 42px;"></th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 171px;">
                                  No
                                </th>
                                <th class="sorting_asc" tabindex="0" rowspan="1" colspan="1" style="width: 279px;">
                                  Status
                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 350px;">
                                  Lokasi
                                </th>
                                <th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 322px;">
                                  Tanggal
                                </th>
                              </tr>
                            </thead>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                          <tr class="gradeA odd">
                                            <td class="center "></td>
                                            <td class=" sorting_1">1</td>
                                            <td class=" sorting_1">Kontrak/Penugasan</td>
                                            <td class=" ">Pusertif</td>
                                            <td class=" ">12 Januari 2016</td>
                                          </tr>
                                          <tr class="gradeA odd">
                                           <td class="center "></td>
                                           <td class=" sorting_1">2</td>
                                           <td class=" sorting_1">Pembukaan Pekerjaan</td>
                                           <td class=" ">Pusertif</td>
                                           <td class=" ">22 Januari 2016</td>
                                          </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
@stop

@section('script')
<script type="text/javascript">
$(function () {


});

</script>
@stop