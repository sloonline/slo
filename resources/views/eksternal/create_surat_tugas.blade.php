@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/jquery-treegrid/css/jquery.treegrid.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.css" rel="stylesheet">
    <style>
        .ui-autocomplete {
            position: absolute;
            z-index: 2147483647;
            cursor: default;
            padding: 5px;
            margin-top: 2px;
            list-style: none;
            background-color: #ffffff;
            border: 1px solid #ccc;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        }

        .ui-autocomplete > li {
            padding: 10px 20px;
        }

        .ui-autocomplete > li.ui-state-focus {
            background-color: #DDD;
        }
    </style>
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Surat <strong>Tugas</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li class="active">Surat Tugas</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-bookmark"></i> DATA <strong>SURAT TUGAS</strong></h3>
                    </div>
                    <div class="panel-content">
                        <div class="nav-tabs2">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#general" data-toggle="tab"><i class="icon-home"></i> General</a>
                                </li>
                                <li>
                                    @if($surat != null)
                                        <a href="#program" data-toggle="tab"><i
                                                    class="icon-user"></i> Program Distribusi</a>
                                    @endif
                                </li>
                            </ul>
                            <div class="tab-content bg-white">
                                <div class="tab-pane active" id="general">
                                    {!! Form::open(array('id'=>'form_surat','url'=> '/eksternal/create_surat_tugas', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                    <input type="hidden" name="id" value="{{($surat == null)?0 : $surat->id}}"/>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Nama Perusahaan</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <h1>{{Auth::user()->perusahaan->nama_perusahaan}}</h1>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Nomor Surat Tugas</label>
                                            </div>
                                            <div class="col-sm-6 prepend-icon">
                                                <input name="nomor_surat" onkeyup="checkform()"
                                                       value="{{($surat != null) ? $surat->nomor_surat : "" }}"
                                                       type="text" class="form-control form-white"
                                                       placeholder="Nomor Surat" required>
                                                <i class="icon-envelope"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Tanggal Surat Tugas</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <input name="tanggal_surat" onkeyup="checkform()"
                                                       value="{{($surat != null) ? $surat->tanggal_surat : "" }}"
                                                       type="text"
                                                       class="date-picker form-control form-white" placeholder="Tanggal"
                                                       required>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Periode (Tahun)</label>
                                            </div>
                                            <div class="col-sm-9 prepend-icon">
                                                <input name="periode" type="number" min="1900" onkeyup="checkform()"
                                                       value="{{($surat != null) ? $surat->periode : "" }}"
                                                       class="form-control form-white" placeholder="Periode Surat"
                                                       required/>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">Layanan</label>
                                            </div>
                                            <div class="col-sm-9">
                                                @foreach($layanan as $row)
                                                    <div class="col-sm-4">
                                                        <input type="checkbox" id="layanan_{{$row->id}}" name="produk[]"
                                                               kode="{{$row->kode_layanan}}" onchange="checkform();"
                                                               {{(in_array(array('produk_id' => $row->id), $used_layanan)) ? "checked='checked'" : ""}}
                                                               value="{{$row->id}}"/> {{$row->produk_layanan}}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label">File Surat</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="file">
                                                    <div class="option-group">
                                                        <span class="file-button btn-primary">
                                                        {{($surat != null && $surat->file_surat != null) ? "Ganti File" : "Choose File"}}
                                                        </span>
                                                        <input type="file" class="custom-file" name="file_surat"
                                                               required
                                                               onchange="document.getElementById('uploader').value = this.value;checkform();" {{($surat == null) ? "required" : ""}}>
                                                        <input type="text" class="form-control form-white" id="uploader"
                                                               placeholder="no file selected"
                                                               value="{{($surat != null && $surat->file_surat != null) ? $surat->file_surat : null}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($surat != null && $surat->file_surat != null){
                                        ?>
                                        <div class="form-group">
                                            <div class="col-sm-2 m-l-30">
                                                <label class="col-sm-12 control-label"></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <a href="{{url('upload/'.$surat->file_surat)}}" class="btn btn-primary"><i
                                                            class="fa fa-download"></i>View</a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" id="submit_form"
                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                data-style="zoom-in">Simpan &nbsp;<i
                                                    class="fa fa-save"></i>
                                        </button>
                                        <a href="{{url('/eksternal/surat_tugas')}}"
                                           class="btn btn-danger btn-square btn-embossed">Kembali
                                            &nbsp;<i class="icon-remove"></i></a>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane" id="program">
                                    <div class="m-b-10 border-bottom">
                                        <div class="btn-group">
                                            <button type="button" onclick="formProgram(0)"
                                                    class="btn btn-success btn-square btn-block btn-embossed"><i
                                                        class="fa fa-plus"></i> Tambah Program
                                            </button>
                                        </div>
                                    </div>
                                    <table class="tree table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 70px;">No</th>
                                            <th>Nama Program</th>
                                            <th style="width: 150px;">Jenis Program</th>
                                            <th>Periode</th>
                                            <th style="width: 180px;">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1; ?>
                                        @if($surat != null)
                                            @foreach($surat->program as $row)
                                                <tr class="treegrid-ac1{{$row->id}}">
                                                    <td>{{$no}}</td>
                                                    <td>{{$row->deskripsi}}</td>
                                                    <td>{{$row->jenisProgram->jenis_program}}</td>
                                                    <td>{{$row->periode }}</td>
                                                    <td>
                                                        <button type="button"
                                                                onclick="formLevel1('{{$row->jenisProgram->acuan_1}}','{{$row->id}}',0)"
                                                                class="btn btn-sm btn-success btn-square btn-embossed"
                                                                data-toggle="tooltip"
                                                                title="TAMBAH {{$row->jenisProgram->acuan_1}} ({{$row->deskripsi}})">
                                                            <i
                                                                    class="fa fa-plus"></i></button>
                                                        <button type="button" onclick="formProgram({{$row->id}})"
                                                                class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                data-toggle="tooltip"
                                                                title="EDIT"><i
                                                                    class="fa fa fa-pencil"></i></button>
                                                        <a href="{{url('/eksternal/delete_komponen_program/'.@$surat->id.'/'.$row->id.'/'.TIPE_PROGRAM)}}"
                                                           class="btn btn-sm btn-danger btn-square btn-embossed"
                                                           onclick="return confirm('Apakah anda yakin untuk menghapus program ini?')"><i
                                                                    class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>
                                                <?php $firstChild = $row->firstChild();?>
                                                @if(sizeof($firstChild) > 0)
                                                    <tr class="treegrid-ac20  treegrid-parent-ac1{{$row->id}}">
                                                        <td style="background-color: lightgrey;"></td>
                                                        <td colspan="4"
                                                            style="background-color: lightgrey;">
                                                            <b>{{$row->jenisProgram->acuan_1}}</b></td>
                                                    </tr>
                                                @endif
                                                @foreach($firstChild as $child1)
                                                    <tr class="treegrid-ac2{{$child1->id}}  treegrid-parent-ac1{{$row->id}}">
                                                        <td></td>
                                                        <td colspan="3">{{$child1->name}}</td>
                                                        <td>
                                                            <button type="button"
                                                                    onclick="formLevel2('{{$row->jenisProgram->acuan_2}}','{{$row->id}}','{{$child1->id}}',0)"
                                                                    class="btn btn-sm btn-success btn-square btn-embossed"
                                                                    data-toggle="tooltip"
                                                                    title="TAMBAH {{$row->jenisProgram->acuan_2}} ({{$child1->name}})">
                                                                <i class="fa fa-plus"></i></button>
                                                            <button type="button"
                                                                    onclick="formLevel1('{{$row->jenisProgram->acuan_1}}','{{$row->id}}','{{$child1->id}}')"
                                                                    class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                    data-toggle="tooltip"
                                                                    title="EDIT"><i
                                                                        class="fa fa fa-pencil"></i></button>
                                                            <a href="{{url('/eksternal/delete_komponen_program/'.@$surat->id.'/'.$child1->id.'/'.$row->jenisProgram->acuan_1)}}"
                                                               class="btn btn-sm btn-danger btn-square btn-embossed"
                                                               onclick="return confirm('Apakah anda yakin untuk menghapus {{$child1->name}} ?')"><i
                                                                        class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php $secondChild = $row->secondChild($child1->obj);?>
                                                    @if(sizeof($secondChild) > 0)
                                                        <tr class="treegrid-ac30  treegrid-parent-ac2{{$child1->id}}">
                                                            <td style="background-color: lightgrey;"></td>
                                                            <td colspan="4"
                                                                style="background-color: lightgrey;padding-left: 50px;">
                                                                <b>{{$row->jenisProgram->acuan_2}}</b></td>
                                                        </tr>
                                                    @endif
                                                    @foreach($secondChild as $child2)
                                                        <tr class="treegrid-ac3{{$child2->id}}  treegrid-parent-ac2{{$child1->id}}">
                                                            <td colspan="1"></td>
                                                            <td colspan="3" style="padding-left: 50px;">{{$child2->name}}</td>
                                                            <td>
                                                                <a href="{{url('/eksternal/create_instalasi_program/'.$row->id.'/'.$child1->id.'/'.$child2->id.'')}}"
                                                                   class="btn btn-sm btn-success btn-square btn-embossed"
                                                                   data-toggle="tooltip"
                                                                   title="TAMBAH INSTALASI ({{$child2->name}})"><i
                                                                            class="fa fa-plus"></i></a>
                                                                <button type="button"
                                                                        onclick="formLevel2('{{$row->jenisProgram->acuan_2}}','{{$row->id}}','{{$child1->id}}','{{$child2->id}}')"
                                                                        class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                        data-toggle="tooltip"
                                                                        title="{{$row->jenisProgram->acuan_1}}"><i
                                                                            class="fa fa fa-pencil"></i></button>
                                                                <a href="{{url('/eksternal/delete_komponen_program/'.@$surat->id.'/'.$child2->id.'/'.$row->jenisProgram->acuan_2)}}"
                                                                   class="btn btn-sm btn-danger btn-square btn-embossed"
                                                                   onclick="return confirm('Apakah anda yakin untuk menghapus {{$child2->name}} ?')"><i
                                                                            class="fa fa-trash"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php $instalasi = $child2->instalasi; ?>
                                                        @foreach($instalasi as $dt)
                                                            <tr class="treegrid-ac4{{$dt->id}}  treegrid-parent-ac3{{$child2->id}}">
                                                                <td colspan="1"></td>
                                                                <td colspan="3"
                                                                    style="padding-left: 100px;">{{$dt->nama_instalasi}}</td>
                                                                <td>
                                                                    <a href="{{url('/eksternal/create_instalasi_program/'.$row->id.'/'.$child1->id.'/'.$child2->id.'/'.$dt->id)}}"
                                                                       class="btn btn-sm btn-warning btn-square btn-embossed"
                                                                       data-toggle="tooltip"
                                                                       title="EDIT INSTALASI"><i
                                                                                class="fa fa fa-pencil"></i></a>
                                                                    <a href="{{url('/eksternal/delete_komponen_program/'.@$surat->id.'/'.$dt->id.'/'.TIPE_INSTALASI_PROGRAM)}}"
                                                                       class="btn btn-sm btn-danger btn-square btn-embossed"
                                                                       onclick="return confirm('Apakah anda yakin untuk menghapus {{$dt->nama_instalasi}} ?')"><i
                                                                                class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                                <?php $no++; ?>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>

        <!-- BEGIN MODAL -->
        <div class="modal fade" id="modal-form" aria-hidden="true" data-backdrop="static" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width: 1000px;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <div class="pull-right">
                            <button type="button" style="color: white;" class="btn btn-sm btn-warning btn-square"
                                    data-dismiss="modal" aria-hidden="true"><b><i
                                            class="icons-office-52"></i></b></button>
                        </div>
                        <h4 class="modal-title"><span id="form-title"></span></h4>
                    </div>
                    <div class="modal-body" id="form-body">
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL-->

        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script src="{{ url('/') }}/assets/global/plugins/jquery-treegrid/js/jquery.treegrid.js"></script>
            <script type="text/javascript">
                $('.tree').treegrid();
                for (var i = 0; i < $("input[name*='produk']:checked").length; i++) {
                    var produk = $("input[name*='produk']:checked")[i];
                    validateLayanan(produk.getAttribute('kode'));
                }
                function formProgram(id) {
                    $("#form-title").html('FORM PROGRAM DISTRIBUSI');
                    showFormKomponenProgram('{{($surat == null) ? 0 : $surat->id}}', id, 0, 0, '{{TIPE_PROGRAM}}');
                }

                function formLevel1(tipe, id_program, id) {
                    switch (tipe) {
                        case "{{TIPE_AREA}}":
                            $("#form-title").html('FORM AREA PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_KONTRAK}}":
                            $("#form-title").html('FORM KONTRAK PROGRAM DISTRIBUSI');
                            break;
                    }
                    showFormKomponenProgram(id_program, id, 0, 0, tipe);
                }

                function formLevel2(tipe, id_program, id_area_kontrak, id) {
                    switch (tipe) {
                        case "{{TIPE_LOKASI}}":
                            $("#form-title").html('FORM LOKASI PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_PENYULANG}}":
                            $("#form-title").html('FORM PENYULANG PROGRAM DISTRIBUSI');
                            break;
                        case "{{TIPE_GARDU}}":
                            $("#form-title").html('FORM GARDU PROGRAM DISTRIBUSI');
                            break;
                    }
                    showFormKomponenProgram(id_program, id_area_kontrak, id, 0, tipe);
                }


                function showFormKomponenProgram(param1, param2, param3, param4, tipe) {
                    $("#form-body").load("{{url('/eksternal/edit_komponen_program/')}}/" + param1 + "/" + param2 + "/" + param3 + "/" + param4 + "/" + tipe, function () {
                        $("#modal-form").modal('show');
                    });
                }

                // For oncheck callback
                $('input[name*="produk"]').on('ifChecked', function () {
                    checkform();
                    var kode = this.getAttribute('kode');
                    validateLayanan(kode);
                });

                $('input[name*="produk"]').on('ifUnchecked', function () {
                    $('input[name*="produk"]').iCheck('enable');
                    checkform();
                });


                function validateLayanan(kode) {
                    if (kode == "{{KODE_SLO}}") {
                        var reslo = document.querySelector('[kode="{{KODE_RESLO}}"]');
                        reslo = reslo.getAttribute('id');
                        $("#" + reslo).iCheck('disable');
                    } else if (kode == "{{KODE_RESLO}}") {
                        var slo = document.querySelector('[kode="{{KODE_SLO}}"]');
                        slo = slo.getAttribute('id');
                        $("#" + slo).iCheck('disable');
                    }
                }

                function checkform() {
                    var f = document.forms["form_surat"].elements;
                    var cansubmit = true;
                    var validLayanan = true;
                    for (var i = 0; i < f.length; i++) {
                        if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                            cansubmit = false;
                        }
                    }
                    //cek pilih layanan
                    var layanan = $("input[name*='produk']:checked").length - $("input[name*='produk']:disabled").length;
                    validLayanan = (layanan > 0) ? true : false;
                    cansubmit = validLayanan;
                    document.getElementById('submit_form').disabled = !cansubmit;
                }
                window.onload = checkform;
            </script>
            <script type="text/javascript">
                var tab = "general";
                @if(Session::get('tab') != null)
                        tab = "program";
                @endif
                $('.nav-tabs a[href="#' + tab + '"]').tab('show');
            </script>
@endsection