@extends('../layout/layout_eksternal')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.demo2.css" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/global/plugins/hover-effects/hover-effects.css">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Layanan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal')}}">Layanan</a></li>
                    <li class="active">{{$layanan->produk_layanan}}</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <br/>
                    <div class="panel-content">
                        <div class="row column-seperation">
                            <div class="col-md-4 line-separator">
                                <section id="gallery" class="simplegallery">
                                <div class="content">
                                    <img src="{{ url('/assets/global/images/produk/'.$layanan->foto) }}" class="image_1 img-thumbnail" alt="" />
                                    {{--<img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" class="image_2 img-thumbnail" style="display:none" alt="" />--}}
                                    {{--<img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" class="image_3 img-thumbnail" style="display:none" alt="" />--}}
                                </div>
                                {{--<div class="thumbnail">--}}
                                    {{--<div class="thumb">--}}
                                        {{--<a href="#" rel="1">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" id="thumb_1" alt="" />--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="thumb">--}}
                                        {{--<a href="#" rel="2">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" id="thumb_2" alt="" />--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="thumb last">--}}
                                        {{--<a href="#" rel="3">--}}
                                            {{--<img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" id="thumb_3" alt="" />--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                </section>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <h1 class="f-32">{{explode(' ',$layanan->produk_layanan)[0]}} <strong>{{ltrim(strstr($layanan->produk_layanan,' '))}}</strong></h1>
                                        <h3 class="m-t-0"><strong>{{$tipe_instalasi->nama_instalasi}}</strong></h3><hr/>
                                        <div>
                                            <p>{{$layanan->keterangan}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="panel-header bg-primary text-center">
                        <h3><i class="icon-energy"></i> <strong>Instalasi {{$tipe_instalasi->nama_instalasi}}</strong></h3>
                    </div>
                    <div class="panel-content p-t-0 p-b-20">

                        @if($instalasi->count()!=0)

                            <div style="margin-top: 15px; margin-bottom: 15px;" class="row">
                                <div class="col-md-11">
                                    Silakan pilih instalasi {{$tipe_instalasi->nama_instalasi}} yang akan dilakukan {{$layanan->produk_layanan}}.
                                </div>
                                <div class="col-md-1" style="text-align: right;">
                                    <a href="javascript:" onclick="display_grid()" title="Grid View"><i class="fa fa-th-large"></i></a>
                                    <a href="javascript:" onclick="display_list()" title="List View"><i class="fa fa-th-list"></i></a>
                                </div>

                            </div>
                            <div class="portfolioContainer grid" id="grid_instalasi">
                                @foreach($instalasi as $wa)
                                    <div class="col-md-3 pembangkit p-b-20 text-center" style="margin-bottom: 25px;">
                                        <img style="margin: 0 auto; width: 200px;height: 150px;" src="{{($wa->foto_1 != null) ?  url('upload/instalasi/'.$wa->foto_1) : url('/assets/global/images/picture.png')}}"
                                             class="img-thumbnail"
                                             onmouseover="this.style.cursor='pointer';" onclick="window.location.href='{{url('eksternal/layanan/'.$layanan->id.'/'.$tipe_instalasi->id.'/'.$wa->id)}}';"/>
                                        <h5 class="m-0 m-t-10" onmouseover="this.style.cursor='pointer';"
                                            onclick="window.location.href='{{url('eksternal/layanan/'.$layanan->id.'/'.$tipe_instalasi->id.'/'.$wa->id)}}';">
                                            <strong>{{$wa->nama_instalasi}}</strong></h5>
                                        <h5 class="m-0" onmouseover="this.style.cursor='pointer';"
                                            onclick="window.location.href='{{url('eksternal/layanan/'.$layanan->id.'/'.$tipe_instalasi->id.'/'.$wa->id)}}';">
                                            {{$wa->pemilik->nama_pemilik}}</h5>
                                    </div>
                                @endforeach
                            </div>

                            <div id="list_instalasi" class="hidden">
                                <table class="table table-hover table-dynamic">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nama Instalasi</th>
                                        <th>Jenis</th>
                                        <th>Pemilik</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(is_array($instalasi) || is_object($instalasi))
                                        <?php $no=1; ?>
                                        @foreach($instalasi as $item)
                                            <tr>
                                                {{--<td>{{ $no }}</td>--}}
                                                <td><a href="{{url('eksternal/layanan/'.$layanan->id.'/'.$tipe_instalasi->id.'/'.$item->id)}}"><img style="margin: 0 auto;" src="{{($item->foto_1 != null) ?  url('upload/instalasi/'.$item->foto_1) : url('/assets/global/images/picture.png')}}" class="img-thumbnail" width="100"></a></td>
                                                <td><a href="{{url('eksternal/layanan/'.$layanan->id.'/'.$tipe_instalasi->id.'/'.$item->id)}}">{{ $item->nama_instalasi }}</a></td>
                                                <td>{{ $item->jenis_instalasi->jenis_instalasi }}</td>
                                                <td>{{ $item->pemilik->nama_pemilik }}</td>
                                                <td>{{ $item->provinsi->province }}</td>
                                                <td>{{ $item->kota->city }}</td>
                                            </tr>
                                            <?php $no++; ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                            </div>

                        @else

                            <div class="" style="text-align: center;">
                                <br/>
                                <div style="text-align: center;">Anda belum memiliki instalasi {{$tipe_instalasi->nama_instalasi}}. Klik tombol <strong>Tambah Instalasi</strong> untuk membuat instalasi baru.</div>
                                <br>
                                <button type="button" class="btn btn-success" onclick="window.location.href='{{url('eksternal/'.$url_create[$tipe_instalasi->id])}}'"><i class="fa fa-plus"></i> Tambah Instalasi</button>
                                <br/>
                                <br/>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_script')
<script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/magnific/jquery.magnific-popup.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/isotope/isotope.pkgd.min.js"></script>
<script>
    $(window).load(function(){
      var $container = $('.portfolioContainer');
      $container.isotope();
      $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
        var selector = $(this).attr('data-filter');
        $container.isotope({
          filter: selector
        });
        return false;
      });
        $('#gallery').simplegallery({
            galltime : 400,
            gallcontent: '.content',
            gallthumbnail: '.thumbnail',
            gallthumb: '.thumb'
        });

    });

    function display_list(){
        $('#list_instalasi').removeClass('hidden');
        $('#grid_instalasi').addClass('hidden');
    }

    function display_grid(){
        $('#list_instalasi').addClass('hidden');
        $('#grid_instalasi').removeClass('hidden');
    }
</script>

@endsection