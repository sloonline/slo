@extends('../layout/layout_internal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="page-content">
        <div class="header">
            <h2>Manajemen <strong>Kontrak</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="#">Master</a></li>
                    <li class="active">Kontrak</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-exclamation-sign"></i> {{Session::get('error')}}
                    </div>
                @endif
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <p class="m-t-10 m-b-20 f-16"></p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Data <strong>Kontrak</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('eksternal/create_kontrak')}}" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah Kontrak</a>
                            </div>
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Program</th>
                                <th>Periode</th>
                                <th>Nomor Kontrak</th>
                                <th>Tanggal Kontrak</th>
                                <th>Vendor</th>
                                <th>User Create</th>
                                <th>User Update</th>
                                <th style="width: 170px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            ?>
                            @foreach($kontrak as $row)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row->program->nama_program}}</td>
                                    <td>{{$row->periode}}</td>
                                    <td>{{$row->nomor_kontrak}}</td>
                                    <td>{{date('d-M-Y',strtotime($row->tanggal_kontrak))}}</td>
                                    <td>{{$row->vendor}}</td>
                                    <td>{{$row->user_create}}</td>
                                    <td>{{$row->user_update}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Edit" href="{{url('eksternal/create_kontrak/'.$row->id)}}"><i class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" title="" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus kontrak ini?')" href="{{url('eksternal/delete_kontrak/'.$row->id)}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection


        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection