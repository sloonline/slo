@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
    rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Order</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(array('url'=> 'eksternal/revise_order','class'=> 'form-horizontal')) !!}
                <div class="panel">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                GENERAL</a></li>
                                <li><a href="#order" data-toggle={{($order == null) ? "" :"tab"}}><i
                                    class="icon-bag"></i> PERMOHONAN</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="general">
                                        <input type="hidden" name="perusahaan" value="{{$perusahaan->id}}"/>
                                        <input type="hidden" name="order_id" value="{{($order != null) ? $order->id : ""}}">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="panel border">
                                                    <div class="panel-header bg-primary">
                                                        <h2 class="panel-title">DATA <strong>DETAIL ORDER</strong></h2>
                                                    </div>
                                                    <div class="panel-body bg-white">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="col-sm-4">
                                                                        <label class="col-sm-12 control-label">Nomor
                                                                            Order</label>
                                                                        </div>
                                                                        <div class="col-sm-7 prepend-icon">
                                                                            <h2 class="alert bg-primary m-0">{{($order != null)?$order->nomor_order: "-"}}</h2>
                                                                            <input type="hidden" name="nomor_order"
                                                                            value="{{($order != null)?$order->nomor_order: ""}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-4">
                                                                            <label class="col-sm-12 control-label">Nomor Surat
                                                                                Permintaan</label>
                                                                            </div>
                                                                            <div class="col-sm-7 prepend-icon">
                                                                                <input name="nomor_sp"
                                                                                value="{{($order != null) ? $order->nomor_sp : ''}}"
                                                                                type="text" class="form-control form-white"
                                                                                placeholder="Nomor Surat" disabled/>
                                                                                <i class="icon-user"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-sm-4">
                                                                                <label class="col-sm-12 control-label">Tanggal Surat
                                                                                    Permintaan</label>
                                                                                </div>
                                                                                <div class="col-sm-7 prepend-icon">
                                                                                    <input name="tanggal_sp"
                                                                                    value="{{($order != null) ? $order->tanggal_sp : ''}}"
                                                                                    type="text"
                                                                                    data-date-format="dd-mm-yyyy" disabled
                                                                                    class="b-datepicker form-control form-white"
                                                                                    placeholder="Tanggal" required>
                                                                                    <i class="icon-calendar"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-sm-4">
                                                                                    <label class="col-sm-12 control-label">File Surat
                                                                                        Permintaan</label>
                                                                                    </div>
                                                                                    <div class="col-sm-7">
                                                                                        <div class="file">
                                                                                            <div class="option-group">
                                                                                                @if(($order != null))
                                                                                                    <a href="{{url('upload/'.$order->file_sp)}}" target="_blank"
                                                                                                        style="color:deepskyblue"><i
                                                                                                        class="fa fa-paperclip"></i> {{$order->file_sp}}
                                                                                                    </a>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-11">
                                                                                        <hr/>
                                                                                        <div class="pull-right">
                                                                                            @if($order == null || $order->is_submitted == 0)
                                                                                                <button type="submit"
                                                                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                                                                data-style="zoom-in">Simpan &nbsp;<i
                                                                                                class="glyphicon glyphicon-floppy-saved"></i>
                                                                                            </button>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="panel border">
                                                                <div class="panel-header bg-primary">
                                                                    <h2 class="panel-title">DATA <strong>PEMINTA JASA</strong></h2>
                                                                </div>
                                                                <div class="panel-body bg-white">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="col-sm-11 prepend-icon">
                                                                                    <input name="nama_perusahaan" type="text"
                                                                                    class="form-control form-white"
                                                                                    placeholder="Nama Perusahaan"
                                                                                    required readonly
                                                                                    value="{{$perusahaan->nama_perusahaan}}">
                                                                                    <i class="fa fa-building-o"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-sm-11 prepend-icon">
                                                                                    <input name="nama_perusahaan" type="text"
                                                                                    class="form-control form-white"
                                                                                    placeholder="Nama Perusahaan"
                                                                                    required readonly
                                                                                    value="{{$perusahaan->kategori->nama_kategori}}">
                                                                                    <i class="fa fa-building-o"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-sm-11 prepend-icon">
                                                                                    <input name="nama_perusahaan" type="text"
                                                                                    class="form-control form-white"
                                                                                    placeholder="Nama Perusahaan"
                                                                                    required readonly
                                                                                    value="{{$perusahaan->city->city}}">
                                                                                    <i class="icon-pointer"></i>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-sm-11 prepend-icon">
                                                                                    <textarea name="alamat_perusahaan"
                                                                                    class="form-control form-white"
                                                                                    placeholder="Alamat" required
                                                                                    readonly>{{$perusahaan->alamat_perusahaan}}</textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="order">
                                                    @if($permohonan->count() > 0)
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Foto</th>
                                                                    <th>Nomor Permohonan</th>
                                                                    <th>Tanggal Permohonan</th>
                                                                    <th>Jenis Instalasi</th>
                                                                    <th>Jenis Pekerjaan</th>
                                                                    <th>Jenis Lingkup</th>
                                                                    <th>Instalasi</th>
                                                                    <th style="width: auto;">Aksi</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @if(is_array($permohonan) || is_object($permohonan))
                                                                    <?php $no = 1; ?>
                                                                    @foreach($permohonan as $item)
                                                                        <tr>
                                                                            <td>{{ $no }}</td>
                                                                            <td>
                                                                                <img style="margin: 0 auto;"
                                                                                src="{{($item->instalasi->foto_1 != null) ?  url('upload/instalasi/'.$item->instalasi->foto_1) : url('/assets/global/images/picture.png')}}"
                                                                                class="img-thumbnail" width="100"></td>
                                                                                <td>{{ $item->nomor_permohonan}}</td>
                                                                                <td>{{ date('d M Y',strtotime($item->tanggal_permohonan)) }}</td>
                                                                                <td style="white-space: nowrap;">{{ $item->tipeInstalasi->nama_instalasi }}</td>
                                                                                <td style="white-space: nowrap;">{{ $item->produk->produk_layanan }}</td>
                                                                                <td style="white-space: nowrap;">{{ $item->lingkup_pekerjaan->jenis_lingkup_pekerjaan }}
                                                                                    @if($item->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                                                        <br/>
                                                                                        @foreach($item->bayPermohonan as $bg)
                                                                                            - {{$bg->bayGardu->nama_bay}}<br/>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </td>
                                                                                <td style="white-space: nowrap;">{{ $item->instalasi->nama_instalasi}}</td>
                                                                                <td style="white-space: nowrap;">
                                                                                    <a href="{{url('/eksternal/create_permohonan/'.$item->id_orders.'/'.$item->id)}}"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                        class="fa fa-eye"></i></a>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php $no++; ?>
                                                                            @endforeach
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            @endif
                                                            @if($permohonan_distribusi->count() > 0)
                                                                <h2>PERMOHONAN INSTALASI DISTRIBUSI</h2>
                                                                <hr/>
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No</th>
                                                                            <th>Nomor Surat</th>
                                                                            <th>Tanggal Surat</th>
                                                                            <th>Periode</th>
                                                                            <th>Jenis Pekerjaan</th>
                                                                            <th style="width: 50px;">Aksi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $no = 1; ?>
                                                                        @foreach($permohonan_distribusi as $wa)
                                                                            <tr>
                                                                                <td>
                                                                                    {{$no}}
                                                                                </td>
                                                                                <td>
                                                                                    {{$wa->nomor_surat}}
                                                                                </td>
                                                                                <td>
                                                                                    {{date('d F Y',strtotime($wa->tanggal_surat))}}
                                                                                </td>
                                                                                <td>
                                                                                    {{$wa->periode}}
                                                                                </td>
                                                                                <td>
                                                                                    <?php $layanan = $wa->produk ?>
                                                                                    @foreach(@$layanan as $row)
                                                                                        <p>{{@$row->produk->produk_layanan}}</p>
                                                                                    @endforeach
                                                                                </td>
                                                                                <td>
                                                                                    <a href="{{url('/eksternal/view_surat_tugas/'.$wa->id)}}"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                                                        class="fa fa-eye"></i></a>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php $no++; ?>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @include('workflow_view')
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endsection


                                    @section('page_script')
                                        <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
                                        <!-- Tables Filtering, Sorting & Editing -->
                                        <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
                                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
                                        <!-- Buttons Loading State -->
                                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
                                        <!-- Select Inputs -->
                                        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
                                        <!-- >Bootstrap Date Picker -->
                                    @endsection
