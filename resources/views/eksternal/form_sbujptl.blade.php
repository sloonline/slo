@extends('../layout/layout_eksternal')

@section('page_css')
    <!-- BEGIN PAGE STYLE -->
    <link href="../assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet">
    <link href="../assets/global/plugins/input-text/style.min.css" rel="stylesheet">
    <!-- END PAGE STYLE -->
@endsection

@section('content')
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>SBUJPTL</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="#">Produk</a>
                    </li>
                    <li><a href="#">Input</a>
                    </li>
                    <li class="active">SBUJPTL</li>
                </ol>
            </div>
        </div>
        {{--Begin form input--}}
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header md-panel-controls">
                        <h3><i class="icon-bulb"></i> Form <strong>Input</strong></h3>
                    </div>
                    <div class="panel-content">
                        <p>Silahkan inputkan data pemohon dan data SBUJPTL yang hendak diajukan dalam permohonan
                            ini</p>
                        {!! Form::open(['url'=>'pembangkit','post','class'=> 'form-horizontal'  ]) !!}
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Nama Badan Usaha Jasa Pembangunan dan Pemasangan Instalasi Tenaga Listrik</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" required="required" name="nama_badan"
                                                   class="form-control form-white lastname"
                                                   placeholder="Nama Badan Usaha">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Alamat</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <textarea name="alamat" class="form-control form-white" placeholder="Alamat"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>No. Telepon</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Nomor Telepon">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>No. Faxn</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Nomor Fax">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Email</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="email" class="form-control form-white" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Kode Badan Usaha</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Kode Badan Usaha">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Masa Berlaku s.d</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="date" class="form-control form-white" placeholder="Masa Berlaku">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Klasifikasi BUJBTL</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Klasifikasi">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Kualifikasi BUJBTL</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Kualifikasi">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Nama Penanggung Jawab BUJBTL</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Nama Penanggung Jawab">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Nama Penanggung Jawab Teknik BUJBTL</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="append-icon">
                                            <input type="text" class="form-control form-white" placeholder="Nama Penanggung Jawab Teknik">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5">
                                        <div class="append-icon">
                                            <label>Upload SBUJPTL</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" name="file_foto_user" class="custom-file">
                                                <input type="text" class="form-control form-white" id="uploader"
                                                       placeholder="no file selected" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-9">
                                <button type="button" class="btn btn-warning">Reset</button>
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        {{--End form input--}}
        <div class="footer">
            <div class="copyright">
                <p class="pull-left sm-pull-reset">
                    <span>Copyright <span class="copyright">©</span> 2015 </span>
                    <span>THEMES LAB</span>.
                    <span>All rights reserved. </span>
                </p>
                <p class="pull-right sm-pull-reset">
                        <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of
                                use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                </p>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT -->
    </div>
    <!-- END MAIN CONTENT -->

    </section>
    <!-- BEGIN PRELOADER -->
    <div class="loader-overlay">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
@endsection

@section('page_script')
    <!-- BEGIN PAGE SCRIPT -->
    <script src="../assets/global/plugins/switchery/switchery.min.js"></script> <!-- IOS Switch -->
    <script src="../assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
    <script src="../assets/global/plugins/dropzone/dropzone.min.js"></script>  <!-- Upload Image & File in dropzone -->
    <script src="../assets/global/js/pages/form_icheck.js"></script>  <!-- Change Icheck Color - DEMO PURPOSE - OPTIONAL -->
    <!-- END PAGE SCRIPTS -->
@endsection