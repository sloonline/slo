@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
	<div class="header">
		<h2>{{($pemanfaatan_tt != null) ? "Ubah" : "Tambah"}} Instalasi <strong>Pemanfaatan Tegangan Tinggi</strong></h2>
		<div class="breadcrumb-wrapper">
			<ol class="breadcrumb">
				<li><a href="{{url('/internal')}}">Dashboard</a></li>
				<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">Data Instalasi</a></li>
				<li class="active">{{($pemanfaatan_tt != null) ? "Ubah" : "Tambah"}} Instalasi Pemanfaatan Tegangan Tinggi</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 portlets">
			<p class="m-t-10 m-b-20 f-16">Silahkan inputkan data instalasi pemanfaatan tegangan tinggi.</p>
			<div class="panel">
				{!! Form::open(array('url'=> 'eksternal/create_instalasi_pemanfaatan_tt', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
				<input type="hidden" name="id" value="{{($pemanfaatan_tt != null) ? $pemanfaatan_tt->id : 0}}"/>
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>{{($pemanfaatan_tt != null) ? "Ubah" : "Tambah"}} Instalasi</strong></h3>
				</div>
				<div class="panel-content">

					<div class="nav-tabs2" id="tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
								Data Umum Instalasi</a>
							</li>
							<li><a href="#kapasitas" data-toggle="tab"><i
								class="icon-speedometer"></i> Kapasitas</a>
							</li>
							<li><a href="#pemilik" data-toggle="tab"><i
								class="icon-user"></i> Pemilik Instalasi</a>
							</li>
							<li><a href="#lokasi" data-toggle="tab"><i
								class="icon-map"></i> Lokasi Instalasi</a>
							</li>
						</ul>
						<div class="tab-content bg-white">
							{{--start tab general--}}
							<div class="tab-pane active" id="general">
								<div class="panel-body bg-white">
									<!--Data umum instalasi tab -->
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Jenis Instalasi</label>
										</div>
										<div class="col-sm-9">
											<select class="form-control form-white" data-search="true" name="jenis_instalasi" id="jenis_instalasi">
												<option value="">--- Pilih ---</option>
												@foreach($jenis_instalasi as $item)
												<option value="{{$item->id}}" {{($pemanfaatan_tt != null && $pemanfaatan_tt->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Nama Instalasi</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->nama_instalasi : ""}}" name="nama_instalasi">
											<i class="fa fa-building"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">PHB TM</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="phb_tm" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->phb_tm : ""}}">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">PHB TR</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="phb_tr" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->phb_tr : ""}}">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Penyedia Tenaga Listrik</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="penyedia_tenaga_listrik" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->penyedia_tl : ""}}">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Tegangan Pengenal</label>
										</div>
										<div class="col-sm-9">
											<select class="form-control form-white" name="tegangan_pengenal">
												<option value="">--- Pilih ---</option>
												@foreach($tegangan_pengenal as $item)
												<option value="{{$item->id}}" {{($pemanfaatan_tt != null && $pemanfaatan_tt->tegangan_pengenal == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
												@endforeach
											</select>
										</div>
									</div>

								</div>
							</div>
							<div class="tab-pane" id="kapasitas">
								<div class="panel-body bg-white">
									<!-- kapasita instalasi -->
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Kapasitas Trafo Terpasang</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="kapasitas_trafo_terpasang" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->kapasitas_trafo : ""}}">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Daya Tersambung</label>
										</div>
										<div class="col-sm-9 prepend-icon">
											<input type="text" class="form-control form-white" name="daya_tersambung" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->daya_sambung : ""}}">
											<i class="fa fa-credit-card"></i>
										</div>
									</div>
									<!-- end kapasitas instalasi -->
								</div>
							</div> <!-- end kapasitas instalasi -->
							<div class="tab-pane" id="pemilik">
								<div class="panel-body bg-white">
									<!-- start pemilik instalasi -->
									<div class="form-group">
										<div class="col-sm-3">
											<label class="col-sm-12 control-label">Kepemilikan Instalasi</label>
										</div>
										<div class="col-sm-9">
											<select id="kepemilikan_instalasi" class="form-control form-white" data-search="true" name="jenis_pemilik_instalasi" id="jenis_jenis_instalasi">
												<option value="">--- Pilih ---</option>
												<option value="1">Milik sendiri</option>
												<option value="2">Lainnya</option>
											</select>
										</div>
									</div>
									<div id="panel_pemilik_instalasi" class="panel">
										<div class="panel-content">
											<h1><strong>Pemilik</strong> Instalasi</h1>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Nama Pemilik</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input id="nama_pemilik" name="nama_pemilik"
													placeholder="nama pemilik" type="text"
													class="form-control form-white autocomplete"
													{{--value="{{($pemilik != null) ? $pemilik->nama_pemilik : null}}">--}}
													value="">
													<i class="fa fa-building"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Alamat Instalasi</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
													class="form-control form-white"
													placeholder="Alamat Instalasi...">
													{{--{{($pemilik != null) ? $pemilik->alamat_pemilik : null}}--}}
												</textarea>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Provinsi</label>
											</div>
											<div class="col-sm-9">
												<select name="provinsi" class="form-control form-white"
												data-search="true"
												id="province">
												<option value="">--- Pilih ---</option>
												@foreach($province as $item)
												<option value="{{$item->id}}"
													{{--{{($pemilik != null && $pemilik->id_province == $item->id) ? "selected" : ""}}--}}
													>{{$item->province}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Kabupaten / Kota</label>
											</div>
											<div class="col-sm-9">
												<select name="kabupaten" id="city"
												class="form-control form-white">
												<option value="">--- Pilih ---</option>
												@foreach($city as $item)
												<option value="{{$item->id}}"
													class="{{$item->id_province}}"
													{{--{{($pemilik != null && $pemilik->id_city == $item->id) ? "selected" : ""}}--}}
													>{{$item->city}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Kode Pos</label>
											</div>
											<div class="col-sm-9 prepend-icon">
												<input id="kode_pos" name="kode_pos" type="text"
												class="form-control form-white"
												{{--value="{{($pemilik != null) ? $pemilik->kode_pos_pemilik : null}}">--}}
												value="">
												<i class="fa fa-credit-card"></i>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Telepon</label>
											</div>
											<div class="col-sm-9 prepend-icon">
												<input id="telepon" name="telepon" type="text"
												class="form-control form-white"
												{{--                                                   value="{{($pemilik != null) ? $pemilik->telepon_pemilik : null}}">--}}
												value="">
												<i class="fa fa-credit-card"></i>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">No Fax</label>
											</div>
											<div class="col-sm-9 prepend-icon">
												<input id="no_fax" name="no_fax" type="text" class="form-control form-white"
												{{--                                                   value="{{($pemilik != null) ? $pemilik->no_fax_pemilik : null}}">--}}
												value="">
												<i class="fa fa-credit-card"></i>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Email</label>
											</div>
											<div class="col-sm-9 prepend-icon">
												<input id="email_pemilik" name="email_pemilik" type="text"
												class="form-control form-white"
												{{--                                                   value="{{($pemilik != null) ? $pemilik->email_pemilik : null}}">--}}
												value="">
												<i class="fa fa-credit-card"></i>
											</div>
										</div>
									</div>
								</div>
								<div id="panel_ijin_usaha" class="panel">
									<div class="panel-content">
										<h1><strong>Ijin</strong> Usaha</h1>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
											</div>
											<div class="col-sm-9">
												<select name="jenis_ijin_usaha" class="form-control form-white">
													<option value="">--- Pilih ---</option>
													@foreach($jenis_ijin_usaha as $item)
													{{--                                                                <option value="{{$item->id}}" {{($pemilik != null && $pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>--}}
													<option value="{{$item->id}}">{{$item->nama_reference}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-3">
												<label class="col-sm-12 control-label">Penerbit Ijin
													Usaha</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="penerbit_ijin_usaha" type="text"
													class="form-control form-white"
													{{--                                                               value="{{($pemilik != null) ? $pemilik->penerbit_ijin_usaha : null}}">--}}
													value="">
													<i class="fa fa-credit-card"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">No Ijin Usaha</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="no_ijin_usaha" type="text"
													class="form-control form-white"
													{{--value="{{($pemilik != null) ? $pemilik->no_ijin_usaha : null}}">--}}
													value="">
													<i class="fa fa-credit-card"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">Masa Berlaku IU</label>
												</div>
												<div class="col-sm-9 prepend-icon">
													<input name="masa_berlaku_iu" type="text"
													class="form-control date-picker form-white"
													data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
													{{--                                                               value="{{($pemilik != null) ? $pemilik->masa_berlaku_iu : null}}">--}}
													value="">
													<i class="fa fa-credit-card"></i>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-3">
													<label class="col-sm-12 control-label">File Surat Ijin
														Usaha</label>
													</div>
													<div class="col-sm-9">
														<div class="file">
															<div class="option-group">
																<span class="file-button btn-primary">
																	{{--                                                            {{($pemilik != null && $pemilik->file_siup != null) ? "Ganti File" : "Choose File"}}--}}
																	Choose File
																</span>
																<input type="file" class="custom-file"
																name="file_surat_iu"
																onchange="document.getElementById('uploader').value = this.value;">
																<input type="text" class="form-control form-white"
																id="uploader"
																placeholder="no file selected"
																{{--                                                                       value="{{($pemilik != null && $pemilik->file_siup != null) ? $pemilik->file_siup : null}}">--}}
																value="">
															</div>
														</div>
													</div>
												</div>
												<?php
												//if($pemilik != null && $pemilik->file_siup != null){
												?>
												{{--<div class="form-group">--}}
													{{--<div class="col-sm-3">--}}
														{{--<label class="col-sm-12 control-label"></label>--}}
														{{--</div>--}}
														{{--<div class="col-sm-9">--}}
															{{--<a href="{{url('upload/'.$pemilik->file_siup)}}" class="btn btn-primary"><i--}}
																{{--class="fa fa-download"></i>View</a>--}}
																{{--</div>--}}
																{{--</div>--}}
																<?php
																//}
																?>
																<div class="form-group">
																	<div class="col-sm-3">
																		<label class="col-sm-12 control-label">Nama Kontrak</label>
																	</div>
																	<div class="col-sm-9 prepend-icon">
																		<input name="nama_kontrak" type="text"
																		class="form-control form-white"
																		{{--value="{{($pemilik != null) ? $pemilik->nama_kontrak : null}}">--}}
																		value="">
																		<i class="fa fa-credit-card"></i>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-sm-3">
																		<label class="col-sm-12 control-label">Nomor Kontrak</label>
																	</div>
																	<div class="col-sm-9 prepend-icon">
																		<input name="no_kontrak" type="text"
																		class="form-control form-white"
																		{{--value="{{($pemilik != null) ? $pemilik->no_kontrak : null}}">--}}
																		value="">
																		<i class="fa fa-credit-card"></i>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-sm-3">
																		<label class="col-sm-12 control-label">Tanggal Pengesahaan
																			Kontrak</label>
																		</div>
																		<div class="col-sm-9 prepend-icon">
																			<input name="tgl_pengesahan_kontrak" type="text"
																			class="form-control date-picker form-white"
																			data-format="yyyy-mm-dd"
																			data-lang="en" data-RTL="false"
																			{{--value="{{($pemilik != null) ? $pemilik->tgl_pengesahan_kontrak : null}}">--}}
																			value="">
																			<i class="fa fa-credit-card"></i>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-sm-3">
																			<label class="col-sm-12 control-label">Masa Berlaku
																				Kontrak</label>
																			</div>
																			<div class="col-sm-9 prepend-icon">
																				<input name="masa_berlaku_kontrak" type="text"
																				class="form-control date-picker form-white"
																				data-format="yyyy-mm-dd"
																				data-lang="en" data-RTL="false"
																				{{--value="{{($pemilik != null) ? $pemilik->masa_berlaku_kontrak : null}}">--}}
																				value="">
																				<i class="fa fa-credit-card"></i>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-sm-3">
																				<label class="col-sm-12 control-label">File Kontrak Sewa</label>
																			</div>
																			<div class="col-sm-9">
																				<div class="file">
																					<div class="option-group">
																						<span class="file-button btn-primary">Choose File</span>
																						<input type="file" class="custom-file"
																						name="file_kontrak_sewa"
																						onchange="document.getElementById('uploader2').value = this.value;">
																						<input type="text" class="form-control form-white"
																						id="uploader2"
																						placeholder="no file selected"
																						{{--value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">--}}
																						value="">
																					</div>
																				</div>
																			</div>
																		</div>
																		<?php
																		//if($pemilik != null && $pemilik->file_kontrak != null){
																		?>
																		{{--<div class="form-group">--}}
																			{{--<div class="col-sm-3">--}}
																				{{--<label class="col-sm-12 control-label"></label>--}}
																				{{--</div>--}}
																				{{--<div class="col-sm-9">--}}
																					{{--<a href="{{url('upload/'.$pemilik->file_kontrak)}}" class="btn btn-primary"><i--}}
																						{{--class="fa fa-download"></i>View</a>--}}
																						{{--</div>--}}
																						{{--</div>--}}
																						<?php
																						//}
																						?>
																						<div class="form-group">
																							<div class="col-sm-3">
																								<label class="col-sm-12 control-label">Nomor SPJBTL</label>
																							</div>
																							<div class="col-sm-9 prepend-icon">
																								<input name="no_spjbtl" type="text"
																								class="form-control form-white"
																								{{--value="{{($pemilik != null) ? $pemilik->no_spjbtl : null}}">--}}
																								value="">
																								<i class="fa fa-credit-card"></i>
																							</div>
																						</div>
																						<div class="form-group">
																							<div class="col-sm-3">
																								<label class="col-sm-12 control-label">Tanggal SPJBTL</label>
																							</div>
																							<div class="col-sm-9 prepend-icon">
																								<input name="tgl_spjbtl" type="text"
																								class="form-control date-picker form-white"
																								data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
																								{{--value="{{($pemilik != null) ? $pemilik->tgl_spjbtl : null}}">--}}
																								value="">
																								<i class="fa fa-credit-card"></i>
																							</div>
																						</div>
																						<div class="form-group">
																							<div class="col-sm-3">
																								<label class="col-sm-12 control-label">Masa Berlaku
																									SPJBTL</label>
																								</div>
																								<div class="col-sm-9 prepend-icon">
																									<input name="masa_berlaku_spjbtl" type="text"
																									class="form-control date-picker form-white"
																									data-format="yyyy-mm-dd"
																									data-lang="en" data-RTL="false"
																									{{--value="{{($pemilik != null) ? $pemilik->masa_berlaku_spjbtl : null}}">--}}
																									value="">
																									<i class="fa fa-credit-card"></i>
																								</div>
																							</div>
																							<div class="form-group">
																								<div class="col-sm-3">
																									<label class="col-sm-12 control-label">File SPJBTL</label>
																								</div>
																								<div class="col-sm-9">
																									<div class="file">
																										<div class="option-group">
																											<span class="file-button btn-primary">Choose File</span>
																											<input type="file" class="custom-file"
																											name="file_spjbtl"
																											onchange="document.getElementById('uploader3').value = this.value;">
																											<input type="text" class="form-control form-white"
																											id="uploader3"
																											placeholder="no file selected"
																											{{--                                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">--}}
																											value="">
																										</div>
																									</div>
																								</div>
																							</div>
																							<?php
																							//if($pemilik != null && $pemilik->file_spjbtl != null){
																							?>
																							{{--<div class="form-group">--}}
																								{{--<div class="col-sm-3">--}}
																									{{--<label class="col-sm-12 control-label"></label>--}}
																									{{--</div>--}}
																									{{--<div class="col-sm-9">--}}
																										{{--<a href="{{url('upload/'.$pemilik->file_spjbtl)}}" class="btn btn-primary"><i--}}
																											{{--class="fa fa-download"></i>View</a>--}}
																											{{--</div>--}}
																											{{--</div>--}}
																											<?php
																											//}
																											?>

																										</div>
																									</div>
																									<!-- end pemilik instalasi -->
																								</div>
																							</div>
																							<div class="tab-pane" id="lokasi">
																								<div class="panel-body bg-white">
																									<!-- start lokasi instalasi -->
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Longitude</label>
																										</div>
																										<div class="col-sm-9 prepend-icon">
																											<input type="text" class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->longitude_awal : ""}}">
																											<i class="fa fa-credit-card"></i>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Latitude</label>
																										</div>
																										<div class="col-sm-9 prepend-icon">
																											<input type="text" class="form-control form-white" name="latitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->latitude_awal : ""}}">
																											<i class="fa fa-credit-card"></i>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Longitude</label>
																										</div>
																										<div class="col-sm-9 prepend-icon">
																											<input type="text" class="form-control form-white" name="longitude_akhir" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->longitude_akhir : ""}}">
																											<i class="fa fa-credit-card"></i>
																										</div>
																									</div>
																									<div class="form-group">
																										<div class="col-sm-3">
																											<label class="col-sm-12 control-label">Latitude</label>
																										</div>
																										<div class="col-sm-9 prepend-icon">
																											<input type="text" class="form-control form-white" name="latitude_akhir" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->latitude_akhir : ""}}">
																											<i class="fa fa-credit-card"></i>
																										</div>
																									</div>
																									<!-- end lokasi instalasi -->
																								</div>
																							</div>
																						</div> <!-- end tab panel -->
																					</div>
																				</div>
																				<hr />
																				<div class="panel-footer clearfix bg-white">
																					<div class="pull-right">
																						<a href="{{url('/eksternal/view_instalasi_pemanfaatan_tt')}}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
																						<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
																					</div>
																				</div>
																				{!! Form::close() !!}
																			</div>
																		</div>
																	</div>
																	@endsection

																	@section('page_script')
																	<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
																	<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
																	<script>$("#city").chained("#province"); </script>
																	<script>

																	panelControl();
																	
																	$('#kepemilikan_instalasi').change(function(){
																		panelControl();
																	});

																	function panelControl(){
																		var val = ($('#kepemilikan_instalasi').val());
																		if(val == '1'){ //milik sendiri
																			$('#panel_pemilik_instalasi').hide();
																			$('#panel_ijin_usaha').show();
																		}else if (val == '2') { //lainnya
																			$('#panel_pemilik_instalasi').show();
																			$('#panel_ijin_usaha').show();
																		}else{ //belum dipilih
																			$('#panel_pemilik_instalasi').hide();
																			$('#panel_ijin_usaha').hide();
																		}
																	}
																	</script>
																	@endsection
