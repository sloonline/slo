@extends('../layout/layout_internal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/jquery-treegrid/css/jquery.treegrid.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12 portlets">
				<div class="panel">
					{!! Form::open(['url'=>'master/gardu_induk/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
					<div class="panel-header bg-primary">
						<h3><i class="fa fa-table"></i> Program <strong>Blablabla</strong></h3>
					</div>
					<div class="panel-content">
					    <div class="m-b-10 border-bottom">
                            <div class="btn-group">
                                <a href="#" data-toggle="modal" data-target="#input-area-kontrak" class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i> Tambah Area / Kontrak</a>
                            </div>
                        </div>
                        <table class="tree table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Area / Kontrak</th>
                                    <th>Lokasi / Penyulang / GI</th>
                                    <th>Periode</th>
                                    <th>Jenis Program</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <tr class="treegrid-1">
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>
                                        <a href="#"
                                        class="btn btn-sm btn-default btn-square btn-embossed"
                                        data-rel="tooltip" data-placement="top" data-original-title="Tambah Lokasi / Penyulang / GI"
                                        data-toggle="modal" data-target="#input-lokasi-penyulang-gi">
                                        <i class="fa fa-plus"></i>
                                        </a>
                                        <a href="#" class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus area / kontrak ini?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr class="treegrid-2 treegrid-parent-1">
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>
                                        <a href="#"
                                        class="btn btn-sm btn-default btn-square btn-embossed"
                                        data-rel="tooltip" data-placement="top" data-original-title="Tambah Jenis Lingkup Pekerjaan"
                                        data-toggle="modal" data-target="#input-lingkup-pekerjaan">
                                        <i class="fa fa-plus"></i>
                                        </a>
                                        <a href="#" class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus lokasi / penyulang / gi ini?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <tr class="treegrid-4 treegrid-parent-2">
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>{{ '******' }}</td>
                                    <td>
                                        <a href="#" class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip" data-placement="top" data-original-title="Delete" onclick="return confirm('Apakah anda yakin untuk menghapus lingkup pekerjaan ini?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            </tbody>
                        </table>
					</div>
					<hr />
					<div class="panel-footer clearfix bg-white">
						<div class="pull-right">
							<input type="hidden" name="id" value="">
							<a href="{{ url('master/gardu_induk') }}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
							<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="modal fade" id="input-area-kontrak" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    {!! Form::open(['url'=>'master/gardu_induk/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                        <h4 class="modal-title">Tambah <strong>Area / Kontrak</strong></h4>
                    </div>
                    <div class="modal-body">
                        <div class="p-t-20 m-b-20 p-l-40">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Program</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="gardu_induk" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area / Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control form-white" data-search="true" name="areakontrak">
                                        <option value="">Icikiwir</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Simpan</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="modal fade" id="input-lokasi-penyulang-gi" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    {!! Form::open(['url'=>'master/gardu_induk/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                        <h4 class="modal-title">Tambah <strong>Lokasi / Penyulang / GI</strong></h4>
                    </div>
                    <div class="modal-body">
                        <div class="p-t-20 m-b-20 p-l-40">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Program</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="program" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area / Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="areakontrak" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Lokasi / Penyulang / GI</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control form-white" data-search="true" name="lokasipenyulanggi">
                                        <option value="">Icikiwir</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Simpan</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="modal fade" id="input-lingkup-pekerjaan" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    {!! Form::open(['url'=>'master/gardu_induk/input','id'=>'form_register', 'class'=>'form-horizontal', 'role'=>'form']) !!}
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                        <h4 class="modal-title">Tambah <strong>Lokasi / Penyulang / GI</strong></h4>
                    </div>
                    <div class="modal-body">
                        <div class="p-t-20 m-b-20 p-l-40">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Program</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="program" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Area / Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="lokasipenyulanggi" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Lokasi / Penyulang / GI</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="append-icon">
                                        <input type="text" required="required" name="areakontrak" class="form-control form-white" readonly="">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Lingkup Pekerjaan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control form-white" data-search="true" name="lingkuppekerjaan">
                                        <option value="">Icikiwir</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary btn-embossed" data-dismiss="modal">Simpan</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@endsection

@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
<script src="{{ url('/') }}/assets/global/plugins/jquery-treegrid/js/jquery.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery-treegrid/js/jquery.treegrid.js"></script>

<script type="text/javascript">
  $('.tree').treegrid();
</script>
@endsection