<br>
<div class="form-group">
    <div class="col-sm-3">
        <h4 class="col-sm-12 control-label"><strong>Data Pemilik Instalasi</strong></h4>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Pemilik</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->pemilik->nama_pemilik : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Instalasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->jenis_instalasi->jenis_instalasi : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Alamat Instalasi</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <textarea readonly rows="3" class="form-control form-white" name="alamat_instalasi" placeholder="Alamat Instalasi">{{($pemanfaatan_tm != null)? $pemanfaatan_tm->alamat_instalasi : ""}}</textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Provinsi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->provinsi->province : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->kota->city : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->longitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="latitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->latitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="longitude_akhir" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->longitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="latitude_akhir" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->latitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Trafo Terpasang</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="kapasitas_trafo_terpasang" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->kapasitas_trafo : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Daya Tersambung</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="daya_tersambung" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->daya_sambung : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">PHB TM</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="phb_tm" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->phb_tm : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">PHB TR</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="phb_tr" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->phb_tr : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Penyedia Tenaga Listrik</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="penyedia_tenaga_listrik" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->penyedia_tl : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Tegangan Pengenal</label>
    </div>
    <div class="col-sm-9">\
    </div>
</div>