@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="page-content">
        <div class="header">
            <h2>Manajemen <strong>Surat Tugas</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li class="active">Surat Tugas</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <p class="m-t-10 m-b-20 f-16"></p>
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Data <strong>Surat Tugas</strong></h3>
                    </div>
                    <div class="panel-content pagination2 table-responsive">
                        <div class="m-b-20 border-bottom">
                            <div class="btn-group">
                                <a href="{{url('eksternal/create_surat_tugas')}}"
                                   class="btn btn-success btn-square btn-block btn-embossed"><i class="fa fa-plus"></i>
                                    Tambah Surat Tugas</a>
                            </div>
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Surat</th>
                                <th>Tanggal Surat</th>
                                <th>Periode</th>
                                <th style="width: 250px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            ?>
                            @foreach($surat as $row)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$row->nomor_surat}}</td>
                                    <td>{{date('d F Y',strtotime($row->tanggal_surat))}}</td>
                                    <td>{{$row->periode}}</td>
                                    <td>
                                        @if($row->isInUsed())
                                            <a href="{{url('/eksternal/view_surat_tugas/'.$row->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed" data-rel="tooltip"
                                               data-placement="top" data-original-title="Detail"><i
                                                        class="fa fa-eye"></i></a>
                                        @else
                                            <a href="{{url('/eksternal/create_surat_tugas/'.$row->id)}}"
                                               class="btn btn-sm btn-warning btn-square btn-embossed" data-rel="tooltip"
                                               data-placement="top" data-original-title="Edit"><i
                                                        class="fa fa-pencil-square-o"></i></a>
                                        @endif
                                        <a class="btn btn-sm btn-danger btn-square btn-embossed" data-rel="tooltip"
                                           data-placement="top" title="" data-original-title="Delete"
                                           onclick="return confirm('Apakah anda yakin untuk menghapus {{$row->nomor_surat}} ?')"
                                           href="{{url('eksternal/delete_surat_tugas/'.$row->id)}}"><i
                                                    class="fa fa-trash"></i></a>
                                        <a class="btn btn-sm btn-success btn-square btn-embossed" data-rel="tooltip"
                                           data-placement="top" title="" data-original-title="Add to Cart"
                                           onclick="return confirm('Apakah anda yakin untuk melakukan order {{$row->nomor_surat}} ?')"
                                           href="{{url('eksternal/addtocart_surat_tugas/'.$row->id)}}"><i
                                                    class="glyphicon glyphicon-shopping-cart"></i></a>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection