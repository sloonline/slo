@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>{{($distribusi != null) ? "Ubah" : "Tambah"}} Instalasi <strong>Distribusi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/view_instalasi_distribusi')}}">Data Instalasi</a></li>
                    <li><a href="{{url('/eksternal/view_instalasi_distribusi')}}">Distribusi</a></li>
                    <li class="active">{{($distribusi != null) ? "Ubah" : "Tambah"}} Instalasi Distribusi</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="panel">
                    {!! Form::open(array('name' => 'form_instalasi','url'=> 'eksternal/create_instalasi_distribusi', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    <input type="hidden" name="id" value="{{($distribusi != null) ? $distribusi->id : 0}}"/>
                    <input type="hidden" name="pemilik_instalasi_baru_id"
                           value="{{($distribusi != null) ? $distribusi->pemilik_instalasi_id : 0}}"/>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#kapasitas" data-toggle="tab"><i
                                            class="icon-speedometer"></i> KAPASITAS</a></li>
                            <li><a href="#pemilik" data-toggle="tab"><i class="icon-user"></i> PEMILIK</a></li>
                            <li><a href="#lokasi" data-toggle="tab"><i class="icon-map"></i> LOKASI</a></li>
                            <li><a href="#additional" data-toggle="tab"><i class="icon-info"></i> ADDITIONAL INFO</a></li>
                            <li><a href="#foto" data-toggle="tab"><i class="icon-picture"></i> FOTO</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status Instalasi *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true" required
                                                onchange="isiSuratPernyataan();checkform();"
                                                name="status_baru" id="status_baru">
                                            <option value="">--- Pilih ---</option>
                                            <option value="1" {{($distribusi != null && $distribusi->status_baru == 1) ? "selected='selected'":""}}>
                                                Instalasi Baru
                                            </option>
                                            <option value="0" {{($distribusi != null && $distribusi->status_baru == 0) ? "selected='selected'":""}}>
                                                Instalasi Lama
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none" id="div_status_slo">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Status SLO *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true"
                                                onchange="checkform();isiSuratPernyataan();changeLabel();"
                                                name="status_slo" id="status_slo">
                                            <option value="">--- Pilih ---</option>
                                            <option value="1" {{($distribusi != null && $distribusi->status_slo == 1) ? "selected='selected'":""}}>
                                                Perpanjangan SLO
                                            </option>
                                            <option value="0" {{($distribusi != null && $distribusi->status_slo == 0) ? "selected='selected'":""}}>
                                                Belum Ada SLO
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="form-group" id="suratpernyataan"> --}}
                                <div class="form-group" id="" style="display:none">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Surat Pernyataan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" name="file_sp"
                                                       id="file_sp"
                                                       class="custom-file max-file"
                                                       name="avatar" accept="image/*"
                                                       onchange="document.getElementById('file_sp_text').value = this.value;">
                                                <input type="text" id="file_sp_text"
                                                       class="form-control form-white"
                                                       placeholder="no file selected"
                                                       value="{{(@$distribusi->surat_pernyataan)}}" readonly="">
                                                <small class="text-muted block"><i
                                                            class="icon-paper-clip"></i> Max
                                                    file size: 1Mb
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Instalasi *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true" required
                                                onchange="checkform();"
                                                name="jenis_instalasi" id="jenis_instalasi">
                                            <option value="">--- Pilih ---</option>
                                            @foreach($jenis_instalasi as $item)
                                                <option jenis="{{$item->jenis_instalasi}}"
                                                        value="{{$item->id}}" {{($distribusi != null && $distribusi->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Instalasi *</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="text" class="form-control form-white" required
                                               onkeyup="checkform()"
                                               value="{{($distribusi != null)? $distribusi->nama_instalasi : ""}}"
                                               name="nama_instalasi">
                                        <i class="glyphicon glyphicon-flash"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Panjang Saluran</label>
                                    </div>
                                    <div class="col-sm-7 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="panjang_saluran"
                                               value="{{($distribusi != null)? $distribusi->panjang_kms : ""}}">
                                        <i class="fa fa-wrench"></i>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">kms</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jumlah Tiang</label>
                                    </div>
                                    <div class="col-sm-7 prepend-icon">
                                        <input type="text" class="form-control form-white" name="jumlah_tiang"
                                               onkeyup="checkform()"
                                               value="{{($distribusi != null)? $distribusi->jumlah_tiang : ""}}">
                                        <i class="fa fa-asterisk"></i>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">buah</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jumlah Gardu Distribusi</label>
                                    </div>
                                    <div class="col-sm-7 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="jumlah_gardu_distribusi"
                                               value="{{($distribusi != null)? $distribusi->jml_gardu : ""}}">
                                        <i class="fa fa-asterisk"></i>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">buah</label>
                                    </div>
                                </div>
                                <div class="form-group" id="phb">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jumlah Panel</label>
                                    </div>
                                    <div class="col-sm-7 prepend-icon">
                                        <input type="text" class="form-control form-white" name="jumlah_panel"
                                               onkeyup="checkform()"
                                               value="{{($distribusi != null)? $distribusi->jumlah_panel : ""}}">
                                        <i class="fa fa-asterisk"></i>
                                    </div>
                                </div>
                                <div class="form-group" id="teg_pengenal">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Sistem Jaringan Distribusi*</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true"
                                                onchange="checkform()" required
                                                name="sistem_jaringan_distribusi">
                                            <option value="">--- Pilih ---</option>
                                            @foreach(@$sistem_jaringan_distribusi as $item)
                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->sistem_jaringan == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="teg_pengenal">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tegangan Pengenal *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white" data-search="true"
                                                onchange="checkform()" required
                                                name="tegangan_pengenal">
                                            <option value="">--- Pilih ---</option>
                                            @foreach($tegangan_pengenal as $item)
                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->tegangan_pengenal == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="search_kontraktor">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Kontraktor (SBU) *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="kode_kontraktor"
                                                   value="{{@$distribusi->kode_kontraktor}}" id="kode_kontraktor">
                                            <input type="text"
                                                   value="{{(@$distribusi->kontraktor != null) ? @$distribusi->kode_kontraktor . " - " .@$distribusi->kontraktor : ""}}"
                                                   class="form-control form-white typeahead" autocomplete="off"
                                                   typeahead-editable="false" id="nama_kontraktor" name="nama_kontraktor"
                                                   spellcheck="false">
                                        </div>
                                    </div>
                                <div class="form-group" id="form_pernyataan">
                                    <div class="col-sm-3">
                                        <label id="label_pernyataan" class="col-sm-12 control-label">Surat Pernyataan <br/>(* diisi jika tidak memiliki kontraktor</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" class="custom-file"
                                                       {{(@$distribusi->kode_kontraktor != null || @$distribusi->file_pernyataan != null) ? "":"required"}}
                                                       name="file_pernyataan" id="file_pernyataan"
                                                       onchange="document.getElementById('uploader_pernyataan').value = this.value;checkform();">
                                                <input type="text" class="form-control form-white"
                                                       id="uploader_pernyataan"
                                                       placeholder="no file selected"
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    @if(@$distribusi->file_pernyataan != null)
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <a target="_blank"
                                                       href="{{url('upload/'.$distribusi->file_pernyataan)}}"
                                                       class="btn btn-primary"><i
                                                                class="fa fa-download"></i>View</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group" id="file_sbujk">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File SBUJPTL</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" class="custom-file"
                                                       name="file_sbujk"
                                                       onchange="document.getElementById('uploader_sbujk').value = this.value;checkform();">
                                                <input type="text" class="form-control form-white"
                                                       id="uploader_sbujk"
                                                       placeholder="no file selected"
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    @if(@$distribusi->file_sbujk != null)
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <a target="_blank"
                                                       href="{{url('upload/'.$distribusi->file_sbujk)}}"
                                                       class="btn btn-primary"><i
                                                                class="fa fa-download"></i>View</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group" id="file_iujk">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File IUJPTL</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" class="custom-file"
                                                       name="file_iujk" id="file_iujk"
                                                       onchange="document.getElementById('uploader_iujk').value = this.value;checkform();">
                                                <input type="text" class="form-control form-white"
                                                       id="uploader_iujk"
                                                       placeholder="no file selected"
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    @if(@$distribusi->file_iujk!= null)
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="col-sm-3">
                                                    <a target="_blank"
                                                       href="{{url('upload/'.$distribusi->file_iujk)}}"
                                                       class="btn btn-primary"><i
                                                                class="fa fa-download"></i>View</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group" id="file_sld">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Single Line Diagram</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" class="custom-file"
                                                       name="file_sld" id="file_sld"
                                                       onchange="document.getElementById('uploader_sld').value = this.value;checkform();">
                                                <input type="text" class="form-control form-white"
                                                       id="uploader_sld"
                                                       placeholder="no file selected"
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    @if(@$distribusi->file_sld!= null)
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-3">
                                                <a target="_blank"
                                                   href="{{url('upload/'.$distribusi->file_sld)}}"
                                                   class="btn btn-primary"><i
                                                            class="fa fa-download"></i>View</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kapasitas">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kapasitas Gardu Distribusi</label>
                                    </div>
                                    <div class="col-sm-6 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="kapasitas_gardu_distribusi"
                                               value="{{($distribusi != null)? $distribusi->kapasitas_gardu : ""}}">
                                        <i class="fa fa-tasks"></i>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">kVA</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kapasitas Arus Hubung Singkat</label>
                                    </div>
                                    <div class="col-sm-6 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="kapasitas_arus_hubung_singkat"
                                               value="{{($distribusi != null)? $distribusi->kapasitas_arus_hubung_singkat : ""}}">
                                        <i class="fa fa-tasks"></i>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-sm-12 control-label">kA</label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pemilik">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kepemilikan Instalasi *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control" data-search="true" required
                                                onchange="checkform()"
                                                name="tipe_pemilik" id="jenis_pemilik_instalasi">
                                            {{--<option value="">--- Pilih ---</option>--}}
                                            <option value="{{MILIK_SENDIRI}}" {{($distribusi != null && $distribusi->tipe_pemilik == MILIK_SENDIRI) ? "selected" : ""}}>
                                                Milik sendiri
                                            </option>
                                            <option value="{{TERDAFTAR}}" {{($distribusi != null && $distribusi->tipe_pemilik == TERDAFTAR) ? "selected" : ""}}>
                                                Milik perusahaan lain yang terdaftar
                                            </option>
                                            {{-- <option value="{{BELUM_TERDAFTAR}}" {{($distribusi != null && $distribusi->tipe_pemilik == BELUM_TERDAFTAR) ? "selected" : ""}}>
                                                Milik perusahaan lain yang belum terdaftar
                                            </option> --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::hidden('pemilik_instalasi_id', @$pemilik_instalasi->id) !!}
                                        <div class="panel hidden border" id="pemilik_instalasi_baru">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">PEMILIK <strong>INSTALASI</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="nama_pemilik" name="nama_pemilik"
                                                               placeholder="Nama Pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik != null) ? $distribusi->pemilik->nama_pemilik : null}}">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                    <textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
                                                              class="form-control form-white"
                                                              placeholder="Alamat Instalasi...">{{($distribusi != null && $distribusi->pemilik != null) ? trim($distribusi->pemilik->alamat_pemilik) : null}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select name="provinsi_pemilik" class="form-control form-white"
                                                                data-search="true"
                                                                id="province_pemilik">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($province as $item)
                                                                <option value="{{$item->id}}"
                                                                        {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->id_province == $item->id) ? "selected" : ""}}
                                                                >{{$item->province}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select name="kabupaten_pemilik" id="city_pemilik"
                                                                class="form-control form-white" data-search="true">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($city as $item)
                                                                <option value="{{$item->id}}"
                                                                        class="{{$item->id_province}}"
                                                                        {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->id_city == $item->id) ? "selected" : ""}}
                                                                >{{$item->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="kode_pos" name="kode_pos" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->kode_pos_pemilik : null}}">
                                                        <i class="fa fa-map-marker"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Telepon</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="telepon" name="telepon" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->telepon_pemilik : null}}">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Fax</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_fax" name="no_fax" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_fax_pemilik : null}}">
                                                        <i class="fa fa-fax"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Email</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="email_pemilik" name="email_pemilik" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->email_pemilik : null}}">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel hidden border" id="ijin_usaha_baru">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">IJIN <strong>USAHA</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select id="jenis_ijin_usaha" name="jenis_ijin_usaha"
                                                                class="form-control form-white">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($jenis_ijin_usaha as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->pemilik && $distribusi->pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Penerbit Ijin
                                                            Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="penerbit_ijin_usaha" name="penerbit_ijin_usaha"
                                                               type="text" class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->penerbit_ijin_usaha : null}}">
                                                        <i class="fa fa-list-alt"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_ijin_usaha" name="no_ijin_usaha" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_ijin_usaha : null}}">
                                                        <i class="fa fa-th-list"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="masa_berlaku_iu" name="masa_berlaku_iu" type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy" data-lang="en"
                                                               data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_iu)) : null}}">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File Surat Ijin
                                                            Usaha</label>
                                                    </div>
                                                    @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_siup != null)
                                                        <div class="col-sm-2">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.$distribusi->pemilik->file_siup)}}"
                                                               class="btn btn-primary"><i
                                                                        class="fa fa-download"></i>View</a>
                                                        </div>
                                                    @endif
                                                    <div class="col-sm-7">
                                                        <div class="file">
                                                            <div class="option-group">
                                                                <span class="file-button btn-primary">
                                                                    {{--                                                            {{($pemilik != null && $pemilik->file_siup != null) ? "Ganti File" : "Choose File"}}--}}
                                                                    Choose File
                                                                </span>
                                                                <input type="file" class="custom-file"
                                                                       name="file_surat_iu"
                                                                       onchange="document.getElementById('uploader').value = this.value;">
                                                                <input type="text" class="form-control form-white"
                                                                       id="uploader"
                                                                       placeholder="no file selected"
                                                                       {{--                                                                       value="{{($pemilik != null && $pemilik->file_siup != null) ? $pemilik->file_siup : null}}">--}}
                                                                       value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Sewa (Jika Ada)</label>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <input name="has_sewa" type="radio" id="sewa_true"
                                                               class="form-control"
                                                               {{($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->nama_kontrak != "" ) ? "checked" : ""}}
                                                               value="1"> Ya
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <input name="has_sewa"
                                                               {{ ($distribusi == null) ? "checked": (($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->nama_kontrak == "") ? "checked" : "")}} type="radio"
                                                               id="sewa_false" class="form-control" value="0"> Tidak
                                                    </div>
                                                </div>
                                                <div id="kontrak">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Nama Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="nama_kontrak" name="nama_kontrak" type="text"
                                                                   class="form-control form-white"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->nama_kontrak : null}}">
                                                            <i class="fa fa-list-alt"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="no_kontrak" name="no_kontrak" type="text"
                                                                   class="form-control form-white"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_kontrak : null}}">
                                                            <i class="fa fa-th-list"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Tanggal Pengesahaan
                                                                Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="tgl_pengesahan_kontrak"
                                                                   name="tgl_pengesahan_kontrak"
                                                                   type="text"
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en" data-RTL="false"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->tgl_pengesahan_kontrak)) : null}}">
                                                            <i class="fa fa-th-list"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">Masa Berlaku
                                                                Kontrak</label>
                                                        </div>
                                                        <div class="col-sm-9 prepend-icon">
                                                            <input id="masa_berlaku_kontrak" name="masa_berlaku_kontrak"
                                                                   type="text"
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en" data-RTL="false"
                                                                   value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_kontrak)) : null}}">
                                                            <i class="fa fa-calendar-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-12 control-label">File Kontrak
                                                                Sewa</label>
                                                        </div>
                                                        @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_kontrak != null)
                                                            <div class="col-sm-2">
                                                                <a target="_blank"
                                                                   href="{{url('upload/'.$distribusi->pemilik->file_kontrak)}}"
                                                                   class="btn btn-primary"><i
                                                                            class="fa fa-download"></i>View</a>
                                                            </div>
                                                        @endif
                                                        <div class="col-sm-7">
                                                            <div class="file">
                                                                <div class="option-group">
                                                                    <span class="file-button btn-primary">Choose File</span>
                                                                    <input type="file" class="custom-file"
                                                                           name="file_kontrak_sewa"
                                                                           onchange="document.getElementById('uploader2').value = this.value;">
                                                                    <input type="text" class="form-control form-white"
                                                                           id="uploader2"
                                                                           placeholder="no file selected"
                                                                           {{--value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">--}}
                                                                           value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label class="col-sm-12 control-label">*) SPJBTL di-isikan
                                                            sesuai dengan lokasi instalasi</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="no_spjbtl" name="no_spjbtl" type="text"
                                                               class="form-control form-white"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? $distribusi->pemilik->no_spjbtl : null}}">
                                                        <i class="fa fa-th-list"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="tgl_spjbtl" name="tgl_spjbtl" type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy" data-lang="en"
                                                               data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->tgl_spjbtl)) : null}}">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Masa Berlaku
                                                            SPJBTL</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                        <input id="masa_berlaku_spjbtl" name="masa_berlaku_spjbtl"
                                                               type="text"
                                                               class="form-control b-datepicker form-white"
                                                               data-date-format="dd-mm-yyyy"
                                                               data-lang="en" data-RTL="false"
                                                               value="{{($distribusi != null && $distribusi->pemilik) ? date('d-m-Y',strtotime($distribusi->pemilik->masa_berlaku_spjbtl)) : null}}">
                                                        <i class="fa fa-calendar-o"></i>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                                    </div>
                                                    @if($distribusi != null && $distribusi->pemilik != null && $distribusi->pemilik->file_spjbtl != null)
                                                        <div class="col-sm-2">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.$distribusi->pemilik->file_spjbtl)}}"
                                                               class="btn btn-primary"><i
                                                                        class="fa fa-download"></i>View</a>
                                                        </div>
                                                    @endif
                                                    <div class="col-sm-7">
                                                        <div class="file">
                                                            <div class="option-group">
                                                                <span class="file-button btn-primary">Choose File</span>
                                                                <input type="file" class="custom-file"
                                                                       name="file_spjbtl"
                                                                       onchange="document.getElementById('uploader3').value = this.value;">
                                                                <input type="text" class="form-control form-white"
                                                                       id="uploader3"
                                                                       placeholder="no file selected"
                                                                       {{--                                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">--}}
                                                                       value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel hidden border" id="pemilik_instalasi">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">PEMILIK <strong>INSTALASI</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Pemilik Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select class="form-control form-white" data-search="true"
                                                                name="pemilik_instalasi_lain"
                                                                id="pemilik_instalasi_lain">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($pemilik as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->pemilik_instalasi_id == $item->id) ? "selected" : ""}}>{{$item->nama_pemilik}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lokasi">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong> (AWAL)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi *</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                                        <textarea rows="3"
                                                                                  class="form-control form-white"
                                                                                  required onkeyup="checkform()"
                                                                                  name="alamat_instalasi"
                                                                                  placeholder="Alamat Instalasi">{{($distribusi != null)? $distribusi->alamat_instalasi : ""}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi *</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select class="form-control form-white" data-search="true"
                                                                required
                                                                onchange="checkform()"
                                                                name="provinsi" id="province_instalasi">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($province as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->id_provinsi == $item->id) ? "selected" : ""}}>{{$item->province}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota *</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select id="city_instalasi" name="kabupaten"
                                                                class="form-control form-white" required
                                                                onchange="checkform()"
                                                                data-search="true">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($city as $item)
                                                                <option value="{{$item->id}}"
                                                                        class="{{$item->id_province}}" {{($distribusi != null && $distribusi->id_kota == $item->id) ? "selected" : ""}}>{{$item->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude (menggunakan format desimal) *</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-6 col-md-6 col-lg-6">
                                                            <input type="text" required
                                                                   placeholder="Latitude awal (decimal, ex : 6.671271)"
                                                                   class="form-control form-white"
                                                                   name="latitude_awal" onkeyup="checkform()"
                                                                   value="{{($distribusi != null)? $distribusi->latitude_awal : ""}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude (menggunakan format desimal) *</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-6 col-md-6 col-lg-6">
                                                            <input type="text" required
                                                                   placeholder="Longitude awal (decimal, ex : -7.671271)"
                                                                   class="form-control form-white"
                                                                   name="longitude_awal" onkeyup="checkform()"
                                                                   value="{{($distribusi != null)? $distribusi->longitude_awal : ""}}"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="panel border">
                                            <div class="panel-header bg-primary">
                                                <h2 class="panel-title">LOKASI <strong> (AKHIR)</strong></h2>
                                            </div>
                                            <div class="panel-body bg-white">
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                                    </div>
                                                    <div class="col-sm-9 prepend-icon">
                                                                        <textarea rows="3"
                                                                                  class="form-control form-white"
                                                                                  onkeyup="checkform()"
                                                                                  name="alamat_akhir_instalasi"
                                                                                  placeholder="Alamat Instalasi">{{($distribusi != null)? $distribusi->alamat_akhir_instalasi : ""}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Provinsi</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select class="form-control form-white" data-search="true"
                                                                onchange="checkform()"
                                                                name="provinsi_akhir" id="province_instalasi_akhir">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($province as $item)
                                                                <option value="{{$item->id}}" {{($distribusi != null && $distribusi->id_provinsi_akhir == $item->id) ? "selected" : ""}}>{{$item->province}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <select id="city_instalasi_akhir" name="kabupaten_akhir"
                                                                class="form-control form-white"
                                                                onchange="checkform()"
                                                                data-search="true">
                                                            <option value="">--- Pilih ---</option>
                                                            @foreach($city as $item)
                                                                <option value="{{$item->id}}"
                                                                        class="{{$item->id_province}}" {{($distribusi != null && $distribusi->id_kota_akhir == $item->id) ? "selected" : ""}}>{{$item->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Latitude (menggunakan format desimal)</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-6 col-md-6 col-lg-6">
                                                            <input type="text"
                                                                   placeholder="example. -7.2323"
                                                                   class="form-control form-white"
                                                                   name="latitude_akhir"
                                                                   onkeyup="checkform()"
                                                                   value="{{($distribusi != null)? $distribusi->latitude_akhir : ""}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-3">
                                                        <label class="col-sm-12 control-label">Longitude (menggunakan format desimal)</label>
                                                    </div>
                                                    <div class="col-sm-9 col-md-8 col-lg-7 form-inline">
                                                        <div class="input-group col-sm-6 col-md-6 col-lg-6">
                                                            <input type="text"
                                                                   placeholder="example. 102.39283"
                                                                   class="form-control form-white"
                                                                   name="longitude_akhir"
                                                                   onkeyup="checkform()"
                                                                   value="{{($distribusi != null)? $distribusi->longitude_akhir : ""}}"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="additional">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Program</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control form-white"
                                                onchange="checkform();"
                                                name="id_program">
                                            <option value="">--- Pilih Program---</option>
                                            @foreach($jenis_program as $row)
                                                <option value="{{@$row->id}}" {{(@$row->id == @$distribusi->id_program)  ? "selected='selected'" : ""}}>{{@$row->jenis_program}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kontrak</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input type="text" class="form-control form-white"
                                               onkeyup="checkform()"
                                               value="{{($distribusi != null)? $distribusi->kontrak : ""}}"
                                               name="kontrak">
                                        <i class="glyphicon glyphicon-flash"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Lokasi</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="lokasi">{{($distribusi != null)? $distribusi->lokasi : ""}}</textarea>
                                        {{--<i class="glyphicon glyphicon-flash"></i>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Area/APD</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea class="form-control form-white"
                                               onkeyup="checkform()"
                                               name="area_apd">{{($distribusi != null)? $distribusi->area_apd : ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penyulang</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea class="form-control form-white"
                                                  onkeyup="checkform()"
                                                  name="penyulang">{{($distribusi != null)? $distribusi->penyulang : ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">GI/GH</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea class="form-control form-white"
                                                  onkeyup="checkform()"
                                                  name="gi_gh">{{($distribusi != null)? $distribusi->gi_gh : ""}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Keterangan</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <textarea class="form-control form-white"
                                                  onkeyup="checkform()"
                                                  name="keterangan">{{($distribusi != null)? $distribusi->keterangan : ""}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="foto">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Foto Papan Nama Lokasi Instalasi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" name="file_foto_1"
                                                       id="file_foto_1"
                                                       class="custom-file max-file" id="file_foto_1"
                                                       name="avatar" id="avatar" accept="image/*"
                                                       onchange="document.getElementById('file_foto_1_text').value = this.value;">
                                                <input type="text" id="file_foto_1_text"
                                                       class="form-control form-white"
                                                       placeholder="no file selected" readonly="">
                                                <small class="text-muted block"><i
                                                            class="icon-paper-clip"></i> Max
                                                    file size: 1Mb (jpg/png/gif)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Foto Instalasi Secara Umum</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" name="file_foto_2"
                                                       id="file_foto_2"
                                                       class="custom-file max-file" id="file_foto_2"
                                                       name="avatar" id="avatar" accept="image/*"
                                                       onchange="document.getElementById('file_foto_2_text').value = this.value;">
                                                <input type="text" id="file_foto_2_text"
                                                       class="form-control form-white"
                                                       placeholder="no file selected" readonly="">
                                                <small class="text-muted block"><i
                                                            class="icon-paper-clip"></i> Max
                                                    file size: 1Mb (jpg/png/gif)
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($distribusi != null)
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="panel border">
                                                <div class="panel-header bg-primary">
                                                    <h2 class="panel-title">FOTO <strong>INSTALASI</strong></h2>
                                                </div>
                                                <div class="panel-body bg-white">
                                                    <div class="form-group">
                                                        @if($distribusi->foto_1 != null)
                                                            <div class="col-sm-3">
                                                                <a target="_blank"
                                                                   href="{{url('upload/instalasi/'.$distribusi->foto_1)}}">
                                                                    <img style="margin: 0 auto;"
                                                                         src="{{url('upload/instalasi/'.$distribusi->foto_1)}}"
                                                                         class="img-thumbnail" width="100">
                                                                </a>
                                                            </div>
                                                        @endif
                                                        @if($distribusi->foto_2 != null)
                                                            <div class="col-sm-3">
                                                                <a target="_blank"
                                                                   href="{{url('upload/instalasi/'.$distribusi->foto_2)}}">
                                                                    <img style="margin: 0 auto;"
                                                                         src="{{url('upload/instalasi/'.$distribusi->foto_2)}}"
                                                                         class="img-thumbnail" width="100">
                                                                </a>
                                                            </div>
                                                        @endif
                                                        @if($distribusi->foto_3 != null)
                                                            <div class="col-sm-3">
                                                                <a target="_blank"
                                                                   href="{{url('upload/instalasi/'.$distribusi->foto_3)}}">
                                                                    <img style="margin: 0 auto;"
                                                                         src="{{url('upload/instalasi/'.$distribusi->foto_3)}}"
                                                                         class="img-thumbnail" width="100">
                                                                </a>
                                                            </div>
                                                        @endif
                                                        @if($distribusi->foto_4 != null)
                                                            <div class="col-sm-3">
                                                                <a target="_blank"
                                                                   href="{{url('upload/instalasi/'.$distribusi->foto_4)}}">
                                                                    <img style="margin: 0 auto;"
                                                                         src="{{url('upload/instalasi/'.$distribusi->foto_4)}}"
                                                                         class="img-thumbnail" width="100">
                                                                </a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr class="p-t-0 m-t-0">
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="{{url('/eksternal/view_instalasi_distribusi')}}"
                               class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" id="submit_form"
                                    class="btn btn-success btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i
                                        class="glyphicon glyphicon-floppy-disk"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $("#suratpernyataan").hide();
                isiSuratPernyataan();
                changeLabel();
                $("#city_instalasi").chained("#province_instalasi");
                $("#city_instalasi_akhir").chained("#province_instalasi_akhir");
                $("#city_pemilik").chained("#province_pemilik");

                panelControl();
                @if($distribusi != null)
                     setPemilik();
                @if($distribusi->tipe_pemilik == TERDAFTAR)
                    $("#pemilik_instalasi_lain").val('{{$distribusi->pemilik_instalasi_id}}');
                @endif
            @endif
        $('#jenis_pemilik_instalasi').change(function () {
                    panelControl();
                });
                function panelControl() {
                    var val = ($('#jenis_pemilik_instalasi').val());
                    if (val == '{{MILIK_SENDIRI}}') { //milik sendiri
                        $('#panel_pemilik_instalasi').hide();
                        $('#panel_ijin_usaha').show();
                    } else if (val == '{{TERDAFTAR}}') { //lainnya
                        $('#panel_pemilik_instalasi').show();
                        $('#panel_ijin_usaha').show();
                    } else { //belum dipilih
                        $('#panel_pemilik_instalasi').hide();
                        $('#panel_ijin_usaha').hide();
                    }
                }


                $('#jenis_pemilik_instalasi').change(function () {
                    setPemilik();
                });

                function setPemilik() {

                    if ($("#jenis_pemilik_instalasi").val() == '{{MILIK_SENDIRI}}') {
                        $("#pemilik_instalasi_baru").addClass('hidden');
                        $("#ijin_usaha_baru").addClass('hidden');
                        $("#pemilik_instalasi").addClass('hidden');
                        $("#nama_pemilik").val('{{@$pemilik_instalasi->nama_pemilik}}');
                        $("#alamat_pemilik").val('{{str_replace(["\r\n", "\r", "\n"],' ',@$pemilik_instalasi->alamat_pemilik)}}');
                        $("#province").val('{{@$pemilik_instalasi->id_province}}');
                        $('#province').change();
                        $("#city").val('{{@$pemilik_instalasi->id_city}}');
                        $('#city').change();
                        $("#kode_pos").val('{{@$pemilik_instalasi->kode_pos_pemilik}}');
                        $("#telepon").val('{{@$pemilik_instalasi->telepon_pemilik}}');
                        $("#no_fax").val('{{@$pemilik_instalasi->no_fax_pemilik}}');
                        $("#email_pemilik").val('{{@$pemilik_instalasi->email_pemilik}}');
                        $("#jenis_ijin_usaha").val('{{@$pemilik_instalasi->jenis_ijin_usaha}}');
                        $('#jenis_ijin_usaha').change();
                        $("#penerbit_ijin_usaha").val('{{@$pemilik_instalasi->penerbit_ijin_usaha}}');
                        $("#no_ijin_usaha").val('{{@$pemilik_instalasi->no_ijin_usaha}}');
                        $("#masa_berlaku_iu").val('{{@$pemilik_instalasi->masa_berlaku_iu}}');
                        $("#uploader").val('{{@$pemilik_instalasi->file_siup}}');
                        $("#nama_kontrak").val('{{@$pemilik_instalasi->nama_kontrak}}');
                        $("#no_kontrak").val('{{@$pemilik_instalasi->no_kontrak}}');
                        $("#tgl_pengesahan_kontrak").val('{{@$pemilik_instalasi->tgl_pengesahan_kontrak}}');
                        $("#masa_berlaku_kontrak").val('{{@$pemilik_instalasi->masa_berlaku_kontrak}}');
                        $("#uploader2").val('{{@$pemilik_instalasi->file_kontrak}}');
                        $("#no_spjbtl").val('{{@$pemilik_instalasi->no_spjbtl}}');
                        $("#tgl_spjbtl").val('{{@$pemilik_instalasi->tgl_spjbtl}}');
                        $("#masa_berlaku_spjbtl").val('{{@$pemilik_instalasi->masa_berlaku_spjbtl}}');
                        $("#uploader3").val('{{@$pemilik_instalasi->file_spjbtl}}');
                        $("#pemilik_instalasi_lain").val('');
                    }
                    else if ($("#jenis_pemilik_instalasi").val() == '{{TERDAFTAR}}') {
                        $("#pemilik_instalasi_baru").addClass('hidden');
                        $("#ijin_usaha_baru").addClass('hidden');
                        $("#pemilik_instalasi").removeClass('hidden');
                        $("#pemilik_instalasi_lain").val('');
                    }
                    else {
                        $("#pemilik_instalasi_baru").removeClass('hidden');
                        $("#ijin_usaha_baru").removeClass('hidden');
                        $("#pemilik_instalasi").addClass('hidden');
                        @if($distribusi == null || $distribusi->tipe_pemilik != BELUM_TERDAFTAR)
                            $("#nama_pemilik").val('');
                        $("#alamat_pemilik").val('');
                        $("#province").val('');
                        $('#province').change();
                        $("#city").val('');
                        $('#city').change();
                        $("#kode_pos").val('');
                        $("#telepon").val('');
                        $("#no_fax").val('');
                        $("#email_pemilik").val('');
                        $("#jenis_ijin_usaha").val('');
                        $('#jenis_ijin_usaha').change();
                        $("#penerbit_ijin_usaha").val('');
                        $("#no_ijin_usaha").val('');
                        $("#masa_berlaku_iu").val('');
                        $("#uploader").val('');
                        $("#nama_kontrak").val('');
                        $("#no_kontrak").val('');
                        $("#tgl_pengesahan_kontrak").val('');
                        $("#masa_berlaku_kontrak").val('');
                        $("#uploader2").val('');
                        $("#no_spjbtl").val('');
                        $("#tgl_spjbtl").val('');
                        $("#masa_berlaku_spjbtl").val('');
                        $("#uploader3").val('');
                        $("#pemilik_instalasi_lain").val('');
                        @endif
                    }


                }
                function checkform() {
                    var f = document.forms["form_instalasi"].elements;
                    var cansubmit = true;
                    for (var i = 0; i < f.length; i++) {
                        if ("value" in f[i] && f[i].value.length == 0 && f[i].getAttribute("required") != null) {
                            cansubmit = false;
                        }
                    }
                    document.getElementById('submit_form').disabled = !cansubmit;
                }
                window.onload = checkform;
                statusSewa();

                // For oncheck callback
                $('input[name="has_sewa"]').on('ifChecked', function () {
                    statusSewa();
                });

                $('input[name="has_sewa"]').on('ifUnchecked', function () {
                    statusSewa();
                });

                function statusSewa() {
                    if ($("#sewa_true").is(':checked')) {
                        $("#kontrak").show('medium');
                    } else {
                        $("#kontrak").hide('medium');
                    }
                }


                function isiSuratPernyataan() {

                    var jenis = $("#status_baru").find('option:selected').attr("value");
                    $("#suratpernyataan").hide();
                    $("#default_kontraktor").hide();
                    
                    if (jenis == "0") {
                        $("#suratpernyataan").show();
                        $("#form_pernyataan").show();
                        $("#file_sbujk").hide();
                        $("#file_iujk").hide();
                        $("#file_sld").hide();
                        $("#search_kontraktor").hide();
                        $("#div_status_slo").show();
                        $("#status_slo").attr("required", true);
                    } else {
                        $("#file_sbujk").show();
                        $("#form_pernyataan").hide();
                        $("#file_iujk").show();
                        $("#file_sld").show();
                        $("#suratpernyataan").hide();
                        $("#search_kontraktor").show();
                        $("#div_status_slo").hide();
                        $("#status_slo").removeAttr('required');
                    }
                }

                function changeLabel(){
                    var status = $("#status_slo").find('option:selected').attr("value");
                    if (status == "1") { //perpanjangan SLO
                        $("#label_pernyataan").text("File SLO Lama");
                    }else{ 
                        $("#label_pernyataan").text("Surat Pernyataan (* diisi jika tidak memiliki kontraktor");
                    }
                }

                // Initializing the typeahead
                var namaKontraktor = new Array();
                var kodeKontraktor = new Object();
                var idSbuElement = "nama_kontraktor";
                var textInputSbu = document.getElementById(idSbuElement);

                // Init a timeout variable to be used below
                var timeout = null;

                // Listen for keystroke events
                textInputSbu.onkeypress = function (e) {
                    clearTimeout(timeout);
                    namaKontraktor = new Array();
                    kodeKontraktor = new Object();

                    // Make a new timeout set to go off in 800ms
                    timeout = setTimeout(function () {
                        var result;
                        namaKontraktor = new Array();
                        kodeKontraktor = new Object();
                        var query = textInputSbu.value;
                        if (query.length >= 4) {
                            $('#ajax_loader').show();
                            setTimeout(function () {
                                $.ajax({
                                    url: "{{url('/')}}/kontraktor/search/" + query,
                                    type: 'GET',
                                    dataType: 'json',
                                    async: false,
                                    beforeSend: function () {
                                        $('#ajax_loader').show();
                                    },
                                    success: function (data) {
                                        namaKontraktor = new Array();
                                        kodeKontraktor = new Object();
                                        data = JSON.parse(JSON.stringify(data));
                                        var allKontraktor = data.kontraktor;
                                        // result = data.kontraktor;
                                        $.each(allKontraktor, function (index, kontraktor) {
                                            namaKontraktor.push(kontraktor.kode + " - " + kontraktor.nama);
                                            kodeKontraktor[kontraktor.kode + " - " + kontraktor.nama] = kontraktor.kode;
                                        });
                                        $('#nama_kontraktor').typeahead('val', '')
                                        $('#nama_kontraktor').focus().typeahead('val', query).focus();
                                    }
                                });
                                $('#ajax_loader').hide();
                            }, 500);
                        }
                    }, 1000);
                };
                $('.typeahead').typeahead({
                            hint: true,
                            highlight: true, /* Enable substring highlighting */
                            minLength: 4, /* Specify minimum characters required for showing result */
                        },
                        {
                            name: 'kontraktor',
                            source: function (query, process) {
                                return process(namaKontraktor);
                            }
                        }
                );
                $(".tt-menu").css('position', 'relative');
                $("#nama_kontraktor").on('typeahead:selected', function (e, datum) {
                    var kode = kodeKontraktor[datum];
                    $("#kode_kontraktor").val(kode);
                    $("#file_pernyataan").removeAttr('required');
                    checkform();
                });
                $("#nama_kontraktor").change(function () {
                    var nama = $("#nama_kontraktor").val();
                    var kode = kodeKontraktor[nama];
                    $("#kode_kontraktor").val(kode);
                    if (typeof kode != 'undefined') {
                        $("#file_pernyataan").removeAttr('required');
                    } else {
                        $("#nama_kontraktor").typeahead('val', "");
                        $("#nama_kontraktor").val('');
                        @if(@$transmisi->file_pernyataan == null)
                          $("#file_pernyataan").attr("required", true);
                        @endif
                        checkform();
                    }
                });
            </script>
@endsection
