<br>
<div class="form-group">
    <div class="col-sm-3">
        <h4 class="col-sm-12 control-label"><strong>Data Pemilik Instalasi</strong></h4>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Pemilik</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->pemilik->nama_pemilik : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Instalasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->jenis_instalasi->jenis_instalasi : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Alamat Instalasi</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <textarea readonly rows="3" class="form-control form-white" name="alamat_instalasi" placeholder="Alamat Instalasi">{{($pemanfaatan_tt != null)? $pemanfaatan_tt->alamat_instalasi : ""}}</textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Provinsi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->provinsi->province : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->kota->city : ""}}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->longitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="latitude_awal" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->latitude_awal : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="longitude_akhir" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->longitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="latitude_akhir" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->latitude_akhir : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Trafo Terpasang</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="kapasitas_trafo_terpasang" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->kapasitas_trafo : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Daya Tersambung</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="daya_tersambung" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->daya_sambung : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">PHB TM</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="phb_tm" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->phb_tm : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">PHB TR</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="phb_tr" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->phb_tr : ""}}">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Penyedia Tenaga Listrik</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white" name="penyedia_tenaga_listrik" value="{{($pemanfaatan_tt != null)? $pemanfaatan_tt->penyedia_tl : ""}}">
       
    </div>
</div>