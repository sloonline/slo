@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>{{($pemilik != null) ? "Ubah" : "Tambah"}} Pemilik <strong>Instalasi</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/pemilik_instalasi')}}">Pemilik Instalasi</a></li>
                    <li class="active">{{($pemilik != null) ? "Ubah" : "Tambah"}}</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    {!! Form::open(array('url'=> '/eksternal/create_pemilik_instalasi', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
                    <input type="hidden" name="id" value="{{($pemilik != null) ? $pemilik->id : null}}">
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    GENERAL</a></li>
                            <li><a href="#kontrak" data-toggle="tab"><i
                                            class="icon-speedometer"></i> KONTRAK</a></li>
                            <li><a href="#spjbtl" data-toggle="tab"><i class="icon-user"></i> SPJBTL</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nama Pemilik</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input id="nama_pemilik" name="nama_pemilik" placeholder="nama pemilik"
                                               type="text"
                                               class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->nama_pemilik : null}}">
                                        <i class="fa fa-building"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                <textarea name="alamat_pemilik" rows="3" class="form-control form-white"
                                          placeholder="Alamat Instalasi...">{{($pemilik != null) ? $pemilik->alamat_pemilik : null}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Provinsi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select name="provinsi" class="form-control form-white" data-search="true" required
                                                id="province">
                                            <option value="">--- Pilih ---</option>
                                            @foreach($province as $item)
                                                <option value="{{$item->id}}" {{($pemilik != null && $pemilik->id_province == $item->id) ? "selected" : ""}}>{{$item->province}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select name="kabupaten" id="city" class="form-control form-white" required>
                                            <option value="">--- Pilih ---</option>
                                            @foreach($city as $item)
                                                <option value="{{$item->id}}"
                                                        class="{{$item->id_province}}" {{($pemilik != null && $pemilik->id_city == $item->id) ? "selected" : ""}}>{{$item->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Kode Pos</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="kode_pos" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->kode_pos_pemilik : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Telepon</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="telepon" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->telepon_pemilik : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">No Fax</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="no_fax" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->no_fax_pemilik : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Email</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="email_pemilik" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->email_pemilik : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select name="jenis_ijin_usaha" class="form-control form-white">
                                            <option value="">--- Pilih ---</option>
                                            @foreach($jenis_ijin_usaha as $item)
                                                <option value="{{$item->id}}" {{($pemilik != null && $pemilik->jenis_ijin_usaha == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Penerbit Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="penerbit_ijin_usaha" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->penerbit_ijin_usaha : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">No Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="no_ijin_usaha" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->no_ijin_usaha : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Masa Berlaku IU</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="masa_berlaku_iu" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                               value="{{($pemilik != null) ? $pemilik->masa_berlaku_iu : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File Surat Ijin Usaha</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="file">
                                            <div class="option-group">
									<span class="file-button btn-primary">
									{{($pemilik != null && $pemilik->file_siup != null) ? "Ganti File" : "Choose File"}}
									</span>
                                                <input type="file" class="custom-file" name="file_surat_iu"
                                                       onchange="document.getElementById('uploader').value = this.value;">
                                                <input type="text" class="form-control form-white" id="uploader"
                                                       placeholder="no file selected"
                                                       value="{{($pemilik != null && $pemilik->file_siup != null) ? $pemilik->file_siup : null}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($pemilik != null && $pemilik->file_siup != null){
                                ?>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label"></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <a href="{{url('upload/'.$pemilik->file_siup)}}" class="btn btn-primary"><i
                                                    class="fa fa-download"></i>View</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane fade" id="kontrak">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Sewa (Jika Ada)</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input name="has_sewa" type="radio" id="sewa_true" class="form-control" {{($pemilik != null && $pemilik->nama_kontrak != "" ) ? "checked" : ""}}
                                               value="1"> Ya
                                    </div>
                                    <div class="col-sm-1">
                                        <input name="has_sewa" {{ ($pemilik == null) ? "checked": (($pemilik != null && $pemilik->nama_kontrak == "") ? "checked" : "")}} type="radio"
                                               id="sewa_false" class="form-control" value="0"> Tidak
                                    </div>
                                </div>
                                <div id="div-kontrak">
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nama Kontrak</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input name="nama_kontrak" type="text" class="form-control form-white"
                                                   value="{{($pemilik != null) ? $pemilik->nama_kontrak : null}}">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Nomor Kontrak</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input name="no_kontrak" type="text" class="form-control form-white"
                                                   value="{{($pemilik != null) ? $pemilik->no_kontrak : null}}">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Tanggal Pengesahaan Kontrak</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input name="tgl_pengesahan_kontrak" type="text"
                                                   class="form-control b-datepicker form-white"
                                                   data-date-format="dd-mm-yyyy"
                                                   data-lang="en" data-RTL="false"
                                                   value="{{($pemilik != null) ? $pemilik->tgl_pengesahan_kontrak : null}}">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">Masa Berlaku Kontrak</label>
                                        </div>
                                        <div class="col-sm-9 prepend-icon">
                                            <input name="masa_berlaku_kontrak" type="text"
                                                   class="form-control b-datepicker form-white"
                                                   data-date-format="dd-mm-yyyy"
                                                   data-lang="en" data-RTL="false"
                                                   value="{{($pemilik != null) ? $pemilik->masa_berlaku_kontrak : null}}">
                                            <i class="fa fa-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label">File Kontrak Sewa</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="file">
                                                <div class="option-group">
                                                    <span class="file-button btn-primary">Choose File</span>
                                                    <input type="file" class="custom-file" name="file_kontrak_sewa"
                                                           onchange="document.getElementById('uploader2').value = this.value;">
                                                    <input type="text" class="form-control form-white" id="uploader2"
                                                           placeholder="no file selected"
                                                           value="{{($pemilik != null && $pemilik->file_kontrak != null) ? $pemilik->file_kontrak : null}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($pemilik != null && $pemilik->file_kontrak != null){
                                    ?>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="col-sm-12 control-label"></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <a href="{{url('upload/'.$pemilik->file_kontrak)}}" class="btn btn-primary"><i
                                                        class="fa fa-download"></i>View</a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="spjbtl">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Nomor SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="no_spjbtl" type="text" class="form-control form-white"
                                               value="{{($pemilik != null) ? $pemilik->no_spjbtl : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Tanggal SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="tgl_spjbtl" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy" data-lang="en" data-RTL="false"
                                               value="{{($pemilik != null) ? $pemilik->tgl_spjbtl : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Masa Berlaku SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9 prepend-icon">
                                        <input name="masa_berlaku_spjbtl" type="text"
                                               class="form-control b-datepicker form-white"
                                               data-date-format="dd-mm-yyyy"
                                               data-lang="en" data-RTL="false"
                                               value="{{($pemilik != null) ? $pemilik->masa_berlaku_spjbtl : null}}">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">File SPJBTL</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="file">
                                            <div class="option-group">
                                                <span class="file-button btn-primary">Choose File</span>
                                                <input type="file" class="custom-file" name="file_spjbtl"
                                                       onchange="document.getElementById('uploader3').value = this.value;">
                                                <input type="text" class="form-control form-white" id="uploader3"
                                                       placeholder="no file selected"
                                                       value="{{($pemilik != null && $pemilik->file_spjbtl != null) ? $pemilik->file_spjbtl : null}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php if($pemilik != null && $pemilik->file_spjbtl != null){
                                ?>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label"></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <a href="{{url('upload/'.$pemilik->file_spjbtl)}}" class="btn btn-primary"><i
                                                    class="fa fa-download"></i>View</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-t-0 m-t-0">
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a href="{{url('/eksternal/pemilik_instalasi')}}"
                               class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
                            <button type="submit" id="submit_form"
                                    class="btn btn-success btn-square btn-embossed"
                                    data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-disk"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
            <!-- Select Inputs -->
            <script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script>
                $("#city").chained("#province");
                statusSewa();

                // For oncheck callback
                $('input[name="has_sewa"]').on('ifChecked', function () {
                    statusSewa();
                });

                $('input[name="has_sewa"]').on('ifUnchecked', function () {
                    statusSewa();
                });

                function statusSewa() {
                    if ($("#sewa_true").is(':checked')) {
                        $("#div-kontrak").show('medium');
                    } else {
                        $("#div-kontrak").hide('medium');
                    }
                }
            </script>
@endsection