@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
@endsection

@section('content')
	<div class="page-content">
		<div class="header">
            <h2>{{($pemanfaatan_tm != null) ? "Ubah" : "Tambah"}} Instalasi <strong>Pemanfaatan Tegangan Menengah</strong></h2>
            <div class="breadcrumb-wrapper">
				<ol class="breadcrumb">
					<li><a href="{{url('/internal')}}">Dashboard</a></li>
					<li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">Data Instalasi</a></li>
					<li class="active">{{($pemanfaatan_tm != null) ? "Ubah" : "Tambah"}} Instalasi Pemanfaatan Tegangan Menengah</li>
				</ol>
            </div>
		</div>
		<div class="row">
            <div class="col-lg-12 portlets">
				<p class="m-t-10 m-b-20 f-16">Silahkan inputkan data instalasi pemanfaatan tegangan menengah.</p>
				<div class="panel">
					{!! Form::open(array('url'=> 'eksternal/create_instalasi_pemanfaatan_tm', 'enctype'=>"multipart/form-data", 'class'=> 'form-horizontal')) !!}
					<input type="hidden" name="id" value="{{($pemanfaatan_tm != null) ? $pemanfaatan_tm->id : 0}}"/>
				<div class="panel-header bg-primary">
					<h3><i class="icon-bulb"></i> Form <strong>{{($pemanfaatan_tm != null) ? "Ubah" : "Tambah"}} Instalasi</strong></h3>
				</div>
				<div class="panel-content">
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Pemilik Instalasi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="pemilik_instalasi" id="jenis_instalasi">
								<option value="">--- Pilih ---</option>
								@foreach($pemilik as $item)
								<option value="{{$item->id}}" {{($pemanfaatan_tm != null && $pemanfaatan_tm->pemilik_instalasi_id == $item->id) ? "selected" : ""}}>{{$item->nama_pemilik}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Jenis Instalasi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="jenis_instalasi" id="jenis_instalasi">
								<option value="">--- Pilih ---</option>
								@foreach($jenis_instalasi as $item)
								<option value="{{$item->id}}" {{($pemanfaatan_tm != null && $pemanfaatan_tm->id_jenis_instalasi == $item->id) ? "selected" : ""}}>{{$item->jenis_instalasi}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Nama Instalasi</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->nama_instalasi : ""}}" name="nama_instalasi">
							<i class="fa fa-building"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Alamat Instalasi</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<textarea rows="3" class="form-control form-white" name="alamat_instalasi" placeholder="Alamat Instalasi">{{($pemanfaatan_tm != null)? $pemanfaatan_tm->alamat_instalasi : ""}}</textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Provinsi</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" data-search="true" name="provinsi" id="province">
								<option value="">--- Pilih ---</option>
								@foreach($province as $item)
								<option value="{{$item->id}}" {{($pemanfaatan_tm!= null && $pemanfaatan_tm->id_provinsi == $item->id) ? "selected" : ""}}>{{$item->province}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kabupaten / Kota</label>
						</div>
						<div class="col-sm-9">
							<select id="city" name="kabupaten" class="form-control form-white" data-search="true">
								<option value="">--- Pilih ---</option>
								@foreach($city as $item)
								<option value="{{$item->id}}" class="{{$item->id_province}}"  {{($pemanfaatan_tm != null && $pemanfaatan_tm->id_kota == $item->id) ? "selected" : ""}}>{{$item->city}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Longitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="longitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->longitude_awal : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Latitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="latitude_awal" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->latitude_awal : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Longitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="longitude_akhir" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->longitude_akhir : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Latitude</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="latitude_akhir" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->latitude_akhir : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Kapasitas Trafo Terpasang</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="kapasitas_trafo_terpasang" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->kapasitas_trafo : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Daya Tersambung</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="daya_tersambung" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->daya_sambung : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">PHB TM</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="phb_tm" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->phb_tm : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">PHB TR</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="phb_tr" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->phb_tr : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Penyedia Tenaga Listrik</label>
						</div>
						<div class="col-sm-9 prepend-icon">
							<input type="text" class="form-control form-white" name="penyedia_tenaga_listrik" value="{{($pemanfaatan_tm != null)? $pemanfaatan_tm->penyedia_tl : ""}}">
							<i class="fa fa-credit-card"></i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label class="col-sm-12 control-label">Tegangan Pengenal</label>
						</div>
						<div class="col-sm-9">
							<select class="form-control form-white" name="tegangan_pengenal">
								<option value="">--- Pilih ---</option>
								@foreach($tegangan_pengenal as $item)
								<option value="{{$item->id}}" {{($pemanfaatan_tm != null && $pemanfaatan_tm->tegangan_pengenal == $item->id) ? "selected" : ""}}>{{$item->nama_reference}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<hr />
				<div class="panel-footer clearfix bg-white">
					<div class="pull-right">
						<a href="{{url('/eksternal/view_instalasi_pemanfaatan_tm')}}" class="btn btn-warning btn-square btn-embossed">Batal &nbsp;<i class="icon-ban"></i></a>
						<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed" data-style="zoom-in">Simpan &nbsp;<i class="glyphicon glyphicon-floppy-saved"></i></button>
					</div>
				</div>
				{!! Form::close() !!}
				</div>
            </div>
		</div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/jquery/jquery.chained.min.js"></script>
<script>$("#city").chained("#province"); </script>
@endsection