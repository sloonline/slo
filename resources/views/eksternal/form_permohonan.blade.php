@extends('../layout/layout_eksternal')

@section('page_css')
        <!-- BEGIN PAGE STYLE -->
<link href="{{ url('/') }}/assets/global/plugins/input-text/style.min.css" rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
      rel="stylesheet">
<link href="{{ url('/') }}/assets/global/plugins/rateit/rateit.css" rel="stylesheet">
<!-- END PAGE STYLE -->
<style>
    .disabled {
        pointer-events: none;
        cursor: not-allowed;
    }
</style>
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Detail <strong>Permohonan</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="#">Order</a></li>
                    <li class="active">Detail Permohonan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(array('url'=> '/eksternal/store_permohonan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                <div class="panel">
                    <input type="hidden" name="order_id" value="{{$order_id}}"/>
                    <input type="hidden" name="permohonan_id" id="permohonan_id" value="{{$permohonan_id}}"/>
                    <div class="form-group">
                        <div class="col-sm-12" style="font-size: 16pt;">
                            <h2 class="alert bg-primary m-0">Nomor Permohonan : {{($permohonan == null) ? "-" :$permohonan->nomor_permohonan}}</h2>
                        </div>
                    </div>
                    <div class="panel-content">
                        <ul class="nav nav-tabs nav-primary">
                            <li class="active"><a href="#general" data-toggle="tab"><i class="icon-info"></i>
                                    PERMOHONAN</a></li>
                            <li><a href="#lingkup" data-toggle={{($order == null) ? "" :"tab"}}><i
                                            class="icon-bag"></i> LINGKUP PEKERJAAN</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="general">
                                <div class="panel-body bg-white">
                                    <input type="hidden" name="no_order"
                                           value="{{($order != null)?$order->id: 0}}"/>
                                    <input type="hidden" name="permohonan_id"
                                           value="{{($permohonan != null) ? $permohonan->id : 0}}"/>
                                    @include($file_detail, ['instalasi' => $instalasi])
                                </div>
                            </div>
                            <div class="tab-pane fade" id="lingkup">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Produk Layanan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-white" name="produk_layanan"
                                               readonly value="{{$permohonan->produk->produk_layanan}}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <label class="col-sm-12 control-label">Jenis Lingkup Pekerjaan</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="lingkup_pekerjaan"
                                                class="form-control form-white disabled" data-style="white"
                                                data-search="true" name="lingkup_pekerjaan" required>
                                            @foreach($lingkup_pekerjaan as $row)
                                                <option {{($permohonan != null && $permohonan->id_lingkup_pekerjaan == $row->id) ? "selected='selected'" : ""}} value="{{$row->id}}">{{$row->jenis_lingkup_pekerjaan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="form_upload">
                                    {{--Form upload file digenerate di sini--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <a class="btn btn-warning btn-square"
                               href="{{url(($order != null) ? '/eksternal/create_order/'.$order->id:'#')}}">Kembali</a>

                            @if($order->flow_status->modul == MODUL_ORDER && $order->flow_status->tipe_status == REJECTED )
                                {{--<button type="submit" class="btn btn-success ladda-button btn-square btn-embossed"--}}
                                        {{--data-style="zoom-in">Simpan &nbsp;<i--}}
                                            {{--class="glyphicon glyphicon-floppy-saved"></i></button>--}}
                            @endif
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

    <script type="text/javascript">
        if ($('#permohonan_id').val() > 0) generateForm();
        if ($('#permohonan_id').val() > 0) generateFormInstalasi();
        $('#lingkup_pekerjaan').change(function () {
            generateForm();
        });
        $('#lingkup_program').change(function () {
            generateForm();
        });
        $('#id_instalasi').change(function () {
            generateFormInstalasi();
        });
        function generateForm() {
            if ($('#lingkup_pekerjaan').val() != "") {
                @if($order->flow_status->modul == MODUL_ORDER && $order->flow_status->tipe_status == REJECTED )
                    $('#form_upload').load("/eksternal/custom_field/" + $('#lingkup_pekerjaan').val() + "/" + $('#permohonan_id').val() + "/{{@$instalasi->status_baru}}");
                @else
                     $('#form_upload').load("/eksternal/custom_field_verifikasi/" + $('#lingkup_pekerjaan').val() + "/" + $('#permohonan_id').val() + "/{{@$instalasi->status_baru}}");
                @endif
               $('#form_upload').show();
            }
        }

        function generateFormInstalasi() {
            if ($('#id_instalasi').val() == "") {
                $('#detail_instalasi').html("");
            } else {
                $('#detail_instalasi').load("/eksternal/instalasi_field/" + $('#tipe_instalasi').val() + "/" + $('#id_instalasi').val());
            }
        }

    </script>
@endsection