<div class="col-md-12" style="margin: 10px;overflow: auto;height: 500px;padding: 10px;">
    <h2 align="center" style="font-weight: bold;">SYARAT DAN ATURAN</h2>

    <div class="row" style="margin-top: 50px;width: 950px;">
        <div class="col-md-12">
            <ol type="1">
                <li>Konstruksi telah selesai serta Instalasi sudah terpasang dan siap untuk disertifikasi.</li>
                <li>PLN Pusat Sertifikasi dalam melaksanakan Inspeksi secara konsisten yang disertai ketelitian dan kehati-hatian yang sebaik mungkin yang umumnya dilakukan oleh profesional didalam melaksanakan Inspeksi yang sifatnya serupa, dengan memperhitungkan standar dan peraturan yang berlaku pada saat Inspeksi dilakukan, dimana pelaksanaannya dilakukan dengan aman, sah menurut hukum, efisien dan pantas.</li>
                <li>Pelaksanaan Pekerjaan dimulai sejak inspektor dari PLN Pusat Sertifikasi siap di lapangan, Instalasi benar dan lengkap pemasangannya (construction essentially complete) dan siap diinspeksi. Pada setiap tahapan pelaksanaan pekerjaan, Pemohon menyampaikan surat pemberitahuan resmi kepada PLN Pusat Sertifikasi.</li>
                <li>SLO diterbitkan apabila sudah dilakukan inspeksi dan evaluasi sesuai standar dan peraturan sesuai lingkup inspeksi dalam perjanjian ini, untuk memenuhi persyaratan kontrak, standar dan peraturan keselamatan ketenagalistrikan. Peraturan yang dimaksud yaitu suatu keadaan yang terwujud apabila terpenuhi persyaratan kondisi andal bagi instalasi dan aman bagi instalasi dan manusia, baik pekerja dan masyarakat umum, serta kondisi akrab lingkungan dalam arti tidak merusak lingkungan hidup di sekitar instalasi ketenagalistrikan serta peralatan dan pemanfaat tenaga listrik yang memenuhi standar.</li>
                <li>Inspeksi dan SLO dilaksanakan dengan cara sesuai definisi dan batasan inspeksi di standar ISO/IEC 17020 dan sesuai dengan Permen No. 10 tahun 2016 tentang Tata Cara Akreditasi dan Sertifikasi Ketenagalistrikan.</li>
                <li>Pemohon SLO bertanggung jawab pada kelancaran dan kebenaran seluruh pengujian yang disyaratkan sesuai dengan metode uji yang telah disepakati oleh kedua belah pihak. Seluruh hasil pengujian mendapat persetujuan oleh kedua belah pihak dan selanjutnya sebagai dasar evaluasi keputusan sertifikasi.</li>
                <li>Pemohon SLO telah memastikan bahwa instalasi tenaga listrik yaitu Pembangkit sebelum dilakukan inspeksi dan sertifikasi sudah terhubung dengan sistem jaringan tenaga listrik external sesuai standar dan peraturan untuk keperluan pengujian dan pembebanan.</li>
                <li>Pemohon SLO harus memastikan bahwa peralatan individu, sub sistem dan sistem yang terpasang di instalasi tenaga listrik yang dilakukan inspeksi dan sertifikasi sesuai dengan standar yang ditetapkan dan dibuktikan dengan type test, Factory Acceptance Test (FAT) dan sertifikasi produk.</li>
                <li>Keputusan dan penerbitan SLO didasarkan pada data, rekaman mutu, hasil witness pengujian, dan evaluasi inspeksi yang berlaku pada saat dilakukan inspeksi.</li>
                <li>Sertifikat Laik Operasi berlaku paling lama  5 (lima) tahun. Selama masa berlaku sertifikat pemilik dan pihak yang mengoperasikan instalasi tenaga listrik harus melaksanakan pemeliharaan, pengoperasian, dan pengamanan instalasi tenaga listrik sesuai standar dan peraturan yang ditetapkan.</li>
                <li>Sertifikasi Laik Operasi tidak berlaku jika pada masa berlakunya sertifikat terjadi  perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</li>
                <li>Apabila terjadi perubahan seperti tersebut diatas dan Pemohon SLO harus memberitahukan kepada PLN Pusat Sertifikasi untuk dievaluasi dan dilakukan inspeksi ulang oleh Lembaga Inspeksi yang ditunjuk, maka SLO dinyatakan gugur. Apabila Pemohon SLO mengabaikan keharusan evaluasi dan inspeksi seperti tersebut diatas maka SLO dinyatakan tidak berlaku.</li>
                <li>Apabila dalam pelaksanaan Inspeksi terdapat peralatan individu, sub system, system tidak berfungsi sesuai standar dan peraturan yang ditetapkan, sehingga SLO tidak bisa diterbitkan maka menjadi tanggung jawab Pemohon SLO dan tidak menjadi pekerjaan berkurang.</li>
                <li>Apabila terjadi keterlambatan dalam pelaksanaan pekerjaan yang disebabkan karena peralatan atau instalasi Pemohon SLO tidak siap uji dan atau terganggunya system tenaga listrik external maka keterlambatan tersebut akan diperhitungkan sebagai biaya tambah.</li>
                <li>Apabila PLN Pusat Sertifikasi dalam melakukan inspeksi ditemukan Pending Item maka Pemohon SLO harus memperbaiki pending item tersebut paling lambat 90 (sembilan puluh) hari setelah Pemohon SLO menerima pemberitahuan pending item dari PLN Pusat Sertifikasi. Setelah dilakukan perbaikan pending item oleh Pemohon SLO maka PLN Pusat Sertifikasi melakukan evaluasi perbaikan dan inspeksi ulang setelah Pemohon SLO mengirimkan surat pemberitahuan perbaikan pending item.</li>
                <li>Apabila Pemohon SLO dalam melakukan perbaikan pending item tidak melampaui batas waktu 90 (sembilan puluh) hari dan PLN Pusat Sertifikasi dalam melakukan inspeksi ulang untuk evaluasi dan inspeksi  perbaikan pending item membutuhkan tambahan Mandays dan trip karena melampaui ketersediaan Mandays dan trip akan diperhitungkan sebagai biaya tambah.</li>
                <li>Apabila dalam kurun waktu 90 (sembilan puluh) hari Pemohon SLO belum dapat menyelesaikan pending item, secara teknis dan peraturan dapat dibuktikan bahwa hasil inspeksi tidak dapat diputuskan untuk terbitnya SLO, maka pekerjaan dinyatakan selesai dan apabila pemohon SLO ingin melanjutkan pekerjaan tersebut maka akan diperhitungkan sebagai pekerjaan baru atau inspeksi ulang.</li>
                <li>Apabila dari hasil inspeksi dan evaluasi inspeksi tidak memenuhi persyaratan diterbitkannya SLO berdasarkan standar dan peraturan dan Pemohon SLO tidak melanjutkan proses inspeksi dan sertifikasi maka hasil inspeksi menjadi tanggung jawab Pemohon SLO.</li>
                <li>Hasil Inspeksi merupakan data yang harus dilindungi kerahasiaannya dan menjadi milik Pemohon SLO.</li>
                <li>Apabila pekerjaan inspeksi telah selesai dilaksanakan, dan telah diterbitkan SLO, maka
                    <ol type="a">
                        <li>Pemilik instalasi harus melakukan pengoperasian dan pemeliharaan instalasi penyediaan tenaga listrik dan atau instalasi pemanfaatan tenaga listrik sesuai standart dan peraturan yang berlaku.</li>
                        <li>Pemilik instalasi harus melakukan pengamanan instalasi penyediaan tenaga listrik dan atau instalasi pemanfaatan sesuai dengan standart dan peraturan yang berlaku.</li>
                    </ol>
                    Keterangan :
                    <ul>
                        <li>Pengoperasian adalah suatu kegiatan usaha untuk mengendalikan dan mengkoordinasikan antar system pada instalasi.</li>
                        <li>Pemeliharaan adalah segala kegiatan yang meliputi program pemeriksaan, perawatan, perbaikan dan uji ulang, agar instalasi selalu dalam keadaan baik dan bersih, penggunaannya aman, dan gangguan serta kerusakan mudah diketahui, dicegah atau diperkecil.</li>
                        <li>Pengamanan adalah segala kegiatan, system dan perlengkapannya, untuk mencegah bahaya terhadap keamanan instalasi, keselamatan kerja dan keselamatan umum, baik yang diakibatkan oleh instalasi maupun oleh lingkungan</li>
                    </ul>
                </li>
                <li>Keselamatan kerja selama inspeksi, mohon kerjasamanya dalam mengidentifikasi sumber bahaya di instalasi ketenagalistrikan, sehingga inspektor bisa menyiapkan cara kerja yang aman dan dilengkapi dengan alat pelindung diri (APD) yang sesuai.</li>
                <li>Pemohon SLO harus bertanggung jawab dan membebaskan PLN Pusat Sertifikasi dari dan setiap klaim, tuntutan biaya - biaya, pembayaran-pembayaran dan pengeluaran-pengeluaran sehubungan dengan terjadinya kerusakan - kerusakan alat/ instalasi saat sedang dilakukan Inspeksi.</li>
                <li>Pemohon SLO harus membebaskan PLN Pusat Sertifikasi dari segala tuntutan hukum atas pengadaan barang / alat yang dilakukan oleh Pemohon SLO.</li>
                <li>Inspektor kami sudah diberikan biaya dan fasilitas yang cukup sesuai dengan peraturan yang berlaku, mohon untuk tidak memberikan tambahan biaya dan fasilitas lainnya dalam bentuk apapun.</li>
                <li>Pekerjaan dapat mulai dilaksanakan jika telah ditanda tanganinya kontrak perjanjian antara kedua belah pihak dan Saudara menyetujui persyaratan pelaksanaan pekerjaan dan telah menyelesaikan biaya tersebut</li>
                <li>Apabila sampai berakhirnya pelaksanaan Inspeksi, dokumen pendukung belum dilengkapi, maka PLN Pusat Sertifikasi hanya akan menerbitkan Rekomendasi Laik Sinkron.</li>

            </ol>
        </div>
    </div>

    <div class="row" style="margin-top: 15px;width: 950px;">
        <div class="col-md-10">&nbsp;</div>
        <div class="col-md-2" align="right">
            <button type="submit" class="btn btn-success" id="btn_setuju"><i
                        class="fa fa-check"></i> Setuju
            </button>
        </div>
    </div>

</div>