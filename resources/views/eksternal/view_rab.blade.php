@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            {{--<h2><strong>RAB</strong> Permohonan {{$permohonan->nomor_permohonan}}</h2>--}}
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/internal')}}">Dashboard</a></li>
                    <li><a href="{{url('/internal/view_user_peminta_jasa_pln')}}">RAB</a></li>
                    <li class="active">New</li>
                </ol>
            </div>
        </div>
        {!! Form::open(array('url'=> '/internal/create_rancangan_biaya', 'files'=> true, 'class'=> 'form-horizontal')) !!}
        <input type="hidden" name="order_id" value="{{$order_id}}">
        <input type="hidden" name="rab_id" value="{{($rab == null) ? null : $rab->id}}">
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel">
                    <div class="panel-header panel-controls bg-primary">
                        <h3><i class="fa fa-table"></i> Form Input <strong>RAB</strong></h3>
                    </div>
                    <div class="panel-content">
                        {{-- start form pilih permohonan--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne">
                                            <i class="icon-plus"></i> Pilih Permohonan
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in {{($rab != null)?'in':''}}">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor Permohonan</th>
                                                        <th>Tanggal Permohonan</th>
                                                        <th>Jenis Instalasi</th>
                                                        <th>Nama Instalasi</th>
                                                        <th>Produk Layanan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(is_array($permohonan) || is_object($permohonan))
                                                        <?php $no = 1; ?>
                                                        @foreach($permohonan as $item)
                                                            <?php
                                                            $status = "";
                                                            foreach ($used_perm as $p) {
                                                            if ($p->id == $item->id) {
                                                            if($p->status == "curr") {
                                                            ?>
                                                            <tr>
                                                                <td>{{ $no}}</td>
                                                                <td>{{ $item->nomor_permohonan}}</td>
                                                                <td>{{ date('d M Y',strtotime($item->tanggal_permohonan)) }}</td>
                                                                <td>{{ $item->lingkup_pekerjaan->jenis_lingkup_pekerjaan}}
                                                                    @if($item->instalasi->jenis_instalasi->keterangan == JENIS_GARDU)
                                                                        <br/>
                                                                        @foreach($item->bayPermohonan as $bg)
                                                                            - {{$bg->bayGardu->nama_bay}}<br/>
                                                                        @endforeach
                                                                    @endif
                                                                </td>
                                                                <td>{{ $item->instalasi->jenis_instalasi->jenis_instalasi}}</td>
                                                                <td>{{ @$item->produk->produk_layanan}}</td>
                                                            </tr>
                                                            <?php
                                                            }

                                                            break;
                                                            }
                                                            }
                                                            ?>

                                                            <?php $no++; ?>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end form pilih permohonan--}}
                        {{-- start rancangan biaya--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo">
                                            <i class="icon-plus"></i> Rancangan Biaya
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in {{($rab != null)?'in':''}}">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="nav-tabs2" id="tabs">
                                                    <ul class="nav nav-tabs">
                                                        <?php $no = 0 ?>
                                                        @foreach($jenis as $item)
                                                            <li {{($no == 0) ? "class=active":''}}>
                                                                <a href="{{'#'.$item->div_id}}" data-toggle="tab">
                                                                    {{$item->jenis}}
                                                                </a>
                                                            </li>
                                                            <?php $no++ ?>
                                                        @endforeach
                                                    </ul>
                                                    <div class="tab-content bg-white">
                                                        <?php $no = 0 ?>
                                                        @foreach($jenis as $i => $item)
                                                            <div class="tab-pane {{($no == 0) ? "active":''}}"
                                                                 id="{{$item->div_id}}">
                                                                <table class="table">
                                                                    <thead>
                                                                    <tr>
                                                                        <th rowspan="2" colspan="2" class="text-left">NO
                                                                        </th>
                                                                        <th rowspan="2" colspan="2" class="text-left">
                                                                            KODE
                                                                        </th>
                                                                        <th rowspan="2" class="text-left"
                                                                            style="width:20%">UNSUR BIAYA
                                                                        </th>
                                                                        <th rowspan="2" class="text-center"
                                                                            style="width:15%">TARIF
                                                                        </th>
                                                                        <th colspan="2" class="text-center">HARI ORANG
                                                                            (MD)
                                                                        </th>
                                                                        <th rowspan="2" class="text-center"
                                                                            style="width:5%">SATUAN
                                                                        </th>
                                                                        <th rowspan="2" class="text-center"
                                                                            style="width:20%">VOLUME
                                                                        </th>
                                                                        <th rowspan="2" class="text-center"
                                                                            style="width:20%">JUMLAH BIAYA
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center" style="width:10%">
                                                                            HARI
                                                                        </th>
                                                                        <th class="text-center" style="width:10%">ORANG
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php
                                                                    $no = 1;
                                                                    ?>
                                                                    @foreach($unsur_arr[$item->jenis] as $j => $unsur_item)
                                                                        <?php
                                                                        $unsur = null;
                                                                        foreach ($detail_rab as $row) {
                                                                            if ($row->id_unsur_biaya == $unsur_item->id) {
                                                                                $unsur = $row;
                                                                                break;
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <input name="{{'rab_detail_'.$item->div_id.'_id[]'}}"
                                                                               type="hidden"
                                                                               value="{{($unsur != null) ? $unsur->id : null}}">
                                                                        <input name="{{$item->div_id.'_id[]'}}"
                                                                               type="hidden"
                                                                               class="form-control form-white"
                                                                               value="{{$unsur_item->id}}"/>
                                                                        <tr class="item-row">
                                                                            <td>{{$no}}</td>
                                                                            <td></td>
                                                                            <td>{{$unsur_item->kode}}</td>
                                                                            <td></td>
                                                                            <td>{{$unsur_item->unsur}}</td>
                                                                            <td class="text-center"><input
                                                                                        id="{{'tarif_'.$i.'_'.$j}}"
                                                                                        value="{{$unsur_item->tarif}}"
                                                                                        onchange="calculate({{$i.','.$j.','.$unsur_item->satuan}})"
                                                                                        name="{{'tarif_'.$i.'[]'}}"
                                                                                        type="number"
                                                                                        min="0"
                                                                                        style="text-align: right"
                                                                                        class="form-control form-white"
                                                                                        readonly>

                                                                            </td>
                                                                            <td class="text-center"><input
                                                                                        id="{{'hari_'.$i.'_'.$j}}"
                                                                                        onchange="calculate({{$i.','.$j.','.$unsur_item->satuan}})"
                                                                                        name="{{'hari_'.$i.'[]'}}"
                                                                                        type="number"
                                                                                        min="0"
                                                                                        style="text-align: right"
                                                                                        class="form-control form-white"
                                                                                        value="{{@$unsur->kantor_hari}}"
                                                                                        readonly
                                                                                >
                                                                            </td>
                                                                            <td class="text-center"><input
                                                                                        id="{{'orang_'.$i.'_'.$j}}"
                                                                                        onchange="calculate({{$i.','.$j.','.$unsur_item->satuan}})"
                                                                                        name="{{'orang_'.$i.'[]'}}"
                                                                                        type="number"
                                                                                        min="0"
                                                                                        style="text-align: right"
                                                                                        class="form-control form-white"
                                                                                        value="{{@$unsur->kantor_orang}}"
                                                                                        readonly
                                                                                >
                                                                            </td>
                                                                            <td class="text-center">{{($unsur_item->satuan==0)?'HO':'BO'}}</td>
                                                                            <td class="text-center"><input
                                                                                        id="{{'jumlah_'.$i.'_'.$j}}"
                                                                                        onchange="calculate({{$i.','.$j.','.$unsur_item->satuan}})"
                                                                                        name="{{'jumlah_'.$i.'[]'}}"
                                                                                        type="number"
                                                                                        min="0"
                                                                                        style="text-align: right"
                                                                                        class="form-control bg-aero"
                                                                                        value="{{@$unsur->jumlah}}"
                                                                                        readonly
                                                                                >
                                                                            </td>
                                                                            <td class="text-center"><input
                                                                                        id="{{'jumlah_biaya_'.$i.'_'.$j}}"
                                                                                        name="{{'jumlah_biaya_'.$i.'[]'}}"
                                                                                        type="number" min="0"
                                                                                        style="text-align: right"
                                                                                        class="form-control bg-aero"
                                                                                        value="{{@$unsur->jumlah_biaya}}"
                                                                                        readonly>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        $no++;
                                                                        ?>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <?php $no++ ?>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end rancangan biaya--}}
                    </div>
                    <div class="panel-footer clearfix bg-white">
                        <div class="pull-right">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Jumlah Biaya</th>
                                    <th><input name="total_biaya" id="total_biaya" class="form-control form-white"
                                               type="hidden" readonly min="0"
                                               value="{{@$rab->jumlah_biaya}}"
                                               style="width:300px">
                                        <input id="total_biaya_text" class="form-control form-white"
                                               type="text" readonly min="0"
                                               value="{{@$rab->jumlah_biaya}}"
                                               style="width:300px">
                                    </th>
                                </tr>
                                <tr>
                                    <th>PPN 10%</th>
                                    <th><input name="total_ppn" id="total_ppn" class="form-control form-white"
                                               type="hidden" readonly min="0"
                                               value="{{@$rab->ppn}}"
                                               style="width:300px">
                                        <input id="total_ppn_text" class="form-control form-white"
                                               type="text" readonly min="0"
                                               value="{{@$rab->ppn}}"
                                               style="width:300px">
                                    </th>
                                </tr>
                                <tr>
                                    <th>TOTAL BIAYA JASA SETELAH PPN</th>
                                    <th><input name="total_bayar" id="total_bayar" class="form-control input-lg bg-aero"
                                               type="hidden" readonly min="0"
                                               value="{{@$rab->total_biaya}}"
                                               style="width:300px">
                                        <input id="total_bayar_text" class="form-control input-lg bg-aero"
                                               type="text" readonly min="0"
                                               value="{{@$rab->total_biaya}}"
                                               style="width:300px">
                                    </th>
                                </tr>
                                </thead>
                            </table>
                            <div class="pull-right">
                                <a href="{{url('eksternal/rancangan_biaya_order/'.$order_id)}}"
                                   class="btn btn-warning btn-square btn-embossed">Kembali <i
                                            class="icon-ban"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @endsection

        @section('page_script')
            <script>
                $(document).ready(function () {
                    $('#total_biaya_text').val("Rp " + addCommas($('#total_biaya_text').val()));
                    $('#total_ppn_text').val("Rp " + addCommas($('#total_ppn_text').val()));
                    $('#total_bayar_text').val("Rp " + addCommas($('#total_bayar_text').val()));
                });

                function calculate(i, j, satuan) {

                    var total_biaya = $('#total_biaya').val();

                    var hari = $('#hari_' + i + '_' + j).val();
                    var orang = $('#orang_' + i + '_' + j).val();
                    var tarif = $('#tarif_' + i + '_' + j).val();

                    if (satuan == 0) {
                        $('#jumlah_' + i + '_' + j).val(hari * orang);
                        var jumlah_biaya_bf = $('#jumlah_biaya_' + i + '_' + j).val();
                        var jumlah_biaya = $('#jumlah_biaya_' + i + '_' + j).val(hari * orang * tarif);
                        total_biaya = $('#total_biaya').val((+total_biaya - jumlah_biaya_bf) + +jumlah_biaya.val());
                    } else {
                        $('#jumlah_' + i + '_' + j).val(hari / orang);
                        var jumlah_biaya = $('#jumlah_biaya_' + i + '_' + j).val(hari / orang * tarif);
                        total_biaya = $('#total_biaya').val(total_biaya + jumlah_biaya.val());
                    }

                    var ppn = $('#total_ppn').val(0.1 * total_biaya.val()); //JUMLAH PPN 10%
                    $('#total_bayar').val(+total_biaya.val() + +ppn.val()); //TOTAL BAYAR

                    //memberi separator pada currency
                    $('#total_biaya_text').val("Rp " + addCommas(total_biaya.val()));
                    $('#total_ppn_text').val("Rp " + addCommas(total_biaya.val()));
                    $('#total_bayar_text').val("Rp " + addCommas(total_biaya.val()));

                }

                function validatePermohonan() {
                    var checked = $('.permohonan:checked');
                    var count = 0;
                    for (var i = 0; i < checked.length; i++) {
                        if (checked[i].getAttribute('disabled') == null) {
                            count++;
                        }
                    }
                    if ($("#total_bayar").val() == "") {
                        alert('Anda belum mengisi data RAB');
                        return false;
                    }
                    if (count == 0) {
                        alert('Anda belum memilih permohonan');
                        return false;
                    }
                }

                function addCommas(nStr) {
                    nStr += '';
                    x = nStr.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    return x1 + x2;
                }
            </script>
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <!-- Buttons Loading State -->
            <script>
                $("input").unbind('keydown');
            </script>
@endsection