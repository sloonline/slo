<br>
<div class="form-group">
    <div class="col-sm-3">
        <h4 class="col-sm-12 control-label"><strong>Data Pemilik Instalasi</strong></h4>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Nama Pemilik</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white" value="{{($pembangkit != null)? $pembangkit->pemilik->nama_pemilik : ""}}" name="jenis_instalasi">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jenis Instalasi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->jenis_instalasi->jenis_instalasi : ""}}" name="longitude_awal">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Alamat Instalasi</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <textarea readonly rows="3" class="form-control form-white" name="alamat_instalasi"
                  placeholder="Alamat Instalasi..."> {{($pembangkit != null)? $pembangkit->alamat_instalasi : ""}}</textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Provinsi</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->provinsi->province : ""}}" name="longitude_awal">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kabupaten / Kota</label>
    </div>
    <div class="col-sm-9">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->kota->city : ""}}" name="longitude_awal">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Awal Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->longitude_awal : ""}}" name="longitude_awal">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->latitude_awal : ""}}" name="latitude_awal">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Titik Koordinat Akhir Instalasi</label>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Longitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->longitude_akhir : ""}}" name="longitude_akhir">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Latitude</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->latitude_akhir : ""}}" name="latitude_akhir">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Terpasang</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->kapasitas_terpasang : ""}}" name="kapasitas_terpasang">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Hasil Uji</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->kapasitas_hasil_uji : ""}}" name="kapasitas_hasil_uji">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">No Unit Pembangkit</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->no_unit_pembangkit : ""}}" name="no_unit_pembangkit">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">No Seri Generator</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->no_seri_generator : ""}}" name="no_seri_generator">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">No Seri Turbin / Mesin</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->no_seri_turbin : ""}}" name="no_seri_turbin">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Modul Per Unit</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->kapasitas_modul : ""}}" name="kapasitas_modul_per_unit">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Kapasitas Inverter Per Unit</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->kapasitas_inverter : ""}}"
               name="kapasitas_inverter_per_unit">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jumlah Modul</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->jumlah_modul : ""}}" name="jumlah_modul">
       
    </div>
</div>
<div class="form-group">
    <div class="col-sm-3">
        <label class="col-sm-12 control-label">Jumlah Inverter</label>
    </div>
    <div class="col-sm-9 prepend-icon">
        <input type="text" readonly class="form-control form-white"
               value="{{($pembangkit != null)? $pembangkit->jumlah_inverter : ""}}" name="jumlah_inverter">
       
    </div>
</div>