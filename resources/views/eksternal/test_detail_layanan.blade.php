@extends('../layout/layout_eksternal')

@section('page_css')
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.demo2.css" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/global/plugins/hover-effects/hover-effects.css">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2><strong>Detail</strong> Instalasi</h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/instalasi')}}">Layanan</a></li>
                    <li class="active">Detail Layanan</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <br/>
                    <div class="panel-content">
                        <div class="row column-seperation">
                            <div class="col-md-4 line-separator">
                                <section id="gallery" class="simplegallery">
                                <div class="content">
                                    <img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" class="image_1 img-thumbnail" alt="" />
                                    <img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" class="image_2 img-thumbnail" style="display:none" alt="" />
                                    <img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" class="image_3 img-thumbnail" style="display:none" alt="" />
                                </div>
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <a href="#" rel="1">
                                            <img src="{{ url('/') }}/assets/global/images/widgets/square1.jpg" id="thumb_1" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="#" rel="2">
                                            <img src="{{ url('/') }}/assets/global/images/widgets/square2.jpg" id="thumb_2" alt="" />
                                        </a>
                                    </div>
                                    <div class="thumb last">
                                        <a href="#" rel="3">
                                            <img src="{{ url('/') }}/assets/global/images/widgets/square3.jpg" id="thumb_3" alt="" />
                                        </a>
                                    </div>
                                </div>
                                </section>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <h1 class="f-32">Supervisi <strong>Komisioning</strong></h1>
                                        <h3 class="m-t-0"><strong>Pembangkit</strong></h3><hr/>
                                        <div class="tarkiman">
                                            <p>Visual hierarchy is one of the most important principles behind effective web design.
                                            Design is a funny word. Some people think design means how it looks.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="panel-header bg-primary text-center">
                        <h3><i class="icon-energy"></i> <strong>Instalasi Pembangkit</strong></h3>
                    </div>
                    <div class="panel-content p-t-0 p-b-20">
                    <br>
                        <div class="portfolioContainer grid">
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/14.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/3.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/2.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/1.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/5.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/5.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/5.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                            <div class="col-md-3 pembangkit p-b-20 text-center">
                              <img width="100%" src="{{ url('/') }}/assets/global/images/gallery/5.jpg" class="img-thumbnail"/>
                              <h3 class="m-0 m-t-10"><strong>NAMA INSTALASI</strong></h3>
                              <h3 class="m-0"><strong>JENIS INSTALASI</strong></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_script')
<script src="{{ url('/') }}/assets/global/js/pages/slider.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/simplegallery/simplegallery.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/magnific/jquery.magnific-popup.min.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/isotope/isotope.pkgd.min.js"></script>
<script>
    $(window).load(function(){
      var $container = $('.portfolioContainer');
      $container.isotope();
      $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
        var selector = $(this).attr('data-filter');
        $container.isotope({
          filter: selector
        });
        return false;
      });
        $('#gallery').simplegallery({
            galltime : 400,
            gallcontent: '.content',
            gallthumbnail: '.thumbnail',
            gallthumb: '.thumb'
        });

    });
</script>

@endsection