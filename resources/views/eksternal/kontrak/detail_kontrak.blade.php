@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    @if(Auth::user()->jenis_user == TIPE_PLN)
        {{$label = 'Konfirmasi Biaya'}}
    @else
        {{$label = 'Kontrak'}}
    @endif
    <div class="page-content">
        <div class="header">
            <h2>Detail <strong>Kontrak</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/kontrak')}}">Kontrak</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    <div class="panel-content">
                        {!! Form::open(array('url'=> '', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {{--start form pilih rab--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <i class="icon-plus"></i> <strong>RAB</strong>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 m-l-0">
                                                <table class="table table-bordered m-b-0">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nomor RAB</th>
                                                        <th>Nomor Order</th>
                                                        <th>Tanggal RAB</th>
                                                        <th>Total Biaya</th>
                                                        <th>Peminta Jasa</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $no = 1; ?>
                                                    {{--list used rab--}}
                                                    @foreach($allRab as $item)
                                                        @if($item['val'] > 0)
                                                            <tr>
                                                                <td>{{ $no}}</td>
                                                                <td>{{ @$item["rab"]->no_dokumen}}</td>
                                                                <td>{{ @$item["rab"]->order->nomor_order}}</td>
                                                                <td>{{ date('d M Y',strtotime(@$item["rab"]->created_at)) }}</td>
                                                                <td>{{ @$item["rab"]->total_biaya}}</td>
                                                                <td>{{ @$item["rab"]->order->created_by}}</td>
                                                            </tr>
                                                        @endif
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end form pilih rab--}}
                        {{--start kontrak--}}
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            <i class="icon-plus"></i> <strong>DATA {{strtoupper(@$label)}}</strong>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 m-l-0">
                                                <div class="panel-content p-0">
                                                    <ul class="nav nav-tabs nav-primary">
                                                        <li class="active">
                                                            <a href="#kontrak" data-toggle="tab">
                                                                {{@$label}}
                                                            </a>
                                                        </li>
                                                        @if(Auth::user()->jenis_user == TIPE_EKSTERNAL)
                                                            @if($kontrak_id != 0)
                                                                <li>
                                                                    <a href="#amandemen_kontrak" data-toggle="tab">
                                                                        Amandemen Kontrak
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        @endif
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane fade active in" id="kontrak">
                                                            {!! Form::hidden('id', $kontrak_id) !!}
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label class="col-sm-12 control-label">Nomor {{@$label}}</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" required="required"
                                                                               name="nomor_kontrak"
                                                                               value="{{($latest != null) ? $latest->nomor_kontrak : ''}}"
                                                                               class="form-control form-white"
                                                                               placeholder="Nomor Kontrak">
                                                                        <i class="icon-list"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label class="col-sm-12 control-label">Tanggal {{@$label}}</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" required="required"
                                                                               name="tanggal_kontrak"
                                                                               value="{{($latest != null) ? date('d-m-Y',strtotime($latest->tgl_kontrak)): ''}}"
                                                                               class="form-control b-datepicker form-white"
                                                                               data-date-format="dd-mm-yyyy"
                                                                               data-lang="en"
                                                                               data-RTL="false"
                                                                               placeholder="Tanggal">
                                                                        <i class="icon-calendar"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @if(Auth::user()->jenis_user == TIPE_EKSTERNAL)
                                                                <div class="form-group">
                                                                    <div class="col-md-4">
                                                                        <label class="col-sm-12 control-label">Masa
                                                                            Berlaku {{@$label}}</label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="col-sm-12 prepend-icon">
                                                                            <input disabled type="text"
                                                                                   required="required"
                                                                                   name="masa_berlaku_kontrak"
                                                                                   value="{{($latest != null) ? date('d-m-Y',strtotime($latest->masa_berlaku_kontrak)): ''}}"
                                                                                   class="form-control b-datepicker form-white"
                                                                                   data-date-format="dd-mm-yyyy"
                                                                                   data-lang="en"
                                                                                   data-RTL="false"
                                                                                   placeholder="Masa Berlaku Kontrak">
                                                                            <i class="icon-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label class="col-sm-12 control-label">Deskripsi {{@$label}}</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="col-sm-12 prepend-icon">
                                                            <textarea disabled class="form-control form-white" rows="4"
                                                                      name="deskripsi_kontrak"
                                                                      placeholder="Deskripsi Kontrak">{{($latest != null) ? $latest->uraian : ''}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-sm-4">
                                                                    <label class="col-sm-12 control-label">Nilai {{@$label}}</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="col-sm-12 prepend-icon">
                                                                        <input disabled type="text" min="0"
                                                                               required="required"
                                                                               name="nilai_kontrak"
                                                                               class="form-control form-white"
                                                                               value="{{number_format(@$latest->nilai_kontrak, 0, '.', ',')}}"
                                                                               placeholder="Nilai Kontrak">
                                                                        <i class="glyphicon glyphicon-ok"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label class="col-sm-12 control-label">File {{@$label}}</label>
                                                                </div>
                                                                @if($latest != null && $latest->file != null)
                                                                    <div class="col-md-8">
                                                                        <a target="_blank" class="p-t-0"
                                                                           href="{{url('upload/'.$latest->file)}}"
                                                                        >
                                                                            <button type="button"
                                                                                    class="btn btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>VIEW
                                                                            </button>
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                                @if($kontrak_id == 0)
                                                                    <div class="col-md-8">
                                                                        <div class="col-sm-12 prepend-icon">
                                                                            <input type="file" required="required"
                                                                                   name="file_kontrak"
                                                                                   class="form-control form-white"
                                                                                   placeholder="File Kontrak">
                                                                            <i class="glyphicon glyphicon-paperclip"></i>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            @if(@$kontrak->sys_status != null && @$kontrak->sys_status != "")
                                                                <div class="form-group">
                                                                    <div class="col-sm-4">
                                                                        <label class="col-sm-12 control-label">Status</label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <div class="col-sm-12">
                                                                            <input type="text" disabled
                                                                                   class="form-control form-white"
                                                                                   value="{{@$kontrak->sys_status}}"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br/><br/>
                                                            @endif
                                                        </div>
                                                        <div class="tab-pane fade" id="amandemen_kontrak">
                                                            <table class="table table-bordered m-b-0">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nomor Kontrak</th>
                                                                    <th>Tanggal Amandemen</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Nilai Kontrak</th>
                                                                    <th>Amandemen ke</th>
                                                                    <th style="text-align:center">Aksi</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php $no = 1;?>
                                                                @foreach($kontrakVersion as $item)
                                                                    <tr>
                                                                        <td>{{$no}}</td>
                                                                        <td>{{@$item->nomor_kontrak}}</td>
                                                                        <td>{{date('d M Y',strtotime(@$item->tgl_kontrak))}}</td>
                                                                        <td>{{@$item->uraian}}</td>
                                                                        <td>{{number_format(@$item->nilai_kontrak, 0, '.', ',')}}</td>
                                                                        <td>{{@$item->version}}</td>
                                                                        <td style="text-align:center">
                                                                            <a href="{{url('/eksternal/detail_amandemen/'.@$item->id)}}">
                                                                                <button type="button"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                    <i
                                                                                            class="fa fa-eye"></i>
                                                                                </button>
                                                                            </a></td>
                                                                    </tr>
                                                                    <?php $no++;?>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end kontrak--}}
                        {!! Form::close() !!}
                        @if($is_last_approval)
                            @if($needPayment)
                                <div class="panel-group panel-accordion" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseThree">
                                                    <i class="icon-plus"></i> Pembayaran
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse">
                                            <div class="panel-body" style="background-color: white">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        @if(\App\User::isCurrUserAllowPermission(PERMISSION_CREATE_KONTRAK))
                                                            <button id="btn_tambah" type="button"
                                                                    class="btn btn-success btn-square btn-embossed">
                                                                <i class="fa fa-plus"></i> Tambah Termin
                                                            </button>
                                                        @endif
                                                        <table id="table_termin"
                                                               class="table table-hover table-dynamic">
                                                            <thead>
                                                            <tr>
                                                                <th>Nilai</th>
                                                                <th>Status</th>
                                                                <th>Invoice</th>
                                                                <th>Bukti Transfer</th>
                                                                <th>Bukti PPH23</th>
                                                                <th>Kwitansi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php $no = 1;?>
                                                            @foreach($pembayaran as $item)
                                                                <tr>
                                                                    <td>{{number_format(@$item->nilai, 0, '.', ',')}}</td>
                                                                    <td>{{@$item->status}}</td>
                                                                    <td>
                                                                        <input type="hidden" name="nilai"
                                                                               joss="{{@$item->nilai}}">
                                                                        @if(@$item->file_invoice != null && @$item->status != INVOICE_CREATED)
                                                                            <button type="button"
                                                                                    onclick="window.open('{{url('upload/'.@$item->file_invoice)}}','_blank')"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                INVOICE
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td style="white-space: nowrap">
                                                                        @if((@$item->status == INVOICE_SENT || @$item->status == BUKTI_UPLOADED) && Auth::user()->getRolesByName("PJ"))
                                                                            <button type="button" value="{{@$item->id}}"
                                                                                    onclick="modal_pembayaran({{@$item->id}})"
                                                                                    class="btn btn-sm btn-warning btn-square btn-embossed btn_upload">
                                                                                <i
                                                                                        class="fa fa-file-text"></i>
                                                                                UPLOAD
                                                                                BUKTI
                                                                            </button>
                                                                        @endif
                                                                        @if(@$item->bukti != null)
                                                                            <button type="button"
                                                                                    onclick="window.open('{{url('upload/'.@$item->bukti)}}','_blank')"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                BUKTI TRF
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if(@$item->bukti_pph != null)
                                                                            <button type="button"
                                                                                    onclick="window.open('{{url('upload/'.@$item->bukti_pph)}}','_blank')"
                                                                                    class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                <i
                                                                                        class="fa fa-download"></i>
                                                                                PPh 23
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                    <td style="white-space: nowrap">
                                                                        @if(@$item->status == KUITANSI_SENT || @$item->status == PAID)
                                                                            @if(@$item->file_kuitansi_transfer != null)
                                                                                <button type="button"
                                                                                        onclick="window.open('{{url('upload/'.@$item->file_kuitansi_transfer)}}','_blank')"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                    <i
                                                                                            class="fa fa-download"></i>
                                                                                    KWITANSI TRF
                                                                                </button>
                                                                            @endif
                                                                            @if(@$item->file_kuitansi_pph23 != null || @$item->status == PAID)
                                                                                <button type="button"
                                                                                        onclick="window.open('{{url('upload/'.@$item->file_kuitansi_pph23)}}','_blank')"
                                                                                        class="btn btn-sm btn-primary btn-square btn-embossed">
                                                                                    <i
                                                                                            class="fa fa-download"></i>
                                                                                    KUITANSI PPh 23
                                                                                </button>
                                                                            @endif
                                                                        @endif

                                                                        {{--@if(@$item->status == UNPAID && Auth::user()->getRolesByName("P-KEU"))--}}
                                                                        {{--<button type="button"--}}
                                                                        {{--onclick="window.location.href='{{url('internal/invoice_pembayaran/'.@$item->id)}}'"--}}
                                                                        {{--class="btn btn-sm btn-primary btn-square btn-embossed">--}}
                                                                        {{--<i--}}
                                                                        {{--class="fa fa-paper-plane"></i>--}}
                                                                        {{--SEND--}}
                                                                        {{--INVOICE--}}
                                                                        {{--</button>--}}
                                                                        {{--@elseif(@$item->status == BUKTI_UPLOADED && Auth::user()->getRolesByName("P-KEU"))--}}
                                                                        {{--<button type="button"--}}
                                                                        {{--onclick="window.location.href='{{url('internal/paid_pembayaran/'.@$item->id)}}'"--}}
                                                                        {{--class="btn btn-sm btn-success btn-square btn-embossed">--}}
                                                                        {{--<i--}}
                                                                        {{--class="fa fa-check"></i> PAID--}}
                                                                        {{--</button>--}}
                                                                        {{--@elseif(@$item->status == INVOICE_SENT && Auth::user()->getRolesByName("PMJ"))--}}
                                                                        {{--<button type="button" value="{{@$item->id}}"--}}
                                                                        {{--onclick="modal_pembayaran({{@$item->id}})"--}}
                                                                        {{--class="btn btn-sm btn-warning btn-square btn-embossed btn_upload">--}}
                                                                        {{--<i--}}
                                                                        {{--class="fa fa-file-text"></i>--}}
                                                                        {{--UPLOAD--}}
                                                                        {{--BUKTI--}}
                                                                        {{--</button>--}}
                                                                        {{--@else--}}
                                                                        {{--<p>-</p>--}}
                                                                        {{--@endif--}}
                                                                    </td>
                                                                </tr>
                                                                <?php $no++;?>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="panel-group panel-accordion" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseFour">
                                            <i class="icon-plus"></i> Jadwal Pelaksanaan
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse">
                                    <div class="panel-body" style="background-color: white">
                                        <div class="row">
                                            {!! Form::open(array('url'=> '/eksternal/save_jadwal_pelaksanaan', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                                            {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Tanggal Mulai</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="append-icon">
                                                            <input {{($kontrak->jadwal!=null)?'value='.$kontrak->jadwal->tanggal_mulai.' readonly':''}}
                                                                   name="tanggal_mulai" type="text" required
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en"
                                                                   data-RTL="false"
                                                                   placeholder="Tanggal">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Tanggal
                                                            selesai</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="append-icon">
                                                            <input {{($kontrak->jadwal!=null)?'value='.$kontrak->jadwal->tanggal_selesai.' readonly':''}}
                                                                   name="tanggal_selesai" type="text" required
                                                                   class="form-control b-datepicker form-white"
                                                                   data-date-format="dd-mm-yyyy"
                                                                   data-lang="en"
                                                                   data-RTL="false"
                                                                   placeholder="Tanggal">
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(Auth::user()->jenis_user == TIPE_EKSTERNAL)
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <p class="col-sm-12 control-label">*) Tanggal di atas tidak
                                                                dapat melewati masa berlaku kontrak</p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">File Jadwal</label>
                                                    </div>
                                                    @if($kontrak->jadwal != null && $kontrak->jadwal->file_jadwal != null)
                                                        <div class="col-sm-1">
                                                            <a target="_blank"
                                                               href="{{url('upload/'.$kontrak->jadwal->file_jadwal)}}"
                                                            >
                                                                <button type="button"
                                                                        class="btn btn-primary"><i
                                                                            class="fa fa-download"></i>View
                                                                </button>
                                                            </a>
                                                        </div>
                                                    @else
                                                        <div class="col-sm-8">
                                                            <div class="append-icon">
                                                                <input name="file_jadwal" type="file"
                                                                       required="required"
                                                                       class="form-control form-white"
                                                                       placeholder="File Kontrak">
                                                                <i class="glyphicon glyphicon-paperclip"></i>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <label class="col-sm-12 control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <div class="append-icon">
                                                             <textarea name="keterangan" class="form-control form-white"
                                                                       {{($kontrak->jadwal!=null)?'readonly':''}}
                                                                       rows="4"
                                                                       placeholder="Keterangan">{{($kontrak->jadwal!=null)?$kontrak->jadwal->keterangan:''}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bg-white pull-right">
                                                    @if($kontrak->jadwal == null)
                                                        <button type="submit"
                                                                class="btn btn-success ladda-button btn-square btn-embossed"
                                                                data-style="zoom-in">Simpan &nbsp;<i
                                                                    class="glyphicon glyphicon-floppy-saved"></i>
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @include('workflow_view')
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-termin" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        {!! Form::hidden('status', UNPAID) !!}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Termin</label></div>
                                    <div class="col-md-9"><input type="number" min="1" placeholder="Termin"
                                                                 name="termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Presentase</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Persentase"
                                                                 name="persentase_termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nilai</label></div>
                                    <div class="col-md-9"><input type="number" min="0" placeholder="Nilai"
                                                                 name="nilai_termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-termin" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        {!! Form::hidden('status', UNPAID) !!}
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Termin</label></div>
                                    <div class="col-md-9"><input type="number" min="1" placeholder="Termin"
                                                                 name="termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Presentase</label></div>
                                    <div class="col-md-9"><input type="text" placeholder="Persentase"
                                                                 name="persentase_termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label>Nilai</label></div>
                                    <div class="col-md-9"><input type="number" min="0" placeholder="Nilai"
                                                                 name="nilai_termin"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-upload" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Upload</strong> Bukti Trasfer</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '/eksternal/save_bukti_pembayaran', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        <input type="hidden" name="pembayaran_id" id="pembayaran_id">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-4"><label>File Pembayaran</label></div>
                                    <div class="col-md-8"><input type="file"
                                                                 name="bukti"
                                                                 class="form-control form-white" required></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4"><label>File PPH 23</label></div>
                                    <div class="col-md-8"><input type="file"
                                                                 name="bukti_pph"
                                                                 class="form-control form-white" required></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Upload
                                    </button>
                                    <button class="btn btn-warning btn-square btn-embossed pull-right"
                                            data-dismiss="modal">
                                        <i class="fa fa-close"></i> Close
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('page_script')
            <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
            <!-- Tables Filtering, Sorting & Editing -->
            <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
            <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <!-- Buttons Loading State -->
            <script>
                $("#btn_tambah").on('click', function () {
                    $("#modal-termin").modal("show");
                });

                function modal_pembayaran(pembayaran_id) {
                    $("#modal-upload").modal("show");
                    $('#pembayaran_id').val(pembayaran_id);
                }

                $("input").unbind('keydown');

                $('input[name=tanggal_mulai]').change(function () {
                    compareDate('tanggal_mulai', 'masa_berlaku_kontrak');
                });

                $('input[name=tanggal_selesai]').change(function () {
                    compareDate('tanggal_selesai', 'masa_berlaku_kontrak');
                });

                function compareDate(date_a, date_b) {
                    var start_dt = $('input[name=' + date_a + ']').val().split("-");
                    var new_tgl_mulai = new Date(start_dt[2], start_dt[1] - 1, start_dt[0]);

                    var mb_kontrak = $('input[name=' + date_b + ']').val().split("-");
                    var new_masa_berlaku_kontrak = new Date(mb_kontrak[2], mb_kontrak[1] - 1, mb_kontrak[0]);

                    if (new Date(new_masa_berlaku_kontrak) < new Date(new_tgl_mulai)) {
                        alert('Tanggal mulai pekerjaan tidak dapat melebihi masa berlaku kontrak!');
                        $('input[name=' + date_a + ']').val('');
                    }
                }
            </script>
@endsection