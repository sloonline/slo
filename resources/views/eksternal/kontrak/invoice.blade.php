@extends('../layout/layout_eksternal')
@section('page_css')
    <style>
        @media print {
            .btn {
                display: none !important;
            }

            .copyright {
                display: none !important;
            }

            .control-btn {
                display: none !important;
            }

            hr{
                display: none !important;
            }
        }
    </style>
@endsection
@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Invoice</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li><a href="{{url('/eksternal/kontrak')}}">Kontrak</a></li>
                    <li class="active">Invoice</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel border">
                    <div class="panel-content">
                        <button type="button" class="btn btn-primary btn-square btn-embossed m-r-10 m-b-10"  onclick="return window.print()"><i class="glyphicon glyphicon-print m-r-5"></i> PRINT INVOICE</button>
                        <button type="button" class="btn btn-warning btn-square btn-embossed m-r-10 m-b-10"><i class="fa fa-file-text m-r-5"></i> EXPORT PDF</button>
                        <button type="button" class="btn btn-blue btn-square btn-embossed m-r-10 m-b-10"><i class="fa fa-envelope m-r-5"></i> SEND EMAIL</button>
                        @if($pembayaran->status == INVOICE_SENT)
                            <button class="btn btn-default btn-square btn-embossed" value="{{$pembayaran_id}}"
                                    onclick="modal_pembayaran({{$pembayaran_id}})"><i
                                        class="fa fa-paper-plane btn_upload m-r-5"></i>
                                SUBMIT PEMBAYARAN
                            </button>
                        @endif
                        <hr style="margin-top: 0px !important; margin-bottom: 0px !important;">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="w-500 c-blue f-20 p-b-10" style="text-align: center"><strong>INVOICE TERMIN KONTRAK</strong></h3>
                                <div class="pull-left">
                                    <h4 class="w-500 c-gray f-20 p-b-10"><strong>DARI</strong></h4>
                                    <address>
                                        <p class="width-300 m-t-10"><strong>PT. PLN (Persero) Pusat Sertifikasi</strong>
                                        <br>Jalan Laboratorium No. 9, Duren Tiga
                                        <br>Jakarta Selatan, 12760
                                        <br><abbr title="Phone">Telp :</abbr> (021) 7900034</p>
                                    </address>
                                </div>
                                <div class="pull-right">
                                    <h4 class="w-500 c-gray f-20 p-b-10"><strong>KEPADA</strong></h4>
                                    <address>
                                        <p class="width-300 m-t-10"><strong>{{$user->nama_user}}</strong>
                                        <br>{{$user->alamat_user}}
                                        <br>Nama Kota / Kabupaten, Kode Pos
                                        <br><abbr title="Phone">Telp :</abbr> {{$user->no_hp_user}}</p>
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 m-t-20 m-b-20">
                                        <p><strong>ORDER DATE :</strong> <span>{{ date('d F Y',strtotime($order->created_at ))}}</span></p>
                                    </div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 100px;" class="unseen text-center">TERMIN KE</th>
                                        <th class="text-center">PRESENTASE</th>
                                        <th class="text-center">NILAI KONTRAK</th>
                                        <th class="text-right">TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="item-row">
                                        <td class="delete-wpr">
                                            <p class="qty text-center">{{$pembayaran->termin_ke}}</p>
                                        </td>
                                        <td>
                                            <div class="text-primary">
                                                <p class="text-center"><strong>{{$pembayaran->persentase}} %</strong></p>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="text-center cost">{{\App\Money::rupiah($kontrak->nilai_kontrak)}}</p>
                                        </td>
                                        <td class="text-right price">{{\App\Money::rupiah($pembayaran->nilai)}}</td>
                                    </tr>
                                    <tr>
                                        <td class="thick-line text-right" colspan="3"><strong>SUBTOTAL</strong></td>
                                        <td class="text-right" id="total">{{\App\Money::rupiah($pembayaran->nilai)}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="well bg-white">
                                    Terimakasih atas kerja samanya. Mohon untuk segera melakukan pembayaran ke <strong>PT. PLN (Persero) Pusat Sertifikasi</strong>. Nomor Rekening. 35757745
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-upload" style="margin-top: -100px;" tabindex="-1"
             role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: teal;color:white;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Termin</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(array('url'=> '/eksternal/save_bukti_pembayaran', 'files'=> true, 'class'=> 'form-horizontal')) !!}
                        {!! Form::hidden('kontrak_id', $kontrak_id) !!}
                        <input type="hidden" name="pembayaran_id" id="pembayaran_id">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-3"><label>Termin</label></div>
                                    <div class="col-md-9"><input type="file"
                                                                 name="bukti"
                                                                 class="form-control form-white"></div>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-square btn-embossed pull-right">
                                        <i class="fa fa-save"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('page_script')
    <script>
        function modal_pembayaran(pembayaran_id) {
            $("#modal-upload").modal("show");
            $('#pembayaran_id').val(pembayaran_id);
        }
    </script>
@endsection