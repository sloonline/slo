@extends('../layout/layout_eksternal')

@section('page_css')
    <link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="page-content">
        <div class="header">
            <h2>Data <strong>Kontrak</strong></h2>
            <div class="breadcrumb-wrapper">
                <ol class="breadcrumb">
                    <li><a href="{{url('/eksternal')}}">Dashboard</a></li>
                    <li class="active">Kontrak</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <i class="glyphicon glyphicon-check"></i> {{Session::get('message')}}
                    </div>
                @endif
                <div class="panel border">
                    <div class="panel-content pagination2 table-responsive">
                        <table id="my_table" class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Kontrak</th>
                                <th>Tanggal Kontrak</th>
                                <th>Deskripsi</th>
                                <th>Instalasi</th>
                                <th>Nilai Kontrak</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($kontrak) && (is_array($kontrak) || is_object($kontrak)))
                                <?php $no = 1; ?>
                                @foreach($kontrak as $item)
                                    <tr>
                                        <td>{{ $no }}</td>
                                        <td>{{ @$item->latest_kontrak->nomor_kontrak }}</td>
                                        <td>{{ date('d-M-Y',strtotime(@$item->latest_kontrak->tgl_kontrak ))}}</td>
                                        <td>{{ @$item->latest_kontrak->uraian }}</td>
                                        <td>
                                            <ul style="list-style-type: none;">
                                                @foreach(@$item->nama_instalasi as $row)
                                                    <li>{{$row}}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>{{ @$item->latest_kontrak->nilai_kontrak }}</td>
                                        <td>{{ @$item->flow_status->status_eksternal }}</td>
                                        <td>
                                            <a data-rel="tooltip" data-placement="top" title="Detail Kontrak"
                                            href="{{url('/eksternal/detail_kontrak/'.@$item->id)}}"
                                               class="btn btn-sm btn-primary btn-square btn-embossed"><i
                                                        class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('page_script')
    <script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- Tables Filtering, Sorting & Editing -->
    <script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
    <script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
    <!-- Buttons Loading State -->

    <script>
        $(document).ready(function () {
            var oTable = $('#my_table').dataTable();

            // Sort immediately with columns 0 and 1
            oTable.fnSort([[2, 'desc']]);
        });
    </script>
@endsection