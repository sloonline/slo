@extends('../layout/layout_eksternal')


@section('content')
<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-wizard">
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header md-panel-controls">
                    <h3><i class="icon-bulb"></i> Input <strong>Pemanfaatan Tenaga Listrik</strong></h3>
                </div>
                <div class="panel-content">
                    <p>Silahkan masukan data pemohon dan detail pemnanfaatan tenaga listrik yang akan diinspeksi.</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label"><h2><b>Data Pemilik Instalasi</b></h2></label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama Pemilik Instalasi</label>
                                <input type="text" class="form-control" placeholder="Nama pemilik">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Lokasi</label>
                                <input type="text" class="form-control" placeholder="Lokasi">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Wilayah Kerja</label>
                                <input type="text" class="form-control" placeholder="Wilayah kerja">
                            </div>
                            <div class="form-group">
                                <label class="form-label"><h2><b>Data Pemanfaatan</b></h2></label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Provinsi</label>
                                <select class="form-control" placeholder="Pilih provinsi">
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kota/Kabupaten</label>
                                <select class="form-control" placeholder="Pilih kota/kabupaten">
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jenis Instalasi Pemanfaatan</label>
                                <select class="form-control" placeholder="Jenis instalasi pemanfaatan">
                                    <option>Bangunan perkantoran: perkantoran pemerintah, niaga, laboratorium dan
                                        sejenisnya
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Jenis Usaha Penyedia Tenaga Listrik</label>
                                <select class="form-control" placeholder="Jenis usaha penyedia tenaga listrik">
                                    <option>IO</option>
                                    <option>Pelanggan</option>
                                    <option>PIUPTL</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Pasang Baru</div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-3">Tambah Daya</div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-2">kVA ke</div>
                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="col-lg-1">kVA</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label"><h2><b>Surat Perjanjian Jual Beli Tenaga Listrik</b></h2>
                                </label><br/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nomor</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" placeholder="Nomor"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Tanggal</label>
                                <div class="row">
                                    <div class="col-lg-7">
                                        <input type="date" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jual Beli/SIT/ID Pelanggan</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Single Line Diagram</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Gambar Instalasi dan Tata Letak</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Jenis dan Kapasitas Instalasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Spesifikasi Peralatan Utama Instalasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File Ijin Operasi</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File NPWP</label>
                                <input type="file" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label class="form-label">File PKP</label>
                                <input type="file" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                        <button class="btn btn-success" type="submit" name="save"><i class="fa fa-save"></i> Simpan
                        </button>
                    </div>
                    </br> </br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page_script')


@endsection