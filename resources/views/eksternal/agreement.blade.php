@extends('../layout/layout_eksternal')

@section('page_css')
<link href="{{ url('/') }}/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content">
  <div class="header">
	<h2>AGREE<strong>MENT</strong></h2>
	<div class="breadcrumb-wrapper">
	  <ol class="breadcrumb">
		<li><a href="{{url('/internal')}}">Dashboard</a></li>
		<li><a href="#">Permohonan SLO</a></li>
		<li class="active">Agreement</li>
	  </ol>
	</div>
  </div>
  <div class="row">
	<div class="col-lg-12 portlets">
	  <p class="m-t-10 m-b-20 f-16">Silahkan inputkan data pemohon dan data trasnmisi yang hendak diajukan dalam permohonan ini.</p>
	  <div class="panel">
		<div class="panel-header panel-controls bg-primary">
		  <h3><i class="fa fa-table"></i> AGREE<strong>MENT</strong></h3>
		</div>
		<div class="panel-content">
			<div class="row">
				<div class="col-md-12">
				  <div class="text-center">
					<h3><strong>Pernyataan Persetujuan</strong></h3>
				  </div>
				  <hr>
				  <p>Inline Editing allows you to select any editable element on the page and edit it in-place. As a result, the editor can be used to edit pages that look just like the final page.</p>
					<p>Armstrong spent about two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third
					  member of the mission, <a href="http://en.wikipedia.org/wiki/Michael_Collins_(astronaut)" title="Michael Collins (astronaut)">Michael Collins</a>, piloted the <a href="http://en.wikipedia.org/wiki/Apollo_Command/Service_Module"
						title="Apollo Command/Service Module">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.
					</p>
				  <p>1. Inline Editing allows you to select any editable element on the page and edit it in-place</p>
				  <p>2. Inline Editing allows you to select any editable element on the page and edit it in-place</p>
				  <p>3. Inline Editing allows you to select any editable element on the page and edit it in-place</p>
				  <p>4. Inline Editing allows you to select any editable element on the page and edit it in-place</p>
				  <p>5. Inline Editing allows you to select any editable element on the page and edit it in-place</p>
				  <br>
				  <div class="input-group">
					<div class="icheck-list">
					  <label><input type="checkbox" data-checkbox="icheckbox_square-blue"> Saya Setuju Dengan Pernyataan Diatas</label>
					</div>
				  </div>
				  <br>
				</div>
			</div>
		</div>
		<div class="panel-footer clearfix">
		  <div class="text-center">
			<a class="btn btn-warning btn-square" href="{{url('eksternal')}}">Kembali</a>
			<a class="btn btn-success btn-square" href="{{url('eksternal/view_order')}}">Lanjut</a>
		  </div>
		</div>
	  </div>
	</div>
  </div>
@endsection


@section('page_script')
<script src="{{ url('/') }}/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('/') }}/assets/global/js/pages/table_dynamic.js"></script>
<script src="{{ url('/') }}/assets/global/plugins/bootstrap-loading/lada.min.js"></script> <!-- Buttons Loading State -->
@endsection