<div class="panel-content p-t-0">
    <ul class="nav nav-tabs nav-primary">
        <li class="active"><a href="#general-instalasi" data-toggle="tab"><i
                        class="icon-info"></i>
                GENERAL</a></li>
        <li><a href="#kapasitas" data-toggle="tab"><i
                        class="icon-speedometer"></i> KAPASITAS</a></li>
        <li><a href="#pemilik" data-toggle="tab"><i class="icon-user"></i>
                PEMILIK</a></li>
        <li><a href="#lokasi" data-toggle="tab"><i class="icon-map"></i> LOKASI</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="general-instalasi">
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Status Instalasi</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white"
                           value="{{($instalasi != null)? (($instalasi->status_baru == 1) ? "INSTALASI BARU" : "INSTALASI LAMA") :""}}"
                           name="nama_instalasi" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Jenis Instalasi</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white"
                           value="{{($instalasi != null)? $instalasi->jenis_instalasi->jenis_instalasi : ""}}"
                           name="jenis_instalasi" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Nama Instalasi</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white"
                           value="{{($instalasi != null)? $instalasi->nama_instalasi : ""}}" name="nama_instalasi"
                           readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Panjang Saluran</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="panjang_saluran"
                           value="{{($instalasi != null)? $instalasi->panjang_kms : ""}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Jumlah Tiang</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="jumlah_tiang"
                           value="{{($instalasi != null)? $instalasi->jumlah_tiang : ""}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Jumlah Gardu Distribusi</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="jumlah_gardu_distribusi"
                           value="{{($instalasi != null)? $instalasi->jml_gardu : ""}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Jumlah Panel</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="jumlah_panel"
                           value="{{($instalasi != null)? $instalasi->jumlah_panel : ""}}" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Tegangan Pengenal</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="tegangan_pengenal"
                           value="{{($instalasi->teganganPengenal != null)? $instalasi->teganganPengenal->nama_reference : ""}}"
                           readonly="">
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="kapasitas">
            <!-- kapasita instalasi -->
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Kapasitas Gardu Distribusi</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="kapasitas_gardu_distribusi"
                           value="{{($instalasi != null)? $instalasi->kapasitas_gardu : ""}}" readonly="">

                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label class="col-sm-12 control-label">Kapasitas Arus Hubung Singkat</label>
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control form-white" name="kapasitas_arus_hubung_singkat"
                           value="{{($instalasi != null)? $instalasi->kapasitas_arus_hubung_singkat : ""}}" readonly="">

                </div>
            </div>
            <!-- end kapasitas instalasi -->
        </div>
        <div class="tab-pane fade" id="pemilik">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="panel border">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title">Pemilik <strong>Instalasi</strong></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Nama Pemilik</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="nama_pemilik" name="nama_pemilik" placeholder="Nama Pemilik" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->nama_pemilik : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Alamat Instalasi</label>
                                </div>
                                <div class="col-sm-9">
                                                    <textarea id="alamat_pemilik" name="alamat_pemilik" rows="3"
                                                              class="form-control form-white"
                                                              placeholder="Alamat Instalasi..."
                                                              readonly="">{{($instalasi->pemilik != null) ? $instalasi->pemilik->alamat_pemilik : null}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Provinsi</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="provinsi" name="provinsi" placeholder="Provinsi" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik->province != null) ? $instalasi->pemilik->province->province : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Kabupaten / Kota</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="kabupaten" name="kabupaten" placeholder="Kabupaten / Kota" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik->city != null) ? $instalasi->pemilik->city->city : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Kode Pos</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="kode_pos" name="kode_pos" type="text" class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->kode_pos_pemilik : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Telepon</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="telepon" name="telepon" type="text" class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->telepon_pemilik : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">No Fax</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="no_fax" name="no_fax" type="text" class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_fax_pemilik : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Email</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="email_pemilik" name="email_pemilik" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->email_pemilik : null}}"
                                           readonly="">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="panel border">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title">Ijin <strong>Usaha</strong></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Jenis Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="jenis_ijin_usaha" name="jenis_ijin_usaha" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->jenisIjinUsaha->nama_reference : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Penerbit Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="penerbit_ijin_usaha" name="penerbit_ijin_usaha" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->penerbit_ijin_usaha : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">No Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="no_ijin_usaha" name="no_ijin_usaha" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_ijin_usaha : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Masa Berlaku IU</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="masa_berlaku_iu" name="masa_berlaku_iu" type="text"
                                           class="form-control date-picker form-white"
                                           data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                           value="{{($instalasi->pemilik->masa_berlaku_iu != null) ? $instalasi->pemilik->masa_berlaku_iu->format('d F Y') : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">File Surat Ijin Usaha</label>
                                </div>
                                <div class="col-sm-9">
                                    @if($instalasi->pemilik->file_siup != null)
                                        <a href="{{url('upload/'.$instalasi->pemilik->file_siup)}}" target="_blank">
                                            <i class="fa fa-download"></i> {{$instalasi->pemilik->file_siup}}
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Nama Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="nama_kontrak" name="nama_kontrak" type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->nama_kontrak : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Nomor Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="no_kontrak" name="no_kontrak" type="text" class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_kontrak : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Tanggal Pengesahaan Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="tgl_pengesahan_kontrak" name="tgl_pengesahan_kontrak" type="text"
                                           class="form-control date-picker form-white" data-format="yyyy-mm-dd"
                                           data-lang="en" data-RTL="false"
                                           value="{{($instalasi->pemilik->tgl_pengesahan_kontrak != null) ? $instalasi->pemilik->tgl_pengesahan_kontrak->format('d F Y') : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Masa Berlaku Kontrak</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="masa_berlaku_kontrak" name="masa_berlaku_kontrak" type="text"
                                           class="form-control date-picker form-white" data-format="yyyy-mm-dd"
                                           data-lang="en" data-RTL="false"
                                           value="{{($instalasi->pemilik->masa_berlaku_kontrak != null) ? $instalasi->pemilik->masa_berlaku_kontrak->format('d F Y') : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">File Kontrak Sewa</label>
                                </div>
                                <div class="col-sm-9">
                                    @if($instalasi->pemilik->file_kontrak != null)
                                        <a href="{{url('upload/'.$instalasi->pemilik->file_kontrak)}}" target="_blank">
                                            <i class="fa fa-download"></i> {{$instalasi->pemilik->file_kontrak}}
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Nomor SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="no_spjbtl" name="no_spjbtl" type="text" class="form-control form-white"
                                           value="{{($instalasi->pemilik != null) ? $instalasi->pemilik->no_spjbtl : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Tanggal SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="tgl_spjbtl" name="tgl_spjbtl" type="text"
                                           class="form-control date-picker form-white"
                                           data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"
                                           value="{{($instalasi->pemilik->tgl_spjbtl != null) ? $instalasi->pemilik->tgl_spjbtl->format('d F Y') : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">Masa Berlaku SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="masa_berlaku_spjbtl" name="masa_berlaku_spjbtl" type="text"
                                           class="form-control date-picker form-white" data-format="yyyy-mm-dd"
                                           data-lang="en" data-RTL="false"
                                           value="{{($instalasi->pemilik->masa_berlaku_spjbtl != null) ? $instalasi->pemilik->masa_berlaku_spjbtl->format('d F Y') : null}}"
                                           readonly="">


                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="control-label">File SPJBTL</label>
                                </div>
                                <div class="col-sm-9">
                                    @if($instalasi->pemilik->file_spjbtl != null)
                                        <a href="{{url('upload/'.$instalasi->pemilik->file_spjbtl)}}" target="_blank">
                                            <i class="fa fa-download"></i> {{$instalasi->pemilik->file_spjbtl}}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="lokasi">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="panel border">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title">LOKASI <strong>(AWAL)</strong></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                </div>
                                <div class="col-sm-9">
                                        <textarea rows="3" class="form-control form-white" name="alamat_instalasi"
                                                  placeholder="Alamat Instalasi..."
                                                  readonly=""> {{($instalasi != null)? $instalasi->alamat_instalasi : ""}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Provinsi</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                           type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->provinsi != null) ? $instalasi->provinsi->province : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                           type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->kota != null) ? $instalasi->kota->city : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Longitude</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{($instalasi != null)? $instalasi->longitude_awal : ""}}"
                                           name="longitude_awal" readonly="">
                                    {{--<i class="fa fa-credit-card"></i>--}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Latitude</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{($instalasi != null)? $instalasi->latitude_awal : ""}}"
                                           name="latitude_awal" readonly="">
                                    {{--<i class="fa fa-credit-card"></i>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="panel border">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title">LOKASI <strong>(AKHIR)</strong></h2>
                        </div>
                        <div class="panel-body bg-white">

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Alamat Instalasi</label>
                                </div>
                                <div class="col-sm-9">
                                        <textarea rows="3" class="form-control form-white" name="alamat_instalasi"
                                                  placeholder="Alamat Instalasi..."
                                                  readonly=""> {{($instalasi != null)? $instalasi->alamat_akhir_instalasi : ""}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Provinsi</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                           type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->provinsi_akhir != null) ? $instalasi->provinsi_akhir->province : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Kabupaten / Kota</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="provinsi_instalasi" name="provinsi_instalasi" placeholder="Provinsi"
                                           type="text"
                                           class="form-control form-white"
                                           value="{{($instalasi->kota_akhir != null) ? $instalasi->kota_akhir->city : null}}"
                                           readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Longitude</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{($instalasi != null)? $instalasi->longitude_akhir : ""}}"
                                           name="longitude_akhir" readonly="">
                                    {{--<i class="fa fa-credit-card"></i>--}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label class="col-sm-12 control-label">Latitude</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-white"
                                           value="{{($instalasi != null)? $instalasi->latitude_akhir : ""}}"
                                           name="latitude_akhir" readonly="">
                                    {{--<i class="fa fa-credit-card"></i>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>