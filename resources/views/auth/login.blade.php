@extends('../layout/layout_front')

@section('header')
<div id="header" class="sticky clearfix">
	@endsection

	@section('logo')
	<!-- Logo -->
	<a class="logo pull-left" href="{{url('/')}}#slider">
		<img src="{{ url('/') }}/assets_front/images/logo_dark.png" alt="" />
	</a>
	@endsection

	@section('content')
	<!-- -->
        <section>
            <div class="container">

                <div class="row">

                    <!-- IMPORTANT INFORMATION -->
                    <div class="col-md-4 col-md-offset-1">
                        <form action="#" method="post" class="sky-form boxed">

                            <header class="size-18 margin-bottom-20">
                                <i class="fa fa-info-circle"></i> IMPORTANT INFORMATION
                            </header>

                            <fieldset class="nomargin">

                                <div class="row">

                                    <div class="col-md-12">
                                        <ul class="list-unstyled list-icons">
                                            <li><i class="fa fa-info-circle text-info"></i> <strong>User PLN</strong></li>
                                                <p class="text-muted">Menggunakan domain dan user email PLN<br>
                                                contoh : pusat\username</p>
                                            <li><i class="fa fa-info-circle text-info"></i> <strong>User Peminta Jasa</strong></li>
                                                <p class="text-muted">Menggunakan email user peminta jasa<br>
                                                contoh : user@pemintajasa.com</p>
                                        </ul>
                                    </div>
                                </div>

                            </fieldset>

                        </form>

                    </div>
                    <!-- /IMPORTANT INFORMATION -->

                    <div class="col-md-6">

                        <!-- ALERT -->
                        @if (Session::has('status'))
                            <div class="alert alert-mini alert-danger margin-bottom-30">
                                <strong>{{Session::get('status')}}</strong>
                            </div>
                        @endif
                        <!-- /ALERT -->

                        <div>

                            {!! Form::open(['url'=>'auth/login','id'=>'form_login', 'class'=>'sky-form boxed', 'role'=>'form', 'autocomplete'=>'off']) !!}
                                <header class="size-18 margin-bottom-20">
                                    <i class="fa fa-unlock-alt"></i> LOGIN
                                </header>

                                <fieldset class="nomargin">
                                    <label class="input margin-bottom-10">
                                        <i class="ico-append fa fa-user"></i>
                                        <input required type="text" name="username" placeholder="Username">
                                        <b class="tooltip tooltip-bottom-right">Input username anda disini</b>
                                    </label>

                                    <label class="input margin-bottom-10">
                                        <i class="ico-append fa fa-lock"></i>
                                        <input required type="password" name="password" placeholder="Password">
                                        <b class="tooltip tooltip-bottom-right">Input password anda disini</b>
                                    </label>
                                    {{--<div class="clearfix note margin-bottom-30">--}}
                                        {{--<label class="checkbox weight-300">--}}
                                            {{--<input type="checkbox" name="remember">--}}
                                            {{--<i></i> Remember me--}}
                                        {{--</label>--}}
                                    {{--</div>--}}

                                </fieldset>

                                <footer>

                                    <button type="submit" class="btn btn-primary noradius pull-right"><i class="fa fa-check"></i> LOGIN</button>

                                </footer>

                            {!! Form::close() !!}

                        </div>

                        <div class="margin-top-10 text-center">

                            <a href="{{ url('register') }}">Belum Punya Akun ? | <strong>Registrasi Disini</strong></a>

                        </div>

                    </div>
                </div>

            </div>
        </section>

        <br>
        <br>
    <!-- / -->
	@endsection