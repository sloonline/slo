@extends('../layout/layout_login')

@section('content')
    <div class="container2" id="login-block">
        <div class="row">
            <br/>
            <div class="col-sm-12">
                <center>
                    <h2>Registrasi berhasil dilakukan<br>Mohon menunggu konfirmasi terkait verifikasi data melalui email.</h2>
                </center>
                <hr/>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="pull-right">
                        <a href="{{url('/auth/login')}}" id="submit-form" class="btn btn-lg btn-success ladda-button">
                            Selesai
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection