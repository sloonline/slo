'@extends('../layout/layout_login')

@section('content')
    <body class="sidebar-top account2 signup" data-page="signup">
    <!-- BEGIN LOGIN BOX -->
    <div class="container2" id="login-block">
        <i class="user-img icons-faces-users-03"></i>

        <div class="account-form">
            <div class="form-header">
                <h3><strong>Registrasi Pusertif</strong></h3>
            </div>
            <h4>Data <strong>Bidang</strong></h4>
            <hr/>
            {!! Form::open(['url'=>'auth/register','id'=>'form_register', 'class'=>'form-signup', 'role'=>'form','method'=>'POST', 'files'=>true]) !!}
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Status Pekerja</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <select name="status_pekerja" id="status_pekerja" class="form-control form-white" data-style="white"
                                data-placeholder="Pilih status pekerja...">
                            <option value="ORGANIK">Organik PLN Jaser</option>
                            <option value="OS">Non-organik/OS PLN Jaser</option>
                            <option value="KOORDINATOR">Koordinator Perwakilan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Area Bidang</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <select name="id_bidang" id="id_bidang" required="required" class="form-control form-white"
                                data-style="white" data-placeholder="Pilih bidang...">
                            @foreach($bidang as $row)
                                <option value="{{$row->id}}">{{$row->nama_bidang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Sub Bidang</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <select name="id_sub_bidang" id="sub_bidang" required="required"
                                class="form-control form-white" data-style="white"
                                data-placeholder="Pilih Sub Bidang...">
                            @foreach($sub_bidang as $row)
                                <option value="{{$row->id}}" class="{{$row->id_bidang}}">{{$row->nama_sub_bidang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <h4>Data <strong>User</strong></h4>
            <hr/>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Nama Lengkap</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="nama_user" id="nama_user"
                               class="form-control form-white lastname"
                               placeholder="Nama Lengkap">
                        <i class="icon-user"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>NIP/User ID</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="nip_user" id="nip_user"
                               class="form-control form-white lastname"
                               placeholder="NIP/User ID">
                        <i class="icon-tag"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Email</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="email" required="required" id="email" name="email" id="lastname"
                               class="form-control form-white lastname" placeholder="Email">
                        <i class="icon-envelope"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Tempat Lahir</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="tempat_lahir_user" id="tempat_lahir_user"
                               class="form-control form-white lastname" placeholder="Tempat Lahir...">
                        <i class="icon-direction"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Tanggal Lahir</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="tanggal_lahir" id="tanggal_lahir"
                               class="date-picker form-control form-white" placeholder="Tanggal Lahir..."/>
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Alamat</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                            <textarea rows="3" required="required" name="alamat_user" id="alamat_user"
                                      class="form-control form-white"
                                      placeholder="Alamat Lengkap..."></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>No HP</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="no_hp_user" id="no_hp_user"
                               class="form-control form-white lastname" placeholder="No HP..">
                        <i class="icon-call-in"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Jenis Kelamin</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <select name="gender_user" id="gender_user" class="form-control form-white"
                                data-style="white" data-placeholder="Jenis Kelamin...">
                            <option value="PRIA">Pria</option>
                            <option value="WANITA">Wanita</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Grade</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="grade_user" id="grade_user"
                               class="form-control form-white lastname" placeholder="Grade..">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Jabatan</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <input type="text" required="required" name="jabatan_user" id="jabatan_user"
                               class="form-control form-white lastname" placeholder="Jabatan..">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Pendidikan Terakhir</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="append-icon">
                        <select name="grade_user" id="grade_user" class="form-control form-white" data-style="white"
                                data-placeholder="Pendidikan Terakhir...">
                            <option value="S3">S3</option>
                            <option value="S2">S2</option>
                            <option value="S1">S1</option>
                            <option value="D4">D4</option>
                            <option value="D3">D3</option>
                            <option value="D1">D1</option>
                            <option value="SMA/SMK/Sederajat">SMA/SMK/Sederajat</option>
                        </select>
                        <i class="icon-graduation"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Status Pernikahan</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-5"><input type="radio" name="status_pernikahan_user"
                                                     id="status_pernikahan_user" checked="checked" value="MENIKAH"/>
                            Menikah
                        </div>
                        <div class="col-sm-5"><input type="radio" name="status_pernikahan_user"
                                                     id="status_pernikahan_user" value="SINGLE"/> Single
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="append-icon">
                        <label>Foto</label>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="file">
                        <div class="option-group">
                            <span class="file-button btn-primary">Choose File</span>
                            <input type="file" name="file_foto_user" id="file_foto_user" class="custom-file"
                                   name="avatar" id="avatar" accept="image/*"
                                   onchange="document.getElementById('uploader').value = this.value;" required>
                            <input type="text" class="form-control form-white" id="uploader"
                                   placeholder="no file selected" readonly="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-9">
                    <img id="preview_img" src="#" alt="foto" width="200"/>
                </div>
            </div>
            <hr/>
            <div class="row">
                <button type="submit" id="submit-form" class="btn btn-lg btn-success ladda-button"
                        data-style="expand-left">Registrasi
                </button>
                <a href="{{ url('auth/login') }}" id="submit-form" class="btn btn-lg btn-danger ladda-button"
                   data-style="expand-left">Cancel
                </a>
            </div>
            {!! Form::close() !!}
            <div class="form-footer">
                <div class="clearfix">
                    <h5><a href="{{ url('auth/login') }}">Sudah Punya Akun ? | <strong>Login Disini</strong></a></h5>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('page_script')
        <script src="../assets/global/plugins/jquery/jquery.chained.min.js"></script>
        <script type="text/javascript">
            $('#preview_img').hide();
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview_img').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                    $('#preview_img').show();
                }
            }

            $("#file_foto_user").change(function () {
                $('#preview_img').hide();
                readURL(this);
            });
            $("#sub_bidang").chained("#id_bidang");
        </script>
@endsection