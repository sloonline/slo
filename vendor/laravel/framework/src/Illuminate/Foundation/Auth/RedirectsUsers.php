<?php

namespace Illuminate\Foundation\Auth;

use App\Http\Controllers\LogController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        $user = Auth::user();
        $url = "/home";
        $perusahaan = $user->perusahaan;
        if ($perusahaan == null) {
            $url = '/internal';
            //menampung seluruh permission yang dimiliki user.
            Session::put('user_permissions', User::getUserPermission($user));
        } else {
            $url ='/eksternal';
        }
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $url : '/';
    }
}
