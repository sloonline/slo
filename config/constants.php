<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/8/2016
 * Time: 1:58 PM
 */

//konstanta pemilik instalasi
define('MILIK_SENDIRI',  'MILIK SENDIRI');
define('TERDAFTAR',  'PERUSAHAAN TERDAFTAR');
define('BELUM_TERDAFTAR',  'PERUSAHAAN BELUM TERDAFTAR');

//konstanta modul
define('MODUL_ORDER',  'ORDER');
define('MODUL_RAB',  'RAB');
define('MODUL_KONTRAK',  'KONTRAK');
define('MODUL_WBS_IO_NORMAL',  'WBS_IO_NORM');
define('MODUL_WBS_IO_EMERGENCY',  'WBS_IO_EMRG');
define('MODUL_PEKERJAAN',  'PEKERJAAN');
define('MODUL_PERMOHONAN_PERALATAN',  'PERMOHONAN_PERALATAN');
define('MODUL_PERMOHONAN_RLB', 'PERMOHONAN_RLB');
define('MODUL_RLB', 'RLB');
define('MODUL_PERMOHONAN_RLS', 'PERMOHONAN_RLS');
define('MODUL_RLS', 'RLS');
define('MODUL_PERMOHONAN', 'PERMOHONAN');


//konstanta model
define('MODEL_ORDER', 'Order');
define('MODEL_RAB', 'RAB');
define('MODEL_KONTRAK', 'Kontrak');
define('MODEL_WBS_IO', 'WbsIo');
define('MODEL_PEKERJAAN', 'Pekerjaan');
define('MODEL_PERMOHONAN_PERALATAN', 'PermohonanPeralatan');
define('MODEL_PERMOHONAN_RLB', 'PermohonanRlb');
define('MODEL_RLB', 'Rlb');
define('MODEL_PERMOHONAN_RLS', 'PermohonanRls');
define('MODEL_RLS', 'Rls');

//konstanta status
define('CREATED', 'CREATED');
define('ACTIVE', 'ACTIVE');
define('SUBMITTED',  'SUBMITTED');
define('APPROVED',  'APPROVED');
define('REJECTED',  'REJECTED');
define('REVISED',  'REVISED');
define('SENT', 'SENT');
define('LOCKED', 'LOCKED');
define('RECEIVED', 'RECEIVED');
define('VERIFIED', 'VERIFIED');

//konstanta instalasi
define('PEMBANGKIT',  'Pembangkit');
define('TRANSMISI',  'Transmisi');
define('DISTRIBUSI',  'Distribusi');
define('PEMANFAATAN_TT',  'Pemanfaatan TT');
define('PEMANFAATAN_TM',  'Pemanfaatan TM');

//konstanta jenis instalasi
define('JENIS_GARDU','GARDU INDUK');
define('JENIS_TRANSMISI','JARINGAN TRANSMISI');

//konstanta jenis bay
define('BAY_LINE','Bay Line');
define('BAY_COUPLER','Bay Bus Coupler');
define('BAY_TRANSFORMATOR','Bay Transformator Tenaga');
define('BAY_REAKTOR','Bay Reaktor');
define('BAY_KAPASITOR','Bay Kapasitor');
define('PHB','PHB');
define('BAY_CUSTOM','Custom');

//id tipe instalasi
define('ID_TRANSMISI',2);
define('ID_PEMBANGKIT',1);
define('ID_DISTRIBUSI',3);
define('ID_PEMANFAATAN_TT',4);
define('ID_PEMANFAATAN_TM',5);

#constant for action worflow
define('ACTION_ORDER', '/internal/approval_order');
define('ACTION_EKSTERNAL_RAB', '/eksternal/approval_rab');
define('ACTION_INTERNAL_RAB', '/internal/approval_rab');
define('ACTION_INTERNAL_KONTRAK', '/internal/approval_kontrak');
define('ACTION_INTERNAL_WBS_IO', '/internal/approval_wbs_io');
define('ACTION_WORKFLOW', '/internal/approval_workflow');


//tipe user
define('TIPE_PLN', 'PLN');
define('TIPE_EKSTERNAL','EKSTERNAL');
define('TIPE_INTERNAL','INTERNAL');
define('TIPE_ALL','ALL');

define('TABLE_ORDER', 'orders');
define('TABLE_KONTRAK', 'kontrak');
define('TABLE_WBS_IO', 'wbs_io');
define('TABLE_PEKERJAAN', 'pekerjaan');
define('TABLE_PEKERJAAN_PERALATAN', 'permohonan_peralatan');
define('TABLE_PERMOHONAN_RLB', 'permohonan_rlb');
define('TABLE_RLB', 'rlb');
define('TABLE_PERMOHONAN_RLS', 'permohonan_rls');
define('TABLE_RLS', 'rls');

//PLTS
define('KODE_PLTS','193');

//kode layanan
define('KODE_SLO','SLO');
define('KODE_RESLO','RE-SLO');

#status pembayaran
define('TERMIN_CREATED', 'CREATED');
define('UNPAID', 'UNPAID');
define('PAID', 'PAID');
define('INVOICE_REQUESTED', 'INVOICE REQUESTED');
define('INVOICE_CREATED', 'INVOICE CREATED');
define('INVOICE_SENT', 'INVOICE SENT');
define('BUKTI_UPLOADED', 'BUKTI UPLOADED');
define('KUITANSI_CREATED', 'KUITANSI CREATED');
define('KUITANSI_SENT', 'KUITANSI SENT');

//tipe program distribusi
define('TIPE_PROGRAM',"PROGRAM");
define('TIPE_AREA',"AREA");
define('TIPE_KONTRAK',"KONTRAK");
define('TIPE_LOKASI',"LOKASI");
define('TIPE_PENYULANG',"PENYULANG");
define('TIPE_GARDU',"GI");
define('TIPE_INSTALASI_PROGRAM',"DISTRIBUSI");

//tipe wbs io
define('WBS',"WBS");
define('IO',"IO");
define('TIPE_NORMAL',"Normal");
define('TIPE_EMERGENCY',"Emergency");


//sys status
define('CONTRACT','CONTRACT');

#status upload file realisasi wbs/io
define('SUCCESS',"SUCCESS");
define('ERROR',"ERROR");
define('DUPLICATE',"DUPLICATE");

#kategori perusahaan
define('PLN',"PLN");
define('SWASTA',"SWASTA");
define('ANAK_PERUSAHAAN',"ANAK PERUSAHAAN");

#status kontrak
define('SCHEDULED',"SCHEDULED");

//nama jabatan inspeksi
define("PELAKSANA_INSPEKSI","Pelaksana Inspeksi");
define("TENAGA_TEKNIK","Tenaga Teknik");

//status pekerjaan
define("PEKERJAAN_CLOSED","CLOSED");

//status peralatan
define('PERALATAN_NORMAL','NORMAL');
define('PERALATAN_RUSAK','BREAKDOWN');
define('PERALATAN_BOOKED','BOOKED');
define('PERALATAN_DIPAKAI','USED');
define('PERALATAN_DIPINJAM','BORROWED');
define('PERALATAN_KALIBRASI','CALIBRATE');

//template word filename
define('TEMPLATE_KICKOFF', 'template_kickoff_meeting.docx');
define('TEMPLATE_PENDING_ITEMS', 'pending_items.docx');
define('TEMPLATE_PERMOHONAN_PERALATAN', 'template_permohonan_peralatan.docx');
define('TEMPLATE_SURAT_PERINTAH_TUGAS', 'template_surat_perintah_tugas.docx');
define('TEMPLATE_BA_INSPEKSI', 'template_berita_acara_inspeksi.docx');

//template word RLB
define('TEMPLATE_PERMOHONAN_RLB_PEMBANGKIT','FORMULIR_PERMINTAAN_RLB_PEMBANGKIT.docx');
define('TEMPLATE_PERMOHONAN_RLB_DISTRIBUSI','FORMULIR_PERMINTAAN_RLB_DISTRIBUSI.docx');
define('TEMPLATE_PERMOHONAN_RLB_TRANSMISI','FORMULIR_PERMINTAAN_RLB_TRANSMISI.docx');
define('TEMPLATE_RLB_PEMBANGKIT','RLB_PEMBANGKIT.docx');
define('TEMPLATE_RLB_TRANSMISI','RLB_TRANSMISI.docx');
define('TEMPLATE_RLB_GI','RLB_GI.docx');

//template word RLS
define('TEMPLATE_PERMOHONAN_RLS','FORMULIR_PERMINTAAN_RLS.docx');
define('TEMPLATE_RLS','TEMPLATE_RLS.docx');

//permission constant
//Mendapat notifikasi saat wbs/io $persen_realisasi > 70 && $persen_realisasi <= 90
define('PERMISSION_NOTIF_ALERT_WBS','NOTIF WBS/IO WARNING');
//Mendapat notifikasi jika lebih dari 90% maka wbs/io LOCKED
define('PERMISSION_NOTIF_ALERT_WBS_LOCKED','NOTIF WBS/IO LOCKED');
define('PERMISSION_VERIFICATE_USER','VERIFICATION USER');
define('PERMISSION_VERIFICATION_RLB_RLS','VERIFICATION RLB/RLS');
define('PERMISSION_VERIFICATION_RELEASE_RLB_RLS','VERIFICATION RELEASE RLB/RLS');
define('PERMISSION_SPV_REGIONAL','SPV REGIONAL PERMISSION');
define('PERMISSION_NOTIF_REGIONAL','NOTIF REGIONAL');
//permission RAB
define('PERMISSION_REVISI_RAB','REVISI RAB');
define('PERMISSION_CREATE_RAB','CREATE RAB');
//permission Kontrak
define('PERMISSION_CREATE_KONTRAK','CREATE KONTRAK');
define('PERMISSION_CREATE_INVOICE','CREATE INVOICE');
define('PERMISSION_AMANDEMEN_KONTRAK','AMANDEMEN KONTRAK');
define('PERMISSION_REQUEST_INVOICE','REQUEST INVOICE');
//permission user
define('PERMISSION_VIEW_USER','VIEW USER');
//permission order
define('PERMISSION_VIEW_PERMOHONAN','VIEW PERMOHONAN');
define('PERMISSION_VIEW_ORDER','VIEW ORDER');
define('PERMISSION_VERIFICATION_ORDER','VERIFICATION ORDER');
//permission wbs
define('PERMISSION_MONITOR_WBS_IO','MONITOR WBS/IO');
define('PERMISSION_CREATE_WBS_IO','CREATE WBS/IO');
define('PERMISSION_CREATE_REALISASI_WBS_IO','CREATE REALISASI WBS/IO');
define('PERMISSION_CLOSE_WBS_IO','CLOSE WBS/IO');
define('PERMISSION_LOCK_WBS_IO','LOCK WBS/IO');
define('PERMISSION_ACTIVATE_WBSIO','ACTIVATE WBS/IO');
//permission peminjaman peralatan
define('PERMISSION_CREATE_PEMINJAMAN_PERALATAN','CREATE PEMINJAMAN PERALATAN');
define('PERMISSION_CREATE_PENGEMBALIAN_PERALATAN','CREATE PENGEMBALIAN PERALATAN');
//permission master personil
define('PERMISSION_VIEW_PERSONIL','VIEW PERSONIL');
define('PERMISSION_EDIT_PERSONIL','EDIT PERSONIL');
define('PERMISSION_CREATE_PERSONIL','CREATE PERSONIL');
//permission master peralatan
define('PERMISSION_VIEW_MASTER_PERALATAN','VIEW MASTER PERALATAN');
define('PERMISSION_EDIT_MASTER_PERALATAN','EDIT MASTER PERALATAN');
define('PERMISSION_CREATE_MASTER_PERALATAN','CREATE MASTER PERALATAN');
//personil
define('PERMISSION_CREATE_PENUGASAN_PERSONIL','CREATE PENUGASAN PERSONIL');
define('PERMISSION_PRINT_SURAT_TUGAS_PERSONIL','PRINT SURAT TUGAS PERSONIL');
define('PERMISSION_CREATE_PROGRESS_PEKERJAAN','CREATE PROGRESS PEKERJAAN');
//lhpp
define('PERMISSION_UPLOAD_DRAFT_LHPP','UPLOAD DRAFT LHPP');
define('PERMISSION_CREATE_LHPP','CREATE LHPP');
//RLB & RLS
define('PERMISSION_VIEW','VIEW');
define('PERMISSION_CREATE_RLB_RLS','CREATE RLB/RLS');

//URL DJK
define('URL_PROD_DJK', 'https://slodjk.esdm.go.id/api/');
define('URL_DEV_DJK', 'http://103.87.161.97/slo/api/');
define('DJK_PJT','lit/pjt');
define('DJK_TT','lit/tt');
define('DJK_AREA','pln/area');
define('DJK_RAYON','pln/rayon');
define('DJK_WILAYAH','pln/wilayah');
define('DJK_KOTA','ref/kota');
define('DJK_PROVINSI','ref/provinsi');
define('DJK_SBU','ref/sbu');
define('DJK_HASIL','tm/slo/hasil');
define('DJK_CRT_PMH','tm/permohonan/create');
define('DJK_UPDT_PMH','tm/permohonan/update');
define('DJK_REG','tm/slo/register');

//tipe pelaksana
define('ID_KETUA_PELAKSANA','1');
define('ID_ANGGOTA','2');
define('ID_PJT','3');

//tipe iuptl
define('ID_KONT_PEMASANGAN','1');
define('ID_KONS_PERENCANA','2');

//url lhpp
define('URL_LHPP_LOKASI','public/lhpp/lokasiInstalasi/');
define('URL_LHPP_FOTO','public/lhpp/foto/');
define('URL_LHPP_DETAIL','public/lhpp/');

//kode jenis gardu induk
define('KODE_GARDU_INDUK_TRANSMISI','231');

//status tracking
define('STATUS_ORDER','ORDER');
define('STATUS_RAB','RANCANGAN BIAYA');
define('STATUS_KONTRAK','PROSES KONTRAK');
define('STATUS_WBS','PEMBUATAN WBS');
define('STATUS_PEKERJAAN','PELAKSANAAN INSPEKSI');
define('STATUS_REGISTRASI_SLO','PROSES REGISTRASI SLO');

//status permohonan djk
define('DJK_NOT_SENT', 'NOT SENT');
define('DJK_SENT', 'SENT');
define('DJK_REJECTED', 'REJECTED');
define('DJK_APPROVED', 'APPROVED');

//jenis instsalasi gardu induk
define('GARDU_INDUK', 19);