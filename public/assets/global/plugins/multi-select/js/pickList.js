(function ($) {

    $.fn.pickList = function (options) {
        var opts = $.extend({}, $.fn.pickList.defaults, options);
        var parent = opts.parentId;
        this.fill = function () {
            var option = '';

            $.each(opts.data, function (key, val) {
                option += '<option id=' + val.id + '>' + val.text + '</option>';
            });
            this.find('#pickData' + parent).append(option);
        };
        this.controll = function () {
            var pickThis = this;

            $("#pAdd" + parent).on('click', function () {
                var p = pickThis.find("#pickData" + parent + " option:selected");
                p.clone().appendTo("#pickListResult" + parent);
                p.remove();
            });

            $("#pAddAll" + parent).on('click', function () {
                var p = pickThis.find("#pickData" + parent + " option");
                p.clone().appendTo("#pickListResult" + parent);
                p.remove();
            });

            $("#pRemove" + parent).on('click', function () {
                var p = pickThis.find("#pickListResult" + parent + " option:selected");
                p.clone().appendTo("#pickData" + parent);
                p.remove();
            });

            $("#pRemoveAll" + parent).on('click', function () {
                var p = pickThis.find("#pickListResult" + parent + " option");
                p.clone().appendTo("#pickData" + parent);
                p.remove();
            });
        };
        this.getValues = function () {
            var objResult = [];
            this.find("#pickListResult" + parent + " option").each(function () {
                objResult.push({id: this.id, text: this.text});
            });
            return objResult;
        };
        this.init = function () {
            var pickListHtml =
                "<div class='row'>" +
                " <div class='form-group'>" +
                "  <div class='col-sm-5'>" +
                "	 <select class='form-control pickListSelect form-white' id='pickData" + parent + "' multiple></select>" +
                " </div>" +
                " <div class='col-sm-2 pickListButtons'>" +
                "	<button id='pAdd" + parent + "' class='btn btn-primary btn-square btn-embossed'>" + opts.add + "</button>" + "<br>" +
                "  <button id='pAddAll" + parent + "' class='btn btn-primary btn-square btn-embossed'>" + opts.addAll + "</button>" + "<br>" +
                "	<button id='pRemove" + parent + "' class='btn btn-primary btn-square btn-embossed'>" + opts.remove + "</button>" + "<br>" +
                "	<button id='pRemoveAll" + parent + "' class='btn btn-primary btn-square btn-embossed'>" + opts.removeAll + "</button>" +
                " </div>" +
                " <div class='col-sm-5'>" +
                "    <select class='form-control pickListSelect form-white' id='pickListResult" + parent + "' multiple></select>" +
                " </div>" +
                " </div>" +
                "</div>";

            this.append(pickListHtml);

            this.fill();
            this.controll();
        };

        this.init();
        return this;
    };

    $.fn.pickList.defaults = {
        add: 'Add',
        addAll: 'Add All',
        remove: 'Remove',
        removeAll: 'Remove All'
    };

}(jQuery));


