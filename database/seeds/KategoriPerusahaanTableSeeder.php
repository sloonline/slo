<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KategoriPerusahaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_perusahaan')->delete();
        DB::table('kategori_perusahaan')->insert(array(
            array('id'=>'1','nama_kategori'=>'PLN','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','nama_kategori'=>'Swasta','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','nama_kategori'=>'Anak Perusahaan','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
        ));
    }
}
