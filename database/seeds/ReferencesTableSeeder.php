<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('references')->delete();
        DB::table('references')->insert(array(
            //jenis ijin karyawan
            array('id'=>'1','nama_reference'=>'Jenis Ijin Perusahaan','parent'=>'0','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','nama_reference'=>'IUPTL','parent'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','nama_reference'=>'IO','parent'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','nama_reference'=>'Surat Keterangan Terdaftar','parent'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            //jenis ijin usaha
            array('id'=>'5','nama_reference'=>'Jenis Ijin Usaha','parent'=>'0','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'6','nama_reference'=>'IUPTL','parent'=>'5','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'7','nama_reference'=>'IO','parent'=>'5','created_at' => Carbon::now(),'updated_at' => Carbon::now()),

        ));
    }
}
