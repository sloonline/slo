<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BidangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bidang')->delete();
        DB::table('bidang')->insert(array(
            array('id'=>'1','nama_bidang'=>'BKI','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','nama_bidang'=>'BKI 2','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','nama_bidang'=>'BPU','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','nama_bidang'=>'BKA','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'5','nama_bidang'=>'GM','isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
        ));
    }
}
