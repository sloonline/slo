<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PenugasanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('penugasan')->delete();
        DB::table('penugasan')->insert(array(
            // array('id'=>'1','role_id'=>'6','sub_bidang_id'=>'1','nama_penugasan'=>'PLTU','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            // array('id'=>'2','role_id'=>'6','sub_bidang_id'=>'1','nama_penugasan'=>'PLTG','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            // array('id'=>'3','role_id'=>'6','sub_bidang_id'=>'1','nama_penugasan'=>'PLTG','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'1','sub_bidang_id'=>'1','nama_penugasan'=>'PLTU','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','sub_bidang_id'=>'1','nama_penugasan'=>'PLTG','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','sub_bidang_id'=>'1','nama_penugasan'=>'PLTG','created_at' => Carbon::now(),'updated_at' => Carbon::now()),

        ));
    }
}
