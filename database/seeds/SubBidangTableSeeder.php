<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SubBidangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sub_bidang')->delete();
        DB::table('sub_bidang')->insert(array(
            array('id'=>'1','nama_sub_bidang'=>'Sistem Pembangkit','id_bidang'=>1,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','nama_sub_bidang'=>'Sisem Transmisi & Gardu Induk','id_bidang'=>1,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','nama_sub_bidang'=>'Sistem Distribusi','id_bidang'=>1,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','nama_sub_bidang'=>'Kendali Mutu Ketenagalistrikan','id_bidang'=>1,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'5','nama_sub_bidang'=>'Sistem Pembangkit','id_bidang'=>2,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'6','nama_sub_bidang'=>'Sisem Transmisi & Gardu Induk','id_bidang'=>2,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'7','nama_sub_bidang'=>'Sistem Distribusi','id_bidang'=>2,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'8','nama_sub_bidang'=>'Kendali Mutu Ketenagalistrikan','id_bidang'=>2,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'9','nama_sub_bidang'=>'Niaga','id_bidang'=>3,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'10','nama_sub_bidang'=>'Perencanaan','id_bidang'=>3,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'11','nama_sub_bidang'=>'Keuangan','id_bidang'=>4,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'12','nama_sub_bidang'=>'SDM','id_bidang'=>4,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'13','nama_sub_bidang'=>'Umum','id_bidang'=>4,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'14','nama_sub_bidang'=>'Manajemen','id_bidang'=>5,'isAktif'=>'1','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
        ));
    }
}
