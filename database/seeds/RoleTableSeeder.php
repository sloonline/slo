<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert(array(
            array('id'=>'1','name'=>'General Manajer','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','name'=>'Manajer Bidang','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','name'=>'Deputi Manajer','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','name'=>'Pengendali','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'5','name'=>'Support','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'6','name'=>'Penanggung Jawab Teknik','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'7','name'=>'Tenaga Teknik','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'8','name'=>'Pelaksana Inspeksi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'9','name'=>'Administrator','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'10','name'=>'Peminta Jasa','created_at' => Carbon::now(),'updated_at' => Carbon::now())
        ));
    }
}
