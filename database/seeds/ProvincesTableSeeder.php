<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   DB::table('cities')->delete();
        DB::table('provinces')->delete();
        DB::table('provinces')->insert(array(
            array('id'=>'1','province'=>'Nanggroe Aceh Darussalam','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','province'=>'Sumatera Utara','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','province'=>'Sumatera Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','province'=>'Riau','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'5','province'=>'Kepulauan Riau','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'6','province'=>'Kepulauan Bangka-Belitung','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'7','province'=>'Jambi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'8','province'=>'Bengkulu','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'9','province'=>'Sumatera Selatan','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'10','province'=>'Lampung','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'11','province'=>'Banten','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'12','province'=>'DKI Jakarta','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'13','province'=>'Jawa Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'14','province'=>'Jawa Tengah','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'15','province'=>'Daerah Istimewa Yogyakarta  ','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'16','province'=>'Jawa Timur','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'17','province'=>'Bali','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'18','province'=>'Nusa Tenggara Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'19','province'=>'Nusa Tenggara Timur','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'20','province'=>'Kalimantan Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'21','province'=>'Kalimantan Tengah','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'22','province'=>'Kalimantan Selatan','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'23','province'=>'Kalimantan Timur','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'24','province'=>'Gorontalo','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'25','province'=>'Sulawesi Selatan','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'26','province'=>'Sulawesi Tenggara','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'27','province'=>'Sulawesi Tengah','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'28','province'=>'Sulawesi Utara','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'29','province'=>'Sulawesi Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'30','province'=>'Maluku','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'31','province'=>'Maluku Utara','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'32','province'=>'Papua Barat','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'33','province'=>'Papua','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'34','province'=>'Kalimantan Utara','created_at' => Carbon::now(),'updated_at' => Carbon::now()),

        ));
    }
}
