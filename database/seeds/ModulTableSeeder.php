<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modul')->delete();
        DB::table('modul')->insert(array(
            array('id'=>'1','nama_modul'=>'Register','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'2','nama_modul'=>'Approval','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'3','nama_modul'=>'Profile','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'4','nama_modul'=>'Permohonan','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'5','nama_modul'=>'Data Instalasi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'6','nama_modul'=>'Data Lingkup Kerja','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'7','nama_modul'=>'Verifikasi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'8','nama_modul'=>'Unlock Order','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'9','nama_modul'=>'Form 1B','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'10','nama_modul'=>'Form 1A','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'11','nama_modul'=>'Approval Form 1B','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'12','nama_modul'=>'Approval Form 1A','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'13','nama_modul'=>'Lock Form 1A','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'14','nama_modul'=>'Persetujuan Biaya','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'15','nama_modul'=>'Negosiasi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
            array('id'=>'16','nama_modul'=>'Approval Negosiasi','created_at' => Carbon::now(),'updated_at' => Carbon::now()),
          ));
    }
}
