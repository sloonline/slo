<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ProvincesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(BidangTableSeeder::class);
        $this->call(SubBidangTableSeeder::class);
        $this->call(KategoriPerusahaanTableSeeder::class);

        $this->call(RoleTableSeeder::class);
        $this->call(PenugasanTableSeeder::class);
        $this->call(ReferencesTableSeeder::class);
        $this->call(ModulTableSeeder::class);
        $this->call(PerusahaanTableSeeder::class);


        Model::reguard();
    }
}
