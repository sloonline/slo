<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePjtDjk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pjt_djk', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pjt_id');
            // $table->integer('wilayah_id');
            // $table->integer('area_id');
            $table->string('bidang', 255);
            $table->string('subbidang', 255);
            $table->string('nama', 255);
            $table->string('nik', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pjt_djk');
    }
}
