<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFotoInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            //
            $table->text('foto_1')->nullable();
            $table->text('foto_2')->nullable();
            $table->text('foto_3')->nullable();
            $table->text('foto_4')->nullable();
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            //
            $table->text('foto_1')->nullable();
            $table->text('foto_2')->nullable();
            $table->text('foto_3')->nullable();
            $table->text('foto_4')->nullable();
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            //
            $table->text('foto_1')->nullable();
            $table->text('foto_2')->nullable();
            $table->text('foto_3')->nullable();
            $table->text('foto_4')->nullable();
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            //
            $table->text('foto_1')->nullable();
            $table->text('foto_2')->nullable();
            $table->text('foto_3')->nullable();
            $table->text('foto_4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            //
            $table->dropColumn('foto_1');
            $table->dropColumn('foto_2');
            $table->dropColumn('foto_3');
            $table->dropColumn('foto_4');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            //
            $table->dropColumn('foto_1');
            $table->dropColumn('foto_2');
            $table->dropColumn('foto_3');
            $table->dropColumn('foto_4');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            //
            $table->dropColumn('foto_1');
            $table->dropColumn('foto_2');
            $table->dropColumn('foto_3');
            $table->dropColumn('foto_4');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            //
            $table->dropColumn('foto_1');
            $table->dropColumn('foto_2');
            $table->dropColumn('foto_3');
            $table->dropColumn('foto_4');
        });
    }
}
