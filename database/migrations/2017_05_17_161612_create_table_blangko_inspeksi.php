<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBlangkoInspeksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blangko_inspeksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan');
            $table->string('tanggal_mulai_pemeriksaan', 36);
            $table->string('tanggal_selesai_pemeriksaan', 36);
            $table->string('file_blangko_inspeksi', 124)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blangko_inspeksi');
    }
}
