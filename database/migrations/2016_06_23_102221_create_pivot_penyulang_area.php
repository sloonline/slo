<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotPenyulangArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyulang_area', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_area_program')->unsigned();
            $table->foreign('id_area_program')->references('id')->on('area_program');
            $table->integer('id_penyulang')->unsigned();
            $table->foreign('id_penyulang')->references('id')->on('penyulang');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penyulang_area');
    }
}
