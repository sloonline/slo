<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRabKontrakWbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->string('sys_status')->nullable();
        });

        Schema::table('kontrak', function (Blueprint $table) {
            $table->string('sys_status')->nullable();
        });

        Schema::table('wbs_io', function (Blueprint $table) {
            $table->string('sys_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->dropColumn('sys_status');
        });

        Schema::table('kontrak', function (Blueprint $table) {
            $table->dropColumn('sys_status');
        });

        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropColumn('sys_status');
        });
    }
}
