<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPerusahaanIdTableProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       /* Schema::table('program', function (Blueprint $table) {
            $table->integer('id_surat_tugas')->nullable()->change();
            $table->integer('perusahaan_id')->nullable();
            $table->foreign('perusahaan_id')->references('id')->on('perusahaan');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program', function (Blueprint $table) {
            $table->dropForeign('perusahaan_id');
            $table->dropColumn('perusahaan_id');
        });
    }
}
