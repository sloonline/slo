<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->integer('status')->unsigned()->nullable();
            $table->foreign('status')->references('id')->on('references');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropForeign('status');
            $table->dropColumn('status');
        });
    }
}
