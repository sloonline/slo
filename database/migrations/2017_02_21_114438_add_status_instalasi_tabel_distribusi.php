<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusInstalasiTabelDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('instalasi_distribusi', function (Blueprint $table) {
                $table->integer('status_baru')->default(1);
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('status_baru');
        });
    }
}
