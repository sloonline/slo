<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/
            $table->string('nomor_order', 50);
            $table->string('tanggal_order', 50)->nullable();
            $table->integer('id_perusahaan')->unsigned();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaan');
            
            $table->string('nomor_sp', 50)->nullable(); //nomor surat permintaan
            $table->string('tanggal_sp', 50)->nullable();  //tanggal surat permintaan
            $table->text('file_sp')->nullable();  //file surat permintaan


            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'nomor_order']);
        });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
