<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableWbsioRab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rab_wbsio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rab_id')->unsigned()->index();
            $table->foreign('rab_id')->references('id')->on('rab');
            $table->integer('wbsio_id')->unsigned()->index();
            $table->foreign('wbsio_id')->references('id')->on('wbs_io');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rab_wbsio');
    }
}
