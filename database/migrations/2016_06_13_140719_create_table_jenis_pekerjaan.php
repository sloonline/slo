<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJenisPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_pekerjaan', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field jenis pekerjaan*/
            $table->string('jenis_pekerjaan', 150);

            $table->integer('tipe_instalasi_id')->unsigned();
            $table->foreign('tipe_instalasi_id')->references('id')->on('tipe_instalasi');

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'jenis_pekerjaan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jenis_pekerjaan');
    }
}
