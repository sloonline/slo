<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotAreaProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_program', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_program')->unsigned();
            $table->foreign('id_program')->references('id')->on('program');
            $table->integer('business_area')->unsigned();
            $table->foreign('business_area')->references('id')->on('business_area');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_program');
    }
}
