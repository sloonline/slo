<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLaporanHilo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_hilo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->integer('lingkup_pekerjaan_id')->unsigned();
            $table->foreign('lingkup_pekerjaan_id')->references('id')->on('lingkup_pekerjaan')->onDelete('cascade');
            $table->string('tanggal_mulai_pemeriksaan')->nullable();
            $table->string('tanggal_selesai_pemeriksaan')->nullable();
            $table->text('hasil_hilo')->nullable();
            $table->text('hasil_form_hilo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('laporan_hilo');
    }
}
