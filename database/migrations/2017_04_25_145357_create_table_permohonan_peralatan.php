<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermohonanPeralatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_peralatan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip_pemohon', 128);
            $table->string('nama_pemohon', 128);
            $table->string('tanggal_pemakaian', 128);
            $table->string('lokasi_inspeksi', 128);
            $table->string('maksud_perjalanan', 128);
            $table->string('keterangan', 128);
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan_peralatan');
    }
}
