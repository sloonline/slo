<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->index(['tanggal_order','id_perusahaan','nomor_sp','tanggal_sp','tipe_instalasi_id','id_surat_tugas']);
        });

        Schema::table('program', function (Blueprint $table) {
            $table->index(['id_jenis_program','periode']);
        });

        Schema::table('permohonan', function (Blueprint $table) {
            $table->index(['id_orders','nomor_permohonan','tanggal_permohonan','id_lingkup_pekerjaan','id_lingkup_program','id_program','id_area_kontrak','id_lokasi_penyulang']);
        });


        Schema::table('kontrak', function (Blueprint $table) {
            $table->index(['id_program','periode','nomor_kontrak','tanggal_kontrak']);
        });

        Schema::table('permohonan_lingkup', function (Blueprint $table) {
            $table->index(['permohonan_id','jenis_pekerjaan_id']);
        });


        Schema::table('lingkup_program', function (Blueprint $table) {
            $table->index(['id_lokasi_area','id_penyulang_area','id_gardu_area','id_lingkup_pekerjaan']);
        });


        Schema::table('lokasi_area', function (Blueprint $table) {
            $table->index(['id_area_program','id_kontrak','id_lokasi','id_jenis_lokasi']);
        });


        Schema::table('surat_tugas', function (Blueprint $table) {
            $table->index(['id_perusahaan','periode','nomor_surat','tanggal_surat']);
        });


        Schema::table('area_program', function (Blueprint $table) {
            $table->index(['id_program','business_area']);
        });


        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->index(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->index(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->index(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->index(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('pemilik_instalasi', function (Blueprint $table) {
            $table->index(['id','nama_pemilik','id_province','id_city','jenis_ijin_usaha','perusahaan_id']);
        });


        Schema::table('gardu_area', function (Blueprint $table) {
            $table->index(['id_area_program','id_gardu_induk']);
        });

        Schema::table('gardu_induk', function (Blueprint $table) {
            $table->index(['business_area','gardu_induk','jenis_gardu']);
        });


        Schema::table('penyulang_area', function (Blueprint $table) {
            $table->index(['id_area_program','id_penyulang']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropIndex(['tanggal_order','id_perusahaan','nomor_sp','tanggal_sp','tipe_instalasi_id','id_surat_tugas']);
        });

        Schema::table('program', function (Blueprint $table) {
            $table->dropIndex(['id_jenis_program','periode']);
        });



        Schema::table('permohonan', function (Blueprint $table) {
            $table->dropIndex(['id_orders','nomor_permohonan','tanggal_permohonan','id_lingkup_pekerjaan','id_lingkup_program','id_program','id_area_kontrak','id_lokasi_penyulang']);
        });


        Schema::table('kontrak', function (Blueprint $table) {
            $table->dropIndex(['id_program','periode','nomor_kontrak','tanggal_kontrak']);
        });

        Schema::table('permohonan_lingkup', function (Blueprint $table) {
            $table->dropIndex(['permohonan_id','jenis_pekerjaan_id']);
        });


        Schema::table('lingkup_program', function (Blueprint $table) {
            $table->dropIndex(['id_lokasi_area','id_penyulang_area','id_gardu_area','id_lingkup_pekerjaan']);
        });


        Schema::table('lokasi_area', function (Blueprint $table) {
            $table->dropIndex(['id_area_program','id_kontrak','id_lokasi','id_jenis_lokasi']);
        });


        Schema::table('surat_tugas', function (Blueprint $table) {
            $table->dropIndex(['id_perusahaan','periode','nomor_surat','tanggal_surat']);
        });


        Schema::table('area_program', function (Blueprint $table) {
            $table->dropIndex(['id_program','business_area']);
        });


        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropIndex(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropIndex(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropIndex(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropIndex(['id_jenis_instalasi','id_provinsi','id_kota','pemilik_instalasi_id']);
        });


        Schema::table('pemilik_instalasi', function (Blueprint $table) {
            $table->dropIndex(['id','nama_pemilik','id_province','id_city','jenis_ijin_usaha','perusahaan_id']);
        });


        Schema::table('gardu_area', function (Blueprint $table) {
            $table->dropIndex(['id_area_program','id_gardu_induk']);
        });

        Schema::table('gardu_induk', function (Blueprint $table) {
            $table->dropIndex(['business_area','gardu_induk','jenis_gardu']);
        });


        Schema::table('penyulang_area', function (Blueprint $table) {
            $table->dropIndex(['id_area_program','id_penyulang']);
        });

    }
}
