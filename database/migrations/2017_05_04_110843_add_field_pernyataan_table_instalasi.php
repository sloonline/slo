<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldPernyataanTableInstalasi extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('instalasi_pembangkit', function (Blueprint $table) {
      $table->text('file_pernyataan')->nullable();
    });
    Schema::table('instalasi_transmisi', function (Blueprint $table) {
      $table->text('file_pernyataan')->nullable();
    });
    Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
      $table->text('file_pernyataan')->nullable();
    });
    Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
      $table->text('file_pernyataan')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('instalasi_pembangkit', function (Blueprint $table) {
      $table->dropColumn('file_pernyataan');
    });
    Schema::table('instalasi_transmisi', function (Blueprint $table) {
      $table->dropColumn('file_pernyataan');
    });
    Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
      $table->dropColumn('file_pernyataan');
    });
    Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
      $table->dropColumn('file_pernyataan');
    });
  }
}
