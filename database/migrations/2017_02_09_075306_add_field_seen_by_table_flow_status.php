<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSeenByTableFlowStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //status seen by untuk menentukan saat status tersebut orang siapa saja yg bisa akses
    public function up()
    {
        Schema::table('flow_status', function (Blueprint $table) {
            $table->string('allow_seen')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flow_status', function (Blueprint $table) {
            $table->dropColumn('allow_seen');
        });
    }
}
