<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJenisInstalasiPermohonanDjk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan_djk', function (Blueprint $table) {
            $table->integer('jenis_instalasi_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan_djk', function (Blueprint $table) {
            $table->dropColumn('jenis_instalasi_id');
        });
    }
}
