<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContract extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('kontrak', function (Blueprint $table) {

            $table->increments('id')->unsigned();
            $table->integer('id_kontrak_version')->nullable();
            $table->integer('id_flow_status')->nullable();
            $table->string('sys_status', 200)->nullable();
            $table->integer('flag_active')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'id_kontrak_version']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('kontrak');
    }

}
