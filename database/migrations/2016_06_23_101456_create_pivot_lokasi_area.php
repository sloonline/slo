<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotLokasiArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokasi_area', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_area_program')->unsigned()->nullable();
            $table->integer('id_kontrak')->unsigned()->nullable();
            $table->integer('id_lokasi')->unsigned();
            $table->foreign('id_lokasi')->references('id')->on('lokasi');
            $table->integer('id_jenis_lokasi')->unsigned()->nullable();
            $table->foreign('id_jenis_lokasi')->references('id')->on('jenis_lokasi');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lokasi_area');
    }
}
