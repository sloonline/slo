<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusEksternalFlowStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //status ini untuk nama status yang ditampilkan di eksternal
    public function up()
    {
        Schema::table('flow_status', function (Blueprint $table) {
            $table->string('status_eksternal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flow_status', function (Blueprint $table) {
            $table->dropColumn("status_eksternal")->nullable();
        });
    }
}
