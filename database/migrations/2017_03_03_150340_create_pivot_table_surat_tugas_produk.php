<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableSuratTugasProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layanan_surat_tugas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produk_id')->unsigned()->index();
            $table->foreign('produk_id')->references('id')->on('produk');
            $table->integer('surat_tugas_id')->unsigned()->index();
            $table->foreign('surat_tugas_id')->references('id')->on('surat_tugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('layanan_surat_tugas');
    }
}
