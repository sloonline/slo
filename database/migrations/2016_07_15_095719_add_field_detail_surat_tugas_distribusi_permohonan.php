<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDetailSuratTugasDistribusiPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('id_surat_tugas')->unsigned()->nullable();
            $table->foreign('id_surat_tugas')->references('id')->on('surat_tugas');
        });
        Schema::table('permohonan', function (Blueprint $table) {
            $table->integer('id_lingkup_program')->unsigned()->nullable();
            $table->foreign('id_lingkup_program')->references('id')->on('lingkup_program');
            $table->integer('id_program')->unsigned()->nullable();
            $table->foreign('id_program')->references('id')->on('program');
            $table->integer('id_area_kontrak')->nullable();
            $table->integer('id_lokasi_penyulang')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['id_surat_tugas']);
            $table->dropColumn('id_surat_tugas');
        });
        Schema::table('permohonan', function (Blueprint $table) {
            $table->dropForeign(['id_lingkup_program','id_program']);
            $table->dropColumn('id_lingkup_program');
            $table->dropColumn('id_program');
            $table->dropColumn('id_area_kontrak');
            $table->dropColumn('id_lokasi_penyulang');
        });
    }
}
