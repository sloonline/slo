<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_surat_tugas')->unsigned();
            $table->foreign('id_surat_tugas')->references('id')->on('surat_tugas');
            $table->integer('id_jenis_program')->unsigned();
            $table->foreign('id_jenis_program')->references('id')->on('jenis_program');
            $table->integer('periode');
            $table->string('deskripsi',200);
            $table->string('user_create',50);
            $table->string('user_update',50);

            $table->softDeletes();
            $table->index(['id', 'id_surat_tugas']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('program');
    }
}
