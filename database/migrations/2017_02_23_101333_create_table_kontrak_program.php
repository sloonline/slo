<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKontrakProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('kontrak_program', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_program')->unsigned();
            $table->foreign('id_program')->references('id')->on('program');
            $table->integer('periode');
            $table->string('nomor_kontrak',50);
            $table->string('tanggal_kontrak');
            $table->string('vendor',50);
            $table->string('user_create',50);
            $table->string('user_update',50);

            $table->softDeletes();
            $table->index(['id', 'id_program']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kontrak_program');
    }
}
