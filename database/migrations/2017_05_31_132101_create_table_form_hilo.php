<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFormHilo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_hilo', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lingkup_pekerjaan_id')->unsigned();
          $table->foreign('lingkup_pekerjaan_id')->references('id')->on('lingkup_pekerjaan')->onDelete('cascade');
          $table->string('nama_field')->nullable();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_hilo');
    }
}
