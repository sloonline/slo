<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableKeahlianPersonilInspeksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keahlian_personil',function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('keahlian_id')->unsigned();
            $table->foreign('keahlian_id')->references('id')->on('keahlian');
            $table->integer('personil_id')->unsigned();
            $table->foreign('personil_id')->references('id')->on('keahlian');
            $table->string('level_keahlian')->nullable();
            $table->string('periode')->nullable();
            $table->string('no_sertifikat')->nullable();
            $table->string('file_sertifikat')->nullable();
            $table->string('lembaga')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('keahlian_personil');
    }
}
