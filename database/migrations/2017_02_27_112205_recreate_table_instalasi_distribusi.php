<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateTableInstalasiDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('instalasi_distribusi')) {
            Schema::drop('instalasi_distribusi');
        }
        Schema::create('instalasi_distribusi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_instalasi',200);
            $table->integer('status_baru')->default(1);
            $table->integer('id_jenis_instalasi')->unsigned();
            $table->foreign('id_jenis_instalasi')->references('id')->on('jenis_instalasi');
            $table->integer('id_jenis_instalasi_distribusi');
            $table->decimal('panjang_kms')->nullable();
            $table->integer('jml_gardu')->nullable();
            $table->string('kapasitas_gardu')->nullable();
            $table->integer('jml_cubicle')->nullable();
            $table->decimal('kapasitas_arus_hubung_singkat')->nullable();
            $table->integer('id_surat_tugas')->nullable();
            $table->foreign('id_surat_tugas')->references('id')->on('surat_tugas');
            $table->integer('id_program')->nullable();
            $table->foreign('id_program')->references('id')->on('program');
            $table->integer('id_gardu_area')->nullable();
            $table->foreign('id_gardu_area')->references('id')->on('gardu_area');
            $table->integer('id_penyulang_area')->nullable();
            $table->foreign('id_penyulang_area')->references('id')->on('penyulang_area');
            $table->integer('id_lokasi_area')->nullable();
            $table->foreign('id_lokasi_area')->references('id')->on('lokasi_area');
            $table->integer('id_perusahaan');
            $table->integer('pemilik_instalasi_id')->nullable();
            $table->foreign('pemilik_instalasi_id')->references('id')->on('pemilik_instalasi');
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');
            $table->string('lokasi',250)->nullable();
            $table->string('longitude_awal', 200)->nullable();
            $table->string('latitude_awal', 200)->nullable();
            $table->string('longitude_akhir', 200)->nullable();
            $table->string('latitude_akhir', 200)->nullable();
            $table->decimal('tegangan_pengenal')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('tipe_pemilik');
            $table->string('created_by');
            $table->string('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalasi_distribusi');
    }
}
