<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTableRabAndRabPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('rab');
        Schema::dropIfExists('rab_permohonan');

        Schema::create('rab', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->string('no_dokumen',  32);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('rab_detail', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_unsur_biaya');
            $table->foreign('id_unsur_biaya')->references('id')->on('unsur_biaya')->onDelete('cascade');
            $table->integer('id_rab');
            $table->foreign('id_rab')->references('id')->on('rab')->onDelete('cascade');
            $table->integer('kantor_hari');
            $table->integer('kantor_orang');
            $table->integer('lapangan_hari');
            $table->integer('lapangan_orang');
            $table->integer('jumlah');
            $table->integer('jumlah_biaya');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('rab_permohonan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('permohonan_id');
            $table->foreign('permohonan_id')->references('id')->on('permohonan')->onDelete('cascade');
            $table->integer('rab_id');
            $table->foreign('rab_id')->references('id')->on('rab')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rab');
        Schema::drop('rab_detail');
    }
}
