<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKodeKontraktorInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });

        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });

        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });

        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });

        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });

        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });
    }
}
