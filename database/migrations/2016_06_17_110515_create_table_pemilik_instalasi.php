<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePemilikInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemilik_instalasi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_pemilik', 50);
            $table->text('alamat_pemilik', 100)->nullable();
            $table->integer('id_province')->unsigned();
            $table->foreign('id_province')->references('id')->on('provinces');
            $table->integer('id_city')->unsigned();
            $table->foreign('id_city')->references('id')->on('cities');
            $table->string('kode_pos_pemilik', 50)->nullable();
            $table->string('telepon_pemilik', 50)->nullable();
            $table->string('no_fax_pemilik', 50)->nullable();
            $table->string('email_pemilik', 50)->nullable();  
            $table->string('jenis_ijin_usaha', 50)->nullable();
            $table->string('penerbit_ijin_usaha', 50)->nullable();
            $table->string('no_ijin_usaha', 50)->nullable();
            $table->date('masa_berlaku_iu')->nullable();
            $table->text('file_siup')->nullable();
            $table->string('nama_kontrak', 50)->nullable();
            $table->string('no_kontrak')->nullable();
            $table->date('tgl_pengesahan_kontrak')->nullable();
            $table->date('masa_berlaku_kontrak')->nullable();
            $table->text('file_kontrak')->nullable();
            $table->string('no_spjbtl', 50)->nullable();
            $table->date('tgl_spjbtl')->nullable();
            $table->date('masa_berlaku_spjbtl')->nullable();
            $table->text('file_spjbtl')->nullable();
            $table->integer('perusahaan_id')->unsigned();
            $table->string('user_create')->nullable();
            $table->string('user_update')->nullable();
            $table->timestamps();
            $table->softDeletes();
            /*$table->index(['id']);*/
        });
        /*Instalasi pembangkit*/
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->integer('id_pemilik_instalasi')->unsigned();
            $table->foreign('id_pemilik_instalasi')->references('id')->on('pemilik_instalasi');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->integer('id_pemilik_instalasi')->unsigned();
            $table->foreign('id_pemilik_instalasi')->references('id')->on('pemilik_instalasi');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->integer('id_pemilik_instalasi')->unsigned();
            $table->foreign('id_pemilik_instalasi')->references('id')->on('pemilik_instalasi');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->integer('id_pemilik_instalasi')->unsigned();
            $table->foreign('id_pemilik_instalasi')->references('id')->on('pemilik_instalasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropForeign('id_pemilik_instalasi');
            $table->dropColumn('id_pemilik_instalasi');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropForeign('id_pemilik_instalasi');
            $table->dropColumn('id_pemilik_instalasi');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropForeign('id_pemilik_instalasi');
            $table->dropColumn('id_pemilik_instalasi');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropForeign('id_pemilik_instalasi');
            $table->dropColumn('id_pemilik_instalasi');
        });
        Schema::drop('pemilik_instalasi');
    }
}
