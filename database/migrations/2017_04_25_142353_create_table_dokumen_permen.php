<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDokumenPermen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_permen', function (Blueprint $table) {
            $table->increments('dokumen_id')->unsigned();
            $table->integer('id_jenis');
            $table->string('name')->nullable();
            $table->string('label')->nullable();
            $table->string('value')->nullable();
            $table->string('class')->nullable();
            $table->string('id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dokumen_permen');
    }
}
