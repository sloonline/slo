<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wbs_io', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('kontrak_id')->nullable();
            $table->foreign('kontrak_id')->references('id')->on('kontrak');
            $table->integer('jenis_id')->unsigned();
            $table->foreign('jenis_id')->references('id')->on('references');
            $table->integer('tipe');
            $table->string('nomor',35)->nullable();
            $table->integer('tahun');
            $table->integer('business_area')->nullable();
            $table->foreign('business_area')->references('id')->on('business_area');
            $table->text('uraian_pekerjaan');
            $table->decimal('nilai');
            $table->string('username', 50);
            $table->integer('id_flow_status');
            $table->integer('perusahaan_id')->unsigned();
            $table->foreign('perusahaan_id')->references('id')->on('perusahaan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wbs_io');
    }
}
