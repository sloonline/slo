<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFotoBayChangeKapasitasToVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //untuk bay line / kapasitor maka hanya isi kapasitas pemutus saja sedangkan untuk jenis lainnya isi kapasitas pemutus dan trafo
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->text('foto')->nullable();
            $table->string('kapasitas_pemutus',255)->nullable();
            $table->string('kapasitas_trafo',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->dropColumn('foto');
            $table->dropColumn('kapasitas_pemutus',255);
            $table->dropColumn('kapasitas_trafo',255);
        });
    }
}
