<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTemplateDataTeknikLingkupPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lingkup_pekerjaan', function (Blueprint $table) {
            $table->string('template_data_teknik',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lingkup_pekerjaan', function (Blueprint $table) {
            $table->dropColumn('template_data_teknik');
        });
    }
}
