<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePeralatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peralatan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_alat', 100);
            $table->string('merk_alat', 100);
            $table->string('tipe_alat', 100);
            $table->string('nomor_seri', 100);
            $table->integer('bidang_id')->unsigned();
            $table->foreign('bidang_id')->references('id')->on('bidang');
            $table->integer('sub_bidang_id')->unsigned();
            $table->foreign('sub_bidang_id')->references('id')->on('sub_bidang');
            $table->string('kondisi_alat', 100);
            $table->string('lokasi_alat', 100);
            $table->string('kategori_alat', 100);
            $table->string('foto_peralatan', 255);
            $table->timestamps();
            $table->softDeletes();

            $table->index(['id','nama_alat']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peralatan');
    }
}
