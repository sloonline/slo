<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSuratPernyataanPeminjamanPengembalianAlat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan_peralatan', function (Blueprint $table) {
            $table->text('file_permohonan')->nullable();
            $table->text('file_pengembalian')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan_peralatan', function (Blueprint $table) {
            $table->dropColumn('file_permohonan');
            $table->dropColumn('file_pengembalian');
        });
    }
}
