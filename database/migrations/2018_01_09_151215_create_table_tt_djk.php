<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTtDjk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_djk', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('wilayah_id');
            // $table->integer('area_id');
            $table->string('bidang', 255);
            $table->string('subbidang', 255);
            $table->string('nama',255);
            $table->string('nik', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tt_djk');
    }
}
