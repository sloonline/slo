<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonilInspeksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personil_inspeksi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_jabatan_inspeksi')->unsigned()->nullable();
            $table->foreign('id_jabatan_inspeksi')->references('id')->on('jabatan_inspeksi');
            $table->integer('id_status_pekerja')->unsigned();
            $table->foreign('id_status_pekerja')->references('id')->on('status_pekerja');
            $table->integer('id_bidang')->unsigned();
            $table->foreign('id_bidang')->references('id')->on('bidang');
            $table->integer('id_sub_bidang')->unsigned()->nullable();
            $table->foreign('id_sub_bidang')->references('id')->on('sub_bidang');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('nip')->nullable();
            $table->string('email')->nullable();
            $table->string('nama')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('status_menikah')->nullable();
            $table->string('no_hp')->nullable();
            $table->text('alamat')->nullable();
            $table->string('grade')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->integer('is_aktif')->default(1);
            $table->string('foto_personil')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personil_inspeksi');
    }
}
