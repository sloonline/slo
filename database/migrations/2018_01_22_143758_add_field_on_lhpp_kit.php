<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldOnLhppKit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lhpp_kit', function (Blueprint $table) {
            $table->string('kapasitas_modul', 50)->nullable();
            $table->string('kapasitas_inverter', 50)->nullable();
            $table->string('jumlah_modul', 50)->nullable();
            $table->string('jumlah_inverter', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lhpp_kit', function (Blueprint $table) {
            $table->dropColumn('kapasitas_modul');
            $table->dropColumn('kapasitas_inverter');
            $table->dropColumn('jumlah_modul');
            $table->dropColumn('jumlah_inverter');
        });
    }
}
