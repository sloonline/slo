<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFileSbujkIujkSldTableInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });

        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });

        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });

        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });

        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });

        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });
    }
}
