<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDjkAgenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('djk_agenda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instalasi_id');
            $table->integer('tipe_instalasi_id');
            $table->string('no_agenda', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('djk_agenda');
    }
}
