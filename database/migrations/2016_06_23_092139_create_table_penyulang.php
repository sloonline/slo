<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePenyulang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyulang', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('business_area')->unsigned();
            $table->foreign('business_area')->references('id')->on('business_area');
            $table->string('penyulang', 50);

            $table->softDeletes();
            $table->index(['id', 'business_area']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penyulang');
    }
}
