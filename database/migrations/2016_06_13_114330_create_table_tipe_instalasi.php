<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTipeInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipe_instalasi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_instalasi', 50);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'nama_instalasi']);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('tipe_instalasi_id')->unsigned();
            $table->foreign('tipe_instalasi_id')->references('id')->on('tipe_instalasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipe_instalasi');
    }
}
