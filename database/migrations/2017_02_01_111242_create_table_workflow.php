<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flow_status_id');
            $table->foreign('flow_status_id')->references('id')->on('flow_status');
            $table->integer('role_id');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('modul');
            $table->integer('next')->nullable();
            $table->integer('prev')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workflow');
    }
}
