<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldKotaProvinisiAkhirAdditionalDataDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->integer('id_provinsi_akhir')->unsigned()->nullable();
            $table->integer('id_kota_akhir')->unsigned()->nullable();
            $table->text('alamat_akhir_instalasi')->nullable();
            $table->text('alamat_instalasi')->nullable();
            $table->string('kontrak')->nullable();
            $table->text('area_apd')->nullable();
            $table->text('penyulang')->nullable();
            $table->text('gi_gh')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('id_provinsi_akhir');
            $table->dropColumn('id_kota_akhir');
            $table->dropColumn('alamat_akhir_instalasi');
            $table->dropColumn('alamat_instalasi');
            $table->dropColumn('kontrak');
            $table->dropColumn('area_apd');
            $table->dropColumn('penyulang');
            $table->dropColumn('gi_gh');
        });
    }
}
