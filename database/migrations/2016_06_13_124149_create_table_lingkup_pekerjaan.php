<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLingkupPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lingkup_pekerjaan', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field lingkup pekerjaan*/
            $table->string('jenis_lingkup_pekerjaan', 150);
            $table->string('template_data_teknik', 225);
            $table->string('template_blangko_inspeksi', 225);


            $table->integer('lingkup_pekerjaan_id');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'jenis_lingkup_pekerjaan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('lingkup_pekerjaan');

    }
}
