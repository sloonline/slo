<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusJenisTokenUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('remember_token',100)->nullable();
            $table->string('jenis_user',50)->default("INTERNAL");
            $table->string('status_user',50)->default("CREATED");
            //menampung alasan dari jaser jika user dipending (ditolak) saat verifikasi
            $table->text('keterangan_pending')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('remember_token');
            $table->dropColumn('jenis_user');
            $table->dropColumn('status_user');
            $table->dropColumn('keterangan_pending');
        });
    }
}
