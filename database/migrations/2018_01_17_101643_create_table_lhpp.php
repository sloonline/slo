<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLhpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lhpp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_lhpp', 50)->nullable();
            $table->string('no_slo', 50)->nullable();
            $table->string('no_permohonan', 50)->nullable();
            $table->string('nama_pemohon', 50)->nullable();
            $table->string('no_surat', 50)->nullable();
            $table->string('tgl_surat', 50)->nullable();
            $table->string('nama_pemilik_instalasi', 50)->nullable();
            $table->text('alamat_pemilik_instalasi')->nullable();
            $table->integer('tipe_instalasi')->nullable();
            $table->integer('instalasi_id')->nullable();
            $table->string('nama_badan_usaha', 50)->nullable();
            $table->text('alamat_badan_usaha')->nullable();
            $table->string('kode_badan_usaha', 50)->nullable();
            $table->string('klasifikasi', 50)->nullable();
            $table->string('kualifikasi', 50)->nullable();
            $table->string('pj_badan_usaha', 50)->nullable();
            $table->string('pjt_badan_usaha', 50)->nullable();
            $table->string('masa_berlaku', 50)->nullable();
            $table->string('no_msl_diagram', 50)->nullable();
            $table->string('tgl_msl_diagram', 50)->nullable();
            $table->string('waktu_pelaksanaan', 50)->nullable();
            $table->string('no_spkppp', 50)->nullable();
            $table->string('tgl_spkppp', 50)->nullable();
            $table->text('hasil_pp')->nullable();
            $table->text('kesimpulan')->nullable();
            $table->text('rekomendasi')->nullable();
            $table->text('no_lap_p_inspeksi')->nullable();
            $table->string('file_gbr_msl_diagram', 50)->nullable();
            $table->integer('bay_gardu_id')->nullable();
            $table->text('lembar_lhpp')->nullable();
            $table->text('laporan_final')->nullable();
            $table->text('draft_sertifikat_slo')->nullable();
            $table->text('sertifikat_slo')->nullable();
            $table->text('surat_kesesuaian')->nullable();
            $table->text('surat_permohonan')->nullable();
            $table->text('spjbtl')->nullable();
            $table->text('foto')->nullable();
            $table->integer('no_urut')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lhpp');
    }
}
