<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldNomorProjectDeskripsiTableWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->text('deskripsi')->nullable();
            $table->string('nomor_project')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropColumn('deskripsi');
            $table->dropColumn('nomor_project');
        });
    }
}
