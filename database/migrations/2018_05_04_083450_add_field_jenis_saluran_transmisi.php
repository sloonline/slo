<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJenisSaluranTransmisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->string('jenis_saluran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('jenis_saluran');
        });
    }
}
