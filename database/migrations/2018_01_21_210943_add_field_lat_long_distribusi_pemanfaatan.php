<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLatLongDistribusiPemanfaatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->string('lat_awal')->nullable();
            $table->string('long_awal')->nullable();
            $table->string('lat_akhir')->nullable();
            $table->string('long_akhir')->nullable();
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->string('lat_awal')->nullable();
            $table->string('long_awal')->nullable();
            $table->string('lat_akhir')->nullable();
            $table->string('long_akhir')->nullable();
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->string('lat_awal')->nullable();
            $table->string('long_awal')->nullable();
            $table->string('lat_akhir')->nullable();
            $table->string('long_akhir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('lat_awal');
            $table->dropColumn('long_awal');
            $table->dropColumn('lat_akhir');
            $table->dropColumn('long_akhir');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('lat_awal');
            $table->dropColumn('long_awal');
            $table->dropColumn('lat_akhir');
            $table->dropColumn('long_akhir');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('lat_awal');
            $table->dropColumn('long_awal');
            $table->dropColumn('lat_akhir');
            $table->dropColumn('long_akhir');
        });
    }
}
