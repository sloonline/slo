<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstalasiDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalasi_distribusi', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/

            $table->integer('id_jenis_instalasi')->unsigned();
            $table->foreign('id_jenis_instalasi')->references('id')->on('jenis_instalasi');
            $table->string('nama_instalasi', 150);
            $table->text('alamat_instalasi')->nullable();
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');

            $table->string('longitude_awal', 200)->nullable();
            $table->string('latitude_awal', 200)->nullable();
            $table->string('longitude_akhir', 200)->nullable();
            $table->string('latitude_akhir', 200)->nullable();

            $table->integer('panjang_saluran')->nullable();
            $table->integer('jumlah_tiang')->nullable();
            $table->integer('jumlah_gardu_distribusi')->nullable();
            $table->float('kapasitas_gardu_distribusi')->nullable();
            $table->integer('jumlah_panel')->nullable();
            $table->float('kapasitas_arus_hubung_singkat')->nullable();

            $table->integer('tegangan_pengenal')->nullable();  //tegangan di tabel reference
            $table->foreign('tegangan_pengenal')->references('id')->on('references');

            $table->integer('pemilik_instalasi_id')->nullable();  //tegangan di tabel reference
            $table->foreign('pemilik_instalasi_id')->references('id')->on('pemilik_instalasi');

            $table->text('foto_1')->nullable();
            $table->text('foto_2')->nullable();
            $table->text('foto_3')->nullable();
            $table->text('foto_4')->nullable();

            $table->string('created_by',50)->nullable();
            $table->foreign('created_by')->references('username')->on('users');

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id','id_jenis_instalasi','nama_instalasi', 'tegangan_pengenal', 'pemilik_instalasi_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalasi_distribusi');
    }
}
