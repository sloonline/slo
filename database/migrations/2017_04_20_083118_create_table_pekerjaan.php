<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pekerjaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('wbs_id')->unsigned();
            $table->foreign('wbs_id')->references('id')->on('wbsio')->onDelete('cascade');
            $table->integer('permohonan_id')->unsigned();
            $table->foreign('permohonan_id')->references('id')->on('permohonan')->onDelete('cascade');
            $table->integer('id_flow_status')->unsigned();
            $table->foreign('id_flow_status')->references('id')->on('flow_status')->onDelete('cascade');
            $table->string('pekerjaan', 150);
            $table->string('no_surat_penugasan', 150)->nullable();
            $table->string('tgl_surat_penugasan', 32)->nullable();
            $table->text('uraian_pekerjaan');
            $table->text('file_surat_penugasan')->nullable();
            $table->string('tgl_mulai_pekerjaan', 64);
            $table->string('tgl_selesai_pekerjaan', 64);
            $table->string('sys_status', 20);
            $table->integer('progress')->default(0);
            $table->string('status_pekerjaan', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pekerjaan');
    }
}
