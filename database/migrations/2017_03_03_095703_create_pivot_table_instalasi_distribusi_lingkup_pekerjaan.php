<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableInstalasiDistribusiLingkupPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lingkup_distribusi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instalasi_id')->unsigned()->index();
            $table->foreign('instalasi_id')->references('id')->on('instalasi_distribusi');
            $table->integer('lingkup_id')->unsigned()->index();
            $table->foreign('lingkup_id')->references('id')->on('lingkup_pekerjaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lingkup_distribusi');
    }
}
