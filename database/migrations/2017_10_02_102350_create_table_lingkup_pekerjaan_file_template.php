<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLingkupPekerjaanFileTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lingkup_pekerjaan_template', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_lingkup_pekerjaan')->unsigned();
            $table->foreign('id_lingkup_pekerjaan')->references('id')->on('lingkup_pekerjaan');
            $table->integer('id_produk')->unsigned();
            $table->foreign('id_produk')->references('id')->on('produk');
            $table->text('template_data_teknik')->nullable();
            $table->text('template_blangko_inspeksi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lingkup_pekerjaan_template');
    }
}
