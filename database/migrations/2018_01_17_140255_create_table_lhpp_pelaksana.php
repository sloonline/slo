<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLhppPelaksana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lhpp_pelaksana', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 255)->nullable();
            $table->string('nik', 255)->nullable();
            $table->string('no_reg_kompetensi', 255)->nullable();
            $table->string('klasifikasi', 255)->nullable();
            $table->string('level', 255)->nullable();
            $table->string('lhpp_id', 50)->nullable();
            $table->integer('tipe_pelaksana')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lhpp_pelaksana');
    }
}
