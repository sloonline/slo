<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBayPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bay_permohonan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('permohonan_id')->unsigned();
            $table->foreign('permohonan_id')->references('id')->on('permohonan');
            $table->integer('bay_id')->unsigned();
            $table->foreign('bay_id')->references('id')->on('bay_gardu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bay_permohonan');
    }
}
