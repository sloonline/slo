<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableKontrakVersionRab extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('kontrak_version_rab', function (Blueprint $table) {

            $table->increments('id')->unsigned;
            $table->integer('id_kontrak_version')->unsigned();
            $table->foreign('id_kontrak_version')->references('id')->on('kontrak_version');
            $table->integer('id_rab')->unsigned();
            $table->foreign('id_rab')->references('id')->on('rab');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('kontrak_version_rab');
    }

}
