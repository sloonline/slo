<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonilLingkupPekerjaan extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('lingkup_personil', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('personil_inspeksi_id')->unsigned()->index();
      $table->foreign('personil_inspeksi_id')->references('id')->on('personil_inspeksi')->onDelete('cascade');
      $table->integer('tipe_instalasi_id')->unsigned()->index();
      $table->foreign('tipe_instalasi_id')->references('id')->on('tipe_instalasi')->onDelete('cascade');
      $table->integer('lingkup_pekerjaan_id')->unsigned()->index();
      $table->foreign('lingkup_pekerjaan_id')->references('id')->on('lingkup_pekerjaan')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::drop('lingkup_personil');
  }
}
