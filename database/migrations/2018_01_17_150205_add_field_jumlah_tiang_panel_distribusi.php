<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJumlahTiangPanelDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->integer('jumlah_tiang')->nullable();
            $table->integer('jumlah_panel')->nullable();
            $table->dropColumn('id_jenis_instalasi_distribusi');
            $table->dropForeign('id_program');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('jumlah_tiang');
            $table->dropColumn('jumlah_panel');
            $table->integer('id_jenis_instalasi_distribusi')->nullable();
            $table->foreign("id_program")->references('id')->on('jenis_program');
        });
    }
}
