<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstalasiPemanfaatanTm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalasi_pemanfaatan_tm', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/

            $table->integer('id_jenis_instalasi')->unsigned();
            $table->foreign('id_jenis_instalasi')->references('id')->on('jenis_instalasi');
            $table->string('nama_instalasi', 150);
            $table->text('alamat_instalasi')->nullable();
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');
            
            $table->string('longitude_awal', 200)->nullable();
            $table->string('latitude_awal', 200)->nullable();
            $table->string('longitude_akhir', 200)->nullable();
            $table->string('latitude_akhir', 200)->nullable();
            $table->integer('kapasitas_trafo')->nullable();
            $table->integer('daya_sambung')->nullable();
            $table->string('phb_tm',50)->nullable();
            $table->string('phb_tr',50)->nullable();
            $table->string('penyedia_tl',50)->nullable();
            $table->integer('tegangan_pengenal')->nullable();  //tegangan di tabel reference


            $table->timestamps();
            $table->softDeletes();
            $table->index(['id','nama_instalasi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalasi_pemanfaatan_tm');
    }
}
