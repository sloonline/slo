<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAlamatAkhirTabelDistribusiPemanfaatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->text('alamat_akhir_instalasi')->nullable();
            $table->integer('id_provinsi_akhir')->unsigned()->nullable();
            $table->foreign('id_provinsi_akhir')->references('id')->on('provinces');
            $table->integer('id_kota_akhir')->unsigned()->nullable();
            $table->foreign('id_kota_akhir')->references('id')->on('cities');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->text('alamat_akhir_instalasi')->nullable();
            $table->integer('id_provinsi_akhir')->unsigned()->nullable();
            $table->foreign('id_provinsi_akhir')->references('id')->on('provinces');
            $table->integer('id_kota_akhir')->unsigned()->nullable();
            $table->foreign('id_kota_akhir')->references('id')->on('cities');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->text('alamat_akhir_instalasi')->nullable();
            $table->integer('id_provinsi_akhir')->unsigned()->nullable();
            $table->foreign('id_provinsi_akhir')->references('id')->on('provinces');
            $table->integer('id_kota_akhir')->unsigned()->nullable();
            $table->foreign('id_kota_akhir')->references('id')->on('cities');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('alamat_akhir_instalasi');
            $table->dropForeign('id_provinsi_akhir');
            $table->dropColumn('id_provinsi_akhir');
            $table->dropForeign('id_kota_akhir');
            $table->dropColumn('id_kota_akhir');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('alamat_akhir_instalasi');
            $table->dropForeign('id_provinsi_akhir');
            $table->dropColumn('id_provinsi_akhir');
            $table->dropForeign('id_kota_akhir');
            $table->dropColumn('id_kota_akhir');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('alamat_akhir_instalasi');
            $table->dropForeign('id_provinsi_akhir');
            $table->dropColumn('id_provinsi_akhir');
            $table->dropForeign('id_kota_akhir');
            $table->dropColumn('id_kota_akhir');
        });
    }
}
