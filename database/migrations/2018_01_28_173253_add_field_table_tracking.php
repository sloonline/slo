<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTableTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tracking', function (Blueprint $table) {
            $table->integer('lpi_id')->nullable();
            $table->integer('lhpp_id')->nullable();
            $table->integer('djk_hasil_id')->nullable();
            $table->integer('tipe_instalasi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracking', function (Blueprint $table) {
            $table->dropColumn('lpi_id');
            $table->dropColumn('lhpp_id');
            $table->dropColumn('djk_hasil_id');
            $table->dropColumn('tipe_instalasi');
        });
    }
}
