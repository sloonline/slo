
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusBaruInstalasiPembangkitTransmisiTabelInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //status baru menandakan apakah instalasi tersebut baru atau lama
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->integer('status_baru')->default(1);
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->integer('status_baru')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('status_baru');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('status_baru');
        });
    }
}
