<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermohonanRls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_rls', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nomor_permintaan_rls', 255);
            $table->string('tgl_permintaan', 55);
            $table->integer('manager')->unsigned();
            $table->foreign('manager')->references('id')->on('users')->onDelete('cascade');
            $table->integer('deputi_manager')->unsigned();
            $table->foreign('deputi_manager')->references('id')->on('users')->onDelete('cascade');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->string('tgl_inspeksi', 55);
            $table->string('sys_status', 20);
            $table->integer('id_flow_status')->unsigned();
            $table->foreign('id_flow_status')->references('id')->on('flow_status');
            $table->integer('rls_id')->unsigned();
            $table->foreign('rls_id')->references('id')->on('rls');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan_rls');
    }
}
