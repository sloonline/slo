<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field permohonan*/
            $table->integer('id_orders')->unsigned();
            $table->foreign('id_orders')->references('id')->on('orders');
            $table->string('nomor_permohonan', 50);
            $table->string('tanggal_permohonan')->nullable();
            $table->integer('id_lingkup_pekerjaan')->nullable();
            $table->foreign('id_lingkup_pekerjaan')->references('id')->on('lingkup_pekerjaan');
            $table->integer('id_jenis_usaha')->nullable(); // ambil ditabel reference id 5
            $table->string('nomor_ijin_usaha', 50)->nullable();
            $table->date('masa_ijin_usaha')->nullable();
            $table->text('file_ijin_usaha')->nullable();
            $table->string('nama_kontrak', 150)->nullable();
            $table->string('nomor_kontrak', 50)->nullable();
            $table->date('tanggal_kontrak')->nullable(); //pengesahan
            $table->date('masa_kontrak')->nullable(); //masa berlaku kontrak
            $table->text('file_kontrak')->nullable();
            $table->string('nomor_sp_jbtl', 50)->nullable(); //nomor surat perjanjian jual beli tenaga listrik
            $table->date('tanggal_sp_jbtl')->nullable(); //tanggal pengesahan surat perjanjian jual beli tenaga listrik
            $table->date('masa_sp_jbtl')->nullable(); //masa berlaku surat perjanjian jual beli tenaga listrik
            $table->text('file_sp_jbtl')->nullable(); //filesurat perjanjian jual beli tenaga listrik
            $table->string('nama_instalasi', 150);
            $table->text('alamat_instalasi');
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');
            //$table->integer('id_program')->unsigned()->nullable();
            $table->integer('business_area')->unsigned()->nullable();
            $table->integer('id_lokasi_program')->unsigned()->nullable();
            $table->integer('id_penyulang_area')->unsigned()->nullable();
            $table->integer('id_gardu_area')->unsigned()->nullable();
            $table->text('file_upload_lingkup')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'id_orders','nomor_permohonan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan');
    }
}
