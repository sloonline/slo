<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldKapasitasTeganganPengenalTabelBay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->integer('kapasitas')->nullable();
            $table->integer('tegangan_pengenal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->dropColumn('kapasitas');
            $table->dropColumn('tegangan_pengenal');
        });
    }
}
