<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubBidangTipeInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_bidang_tipe_instalasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sub_bidang_id')->unsigned()->index();
            $table->foreign('sub_bidang_id')->references('id')->on('sub_bidang')->onDelete('cascade');
            $table->integer('tipe_instalasi_id')->unsigned()->index();
            $table->foreign('tipe_instalasi_id')->references('id')->on('tipe_instalasi')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_bidang_tipe_instalasi');
    }
}
