<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContractVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('kontrak_version', function (Blueprint $table) {

            $table->increments('id')->unsigned();
            $table->integer('id_kontrak');
            $table->foreign('id_kontrak')->references('id')->on('kontrak');

            $table->string('version', 200)->default(1);
            $table->string('nomor_kontrak', 200)->nullable();
            $table->string('tgl_kontrak', 50)->nullable();
            $table->text('uraian')->nullable();
            $table->integer('nilai_kontrak');
            $table->text('file');

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'id_kontrak']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('kontrak_version');
    }
}
