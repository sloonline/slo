<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePivotPersonilJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_personil', function (Blueprint $table) {
            $table->integer('personil_inspeksi_id')->unsigned()->index();
            $table->foreign('personil_inspeksi_id')->references('id')->on('personil_inspeksi')->onDelete('cascade');
            $table->integer('jabatan_inspeksi_id')->unsigned()->index();
            $table->foreign('jabatan_inspeksi_id')->references('id')->on('jabatan_inspeksi')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jabatan_personil');
    }
}
