<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableJenisInstalasiLingkupPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_instalasi_pekerjaan', function (Blueprint $table) {
            $table->increments('id')->unsigned;
            $table->integer('jenis_instalasi_id')->unsigned();
            $table->foreign('jenis_instalasi_id')->references('id')->on('jenis_instalasi');
            $table->integer('lingkup_pekerjaan_id')->unsigned();
            $table->foreign('lingkup_pekerjaan_id')->references('id')->on('lingkup_pekerjaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jenis_instalasi_pekerjaan');
    }
}
