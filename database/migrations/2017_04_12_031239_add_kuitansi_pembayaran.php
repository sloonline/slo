<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKuitansiPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembayaran', function (Blueprint $table) {
            //
            $table->string('nomor_kuitansi_transfer', 50)->nullable();
            $table->date('tanggal_kuitansi_transfer')->nullable();
            $table->text('file_kuitansi_transfer')->nullable();

            $table->string('nomor_kuitansi_pph23', 50)->nullable();
            $table->date('tanggal_kuitansi_pph23')->nullable();
            $table->text('file_kuitansi_pph23')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembayaran', function (Blueprint $table) {
            //
            $table->dropColumn('nomor_kuitansi_transfer');
            $table->dropColumn('tanggal_kuitansi_transfer');
            $table->dropColumn('file_kuitansi_transfer');

            $table->dropColumn('nomor_kuitansi_pph23');
            $table->dropColumn('tanggal_kuitansi_pph23');
            $table->dropColumn('file_kuitansi_pph23');
        });
    }
}
