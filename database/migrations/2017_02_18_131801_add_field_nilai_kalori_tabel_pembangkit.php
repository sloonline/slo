<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldNilaiKaloriTabelPembangkit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->decimal('kalori_hhv')->nullable();
            $table->decimal('kalori_lhv')->nullable();
            $table->text('data_konsumsi_bahan_bakar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('nilai_kalori_hhv');
            $table->dropColumn('nilai_kalori_lhv');
            $table->dropColumn('data_konsumsi_bahan_bakar');

        });
    }
}
