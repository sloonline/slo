<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBaPendingItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ba_pending_item', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('pending_item_id')->unsigned();
            $table->foreign('pending_item_id')->references('id')->on('pending_item');
            $table->string('no_ba',255)->nullable();
            $table->text('file_ba_pending_item');
            $table->string('tanggal_ba',32);
            $table->integer('hasil_verifikasi');
            $table->string('tanggal_verifikasi', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ba_pending_item');
    }
}
