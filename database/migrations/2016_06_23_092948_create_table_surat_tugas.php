<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSuratTugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_tugas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_perusahaan')->unsigned();
            $table->foreign('id_perusahaan')->references('id')->on('perusahaan');
            $table->integer('periode');
            $table->string('nomor_surat',50);
            $table->string('tanggal_surat');
            $table->text('file_surat');
            $table->string('user_create',50);
            $table->string('user_update',50);

            $table->softDeletes();
            $table->index(['id', 'id_perusahaan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('surat_tugas');
    }
}
