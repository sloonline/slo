<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_perusahaan', 200);
            $table->string('nama_pemimpin_perusahaan', 50);
            $table->string('no_npwp_perusahaan', 50)->nullable();
            $table->text('file_npwp_perusahaan')->nullable();
            $table->text('alamat_perusahaan');
            $table->integer('id_province')->nullable();
            $table->integer('id_city')->nullable();
            $table->string('kode_pos_perusahaan', 50)->nullable();
            $table->string('no_telepon_perusahaan', 50)->nullable();
            $table->string('no_fax_perusahaan', 50)->nullable();
            $table->string('email_perusahaan', 100)->nullable();
            $table->text('file_siup_perusahaan')->nullable();
            $table->text('file_skdomisili_perusahaan')->nullable();
            $table->text('file_situ_perusahaan')->nullable();
            $table->text('file_tdp_perusahaan')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('kategori_perusahaan')->nullable();
            $table->string('company_code', 20)->nullable();
            $table->string('business_area', 20)->nullable();
            $table->integer('ijin_perusahaan')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //$table->index(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perusahaan');
    }
}
