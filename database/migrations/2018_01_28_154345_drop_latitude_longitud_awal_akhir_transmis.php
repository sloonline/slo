<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropLatitudeLongitudAwalAkhirTransmis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('latitude_awal');
            $table->dropColumn('longitude_awal');
            $table->dropColumn('latitude_akhir');
            $table->dropColumn('longitude_akhir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->string('latitude_awal')->nullable();
            $table->string('longitude_awal')->nullable();
            $table->string('latitude_akhir')->nullable();
            $table->string('longitude_akhir')->nullable();
        });
    }
}
