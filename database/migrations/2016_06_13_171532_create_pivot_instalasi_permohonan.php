<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotInstalasiPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalasi_permohonan', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/

            $table->integer('id_permohonan')->unsigned();
            $table->foreign('id_permohonan')->references('id')->on('permohonan');
            $table->integer('tipe_instalasi')->unsigned();
            $table->integer('id_instalasi');

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id','id_permohonan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalasi_permohonan');
    }
}
