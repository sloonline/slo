<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKickOffMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_kick_off', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->date('tanggal')->nullable();
            $table->string('waktu')->nullable();
            $table->string('notulis',255)->nullable();
            $table->text('rapat_antara')->nullable();
            $table->string('lokasi',255)->nullable();
            $table->text('agenda')->nullable();
            $table->text('file_kick_off')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('laporan_kick_off');
    }
}
