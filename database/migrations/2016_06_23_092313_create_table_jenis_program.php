<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJenisProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_program', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('jenis_program', 50);
            $table->string('acuan_1', 50);
            $table->string('acuan_2', 50);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jenis_program');
    }
}
