<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStrukturHilo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('struktur_hilo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lingkup_pekerjaan_id')->unsigned();
            $table->foreign('lingkup_pekerjaan_id')->references('id')->on('lingkup_pekerjaan')->onDelete('cascade');
            $table->string('nomor',225)->nullable();
            $table->text('mata_uji')->nullable();
            $table->text('kriteria')->nullable();
            $table->string('is_parent')->nullable();
            $table->string('is_dokumen')->default('0')->nullable();
            $table->integer('sort_num');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('struktur_hilo');
    }
}
