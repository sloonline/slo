<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('users', function (Blueprint $table) {
    		/*$table->increments('id');
    		$table->string('name');
    		$table->string('email')->unique();
    		$table->string('password', 60);
    		$table->rememberToken();
    		$table->timestamps();*/

    		/*$table->increments('id');
    		$table->string('username',30)->unique();
    		$table->string('name',100);
    		$table->string('email')->unique();
    		$table->string('password');
    		$table->integer('active_directory')->default(1);
    		$table->string('ad_display_name',200)->nullable();
    		$table->string('ad_mail',100)->nullable();
    		$table->string('ad_company',100)->nullable();
    		$table->string('ad_department',100)->nullable();
    		$table->string('ad_title',100)->nullable();
    		$table->string('ad_employee_number',100)->nullable();
    		$table->text('address')->nullable();
    		$table->string('phone')->nullable();
    		$table->integer('id_province')->unsigned();
    		$table->foreign('id_province')->references('id')->on('provinces');
    		$table->integer('id_city')->unsigned();
    		$table->foreign('id_city')->references('id')->on('cities');
    		$table->string('level',15);
    		$table->string('status',10)->default('ACTV');
    		$table->rememberToken();
    		$table->timestamps();*/

	        /*$table->primary('id');

    		$table->index(['id', 'username', 'email', 'level', 'status']);*/

    		$table->increments('id')->unsigned();
            $table->integer('id_level_akses')->nullable();
    		$table->string('email', 50);
			$table->string('username', 50)->unique();
    		$table->string('nama_user', 50);
    		$table->string('tempat_lahir_user', 50)->nullable();
    		$table->string('gender_user', 50)->nullable();
    		$table->string('tanggal_lahir')->nullable();
    		$table->text('alamat_user')->nullable();
    		$table->string('no_ktp_user', 50)->nullable();
    		$table->string('no_hp_user', 50)->nullable();
            $table->string('password', 50)->nullable();
            $table->string('status_pernikahan', 50)->nullable();
            $table->string('pendidikan_user', 50)->nullable();
            $table->string('token_confirmation', 50)->nullable();
            $table->string('no_registrasi', 50)->nullable();
			$table->integer('is_pln')->nullable();
			$table->integer('is_user_ad')->nullable();


    		/*field user external*/
    		$table->text('file_npwp_user')->nullable();
    		$table->text('file_ktp_user')->nullable();
    		$table->text('file_foto_user')->nullable();
            $table->integer('perusahaan_id')->nullable();
    		/*--end field user internal*/

    		/*field untuk user internal*/
    		$table->string('nip_user', 50)->nullable();
    		$table->string('grade_user', 50)->nullable();
            $table->string('jabatan_user')->nullable();
    		$table->string('status_pekerja')->nullable();
            $table->integer('id_bidang')->nullable();
            $table->integer('id_sub_bidang')->nullable();
            //$table->integer('status_user')->nullable();
			//$table->string('keterangan_pending')->nullable();
    		/*--end field user internal*/

    		$table->timestamps();
            $table->softDeletes();
    		$table->index(['id', 'nama_user', 'email']);

    	});
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('users');
    }
}
