<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rab', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_rab_permohonan')->unsigned()->index();
            $table->foreign('id_rab_permohonan')->references('id')->on('rab_permohonan')->onDelete('cascade');
            $table->integer('id_unsur_biaya')->unsigned()->index();
            $table->foreign('id_unsur_biaya')->references('id')->on('unsur_biaya')->onDelete('cascade');
            $table->integer('kantor_hari');
            $table->integer('kantor_orang');
            $table->integer('lapangan_hari');
            $table->integer('lapangan_orang');
            $table->integer('jumlah');
            $table->integer('jumlah_biaya');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rab');
    }
}
