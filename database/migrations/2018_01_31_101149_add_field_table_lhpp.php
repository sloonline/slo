<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTableLhpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->string('no_djk_agenda', 255)->nullable();
            $table->string('no_djk_permohonan', 255)->nullable();
            $table->string('tgl_lhpp', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->dropColumn('no_djk_agenda');
            $table->dropColumn('no_djk_permohonan');
            $table->dropColumn('tgl_lhpp');
        });
    }
}
