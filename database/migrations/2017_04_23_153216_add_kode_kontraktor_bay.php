<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKodeKontraktorBay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->string('kode_kontraktor', 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->dropColumn('kode_kontraktor');
        });
    }
}
