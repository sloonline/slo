<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJangkawaktuPembayaranPengalihanTableWbsio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->date('tgl_awal')->nullable();
            $table->date('tgl_akhir')->nullable();
//            $table->integer('pembayaran_pengalihan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropColumn('tgl_awal');
            $table->dropColumn('tgl_akhir');
//            $table->dropColumn('pembayaran_pengalihan');
        });
    }
}
