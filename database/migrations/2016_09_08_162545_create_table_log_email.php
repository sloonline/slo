<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //tabel untuk menampung log keluar masuknya email yg berkaitan dengan sistem
    public function up()
    {
        Schema::create('log_email', function (Blueprint $table) {
            $table->increments('id');
            $table->string('to');
            $table->string('to_name');
            $table->string('subject');
            $table->string('file_view');
            $table->string('email_data_alias');
            $table->text('email_data'); //email data berbentuk json yg nantinya berisi data yg akan di assign ke view yg dipakai di email
            $table->string('modul'); //menyimpan keterangan aksi email untuk modul apa (ex: user, order,etc)
            $table->string('action'); //menyimpan aksi keperluan email (ex : created ->created user, approved->approved_user,etc)
            $table->string('description'); //email description purpose
            $table->string('status'); //sent or not
            $table->string('status_message'); //sent or not
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_email');
    }
}
