<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFileSbujkIujkSldTableBayGardu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->text('file_sbujk')->nullable();
            $table->text('file_iujk')->nullable();
            $table->text('file_sld')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->dropColumn('file_sbujk');
            $table->dropColumn('file_iujk');
            $table->dropColumn('file_sld');
        });
    }
}
