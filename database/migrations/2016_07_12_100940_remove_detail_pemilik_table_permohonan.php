<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDetailPemilikTablePermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan', function (Blueprint $table) {
            $table->dropForeign('permohonan_id_provinsi_foreign');
            $table->dropForeign('permohonan_id_kota_foreign');
            $table->dropColumn([
                'id_jenis_usaha',
                'nomor_ijin_usaha',
                'masa_ijin_usaha',
                'file_ijin_usaha',
                'nomor_sp_jbtl',
                'tanggal_sp_jbtl',
                'masa_sp_jbtl',
                'file_sp_jbtl',
                'id_provinsi',
                'id_kota',
                'nama_instalasi',
                'alamat_instalasi',
                'nama_kontrak',
                'nomor_kontrak',
                'tanggal_kontrak',
                'masa_kontrak',
                'file_kontrak'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan', function (Blueprint $table) {
            $table->integer('id_jenis_usaha')->nullable(); // ambil ditabel reference id 5
            $table->string('nomor_ijin_usaha', 50)->nullable();
            $table->date('masa_ijin_usaha')->nullable();
            $table->text('file_ijin_usaha')->nullable();
            $table->string('nomor_sp_jbtl', 50)->nullable(); //nomor surat perjanjian jual beli tenaga listrik
            $table->date('tanggal_sp_jbtl')->nullable(); //tanggal pengesahan surat perjanjian jual beli tenaga listrik
            $table->date('masa_sp_jbtl')->nullable(); //masa berlaku surat perjanjian jual beli tenaga listrik
            $table->text('file_sp_jbtl')->nullable(); //filesurat perjanjian jual beli tenaga listrik
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');
            $table->string('nama_instalasi', 150);
            $table->text('alamat_instalasi');
            $table->string('nama_kontrak', 150)->nullable();
            $table->string('nomor_kontrak', 50)->nullable();
            $table->date('tanggal_kontrak')->nullable(); //pengesahan
            $table->date('masa_kontrak')->nullable(); //masa berlaku kontrak
            $table->text('file_kontrak')->nullable();
        });
    }
}
