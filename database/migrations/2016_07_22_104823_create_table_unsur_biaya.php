<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUnsurBiaya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_unsur_biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->timestamps();
        });

        Schema::create('unsur_biaya', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('kode', 20);
            $table->string('unsur', 200);
            $table->integer('tarif');
            $table->integer('detail_unsur_id')->unsigned();
            $table->foreign('detail_unsur_id')->references('id')->on('detail_unsur_biaya');
            //$table->string('jenis',200);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'kode', 'unsur']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_unsur_biaya');
        Schema::drop('unsur_biaya');
    }
}
