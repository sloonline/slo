<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusBaruInstalasiPemanfaatanTabelInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->integer('status_baru')->default(1);
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->integer('status_baru')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('status_baru');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('status_baru');
        });
    }
}
