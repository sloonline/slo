<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixForeignPersonilOnKeahlianPersonil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keahlian_personil', function (Blueprint $table) {
            $table->dropForeign(['personil_id']);
            $table->foreign('personil_id')->references('id')->on('personil_inspeksi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keahlian_personil', function (Blueprint $table) {
            $table->dropForeign(['personil_id']);
            $table->foreign('personil_id')->references('id')->on('keahlian');
        });
    }
}
