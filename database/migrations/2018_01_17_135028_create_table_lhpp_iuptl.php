<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLhppIuptl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lhpp_iuptl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 50)->nullable();
            $table->text('alamat')->nullable();
            $table->integer('id_provinsi')->nullable();
            $table->integer('id_kota')->nullable();
            $table->string('no_iujptl', 50)->nullable();
            $table->string('kode_badan_usaha', 50)->nullable();
            $table->string('klasifikasi', 50)->nullable();
            $table->string('kualifikasi', 50)->nullable();
            $table->string('masa_berlaku', 50)->nullable();
            $table->string('nama_pj_perusahaan', 50)->nullable();
            $table->string('penerbit_ijin', 50)->nullable();
            $table->integer('lhpp_id')->nullable();
            $table->integer('tipe_iuptl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lhpp_iuptl');
    }
}
