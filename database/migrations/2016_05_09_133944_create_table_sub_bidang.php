<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubBidang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_bidang', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_sub_bidang', 50);
            $table->integer('id_bidang')->unsigned();
            $table->foreign('id_bidang')->references('id')->on('bidang');
            $table->integer('isAktif')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_bidang');
    }
}
