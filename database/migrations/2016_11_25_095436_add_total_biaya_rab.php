<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalBiayaRab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->integer('jumlah_biaya')->default(0);
            $table->integer('ppn')->default(0);
            $table->integer('total_biaya')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->dropColumn('jumlah_biaya');
            $table->dropColumn('ppn');
            $table->dropColumn('total_biaya');
        });
    }
}
