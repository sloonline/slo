<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFileLandasanTableWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->text('file_landasan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropColumn('file_landasan');
        });
    }
}
