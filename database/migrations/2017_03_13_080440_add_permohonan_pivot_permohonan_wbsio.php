<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermohonanPivotPermohonanWbsio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('permohonan_wbsio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permohonan_id')->unsigned()->index();
            $table->foreign('permohonan_id')->references('id')->on('permohonan');
            $table->integer('wbsio_id')->unsigned()->index();
            $table->foreign('wbsio_id')->references('id')->on('wbs_io');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan_wbsio');
    }
}
