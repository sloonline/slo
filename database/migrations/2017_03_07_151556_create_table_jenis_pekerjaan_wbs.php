<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJenisPekerjaanWbs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //alter foreign key table wbs
        Schema::create('jenis_pekerjaan_wbs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('jenis_pekerjaan');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->integer('jenis_pekerjaan_wbs_id')->nullable();
            $table->foreign('jenis_pekerjaan_wbs_id')->references('id')->on('jenis_pekerjaan_wbs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wbs_io', function (Blueprint $table) {
            $table->dropForeign('jenis_pekerjaan_id');
            $table->dropColumn('jenis_pekerjaan_id');
        });

        Schema::drop('jenis_pekerjaan_wbs');
    }
}
