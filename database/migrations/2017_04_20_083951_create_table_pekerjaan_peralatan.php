<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePekerjaanPeralatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjaan_peralatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peralatan_id')->unsigned();
            $table->foreign('peralatan_id')->references('id')->on('peralatan')->onDelete('cascade');
            $table->integer('permohonan_peralatan_id')->unsigned();
            $table->foreign('permohonan_peralatan_id')->references('id')->on('permohonan_peralatan')->onDelete('cascade');
            $table->string('return_by',255)->nullable();
            $table->integer('status')->unsigned()->nullable();
            $table->foreign('status')->references('id')->on('references')->onDelete('cascade');
            $table->string('keterangan',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pekerjaan_peralatan');
    }
}
