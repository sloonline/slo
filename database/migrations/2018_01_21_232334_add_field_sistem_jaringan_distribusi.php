<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSistemJaringanDistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->integer('sistem_jaringan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('sistem_jaringan');
        });
    }
}
