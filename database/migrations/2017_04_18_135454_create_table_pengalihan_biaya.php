<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengalihanBiaya extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengalihan_biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wbs_id')->unsigned();
            $table->foreign('wbs_id')->references('id')->on('wbs_io');
            $table->date('tgl_pengalihan');
            $table->integer('nilai', 50);
            $table->string('surat_pengalihan')->nullable();
            $table->text('file_pengalihan')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pengalihan_biaya');
    }
}
