<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBusinessArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_area', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('company_code',4);
            $table->foreign('company_code')->references('company_code')->on('company_code');
            $table->string('business_area',4)->unique();
            $table->string('description');
            $table->string('short_text',100)->nullable();
            $table->string('long_text')->nullable();
            $table->string('city',100)->nullable();
            $table->string('address',200)->nullable();

            $table->string('status',10)->default('ACTV');
            $table->timestamps();


            $table->index(['id', 'company_code', 'business_area', 'status']);
        });
       /* Schema::table('users', function (Blueprint $table) {
            $table->integer('id_business_area')->unsigned()->nullable();
            $table->foreign('id_business_area')->references('business_area')->on('id');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        $table->dropForeign('id_business_area');
        $table->dropColumn('id_business_area');
    });
        Schema::drop('business_area');
    }
}
