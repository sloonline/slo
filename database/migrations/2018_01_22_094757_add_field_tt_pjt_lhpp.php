<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTtPjtLhpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->string('nik_pjt', 50)->nullable();
            $table->string('nik_tt', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->dropColumn('nik_pjt');
            $table->dropColumn('nik_tt');
        });
    }
}
