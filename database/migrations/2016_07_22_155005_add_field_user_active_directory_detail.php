<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUserActiveDirectoryDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("ad_display_name")->nullable();
            $table->string("ad_mail")->nullable();
            $table->string("ad_company")->nullable();
            $table->string("ad_department")->nullable();
            $table->string("ad_title")->nullable();
            $table->string("ad_employee_number")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("ad_display_name")->nullable();
            $table->dropColumn("ad_mail")->nullable();
            $table->dropColumn("ad_company")->nullable();
            $table->dropColumn("ad_department")->nullable();
            $table->dropColumn("ad_title")->nullable();
            $table->dropColumn("ad_employee_number")->nullable();
        });
    }
}
