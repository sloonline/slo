<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBeritaAcaraInspeksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ba_inspeksi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->string('hari');
            $table->string('tanggal');
            $table->string('tahun_terbilang');
            $table->string('dokumen_pendukung');
            $table->text('file_berita_acara')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ba_inspeksi');
    }
}
