<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLhppKit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lhpp_kit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lhpp_id');
            $table->string('kalori_hhv', 255)->nullable();
            $table->string('kalori_lhv', 255)->nullable();
            $table->string('kapasitas_hasil_uji', 255)->nullable();
            $table->text('url_file_konsumsi_bb')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lhpp_kit');
    }
}
