<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJabatanInspeksiStatusPekerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_inspeksi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_jabatan',200);
            $table->integer('parent')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'nama_jabatan']);
        });

        Schema::create('status_pekerja', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('status',200);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jabatan_inspeksi');
        Schema::drop('status_pekerja');
    }
}
