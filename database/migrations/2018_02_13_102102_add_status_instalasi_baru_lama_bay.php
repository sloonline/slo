<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusInstalasiBaruLamaBay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->string('status_baru')->nullable();
            $table->text('surat_pernyataan_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bay_gardu', function (Blueprint $table) {
            $table->dropColumn('status_baru');
            $table->dropColumn('surat_pernyataan_status');
        });
    }
}
