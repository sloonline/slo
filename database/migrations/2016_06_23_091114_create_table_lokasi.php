<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLokasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokasi', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('business_area')->unsigned();
            $table->foreign('business_area')->references('id')->on('business_area');
            $table->text('lokasi');

            $table->softDeletes();
            $table->index(['id', 'business_area']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lokasi');
    }
}
