<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRealisasiWbsIo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realisasi_wbs_io', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wbs_io_id');
            $table->string('nomor_wbs_io',25);
            $table->string('tgl_upload', 25);
            $table->string('posting_date', 25);
            $table->float('jumlah');
            $table->string('deskripsi', 100);
            $table->string('username', 25);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('realisasi_wbs_io');
    }
}
