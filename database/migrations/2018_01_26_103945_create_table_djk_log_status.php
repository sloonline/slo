<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDjkLogStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('djk_log_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id');
            $table->text('keterangan');
            $table->integer('user_id');
            $table->integer('djk_agenda_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('djk_log_status');
    }
}
