<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeenByTableWorkflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //status seen by untuk menentukan siapa yang bisa lihat process tersebut, apakah internal, eksternal atau keduanya
    public function up()
    {
        Schema::table('workflow', function (Blueprint $table) {
            $table->string('seen_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflow', function (Blueprint $table) {
            $table->dropColumn('seen_by');
        });
    }
}
