<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSkkiSkkoPrkTableRab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->string('no_skki')->nullable();
            $table->string('no_skko')->nullable();
            $table->string('no_prk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rab', function (Blueprint $table) {
            $table->dropColumn('no_skki');
            $table->dropColumn('no_skko');
            $table->dropColumn('no_prk');
        });
    }
}
