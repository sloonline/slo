<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_code', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code', 4)->unique();
            $table->string('description');
            $table->string('short_text',100)->nullable();
            $table->string('long_text')->nullable();

            $table->string('status',10)->default('ACTV');
            $table->timestamps();


            $table->index(['id', 'company_code', 'status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_code');
    }
}
