<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTipePemilikInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            //milik sendiri, sudah terdaftar, belum terdaftar
            $table->string('tipe_pemilik')->nullable();
        });

        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            //milik sendiri, sudah terdaftar, belum terdaftar
            $table->string('tipe_pemilik')->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            //milik sendiri, sudah terdaftar, belum terdaftar
            $table->string('tipe_pemilik')->nullable();
        });

        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            //milik sendiri, sudah terdaftar, belum terdaftar
            $table->string('tipe_pemilik')->nullable();
        });

        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            //milik sendiri, sudah terdaftar, belum terdaftar
            $table->string('tipe_pemilik')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('tipe_pemilik');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('tipe_pemilik');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            $table->dropColumn('tipe_pemilik');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            $table->dropColumn('tipe_pemilik');
        });
        Schema::table('instalasi_distribusi', function (Blueprint $table) {
            $table->dropColumn('tipe_pemilik');
        });
    }
}
