<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTipeInstalasiIdProdukIdInstalasiStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan', function (Blueprint $table) {
            $table->integer('id_tipe_instalasi')->nullable();
            $table->foreign('id_tipe_instalasi')->references('id')->on('tipe_instalasi');
            $table->integer('id_produk')->nullable();
            $table->foreign('id_produk')->references('id')->on('produk');
            $table->integer('id_instalasi')->nullable();
            $table->string('status',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan', function (Blueprint $table) {
            $table->dropColumn('id_tipe_instalasi');
            $table->dropColumn('id_produk');
            $table->dropColumn('id_instalasi');
            $table->dropColumn('status');
        });
    }
}
