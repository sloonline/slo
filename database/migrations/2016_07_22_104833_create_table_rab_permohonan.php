<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRabPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rab_permohonan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_permohonan')->unsigned()->index();
            $table->foreign('id_permohonan')->references('id')->on('permohonan')->onDelete('cascade');
            $table->string('no_dokumen', 20);

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'no_dokumen']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rab_permohonan');
    }
}
