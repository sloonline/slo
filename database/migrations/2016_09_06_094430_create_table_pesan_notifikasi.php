<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePesanNotifikasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan_notifikasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("status_notif_id")->unsigned();
            $table->foreign("status_notif_id")->references("id")->on("status_notif");
            $table->string("keterangan");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pesan_notifikasi');
    }
}
