<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusTbLhpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->string('status_permohonan_djk', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lhpp', function (Blueprint $table) {
            $table->dropColumn('status_permohonan_djk');
        });
    }
}
