<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLaporanPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_pekerjaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->string('nomor_laporan', 128);
            $table->string('tanggal_surat', 64);
            $table->text('file_laporan_inspeksi');
            $table->string('tanggal_actual_mulai', 64);
            $table->string('tanggal_actual_selesai', 64);
            $table->integer('pemeriksaan_dokumen');
            $table->integer('pemeriksaan_design');
            $table->integer('pemeriksaan_visual');
            $table->integer('evaluasi_hasil_komisioning');
            $table->integer('pengujian_sistem');
            $table->integer('dampak_lingkungan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('laporan_pekerjaan');
    }
}
