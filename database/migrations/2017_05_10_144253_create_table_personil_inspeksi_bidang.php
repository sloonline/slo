<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonilInspeksiBidang extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('personil_bidang', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('personil_inspeksi_id')->unsigned()->index();
      $table->foreign('personil_inspeksi_id')->references('id')->on('personil_inspeksi')->onDelete('cascade');
      $table->integer('bidang_id')->unsigned()->index();
      $table->foreign('bidang_id')->references('id')->on('bidang')->onDelete('cascade');
      $table->integer('sub_bidang_id')->unsigned()->index();
      $table->foreign('sub_bidang_id')->references('id')->on('sub_bidang')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::drop('personil_bidang');
  }
}
