<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            //
            $table->string('created_by',50)->nullable();
            $table->foreign('created_by')->references('username')->on('users');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            //
            $table->string('created_by',50)->nullable();
            $table->foreign('created_by')->references('username')->on('users');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            //
            $table->string('created_by',50)->nullable();
            $table->foreign('created_by')->references('username')->on('users');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            //
            $table->string('created_by',50)->nullable();
            $table->foreign('created_by')->references('username')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            //
            $table->dropColumn('created_by');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            //
            $table->dropColumn('created_by');
        });
        Schema::table('instalasi_pemanfaatan_tt', function (Blueprint $table) {
            //
            $table->dropColumn('created_by');
        });
        Schema::table('instalasi_pemanfaatan_tm', function (Blueprint $table) {
            //
            $table->dropColumn('created_by');
        });
    }
}
