<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldWaktuPelaksanaanDjkAgenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('djk_agenda', function (Blueprint $table) {
            $table->string('waktu_pelaksanaan', 255)->nullable();
            $table->integer('jenis_instalasi_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('djk_agenda', function (Blueprint $table) {
            $table->dropColumn('waktu_pelaksanaan');
            $table->dropColumn('jenis_instalasi_id');
        });
    }
}
