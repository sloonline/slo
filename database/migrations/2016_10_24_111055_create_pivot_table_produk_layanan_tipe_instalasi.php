<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableProdukLayananTipeInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_tipe_instalasi', function (Blueprint $table) {
            $table->integer('produk_id')->unsigned()->index();
            $table->foreign('produk_id')->references('id')->on('produk')->onDelete('cascade');

            $table->integer('tipe_instalasi_id')->unsigned()->index();
            $table->foreign('tipe_instalasi_id')->references('id')->on('tipe_instalasi')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produk_tipe_instalasi');
    }
}
