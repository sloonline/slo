<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLatitudeLongitudeDecimalInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->string('lat_awal')->nullable();
            $table->string('long_awal')->nullable();
            $table->string('lat_akhir')->nullable();
            $table->string('long_akhir')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instalasi_pembangkit', function (Blueprint $table) {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
        Schema::table('instalasi_transmisi', function (Blueprint $table) {
            $table->dropColumn('lat_awal');
            $table->dropColumn('long_awal');
            $table->dropColumn('lat_akhir');
            $table->dropColumn('long_akhir');
        });
    }
}
