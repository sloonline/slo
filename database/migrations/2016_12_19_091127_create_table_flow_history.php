<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFlowHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flow_history', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('ref_id');
            $table->string('ref_table');
            $table->string('modul');
            $table->string('status');
            $table->text('keterangan');
            $table->text('attachment')->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flow_history');
    }
}
