<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUrlFilePermohonanDjk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permohonan_djk', function (Blueprint $table) {
            $table->integer('url_detail_lhpp')->nullable();
            $table->integer('url_foto_pelaksanaan')->nullable();
            $table->integer('url_file_konsumsi_bahan_bakar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permohonan_djk', function (Blueprint $table) {
            $table->dropColumn('url_detail_lhpp');
            $table->dropColumn('url_foto_pelaksanaan');
            $table->dropColumn('url_file_konsumsi_bahan_bakar');
        });
    }
}
