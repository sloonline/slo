<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKombinasiPekerjaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kombinasi_pekerjaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tipe_pekerjaan_id')->unsigned();
            $table->foreign('tipe_pekerjaan_id')->references('id')->on('tipe_instalasi');
            $table->integer('jenis_pekerjaan_id')->unsigned();
            $table->foreign('jenis_pekerjaan_id')->references('id')->on('jenis_pekerjaan');
            $table->integer('child_id')->unsigned();
            $table->foreign('child_id')->references('id')->on('jenis_pekerjaan');
            $table->string('keterangan', 50);

            $table->softDeletes();
            $table->index(['id', 'tipe_pekerjaan_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kombinasi_pekerjaan');
    }
}
