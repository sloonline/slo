<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBayGardu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bay_gardu', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nama_bay');
            $table->string('jenis_bay');
            $table->integer('pemilik_id')->unsigned();
            $table->foreign('pemilik_id')->references('id')->on('pemilik_instalasi');
            $table->string('tipe_pemilik');
            $table->integer('instalasi_id')->unsigned();
            $table->foreign('instalasi_id')->references('id')->on('instalasi_transmisi');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bay_gardu');
    }
}
