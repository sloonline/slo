<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstalasiTransmisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalasi_transmisi', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/

            $table->integer('id_jenis_instalasi')->unsigned();
            $table->foreign('id_jenis_instalasi')->references('id')->on('jenis_instalasi');
            $table->string('nama_instalasi', 150);
            $table->text('alamat_instalasi')->nullable();
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinces');
            $table->integer('id_kota')->unsigned();
            $table->foreign('id_kota')->references('id')->on('cities');

            $table->string('longitude_awal', 200)->nullable();
            $table->string('latitude_awal', 200)->nullable();
            $table->string('longitude_akhir', 200)->nullable();
            $table->string('latitude_akhir', 200)->nullable();
            $table->integer('kapasitas_gi')->nullable();

            $table->string('bay_line',100)->nullable();
            $table->string('bay_bus_coupler',100)->nullable();
            $table->string('bay_tf_tenaga',100)->nullable();
            $table->string('bay_reaktor',100)->nullable();
            $table->string('bay_kapasitor',100)->nullable();
            $table->string('sutet',100)->nullable();
            $table->string('sktt',100)->nullable();
            $table->string('sklt',100)->nullable();
            $table->string('panjang_tt',100)->nullable();
            $table->string('jml_tower',100)->nullable();
            $table->integer('sis_jar_tower')->nullable(); //sistem jaringan tower di reference

            $table->integer('kap_pemutus_tenaga')->nullable();
            $table->integer('kap_tf_tenaga')->nullable();
            $table->integer('kap_bank_kapasitor')->nullable();
            $table->integer('kap_bank_reaktor')->nullable();
            $table->integer('tegangan_pengenal')->nullable(); //tegangan di tabel reference



            $table->timestamps();
            $table->softDeletes();
            $table->index(['id','nama_instalasi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalasi_transmisi');
    }
}
