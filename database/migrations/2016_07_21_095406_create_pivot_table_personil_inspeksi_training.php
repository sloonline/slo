<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTablePersonilInspeksiTraining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personil_training',function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('personil_id')->unsigned();
            $table->foreign('personil_id')->references('id')->on('personil_inspeksi');
            $table->integer('training_id')->unsigned();
            $table->foreign('training_id')->references('id')->on('training');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personil_training');
    }
}
