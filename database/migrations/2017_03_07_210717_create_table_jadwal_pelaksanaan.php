<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJadwalPelaksanaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_pelaksanaan', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tanggal_mulai', 25);
            $table->string('tanggal_selesai', 25);
            $table->text('file_jadwal')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('kontrak_id')->unsigned();
            $table->foreign('kontrak_id')->references('id')->on('kontrak');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jadwal_pelaksanaan');
    }
}
