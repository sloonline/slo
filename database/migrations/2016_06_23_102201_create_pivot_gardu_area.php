<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotGarduArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gardu_area', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_area_program')->unsigned();
            $table->foreign('id_area_program')->references('id')->on('area_program');
            $table->integer('id_gardu_induk')->unsigned();
            $table->foreign('id_gardu_induk')->references('id')->on('gardu_induk');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gardu_area');
    }
}
