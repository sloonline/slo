<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermohonanLingkup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_lingkup', function (Blueprint $table) {
           $table->increments('id')->unsigned();
           $table->integer('permohonan_id');
           $table->integer('jenis_pekerjaan_id');
           $table->integer('child_id');

           $table->timestamps();
           $table->softDeletes();
          //  $table->index(['id', 'child_id']);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan_lingkup');
    }
}
