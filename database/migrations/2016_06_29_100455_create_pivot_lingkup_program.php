<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotLingkupProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('lingkup_program', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_lokasi_area')->unsigned()->nullable();
            $table->foreign('id_lokasi_area')->references('id')->on('lokasi_area');
            $table->integer('id_penyulang_area')->unsigned()->nullable();
            $table->foreign('id_penyulang_area')->references('id')->on('penyulang_area');
            $table->integer('id_gardu_area')->unsigned()->nullable();
            $table->foreign('id_gardu_area')->references('id')->on('gardu_area');
            $table->integer('id_lingkup_pekerjaan')->unsigned()->nullable();
            $table->foreign('id_lingkup_pekerjaan')->references('id')->on('lingkup_pekerjaan');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lingkup_program');
    }
}
