<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJenisInstalasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_instalasi', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field general order*/
            $table->string('kode_instalasi', 20);
            $table->string('jenis_instalasi', 255);

            $table->integer('tipe_instalasi')->nullable();
            /*
             * TIPE INSTALASI ==========
             * 1 : Pembangkit,
             * 2 : Transmisi,
             * 3 : Distribusi,
             * 4 : Pemanfaatan TT,
             * 5 : Pemanfaatan TM
             * */

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'kode_instalasi']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jenis_instalasi');
    }
}
