<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRolesPermissionChangeToEntrust extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penugasan', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
            $table->dropColumn(['role_id']);
        });
        Schema::drop('user_role');
        Schema::drop('modul_role');
        Schema::drop('roles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('kode', 50);
            $table->string('nama_role', 50);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('user_role', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('user_id')->references('id')->on('user');
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            // $table->foreign('role_id')->references('id')->on('role');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('modul_role', function (Blueprint $table) {
            $table->integer('modul_id')->unsigned()->index();
            $table->foreign('modul_id')->references('id')->on('modul')->onDelete('cascade');
            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('penugasan', function (Blueprint $table) {
            $table->integer('role_id');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }
}
