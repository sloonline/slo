<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFlowStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //master flow status
        Schema::create('flow_status', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('status',200);
            $table->string('tipe_status',200);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'status','tipe_status']);
        });

        //add field flow status to order
        Schema::table('orders',function(Blueprint $table){
            $table->integer('id_flow_status')->unsigned()->nullable();
            $table->foreign('id_flow_status')->references('id')->on('flow_status');
            $table->string('sys_status',200)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flow_status');

        Schema::table('orders',function(Blueprint $table){
            $table->dropForeign(['id_flow_status']);
            $table->dropColumn(['id_flow_status','sys_status']);
        });
    }
}
