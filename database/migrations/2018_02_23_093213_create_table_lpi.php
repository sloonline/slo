<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLpi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lpi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor')->nullable();
            $table->string('tanggal_laporan')->nullable();
            $table->integer('pekerjaan_id')->nullable();
            $table->integer('permohonan_id')->nullable();
            $table->integer('instalasi_id')->nullable();
            $table->integer('tipe_instalasi_id')->nullable();
            $table->integer('bay_id')->nullable();
            $table->integer('jenis_instalasi_id')->nullable();
            $table->foreign('jenis_instalasi_id')->references('id')->on('jenis_instalasi');
            $table->integer('perusahaan_id')->nullable();
            $table->foreign('perusahaan_id')->references('id')->on('perusahaan');
            $table->text('alamat_perusahaan')->nullable();
            $table->text('keterangan')->nullable();
            $table->text('file_lpi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lpi');
    }
}
