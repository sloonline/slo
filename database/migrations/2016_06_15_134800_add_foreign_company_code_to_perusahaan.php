<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCompanyCodeToPerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('perusahaan', function (Blueprint $table) {
            //
            $table->integer('id_company_code')->unsigned()->nullable();
            $table->foreign('id_company_code')->references('id')->on('company_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perusahaan', function (Blueprint $table) {
            //
            $table->dropForeign(['id_company_code']);
            $table->dropColumn('id_company_code');
        });
    }
}
