<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGarduInduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gardu_induk', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('business_area')->unsigned();
            $table->foreign('business_area')->references('id')->on('business_area');
            $table->string('gardu_induk', 50);
            $table->string('jenis_gardu', 2);

            $table->softDeletes();
            $table->index(['id', 'business_area']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gardu_induk');
    }
}
