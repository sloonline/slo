<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rls', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('pekerjaan_id')->unsigned();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->string('nomor_rekomendasi', 64);
            $table->string('tanggal_terbit', 24);
            $table->string('no_srt_tim_inspeksi', 64);
            $table->string('tgl_srt_tim_inspeksi', 24);
            $table->integer('manager_bki')->unsigned();
            $table->foreign('manager_bki')->references('id')->on('users')->onDelete('cascade');
            $table->text('file_rls');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rls');
    }
}
