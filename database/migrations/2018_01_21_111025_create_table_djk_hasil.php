<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDjkHasil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('djk_hasil', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('djk_registrasi_id');
            $table->string('no_registrasi', 255);
            $table->integer('hasil_registrasi', 255);
            $table->string('tanggal_terbit', 255);
            $table->string('tanggal_berakhir', 255);
            $table->text('alasan_penolakan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('djk_hasil');
    }
}
