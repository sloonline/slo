<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotPekerjaanPermohonan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjaan_permohonan', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            /*field pekerjaan permohonan*/
            $table->integer('id_permohonan')->unsigned();
            $table->foreign('id_permohonan')->references('id')->on('permohonan');
            $table->integer('id_jenis_pekerjaan')->unsigned();
            $table->foreign('id_jenis_pekerjaan')->references('id')->on('jenis_pekerjaan');

            $table->timestamps();
            $table->softDeletes();
            $table->index(['id_permohonan', 'id_jenis_pekerjaan']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pekerjaan_permohonan');
    }
}
