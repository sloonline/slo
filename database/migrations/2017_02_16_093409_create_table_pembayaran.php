<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('pembayaran', function (Blueprint $table) {

            $table->increments('id')->unsigned();
            $table->integer('id_kontrak');
            $table->foreign('id_kontrak')->references('id')->on('kontrak');
             $table->string('termine_ke', 3);
             $table->text('bukti');
             $table->text('bukti_pph');
            $table->string('persentase', 5);
            $table->integer('nilai');
            $table->string('status', 20);
            $table->text('keterangan');
            $table->timestamp('tgl_bayar')->nullable();
            $table->timestamp('tgl_tagihan')->nullable();
          
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'id_kontrak']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('pembayaran');
    }
}
