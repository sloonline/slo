<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermohonanDjk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonan_djk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_agenda',255);
            $table->integer('pembangkit_id');
            $table->integer('transmisi_id');
            $table->integer('hasil_registrasi');
            $table->string('no_registrasi', 255);
            $table->string('tanggal_terbit', 255);
            $table->string('tanggal_berakhir', 255);
            $table->text('alasann_penolakan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permohonan_djk');
    }
}
