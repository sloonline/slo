<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistoryPeralatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_peralatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('peralatan_id')->unsigned()->nullable();
            $table->foreign('peralatan_id')->references('id')->on('peralatan')->onDelete('cascade');
            $table->integer('pekerjaan_id')->unsigned()->nullable();
            $table->foreign('pekerjaan_id')->references('id')->on('pekerjaan')->onDelete('cascade');
            $table->integer('permohonan_peralatan_id')->unsigned()->nullable();
            $table->foreign('permohonan_peralatan_id')->references('id')->on('permohonan_peralatan')->onDelete('cascade');
            $table->integer('status')->unsigned();
            $table->foreign('status')->references('id')->on('references')->onDelete('cascade');
            $table->text('keterangan')->nullable();
            $table->string('created_by',255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_peralatan');
    }
}
