<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PekerjaanPersonil extends Model
{
    protected $table='pekerjaan_personil';
    use SoftDeletes;

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function personil(){
        return $this->belongsTo('App\PersonilInspeksi', 'personil_id', 'id');
    }

    public function jabatan(){
        return $this->belongsTo('App\JabatanInspeksi', 'jabatan_id', 'id');
    }

    public static function getPelaksanaInspeksi($pekerjaan_id){
        return PekerjaanPersonil::where('pekerjaan_id',$pekerjaan_id)
                ->whereHas('jabatan', function ($query) {
                        $query->where('nama_jabatan',PELAKSANA_INSPEKSI);
                })->get();
    }

    public static function getTenagateknik($pekerjaan_id){
        return PekerjaanPersonil::where('pekerjaan_id',$pekerjaan_id)
            ->whereHas('jabatan', function ($query) {
                $query->where('nama_jabatan',TENAGA_TEKNIK);
            })->get();
    }
}
