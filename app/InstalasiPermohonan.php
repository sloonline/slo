<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstalasiPermohonan extends Model
{
    protected $table = 'instalasi_permohonan';

    protected $fillable = [
                    'id_permohonan','tipe_instalasi','id_instalasi'
    ];

    public function permohonan(){
        return $this->belongsTo('App\Permohonan', 'id_permohonan', 'id');
    }
    public function instalasi(){

        if ($this->tipe_instalasi==1){
            $string='App\InstalasiPembangkit';}
        else if ($this->tipe_instalasi==2){
            $string='App\InstalasiTransmisi';}
        else if ($this->tipe_instalasi==3){
            $string='App\InstalasiDistribusi';}
        else if ($this->tipe_instalasi==4){
            $string='App\InstalasiPemanfaatanTT';}
        else {
            $string='App\InstalasiPemanfaatanTM';}
        return $this->belongsTo($string, 'id_instalasi', 'id');
    }

}
