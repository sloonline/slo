<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisProgram extends Model
{
    //
    protected $table = 'jenis_program';

    public function program(){
        return $this->hasMany('App\Program', 'id_jenis_program', 'id');
    }

}
