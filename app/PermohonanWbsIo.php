<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermohonanWbsIo extends Model
{
    protected $table = "permohonan_wbsio";

    public function permohonan(){
        return $this->belongsTo('App\Permohonan','permohonan_id','id');
    }
}
