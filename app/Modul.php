<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    protected $table = 'modul';

    public function permission(){
        return $this->hasMany('App\Permission','modul_id','id');
    }
}
