<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GarduInduk extends Model
{
    //
    protected $table = 'gardu_induk';

    public function businessArea(){
        return $this->belongsTo('App\BusinessArea', 'business_area', 'id');
    }

    public function garduArea(){
        return $this->hasMany('App\GarduArea', 'id_gardu_induk', 'id');
    }
}
