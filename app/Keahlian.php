<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Keahlian extends Model
{
    use SoftDeletes;
    protected $table = "keahlian";


    public function personil(){
        return $this->hasMany('App\KeahlianPersonil','keahlian_id','id');
    }
}
