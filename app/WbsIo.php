<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WbsIo extends Model
{
    protected $table = 'wbs_io';
    use SoftDeletes;

    public function kontrak()
    {
        return $this->belongsTo('App\Kontrak');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function jenis_pekerjaan()
    {
        return $this->belongsTo('App\JenisPekerjaanWbs', 'jenis_pekerjaan_wbs_id', 'id');
    }

    public function flow_status()
    {
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public function jenis()
    {
        return $this->belongsTo('App\References');
    }

    public function tipeWbs()
    {
        return $this->belongsTo('App\References', 'tipe', 'id');
    }

    public function statusWbsIo()
    {
        return $this->belongsTo('App\References', 'status', 'id');
    }

    public function businessArea()
    {
        return $this->belongsTo('App\BusinessArea', 'business_area', 'id');
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'perusahaan_id', 'id');
    }

    public function realisasi()
    {
        return $this->hasMany('App\WbsIoRealisasi', 'wbs_io_id', 'id');
    }

    public function rab()
    {
        return $this->hasMany('App\RabWbsIo', 'wbsio_id', 'id');
    }

    public function idRab()
    {
        return $this->hasMany('App\RabWbsIo', 'wbsio_id')->select('rab_id');
    }

    public function permohonan()
    {
        return $this->hasMany('App\PermohonanWbsIo', 'wbsio_id', 'id');
    }

    public function idPermohonan()
    {
        return $this->hasMany('App\PermohonanWbsIo', 'wbsio_id')->select('permohonan_id');
    }


    public static function getColorWarning($wbsio)
    {
        $pembayaran = 0;
        if ($wbsio->jenis->nama_reference == IO) {
            $paid = @$wbsio->kontrak->pembayaranPaid;
            if (sizeof($paid) > 0) $pembayaran = $paid->sum('nilai');
        }
        $nilai = ($wbsio->jenis->nama_reference == WBS) ? $wbsio->nilai : $pembayaran;
        $realisasi = $wbsio->realisasi->sum('jumlah');
        $persen_realisasi = ($nilai == 0) ? 0 : round(($realisasi / $nilai) * 100, 2);
        if ($persen_realisasi > 100) $persen_realisasi = 100;
        $color = 'black';
        if ($persen_realisasi >= 0 && $persen_realisasi <= 30) {
            $color = 'green';
        } else if ($persen_realisasi >= 31 && $persen_realisasi <= 70) {
            $color = 'blue';
        } else if ($persen_realisasi >= 71 && $persen_realisasi <= 90) {
            $color = 'orange';
        } else {
            $color = 'red';
        }
        $sisa = $nilai - $realisasi;
        return array('color' => $color, 'saldo' => $sisa, 'persen_realisasi' => $persen_realisasi, 'nilai' => $nilai);
    }

    public static function isAllowEdit($wbsIo)
    {
        if (@$wbsIo->statusWbsIo->nama_reference != LOCKED) {
            $latest_step = WorkflowController::getLatestHistory($wbsIo->id, $wbsIo->getTable());
            $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
            if ($next == null) {
                return false;
            } else if (sizeof($next) == 0) {
                //perubahan dapat dilakukan jika keseluruhan alur workflow sudah selesai
                return ($latest_step->workflow->next == null) ? true : false;
            } else {
                $submit = $next->where('tipe_status', SUBMITTED)->first();
                $created = $next->where('tipe_status', CREATED)->first();
                return (in_array(array('role_id' => @$submit->role_id), Auth::user()->user_roles_id->toArray()) ||
                    in_array(array('role_id' => @$created->role_id), Auth::user()->user_roles_id->toArray()));
            }
        } else {
            return false;
        }
    }

    public static function isWorkflowCompleted($wbsIo)
    {
        $latest_step = WorkflowController::getLatestHistory($wbsIo->id, $wbsIo->getTable());
        return ($latest_step != null && $latest_step->workflow->next == null) ? true : false;

    }

    public static function isAllowActivate($wbsIo)
    {
        if (@$wbsIo->statusWbsIo->nama_reference == CREATED) {
            $latest_step = WorkflowController::getLatestHistory($wbsIo->id, $wbsIo->getTable());
            $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
//            $user = Role::where('name', 'P-KEU')->first();
            $wbs_status = $wbsIo->statusWbsIo->nama_reference;
            if (sizeof($next) == 0 && $wbs_status == CREATED) {
//                return (in_array(array('role_id' => @$user->id), Auth::user()->user_roles_id->toArray()));
                return User::isCurrUserAllowPermission(PERMISSION_ACTIVATE_WBSIO);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function updateSysStatus($parent)
    {
        //        $obj_model = $parent->kontrak;
        //        $obj_model->sys_status = $parent->sys_status;
        //        $obj_model->save();
        //
        //        return $obj_model;
        return $parent;
    }

    public static function getUser($wbs_io)
    {
        return $wbs_io->order->user;
    }

    public static function generateNoWbs($wbs)
    {
        $jenis = $wbs->jenis->nama_reference;
        $tahun = $wbs->tahun;
        if ($jenis == WBS) {
            /*---------Format WBS------------------*/
            /*format : I.8601.AA.16.XX.ZZZ.60
            I : investasi
            8601 : Kode BusinessArea PLN Pusertif
            AA : tahun project
            16 : Fungsi jasa teknik
            XX : Unit pemberi kerja
            ZZZ : ID nomor project
            60 : PDP jasa internal
            */
            $id = $wbs->id;
            $nomorProject = $id;
            for ($i = 0; $i < (3 - strlen($id)); $i++) {
                $nomorProject = "0" . $nomorProject;
            }
            $unit = $wbs->businessArea->business_area;
            $nomor = "I.8601." . $tahun . ".16." . $unit . "." . $nomorProject . ".60";
        } else {
            $nomor = "";
        }
        return $nomor;
    }

    public static function generateIODescNumber($io)
    {

        //format IO belum dibuat
        /*---------Format IO------------------*/
        /*format : 8601-ID-YY-ZZZZ-AA
        8601 : Kode BusinessArea PLN Pusertif
        AA : tahun project
        YY : Jenis Produk
        ZZZZ : ID nomor project
        ID : Kode perusahaan
        */
        $tahun = @$io->tahun;
        $nomorProject = @$io->nomor_project;
        $kodeProduk = (@$io->jenis_pekerjaan->kode == "") ? "0000" : @$io->jenis_pekerjaan->kode;
        $kodePerusahaan = @$io->perusahaan->id_pelanggan;
        $nomor = "8601-" . $kodePerusahaan . "-" . $kodeProduk . "-" . $nomorProject . "-" . $tahun;
        return $nomor;
    }

    public static function getSPV($wbs_io)
    {
        //        $business_area = $wbs_io->order->user->perusahaan->id_business_area;
        //        $spv = businessAreaUser::where('business_area_id', $business_area)->first();
        $spv = null;
        if ($wbs_io->tipeWbs->nama_reference == TIPE_NORMAL) {
            $kontrak = $wbs_io->kontrak;
            return Kontrak::getSPV($kontrak);
        } else {
            $user = $wbs_io->order->user;
            return User::getSpv($user);
        }

        // $spv = $wbs_io->order->user->perusahaan->perusahaanSpv->spv;

    }

    public static function getMonitoringWbs()
    {
        //ambil semua wbs yang statusnya selain created
        $query = DB::table('wbs_io w')
            ->join('"references" r', 'r.id', '=', 'w.status')
            ->join('"references" rf', 'rf.id', '=', 'w.jenis_id')
            ->where('r.nama_reference', '!=', CREATED)
            ->where('rf.nama_reference', WBS)
            ->whereNotNull('w.nomor')
            ->select('w.*')
            ->get();
        $wbs = WbsIo::hydrate($query);
        return $wbs;
    }

    public static function getMonitoringIo()
    {
        //ambil semua wbs yang statusnya selain created
        $query = DB::table('wbs_io w')
            ->join('"references" r', 'r.id', '=', 'w.status')
            ->join('"references" rf', 'rf.id', '=', 'w.jenis_id')
            ->where('r.nama_reference', '!=', CREATED)
            ->where('rf.nama_reference', IO)
            ->whereNotNull('w.nomor')
            ->select('w.*')
            ->get();
        $io = WbsIo::hydrate($query);
        return $io;
    }

    public static function getActivatedWBSIO()
    {

        $wbsIoActive = WbsIo::whereHas('statusWbsIo', function ($query) {
            $query->where('nama_reference', ACTIVE);
        })->get();

        return $wbsIoActive;
    }


    public static function getActivatedWBS()
    {

        $wbsIoActive = WbsIo::whereHas('statusWbsIo', function ($query) {
            $query->where('nama_reference', ACTIVE);
        })->whereHas('jenis', function ($query) {
            $query->where('nama_reference', WBS);
        })->get();

        return $wbsIoActive;
    }

    public function pengalihan_biaya()
    {
        return $this->hasMany('App\PengalihanBiaya', 'wbs_id', 'id');
    }

    public static function permohonanForPekerjaan()
    {
        $wbs = self::getActivatedWBSIO();
//        dd($wbs);
        $wbs_for_pekerjaan = array();
        foreach ($wbs as $key => $item) {
            $a = array(
                'wbsio_id' => $item->id,
                'nomor_wbsio' => $item->nomor,
                'jadwal_pekerjaan' => @$item->kontrak->jadwal,
                'uraian_pekerjaan' => $item->uraian_pekerjaan,
                'kontrak' => @$item->kontrak,
                'order' => @$item->order
            );
            $available_permohonan = RABPermohonan::whereIn('rab_id', $item->idRab->pluck('rab_id')->toArray())
                ->whereNotIn('permohonan_id', function ($query) {
                    $query->select('permohonan_id')
                        ->from('pekerjaan');
                })->get();
            // $available_permohonan = RABPermohonan::whereIn('rab_id', $item->idRab->pluck('rab_id')->toArray())
            //     ->whereIn('permohonan_id',function($query){
            //         $query->select('permohonan_id')
            //             ->from('pekerjaan');
            //     })->get();

//              $available_permohonan = PermohonanWbsIo::where('wbsio_id', $item->id)
//            ->whereNotIn('permohonan_id',function($query){
//                $query->select('permohonan_id')
//                ->from('pekerjaan');
//            })->get();

            $a['permohonan'] = $available_permohonan;
            array_push($wbs_for_pekerjaan, $a);
        }
//         dd($wbs_for_pekerjaan);
        return $wbs_for_pekerjaan;
    }
}