<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WbsIoRealisasi extends Model
{
    protected $table='realisasi_wbs_io';
    use SoftDeletes;

    public function wbs_io(){
        return $this->belongsTo('App\WbsIo');
    }
}
