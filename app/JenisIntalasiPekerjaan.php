<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisIntalasiPekerjaan extends Model
{
    protected $table = 'jenis_instalasi_pekerjaan';


    public function jenisInstalasi(){
        return $this->belongsTo('App\JenisInstalasi', 'jenis_instalasi_id');
    }

    public function lingkupPekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan', 'lingkup_pekerjaan_id');
    }
}
