<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LhppIuptl extends Model
{
    protected $table='lhpp_iuptl';

    public function lhpp() {
        return $this->belongsTo('App\Lhpp', 'lhpp_id', 'id');
    }
}
