<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class References extends Model
{
  protected $table = 'references';

  protected $fillable = [
    'nama_reference',
    'value',
    'parent'

  ];

  public static function getRef($parent){
    $result = References::where('parent', function($query) use ($parent){
      $query->select('id')
      ->from('references')
      ->where('nama_reference', $parent);
    })->get();

    return $result;
  }


  public static function getRefByNameAndParent($parent,$nama){
    $result = References::where('parent',$parent)->where('nama_reference',$nama)->first();
    return $result;
  }


  public static function getRefByArrNameAndParent($parent,$nama){
    $result = References::where('parent',$parent)->whereIn('nama_reference',$nama)->get();
    return $result;
  }


}
