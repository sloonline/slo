<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisPekerjaanWbs extends Model
{
    use SoftDeletes;
    protected $table = "jenis_pekerjaan_wbs";
}
