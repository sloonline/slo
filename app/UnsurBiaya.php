<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnsurBiaya extends Model
{
    //
    use SoftDeletes;
    protected $table = 'unsur_biaya';

    protected $fillable = [
        'kode',
        'unsur',
        'tarif',
        'jenis',

    ];


}