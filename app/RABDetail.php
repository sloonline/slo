<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RABDetail extends Model
{
//    use SoftDeletes;
    protected $table = 'rab_detail';

    public function rab_permohonan()
    {
        return $this->belongsTo('App\RAB', 'id_rab', 'id');
    }
    public function unsur_biaya()
    {
        return $this->belongsTo('App\UnsurBiaya', 'id_unsur_biaya', 'id');
    }


}