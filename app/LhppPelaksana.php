<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LhppPelaksana extends Model
{
    protected $table='lhpp_pelaksana';

    public function lhpp() {
        return $this->belongsTo('App\Lhpp', 'lhpp_id', 'id');
    }
}
