<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DjkHasil extends Model
{
    protected $table = 'djk_hasil';

    public function djk_registrasi(){
        return $this->belongsTo('App\DjkRegistrasi', 'djk_registrasi_id','id');
    }
}
