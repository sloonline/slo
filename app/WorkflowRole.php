<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkflowRole extends Model
{
    protected $table = 'workflow_role';

    public function workflow(){
        return $this->belongsTo('App\Workflow', 'workflow_id', 'id');
    }

    public function role(){
        return $this->belongsTo('App\Workflow', 'workflow_id', 'id');
    }
}
