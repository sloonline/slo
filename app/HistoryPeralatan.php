<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPeralatan extends Model
{
  //
  protected $table = 'history_peralatan';


  public function pekerjaan(){
    return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
  }

  public function peralatan(){
    return $this->belongsTo('App\Peralatan','peralatan_id','id');
  }

  public function permohonan(){
    return $this->belongsTo('App\PermohonanPeralatan', 'permohonan_peralatan_id', 'id');
  }

  public function status_peralatan(){
    return $this->belongsTo('App\References','status','id');
  }

  public function user(){
    return $this->belongsTo('App\User','created_by','username');

  }
}
