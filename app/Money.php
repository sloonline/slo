<?php

namespace App;

class Money
{
    private $angka;
    public function __construct($angka){
        $this->$angka = $angka;
    }

    public static function rupiah($angka)
    {
        $jadi = "Rp. " . number_format($angka,2,',','.');
        return $jadi;
    }
}
