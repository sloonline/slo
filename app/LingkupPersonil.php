<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkupPersonil extends Model
{
  protected $table = "lingkup_personil";

  public function personil(){
    return $this->belongsTo('App\PersonilInspeksi', 'personil_inspeksi_id', 'id');
  }

  public function jenis_lingkup(){
    return $this->belongsTo('App\LingkupPekerjaan', 'jenis_lingkup_pekerjaan', 'id');
  }

  public function tipe_instalasi(){
    return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi_id', 'id');
  }
}
