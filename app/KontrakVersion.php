<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KontrakVersion extends Model
{
    use SoftDeletes;

    protected $table = "kontrak_version";

    public function kontrak(){
        return $this->belongsTo('App\Kontrak', 'id_kontrak', 'id');
    }

    public function kontrak_version_rab(){
        return $this->hasMany('App\KontrakVersionRAB', 'id_kontrak_version', 'id');
    }
}
