<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class businessAreaUser extends Model
{
    protected $table='business_area_user';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function business_area()
    {
        return $this->belongsTo('App\businessArea', 'business_area_id', 'business_area');
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'business_area_id', 'id');
    }
}
