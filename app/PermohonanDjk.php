<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermohonanDjk extends Model
{
    protected $table  = 'permohonan_djk';

    public function instalasiPembangkit()
    {
        return $this->belongsTo('App\InstalasiPembangkit', 'instalasi_id', 'id');
    }

    public function instalasiTransmisi()
    {
        return $this->belongsTo('App\InstalasiTransmisi', 'instalasi_id', 'id');
    }

    public function instalasiDistribusi()
    {
        return $this->belongsTo('App\InstalasiDistribusi', 'instalasi_id', 'id');
    }

    public function instalasiPemanfaatanTT()
    {
        return $this->belongsTo('App\InstalasiPemanfaatanTT', 'instalasi_id', 'id');
    }

    public function instalasiPemanfaatanTM()
    {
        return $this->belongsTo('App\InstalasiPemanfaatanTM', 'instalasi_id', 'id');
    }

    public function tipeInstalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi', 'id');
    }

    public function statusDjk()
    {
        return $this->belongsTo('App\StatusDjk', 'status_id', 'id');
    }

    public function instalasi(){
        if ($this->tipe_instalasi == 1) {
            return $this->instalasiPembangkit();
        } elseif ($this->tipe_instalasi == 2) {
            return $this->instalasiTransmisi();
        } elseif ($this->tipe_instalasi == 3) {
            return $this->instalasiDistribusi();
        } elseif ($this->tipe_instalasi == 4) {
            return $this->instalasiPemanfaatanTT();
        } elseif ($this->tipe_instalasi == 5) {
            return $this->instalasiPemanfaatanTM();
        }
    }

    public function jenisInstalasi(){
        return $this->belongsTo('App\JenisInstalasi', 'jenis_instalasi_id', 'id');
    }
}
