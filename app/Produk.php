<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    //
    protected $table = 'produk';
    public $timestamps = false;

    public function tipeInstalasi(){
        return $this->belongsToMany('App\TipeInstalasi')->withTimestamps();
    }
}
