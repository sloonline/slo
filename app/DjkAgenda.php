<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DjkAgenda extends Model
{
    protected $table = 'djk_agenda';
    
    public function instalasiPembangkit()
    {
        return $this->belongsTo('App\InstalasiPembangkit', 'instalasi_id', 'id');
    }
    
    public function instalasiTransmisi()
    {
        return $this->belongsTo('App\InstalasiTransmisi', 'instalasi_id', 'id');
    }
    
    public function instalasiDistribusi()
    {
        return $this->belongsTo('App\InstalasiDistribusi', 'instalasi_id', 'id');
    }
    
    public function instalasiPemanfaatanTT()
    {
        return $this->belongsTo('App\InstalasiPemanfaatanTT', 'instalasi_id', 'id');
    }
    
    public function instalasiPemanfaatanTM()
    {
        return $this->belongsTo('App\InstalasiPemanfaatanTM', 'instalasi_id', 'id');
    }
    
    public function instalasi(){
        if ($this->tipe_instalasi_id == 1) {
            return $this->instalasiPembangkit();
        } elseif ($this->tipe_instalasi_id == 2) {
            return $this->instalasiTransmisi();
        } elseif ($this->tipe_instalasi_id == 3) {
            return $this->instalasiDistribusi();
        } elseif ($this->tipe_instalasi_id == 4) {
            return $this->instalasiPemanfaatanTT();
        } elseif ($this->tipe_instalasi_id == 5) {
            return $this->instalasiPemanfaatanTM();
        }
    }
    
    public function tipeInstalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi_id', 'id');
    }
    
    public function jenisInstalasi(){
        return $this->belongsTo('App\JenisInstalasi', 'jenis_instalasi_id', 'id');
    }
    
    public function status(){
        return $this->belongsTo('App\References','status_id','id');
    }

    public function history(){
        return $this->hasMany('App\DjkLogStatus');
    }

    public function registrasi(){
        return $this->hasOne('App\DjkRegistrasi','djk_agenda_id','id');
    }
}
