<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LokasiArea extends Model
{
    //
    use SoftDeletes;
    protected $table = "lokasi_area";

    public function areaProgram(){
        return $this->belongsTo('App\AreaProgram','id_area_program','id');
    }

    public function kontrak(){
        return $this->belongsTo('App\Kontrak','id_kontrak','id');
    }

    public function lokasi(){
        return $this->belongsTo('App\Lokasi','id_lokasi','id');
    }

    public function instalasi(){
        return $this->hasMany('App\InstalasiDistribusi','id_lokasi_area ','id');
    }


}
