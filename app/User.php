<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract,
//                                    AuthorizableContract,
    CanResetPasswordContract
{
    //    use Authenticatable, Authorizable, CanResetPassword, EntrustUserTrait; // add this trait to your user model
    use Authenticatable, CanResetPassword, SoftDeletes; // add this trait to your user model

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'nama_user',
        'tempat_lahir_user',
        'gender_user',
        'tanggal_lahir',
        'alamat_user',
        'no_ktp_user',
        'no_hp_user',
        'status_user'

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //    public function Artikel(){
    //        return $this->hasMany('App\Artikel', 'id_user', 'id');
    //    }

    /**
     * @return bool
     */
    //    public function isATeamManager(){
    //        return true;
    //    }

    /*    public function groups(){
        return $this->belongsToMany('App\Group')->withTimestamps();
    }

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function areaUser(){
        return $this->hasOne('App\AreaUser');
    }

    public function isGuest(){
        //        dd('guest');

        if($this->groups->find(2) != null){
            return true;
        }

        return false;
    }

    public function logAset(){
        return $this->hasMany('App\LogAset', 'username', 'username');
    }

    public function logMaterial(){
        return $this->hasMany('App\LogMaterial', 'username', 'username');
    }

    public function logAE1(){
        return $this->hasMany('App\LogAE1', 'username', 'username');
    }

    public function pic(){
        return $this->hasMany('App\PenanggungJawab', 'username', 'username');
    }

    public function isUserLevel2(){
        //        $company_code   = $this->areaUser->businessArea->company_code;
        $business_area  = $this->areaUser->businessArea->business_area;
        if(!ends_with($business_area, '01') && $business_area!='1001')
        return true;
        else
        return false;
    }

    public function isUserLevel1(){
        //        $company_code   = $this->areaUser->businessArea->company_code;
        $business_area  = $this->areaUser->businessArea->business_area;
        if(ends_with($business_area, '01'))
        return true;
        else
        return false;
    }

    public function isUserLevel0(){
        //        $company_code   = $this->areaUser->businessArea->company_code;
        $business_area  = $this->areaUser->businessArea->business_area;
        if($business_area=='1001')
        return true;
        else
        return false;
    }

    public function artikel(){
        return $this->hasMany('App\Artikel', 'id_user', 'id');
    }

    public function province(){
        return $this->belongsTo('App\Provinces', 'id_province', 'id');
    }

    public function city(){
        return $this->belongsTo('App\Cities', 'id_city', 'id');
    }*/
    public function isUserInternal()
    {
        if ($this->jenis_user == "INTERNAL") {
            return true;
        } else {
            return false;
        }
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'perusahaan_id', 'id');
    }

    public function bidang()
    {
        return $this->belongsTo('App\Bidang', 'id_bidang', 'id');
    }

    public function sub_bidang()
    {
        return $this->belongsTo('App\SubBidang', 'id_sub_bidang', 'id');
    }

    public function user_roles()
    {
        return $this->hasMany('App\UserRole', 'user_id', 'id');
    }


    public function user_roles_id()
    {
        return $this->hasMany('App\UserRole', 'user_id', 'id')->select('role_id');
    }

    public function notif()
    {
        $notif = Notification::where('status', 'UNREAD')->where('to', Auth::user()->username)->get()->sortByDesc('id');
        return $notif;
    }

    public static function getUserListFromRole($role)
    {
        $role = Role::where('name', $role)->first();
        $user = ($role->users() != null) ? $role->users()->get() : collect(array());
        return $user;
    }

    public function instalasiPembangkit()
    {
        return $this->hasMany('App\InstalasiPembangkit', 'created_by', 'username');
    }

    public static function getRolesByName($role_name)
    {
        $role_id = Role::select('id')->where('name', $role_name)->first();
        $user_roles = Auth::user()->user_roles;
        $isUser = false;
        foreach ($user_roles as $item) {
            if ($role_id != null && ($role_id->id == $item->role_id)) {
                $isUser = true;
            }
        }
        return $isUser;
    }

    public static function isUnitPLN($user)
    {
        return (@$user->perusahaan->kategori->nama_kategori == "PLN") ? true : false;
    }

    public static function isEksternal($user)
    {
        return ($user->perusahaan != null) ? true : false;
    }

    public function perusahaanSpv()
    {
        return $this->hasMany('App\PerusahaanSpv', 'user_id', 'id');
    }

    public function businessAreaUser()
    {
        return $this->hasMany('App\businessAreaUser', 'user_id', 'id');
    }

    public static function getUserSpv()
    {
        //        return User::getUserListFromRole('SPV-BPU');
        return User::getAllUserByPermission(PERMISSION_SPV_REGIONAL);
    }

    public static function getSpv($user)
    {
        if ($user != null) {
            if ($user->perusahaan != null) {
                $perusahaan = $user->perusahaan;
                if ($perusahaan->id_business_area != null) {
                    $spv = $perusahaan->business_area_user;
                    return ($spv != null) ? $spv->user : null;
                } else {
                    $spv = $perusahaan->perusahaanSpv;
                    return ($spv != null) ? $spv->spv : null;
                }
            } else {
                return null;
            }
        } else {
            return array();
        }
    }

    public function userRole()
    {
        return $this->hasMany('App\UserRole', 'user_id', 'id');
    }

    public static function getAllUserByPermission($permission)
    {
        return User::whereHas('userRole', function ($query) use ($permission) {
            $query->whereHas('roles', function ($query) use ($permission) {
                $query->whereHas('permissionRole', function ($query) use ($permission) {
                    $query->whereHas('permission', function ($query) use ($permission) {
                        $query->where('display_name', $permission);
                    });
                });
            });
        })->get();
    }

    public static function getUserPermission($user)
    {
        $userRole = $user->userRole;
        $permission = array();
        foreach ($userRole as $uRole) {
            $role = $uRole->roles;
            if ($role != null) {
                $permissionRoles = $role->permissionRole;
                foreach ($permissionRoles as $data) {
                    array_push($permission, @$data->permission->display_name);
                }
            }
        }
        $permission = array_unique($permission);
        return collect($permission);
    }


    public static function isCurrUserAllowPermission($permission)
    {
        $permissions = Session::get('user_permissions');
        return ($permissions != null && ((string) $permissions->search($permission) != "")) ? true : false;
        //return true;
    }

}
        
