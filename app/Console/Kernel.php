<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('inspire')->hourly();
//
//        $schedule->command('cache:clear')
//            ->hourly()
//            ->sendOutputTo($filePath)
//            ->emailOutputTo('john@doe.com');
//
//        $schedule->call('SomeClass@method')->dailyAt('10:00');
//
//        $schedule->call(function(){
//            //..
//        })->everyThirtyMinutes();
//
//        $schedule->terminal('gulp task')->fridays()->when(function(){
//            return true;
//        });
        $filePath = base_path() . '/storage/logs/mail.log';
        $filePath_wbsio = base_path() . '/storage/logs/wbsio.log';
        $logPath = base_path() . '/storage/logs/';
        $djkLog = base_path() . '/storage/logs/djk.log';
        $sbuLog = base_path() . '/storage/logs/sbu.log';

        // Send Email
        $schedule->call('\App\Http\Controllers\LogController@sendEmailJob')->everyMinute()->sendOutputTo($filePath);

        // Lock WBS/IO
        #$schedule->call('\App\Http\Controllers\PekerjaanController@lockExpiredEmergencyWbsIo')->daily()->sendOutputTo($filePath_wbsio);

        // Update Kontraktor from API DJK
        #$schedule->call('\App\Http\Controllers\KontraktorController@updateFromDJK')->dailyAt('00:01')->sendOutputTo($logPath . 'API_Kontraktor.log');

        // Update Permohonan DJK from API DJK
        $schedule->call('\App\Http\Controllers\ApiController@getHasilSLO')->dailyAt('00:01')->sendOutputTo($djkLog);
        $schedule->call('\App\Http\Controllers\ApiController@getRefSbu')->dailyAt('00:01')->sendOutputTo($sbuLog);
        //everyTenMinutes
    }
}
