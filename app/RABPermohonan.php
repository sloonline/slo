<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RABPermohonan extends Model
{
    //
//    use SoftDeletes;
    protected $table = 'rab_permohonan';

    public function permohonan()
    {
        return $this->belongsTo('App\Permohonan', 'permohonan_id', 'id');
    }

    public function rab()
    {
        return $this->belongsTo('App\RAB', 'rab_id', 'id');
    }


}