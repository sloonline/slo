<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulRole extends Model
{
    //
    protected $table = 'modul_role';
    protected $primaryKey = null;
    public $incrementing = false;

    public function modul()
    {
        return $this->belongsTo('App\Modul', 'modul_id', 'id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }
}
