<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingItem extends Model
{
    protected $table='pending_item';

    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function ba_pending_item(){
        return $this->hasMany('App\BAPendingItem', 'pending_item_id', 'id');
    }
}
