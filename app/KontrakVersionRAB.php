<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KontrakVersionRAB extends Model
{
    use SoftDeletes;

    protected $table = "kontrak_version_rab";

    public function kontrak_version(){
        return $this->belongsTo('App\KontrakVersion', 'id_kontrak_version', 'id');
    }

    public function rab(){
        return $this->belongsTo('App\RAB', 'id_rab', 'id');

    }
}
