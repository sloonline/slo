<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RabWbsIo extends Model
{
    protected $table = "rab_wbsio";

    public function rab(){
        return $this->belongsTo('App\RAB','rab_id','id');
    }

    public function wbsIo(){
        return $this->belongsTo('App\WbsIo','wbsio_id','id');
    }
}
