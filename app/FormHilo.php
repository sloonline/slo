<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormHilo extends Model
{

  protected $table = "form_hilo";
  use SoftDeletes;

  public function lingkup_pekerjaan(){
    return $this->belongsTo('App\LingkupPekerjaan','lingkup_pekerjaan_id', 'id');
  }

  public static function getByLingkup($lingkup_pekerjaan_id){
    return FormHilo::where('lingkup_pekerjaan_id',$lingkup_pekerjaan_id)->orderBy('id')->get();
  }
}
