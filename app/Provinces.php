<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected $table = 'provinces';

    protected $fillable = [
                'provinces',

    ];

    public function city(){
        return $this->hasMany('App\Cities', 'id_province', 'id');
    }

    public function user(){
        return $this->hasMany('App\Users', 'id_province', 'id');
    }
 }
