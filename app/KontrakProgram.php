<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KontrakProgram extends Model
{
    use SoftDeletes;
    protected $table = 'kontrak_program';

    public function lokasi(){
        return $this->hasMany('App\LokasiArea', 'id_kontrak', 'id');
    }



    public function child($tipe){
        $data = array();
        switch($tipe){
            case TIPE_LOKASI:
                foreach ($this->lokasi as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->lokasi->lokasi;
                    $row->instalasi = $item->instalasi;
                    array_push($data, $row);
                }
                break;
        }
        return $data;

    }
}
