<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    protected $table = 'workflow';

    public function flow_status(){
        return $this->belongsTo('App\FlowStatus', 'flow_status_id', 'id');
    }

    public function role(){
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    public function nextWf(){
        return $this->belongsTo('App\Workflow', 'next', 'id');
    }
    public function prevWf(){
        return $this->belongsTo('App\Workflow', 'prev', 'id');
    }
}
