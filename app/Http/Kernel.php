<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'demo' => \App\Http\Middleware\Demo::class,
        'manager' => \App\Http\Middleware\RedirectIfNotManager::class,
        'user' => \App\Http\Middleware\RedirectIfGuest::class,

//        'ae1' => \App\Http\Middleware\RedirectAE1::class,
//        'ae1_upload' => \App\Http\Middleware\RedirectAE1Upload::class,
//        'ae2' => \App\Http\Middleware\RedirectAE2::class,
//        'ae3' => \App\Http\Middleware\RedirectAE3::class,
//        'ae4' => \App\Http\Middleware\RedirectAE4::class,

        'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission' => \Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,

        //middleware check external or internal user
        'isInternal' => \App\Http\Middleware\CheckIsInternal::class,
        'isEksternal' => \App\Http\Middleware\CheckIsEksternal::class,

        'instalasi_pembangkit' => \App\Http\Middleware\RedirectInstalasiPembangkit::class,
        'instalasi_transmisi' => \App\Http\Middleware\RedirectInstalasiTransmisi::class,
        'instalasi_distribusi' => \App\Http\Middleware\RedirectInstalasiDistribusi::class,
        'instalasi_pemanfaatan_tt' => \App\Http\Middleware\RedirectInstalasiPemanfaatanTT::class,
        'instalasi_pemanfaatan_tm' => \App\Http\Middleware\RedirectInstalasiPemanfaatanTM::class,


    ];
}
