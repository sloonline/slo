<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth']], function () {

    #middleware check user status(internal/eksternal)
    Route::group(['middleware' => ['isInternal']], function () {

        /*======================START REGION INTERNAL=======================*/

        #View User Peminta Jasa PLN
        Route::get('internal/view_user_peminta_jasa_pln', 'UserInternalController@viewUserPemintaJasaPLN');

        #Detail User Peminta Jasa PLN
        Route::get('internal/detail_user_peminta_jasa_pln/{id}', 'UserInternalController@showUserPemintaJasaPLN');

        #Add User Peminta Jasa PLN
        Route::get('internal/create_user_peminta_jasa_pln', 'UserInternalController@createUserPemintaJasaPLN');

        Route::post('internal/create_user_peminta_jasa_pln', 'UserInternalController@storeUserPemintaJasaPLN');

        #Update User Peminta Jasa PLN
        Route::get('internal/edit_user_peminta_jasa_pln/{id}', 'UserInternalController@editUserPemintaJasaPLN');

        Route::post('internal/edit_user_peminta_jasa_pln/{id}', 'UserInternalController@updateUserPemintaJasaPLN');

        #Delete User Peminta Jasa PLN
        Route::get('internal/delete_user_peminta_jasa_pln/{id}', 'UserInternalController@destroyUserPemintaJasaPLN');

        #Delete User Eksternal
        Route::get('internal/delete_user_eksternal/{id}', 'UserInternalController@deleteEksternal');

        #INTERNAL
        #Dashboard Internal
        Route::get('/internal', function () {
            return view('internal/dashboard');
        });

        #Approval Registrasi Peminta Jasa
        Route::get('/internal/app_reg_peminta', 'UserInternalController@viewApprovalExternal');

        Route::get('/internal/det_reg_peminta/{id}', 'UserInternalController@showApprovalExternal');

        Route::post('/internal/det_reg_peminta/{id}', 'UserInternalController@storeApprovalExternal');

        Route::post('/internal/pending_user/', 'UserInternalController@storePendingExternal');

        #Approval Registrasi Internal Pusertif
        Route::get('/internal/app_reg_internal', function () {
            return view('internal.registrasi.app_reg_internal');
        });

        Route::get('/internal/det_reg_internal', function () {
            return view('internal.registrasi.det_reg_internal');
        });

        /*=================START manajemen user internal======================*/
        #Renisa Suryahadikusumah
        #Role by BPU
        Route::get('/internal/user_internal', 'UserInternalController@index_internal');

        #register internal
        Route::get('internal/register/{id?}', 'UserInternalController@registerInternal');

        Route::post('internal/register', 'UserInternalController@storeInternal');

        Route::get('internal/delete_internal/{id}', 'UserInternalController@deleteInternal');

        #list all users & and action
        Route::get('internal/users', 'UserInternalController@showUsers');

        Route::get('internal/view_user/{id}', 'UserInternalController@detailUser');

        #input manual user external by admin
        Route::get('internal/register_eksternal/{id?}', 'UserInternalController@registerEksternalByAdmin');
        Route::post('internal/register_eksternal', 'UserInternalController@storeEksternalByAdmin');
        Route::get('internal/delete_eksternal/{id}', 'UserInternalController@deleteEksternalByAdmin');

        Route::get('internal/edit-eksternal/{id}', 'UserInternalController@editEksternalByAdmin');
        Route::post('internal/edit-eksternal/{id}', 'UserInternalController@updateExternal');

        /*=================END manajemen user internal============================*/

        #START MASTER BIDANG====================================================
        Route::get('master/bidang', 'MasterController@showBidang');
        Route::get('master/bidang/input', 'MasterController@insertBidang');
        Route::get('master/bidang/input/{id}', 'MasterController@insertBidang');
        Route::post('master/bidang/input', 'MasterController@storeBidang');
        Route::get('master/bidang/isaktif/{id}', 'MasterController@isAktifBidang');
        #END MASTER BIDANG====================================================

        #START MASTER SUB BIDANG====================================================
        Route::get('master/sub_bidang', 'MasterController@showSubBidang');
        Route::get('master/sub_bidang/input', 'MasterController@insertSubBidang');
        Route::post('master/sub_bidang/input', 'MasterController@storeSubBidang');
        Route::get('master/sub_bidang/input/{id}', 'MasterController@insertSubBidang');
        Route::get('master/sub_bidang/isaktif/{id}', 'MasterController@isAktifSubBidang');
        #END MASTER SUB BIDANG====================================================

        #START MASTER MODUL====================================================
        Route::get('master/modul', 'MasterController@showModul');
        Route::get('master/modul/input', 'MasterController@insertModul');
        Route::post('master/modul/input', 'MasterController@storeModul');
        Route::get('master/modul/input/{id}', 'MasterController@insertModul');
        Route::get('master/modul/isaktif/{id}', 'MasterController@isAktifModul');
        #END MASTER MODUL====================================================

        #START MASTER HAK AKSES====================================================
        Route::get('master/hak_akses', 'MasterController@showHakAkses');
        Route::get('master/hak_akses/input', 'MasterController@insertHakAkses');
        Route::get('master/hak_akses/input/{id}', 'MasterController@insertHakAkses');
        Route::post('master/hak_akses/input', 'MasterController@storeHakAkses');
        Route::get('master/hak_akses/isaktif/{id}', 'MasterController@isAktifHakAkses');
        #END MASTER HAK AKSES====================================================

        #START MASTER HAK AKSES MODUL====================================================
        Route::get('master/hak_akses_modul', 'MasterController@showHakAksesModul');
        Route::post('master/hak_akses_modul', 'MasterController@inputHakAksesModul');
        #END MASTER HAK AKSES MODUL====================================================

        #START MASTER ROLE====================================================
        Route::get('master/level_akses', 'MasterController@showLevelAkses');
        Route::get('master/level_akses/input', 'MasterController@insertLevelAkses');
        Route::post('master/level_akses/input', 'MasterController@storeLevelAkses');
        Route::get('master/level_akses/input/{id}', 'MasterController@insertLevelAkses');
        Route::get('master/level_akses/isaktif/{id}', 'MasterController@isAktifLevelAkses');
        #END MASTER ROLES====================================================

        /*START MASTER PERALATAN==========================================*/
        Route::get('master/peralatan', 'MasterController@showPeralatan'); #show list data
        Route::get('master/create_peralatan', 'MasterController@insertPeralatan'); #create new
        Route::get('master/create_peralatan/{id}', 'MasterController@insertPeralatan'); #update
        Route::post('master/create_peralatan', 'MasterController@storePeralatan'); #action post
        Route::get('master/delete_peralatan/{id}', 'MasterController@deletePeralatan'); #delete
        /*END MASTER PERALATAN============================================*/

        /*===============MASTER DATA PERSONIL====================*/
        Route::get('master/personil_inspeksi', 'MasterController@showPersonilInspeksi');
        Route::get('master/personil_inspeksi/input', 'MasterController@createPersonilInspeksi');
        Route::get('master/personil_inspeksi/input/{id}', 'MasterController@createPersonilInspeksi');
        Route::post('master/personil_inspeksi/input', 'MasterController@storePersonilInspeksi');
        Route::get('master/personil_inspeksi/isaktif/{id}', 'MasterController@isAktifPersonilInspeksi');
        /*===============END MASTER DATA PERSONIL====================*/

        /*===============MASTER TRAINING============*/
        Route::get('master/training', 'MasterController@showTraining');
        Route::get('master/training/input', 'MasterController@createTraining');
        Route::get('master/training/input/{id}', 'MasterController@createTraining');
        Route::post('master/training/input', 'MasterController@storeTraining');
        Route::get('master/delete_training/{id}', 'MasterController@deleteTraining');
        /*===============END MASTER TRAINING============*/

        /*===============MASTER PERSONIL TRAINING============*/
        Route::get('master/personil_training/input/{personil_id}', 'MasterController@createPersonilTraining');
        Route::get('master/personil_training/input/{personil_id}/{id}', 'MasterController@createPersonilTraining');
        Route::post('master/personil_training/input', 'MasterController@storePersonilTraining');
        Route::get('master/delete_personil_training/{id}', 'MasterController@deletePersonilTraining');
        /*===============END MASTER PERSONIL TRAINING============*/

        /*===============MASTER KEAHLIAN============*/
        Route::get('master/keahlian', 'MasterController@showKeahlian');
        Route::get('master/keahlian/input', 'MasterController@createKeahlian');
        Route::get('master/keahlian/input/{id}', 'MasterController@createKeahlian');
        Route::post('master/keahlian/input', 'MasterController@storeKeahlian');
        Route::get('master/delete_keahlian/{id}', 'MasterController@deleteKeahlian');
        /*===============END MASTER KEAHLIAN============*/

        /*===============MASTER KEAHLIAN PERSONIL============*/
        Route::get('master/keahlian_personil/input/{personil_id}', 'MasterController@createKeahlianPersonil');
        Route::get('master/keahlian_personil/input/{personil_id}/{id}', 'MasterController@createKeahlianPersonil');
        Route::post('master/keahlian_personil/input', 'MasterController@storeKeahlianPersonil');
        Route::get('master/delete_keahlian_personil/{id}', 'MasterController@deleteKeahlianPersonil');
        /*===============END MASTER KEAHLIAN PERSONIL============*/

        /*================START PEMBUATAN RAB==========================================*/
        #show rancangan biaya khusus untuk user internal
        Route::get('internal/create_rancangan_biaya/{order_id}', 'RABController@createRancanganBiaya2');
        Route::get('internal/create_rancangan_biaya/{order_id}/{rab_id}', 'RABController@createRancanganBiaya2');
        Route::post('internal/create_rancangan_biaya', 'RABController@storeRancanganBiaya2');
        Route::get('internal/delete_rancangan_biaya/{order_id}/{id}', 'RABController@deleteRancanganBiaya');
        /*================END PEMBUATAN RAB============================================*/

        Route::get('internal/form_rab', function () {
            return view('internal.rab.create_form_rab');
        });

        Route::get('internal/invoice_rab', function () {
            return view('internal.rab.invoice_rab');
        });

        #START FORM ORDER INTERNAL===========================================================
        Route::get('/internal/order', 'OrderController@showOrdersInternal');
        Route::get('/internal/detail_order/{id}', 'OrderController@detailOrder');
        Route::get('/internal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@verifyPermohonan');
        Route::post('/internal/store_permohonan', 'OrderController@internalStorePermohonan');
        Route::post('/internal/approval_order', 'OrderController@approvalOrder');
        Route::get('/internal/custom_field_verifikasi/{id}/{id_permohonan}/{status_baru}', 'OrderController@createCustomFieldsVerifikasi');

        #END FORM ORDER INTERNAL===========================================================


        #START LOG EMAIL=====================================================
        Route::get('/internal/log_email', 'LogController@showLogEmail');
        Route::get('/internal/resend_email/{id}', 'LogController@resendEmail');
        #END LOG EMAIL=====================================================


        #START LOG ACTIVITY=====================================================
        Route::get('/internal/log_activity', 'LogController@showLogActivity');
        #END LOG ACTIVITY=====================================================

        /*======================END REGION INTERNAL=======================*/

    });


    #Route::group(['middleware' => ['isEksternal']], function () {

    /*======================START REGION EKSTERNAL=======================*/


    #Dashboard
#        Route::get('/eksternal', function () {
#            return view('eksternal/dashboard2');
#        });
    Route::get('/eksternal', 'PagesController@dashboardEksternal');
    Route::get('/eksternal/list_order_ongoing', function () {
        return view('eksternal/list_order_ongoing');
    });

#        Route::get('/eksternal/detail_order', function () {
#            return view('eksternal/detail_order');
#        });

    Route::get('/eksternal/agreement', function () {
        return view('eksternal/agreement');

    });
    #Form Input Transmisi
    Route::get('/eksternal/transmisi', 'TransmisiController@index');
    Route::post('/eksternal/transmisi', 'TransmisiController@store');

    #Form Input Pembangkit
    Route::get('/eksternal/pembangkit', 'PembangkitController@index');
    Route::post('/eksternal/pembangkit', 'PembangkitController@store');

    #Form Input Distribusi
    Route::get('/eksternal/distribusi', function () {
        return view('eksternal/distribusi');
    });

    #Riwayat Permohonan SLO
    Route::get('/eksternal/riwayat', 'RiwayatController@index');

    #Tracking
    Route::get('/eksternal/tracking', 'TrackingController@index');

    #Pemanfaatan
    Route::get('/eksternal/pemanfaatan', function () {
        return view('eksternal/pemanfaatan');
    });

    #Sample API DJK
    Route::get('/eksternal/sample_djk', function () {
        return view('eksternal/sample_api_djk');
    });


    /*================START DATA INSTALASI======================================*/
    #PEMBANGKIT
    Route::get('/eksternal/view_instalasi_pembangkit', 'MasterController@showPembangkit');

    Route::group(['middleware' => ['instalasi_pembangkit']], function () {
        Route::get('/eksternal/instalasi-pembangkit/{id}', ['as' => 'add-instalasi-cart', 'uses' => 'MasterController@detailPembangkit']);
        Route::get('/eksternal/edit-instalasi-pembangkit/{id}', 'MasterController@insertPembangkit');
    });

    Route::get('/eksternal/create_instalasi_pembangkit', 'MasterController@insertPembangkit');
    Route::get('/eksternal/create_instalasi_pembangkit/{id}', 'MasterController@insertPembangkit');
    Route::get('/eksternal/delete_instalasi_pembangkit/{id}', 'MasterController@deletePembangkit');
    Route::post('/eksternal/create_instalasi_pembangkit', 'MasterController@storePembangkit');

    #TRANSMISI
    Route::get('/eksternal/view_instalasi_transmisi', 'MasterController@showTransmisi');

    Route::group(['middleware' => ['instalasi_transmisi']], function () {
        Route::get('/eksternal/instalasi-transmisi/{id}', 'MasterController@detailTransmisi');
        Route::get('/eksternal/edit-instalasi-transmisi/{id}', 'MasterController@insertTransmisi');
    });

    Route::get('/eksternal/create_instalasi_transmisi', 'MasterController@insertTransmisi');
    Route::get('/eksternal/create_instalasi_transmisi/{id}', 'MasterController@insertTransmisi');
    Route::get('/eksternal/delete_instalasi_transmisi/{id}', 'MasterController@deleteTransmisi');
    Route::post('/eksternal/create_instalasi_transmisi', 'MasterController@storeTransmisi');

    #DISTRIBUSI
    Route::get('/eksternal/view_instalasi_distribusi', 'MasterController@showDistribusi');

    Route::group(['middleware' => ['instalasi_distribusi']], function () {
        Route::get('/eksternal/instalasi-distribusi/{id}', 'MasterController@detailDistribusi');
        Route::get('/eksternal/edit-instalasi-distribusi/{id}', 'MasterController@insertDistribusi');
    });

    Route::get('/eksternal/create_instalasi_distribusi', 'MasterController@insertDistribusi');
    Route::get('/eksternal/create_instalasi_distribusi/{id}', 'MasterController@insertDistribusi');
    Route::get('/eksternal/delete_instalasi_distribusi/{id}', 'MasterController@deleteDistribusi');
    Route::post('/eksternal/create_instalasi_distribusi', 'MasterController@storeDistribusi');

    #PEMANFAATAN TT
    Route::get('/eksternal/view_instalasi_pemanfaatan_tt', 'MasterController@showPemanfaatanTT');

    Route::group(['middleware' => ['instalasi_pemanfaatan_tt']], function () {
        Route::get('/eksternal/instalasi-pemanfaatan-tt/{id}', 'MasterController@detailPemanfaatanTT');
        Route::get('/eksternal/edit-instalasi-pemanfaatan-tt/{id}', 'MasterController@insertPemanfaatanTT');
    });

    Route::get('/eksternal/create_instalasi_pemanfaatan_tt', 'MasterController@insertPemanfaatanTT');
    Route::get('/eksternal/create_instalasi_pemanfaatan_tt/{id}', 'MasterController@insertPemanfaatanTT');
    Route::get('/eksternal/delete_instalasi_pemanfaatan_tt/{id}', 'MasterController@deletePemanfaatanTT');
    Route::post('/eksternal/create_instalasi_pemanfaatan_tt', 'MasterController@storePemanfaatanTT');

    #PEMANFAATAN TM
    Route::get('/eksternal/view_instalasi_pemanfaatan_tm', 'MasterController@showPemanfaatanTM');

    Route::group(['middleware' => ['instalasi_pemanfaatan_tm']], function () {
        Route::get('/eksternal/instalasi-pemanfaatan-tm/{id}', 'MasterController@detailPemanfaatanTM');
        Route::get('/eksternal/edit-instalasi-pemanfaatan-tm/{id}', 'MasterController@insertPemanfaatanTM');
    });

    Route::get('/eksternal/create_instalasi_pemanfaatan_tm', 'MasterController@insertPemanfaatanTM');
    Route::get('/eksternal/create_instalasi_pemanfaatan_tm/{id}', 'MasterController@insertPemanfaatanTM');
    Route::get('/eksternal/delete_instalasi_pemanfaatan_tm/{id}', 'MasterController@deletePemanfaatanTM');
    Route::post('/eksternal/create_instalasi_pemanfaatan_tm', 'MasterController@storePemanfaatanTM');
    /*================END DATA INSTALASI======================================*/

    /*===========MASTER PEMILIK INSTALASI===================*/
    Route::get('/eksternal/pemilik_instalasi', 'MasterController@showPemilikInstalasi');
    Route::get('/eksternal/create_pemilik_instalasi', 'MasterController@insertPemilikInstalasi');
    Route::get('/eksternal/create_pemilik_instalasi/{id}', 'MasterController@insertPemilikInstalasi');
    Route::post('/eksternal/create_pemilik_instalasi', 'MasterController@storePemilikInstalasi');
    Route::get('/eksternal/detail_pemilik_instalasi/{id}', 'MasterController@detailPemilikInstalasi');
    Route::get('/eksternal/delete_pemilik_instalasi/{id}', 'MasterController@deletePemilikInstalasi');
    /*==========END MASTER PEMILIK INSTALASI================*/


    /*===========MASTER SURAT TUGAS===================*/
    Route::get('/eksternal/surat_tugas', 'MasterController@showSuratTugas');
    Route::get('/eksternal/create_surat_tugas', 'MasterController@insertSuratTugas');
    Route::get('/eksternal/create_surat_tugas/{id}', 'MasterController@insertSuratTugas');
    Route::post('/eksternal/create_surat_tugas', 'MasterController@storeSuratTugas');
    Route::get('/eksternal/delete_surat_tugas/{id}', 'MasterController@deleteSuratTugas');
    Route::get('/eksternal/program_tree/{id}', 'MasterController@showProgramTree');
    /*==========END MASTER SURAT TUGAS================*/
    #START FORM ORDER===========================================================
    Route::get('/eksternal/view_order', 'OrderController@showOrder');
    Route::get('/eksternal/create_order', 'OrderController@createOrder');
    Route::get('/eksternal/create_order/{id}', 'OrderController@createOrder');
    Route::get('/eksternal/detail_order/{id}', 'OrderController@createOrder');
    Route::post('/eksternal/create_order', 'OrderController@storeGeneralOrder');
    Route::post('/eksternal/submit_order', 'OrderController@submitOrder');
    #END FORM ORDER===========================================================

    #START FORM PERMOHONAN===========================================================
    Route::get('/eksternal/create_permohonan/{order_id}', 'OrderController@createPermohonan');
    Route::get('/eksternal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@createPermohonan');
    Route::post('/eksternal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@storePermohonan');
    Route::post('/eksternal/store_permohonan', 'OrderController@storePermohonan'); #sample Renisa
    Route::get('/eksternal/custom_field/{id}/{id_permohonan}/{status_baru}', 'OrderController@createCustomFields');
    Route::post('/eksternal/custom_field', 'OrderController@storeCustomFields');
    Route::get('/eksternal/custom_field_verifikasi/{id}/{id_permohonan}', 'OrderController@createCustomFieldsVerifikasi');
    Route::get('/eksternal/delete_permohonan/{id}', 'OrderController@deletePermohonan');
    Route::post('/eksternal/filter_bay', 'OrderController@filterBay');
    #END FORM PERMOHONAN===========================================================

    #START RAB EKTERNAL
    Route::get('/eksternal/rancangan_biaya/{order_id}/{rab_id}', 'RABController@viewRABEksternal');
    #END RAB EKSTERNAL

    #START MASTER GARDU INDUK===========================================================
    Route::get('master/gardu_induk', 'MasterController@showGarduInduk');
    Route::get('master/gardu_induk/input', 'MasterController@insertGarduInduk');
    Route::get('master/gardu_induk/input/{id}', 'MasterController@insertGarduInduk');
    Route::post('master/gardu_induk/input', 'MasterController@storeGarduInduk');
    Route::get('master/gardu_induk/delete/{id}', 'MasterController@deleteGarduInduk');
    #END MASTER GARDU INDUK=============================================================

    #START MASTER PENYULANG=============================================================
    Route::get('master/penyulang', 'MasterController@showPenyulang');
    Route::get('master/penyulang/input', 'MasterController@insertPenyulang');
    Route::get('master/penyulang/input/{id}', 'MasterController@insertPenyulang');
    Route::post('master/penyulang/input', 'MasterController@storePenyulang');
    Route::get('master/penyulang/delete/{id}', 'MasterController@deletePenyulang');
    #END MASTER PENYULANG===============================================================

    #START MASTER LOKASI================================================================
    Route::get('master/lokasi', 'MasterController@showLokasi');
    Route::get('master/lokasi/input', 'MasterController@insertLokasi');
    Route::get('master/lokasi/input/{id}', 'MasterController@insertLokasi');
    Route::post('master/lokasi/input', 'MasterController@storeLokasi');
    Route::get('master/lokasi/delete/{id}', 'MasterController@deleteLokasi');
    #END MASTER LOKASI==================================================================

    #START MASTER PROGRAM===============================================================
    Route::get('master/program', 'MasterController@showProgram');
    Route::post('master/program/input', 'MasterController@insertProgram');
    Route::post('master/program/store', 'MasterController@storeProgram');
    Route::get('master/program/delete/{id}', 'MasterController@deleteProgram');
    #END MASTER PROGRAM=================================================================

    #START MASTER JENIS PROGRAM=========================================================
    Route::get('master/jenis_program', 'MasterController@showJenisProgram');
    Route::get('master/jenis_program/input', 'MasterController@insertJenisProgram');
    Route::get('master/jenis_program/input/{id}', 'MasterController@insertJenisProgram');
    Route::post('master/jenis_program/input', 'MasterController@storeJenisProgram');
    Route::get('master/jenis_program/delete/{id}', 'MasterController@destroyJenisProgram');
    #END MASTER JENIS PROGRAM============================================================


    #START MASTER GARDU AREA===============================================================
    Route::post('master/gardu_area/input', 'MasterController@insertGarduArea');
    Route::post('master/gardu_area/store', 'MasterController@storeGarduArea');
    Route::get('master/gardu_area/delete/{id}', 'MasterController@deleteGarduArea');
    #END MASTER GARDU AREA=================================================================


    #START MASTER LINGKUP KERJA PROGRAM===============================================================
    Route::post('master/lingkup_program/input', 'MasterController@insertLingkupProgram');
    Route::post('master/lingkup_program/store', 'MasterController@storeLingkupProgram');
    Route::get('master/lingkup_program/delete/{id}', 'MasterController@deleteLingkupProgram');
    #END MASTER LINGKUP KERJA PROGRAM=================================================================


    #START LAYANAN===============================================================
    Route::get('eksternal/layanan/{id_produk}/{id_tipe_instalasi}', 'LayananController@pilihInstalasi');
    Route::get('eksternal/layanan/{id_produk}/{id_tipe_instalasi}/{id_instalasi}', 'LayananController@detailInstalasi');
    #END LAYANAN=================================================================

    #START ORDER ================================================================
    Route::post('eksternal/add-to-cart/', 'OrderController@addToCart');
    Route::get('eksternal/keranjang/', 'OrderController@viewKeranjang');
    Route::get('eksternal/keranjang/delete/{id}', ['as' => 'keranjang-delete', 'uses' => 'OrderController@deleteChartPermohonan']);
    Route::post('eksternal/checkout/', 'OrderController@checkOutOrder');
    #END ORDER ==================================================================



    /*===========MASTER PROGRAM DISTRIBUSI===================*/
    #program
    Route::get('/eksternal/program_distribusi', 'MasterController@showProgramDistribusi');
    Route::get('/eksternal/create_program', 'MasterController@insertProgramDistribusi');
    Route::get('/eksternal/create_program/{id}', 'MasterController@insertProgramDistribusi');
    Route::post('/eksternal/create_program', 'MasterController@storeProgramDistribusi');
    Route::get('/eksternal/delete_program/{id}', 'MasterController@deleteProgramDistribusi');
    #area
    Route::get('/eksternal/create_area_program/{id_program}', 'MasterController@insertAreaProgram');
    Route::get('/eksternal/create_area_program/{id_program}/{id}', 'MasterController@insertAreaProgram');
    Route::post('/eksternal/create_area_program', 'MasterController@storeAreaProgram');
    Route::get('/eksternal/delete_area_program/{id}', 'MasterController@deleteAreaProgram');
    #kontrak
    Route::get('/eksternal/create_kontrak_program/{id_program}', 'MasterController@insertKontrakProgram');
    Route::get('/eksternal/create_kontrak_program/{id_program}/{id}', 'MasterController@insertKontrakProgram');
    Route::post('/eksternal/create_kontrak_program', 'MasterController@storeKontrakProgram');
    Route::get('/eksternal/delete_kontrak_program/{id}', 'MasterController@deleteKontrakProgram');
    #penyulang
    Route::get('/eksternal/create_penyulang_area/{id_area_program}', 'MasterController@insertPenyulangArea');
    Route::get('/eksternal/create_penyulang_area/{id_area_program}/{id}', 'MasterController@insertPenyulangArea');
    Route::post('/eksternal/create_penyulang_area', 'MasterController@storePenyulangArea');
    Route::get('/eksternal/delete_penyulang_area/{id}', 'MasterController@deletePenyulangArea');
    #lokasi
    Route::get('/eksternal/create_lokasi_area/{id_program}/{id_area_kontrak}', 'MasterController@insertLokasiArea');
    Route::get('/eksternal/create_lokasi_area/{id_program}/{id_area_kontrak}/{id}', 'MasterController@insertLokasiArea');
    Route::post('/eksternal/create_lokasi_area', 'MasterController@storeLokasiArea');
    Route::get('/eksternal/delete_lokasi_area/{id}', 'MasterController@deleteLokasiArea');

    /*==========END MASTER PROGRAM DISTRIBUSI================*/

    /*======================END REGION EKSTERNAL=======================*/

    #});
});


/*======================START REGION BEBAS=======================*/

Route::get('auth/login', 'Auth\AuthController@getLogin')->name('auth_login');
Route::post('auth/login', 'Auth\AuthController@validateLDAP');
Route::get('auth/register', 'UserEksternalController@registerInternal');
Route::post('auth/register', 'UserEksternalController@storeInternal');

#frontend
Route::get('/', function () {
    return view('front/home');
});
Route::get('/s_sistem_manajemen_mutu', function () {
    return view('front/s_sistem_manajemen_mutu');
});
Route::get('/s_sistem_manajemen_lingkungan', function () {
    return view('front/s_sistem_manajemen_lingkungan');
});

Route::get('/s_sistem_pengawasan_mutu', function () {
    return view('front/s_sistem_pengawasan_mutu');
});

Route::get('/slo_&_komisioning', function () {
    return view('front/slo_&_komisioning');
});

Route::get('/s_sistem_manajemen_OHSAS', function () {
    return view('front/s_sistem_manajemen_OHSAS');
});

Route::get('/smk3', function () {
    return view('front/smk3');
});

Route::get('/pusertif', function () {
    return view('front/pusertif');
});

Route::get('/panduan', function () {
    return view('front/panduan');
});

#peminta jasa non-pln
Route::get('register', 'UserEksternalController@createUserPemintaJasa');
Route::post('/register', 'UserEksternalController@storeExternal');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('auth_logout');


Route::get('auth/confirm_register', function () {
    return view('auth.confirm_register');
});

Route::get('auth/confirm_register', function () {
    return view('auth.confirm_register');
});

Route::get('check_email/{email}', 'UserController@checkEmailAlreadyExisted'); #05092016, check email is already exist


/*====================GET UPLOADED FILES=======================================*/
Route::get('upload/{filename}', function ($filename) {
    # Check if file exists in app/storage/file folder

    $arr = explode("-", $filename);

    $file_path = storage_path() . '/upload/' . $arr[0] . '/' . $filename;
#    $finfo = new finfo(FILEINFO_MIME);
#    $type = $finfo->file($file_path);
    if (file_exists($file_path)) {
        # Send Download
        /*
         * This is for force download file
         * return Response::download($file_path, $filename, [
            'Content-Length: ' . filesize($file_path)
        ]);*/
        /*This one is for view the file*/
        return Response::make(file_get_contents($file_path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    } else {
        # Error
        exit('Requested file does not exist on our server!');
    }
});

Route::get('upload/instalasi/{filename}', function ($filename) {
    # Check if file exists in app/storage/file folder

    $arr = explode("-", $filename);

    $file_path = storage_path() . '/upload/foto_instalasi/' . $filename;
#    dd($file_path);
#    $finfo = new finfo(FILEINFO_MIME);
#    $type = $finfo->file($file_path);
    if (file_exists($file_path)) {
        # Send Download
        /*
         * This is for force download file
         * return Response::download($file_path, $filename, [
            'Content-Length: ' . filesize($file_path)
        ]);*/
        /*This one is for view the file*/
        return Response::make(file_get_contents($file_path), 200, [
            'Content-Type' => 'image',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    } else {
        # Error
        exit('Requested file does not exist on our server!');
    }
});
/*====================END GET UPLOADED FILES=====================================*/

Route::get('form_sld', function () {
    return view('eksternal/form_main_single_diagram');
});

Route::get('form_tata_letak', function () {
    return view('eksternal/form_instalasi_tata_letak');
});


Route::get('form_sbujptl', function () {
    return view('eksternal/form_sbujptl');
});

Route::get('form_iujptl', function () {
    return view('eksternal/form_iujptl');
});


#temp route START DATA UMUM ORDER============================================
Route::get('form/data_umum_order', function () {
    return view('data_umum_order');
});
#temp route END DATA UMUM ORDER============================================

/*===================START FORM UPLOAD PERMOHONAN======================================================*/
Route::post('master/delete_program_child', 'MasterController@deleteProgramTreeChild'); #->what is this?
/*===========================END FORM UPLOAD PERMOHONAN=======================================*/

/*================LOAD FORM DETAIL INSTALASI DI PERMOHONAN====================*/
Route::get('/eksternal/instalasi_field/{tipe_instalasi}/{id}', 'OrderController@createDetailInstalasi');
/*================END LOAD FORM DETAIL INSTALASI DI PERMOHONAN====================*/
/*================AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN====================*/
Route::get('/eksternal/get_area_kontrak/{id_program}', 'OrderController@getAreaKontrak');
Route::get('/eksternal/get_lokasi_penyulang/{id_area_kontrak}', 'OrderController@getLokasiPenyulang');
Route::get('/eksternal/get_lingkup_program/{id_program}/{id_lokasi_penyulang}/{id_area_kontrak}/{id_permohonan}', 'OrderController@getLingkupProgram');
/*================END AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN====================*/

Route::get('notif_list', 'NotifController@index');
Route::get('notif/{id}', 'NotifController@readThenRedirect');

/*======================END REGION BEBAS=======================*/

/*=================START USER PROFILE==========================*/
Route::get('internal/profil/{id}', 'UserController@viewProfileUser');
Route::get('eksternal/profil/{id}', 'UserController@viewProfileUser');

Route::get('eksternal/profile/edit/{id}', 'UserEksternalController@editProfileUser');
Route::post('eksternal/profile/edit/{id}', 'UserEksternalController@updateProfileUser');
/*================END USER PROFILE==============================*/

/*=================START TEST UI==========================*/
Route::get('eksternal/test_dashboard', function () {
    return view('eksternal/test_dashboard');
});
Route::get('eksternal/test_detail_instalasi', function () {
    return view('eksternal/test_detail_instalasi');
});
Route::get('eksternal/test_detail_layanan', function () {
    return view('eksternal/test_detail_layanan');
});
/*================END TEST UI==============================*/

/*Route untuk mendapatkan  data pemilik instalasi, route ini seharuanya ditempatkan pada
tempat yang bersangkutan,  apabila route ini masih di sini, harap dipindahkan ke tempatnya tyhanks*/
Route::post('eksternal/get_pemilik_instalasi', 'MasterController@getNamaPemilikInstalasi');

Route::get('internal/rab', 'RABController@showRancanganBiaya');
Route::get('internal/rancangan_biaya_order/{order_id}', 'RABController@rancanganBiayaOrder');
Route::get('internal/rancangan_biaya_order/submit/{order_id}', 'RABController@submitRancanganBiaya');
Route::get('eksternal/rancangan_biaya_order/{order_id}', 'RABController@rancanganBiayaEksternal');
Route::post('eksternal/approval_rab', 'RABController@eksternalApprovalRab');
Route::post('internal/approval_rab', 'RABController@internalApprovalRab');

#START MASTER BIDANG====================================================
Route::get('master/unsur', 'MasterController@showUnsur');
Route::get('master/unsur/input', 'MasterController@insertUnsur');
Route::get('master/unsur/input/{id}', 'MasterController@insertUnsur');
Route::post('master/unsur/input', 'MasterController@storeUnsur');
Route::get('master/unsur/isaktif/{id}', 'MasterController@isAktifUnsur');
#END MASTER BIDANG====================================================


Route::get('internal/export_excel/{order_id}/{rab_id}', 'RABController@downloadExcel');
Route::get('job_email', 'LogController@sendEmailJob');

Route::get('internal/kontrak', 'ContractController@showKontrak');
Route::get('internal/create_kontrak/', 'ContractController@createKontrak');
Route::get('internal/detail_kontrak/{id}', 'ContractController@detailKontrak');
Route::get('internal/create_kontrak/{id}', 'ContractController@createKontrak');
Route::post('internal/save_kontrak', 'ContractController@storeKontrak');
Route::get('internal/create_amandemen/{kontrak_id}', 'ContractController@createAmandemen');
Route::get('internal/detail_amandemen/{kontrak_version_id}', 'ContractController@detailAmandemen');
Route::post('internal/save_amandemen', 'ContractController@storeAmandemen');
Route::post('internal/approval_kontrak', 'ContractController@approvalContract');
Route::post('internal/save_pembayaran', 'ContractController@savePembayaran');
Route::get('internal/invoice_pembayaran/{id}', 'ContractController@invoicePembayaran');
Route::get('internal/paid_pembayaran/{id}', 'ContractController@paidPembayaran');
Route::post('internal/save_bukti_pembayaran', 'ContractController@saveBuktiPembayaran');

Route::get('internal/workflow', 'WorkflowController@showWorkflow');
Route::get('internal/workflow/add', 'WorkflowController@createWorkflow');
Route::post('internal/workflow/add', 'WorkflowController@saveWorkflow');

Route::get('eksternal/kontrak', 'ContractController@eksternalShowKontrak');
Route::get('eksternal/detail_kontrak/{id}', 'ContractController@eksternalDetailKontrak');
Route::get('eksternal/detail_amandemen/{kontrak_version_id}', 'ContractController@eksternalDetailAmandemen');
