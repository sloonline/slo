<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('phpinfo', function () {
    phpinfo();
});

Route::group(['middleware' => ['auth']], function () {

    #fungsi tidak diketahui
    // Route::get('/api/djk/kontraktor', 'KontraktorController@updateFromDJK');

    //middleware check user status(internal/eksternal)
    Route::group(['middleware' => ['isInternal']], function () {

        /*======================START REGION INTERNAL=======================*/

        //View User Peminta Jasa PLN
        Route::get('internal/view_user_peminta_jasa_pln', 'UserInternalController@viewUserPemintaJasaPLN');

        //Detail User Peminta Jasa PLN
        Route::get('internal/detail_user_peminta_jasa_pln/{id}', 'UserInternalController@showUserPemintaJasaPLN');

        //Add User Peminta Jasa PLN
        Route::get('internal/create_user_peminta_jasa_pln', 'UserInternalController@createUserPemintaJasaPLN');
        Route::post('internal/create_user_peminta_jasa_pln', 'UserInternalController@storeUserPemintaJasaPLN');

        //Update User Peminta Jasa PLN
        Route::get('internal/edit_user_peminta_jasa_pln/{id}', 'UserInternalController@editUserPemintaJasaPLN');
        Route::post('internal/edit_user_peminta_jasa_pln/{id}', 'UserInternalController@updateUserPemintaJasaPLN');

        //Delete User Peminta Jasa Eksternal
        Route::get('internal/delete_user_peminta_jasa_pln/{id}', 'UserInternalController@destroyUserPemintaJasaPLN');

        //Delete User Eksternal
        Route::get('internal/delete_user_eksternal/{id}', 'UserInternalController@deleteEksternal');

        //INTERNAL
        //Dashboard Internal
        Route::get('/internal', 'PagesController@dashboardInternal');


        //Approval Registrasi Peminta Jasa
        Route::get('/internal/app_reg_peminta', 'UserInternalController@viewApprovalExternal');
        Route::get('/internal/det_reg_peminta/{id}', 'UserInternalController@showApprovalExternal');
        Route::post('/internal/det_reg_peminta/{id}', 'UserInternalController@storeApprovalExternal');
        Route::post('/internal/pending_user/', 'UserInternalController@storePendingExternal');

        //Reset Password
        Route::get('/internal/reset_password/{id}', 'UserInternalController@resetPassword');

        //Approval Registrasi Internal Pusertif
        Route::get('/internal/app_reg_internal', function () {
            return view('internal.registrasi.app_reg_internal');
        });
        Route::get('/internal/det_reg_internal', function () {
            return view('internal.registrasi.det_reg_internal');
        });
        /*=================START manajemen user internal======================*/
        //Renisa Suryahadikusumah
        //Role by BPU
        Route::get('/internal/user_internal', 'UserInternalController@index_internal');

        //register internal
        Route::get('internal/register/{id?}', 'UserInternalController@registerInternal');
        Route::post('internal/register', 'UserInternalController@storeInternal');
        Route::get('internal/delete_internal/{id}', 'UserInternalController@deleteInternal');

        //list all users & and action
        Route::get('internal/users', 'UserInternalController@showUsers');
        Route::get('internal/view_user/{id}', 'UserInternalController@detailUser');

        //input manual user external by admin
        Route::get('internal/register_eksternal/{id?}', 'UserInternalController@registerEksternalByAdmin');
        Route::post('internal/register_eksternal', 'UserInternalController@storeEksternalByAdmin');
        Route::get('internal/delete_eksternal/{id}', 'UserInternalController@deleteEksternalByAdmin');

        Route::get('internal/edit-eksternal/{id}', 'UserInternalController@editEksternalByAdmin');
        Route::post('internal/edit-eksternal/{id}', 'UserInternalController@updateExternal');

        /*=================END manajemen user internal============================*/

        //START MASTER BIDANG====================================================
        Route::get('master/bidang', 'MasterController@showBidang');
        Route::get('master/bidang/input', 'MasterController@insertBidang');
        Route::get('master/bidang/input/{id}', 'MasterController@insertBidang');
        Route::post('master/bidang/input', 'MasterController@storeBidang');
        Route::get('master/bidang/isaktif/{id}', 'MasterController@isAktifBidang');
        //END MASTER BIDANG====================================================

        //START MASTER SUB BIDANG====================================================
        Route::get('master/sub_bidang', 'MasterController@showSubBidang');
        Route::get('master/sub_bidang/input', 'MasterController@insertSubBidang');
        Route::post('master/sub_bidang/input', 'MasterController@storeSubBidang');

        Route::get('master/sub_bidang/input/{id}', 'MasterController@insertSubBidang');
        Route::get('master/sub_bidang/isaktif/{id}', 'MasterController@isAktifSubBidang');
        //END MASTER SUB BIDANG====================================================

        //START MASTER MODUL====================================================
        Route::get('master/modul', 'MasterController@showModul');
        Route::get('master/modul/input', 'MasterController@insertModul');
        Route::post('master/modul/input', 'MasterController@storeModul');

        Route::get('master/modul/input/{id}', 'MasterController@insertModul');
        Route::get('master/modul/isaktif/{id}', 'MasterController@isAktifModul');
        //END MASTER MODUL====================================================

        //START MASTER HAK AKSES====================================================
        Route::get('master/hak_akses', 'MasterController@showHakAkses');
        Route::get('master/hak_akses/input', 'MasterController@insertHakAkses');
        Route::get('master/hak_akses/input/{id}', 'MasterController@insertHakAkses');
        Route::post('master/hak_akses/input', 'MasterController@storeHakAkses');
        Route::get('master/hak_akses/isaktif/{id}', 'MasterController@isAktifHakAkses');
        //END MASTER HAK AKSES====================================================

        //START MASTER HAK AKSES MODUL====================================================
        Route::get('master/hak_akses_modul', 'MasterController@showHakAksesModul');
        Route::post('master/hak_akses_modul', 'MasterController@inputHakAksesModul');
        //END MASTER HAK AKSES MODUL====================================================

        //START MASTER ROLE====================================================
        Route::get('master/level_akses', 'MasterController@showLevelAkses');
        Route::get('master/level_akses/input', 'MasterController@insertLevelAkses');
        Route::post('master/level_akses/input', 'MasterController@storeLevelAkses');

        Route::get('master/level_akses/input/{id}', 'MasterController@insertLevelAkses');
        Route::get('master/level_akses/isaktif/{id}', 'MasterController@isAktifLevelAkses');
        //END MASTER ROLES====================================================

        /*START MASTER PERALATAN==========================================*/
        Route::get('master/peralatan', 'MasterController@showPeralatan'); //show list data
        Route::get('master/create_peralatan', 'MasterController@insertPeralatan'); //create new
        Route::get('master/create_peralatan/{id}', 'MasterController@insertPeralatan'); //update
        Route::post('master/create_peralatan', 'MasterController@storePeralatan'); //action post
        Route::get('master/delete_peralatan/{id}', 'MasterController@deletePeralatan'); //delete
        Route::get('master/detail_peralatan/{id}', 'MasterController@detailPeralatan'); //detail
        /*END MASTER PERALATAN============================================*/

        /*===============MASTER DATA PERSONIL====================*/
        Route::get('master/personil_inspeksi', 'MasterController@showPersonilInspeksi');
        Route::get('master/personil_inspeksi/input', 'MasterController@createPersonilInspeksi');
        Route::get('master/personil_inspeksi/input/{id}', 'MasterController@createPersonilInspeksi');
        Route::post('master/personil_inspeksi/input', 'MasterController@storePersonilInspeksi');
        Route::get('master/personil_inspeksi/isaktif/{id}', 'MasterController@isAktifPersonilInspeksi');
        /*===============END MASTER DATA PERSONIL====================*/

        /*===============MASTER TRAINING============*/
        Route::get('master/training', 'MasterController@showTraining');
        Route::get('master/training/input', 'MasterController@createTraining');
        Route::get('master/training/input/{id}', 'MasterController@createTraining');
        Route::post('master/training/input', 'MasterController@storeTraining');
        Route::get('master/delete_training/{id}', 'MasterController@deleteTraining');
        /*===============END MASTER TRAINING============*/

        /*===============MASTER PERSONIL TRAINING============*/
        Route::get('master/personil_training/input/{personil_id}', 'MasterController@createPersonilTraining');
        Route::get('master/personil_training/input/{personil_id}/{id}', 'MasterController@createPersonilTraining');
        Route::post('master/personil_training/input', 'MasterController@storePersonilTraining');
        Route::get('master/delete_personil_training/{id}', 'MasterController@deletePersonilTraining');
        /*===============END MASTER PERSONIL TRAINING============*/

        /*===============MASTER KEAHLIAN============*/
        Route::get('master/keahlian', 'MasterController@showKeahlian');
        Route::get('master/keahlian/input', 'MasterController@createKeahlian');
        Route::get('master/keahlian/input/{id}', 'MasterController@createKeahlian');
        Route::post('master/keahlian/input', 'MasterController@storeKeahlian');
        Route::get('master/delete_keahlian/{id}', 'MasterController@deleteKeahlian');
        /*===============END MASTER KEAHLIAN============*/

        /*===============MASTER KEAHLIAN PERSONIL============*/
        Route::get('master/keahlian_personil/input/{personil_id}', 'MasterController@createKeahlianPersonil');
        Route::get('master/keahlian_personil/input/{personil_id}/{id}', 'MasterController@createKeahlianPersonil');
        Route::post('master/keahlian_personil/input', 'MasterController@storeKeahlianPersonil');
        Route::get('master/delete_keahlian_personil/{id}', 'MasterController@deleteKeahlianPersonil');
        /*===============END MASTER KEAHLIAN PERSONIL============*/

        /*================START PEMBUATAN RAB==========================================*/
        //show rancangan biaya khusus untuk user internal
        //Route::get('internal/rancangan_biaya/{order_id}/{permohonan_id}', 'OrderController@showRancanganBiaya');
        //        Route::get('internal/create_rancangan_biaya/{order_id}', 'OrderController@createRancanganBiaya');
        Route::get('internal/create_rancangan_biaya/{order_id}', 'RABController@createRancanganBiaya2');
        Route::get('internal/create_rancangan_biaya/{order_id}/{rab_id}', 'RABController@createRancanganBiaya2');
        Route::get('internal/detail_rancangan_biaya/{order_id}/{rab_id}', 'RABController@detailRancanganBiaya');
        //        Route::post('internal/create_rancangan_biaya', 'OrderController@storeRancanganBiaya');
        Route::post('internal/create_rancangan_biaya', 'RABController@storeRancanganBiaya2');
        Route::get('internal/delete_rancangan_biaya/{order_id}/{id}', 'RABController@deleteRancanganBiaya');
        Route::post('internal/save_surat_penawaran', 'RABController@saveSuratPenawaran');

        Route::get('internal/rab', 'RABController@showRancanganBiaya');
        Route::get('internal/rancangan_biaya_order/{order_id}', 'RABController@rancanganBiayaOrder');
        Route::get('internal/rancangan_biaya_order/submit/{order_id}', 'RABController@submitRancanganBiaya');
        Route::post('internal/approval_rab', 'RABController@internalApprovalRab');
        Route::get('internal/export_excel/{order_id}/{rab_id}', 'RABController@downloadExcel');

        Route::get('internal/amandemen_rancangan_biaya/{order_id}/{rab_id}', 'RABController@AmandemenRancanganBiaya');
        Route::post('internal/amandemen_rancangan_biaya', 'RABController@StoreAmandemen');

        /*================END PEMBUATAN RAB============================================*/

        Route::get('internal/form_rab', function () {
            return view('internal.rab.create_form_rab');
        });

        Route::get('internal/invoice_rab', function () {
            return view('internal.rab.invoice_rab');
        });
        //START FORM ORDER INTERNAL===========================================================
        Route::get('/internal/order', 'OrderController@showOrdersInternal');
        Route::get('/internal/detail_order/{id}', 'OrderController@detailOrder');
        Route::get('/internal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@verifyPermohonan');
        Route::post('/internal/store_permohonan', 'OrderController@internalStorePermohonan');
        Route::post('/internal/approval_order', 'OrderController@approvalOrder');
        Route::get('/internal/custom_field_verifikasi/{id}/{id_permohonan}/{status_baru}', 'OrderController@createCustomFieldsVerifikasi');
        Route::get('/internal/detail_permohonan_surat_tugas/{id_surat_tugas}/', 'OrderController@detaiPermohonanSuratTugas');

        //END FORM ORDER INTERNAL===========================================================


        //START LOG EMAIL=====================================================
        Route::get('/internal/log_email', 'LogController@showLogEmail');
        Route::get('/internal/resend_email/{id}', 'LogController@resendEmail');
        //END LOG EMAIL=====================================================


        //START LOG ACTIVITY=====================================================
        Route::get('/internal/log_activity', 'LogController@showLogActivity');
        //END LOG ACTIVITY=====================================================

        Route::get('internal/kontrak', 'ContractController@showKontrak');
        Route::get('internal/create_kontrak/', 'ContractController@createKontrak');
        Route::get('internal/detail_kontrak/{id}', 'ContractController@detailKontrak');
        Route::get('internal/create_kontrak/{id}', 'ContractController@createKontrak');
        Route::get('internal/create_kontrak_order/{order_id}', 'ContractController@createKontrakByOrder');
        Route::post('internal/save_kontrak', 'ContractController@storeKontrak');
        Route::get('internal/create_amandemen/{kontrak_id}', 'ContractController@createAmandemen');
        Route::get('internal/detail_amandemen/{kontrak_version_id}', 'ContractController@detailAmandemen');
        Route::post('internal/save_amandemen', 'ContractController@storeAmandemen');
        Route::post('internal/approval_kontrak', 'ContractController@approvalContract');
        Route::post('internal/save_pembayaran', 'ContractController@savePembayaran');
        Route::get('internal/invoice_pembayaran/{id}', 'ContractController@invoicePembayaran');
        Route::get('internal/paid_pembayaran/{id}', 'ContractController@paidPembayaran');
        Route::post('internal/save_jadwal_pelaksanaan', 'ContractController@saveJadwalPelaksanaan');

        Route::get('internal/workflow', 'WorkflowController@showWorkflow');
        Route::get('internal/workflow/add', 'WorkflowController@createWorkflow');
        Route::post('internal/workflow/add', 'WorkflowController@saveWorkflow');
        Route::get('internal/edit_workflow/{id}', 'WorkflowController@createWorkflow');
        Route::get('internal/workflow/createNotif/{id}', 'WorkflowController@createNotif');
        Route::post('internal/workflow/role', 'WorkflowController@storeWorkflowRole');

        //permohonan distribusi
        Route::get('/internal/view_surat_tugas/{id}', 'OrderController@verifyDetailSuratTugas');
        Route::get('/internal/detail_instalasi_program/{id_program}/{id_area_program}/{id_parent}/{id}', 'OrderController@verifyDetailInstalasiProgram');


        //START MASTER BIDANG====================================================
        Route::get('master/unsur', 'MasterController@showUnsur');
        Route::get('master/unsur/input', 'MasterController@insertUnsur');
        Route::get('master/unsur/input/{id}', 'MasterController@insertUnsur');
        Route::post('master/unsur/input', 'MasterController@storeUnsur');
        Route::get('master/unsur/isaktif/{id}', 'MasterController@isAktifUnsur');
        Route::get('master/unsur/preview', 'MasterController@previewUnsur');
        //END MASTER BIDANG====================================================

        #START WBS DAN IO
        Route::get('internal/show_wbs_io', 'WBSIOController@showWbsIo');

        Route::get('internal/create_wbs', 'WBSIOController@createWbs');
        Route::get('internal/create_wbs/{id}', 'WBSIOController@createWbs');
        Route::post('internal/save_wbs_io', 'WBSIOController@saveWbsIo');
        Route::get('internal/detail_wbs_io/{id}', 'WBSIOController@detailWbsIo');
        Route::get('internal/delete_realisasi_wbs_io/{id}', 'WBSIOController@deleteRealisasiWbsIo');
        Route::get('internal/update_status_wbs/{id}', 'WBSIOController@updateStatusWbs');
        Route::post('internal/update_wbs_io', 'WBSIOController@updateWbsIo');
        Route::get('internal/monitoring_wbs', 'WBSIOController@monitoringWbs');
        Route::get('internal/monitoring_io', 'WBSIOController@monitoringIo');
        Route::get('internal/monitoring_wbs/export', 'WBSIOController@exportMonitoringWbs');
        Route::get('internal/monitoring_io/export', 'WBSIOController@exportMonitoringIo');

        Route::get('internal/create_io', 'WBSIOController@createIo');
        Route::get('internal/create_io/{id}', 'WBSIOController@createIo');

        Route::post('internal/save_realisasi', 'WBSIOController@saveRealisasi');
        Route::get('internal/realisasi', 'WBSIOController@realisasi');
        Route::get('internal/delete_realisasi/{id}', 'WBSIOController@deleteRealisasi');
        Route::get('internal/pengalihan_biaya', 'WBSIOController@pengalihanBiaya');
        Route::get('internal/create_pengalihan_biaya/{id?}/{wbs_id?}', 'WBSIOController@createPengalihanBiaya');
        Route::post('internal/save_pengalihan_biaya', 'WBSIOController@savePengalihanBiaya');
        Route::get('internal/delete_pengalihan_biaya/{id}', 'WBSIOController@destroyPengalihanBiaya');

        Route::post('internal/approval_wbs_io', 'WBSIOController@approvalWBSIO');
        #END WBS DAN IO

        Route::post('internal/approval_workflow', 'WorkflowController@approval');

        Route::get('internal/request-invoice/{id}', 'ContractController@requestInvoice');
        Route::post('internal/store-invoice', 'ContractController@storeInvoice');
        Route::get('internal/send-invoice/{id}', 'ContractController@sendInvoice');
        Route::post('internal/store-kuitansi', 'ContractController@storeKuitansi');
        Route::get('internal/send-kuitansi/{id}', 'ContractController@sendKuitansi');

        #START PEKERJAAN
        Route::get('internal/pekerjaan', 'PekerjaanController@showPekerjaan');
        Route::get('internal/create_pekerjaan', 'PekerjaanController@createPekerjaan');
        Route::get('internal/create_pekerjaan/{id}', 'PekerjaanController@editPekerjaan');
        Route::get('internal/detail_pekerjaan/{pekerjaan_id}', 'PekerjaanController@detailPekerjaan');
        Route::post('internal/save_pekerjaan', 'PekerjaanController@savePekerjaan');
        Route::get('internal/delete_pekerjaan/{pekerjaan_id}', 'PekerjaanController@deletePekerjaan');
        Route::post('internal/update_progress_pekerjaan', 'PekerjaanController@updateProgress');

        Route::get('internal/pekerjaan/peralatan', 'PekerjaanController@showPekerjaanPeralatan');
        Route::get('internal/create_pekerjaan_peralatan/{id?}', 'PekerjaanController@createPekerjaanPeralatan');
        Route::post('internal/save_pekerjaan_peralatan', 'PekerjaanController@savePekerjaanPeralatan');
        Route::get('internal/detail_pekerjaan_peralatan/{permohonan_peralatan_id}', 'PekerjaanController@detailPekerjaanPeralatan');
        Route::post('internal/save_laporan_inspeksi', 'PekerjaanController@saveLaporanInspeksi');
        Route::get('internal/manage_pekerjaan_peralatan/{id?}', 'PekerjaanController@managePekerjaanPeralatan');
        Route::post('internal/manage_pekerjaan_peralatan', 'PekerjaanController@updateStatusPeralatan');
        Route::get('internal/print_form_peralatan/{id}', 'PekerjaanController@printFormPeralatan');
        Route::get('internal/print_form_pekerjaan/{id}', 'PekerjaanController@printFormSuratTugas');
        Route::post('internal/save_surat_penugasan_pekerjaan', 'PekerjaanController@saveSuratPenugasan');
        #END PEKERJAN

        #START RLB
        Route::get('internal/rlb', 'LaporanController@showRlb');
        Route::get('internal/create_rlb/{permohonan_rlb_id}/{id?}', 'LaporanController@createRlb');
        Route::get('internal/detail_rlb/{rlb_id}', 'LaporanController@detailRlb');
        Route::post('internal/save_rlb', 'LaporanController@saveRlb');
        Route::get('internal/delete_rlb/{id}', 'LaporanController@deleteRlb');

        Route::get('internal/permohonan_rlb', 'LaporanController@showPermohonanRlb');
        Route::get('internal/create_permohonan_rlb/{id?}', 'LaporanController@createPermohonanRlb');
        Route::post('internal/save_permohonan_rlb', 'LaporanController@saveLaporanRlb');
        Route::get('internal/detail_permohoanan_rlb/{id}', 'LaporanController@detailPermohonanRlb');
        Route::get('internal/delete_permohonan_rlb/{id}', 'LaporanController@deletePermohonanRlb');

        Route::get('internal/print_permohonan_rlb/{id}', 'LaporanController@printPermohonanRlb');
        Route::get('internal/print_rlb/{id}', 'LaporanController@printRlb');
        #END RLB

        #START RLS
        Route::get('internal/rls', 'LaporanController@showRls');
        Route::get('internal/create_rls/{permohonan_rls_id}/{id?}', 'LaporanController@createRls');
        Route::get('internal/detail_rls/{rls_id}', 'LaporanController@detailRls');
        Route::post('internal/save_rls', 'LaporanController@saveRls');
        Route::get('internal/delete_rls/{id}', 'LaporanController@destroyRls');

        Route::get('internal/permohonan_rls', 'LaporanController@showPermohonanRls');
        Route::get('internal/create_permohonan_rls/{id?}', 'LaporanController@createPermohonanRls');
        Route::post('internal/save_permohonan_rls', 'LaporanController@saveLaporanRls');
        Route::get('internal/detail_permohoanan_rls/{id}', 'LaporanController@detailPermohonanRls');
        Route::get('internal/delete_permohonan_rls/{id}', 'LaporanController@deletePermohonanRls');

        Route::get('internal/print_permohonan_rls/{id}', 'LaporanController@printPermohonanRls');
        Route::get('internal/print_rls/{id}', 'LaporanController@printRls');
        #END RLS

        #START BLANGKO INSPEKSI
        Route::get('internal/blangko_inspeksi', 'LaporanController@blangkoInspeksi');
        Route::get('internal/create_blangko_inspeksi', 'LaporanController@createBlangkoInspeksi');
        Route::get('internal/create_blangko_inspeksi/{id}', 'LaporanController@createBlangkoInspeksi');
        Route::post('internal/save_blangko_inspeksi', 'LaporanController@saveBlangkoInspeksi');
        Route::get('internal/detail_blangko_inspeksi/{id}', 'LaporanController@detailBlangkoInspeksi');
        Route::get('internal/delete_blangko_inspeksi/{id}', 'LaporanController@destroyBlangkoInspeksi');
        #END BLANGKO INSPEKSI

        #START KICK OFF MEETING
        Route::get('internal/kick_off', 'LaporanController@kickOffMeeting');
        Route::get('internal/create_kick_off/{id?}', 'LaporanController@createKickOffMeeting');
        Route::post('internal/save_kick_off', 'LaporanController@saveKickOffMeeting');
        Route::get('internal/detail_kick_off/{id}', 'LaporanController@detailKickOffMeeting');
        Route::get('internal/delete_kick_off/{id}', 'LaporanController@destroyKickOffMeeting');
        Route::get('internal/print_kick_off/{id}', 'LaporanController@printKickOffMeeting');
        #END KICK OFF MEETING

        #START LAPORAN PENDING ITEM
        Route::get('internal/pending_item', 'LaporanController@pendingItem');
        Route::get('internal/create_pending_item', 'LaporanController@createPendingItem');
        Route::post('internal/save_pending_item', 'LaporanController@savePendingItem');
        Route::get('internal/detail_pekerjaan_pending_item/{pekerjaan_id}', 'LaporanController@DetailPekerjaanPendingItem');
        Route::get('internal/detail_pending_item/{pending_item_id}', 'LaporanController@DetailPendingItem');
        Route::post('internal/save_tindak_lanjut_pending_item', 'LaporanController@saveTindakLanjutPendingItemm');
        Route::get('internal/print_pending_item/{pekerjaan_id}', 'LaporanController@printPendingItem');
        Route::post('internal/save_status_pending_item', 'LaporanController@saveStatusPendingItem');
        Route::get('internal/delete_pending_item/{id}', 'LaporanController@deletePendingItem');
        #END LAPORAN PENDING ITEM

        #START HILO
        Route::get('internal/hilo', 'LaporanController@hilo');
        Route::get('internal/create_hilo/{id?}', 'LaporanController@createHilo');
        Route::get('internal/generate_form_hilo/{id}', 'LaporanController@generateFormHilo');
        Route::post('internal/save_hilo', 'LaporanController@saveHilo');
        Route::get('internal/detail_hilo/{id}', 'LaporanController@detailHilo');
        Route::get('internal/delete_hilo/{id}', 'LaporanController@destroyHilo');
        Route::get('internal/print_hilo/{id}', 'LaporanController@printHilo');
        #END HILO


        #START DATA TEKNIK
        Route::get('internal/data_teknik', 'LaporanController@dataTeknik');
        Route::get('internal/create_data_teknik/{id?}', 'LaporanController@createDataTeknik');
        Route::post('internal/save_data_teknik', 'LaporanController@saveDataTeknik');
        Route::get('internal/detail_data_teknik/{id}', 'LaporanController@detailDataTeknik');
        Route::get('internal/delete_data_teknik/{id}', 'LaporanController@destroyDataTeknik');
        #END DATA TEKNIK

        Route::get('internal/word', 'LaporanController@ExportWord');


        /*START MASTER STRUKTUR HILO==========================================*/
        Route::get('master/struktur_hilo/{lingkup_id?}', 'MasterController@showStrukturHilo'); //show list data
        Route::get('master/create_struktur_hilo/{lingkup_id}', 'MasterController@createStrukturHilo'); //create new
        Route::post('master/create_struktur_hilo', 'MasterController@storeStrukturHilo'); //action post
        Route::get('master/edit_struktur_hilo/{id}', 'MasterController@editStrukturHilo'); //edit
        Route::get('master/delete_struktur_hilo/{id}', 'MasterController@destroyStrukturHilo'); //delete
        Route::get('master/preview_struktur_hilo/{lingkup_id}', 'MasterController@previewStrukturHilo'); //preview hasil struktur
        /*END MASTER STRUKTUR HILO============================================*/

        /*START MASTER BERITA ACARA INSPEKSI==========================================*/
        Route::get('internal/berita_inspeksi', 'LaporanController@beritaInspeksi'); //show list data
        Route::get('internal/create_berita_inspeksi/{id?}', 'LaporanController@createBeritaInspeksi'); //create new
        Route::post('internal/save_berita_inspeksi', 'LaporanController@saveBeritaInspeksi'); //action post
        Route::get('internal/detail_berita_inspeksi/{id}', 'LaporanController@detailBeritaInspeksi');
        Route::get('internal/delete_berita_inspeksi/{id}', 'LaporanController@destroyBeritaInspeksi'); //delete
        Route::get('internal/print_berita_inspeksi/{id}', 'LaporanController@printBeritaInspeksi');
        /*END MASTER BERITA ACARA INSPEKSI============================================*/


        /*START DETAIL INSTALASI==========================================*/
        Route::get('internal/instalasi_pembangkit/{id}', 'MasterController@internalDetailPembangkit');
        Route::get('internal/instalasi_transmisi/{id}', 'MasterController@internalDetailTransmisi');
        Route::get('internal/instalasi_distribusi/{id}', 'MasterController@internalDetailDistribusi');
        Route::get('internal/instalasi_pemanfaatan_tm/{id}', 'MasterController@internalDetailPemanfaatanTm');
        Route::get('internal/instalasi_pemanfaatan_tt/{id}', 'MasterController@internalDetailPemanfaatanTt');
        /*END DETAIL INSTALASI============================================*/


#START LHPP
        Route::get('internal/lhpp', 'LhppController@showLhpp');
        Route::get('internal/lhpp/createLhpp', 'LhppController@createLhpp');
        Route::get('internal/lhpp/update/{id}', 'LhppController@createLhpp');
        Route::post('internal/lhpp/saveLhpp', 'LhppController@saveLhpp');
        Route::get('internal/lhpp/deleteLhpp/{lhpp_id}', 'LhppController@deleteLhpp');
        Route::get('internal/lhpp/detailLhpp/{lhpp_id}', 'LhppController@detailLhpp');
        Route::get('internal/lhpp/requestNomorAgenda/{lhpp_id}', 'LhppController@requestNomorAgenda');
        Route::get('internal/lhpp/registrasiSlo/{lhpp_id}', 'LhppController@registrasiSlo');
        Route::get('internal/lhpp/generateNoSlo/{lhpp_id}', 'LhppController@generateNoSlo');
#END LHPP

        /*START LPI==========================================*/
        Route::get('internal/lpi', 'LaporanController@laporanLPI'); //show list data
        Route::get('internal/create_lpi/{id?}', 'LaporanController@createLPI');
        Route::post('internal/save_lpi', 'LaporanController@saveLPI');
        Route::get('internal/detail_lpi/{id}', 'LaporanController@detailLPI');
        Route::get('internal/delete_lpi/{id}', 'LaporanController@destroyLPI');
        /*END LPI============================================*/

        ///*======================END REGION INTERNAL=======================*/
    });

    Route::group(['middleware' => ['isEksternal']], function () {

        /*===============================START REGION EKSTERNAL==================================================================*/

        //Dashboard
        //        Route::get('/eksternal', function () {
        //            return view('eksternal/dashboard2');
        //        });
        Route::get('/eksternal', 'PagesController@dashboardEksternal');
        Route::get('/eksternal/list_order_ongoing', function () {
            return view('eksternal/list_order_ongoing');
        });

        Route::get('/eksternal/agreement', function () {
            return view('eksternal/agreement');

        });
        //Form Input Transmisi
        Route::get('/eksternal/transmisi', 'TransmisiController@index');
        Route::post('/eksternal/transmisi', 'TransmisiController@store');

        //Form Input Pembangkit
        Route::get('/eksternal/pembangkit', 'PembangkitController@index');
        Route::post('/eksternal/pembangkit', 'PembangkitController@store');

        //Form Input Distribusi
        Route::get('/eksternal/distribusi', function () {
            return view('eksternal/distribusi');
        });

        //Riwayat Permohonan SLO
        Route::get('/eksternal/riwayat', 'RiwayatController@index');

        //Tracking
        Route::get('/eksternal/tracking', 'TrackingController@index');

        //Pemanfaatan
        Route::get('/eksternal/pemanfaatan', function () {
            return view('eksternal/pemanfaatan');
        });

        #Sample API DJK
        // Route::get('/eksternal/sample_djk', function () {
        //     return view('eksternal/sample_api_djk');
        // });


        /*================START DATA INSTALASI======================================*/
        //PEMBANGKIT
        Route::get('/eksternal/view_instalasi_pembangkit', 'MasterController@showPembangkit');

        Route::group(['middleware' => ['instalasi_pembangkit']], function () {
            Route::get('/eksternal/instalasi-pembangkit/{id}', ['as' => 'add-instalasi-cart', 'uses' => 'MasterController@detailPembangkit']);
            Route::get('/eksternal/edit-instalasi-pembangkit/{id}', 'MasterController@insertPembangkit');
        });

        Route::get('/eksternal/create_instalasi_pembangkit', 'MasterController@insertPembangkit');
        Route::get('/eksternal/create_instalasi_pembangkit/{id}', 'MasterController@insertPembangkit');
        Route::get('/eksternal/delete_instalasi_pembangkit/{id}', 'MasterController@deletePembangkit');
        Route::post('/eksternal/create_instalasi_pembangkit', 'MasterController@storePembangkit');

        //TRANSMISI
        Route::get('/eksternal/view_instalasi_transmisi', 'MasterController@showTransmisi');

        Route::group(['middleware' => ['instalasi_transmisi']], function () {
            Route::get('/eksternal/instalasi-transmisi/{id}', 'MasterController@detailTransmisi');
            Route::get('/eksternal/edit-instalasi-transmisi/{id}', 'MasterController@insertTransmisi');
        });

        Route::get('/eksternal/create_instalasi_transmisi', 'MasterController@insertTransmisi');
        Route::get('/eksternal/create_instalasi_transmisi/{id}', 'MasterController@insertTransmisi');
        Route::get('/eksternal/delete_instalasi_transmisi/{id}', 'MasterController@deleteTransmisi');
        Route::post('/eksternal/create_instalasi_transmisi', 'MasterController@storeTransmisi');
        Route::get('/eksternal/detail_bay_gardu/{id_transmisi}', 'MasterController@showBayGardu');
        Route::get('/eksternal/edit_bay_gardu/{id_transmisi}/{id?}', 'MasterController@editBayGardu');
        Route::get('/eksternal/create_bay_gardu/{id_transmisi}/{tipe}', 'MasterController@insertBayGardu');
        Route::post('/eksternal/create_bay_gardu', 'MasterController@storeBayGardu');
        Route::get('/eksternal/delete_bay_gardu/{id}', 'MasterController@deleteBayGardu');

        //DISTRIBUSI
        Route::get('/eksternal/view_instalasi_distribusi', 'MasterController@showDistribusi');

        Route::group(['middleware' => ['instalasi_distribusi']], function () {
            Route::get('/eksternal/instalasi-distribusi/{id}', 'MasterController@detailDistribusi');
            Route::get('/eksternal/edit-instalasi-distribusi/{id}', 'MasterController@insertDistribusi');
        });

        Route::get('/eksternal/create_instalasi_distribusi', 'MasterController@insertDistribusi');
        Route::get('/eksternal/create_instalasi_distribusi/{id}', 'MasterController@insertDistribusi');
        Route::get('/eksternal/delete_instalasi_distribusi/{id}', 'MasterController@deleteDistribusi');
        Route::post('/eksternal/create_instalasi_distribusi', 'MasterController@storeDistribusi');

        //PEMANFAATAN TT
        Route::get('/eksternal/view_instalasi_pemanfaatan_tt', 'MasterController@showPemanfaatanTT');

        Route::group(['middleware' => ['instalasi_pemanfaatan_tt']], function () {
            Route::get('/eksternal/instalasi-pemanfaatan-tt/{id}', 'MasterController@detailPemanfaatanTT');
            Route::get('/eksternal/edit-instalasi-pemanfaatan-tt/{id}', 'MasterController@insertPemanfaatanTT');
        });

        Route::get('/eksternal/create_instalasi_pemanfaatan_tt', 'MasterController@insertPemanfaatanTT');
        Route::get('/eksternal/create_instalasi_pemanfaatan_tt/{id}', 'MasterController@insertPemanfaatanTT');
        Route::get('/eksternal/delete_instalasi_pemanfaatan_tt/{id}', 'MasterController@deletePemanfaatanTT');
        Route::post('/eksternal/create_instalasi_pemanfaatan_tt', 'MasterController@storePemanfaatanTT');

        //PEMANFAATAN TM
        Route::get('/eksternal/view_instalasi_pemanfaatan_tm', 'MasterController@showPemanfaatanTM');

        Route::group(['middleware' => ['instalasi_pemanfaatan_tm']], function () {
            Route::get('/eksternal/instalasi-pemanfaatan-tm/{id}', 'MasterController@detailPemanfaatanTM');
            Route::get('/eksternal/edit-instalasi-pemanfaatan-tm/{id}', 'MasterController@insertPemanfaatanTM');
        });

        Route::get('/eksternal/create_instalasi_pemanfaatan_tm', 'MasterController@insertPemanfaatanTM');
        Route::get('/eksternal/create_instalasi_pemanfaatan_tm/{id}', 'MasterController@insertPemanfaatanTM');
        Route::get('/eksternal/delete_instalasi_pemanfaatan_tm/{id}', 'MasterController@deletePemanfaatanTM');
        Route::post('/eksternal/create_instalasi_pemanfaatan_tm', 'MasterController@storePemanfaatanTM');
        /*================END DATA INSTALASI======================================*/

        /*===========MASTER PEMILIK INSTALASI===================*/
        Route::get('/eksternal/pemilik_instalasi', 'MasterController@showPemilikInstalasi');
        Route::get('/eksternal/create_pemilik_instalasi', 'MasterController@insertPemilikInstalasi');
        Route::get('/eksternal/create_pemilik_instalasi/{id}', 'MasterController@insertPemilikInstalasi');
        Route::post('/eksternal/create_pemilik_instalasi', 'MasterController@storePemilikInstalasi');
        Route::get('/eksternal/detail_pemilik_instalasi/{id}', 'MasterController@detailPemilikInstalasi');
        Route::get('/eksternal/delete_pemilik_instalasi/{id}', 'MasterController@deletePemilikInstalasi');
        /*==========END MASTER PEMILIK INSTALASI================*/

        Route::get('master/pemilik_instalasi', 'MasterController@showPemilikInstalasi');
        Route::get('master/pemilik_instalasi/add', 'MasterController@insertPemilikInstalasi');
        Route::get('master/pemilik_instalasi/edit/{id}', 'MasterController@insertPemilikInstalasi');
        Route::get('master/pemilik_instalasi/delete/{id}', 'MasterController@deletePemilikInstalasi');
        Route::get('master/pemilik_instalasi/detail/{id}', 'MasterController@detailPemilikInstalasi');
        Route::post('master/pemilik_instalasi/save', 'MasterController@storePemilikInstalasi');

        //START FORM ORDER===========================================================
        Route::get('/eksternal/view_order', 'OrderController@showOrder');
        Route::get('/eksternal/create_order', 'OrderController@editOrder');
        Route::get('/eksternal/create_order/{id}', 'OrderController@editOrder');
        Route::get('/eksternal/detail_order/{id}', 'OrderController@detailOrderEksternal');
        Route::post('/eksternal/create_order', 'OrderController@storeGeneralOrder');
        Route::post('/eksternal/submit_order', 'OrderController@submitOrder');
        Route::post('/eksternal/revise_order', 'OrderController@reviseOrder');
        //END FORM ORDER===========================================================

        //START FORM PERMOHONAN===========================================================
        Route::get('/eksternal/create_permohonan/{order_id}', 'OrderController@createPermohonan');
        Route::get('/eksternal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@createPermohonan');
        Route::post('/eksternal/create_permohonan/{order_id}/{permohonan_id}', 'OrderController@storePermohonan');
        Route::post('/eksternal/store_permohonan', 'OrderController@storePermohonan'); //sample Renisa
        Route::get('/eksternal/custom_field/{id}/{id_permohonan}/{status_baru}', 'OrderController@createCustomFields');
        Route::post('/eksternal/custom_field', 'OrderController@storeCustomFields');
        Route::get('/eksternal/custom_field_verifikasi/{id}/{id_permohonan}/{status_baru}', 'OrderController@createCustomFieldsVerifikasi');
        Route::get('/eksternal/delete_permohonan/{id}', 'OrderController@deletePermohonan');
        Route::post('/eksternal/filter_bay', 'OrderController@filterBay');
        //END FORM PERMOHONAN===========================================================

        //START RAB EKTERNAL
        //    Route::get('/eksternal/rancangan_biaya/{order_id}/{rab_id}', 'RABController@viewRABEksternal');
        //END RAB EKSTERNAL


        //START MASTER LINGKUP KERJA PROGRAM===============================================================
        Route::post('master/lingkup_program/input', 'MasterController@insertLingkupProgram');
        Route::post('master/lingkup_program/store', 'MasterController@storeLingkupProgram');
        Route::get('master/lingkup_program/delete/{id}', 'MasterController@deleteLingkupProgram');
        //END MASTER LINGKUP KERJA PROGRAM=================================================================


        //START LAYANAN===============================================================
        Route::get('eksternal/layanan/{id_produk}/{id_tipe_instalasi}', 'LayananController@pilihInstalasi');
        Route::get('eksternal/layanan/{id_produk}/{id_tipe_instalasi}/{id_instalasi}', 'LayananController@detailInstalasi');

        //END LAYANAN=================================================================

        //START ORDER ================================================================
        Route::post('eksternal/add-to-cart/', 'OrderController@addToCart');
        Route::get('eksternal/keranjang/', 'OrderController@viewKeranjang');
        Route::get('eksternal/keranjang/delete/{id}', ['as' => 'keranjang-delete', 'uses' => 'OrderController@deleteChartPermohonan']);
        Route::get('eksternal/keranjang/delete_surat_tugas/{id}', ['as' => 'keranjang-delete-surat-tugas', 'uses' => 'OrderController@deleteChartPermohonanDistribusi']);
        Route::post('eksternal/checkout/', 'OrderController@checkOutOrder');
        //END ORDER ==================================================================


        //MASTER PROGRAM DISTRIBUSI===================================================
        //surat tugas
        Route::get('/eksternal/surat_tugas', 'MasterController@showSuratTugas');
        Route::get('/eksternal/create_surat_tugas', 'MasterController@insertSuratTugas');
        Route::get('/eksternal/create_surat_tugas/{id}', 'MasterController@insertSuratTugas');
        Route::post('/eksternal/create_surat_tugas', 'MasterController@storeSuratTugas');
        Route::get('/eksternal/delete_surat_tugas/{id}', 'MasterController@deleteSuratTugas');
        Route::get('/eksternal/view_surat_tugas/{id}', 'MasterController@detailSuratTugas');

        //program distribusi
        Route::get('/eksternal/create_program/{id_surat_tugas}', 'MasterController@insertProgramDistribusi');
        Route::get('/eksternal/create_program/{id_surat_tugas}/{id}', 'MasterController@insertProgramDistribusi');
        Route::post('/eksternal/create_program', 'MasterController@storeProgramDistribusi');
        Route::post('/eksternal/delete_program/{id_surat_tugas}/{id}', 'MasterController@deleteProgramDistribusi');

        //area
        Route::get('/eksternal/create_area_program/{id_program}', 'MasterController@insertAreaProgram');
        Route::get('/eksternal/create_area_program/{id_program}/{id}', 'MasterController@insertAreaProgram');
        Route::post('/eksternal/create_area_program', 'MasterController@storeAreaProgram');
        Route::get('/eksternal/delete_area_program/{id_surat_tugas}/{id}', 'MasterController@deleteAreaProgram');

        //kontrak
        Route::get('/eksternal/create_kontrak_program/{id_program}', 'MasterController@insertKontrakProgram');
        Route::get('/eksternal/create_kontrak_program/{id_program}/{id}', 'MasterController@insertKontrakProgram');
        Route::post('/eksternal/create_kontrak_program', 'MasterController@storeKontrakProgram');
        Route::get('/eksternal/delete_kontrak_program/{id_surat_tugas}/{id}', 'MasterController@deleteKontrakProgram');

        //lokasi
        Route::get('/eksternal/create_lokasi_area/{id_program}/{id_area_kontrak}', 'MasterController@insertLokasiArea');
        Route::get('/eksternal/create_lokasi_area/{id_program}/{id_area_kontrak}/{id}', 'MasterController@insertLokasiArea');
        Route::post('/eksternal/create_lokasi_area', 'MasterController@storeLokasiArea');
        Route::get('/eksternal/delete_lokasi_area/{id_surat_tugas}/{id}', 'MasterController@deleteLokasiArea');
        Route::post('/eksternal/get_lokasi', 'MasterController@getLokasi');

        //penyulang
        Route::get('/eksternal/create_penyulang_area/{id_program}/{id_area_program}', 'MasterController@insertPenyulangArea');
        Route::get('/eksternal/create_penyulang_area/{id_program}/{id_area_program}/{id}', 'MasterController@insertPenyulangArea');
        Route::post('/eksternal/create_penyulang_area', 'MasterController@storePenyulangArea');
        Route::get('/eksternal/delete_penyulang_area/{id_surat_tugas}/{id}', 'MasterController@deletePenyulangArea');
        Route::post('/eksternal/get_penyulang', 'MasterController@getPenyulang');

        //gardu
        Route::get('/eksternal/create_gardu_area/{id_program}/{id_area_program}', 'MasterController@insertGarduArea');
        Route::get('/eksternal/create_gardu_area/{id_program}/{id_area_program}/{id}', 'MasterController@insertGarduArea');
        Route::post('/eksternal/create_gardu_area', 'MasterController@storeGarduArea');
        Route::get('/eksternal/delete_gardu_area/{id_surat_tugas}/{id}', 'MasterController@deleteGarduArea');
        Route::post('/eksternal/get_gardu', 'MasterController@getGardu');

        //instalasi_program
        Route::get('/eksternal/create_instalasi_program/{id_program}/{id_area_program}/{id_parent}', 'MasterController@insertInstalasiProgram');
        Route::get('/eksternal/create_instalasi_program/{id_program}/{id_area_program}/{id_parent}/{id}', 'MasterController@insertInstalasiProgram');
        Route::post('/eksternal/create_instalasi_program', 'MasterController@storeInstalasiProgram');
        Route::get('/eksternal/delete_instalasi_program/{id_surat_tugas}/{id}', 'MasterController@deleteInstalasiProgram');
        Route::get('/eksternal/detail_instalasi_program/{id_program}/{id_area_program}/{id_parent}/{id}', 'MasterController@detailInstalasiProgram');

        //delete komponen program distribusi
        Route::get('/eksternal/edit_komponen_program/{param1}/{param2}/{param3}/{param4}/{tipe}', 'MasterController@editKomponenProgramDistribusi');
        Route::get('/eksternal/delete_komponen_program/{id_surat_tugas}/{id}/{tipe}', 'MasterController@deleteKomponenProgramDistribusi');
        Route::get('/eksternal/detail_komponen_program/{param1}/{param2}/{param3}/{param4}/{tipe}', 'MasterController@detailKomponenProgramDistribusi');

        //instalasi distribusi add to cart
        Route::get('/eksternal/addtocart_surat_tugas/{id}', 'OrderController@addToCartSuratTugas');

        //modul laporan
        Route::get('/eksternal/laporan/', 'LaporanController@showLaporanEksternal');
        Route::get('/eksternal/laporan_pekerjaan/{id}', 'LaporanController@showLaporanPekerjaanEksternal');
        //==========END MASTER PROGRAM DISTRIBUSI===================================================


        /*Route untuk mendapatkan  data pemilik instalasi, route ini seharuanya ditempatkan pada
        tempat yang bersangkutan,  apabila route ini masih di sini, harap dipindahkan ke tempatnya tyhanks*/
        Route::post('eksternal/get_pemilik_instalasi', 'MasterController@getNamaPemilikInstalasi');

        Route::get('eksternal/rancangan_biaya_order/{order_id}', 'RABController@rancanganBiayaEksternal');
        Route::get('eksternal/edit_rancangan_biaya_order/{order_id}', 'RABController@editRancanganBiayaEksternal');
        Route::post('eksternal/edit_rancangan_biaya_order', 'RABController@storeRancanganBiayaEksternal');
        Route::post('eksternal/upload_surat_penolakan', 'RABController@eksternalUploadSuratPenolakan');
        Route::post('eksternal/upload_surat_penugasan', 'RABController@eksternalUploadSuratPenugasan');
        Route::post('eksternal/approval_rab', 'RABController@eksternalApprovalRab');

        Route::get('eksternal/invoice_pembayaran/{id}', 'ContractController@eksternalInvoicePembayaran');
        Route::post('eksternal/save_bukti_pembayaran', 'ContractController@saveBuktiPembayaran');

        Route::get('eksternal/kontrak', 'ContractController@eksternalShowKontrak');
        Route::get('eksternal/detail_kontrak/{id}', 'ContractController@eksternalDetailKontrak');
        Route::get('eksternal/detail_amandemen/{kontrak_version_id}', 'ContractController@eksternalDetailAmandemen');
        Route::post('eksternal/save_jadwal_pelaksanaan', 'ContractController@saveJadwalPelaksanaan');

        /*======================END REGION EKSTERNAL=====================================================================================*/

    });


    /*===================START FORM UPLOAD PERMOHONAN======================================================*/
    Route::post('master/delete_program_child', 'MasterController@deleteProgramTreeChild'); //->what is this?
    /*===========================END FORM UPLOAD PERMOHONAN=======================================*/

    /*================LOAD FORM DETAIL INSTALASI DI PERMOHONAN====================*/
    Route::get('/eksternal/instalasi_field/{tipe_instalasi}/{id}', 'OrderController@createDetailInstalasi');
    /*================END LOAD FORM DETAIL INSTALASI DI PERMOHONAN====================*/
    /*================AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN====================*/
    Route::get('/eksternal/get_area_kontrak/{id_program}', 'OrderController@getAreaKontrak');
    Route::get('/eksternal/get_lokasi_penyulang/{id_area_kontrak}', 'OrderController@getLokasiPenyulang');
    Route::get('/eksternal/get_lingkup_program/{id_program}/{id_lokasi_penyulang}/{id_area_kontrak}/{id_permohonan}', 'OrderController@getLingkupProgram');
    /*================END AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN====================*/

    Route::get('notif_list', 'NotifController@index');
    Route::get('notif/{id}', 'NotifController@readThenRedirect');

    /*======================END REGION BEBAS=======================*/

    /*=================START USER PROFILE==========================*/
    Route::get('internal/profil/{id}', 'UserController@viewProfileUser');
    Route::get('eksternal/profil/{id}', 'UserController@viewProfileUser');

    Route::get('eksternal/profile/edit/{id}', 'UserEksternalController@editProfileUser');
    Route::post('eksternal/profile/edit/{id}', 'UserEksternalController@updateProfileUser');
    /*================END USER PROFILE==============================*/

    /*=================START TEST UI==========================*/
    Route::get('eksternal/test_dashboard', function () {
        return view('eksternal/test_dashboard');
    });
    Route::get('eksternal/test_detail_instalasi', function () {
        return view('eksternal/test_detail_instalasi');
    });
    Route::get('eksternal/test_detail_layanan', function () {
        return view('eksternal/test_detail_layanan');
    });
    /*================END TEST UI==============================*/


    Route::get('job_email', 'LogController@sendEmailJob');


});

/*======================START REGION BEBAS=======================*/
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('auth_login');
Route::post('auth/login', 'Auth\AuthController@validateLDAP');
Route::get('auth/register', 'UserEksternalController@registerInternal');
Route::post('auth/register', 'UserEksternalController@storeInternal');

//frontend
Route::get('/', function () {
    return view('front/home');
});
Route::get('/s_sistem_manajemen_mutu', function () {
    return view('front/s_sistem_manajemen_mutu');
});
Route::get('/s_sistem_manajemen_lingkungan', function () {
    return view('front/s_sistem_manajemen_lingkungan');
});

Route::get('/s_sistem_pengawasan_mutu', function () {
    return view('front/s_sistem_pengawasan_mutu');
});

Route::get('/slo_&_komisioning', function () {
    return view('front/slo_&_komisioning');
});

Route::get('/s_sistem_manajemen_OHSAS', function () {
    return view('front/s_sistem_manajemen_OHSAS');
});

Route::get('/smk3', function () {
    return view('front/smk3');
});

Route::get('/pusertif', function () {
    return view('front/pusertif');
});

Route::get('/panduan', function () {
    return view('front/panduan');
});

//peminta jasa non-pln
Route::get('register', 'UserEksternalController@createUserPemintaJasa');
Route::post('/register', 'UserEksternalController@storeExternal');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('auth_logout');


Route::get('auth/confirm_register', function () {
    return view('auth.confirm_register');
});

Route::get('auth/confirm_register', function () {
    return view('auth.confirm_register');
});

Route::get('check_email/{email}', 'UserController@checkEmailAlreadyExisted'); //05092016, check email is already exist


/*====================GET UPLOADED FILES=======================================*/
Route::get('upload/{filename}', function ($filename) {
    // Check if file exists in app/storage/file folder
    $arr = explode("-", $filename);

    $file_path = storage_path() . '/upload/' . $arr[0] . '/' . $filename;
    //    $finfo = new finfo(FILEINFO_MIME);
    //    $type = $finfo->file($file_path);
    if (file_exists($file_path)) {
        // Send Download
        /*
        * This is for force download file
        * return Response::download($file_path, $filename, [
        'Content-Length: ' . filesize($file_path)
    ]);*/
        /*This one is for view the file*/
        return Response::make(file_get_contents($file_path), 200, [
            'Content-Type' => mime_content_type($file_path),
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    } else {
        // Error
        exit('Requested file does not exist on our server!');
    }
});

Route::get('upload/instalasi/{filename}', function ($filename) {
    // Check if file exists in app/storage/file folder

    $arr = explode("-", $filename);

    $file_path = storage_path() . '/upload/foto_instalasi/' . $filename;
    //    dd($file_path);
    //    $finfo = new finfo(FILEINFO_MIME);
    //    $type = $finfo->file($file_path);
    if (file_exists($file_path)) {
        // Send Download
        /*
        * This is for force download file
        * return Response::download($file_path, $filename, [
        'Content-Length: ' . filesize($file_path)
    ]);*/
        /*This one is for view the file*/
        return Response::make(file_get_contents($file_path), 200, [
            'Content-Type' => 'image',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    } else {
        // Error
        exit('Requested file does not exist on our server!');
    }
});
/*====================END GET UPLOADED FILES=====================================*/

Route::get('form_sld', function () {
    return view('eksternal/form_main_single_diagram');
});

Route::get('form_tata_letak', function () {
    return view('eksternal/form_instalasi_tata_letak');
});


Route::get('form_sbujptl', function () {
    return view('eksternal/form_sbujptl');
});

Route::get('form_iujptl', function () {
    return view('eksternal/form_iujptl');
});


//temp route START DATA UMUM ORDER============================================
Route::get('form/data_umum_order', function () {
    return view('data_umum_order');
});
//temp route END DATA UMUM ORDER============================================


Route::get('internal/test', function () {
    dd(\App\businessAreaUser::all()[0]->business_area);
});

Route::get('kontraktor/search/{key}', 'KontraktorController@searchKontraktor');

#API DJK MASTER
Route::get('internal/api/getLitPjt', 'ApiController@getLitPJT');
Route::get('internal/api/getLitTt', 'ApiController@getLitTT');
Route::get('internal/api/getPlnArea', 'ApiController@getPlnArea');
Route::get('internal/api/getPlnRayon', 'ApiController@getPlnRayon');
Route::get('internal/api/getPlnWilayah', 'ApiController@getPlnWilayah');
Route::get('internal/api/getRefKota', 'ApiController@getRefKota');
Route::get('internal/api/getRefProvinsi', 'ApiController@getRefProvinsi');
Route::get('internal/api/getRefSbu', 'ApiController@getRefSbu');
#END API DJK MASTER

#API DJK REQUEST
#-- start permohonan baru
Route::get('internal/api/getHasilSlo', 'ApiController@getHasilSLO');
Route::get('internal/api/getHasilSlo/{lhpp_id}', 'ApiController@getHasilSLO');
Route::get('internal/api/showPermohonan', 'ApiController@showPermohonan');
Route::get('internal/api/createPermohonan', 'ApiController@createPermohonan');
Route::post('internal/api/savePermohonan', 'ApiController@savePermohonan');
#-- end permohonan baru

#-- start permohonan baru
Route::get('internal/api/updatePermohonan/{id}', 'ApiController@updatePermohonan');
Route::post('internal/api/saveUpdatePermohonan', 'ApiController@saveUpdatePermohonan');
#-- end permohonan baru

#-- start registrasi SLO
Route::get('internal/api/pengajuanSlo/{id}', 'ApiController@pengajuanSlo');
Route::post('internal/api/saveSloKit', 'ApiController@saveSloKit');
Route::post('internal/api/saveSloTransmisi', 'ApiController@saveSloTrans');

Route::post('internal/api/saveSloTrans', 'ApiController@saveSloTrans');
Route::post('internal/api/saveSloDistribusi', 'ApiController@saveSloDistribusi');
Route::post('internal/api/saveSloPemanfaatan', 'ApiController@saveSloPemanfaatan');
#-- end registrasi SLO
Route::get('internal/api/showDetail/{permohonan_id}', 'ApiController@showDetail');
#END API DJK REQUEST

Route::get('internal/api/tes', function () {
    return view('api.detail_permohonan');
});


#START HALAMAN PUBLIC
Route::get('public/lhpp/lokasiInstalasi/{lhpp_id}', 'LhppController@lokasiInstalasi');
Route::get('public/lhpp/{lhpp_id}', 'LhppController@lhppPublic');
Route::get('public/lhpp/foto/{lhpp_id}', 'LhppController@lhppFoto');
#END HALAMAN PUBLIC

#START TRACKING
Route::get('internal/tracking', 'TrackingController@showTracking');
Route::get('eksternal/tracking', 'TrackingController@showTrackingEksternal');
#END TRACKING

#START MANUAL USER
Route::get('eksternal/download/manual', 'MasterController@downloadManual');
#END MANUAL USER

Route::get('debug', function(){
    // $tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
    // dd($tStamp);
    dd(url('/upload').'/'.'tes');
});

Route::get('refreshPermission', function () {
    Session::put('user_permissions', \App\User::getUserPermission(Auth::user()));
});
