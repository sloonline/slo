<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if($request->user()->isGuest()){
        if($request->user()->hasRole('guest')){
//            return redirect('/')->with('warning', 'Anda tidak diperbolehkan mengakses halaman ini. \nCause: User group Guest.');
            return redirect('/')->with('warning', 'You are not authorized!');
        }
        return $next($request);
    }
}
