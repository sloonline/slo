<?php

namespace App\Http\Middleware;

use Closure;

class CheckIsInternal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->perusahaan_id == null){
            return $next($request);
        }else{
            return redirect('/')->with('warning', 'You are not authorized!');
        }
    }
}
