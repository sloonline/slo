<?php

namespace App\Http\Middleware;

use Closure;

class CheckIsEksternal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->perusahaan() != null){
            return $next($request);
        }else{
            return redirect('/')->with('warning', 'You are not authorized!');
        }
    }
}
