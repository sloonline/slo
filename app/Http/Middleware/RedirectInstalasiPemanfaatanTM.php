<?php

namespace App\Http\Middleware;

use App\InstalasiPemanfaatanTM;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectInstalasiPemanfaatanTM
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id_instalasi   = $request->segment(3);

        $instalasi      = InstalasiPemanfaatanTM::findOrFail($id_instalasi);
//        dd($instalasi);

        if(Auth::user()->username!=$instalasi->created_by){
            return redirect('/eksternal')->with('error','You are not authorized.');
        }
        return $next($request);
    }
}
