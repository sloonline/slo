<?php

namespace App\Http\Controllers;

use App\StatusPekerja;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\WbsIo;
use App\Permohonan;
use App\Pekerjaan;
use App\Peralatan;
use App\PersonilInspeksi;
use App\JabatanInspeksi;
use App\PekerjaanPersonil;
use App\LaporanPekerjaan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\PermohonanPeralatan;
use App\PekerjaanPeralatan;
use Illuminate\Support\Facades\File;
use App\References;
use App\HistoryPeralatan;
use App\Tracking;
use Illuminate\Support\Facades\Session;

class PekerjaanController extends Controller
{
    public function showPekerjaan()
    {
        $pekerjaan = Pekerjaan::orderBy('updated_at', 'desc')->get();
        // dd($pekerjaan);
        return view('internal.pekerjaan.pekerjaan', compact('pekerjaan'));
    }

    public function createPekerjaan()
    {
        $wbs = WbsIo::permohonanForPekerjaan();
        $jabatan = JabatanInspeksi::all();
        $pekerjaan_id = 0;
        // JabatanInspeksi::availablePersonil();
        // $jabatan[0]->personil[0]->isBooked($jabatan[0]->personil[0]->id);
        // dd($wbs);
        return view('internal.pekerjaan.create_pekerjaan', compact('wbs', 'jabatan', 'pekerjaan_id'));
    }


    public function editPekerjaan($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        $jabatan = JabatanInspeksi::all();
        $wbsIo = $pekerjaan->wbs_io;
        $personilPekerjaan = $pekerjaan->pekerjaan_personil;
        $data_personil = array();
        foreach ($jabatan as $key => $item) {
            $data = array();
            foreach ($item->personil as $key => $personil) {
                if ($item->nama_jabatan == PELAKSANA_INSPEKSI) {
                    if (sizeof($personilPekerjaan->where('personil_id', $personil->id)) > 0 || !$personil->isBooked($personil->id, @$wbsIo->tanggal_mulai)) {
                        array_push($data, $personil);
                    }
                } else {
                    array_push($data, $personil);
                }
            }
            $personil = array();
            $personil["jabatan_id"] = $item->id;
            $personil["jabatan"] = $item->nama_jabatan;
            $personil["personil"] = $data;
            array_push($data_personil, $personil);
        }
        $id_personil = $pekerjaan->id_personil->toArray();

        return view('internal.pekerjaan.edit_pekerjaan', compact('data_personil', 'id', 'pekerjaan', 'jabatan', 'id_personil'));
    }

    public function savePekerjaan(Request $request)
    {
        $id = $request->id;
        #start store pekerjaan right here
        $pekerjaan = ($id > 0) ? Pekerjaan::findOrFail($id) : new Pekerjaan();
        #set flow status CREATED pada pekerjaan yang baru dibuat
        if ($id == 0) {
            $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_PEKERJAAN);
            $pekerjaan->id_flow_status = $flow_status->id;
            $pekerjaan->permohonan_id = $request->permohonan;
            $pekerjaan->wbs_id = $request->wbs_io;
        }
        
//	dd(WorkflowController::getFlowStatus(CREATED, MODUL_PEKERJAAN));
	$pekerjaan->pekerjaan = $request->pekerjaan;
//        $pekerjaan->no_surat_penugasan = $request->no_surat_penugasan;
//        $pekerjaan->tgl_surat_penugasan = $request->tgl_surat_penugasan;
        $pekerjaan->uraian_pekerjaan = $request->uraian_pekerjaan;
        $pekerjaan->tgl_mulai_pekerjaan = $request->tgl_mulai;
        $pekerjaan->tgl_selesai_pekerjaan = $request->tgl_selesai;
        //        $pekerjaan->progress = 0;

//        if ($request->file_surat_penugasan != null) {
//            if ($pekerjaan->file_surat_penugasan != null) {
//                $fullPath = storage_path() . '/upload/surat_penugasan/' . $pekerjaan->file_surat_penugasan;
//                if (File::exists($fullPath))
//                    File::delete($fullPath);
//            }
//            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
//            $name = 'surat_penugasan-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_surat_penugasan->getClientOriginalExtension();
//            $url_surat = $name;
//            $request->file_surat_penugasan->move(storage_path() . '/upload/surat_penugasan/', $name);
//            $pekerjaan->file_surat_penugasan = $url_surat;
//        }
        $pekerjaan->save();
        #----end store pekerjaan----

        #store pekerjaan_personil right Here
        //delete dulu yang lama
        $del_personil = $pekerjaan->pekerjaan_personil;
        foreach ($del_personil as $del) {
            $del->delete();
        }
        $jabatan = JabatanInspeksi::all();
        foreach ($jabatan as $item) {
            $text = 'pilih_personil_' . $item->id;
            for ($i = 0; $i < sizeOf($request->$text); $i++) {
                $val = $request->$text;
                $pekerjaan_personil = new PekerjaanPersonil();
                $pekerjaan_personil->personil_id = $val[$i];
                $pekerjaan_personil->pekerjaan_id = $pekerjaan->id;
                $pekerjaan_personil->jabatan_id = $item->id;
                $pekerjaan_personil->save();
            }
        }
        #---end store pekerjaan_personil---

        if ($id == 0) WorkflowController::createFlowHistory($pekerjaan->id, $pekerjaan->getTable(), MODUL_PEKERJAAN, $flow_status, CREATED);

        #create tracking
        $tracking = Tracking::where('wbsio_id', $request->wbs_io)->get();
        if ($tracking != null) {
            foreach ($tracking as $item) {
                $item->pekerjaan_id = $pekerjaan->id;
                $item->status = STATUS_PEKERJAAN;
                $item->save();
            }
        }
        #---------------

        return redirect('internal/pekerjaan')->with('success', 'Data pekerjaan berhasil disimpan.');
    }

    public function detailPekerjaan($pekerjaan_id)
    {
        $pekerjaan = Pekerjaan::find($pekerjaan_id);
        $isLastApproval = Pekerjaan::isLastApproval($pekerjaan);
        $wbs = WbsIo::permohonanForPekerjaan();
        $jabatan = JabatanInspeksi::all();
        $pekerjaan_personil = $pekerjaan->pekerjaan_personil;
        $data_workflow = array(
            'model' => MODEL_PEKERJAAN,
            'modul' => MODUL_PEKERJAAN,
            'url_notif' => 'detail_pekerjaan/',
            'url_redirect' => 'internal/pekerjaan/'
        );
        $laporan_pekerjaan = LaporanPekerjaan::where('pekerjaan_id', $pekerjaan_id)->first();

        $workflow = WorkflowController::workflowJoss($pekerjaan_id, TABLE_PEKERJAAN, ACTION_WORKFLOW);
        //dd($workflow);
	$permohonan_peralatan = PermohonanPeralatan::where('pekerjaan_id', $pekerjaan_id)->get();
        // dd($workflow);
        // dd(@$laporan_pekerjaan->pemeriksaan_dokumen == 0);
        return view('internal.pekerjaan.detail_pekerjaan', compact('laporan_pekerjaan', 'isLastApproval', 'permohonan_peralatan', 'pekerjaan_id', 'workflow', 'data_workflow', 'pekerjaan', 'jabatan', 'wbs', 'pekerjaan_personil'));
    }

    public function updateProgress(Request $request)
    {
        $pekerjaan = Pekerjaan::find($request->pekerjaan_id);
        $pekerjaan->progress = $request->progress;
        $pekerjaan->save();
        return redirect('internal/pekerjaan')->with('success', 'Berhasil melakukan update progress.');
    }


    public function deletePekerjaan($pekerjaan_id)
    {
        $pekerjaan = Pekerjaan::find($pekerjaan_id);
        $nama = @$pekerjaan->pekerjaan;
        if($pekerjaan != null){
            $pekerjaan->delete();
        }
        return redirect('internal/pekerjaan')->with('success', 'Berhasil menghapus pekerjaan '.$nama);
    }

    public function showPekerjaanPeralatan()
    {
        $permohonan_peralatan = PermohonanPeralatan::all();
        // dd($permohonan_peralatan[0]->pekerjaan);
        return view('internal.pekerjaan.pekerjaan_peralatan', compact('permohonan_peralatan'));
    }

    public function createPekerjaanPeralatan($id = 0)
    {
//dd( Session::get('user_permissions'));
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_PEMINJAMAN_PERALATAN)) {
            $pekerjaan = Pekerjaan::all();
            $pekerjaan_dm_approved = array();

            foreach ($pekerjaan as $item) {
                if (Pekerjaan::isLastApproval($item)) {
                    array_push($pekerjaan_dm_approved, $item);
                }
            }

            // dd($pekerjaan_dm_approved);
            $peralatan = Peralatan::getAvailable();
            $permohonan_peralatan = ($id == 0) ? new PermohonanPeralatan() : PermohonanPeralatan::find($id);
            $pekerjaan_peralatan = ($id == 0) ? array() : $permohonan_peralatan->id_pekerjaan_peralatan->toArray();
            if ($id > 0) $peralatan = $peralatan->merge(PermohonanPeralatan::peralatan($permohonan_peralatan));
            $peralatan = $peralatan->sortBy('id');
            return view('internal.pekerjaan.create_pekerjaan_peralatan', compact('pekerjaan_dm_approved', 'pekerjaan', 'peralatan', 'id', 'permohonan_peralatan', 'pekerjaan_peralatan'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function savePekerjaanPeralatan(Request $request)
    {
        $id = $request->id;
        $permohonan_peralatan = ($id == 0) ? new PermohonanPeralatan() : PermohonanPeralatan::findOrFail($id);

        $permohonan_peralatan->nip_pemohon = $request->nip_pemohon;
        $permohonan_peralatan->nama_pemohon = $request->nama_pemohon;
        $permohonan_peralatan->tanggal_pemakaian = $request->tgl_pemakaian;
        $permohonan_peralatan->lokasi_inspeksi = $request->lokasi;
        $permohonan_peralatan->maksud_perjalanan = $request->maksud_perjalanan;
        $permohonan_peralatan->keterangan = $request->keterangan;
        if ($id == 0) {
            $permohonan_peralatan->pekerjaan_id = $request->pekerjaan_id;
            $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_PERMOHONAN_PERALATAN);
            $permohonan_peralatan->id_flow_status = $flow_status->id;
        }

        $permohonan_peralatan->save();

        $ref_status = References::where('nama_reference', 'Kondisi Alat')->first()->id;
        //delete dulu yang lama
        $del_peralatan = $permohonan_peralatan->pekerjaan_peralatan;
        $status_normal = References::getRefByNameAndParent($ref_status, PERALATAN_NORMAL);
        foreach ($del_peralatan as $del) {
            //yang didelete, di set kembali ke normal/available
            $peralatan = Peralatan::find($del->peralatan_id);
            $peralatan->kondisi_alat = $status_normal->id;
            $peralatan->save();
            //add to history_peralatan
            $history = new HistoryPeralatan();
            $history->peralatan_id = $del->peralatan_id;
            $history->pekerjaan_id = $permohonan_peralatan->pekerjaan_id;
            $history->permohonan_peralatan_id = $permohonan_peralatan->id;
            $history->status = $status_normal->id;
            $history->keterangan = "CREATE/EDIT Permohonan Peralatan";
            $history->created_by = Auth::user()->username;
            $history->save();
            $del->delete();
        }

        // dd($permohonan_peralatan->id);
        if (isset($request->peralatan)) {
            $status_booked = References::getRefByNameAndParent($ref_status, PERALATAN_BOOKED);
            foreach ($request->peralatan as $key => $item) {
                //saat create,set status menjadi booked di master dan di pekerjaan peralatan
                $pekerjaan_peralatan = new PekerjaanPeralatan();
                $pekerjaan_peralatan->peralatan_id = $item;
                $pekerjaan_peralatan->permohonan_peralatan_id = $permohonan_peralatan->id;
                $pekerjaan_peralatan->status = $status_booked->id;
                $pekerjaan_peralatan->save();

                $peralatan = Peralatan::find($item);
                $peralatan->kondisi_alat = $status_booked->id;
                $peralatan->save();

                //add to history_peralatan
                $history = new HistoryPeralatan();
                $history->peralatan_id = $item;
                $history->pekerjaan_id = $permohonan_peralatan->pekerjaan_id;
                $history->permohonan_peralatan_id = $permohonan_peralatan->id;
                $history->status = $pekerjaan_peralatan->status;
                $history->keterangan = "CREATE/EDIT Permohonan Peralatan";
                $history->created_by = Auth::user()->username;
                $history->save();
            }
        }
        if ($id == 0) WorkflowController::createFlowHistory($permohonan_peralatan->id, $permohonan_peralatan->getTable(), MODUL_PERMOHONAN_PERALATAN, $flow_status, CREATED);
        return redirect('internal/pekerjaan/peralatan')->with('success', 'Data permohonan peralatan berhasil disimpan');
    }

    public function detailPekerjaanPeralatan($permohonan_peralatan_id)
    {
        $permohonan_peralatan = PermohonanPeralatan::find($permohonan_peralatan_id);
        $peralatan = Peralatan::all();
        $pekerjaan_peralatan = PekerjaanPeralatan::where('permohonan_peralatan_id', $permohonan_peralatan_id)->get();
        $data_workflow = array(
            'model' => MODEL_PERMOHONAN_PERALATAN,
            'modul' => MODUL_PERMOHONAN_PERALATAN,
            'url_notif' => 'detail_pekerjaan_peralatan/',
            'url_redirect' => 'internal/pekerjaan/peralatan'
        );
        $workflow = WorkflowController::workflowJoss($permohonan_peralatan_id, TABLE_PEKERJAAN_PERALATAN, ACTION_WORKFLOW);

        return view('internal.pekerjaan.detail_pekerjaan_peralatan', compact('data_workflow', 'workflow', 'permohonan_peralatan', 'peralatan', 'pekerjaan_peralatan'));
    }

    public function saveLaporanInspeksi(Request $request)
    {
        $laporan = new LaporanPekerjaan();
        // dd($request);
        $laporan->pekerjaan_id = $request->pekerjaan_id;
        $laporan->nomor_laporan = $request->nomor_laporan;
        $laporan->tanggal_surat = $request->tgl_surat;
        // $laporan->file_laporan_inspeksi = ;
        $laporan->tanggal_actual_mulai = $request->tgl_actual_mulai;
        $laporan->tanggal_actual_selesai = $request->tgl_actual_selesai;
        $laporan->pemeriksaan_dokumen = $request->pemeriksaan_dokumen;
        $laporan->pemeriksaan_design = $request->pemeriksaan_design;
        $laporan->pemeriksaan_visual = $request->pemeriksaan_visual;
        $laporan->evaluasi_hasil_komisioning = $request->hasil_komisioning;
        $laporan->pengujian_sistem = $request->pengujian_sistem;
        $laporan->dampak_lingkungan = $request->dampak_lingkungan;
        if ($request->file_laporan_inspeksi != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_laporan_inspeksi-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_laporan_inspeksi->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_laporan_inspeksi->move(storage_path() . '/upload/file_laporan_inspeksi/', $name);
            $laporan->file_laporan_inspeksi = $url_surat;
        }
        $laporan->save();

        $pekerjaan = Pekerjaan::find($request->pekerjaan_id);
        // dd($pekerjaan);
        $pekerjaan->status_pekerjaan = PEKERJAAN_CLOSED;
        $pekerjaan->save();

        return redirect('internal/detail_pekerjaan/' . $request->pekerjaan_id);
    }

    //modul pengembalian peralatan
    public function managePekerjaanPeralatan($id)
    {
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_PENGEMBALIAN_PERALATAN)) {
            $permohonan_peralatan = PermohonanPeralatan::find($id);
            $peralatan = Peralatan::all();
            $pekerjaan_peralatan = PekerjaanPeralatan::where('permohonan_peralatan_id', $id)->get();
            $history_peralatan = HistoryPeralatan::where('permohonan_peralatan_id', $id)->orderBy('created_at', 'desc')->get();
            return view('internal.pekerjaan.manage_pekerjaan_peralatan', compact('history_peralatan', 'permohonan_peralatan', 'peralatan', 'pekerjaan_peralatan'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function updateStatusPeralatan(Request $request)
    {
        if (isset($request->peralatan)) {
            foreach ($request->peralatan as $key => $item) {
                //saat create,set status menjadi booked di master dan di pekerjaan peralatan
                $keterangan = $request->input("keterangan_" . $item);
                $status = $request->input("status_peralatan_" . $item);
                $pekerjaan_peralatan = PekerjaanPeralatan::findOrFail($item);
                $pekerjaan_peralatan->status = $status;
                $pekerjaan_peralatan->keterangan = $keterangan;
                $pekerjaan_peralatan->save();

                $peralatan = Peralatan::find($pekerjaan_peralatan->peralatan_id);
                $peralatan->kondisi_alat = $status;
                $peralatan->save();

                //add to history_peralatan
                $history = new HistoryPeralatan();
                $history->peralatan_id = $pekerjaan_peralatan->peralatan_id;
                $history->pekerjaan_id = $request->pekerjaan_id;
                $history->permohonan_peralatan_id = $pekerjaan_peralatan->permohonan_peralatan_id;
                $history->status = $status;
                $history->keterangan = $keterangan;
                $history->created_by = Auth::user()->username;
                $history->save();
            }
        }
        $id = $request->id;
        $permohonan = PermohonanPeralatan::find($id);
        //file permohonan dan pengembalian
        if ($request->file_permohonan != null) {
            if ($permohonan->file_permohonan != null) {
                $fullPath = storage_path() . '/upload/file_permohonan_peralatan/PERMOHONAN_' . $permohonan->pekerjaan->pekerjaan;
                if (File::exists($fullPath))
                    File::delete($fullPath);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_permohonan_peralatan-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_permohonan->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_permohonan->move(storage_path() . '/upload/file_permohonan_peralatan/', $name);
            $permohonan->file_permohonan = $url_surat;
            $permohonan->save();
        }
        if ($request->file_pengembalian != null) {
            if ($permohonan->file_pengembalian != null) {
                $fullPath = storage_path() . '/upload/file_permohonan_peralatan/PENGEMBALIAN_' . $permohonan->pekerjaan->pekerjaan;
                if (File::exists($fullPath))
                    File::delete($fullPath);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_permohonan_peralatan-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_pengembalian->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_pengembalian->move(storage_path() . '/upload/file_permohonan_peralatan/', $name);
            $permohonan->file_pengembalian = $url_surat;
            $permohonan->save();
        }
        return redirect('internal/pekerjaan/peralatan')->with('success', 'Data peralatan berhasil diperbaharui');
    }

    public function printFormPeralatan($id)
    {
        $permohonan = PermohonanPeralatan::findOrFail($id);
        $peralatan = PermohonanPeralatan::peralatan($permohonan);

        $phpWord = editWord(TEMPLATE_PERMOHONAN_PERALATAN);
        $filename = 'Permohonan Peralatan ' . $permohonan->pekerjaan->pekerjaan . '.docx';
        #set value
        $phpWord->setValue("nama", $permohonan->nama_pemohon);
        $phpWord->setValue("tgl", $permohonan->tanggal_pemakaian);
        $phpWord->setValue("lokasi", $permohonan->lokasi_inspeksi);
        $phpWord->setValue("maksud", $permohonan->maksud_perjalanan);

        $total_peralatan = $peralatan->count();

        $phpWord->cloneRow('no', $total_peralatan);

        $i = 1;
        foreach ($peralatan as $item) {
            $phpWord->setValue("no#" . $i, $i);
            $phpWord->setValue("alat#" . $i, $item->nama_alat);
            $phpWord->setValue("kode#" . $i, $item->tipe_alat);
            $phpWord->setValue("merk#" . $i, $item->merk_alat);
            $phpWord->setValue("seri#" . $i, $item->nomor_seri);
            $i++;
        }

        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }


    public function printFormSuratTugas($id)
    {
        $pekerjaan = Pekerjaan::findOrFail($id);

        $phpWord = editWord(TEMPLATE_SURAT_PERINTAH_TUGAS);
        $filename = 'Surat Perintah tugas ' . $pekerjaan->pekerjaan . '.docx';
        #set value
        $phpWord->setValue("instalasi", $pekerjaan->permohonan->instalasi->nama_instalasi);
        $phpWord->setValue("lokasi", $pekerjaan->permohonan->instalasi->alamat_instalasi);

        $pelaksanaInspeksi = PekerjaanPersonil::getPelaksanaInspeksi($id);
        $tenagaTeknik = PekerjaanPersonil::getTenagateknik($id);
        $tt = ($tenagaTeknik->count() > 0) ? $tenagaTeknik[0] : null;
        $total_pelaksana = $pelaksanaInspeksi->count();

        $phpWord->cloneRow('namapelaksana', $total_pelaksana);

        $i = 1;
        foreach ($pelaksanaInspeksi as $item) {
            $phpWord->setValue("no#" . $i, $i);
            $phpWord->setValue("namapelaksana#" . $i, $item->personil->nama);
            $phpWord->setValue("nipelaksana#" . $i, $item->personil->nip);
            $i++;
        }
        $phpWord->setValue("namaTT", ($tt != null) ? $tt->personil->nama : "");
        $phpWord->setValue("nipTT", ($tt != null) ? $tt->personil->nip : "");

        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    public function saveSuratPenugasan(Request $request)
    {
        $id = $request->id;
        $pekerjaan = Pekerjaan::findOrFail($id);
        $pekerjaan->no_surat_penugasan = $request->nomor_surat;
        $pekerjaan->tgl_surat_penugasan = $request->tgl_surat_penugasan;
        if ($request->file_surat_penugasan != null) {
            if ($pekerjaan->file_surat_penugasan != null) {
                $fullPath = storage_path() . '/upload/surat_penugasan/' . $pekerjaan->file_surat_penugasan;
                if (File::exists($fullPath))
                    File::delete($fullPath);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_penugasan-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_surat_penugasan->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_surat_penugasan->move(storage_path() . '/upload/surat_penugasan/', $name);
            $pekerjaan->file_surat_penugasan = $url_surat;
        }
        $pekerjaan->save();
        return redirect('internal/pekerjaan/')->with('success', 'Data surat penugasan berhasil disimpan.');
    }

}
