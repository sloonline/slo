<?php

namespace App\Http\Controllers;


use App\BusinessArea;
use App\businessAreaUser;
use App\Cities;
use App\Bidang;
use App\CompanyCode;
use App\Jabatan;
use App\Notification;
use App\PemilikInstalasi;
use App\Penugasan;
use App\PenugasanUser;
use App\PesanNotifikasi;
use App\Role;
use App\SubBidang;
use App\StatusNotif;
use App\References;
use App\PerusahaanSpv;

use App\User;
use App\KategoriPerusahaan;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Perusahaan;
use App\Provinces;
use DateTime;
use Illuminate\Support\Facades\File;
use Mail;
use Hash;
use Illuminate\Support\Facades\Session;

class UserInternalController extends Controller
{
    private $user_pj;
    private $perusahaan_pj;

    public function viewUserPemintaJasaPLN()
    {
        $peminta_jasa_pln = User::select('perusahaan.nama_perusahaan', 'perusahaan.nama_pemimpin_perusahaan', 'users.nama_user', 'users.email', 'users.id')
            ->join('perusahaan', 'perusahaan.id', '=', 'users.perusahaan_id')
            ->where('users.is_pln', '=', 1)
            ->orderby('users.created_at', 'desc')
            ->get();
        return view('internal.registrasi.view_user_peminta_jasa_pln', compact('peminta_jasa_pln'));
    }

    public function createUserPemintaJasaPLN()
    {
        $user = null;
        $province = Provinces::all();
        $city = Cities::all();
        $company_code = CompanyCode::all();
        $business_area = BusinessArea::all();
        $user_spv = User::getUserSpv();

//        dd($user_spv);
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        return view('internal.registrasi.create_user_peminta_jasa_pln', compact('user_spv', 'user', 'province', 'city',
            'company_code', 'business_area', 'jenis_ijin_usaha'));
    }

    public function storeUserPemintaJasaPLN(Request $request)
    {
        //cek apakah email sudah dipakai sebelumnya
        $email = User::where('email', $request->email_user)->count();
        //cek apakah username sudah dipakai sebelumnya
        $username = User::where('username', $request->username)->count();
        if ($username == 0) {
            //        if ($email == 0 && $username == 0) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

            $perusahaan = new Perusahaan();
            $perusahaan->nama_pemimpin_perusahaan = $request->nama_pemimpin_perusahaan;
            $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
            $perusahaan->id_province = $request->provinsi;
            $perusahaan->id_city = $request->kabupaten;
            $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
            $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
            $perusahaan->no_fax_perusahaan = $request->no_fax_perusahaan;
            $company = CompanyCode::where('company_code', $request->id_company_code)->first();
            if ($company != null) {
                $perusahaan->id_company_code = $company->id;
                $perusahaan->id_business_area = $request->id_business_area;
                $business_area = BusinessArea::find($request->id_business_area);
                $perusahaan->nama_perusahaan = ($business_area != null) ? $business_area->description : "PLN";
            }

            $nm_p_custom = '-' . $timestamp . '-' . $perusahaan->nama_perusahaan . '.';
            $perusahaan->kategori_perusahaan = 1;
            //save them
            $perusahaan->save();

            $user = new User();
            $user->nama_user = $request->nama_user;
            $user->username = $request->username;
            $user->email = $request->email_user;
            //            $user->password = $request->password;
            $pass = $this->getRandomWord();
            $passencrypt = Hash::make($pass);
            $user->password = $passencrypt;
            $user->tempat_lahir_user = $request->tempat_lahir;
            $user->tanggal_lahir = $request->tanggal_lahir;//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
            $user->gender_user = $request->gender_user;
            $user->alamat_user = $request->alamat_user;
            $user->no_ktp_user = $request->no_ktp_user;
            $user->no_hp_user = $request->no_hp_user;
            $user->status_user = "VERIFIED";
            $user->jenis_user = "PLN";
            $user->no_registrasi = $this->makeIDRegisterExternal();
            $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';
            $user->file_npwp_user = ($request->file_npwp_user != null) ? 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension() : "";
            $user->file_ktp_user = ($request->file_ktp_user != null) ? 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension() : "";
            $user->file_foto_user = ($request->file_foto_user != null) ? 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension() : "";
            /*------------------------------------------------------------------------------------------------*/
            //Save File
            $user->perusahaan_id = $perusahaan->id;
            $user->is_pln = "1";
            $user->save();

            $perusahaan->user_id = $user->id;
            $perusahaan->save();

            //set user role
            $user_role = new UserRole();
            $user_role->user_id = $user->id;
            $user_role->role_id = 1;
            $user_role->save();


            #add mapping supervisor dengan perusahaan
            $mp_spv = new businessAreaUser();
            $mp_spv->user_id = $request->user_spv;
            $mp_spv->business_area_id = $user->perusahaan_id;
            $mp_spv->save();

            /*---------------------------------------move user file upload-------------------------------------*/
            if ($request->file_npwp_user != null) $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
            if ($request->file_ktp_user != null) $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
            /*------------------------------------------------------------------------------------------------*/

            /*-----------------move perusahaan file upload---------*/
            //            if ($request->file_npwp_perusahaan != null) $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension());
            //            if ($request->file_surat_ijin_usaha != null) $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension());
            //            if ($request->file_skdp != null) $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension());
            //            if ($request->file_situ != null) $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension());
            //            if ($request->file_tdp_perusahaan != null) $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension());
            /*-----------------------------------------------------*/

            //set jenis ijin usaha
//save jenis usaha / kepemilikan
            $pemilik = new PemilikInstalasi();
            //dd($pemilik);
            // $pemilik->id = PemilikInstalasi::orderBy('id', 'desc')->first()->id+1;
            $pemilik->nama_pemilik = $perusahaan->nama_perusahaan;
            $pemilik->alamat_pemilik = $perusahaan->alamat_perusahaan;
            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu)->format('Y-m-d H:i:s');
            $pemilik->nama_kontrak = $request->nama_kontrak;
            $pemilik->no_kontrak = $request->no_kontrak;
            $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak)->format('Y-m-d H:i:s');
            $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d H:i:s');
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl)->format('Y-m-d H:i:s');
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl)->format('Y-m-d H:i:s');
            $pemilik->perusahaan_id = $perusahaan->id;
            $pemilik->id_province = $request->provinsi;
            $pemilik->id_city = $request->kabupaten;

            $pemilik->save();

            /*-----------------------------file upload pemilik instalasi------------------------------------*/
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->hasFile('file_kontrak_sewa')) {
                $pemilik->file_kontrak = 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension());
            }

            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_spjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $user->alamat_perusahaan = $perusahaan->alamat_perusahaan;
            $user->nama_perusahaan = $perusahaan->nama_perusahaan;
            $user->registerID = $user->no_registrasi;
            $user->password = $pass;

            //kirim email bahwa user sudah bikinin

            $mail_data = array(
                "data" => $user,
                "data_alias" => "user",
                "to" => $user->email,
                "to_name" => $user->nama_user,
                "subject" => "NEW USER CREATED",
                "description" => "REGISTER USER PLN"
            );
            sendEmail($mail_data, 'emails.create_user', 'CREATE', 'REGISTER');
            //            Mail::send('emails.create_user', ['user' => $user], function ($m) use ($user) {
            //                $m->from('pusertif@pln.co.id', 'PLN Pusertif');
            //                $m->to($user->email, $user->nama_user)->subject('NEW USER CREATED');
            //            });

            Session::flash('success', 'Registrasi user berhasil dilakukan!');
        } else if ($email != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Username ' . $request->username . ' sudah digunakan sebelumnya.');
        }
        return redirect('internal/users');
    }

    public function showUserPemintaJasaPLN($id)
    {
        $user = User::join('perusahaan', 'perusahaan.id', '=', 'users.perusahaan_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'perusahaan.id_province')
            ->leftjoin('cities', 'cities.id', '=', 'perusahaan.id_city')
            ->findOrFail($id);
        return view('internal.registrasi.detail_user_peminta_jasa_pln', compact('user', 'id'));
    }

    public function editUserPemintaJasaPLN($id)
    {
        $user = User::findOrFail($id);
        $perusahaan = $user->perusahaan;
        $province = Provinces::where('id', '!=', 1)->get();
        $city = Cities::where('id', '!=', 2)->get();
        $company_code = CompanyCode::all();
        $business_area = BusinessArea::all();
        $user_spv = User::getUserSpv();
        $spv = ($id == 0) ? null : BusinessAreaUser::where('business_area_id', $user->perusahaan_id)->get();
        if ($spv != null) $spv = $spv->first();

        $pemilik = PemilikInstalasi::where('perusahaan_id', @$user->perusahaan_id)->first();

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        return view('internal.registrasi.edit_user_peminta_jasa_pln', compact('spv', 'user_spv', 'user', 'province', 'city',
            'id', 'company_code', 'business_area', 'perusahaan', 'jenis_ijin_usaha', 'pemilik'));
    }

    public function updateUserPemintaJasaPLN(Request $request, $id)
    {
        //cek apakah email sudah dipakai sebelumnya
        $email = User::where('email', $request->email_user)->where('id', '!=', $id)->count();
        //cek apakah username sudah dipakai sebelumnya
        $username = User::where('username', $request->username)->where('id', '!=', $id)->count();
        if ($email == 0 && $username == 0) {
            $user = User::findOrFail($id);
            $perusahaan = Perusahaan::findOrFail($user->perusahaan_id);
            $perusahaan->nama_pemimpin_perusahaan = $request->nama_pemimpin_perusahaan;
            $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
            $perusahaan->id_province = $request->provinsi;
            $perusahaan->id_city = $request->kabupaten;
            $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
            $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
            $perusahaan->no_fax_perusahaan = $request->no_fax_perusahaan;
            $perusahaan->nama_perusahaan = $request->nama_perusahaan;
            $company = CompanyCode::where('company_code', $request->id_company_code)->first();
            if ($company != null) {
                $perusahaan->id_company_code = $company->id;
                $perusahaan->id_business_area = $request->id_business_area;
//                $business_area = BusinessArea::find($request->id_business_area);
//                $perusahaan->nama_perusahaan = ($business_area != null) ? $business_area->description : "PLN";
            }

            $user->nama_user = $request->nama_user;
            $user->username = $request->username;
            $user->email = $request->email_user;
            $user->tempat_lahir_user = $request->tempat_lahir;
            $user->tanggal_lahir = $request->tanggal_lahir;//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
            $user->gender_user = $request->gender_user;
            $user->alamat_user = $request->alamat_user;
            $user->no_ktp_user = $request->no_ktp_user;
            $user->no_hp_user = $request->no_hp_user;

            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $nm_p_custom = '-' . $timestamp . '-' . $perusahaan->nama_perusahaan . '.';
            $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';

            /*--------------------------------------upload file upload user-----------------------------------*/
            if ($request->hasFile('file_npwp_user')) {
                $filename = $user->file_npwp_user;
                $fullPath = storage_path() . '/upload/npwp_user/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $user->file_npwp_user = 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension();
                $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
            }

            if ($request->hasFile('file_ktp_user')) {
                $filename = $user->file_ktp_user;
                $fullPath = storage_path() . '/upload/ktp_user/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $user->file_ktp_user = 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension();
                $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
            }

            if ($request->hasFile('file_foto_user')) {
                $filename = $user->file_foto_user;
                $fullPath = storage_path() . '/upload/foto_user/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $user->file_foto_user = 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension();
                $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension());
            }
            /*------------------------------------------------------------------------------------------------*/

            $perusahaan->save();
            $user->save();

            //save jenis usaha / kepemilikan
            $pemilik = $perusahaan->pemilikInstalasi;

            if ($pemilik == null)
                $pemilik = new PemilikInstalasi();

            //dd($pemilik);
            // $pemilik->id = PemilikInstalasi::orderBy('id', 'desc')->first()->id+1;
            $pemilik->nama_pemilik = $perusahaan->nama_perusahaan;
            $pemilik->alamat_pemilik = $perusahaan->alamat_perusahaan;
            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu)->format('Y-m-d H:i:s');
            $pemilik->nama_kontrak = $request->nama_kontrak;
            $pemilik->no_kontrak = $request->no_kontrak;
            $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak)->format('Y-m-d H:i:s');
            $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d H:i:s');
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl)->format('Y-m-d H:i:s');
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl)->format('Y-m-d H:i:s');
            $pemilik->perusahaan_id = $perusahaan->id;
            $pemilik->id_province = $request->provinsi;
            $pemilik->id_city = $request->kabupaten;

            $pemilik->save();

            /*-----------------------------file upload pemilik instalasi------------------------------------*/
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->hasFile('file_kontrak_sewa')) {
                $pemilik->file_kontrak = 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension());
            }

            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath)) File::delete($fullPath);
                $request->file_spjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            #delete mapping supervisor dengan perusahaan
            businessAreaUser::where('business_area_id', $user->perusahaan_id)->delete();
            #add mapping supervisor dengan perusahaan
            $mp_spv = new businessAreaUser();
            $mp_spv->user_id = $request->user_spv;
            $mp_spv->business_area_id = $user->perusahaan_id;
            $mp_spv->save();
            Session::flash('success', 'User Berhasil Diupdate!');
        } else if ($email != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Email ' . $request->email . ' sudah digunakan sebelumnya.');
        } else if ($username != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Username ' . $request->username . ' sudah digunakan sebelumnya.');
        }
        return redirect('internal/users');
    }

    public function destroyUserPemintaJasaPLN($id)
    {
        $user = User::find($id);
        if ($user != null) {
            $perusahaan = Perusahaan::find($user->perusahaan_id);

            if ($perusahaan != null) {


                /*-------------------------------------hapus file upload perusahaan--------------------------------*/
                $loc = storage_path() . '/upload/npwp_perusahaan/' . $perusahaan->file_npwp_perusahaan;
                if (file_exists($loc)) File::delete($loc);
                $loc = storage_path() . '/upload/surat_ijin_usaha/' . $perusahaan->file_surat_ijin_usaha;
                if (file_exists($loc)) File::delete($loc);
                $loc = storage_path() . '/upload/surat_keterangan_domisili_perusahaan/' . $perusahaan->file_skdp;
                if (file_exists($loc)) File::delete($loc);
                $loc = storage_path() . '/upload/tanda_daftar_perusahaan/' . $perusahaan->file_tdp_perusahaan;
                if (file_exists($loc)) File::delete($loc);
                /*------------------------------------------------------------------------------------------------*/
                /*-------------------------------------hapus file upload user-------------------------------------*/
                $loc = storage_path() . '/upload/npwp_user/' . $perusahaan->file_npwp_user;
                if (file_exists($loc)) File::delete($loc);
                $loc = storage_path() . '/upload/ktp_user/' . $perusahaan->file_ktp_user;
                if (file_exists($loc)) File::delete($loc);
                $loc = storage_path() . '/upload/foto_user/' . $perusahaan->file_foto_user;
                if (file_exists($loc)) File::delete($loc);
                /*------------------------------------------------------------------------------------------------*/

                //replace username jadi deleted
                $user->username = $user->username."_deleted_".$user->id;
                $user->email = $user->email."_deleted_".$user->id;
                $user->save();
                $user = User::findOrFail($id);
                $user->delete();
                $perusahaan->delete();
            }
        }
        return redirect('internal/users')->with('success', 'User Berhasil Dihapus!');
    }

    //START PAGE MANAJEMEN INTERNAL======================================================================
    public function index_internal()
    {
        $users = User::where('perusahaan_id', null)->get();
        return view('internal.registrasi.user_reg_internal', compact('users'));

    }

    //page registrasi internal jaser
    public function registerInternal($id = 0)
    {
        $bidang = Bidang::where('isAktif', 1)->get();
        $sub_bidang = SubBidang::where('isAktif', 1)->get();
        $roles = Role::orderBy('id', 'asc')->get();
        $user = ($id != 0) ? User::findOrFail($id) : null;
        $user_roles = ($id != 0) ? UserRole::where('user_id', $id)->get() : array();
        if ($id != 0) {
            $user->tanggal_lahir = $user->tanggal_lahir;//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
        }
        return view('internal.registrasi.register_internal', compact('bidang', 'sub_bidang',
            'id', 'user', 'roles', 'user_roles'));
    }

    public function storeInternal(Request $request)
    {
        //        dd($request);
        $id = $request->id;
        //cek apakah edit atau insert
        if ($id == null || $id == 0) {
            $valid = $this->insertInternal($request);
        } else {
            $valid = $this->updateInternal($request, $id);
        }
        return ($valid == false) ? redirect()->back()->withInput() : redirect('internal/users');
    }

    public function insertInternal(Request $request)
    {
        //cek apakah email sudah dipakai sebelumnya
//        $email = User::where('email', $request->email)->count();
        //cek apakah username sudah dipakai sebelumnya
        $username = User::where('username', $request->username)->count();
//        if ($email == 0 && $username == 0) {
        if ($username == 0) {
            $user = new User();
            $user->email = $request->email;
            $user->nama_user = $request->nama_user;
            $user->tempat_lahir_user = $request->tempat_lahir_user;
            $user->gender_user = $request->gender_user;
            $user->tanggal_lahir = $request->tanggal_lahir;//date('y-M-d', strtotime($request->tanggal_lahir));
            $user->alamat_user = $request->alamat_user;
            $user->no_hp_user = $request->no_hp_user;
            $user->nip_user = $request->nip_user;
            $user->grade_user = $request->grade_user;
            $user->jabatan_user = $request->jabatan_user;
            $user->pendidikan_user = $request->pendidikan_user;
            $user->status_pekerja = $request->status_pekerja;
            $user->status_pernikahan = $request->status_pernikahan;
            $user->id_bidang = $request->id_bidang;
            $user->id_sub_bidang = $request->id_sub_bidang;
            $user->password = "";
            $user->username = $request->username;
            $user->status_user = "VERIFIED";
            $user->jenis_user = "INTERNAL";
            $file = $request->file_foto_user;
            if ($file != null) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'foto_user-' . $timestamp . '-' . $request->email . "." . $file->getClientOriginalExtension();
                $user->file_foto_user = $name;
                $file->move(storage_path() . '/upload/foto_user/', $name);
            }
            $user->save();
            //assign user role
            foreach ($request->roles as $role) {
                $user_role = new UserRole();
                $user_role->user_id = $user->id;
                $user_role->role_id = $role;
                $user_role->save();
            }
            $mail_data = array(
                "data" => $user,
                "data_alias" => "user",
                "to" => $user->email,
                "to_name" => $user->nama_user,
                "subject" => "NEW USER CREATED",
                "description" => "REGISTER USER PUSERTIF"
            );
            sendEmail($mail_data, 'emails.create_user_internal', 'CREATE', 'REGISTER');
            Session::flash('success', 'Registrasi user berhasil dilakukan!');
            return true;
//        } else if ($email != 0) {
//            Session::flash('error', 'Registrasi user gagal dilakukan. Email ' . $request->email . ' sudah digunakan sebelumnya.');
        } else if ($username != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Username ' . $request->username . ' sudah digunakan sebelumnya.');
            return false;
        }
    }

    public function updateInternal(Request $request, $id)
    {
        //cek apakah username sudah dipakai sebelumnya
        $username = User::where('username', $request->username)->where('id', '<>', $id)->count();
        if ($username == 0) {
            //update user
            $user = User::findOrFail($id);
            $user->tempat_lahir_user = $request->tempat_lahir_user;
            $user->gender_user = $request->gender_user;
            // $user->tanggal_lahir = Carbon::parse($request->tanggal_lahir)->format('d-M-Y');
            $user->tanggal_lahir = $request->tanggal_lahir;//date('y-M-d', strtotime($request->tanggal_lahir));
            $user->alamat_user = $request->alamat_user;
            $user->no_hp_user = $request->no_hp_user;
            $user->nip_user = $request->nip_user;
            $user->grade_user = $request->grade_user;
            $user->pendidikan_user = $request->pendidikan_user;
            $user->jabatan_user = $request->jabatan_user;
            $user->status_pekerja = $request->status_pekerja;
            $user->status_pernikahan = $request->status_pernikahan;
            $user->id_bidang = $request->id_bidang;
            $user->id_sub_bidang = $request->id_sub_bidang;
            $user->username = $request->username;
            $file = $request->file_foto_user;
            if ($file != null) {
                if ($user->file_foto_user != null) {
                    $loc = storage_path() . '/upload/foto_user/' . $user->file_foto_user;
                    if (file_exists($loc)) unlink($loc);
                }
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'foto_user-' . $timestamp . '-' . $request->email . "." . $file->getClientOriginalExtension();
                $user->file_foto_user = $name;
                $file->move(storage_path() . '/upload/foto_user/', $name);
            }
            $user->save();
            //delete role lama
            UserRole::where('user_id', $id)->delete();
            //insert yg baru
            foreach ($request->roles as $role) {
                $user_role = new UserRole();
                $user_role->user_id = $id;
                $user_role->role_id = $role;
                $user_role->save();
            }
            Session::flash('success', 'Data user berhasil diperbaharui!');
            return true;
        } else {
            Session::flash('error', 'Registrasi user gagal dilakukan. Username ' . $request->username . ' sudah digunakan sebelumnya.');
            return false;
        }
    }

    public function deleteInternal($id)
    {
        $user = User::findOrFail($id);
        //replace username jadi deleted
        $user->username = $user->username."_deleted_".$user->id;
        $user->email = $user->email."_deleted_".$user->id;
        $user->save();
        $user = User::findOrFail($id);
        $user->delete();
        Session::flash('success', 'Data user berhasil dihapus.');
        return redirect('internal/users');
    }


    public function deleteEksternal($id)
    {
        $user = User::findOrFail($id);
        //replace username jadi deleted
        $user->username = $user->username."_deleted_".$user->id;
        $user->email = $user->email."_deleted_".$user->id;
        $user->save();
        $user = User::findOrFail($id);
        $user->delete();
        Session::flash('success', 'Data user berhasil dihapus.');
        return redirect('internal/users');
    }

    //END PAGE MANAJEMEN INTERNAL======================================================================

    //page approval External
    public function viewApprovalExternal()
    {

        $user = User::select('users.id', 'users.created_at', 'perusahaan.nama_perusahaan', 'users.nama_user', 'users.email', 'status_notif.status')
            ->join('perusahaan', 'perusahaan.id', '=', 'users.perusahaan_id')
            ->leftjoin('status_notif', 'status_notif.user_id', '=', 'users.id')
            ->where('status_notif.status', '=', null)
            ->orwhere('status_notif.status', '=', '0')
            ->orderby('users.created_at', 'desc')
            ->get();
        return view('internal.registrasi.app_reg_peminta', compact('user'));

    }

    public function showApprovalExternal($id)
    {
        //$this->sentMail($id);
        $IDReg = $this->makeIDRegisterExternal();
//        $kategori_perusahaan = KategoriPerusahaan::where('isAktif', 1)->get();
//        $ijin_perusahaan = References::getRef('Jenis Ijin Usaha Pemilik Instalasi');
        $user = User::findOrFail($id);
        $user_spv = User::getUserSpv();
        $kategori = KategoriPerusahaan::where('id', '!=', 1)->get();
        $pemilik = PemilikInstalasi::where('perusahaan_id', $user->perusahaan_id)->first();
        return view('internal.registrasi.det_reg_peminta', compact('pemilik', 'kategori', 'user_spv', 'user', 'IDReg'));
    }


    protected function makeIDRegisterExternal()
    {
        $jumlahuser = User::where('perusahaan_id', '<>', '')->count();
        $userterakhir = User::where('perusahaan_id', '<>', '')->orderby('no_registrasi', 'desc')->first();
        $angka = $jumlahuser + 1;
        if ($jumlahuser == 0) {
            $angkaterakhir = '0000';
        } else {
            $angkaterakhir = substr($userterakhir->no_registrasi, 4, 4);
        }

        if ($angka <= 9999) {
            if ($angka < 10) {
                $idpel = '000' . $angka;
            } else if ($angka < 100) {
                $idpel = '00' . $angka;
            } else if ($angka < 1000) {
                $idpel = '0' . $angka;
            } else {
                $idpel = $angka;
            }
        } else if ($angka == 10000) {
            $idpel = 'A000';
        } else {
            $a = substr($angkaterakhir, 0, 1);
            $b = substr($angkaterakhir, 1, 3) + 1;

            if ($b > 999) {
                $a++;
            }

            if ($b < 10) {
                $b = '00' . $b;
            } else if ($b < 100) {
                $b = '0' . $b;
            } else if ($b <= 999) {
                $b = $b;
            } else {
                $b = '000';
            }
            $idpel = strtoupper($a) . $b;
        }


        $now = new DateTime();
        $now = substr($now->format('Y'), 2, 2);

        return "0000" . $idpel . $now;
    }

    protected function getRandomWord($len = 10)
    {
        $word = array_merge(range('a', 'z'), range('A', 'Z'), range('1', '9'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }

    protected function sentMail($id)
    {

    }

    public function storeApprovalExternal(Request $request)
    {
        $countstatus = StatusNotif::where('user_id', '=', $request->id)->count();

        if ($request->nama_perusahaan) {
            $this->updateExternal($request, $request->id);
        }

        $user = User::findOrFail($request->id);
        #add mapping supervisor dengan perusahaan
        // $mp_spv = new businessAreaUser();
        // $mp_spv->user_id = $request->user_spv;
        // $mp_spv->business_area_id = $user->perusahaan_id;
        // $mp_spv->save();
        #insert to PerusahaanSpv instead of businessAreaUser
        #14 Agustus 2017
        $mp_spv = new PerusahaanSpv();
        $mp_spv->user_id = $request->user_spv;
        $mp_spv->perusahaan_id = $user->perusahaan_id;
        $mp_spv->save();

        /*    echo dd($countstatus);*/
        if ($countstatus == 0) {
            Perusahaan::where('id', '=', $user->perusahaan_id)
                ->update(['kategori_perusahaan' => $request->kategori, 'is_pln' => 0]);

            $userstatus = new StatusNotif();
            $userstatus->user_id = $request->id;
            $userstatus->status = 1;
            $userstatus->save();

        } else {
            Perusahaan::where('id', '=', $user->perusahaan_id)
                ->update(['kategori_perusahaan' => $request->kategori, 'is_pln' => 0]);
            StatusNotif::where('user_id', '=', $request->id)
                ->update(['status' => 1]);
        }
        $pass = $this->getRandomWord();
        $passencrypt = Hash::make($pass);
        $registerID = $this->makeIDRegisterExternal();
        User::where('id', '=', $request->id)
            ->update(['password' => $passencrypt, 'nip_user' => $registerID, 'status_user' => "VERIFIED"]);
        //add role peminta jasa to user
        $user_role = new UserRole();
        $user_role->user_id = $user->id;
        $user_role->role_id = 1;
        $user_role->save();

        $usr = $user->toArray();
        $usr['nama_perusahaan'] = $user->perusahaan->nama_perusahaan;
        $usr['alamat_perusahaan'] = $user->perusahaan->alamat_perusahaan;
        $usr['kategori_perusahaan'] = KategoriPerusahaan::findOrFail($request->kategori)->nama_kategori;
        $usr['registerID'] = $registerID;
        $usr['password'] = $pass;

        $mail_data = array(
            "data" => $usr,
            "data_alias" => "user",
            "to" => $user->email,
            "to_name" => $user->nama_user,
            "subject" => "USER APPROVED",
            "description" => "USER APPROVED"
        );
        sendEmail($mail_data, 'emails.aktivasi_user', 'APPROVED', 'REGISTER');
        return redirect('internal/users')->with('success', 'Approve user berhasil dilakukan. ');
    }

    public function storePendingExternal(Request $request)
    {

        $id = $request->id;
        $keterangan = $request->keterangan_pending;
        $status_notif_id = 0;
        $status_notif = StatusNotif::where('user_id', '=', $id)->get();
        if (sizeof($status_notif) != 0) {
            //dd($status_notif);
            $a = $request->keterangan_pending;
            StatusNotif::where('user_id', '=', $id)
                ->update(['status' => 2, 'deskripsi' => "PENDING"]);
            //dd($status_notif[0]->id);
            $status_notif_id = $status_notif[0]->id;
        } else {
            $userstatus = new StatusNotif();
            $userstatus->user_id = $id;
            $userstatus->status = 2;
            $userstatus->deskripsi = $request->keterangan_pending;
            $userstatus->save();
            $status_notif_id = $userstatus->id;
        }

        if ($status_notif_id != 0) {
            $pesan_notifikasi = new PesanNotifikasi();
            $pesan_notifikasi->status_notif_id = $status_notif_id;
            $pesan_notifikasi->keterangan = $request->keterangan_pending;
            $pesan_notifikasi->save();
        }
        $user = User::findOrFail($id);
        $user->status_user = "PENDING";
        $user->keterangan_pending = $keterangan;
        $user->save();

        $perusahaan = Perusahaan::find($user->perusahaan_id);
        $user->nama_perusahaan = @$perusahaan->nama_perusahaan;
        $user->alamat_perusahaan = @$perusahaan->alamat_perusahaan;
        /*$tolak = StatusNotif::where("user_id", "=", $id)->get();
        $tolak->email = $user->email;
        $tolak->nama_user = $user->nama_user;
        $tolak->keterangan_pending = $keterangan;*/

        //dd($user->email);

        $mail_data = array(
            "data" => $user,
            "data_alias" => "user",
            "to" => $user->email,
            "to_name" => $user->nama_user,
            "subject" => "Status verifikasi user",
            "description" => "USER PENDING"
        );
        sendEmail($mail_data, 'emails.aktivasi_user_tolak', 'PENDING', 'REGISTER');
        //        Mail::send('emails.aktivasi_user_tolak', ['user' => $user], function ($m) use ($user) {
        //            $m->from('pusertif@pln.co.id', 'PLN Pusertif');
        //            $m->to($user->email, $user->nama_user)->subject('Status verifikasi user');
        //        });
        return redirect('/internal/users')->with('success', 'Verifikasi user berhasil dinyatakan pending.');
    }

    public function showUsers()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_VIEW_USER)) {
            $users = User::orderBy('id', 'DESC')->get();
            $user_internal = array();
            $user_pln = array();
            $user_eksternal = array();
            foreach ($users as $row) {
                if ($row->is_pln == 1) {
                    if ($row->perusahaan_id == null) {
                        array_push($user_internal, $row);
                    } else {
                        array_push($user_pln, $row);
                    }
                } else {
                    array_push($user_eksternal, $row);
                }
            }
            return view('internal.registrasi.users', compact('user_internal', 'user_pln', 'user_eksternal'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function detailUser($id)
    {
        $user = User::findOrFail($id);
        $province = Provinces::all();
        $city = Cities::all();
        if ($user->perusahaan_id != null) {
            $kategori = KategoriPerusahaan::find($user->perusahaan->kategori_perusahaan);
            if ($kategori != null) $kategori = $kategori->nama_kategori;
            else $kategori = null;
        } else {
            $roles = Role::orderBy('id', 'asc')->get();
            $user_roles = ($id != null) ? UserRole::where('user_id', $id)->get() : array();
            $kategori = null;
        }

        return view('internal.registrasi.detail_user', compact('user', 'province', 'city', 'id', 'kategori', 'roles', 'user_roles'));
    }

    public function registerEksternalByAdmin($id = 0)

    {
        $user = ($id == 0) ? null : User::findOrFail($id);
        $province = Provinces::all();
        $city = Cities::all();
        $kategori = KategoriPerusahaan::where('id', '!=', 1)->get();
        $user_spv = User::getUserSpv();
        $spv = ($id == 0) ? null : User::getSpv($user);
        $pemilik = PemilikInstalasi::where('perusahaan_id', @$user->perusahaan_id)->first();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        //      dd($user->perusahaan->id_province);

        // dd($user);
        return view('internal.registrasi.input_user_eksternal', compact('jenis_ijin_usaha', 'pemilik', 'user', 'province', 'city', 'id', 'kategori', 'user_spv', 'spv'));
    }

    public function editEksternalByAdmin($id = 0)
    {
        $user = ($id == 0) ? null : User::find($id);
        $province = Provinces::all();
        $city = Cities::all();
        $kategori = KategoriPerusahaan::where('id', '!=', 1)->get();
        $user_spv = User::getUserSpv();
        $spv = ($id == 0) ? null : BusinessAreaUser::where('business_area_id', $user->perusahaan_id)->get();
        //      dd($user->perusahaan->id_province);

        //dd($user->perusahaan);
        return view('internal.registrasi.edit_user_eksternal', compact('user', 'province', 'city', 'id', 'kategori', 'spv'));
    }

    public function storeEksternalByAdmin(Request $request)
    {
        $id = $request->id;
        if ($id == 0) {
            $user = $this->insertExternal($request);
            unset($user->registerID);
            //kasih passwordnya langusng karena dibuat oleh admin
            $perusahaan = $user->perusahaan;
            $perusahaan->kategori_perusahaan = $request->kategori;
            $perusahaan->save();
            $pass = $this->getRandomWord();
            $passencrypt = Hash::make($pass);
            $user = user::findOrFail($user->id);
            $user->password = $passencrypt;
            $user->status_user = "VERIFIED";
            $user->save();
            #add mapping supervisor dengan perusahaan
            if ($perusahaan->id_business_area != null) {
                $mp_spv = new businessAreaUser();
                $mp_spv->user_id = $request->user_spv;
                $mp_spv->business_area_id = $user->perusahaan_id;
                $mp_spv->save();
            } else {
                $mp_spv = new PerusahaanSpv();
                $mp_spv->user_id = $request->user_spv;
                $mp_spv->perusahaan_id = $user->perusahaan_id;
                $mp_spv->save();
            }
            $registerID = $this->makeIDRegisterExternal();
            User::where('id', '=', $request->id)
                ->update(['password' => $passencrypt, 'nip_user' => $registerID]);

            //set user role
            $user_role = new UserRole();
            $user_role->user_id = $user->id;
            $user_role->role_id = 1;
            $user_role->save();

            $user['registerID'] = $registerID;
            $user['password'] = $pass;
            $user['nama_perusahaan'] = $user->perusahaan->nama_perusahaan;
            $user['alamat_perusahaan'] = $user->perusahaan->alamat_perusahaan;
            $user['kategori_perusahaan'] = KategoriPerusahaan::findOrFail($request->kategori)->nama_kategori;
		
            $mail_data = array(
                "data" => $user,
                "data_alias" => "user",
                "to" => $user->email,
                "to_name" => $user->nama_user,
                "subject" => "NEW USER APPROVED",
                "description" => "CREATE USER"
            );
            sendEmail($mail_data, 'emails.aktivasi_user', 'NEW USER APPROVED', 'CREATE USER');
	    //            Mail::send('emails.aktivasi_user', ['user' => $user], function ($m) use ($user) {
            //                $m->from('pusertif@pln.co.id', 'PLN Pusertif');
            //
            //                $m->to($user->email, $user->nama_user)->subject('NEW USER APPROVED');
            //            });
        } else {
            if (isset($request->approved)) {
                if ($request->approved == "approved") {
                    $this->storeApprovalExternal($request);
                    Session::flash('success', 'Data user berhasil disetujui.');
                } else {
                    //nanti direject

                }
            } else {
                $this->updateExternal($request, $id);
                Session::flash('success', 'Data user berhasil disimpan.');
            }
        }

        return redirect('/internal/users');
    }

    public function insertExternal(Request $request)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        $user = new User();
        $user->email = $request->email_user;
        //$user->username = $request->username;
        $user->username = $request->email_user;
        $user->nama_user = strtoupper($request->nama_user);
        //$user->password = $request->password;
        $user->tempat_lahir_user = strtoupper($request->tempat_lahir);
        $user->gender_user = strtoupper($request->gender_user);
        $user->tanggal_lahir = $request->tanggal_lahir;//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
        $user->alamat_user = $request->alamat_user;
        $user->no_ktp_user = $request->no_ktp_user;
        $user->no_hp_user = $request->no_hp_user;
        $user->is_pln = "0";
        $user->jenis_user = "EKSTERNAL";
        $user->status_user = "CREATED";
        /*--------------khusus user external-------------------------------------------------------------*/

        $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';
        $user->file_npwp_user = ($request->hasFile('file_npwp_user')) ? 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension() : "";
        $user->file_ktp_user = ($request->hasFile('file_ktp_user')) ? 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension() : "";
        $user->file_foto_user = ($request->hasFile('file_foto_user')) ? 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension() : "";
        /*------------------------------------------------------------------------------------------------*/

        //dd($user->tanggal_lahir );
        $user->save();
        //simpan user di var local
        $this->user_pj = $user;

        $perusahaan = new Perusahaan();
        $perusahaan->nama_perusahaan = strtoupper($request->nama_perusahaan);
        $perusahaan->nama_pemimpin_perusahaan = strtoupper($request->nama_pemimpin_perusahaan);
        $perusahaan->no_npwp_perusahaan = $request->no_npwp_perusahaan;
        $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
        $perusahaan->id_province = $request->provinsi;
        $perusahaan->id_city = $request->kabupaten;
        $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
        $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
        $perusahaan->no_fax_perusahaan = $request->no_fax_perusahaan;
        $perusahaan->email_perusahaan = ($request->email_perusahaan != null) ? $request->email_perusahaan : "";


        $nm_p_custom = '-' . $timestamp . '-' . $request->email_perusahaan . '.';
        $perusahaan->file_npwp_perusahaan = ($request->hasFile('file_npwp_perusahaan')) ? 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension() : "";
        $perusahaan->file_siup_perusahaan = ($request->hasFile('file_surat_ijin_usaha')) ? 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension() : "";
        $perusahaan->file_skdomisili_perusahaan = ($request->hasFile('file_skdp')) ? 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension() : "";
        $perusahaan->file_situ_perusahaan = ($request->hasFile('file_situ')) ? 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension() : "";
        $perusahaan->file_tdp_perusahaan = ($request->hasFile('file_tdp_perusahaan')) ? 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension() : "";
        //dd($perusahaan);
        $perusahaan->save();
        //simpan perusahaan di var local
        $this->perusahaan_pj = $perusahaan;
        //        $user->perusahaan()->save($perusahaan);
        $user = User::findOrFail($user->id);
        $user->perusahaan_id = $perusahaan->id;
        $user->save();
        /*$perusahaan->save();
        $user->perusahaan_id = $perusahaan->id;
        $user->save();*/

        /*----move user file upload*/
        if ($request->hasFile('file_npwp_user')) {
            $filename = $user->file_npwp_user;
            $fullPath = storage_path() . '/upload/npwp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_ktp_user')) {
            $filename = $user->file_ktp_user;
            $fullPath = storage_path() . '/upload/ktp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_foto_user')) {
            $filename = $user->file_foto_user;
            $fullPath = storage_path() . '/upload/foto_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension());
        }
        /* $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user_' . $user->email . '.' . $request->file_npwp_perusahaan->getClientOriginalExtension());
        $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user_' . $user->email . '.' . $request->file_ktp_user->getClientOriginalExtension());
        $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user_' . $user->email . '.' . $request->file_foto_user->getClientOriginalExtension());*/
        /*--------------------*/

        /*----move perusahaan file upload*/
        if ($request->hasFile('file_npwp_perusahaan')) {
            $filename = $perusahaan->file_npwp_perusahaan;
            $fullPath = storage_path() . '/upload/npwp_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension());
        }

        if ($request->hasFile('file_surat_ijin_usaha')) {
            $filename = $perusahaan->file_siup_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension());
        }

        if ($request->hasFile('file_skdp')) {
            $filename = $perusahaan->file_skdp;
            $fullPath = storage_path() . '/upload/surat_keterangan_domisili_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension());
        }

        if ($request->hasFile('file_situ')) {
            $filename = $perusahaan->file_situ_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_tempat_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension());
        }

        if ($request->hasFile('file_tdp_perusahaan')) {
            $filename = $user->file_tdp_perusahaan;
            $fullPath = storage_path() . '/upload/tanda_daftar_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension());
        }
        /*
        $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_npwp_perusahaan->getClientOriginalExtension());
        $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha_' . $perusahaan->email_perusahaan . '.' . $request->file_surat_ijin_usaha->getClientOriginalExtension());
        $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_skdp->getClientOriginalExtension());
        $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha_' . $perusahaan->email_perusahaan . '.' . $request->file_situ->getClientOriginalExtension());
        $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_tdp_perusahaan->getClientOriginalExtension());*/
        /*-------------------------------*/

        //set jenis ijin usaha
//save jenis usaha / kepemilikan
        $pemilik = new PemilikInstalasi();
        //dd($pemilik);
        // $pemilik->id = PemilikInstalasi::orderBy('id', 'desc')->first()->id+1;
        $pemilik->nama_pemilik = $perusahaan->nama_perusahaan;
        $pemilik->alamat_pemilik = $perusahaan->alamat_perusahaan;
        $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
        $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
        $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
        $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu)->format('Y-m-d H:i:s');
        $pemilik->nama_kontrak = $request->nama_kontrak;
        $pemilik->no_kontrak = $request->no_kontrak;
        $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d H:i:s');
        $pemilik->no_spjbtl = $request->no_spjbtl;
        $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->perusahaan_id = $perusahaan->id;
        $pemilik->id_province = $request->provinsi;
        $pemilik->id_city = $request->kabupaten;

        $pemilik->save();

        /*-----------------------------file upload pemilik instalasi------------------------------------*/
        if ($request->hasFile('file_surat_iu')) {
            $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension());
        }

        if ($request->hasFile('file_kontrak_sewa')) {
            $pemilik->file_kontrak = 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension());
        }

        if ($request->hasFile('file_spjbtl')) {
            $pemilik->file_spjbtl = 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_spjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension());
        }

        $pemilik->save();

        //notify to user email
        $user->nama_perusahaan = $perusahaan->nama_perusahaan;
        $user->alamat_perusahaan = $perusahaan->alamat_perusahaan;
        $mail_user = $user;
        $mail_user->registerID = $this->makeIDRegisterExternal();
        //call send email helper
        $mail_data = array(
            "data" => $user,
            "data_alias" => "user",
            "to" => $mail_user->email,
            "to_name" => $mail_user->nama_user,
            "subject" => "NEW USER CREATED",
            "description" => "REGISTER USER EKSTERNAL"
        );
        sendEmail($mail_data, 'emails.create_user', 'CREATE', 'REGISTER');
        //        Mail::send('emails.create_user', ['user' => $user], function ($m) use ($mail_user) {
        //            $m->from('pusertif@pln.co.id', 'PLN Pusertif');
        //            $m->to($mail_user->email, $mail_user->nama_user)->subject('NEW USER CREATED');
        //        });
        return $user;
    }

    public function updateExternal(Request $request, $id)
    {
        $user = User::findOrFail($id);
        #delete mapping supervisor dengan perusahaan
        businessAreaUser::where('business_area_id', $user->perusahaan_id)->delete();

        $perusahaan = Perusahaan::findOrFail($user->perusahaan_id);
        $perusahaan->nama_perusahaan = $request->nama_perusahaan;
        $perusahaan->nama_pemimpin_perusahaan = $request->nama_pemimpin_perusahaan;
        $perusahaan->no_npwp_perusahaan = $request->no_npwp_perusahaan;
        $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
        $perusahaan->id_province = $request->provinsi;
        $perusahaan->id_city = $request->kabupaten;
        $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
        $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
        $perusahaan->email_perusahaan = $request->email_perusahaan;
        $perusahaan->kategori_perusahaan = $request->kategori;
        $user->nama_user = $request->nama_user;
        $user->username = $request->username;
        $user->email = $request->email_user;
        $user->tempat_lahir_user = $request->tempat_lahir;
        $user->tanggal_lahir = $request->tanggal_lahir;//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
        $user->gender_user = $request->gender_user;
        $user->alamat_user = $request->alamat_user;
        $user->no_ktp_user = $request->no_ktp_user;
        $user->no_hp_user = $request->no_hp_user;
        $user->is_pln = "0";

        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $nm_p_custom = '-' . $timestamp . '-' . $request->email_perusahaan . '.';
        $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';

        /*--------------------------------------update file upload perusahaan-----------------------------*/
        if ($request->hasFile('file_npwp_perusahaan')) {
            $filename = $perusahaan->file_npwp_perusahaan;
            $fullPath = storage_path() . '/upload/npwp_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $perusahaan->file_npwp_perusahaan = 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension();
            $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension());
        }

        if ($request->hasFile('file_surat_ijin_usaha')) {
            $filename = $perusahaan->file_siup_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $perusahaan->file_siup_perusahaan = 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension();
            $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension());
        }

        if ($request->hasFile('file_skdp')) {
            $filename = $perusahaan->file_skdp;
            $fullPath = storage_path() . '/upload/surat_keterangan_domisili_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $perusahaan->file_skdp = 'file_skdp' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension();
            $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension());
        }

        if ($request->hasFile('file_situ')) {
            $filename = $perusahaan->file_situ_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_tempat_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $perusahaan->file_situ_perusahaan = 'file_situ' . $nm_p_custom . $request->file_situ->getClientOriginalExtension();
            $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension());
        }

        if ($request->hasFile('file_tdp_perusahaan')) {
            $filename = $user->file_tdp_perusahaan;
            $fullPath = storage_path() . '/upload/tanda_daftar_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $perusahaan->file_tdp_perusahaan = 'file_tdp_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension();
            $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension());
        }
        /*------------------------------------------------------------------------------------------------*/
        /*--------------------------------------upload file upload user-----------------------------------*/
        if ($request->hasFile('file_npwp_user')) {
            $filename = $user->file_npwp_user;
            $fullPath = storage_path() . '/upload/npwp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $user->file_npwp_user = 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension();
            $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_ktp_user')) {
            $filename = $user->file_ktp_user;
            $fullPath = storage_path() . '/upload/ktp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $user->file_ktp_user = 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension();
            $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_foto_user')) {
            $filename = $user->file_foto_user;
            $fullPath = storage_path() . '/upload/foto_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $user->file_foto_user = 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension();
            $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension());
        }
        /*------------------------------------------------------------------------------------------------*/

        $perusahaan->save();
        $user->save();

        //save jenis usaha / kepemilikan
        $pemilik = PemilikInstalasi::where('perusahaan_id', @$user->perusahaan_id)->first();
        //dd($pemilik);
        // $pemilik->id = PemilikInstalasi::orderBy('id', 'desc')->first()->id+1;
        $pemilik->nama_pemilik = $perusahaan->nama_perusahaan;
        $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
        $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
        $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
        $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu)->format('Y-m-d H:i:s');
        $pemilik->nama_kontrak = $request->nama_kontrak;
        $pemilik->no_kontrak = $request->no_kontrak;
        $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d H:i:s');
        $pemilik->no_spjbtl = $request->no_spjbtl;
        $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->perusahaan_id = $perusahaan->id;
        $pemilik->id_province = $request->provinsi;
        $pemilik->id_city = $request->kabupaten;

        $pemilik->save();

        /*-----------------------------file upload pemilik instalasi------------------------------------*/
        if ($request->hasFile('file_surat_iu')) {
            $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension());
        }

        if ($request->hasFile('file_kontrak_sewa')) {
            $pemilik->file_kontrak = 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension());
        }

        if ($request->hasFile('file_spjbtl')) {
            $pemilik->file_spjbtl = 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_spjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension());
        }

        $pemilik->save();
        #add mapping supervisor dengan perusahaan
//        $mp_spv = new businessAreaUser();
//        $mp_spv->user_id = $request->user_spv;
//        $mp_spv->business_area_id = $user->perusahaan_id;
//        $mp_spv->save();
        $spv = User::getSpv($user);
        $spv = ($spv == null) ? null : $spv->first();
        if ($perusahaan->id_business_area != null) {
            $mp_spv = ($spv != null) ? businessAreaUser::where('business_area_id', $user->perusahaan_id)->first() : new businessAreaUser();
            $mp_spv->user_id = $request->user_spv;
            $mp_spv->business_area_id = $user->perusahaan_id;
            $mp_spv->save();
        } else {
            $mp_spv = ($spv != null) ? PerusahaanSpv::where('perusahaan_id', $user->perusahaan_id)->first() : new PerusahaanSpv();
            $mp_spv->user_id = $request->user_spv;
            $mp_spv->perusahaan_id = $user->perusahaan_id;
            $mp_spv->save();
        }
        //        return $user;
        return true;
        //        Session::flash('message', 'Data user berhasil disimpan.');
        //        return redirect('/internal/users');
    }

    public function resetPassword($id = null){
        if($id != null){
            $user = User::findOrFail($id);
            $pass = $this->getRandomWord();
            $passencrypt = Hash::make($pass);

            User::where('id', '=', $id)
            ->update(['password' => $passencrypt]);

            $usr = $user->toArray();
            $usr['username'] = $user->username;
            $usr['password'] = $pass;

            $mail_data = array(
                "data" => $usr,
                "data_alias" => "user",
                "to" => $user->email,
                "to_name" => $user->nama_user,
                "subject" => "RESET PASSWORD",
                "description" => "RESET PASSWORD"
            );

            sendEmail($mail_data, 'emails.reset_password', 'RESET', 'MANAGEMENT USER');
            return redirect('internal/users')->with('success','Reset password berhasil dilakukan. ');
        }else{
            return redirect('internal/users');
        }
    }
}
