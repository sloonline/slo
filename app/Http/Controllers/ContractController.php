<?php

namespace App\Http\Controllers;

use App\JadwalPelaksanaan;
use App\Kontrak;
use App\KontrakVersion;
use App\KontrakVersionRAB;
use App\Notification;
use App\Order;
use App\Pembayaran;
use App\RAB;
use App\RABPermohonan;
use App\Tracking;
use App\User;
use App\UserRole;
use App\Workflow;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{
    public function showKontrak()
    {
        $kontrak = Kontrak::orderBy('updated_at', 'desc')->get();
        for ($i = 0; $i < sizeof($kontrak); $i++) {
            $kontrak[$i]->nama_instalasi = Kontrak::getNamaInstalasiByKontrak($kontrak[$i]);
        }
        return view('internal.kontrak.kontrak', compact('kontrak'));
    }

    public function createKontrakByOrder($order_id)
    {
        return redirect('internal/create_kontrak')->with('order_id', $order_id);
    }

    public function createKontrak($kontrak_id = 0)
    {
        $data = $this->getDetailKontrak($kontrak_id);
        $kontrak = $data["kontrak"];
        $latest = $data["latest"];
        $allRab = $data["allRab"];
        $kontrakVersion = ($kontrak_id == 0) ? array() : $kontrak->kontrak_version;
        $used_user = null;

        $rab_permohonan = RABPermohonan::all();
        $id_user_rab = array();

        foreach ($rab_permohonan as $item) {
            array_push($id_user_rab, $item->permohonan->order->user->id);

        }
        // dd($allRab);
        foreach ($allRab as $item) {
            if ($item['val'] > 0) {
                $used_user = $item['rab']->order->user;
            }
        }
        // dd($id_user_rab);
        $peminta_jasa = User::whereIn('id', $id_user_rab)->get();

        //cek, jika ternyata dilempar dari order maka lakukan set rab dan user yang terpilih
        $order = (session('order_id')) ? Order::find(session('order_id')) : null;
        return view('internal.kontrak.create_form_kontrak', compact('used_user', 'peminta_jasa', 'kontrak_id', 'kontrak', 'latest', 'allRab', 'kontrakVersion', 'order'));
    }

    public function detailKontrak($kontrak_id)
    {
        $data = $this->getDetailKontrak($kontrak_id);
        $kontrak = $data["kontrak"];
        $latest = $data["latest"];
        $allRab = $data["allRab"];
        $pembayaran = null;
        $sisa_termin = 0;
        $sisa_persen = 0;
        $kontrakVersion = ($kontrak_id == 0) ? array() : $kontrak->kontrak_version;

        $needPayment = false;
        if (@$kontrak->perusahaan->kategori->nama_kategori != PLN) {
            $needPayment = true;
        }

        $latest_wf = WorkflowController::getLatestHistory($kontrak_id, TABLE_KONTRAK);
        $next_step = WorkflowController::getAllNextStep($latest_wf->workflow_id);
//        dd(sizeof($next_step));
        $is_last_approval = false;
        if (sizeof($next_step) <= 0) {
            $is_last_approval = true;
            $pembayaran = Pembayaran::where('id_kontrak', $kontrak_id)->get();
            $total_termin = Pembayaran::select(DB::raw('sum(nilai) as nilai, sum(persentase) as persentase'))->where('id_kontrak', $kontrak_id)->first();
            $sisa_termin = $latest->nilai_kontrak - $total_termin->nilai;
            $sisa_persen = 100 - $total_termin->persentase;
            $termin_ke = sizeof($pembayaran) + 1;
        } else {
            foreach ($next_step as $item) {
                if ($item->tipe_status == RECEIVED) {
                    $is_last_approval = true;
                    $pembayaran = Pembayaran::where('id_kontrak', $kontrak_id)->get();
                    $total_termin = Pembayaran::select(DB::raw('sum(nilai) as nilai, sum(persentase) as persentase'))->where('id_kontrak', $kontrak_id)->first();
                    $sisa_termin = $latest->nilai_kontrak - $total_termin->nilai;
                    $sisa_persen = 100 - $total_termin->persentase;
                    $termin_ke = sizeof($pembayaran) + 1;
                }
            }
        }
        if (sizeof($kontrakVersion) == 1) {
            $kontrakVersion = array();
        }

//        dd($kontrak->jadwal);
        $data_workflow = array(
            'model' => MODEL_KONTRAK,
            'modul' => MODUL_KONTRAK,
            'url_notif' => 'detail_kontrak/',
            'url_redirect' => 'internal/kontrak/'
        );
        $workflow = WorkflowController::workflowJoss($kontrak_id, TABLE_KONTRAK, ACTION_WORKFLOW);

        return view('internal.kontrak.detail_kontrak', compact('needPayment', 'termin_ke', 'sisa_termin', 'pembayaran', 'is_last_approval', 'workflow', 'kontrak_id',
            'kontrak', 'latest', 'allRab', 'kontrakVersion', 'data_workflow', 'sisa_persen'));
    }

    public function detailAmandemen($kontrak_version_id)
    {
        $kontrak = kontrakVersion::find($kontrak_version_id);

        $used_rab = $kontrak->kontrak_version_rab;
        $rab_kontrak = KontrakVersionRAB::select('id_rab')->where('id_kontrak_version', $kontrak_version_id)->get()->toArray();
        $allRab = array();
        $rab = RAB::whereNotIn('id', $rab_kontrak)->get();
        foreach ($used_rab as $item) {
            array_push($allRab, array(
                "rab" => $item->rab,
                "val" => $item->id
            ));
        }
        foreach ($rab as $item) {
            array_push($allRab, array(
                "rab" => $item,
                "val" => 0
            ));
        }

        return view('internal.kontrak.detail_amandemen_kontrak', compact('kontrak_version_id', 'kontrak', 'allRab'));
    }


    public function storeKontrak(Request $request)
    {
        $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_KONTRAK);
        $pilih_rab = $request->pilih_rab;
        $kontrak_id = $request->id;
        $kontrak = ($kontrak_id == 0) ? new Kontrak() : Kontrak::findOrFail($kontrak_id);
        if ($kontrak_id == 0) {
            $kontrak->id_flow_status = $flow_status->id;
        }
        $kontrak->perusahaan_id = User::findOrFail($request->user)->perusahaan->id;
        $kontrak->save();

        $this->saveKontrakVersion($pilih_rab, $kontrak, $request);

        //jika membuat kontrak baru maka jalankan workflow submit
        if ($kontrak_id == 0) {
            WorkflowController::createFlowHistory($kontrak->id, $kontrak->getTable(), MODUL_KONTRAK, $flow_status, CREATED);
//            foreach (User::getUserListFromRole('SPV-BPU') as $user_role) {
//                $notif = new Notification();
//                $notif->to = $user_role->username;
//                $notif->from = Auth::user()->username;
//                $notif->status = "UNREAD";
//                $notif->icon = "icon-briefcase";
//                $notif->url = "internal/detail_kontrak/" . $kontrak->id;
//                $notif->subject = "NEED APPROVAL KONTRAK";
//                $notif->message = "Mohon approval kontrak nomor: " . $request->nomor_kontrak;
//                $notif->color = "btn-green";
//                $notif->save();
//            }
        }

        #create tracking
        for ($i = 0; $i < sizeof($pilih_rab); $i++) {
            $tracking = Tracking::where('rab_id', $pilih_rab[$i])->get();
            if ($tracking != null) {
                foreach ($tracking as $item) {
                    $item->kontrak_id = @$kontrak->id;
                    $item->status = STATUS_KONTRAK;
                    $item->save();
                }
            }
        }
        #---------------

        return redirect('/internal/kontrak')->with('success', 'Data Kontrak berhasil disimpan.');
    }


    public function createAmandemen($kontrak_id)
    {
        $data = $this->getDetailKontrak($kontrak_id);
        $allRab = $data["allRab"];
        return view('internal.kontrak.create_form_amandemen ', compact('kontrak_id', 'allRab'));
    }


    public function storeAmandemen(Request $request)
    {
        $kontrak_id = $request->kontrak_id;
        $kontrak = Kontrak::find($kontrak_id);
        $pilih_rab = $request->pilih_rab;
        $this->saveKontrakVersion($pilih_rab, $kontrak, $request);
        return redirect('/internal/create_kontrak/' . $kontrak_id)->with('success', 'Data Kontrak berhasil disimpan.');
    }

    public function getDetailKontrak($kontrak_id)
    {
        //get kontrak
        $kontrak = ($kontrak_id == 0) ? null : Kontrak::find($kontrak_id);
        //get latest kontrak
        $latest = ($kontrak_id == 0) ? null : $kontrak->latest_kontrak;
        //get used rab
        $used_rab = ($kontrak_id == 0) ? array() : $latest->kontrak_version_rab;
        //get all latest version
        $versions = Kontrak::select('id_kontrak_version')->get()->toArray();
        //get unused rab
        $rab_kontrak = KontrakVersionRAB::select('id_rab')->whereIn('id_kontrak_version', $versions)->get()->toArray();
        //collect unused and used rab to array
        $allRab = array();
        $rab = RAB::whereNotIn('id', $rab_kontrak)->get();
//        dd($rab[0]->order->flow_status->id);
        foreach ($used_rab as $item) {
            $isApproved = ($kontrak_id == 0) ? WorkflowController::getCurrentWorkflow($item->order->flow_status->id, MODUL_RAB)->next : '';
            if ($isApproved == null) {
                array_push($allRab, array(
                    "rab" => $item->rab,
                    "val" => $item->id
                ));
            }
        }
        foreach ($rab as $item) {
            // karena Flow status masih di Order, sedangkan di sini modulnya RAB
            if (WorkflowController::getCurrentWorkflow(@$item->order->flow_status->id, MODUL_RAB) == null) continue;
            $isApproved = WorkflowController::getCurrentWorkflow(@$item->order->flow_status->id, MODUL_RAB)->next;
            if ($isApproved == null) {
                array_push($allRab, array(
                    "rab" => $item,
                    "val" => 0
                ));
            }
        }

        return array("kontrak" => $kontrak, "latest" => $latest, "allRab" => $allRab);
    }


    public function saveKontrakVersion($pilih_rab, $kontrak, Request $request)
    {
        //save kontrak version
        $version_id = $request->version_id;
        $kontrak_version = ($version_id > 0) ? KontrakVersion::findOrFail($version_id) : new KontrakVersion();
        $kontrak_version->id_kontrak = $kontrak->id;
        //hanya berlaku untuk kontrak baru
        if (!$version_id > 0) {
            if ($kontrak != null && $kontrak->latest_kontrak != null) {
                $version = $kontrak->latest_kontrak->version + 1;
                $kontrak_version->version = (string)$version;
            }
        }
        $kontrak_version->nomor_kontrak = $request->nomor_kontrak;
        $kontrak_version->tgl_kontrak = Carbon::parse($request->tanggal_kontrak)->format('Y-m-d');
        $kontrak_version->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d');
        $kontrak_version->uraian = $request->deskripsi_kontrak;
        $kontrak_version->nilai_kontrak = str_replace(',', '', $request->nilai_kontrak);
        $url_surat = "";

        if ($request->file_kontrak != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'perjanjian_kontrak-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_kontrak->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_kontrak->move(storage_path() . '/upload/perjanjian_kontrak/', $name);
            $kontrak_version->file = $url_surat;
        }

        /*=======UNTUK AMANDEMEN, INSERT ALASAN========*/
        if (isset($request->file_amandemen)) {
            if ($request->file_amandemen != null) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'file_amandemen-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_kontrak->getClientOriginalExtension();
                $url_surat = $name;
                $request->file_amandemen->move(storage_path() . '/upload/file_amandemen/', $name);
                $kontrak_version->file_amandemen = $url_surat;
            }
        }

        if (isset($request->keterangan_amandemen)) {
            $kontrak_version->alasan_amandemen = $request->keterangan_amandemen;
        }
        $kontrak_version->save();

        /*================END AMANDEMEN=================*/
        if ($version_id > 0) {
            KontrakVersionRAB::where('id_kontrak_version', $version_id)->delete();
        }
        //save rab
        for ($i = 0; $i < sizeof($pilih_rab); $i++) {
            $kontrak_version_rab = new KontrakVersionRAB();
            $kontrak_version_rab->id_kontrak_version = $kontrak_version->id;
            $kontrak_version_rab->id_rab = $pilih_rab[$i];
            $kontrak_version_rab->save();
        }
        //update latest kontrak
        $kontrak->id_kontrak_version = $kontrak_version->id;
        $kontrak->save();
    }

    public function approvalContract(Request $request)
    {
//        dd($request);
        $kontrak_id = $request->id;
        $status = $request->status;
        $keterangan = $request->keterangan;

        $kontrak = Kontrak::findOrFail($request->id);
        $flow_status = WorkflowController::getFlowStatusId($status, MODUL_KONTRAK);

        $kontrak->id_flow_status = ($flow_status != null) ? $flow_status->id : null;
        $kontrak->save();

        #notifikasi
        $current_wf = WorkflowController::getCurrentWorkflow($flow_status->id, MODUL_KONTRAK);
        $users = WorkflowController::getAllNextUsers($current_wf->id);

        if ($flow_status->tipe_status == SENT || $flow_status->tipe_status == SUBMITTED) {
            $keterangan = SUBMITTED;
        }

        #isi data notif
        #create object notif
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->user->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $kontrak_id;
                $notif->subject = "KONTRAK " . $flow_status->tipe_status;
                $notif->to = $item->user->username;
                $notif->status = "UNREAD";
                $notif->message = "KONTRAK " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        WorkflowController::createFlowHistory($kontrak_id, $kontrak->getTable(), MODUL_KONTRAK, $flow_status, $keterangan);
        return redirect('internal/kontrak');

    }

    public function savePembayaran(Request $request)
    {
        $kontrak_id = $request->kontrak_id;
        $pembayaran = new Pembayaran();
        $pembayaran->termin_ke = $request->termin;
        $pembayaran->id_kontrak = $request->kontrak_id;
        $pembayaran->persentase = $request->persentase_termin;
        $pembayaran->nilai = $request->nilai_termin;
        $pembayaran->status = $request->status;
        $pembayaran->keterangan = $request->keterangan;
        $pembayaran->save();

        //$users = Auth::user()->getUserListFromRole('P-BPU');
        $users = User::getAllUserByPermission(PERMISSION_CREATE_KONTRAK);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $kontrak_id;
                $notif->subject = "TERMIN KONTRAK CREATED";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "TERMIN KONTRAK dibuat oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('internal/detail_kontrak/' . $kontrak_id);
    }

    public function changeStatusPembayaran($pembayaran_id, $status)
    {
        $pembayaran = Pembayaran::find($pembayaran_id);
//        dd($pembayaran_id );
        $pembayaran->status = $status;
        $pembayaran->save();
        return $pembayaran;
    }

    public function invoicePembayaran($id)
    {
        $pembayaran_id = $id;
        $pembayaran = $this->changeStatusPembayaran($pembayaran_id, INVOICE_SENT);

        $kontrak = KontrakVersion::where('id_kontrak', $pembayaran->id_kontrak)->first();
        $kontrak_id = $kontrak->id_kontrak;
        $username = @$kontrak->kontrak_version_rab[0]->rab->order->created_by;
        $users = User::where('username', $username)->get();

        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = $item->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $kontrak_id;
                $notif->subject = "INVOICE TERMIN SENT";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "INVOICE TERMIN dikirim oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('internal/detail_kontrak/' . $pembayaran->id_kontrak);
    }

    public function paidPembayaran($id)
    {
        $pembayaran_id = $id;
        $pembayaran = $this->changeStatusPembayaran($pembayaran_id, PAID);
        $kontrak_id = $pembayaran->id_kontrak;

//        $users = Auth::user()->getUserListFromRole('P-BPU');
        $users = User::getAllUserByPermission(PERMISSION_CREATE_KONTRAK);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $kontrak_id;
                $notif->subject = "TERMIN PAID";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "STATUS TERMIN PAID oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('internal/detail_kontrak/' . $pembayaran->id_kontrak)->with('success', 'Status pembayaran PAID.');
    }

    public function saveBuktiPembayaran(Request $request)
    {
        $url_bukti = "";
        $url_bukti_pph = "";
        if ($request->bukti != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'bukti_pembayaran-' . $timestamp . '-' . Auth::user()->email . "." . $request->bukti->getClientOriginalExtension();
            $url_bukti = $name;
            $request->bukti->move(storage_path() . '/upload/bukti_pembayaran/', $name);
        }
        if ($request->bukti_pph != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name_pph = 'bukti_pph-' . $timestamp . '-' . Auth::user()->email . "." . $request->bukti->getClientOriginalExtension();
            $url_bukti_pph = $name_pph;
            $request->bukti_pph->move(storage_path() . '/upload/bukti_pph/', $name_pph);
        }
        $pembayaran = $this->changeStatusPembayaran($request->pembayaran_id, BUKTI_UPLOADED);
        $pembayaran->bukti = $url_bukti;
        $pembayaran->bukti_pph = $url_bukti_pph;
        $pembayaran->save();

//        $users = Auth::user()->getUserListFromRole('P-BPU');
        $users = User::getAllUserByPermission(PERMISSION_CREATE_KONTRAK);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $request->kontrak_id;
                $notif->subject = "BUKTI PEMBAYARAN UPLOADED";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "BUKTI PEMBAYARAN diupload oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        $users = User::getAllUserByPermission(PERMISSION_CREATE_INVOICE);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $request->kontrak_id;
                $notif->subject = "BUKTI PEMBAYARAN UPLOADED";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "BUKTI PEMBAYARAN diupload oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('eksternal/detail_kontrak/' . $pembayaran->id_kontrak)->with('success', 'Bukti pembayaran berhasil disimpan.');
    }

    public function eksternalDetailKontrak($kontrak_id)
    {
        $data = $this->getDetailKontrak($kontrak_id);
        $kontrak = $data["kontrak"];
        $latest = $data["latest"];
        $allRab = $data["allRab"];
        $pembayaran = null;
        $kontrakVersion = ($kontrak_id == 0) ? array() : $kontrak->kontrak_version;

        $needPayment = false;
        if ($kontrak->perusahaan->kategori->nama_kategori != PLN) {
            $needPayment = true;
        }

        $latest_wf = WorkflowController::getLatestHistory($kontrak_id, TABLE_KONTRAK);
        $next_step = WorkflowController::getAllNextStep($latest_wf->workflow_id);

        $is_last_approval = false;
        if (sizeof($next_step) <= 0) {
            $is_last_approval = true;
            $pembayaran = Pembayaran::where('id_kontrak', $kontrak_id)->get();
        } else {
            foreach ($next_step as $item) {
                if ($item->tipe_status == RECEIVED) {
                    $is_last_approval = true;
                    $pembayaran = Pembayaran::where('id_kontrak', $kontrak_id)->get();
                }
            }
        }
        if (sizeof($kontrakVersion) == 1) {
            $kontrakVersion = array();
        }

        $data_workflow = array(
            'model' => MODEL_KONTRAK,
            'modul' => MODUL_KONTRAK,
            'url_notif' => 'detail_kontrak/',
            'url_redirect' => 'eksternal/kontrak/'
        );
        $workflow = WorkflowController::workflowJoss($kontrak_id, TABLE_KONTRAK, ACTION_INTERNAL_KONTRAK,TIPE_EKSTERNAL);
        return view('eksternal.kontrak.detail_kontrak', compact('needPayment', 'data_workflow', 'pembayaran', 'is_last_approval', 'workflow', 'kontrak_id', 'kontrak', 'latest', 'allRab', 'kontrakVersion'));
    }

    public function eksternalShowKontrak()
    {
        $user = Auth::user();
        $kontrak_all = Kontrak::where('perusahaan_id', Auth::user()->perusahaan_id)->orderBy('updated_at', 'desc')->get();
//        dd(Kontrak::getUser($kontrak_all[0])->id);
        $kontrak = array();
        foreach ($kontrak_all as $item) {
            if ($item->flow_status->tipe_status == SENT) {
                if (Kontrak::getUser($item)->id == $user->id) {
                    $item->nama_instalasi = Kontrak::getNamaInstalasiByKontrak($item);
                    array_push($kontrak, $item);
                }
            }
        }
        return view('eksternal.kontrak.kontrak', compact('kontrak'));
    }

    public function eksternalDetailAmandemen($kontrak_version_id)
    {
        $kontrak = kontrakVersion::find($kontrak_version_id);
        $used_rab = $kontrak->kontrak_version_rab;
        $rab_kontrak = KontrakVersionRAB::select('id_rab')->where('id_kontrak_version', $kontrak_version_id)->get()->toArray();
        $allRab = array();
        $rab = RAB::whereNotIn('id', $rab_kontrak)->get();
        foreach ($used_rab as $item) {
            array_push($allRab, array(
                "rab" => $item->rab,
                "val" => $item->id
            ));
        }
        foreach ($rab as $item) {
            array_push($allRab, array(
                "rab" => $item,
                "val" => 0
            ));
        }
        return view('eksternal.kontrak.detail_amandemen_kontrak', compact('kontrak_version_id', 'kontrak', 'allRab'));
    }

    public function eksternalInvoicePembayaran($id)
    {
        $pembayaran_id = $id;
        $pembayaran = Pembayaran::find($pembayaran_id);
        $kontrak = KontrakVersion::where('id_kontrak', $pembayaran->id_kontrak)->first();
        $kontrak_id = $kontrak->id_kontrak;
        $username = $kontrak->kontrak_version_rab[0]->rab->order->created_by;
        $order = $kontrak->kontrak_version_rab[0]->rab->order;
        $user = User::where('username', $username)->first();

//        dd($kontrak);
        return view('eksternal.kontrak.invoice', compact('kontrak', 'kontrak_id', 'pembayaran_id', 'pembayaran', 'user', 'order'));
//        return redirect('eksternal/detail_kontrak/' . $pembayaran->id_kontrak);
    }

    public function saveJadwalPelaksanaan(Request $request)
    {
//        dd($request);
        $jadwal_pelaksanaan = new JadwalPelaksanaan();
        $jadwal_pelaksanaan->tanggal_mulai = $request->tanggal_mulai;
        $jadwal_pelaksanaan->tanggal_selesai = $request->tanggal_selesai;
        $jadwal_pelaksanaan->keterangan = $request->keterangan;
        $jadwal_pelaksanaan->kontrak_id = $request->kontrak_id;

        $kontrak = Kontrak::findOrFail($request->kontrak_id);
        $kontrak->status = SCHEDULED;
        $kontrak->save();

        if ($request->file_jadwal != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_jadwal_pelaksanaan-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_jadwal->getClientOriginalExtension();
            $url = $name;
            $request->file_jadwal->move(storage_path() . '/upload/file_jadwal_pelaksanaan/', $name);
            $jadwal_pelaksanaan->file_jadwal = $url;
        }
        $jadwal_pelaksanaan->save();

//        $users = User::getUserListFromRole('P-BPU');
        $users = User::getAllUserByPermission(PERMISSION_CREATE_KONTRAK);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $request->kontrak_id;
                $notif->subject = "JADWAL PEKERJAAN";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "Jadwal pekerjaan telah disimpan oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        #START SEND REQUEST NO AGENDA TO DJK
//        $instalasi = Kontrak::getInstalasi($kontrak);
//        if($instalasi != null) {
//            foreach ($instalasi as $item) {
//                $bay_gardu_id = null;
//                if($item->jenis_instalasi->id == KODE_GARDU_INDUK_TRANSMISI){
//                    $bay_gardu = $instalasi->BayGardu;
//                    foreach ($bay_gardu as $bay){
//                        if($bay->id == $instalasi->getPermohonan)
//                    }
//                }
//                $param = [
//                    'tipe_instalasi' => $item->jenis_instalasi->tipeInstalasi->id,
//                    'instalasi' => $item,
//                    'bay_gardu_id' => @$bay_transmisi->id,
//                    'waktu_pelaksanaan' => $waktu_pelaksanaan
//                ];
//                #request nomor agenda ke djk
//                $response = requestNomorAgenda($param);
//                #END SEND REQUEST NO AGENDA TO DJK
//            }
//        }

        $tipe_user = (Auth::user()->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
        return redirect(strtolower($tipe_user) . '/detail_kontrak/' . $request->kontrak_id)->with('success', 'Jadwal pekerjaan berhasil disimpan');
    }

    public function requestInvoice($id)
    {
        $pembayaran = Pembayaran::findOrFail($id);
        $pembayaran->status = INVOICE_REQUESTED;
        $pembayaran->save();

        $users = User::getAllUserByPermission(PERMISSION_CREATE_INVOICE);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $pembayaran->kontrak->id;
                $notif->subject = "REQUEST INVOICE";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "Permintaan pembuatan invoice dari <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('internal/detail_kontrak/' . $pembayaran->kontrak->id)->with('success', 'Permintaan invoice berhasil dikirim ke Pengendali Keuagan.');
    }

    public function storeInvoice(Request $request)
    {
//        dd($request);
        $pembayaran = Pembayaran::findOrFail($request->pembayaran_id_invoice);
        $pembayaran->status = INVOICE_CREATED;
        $pembayaran->nomor_surat = $request->nomor_surat;
        $pembayaran->tanggal_surat = Carbon::parse($request->tanggal_surat);
        $pembayaran->jabatan = $request->jabatan;
        $pembayaran->nama_pejabat = $request->nama_pejabat;
        if ($request->file_invoice != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_invoice-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_invoice->getClientOriginalExtension();
            $url = $name;
            $request->file_invoice->move(storage_path() . '/upload/file_invoice/', $name);

            $pembayaran->file_invoice = $url;
        }

        $pembayaran->save();

//        $users = User::getUserListFromRole('P-BPU');
        $users = User::getAllUserByPermission(PERMISSION_CREATE_KONTRAK);
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $pembayaran->kontrak->id;
                $notif->subject = "INVOICE CREATED";
                $notif->to = $item->username;
                $notif->status = "UNREAD";
                $notif->message = "Invoice telah diupload oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        return redirect('internal/detail_kontrak/' . $pembayaran->kontrak->id)->with('success', 'Invoice berhasil disimpan.');
    }

    public function sendInvoice($id)
    {
        $pembayaran = Pembayaran::findOrFail($id);
        $pembayaran->status = INVOICE_SENT;
        $pembayaran->save();

//        $kontrak = $pembayaran->kontrak;

        $user = Kontrak::getUser($pembayaran->kontrak);

//        $users = User::getUserListFromRole('PMJ');
//        if (isset($users)) {
//            foreach ($users as $item) {
        $jenis_user = TIPE_EKSTERNAL;
        $notif = new Notification();
        $notif->from = Auth::user()->username;
        $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $pembayaran->kontrak->id;
        $notif->subject = "INVOICE";
        $notif->to = $user->username;
        $notif->status = "UNREAD";
        $notif->message = "Pemberitahuan invoice dari <br/><b>" . Auth::user()->nama_user . "</b>";
        $notif->icon = "icon-briefcase";
        $notif->color = "btn-green";
        $notif->save();
//            }
//        }

        return redirect('internal/detail_kontrak/' . $pembayaran->kontrak->id)->with('success', 'Invoice berhasil dikirim ke ' . $user->nama_user . '.');
    }

    public function storeKuitansi(Request $request)
    {
//        dd($request);
        $pembayaran = Pembayaran::findOrFail($request->pembayaran_id_kuitansi);
        $pembayaran->status = KUITANSI_CREATED;

        $pembayaran->nomor_kuitansi_transfer = $request->nomor_kuitansi_transfer;
        $pembayaran->tanggal_kuitansi_transfer = Carbon::parse($request->tanggal_kuitansi_transfer);
        if ($request->file_kuitansi_transfer != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_kuitansi_transfer-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_kuitansi_transfer->getClientOriginalExtension();
            $url = $name;
            $request->file_kuitansi_transfer->move(storage_path() . '/upload/file_kuitansi_transfer/', $name);

            $pembayaran->file_kuitansi_transfer = $url;
        }

        $pembayaran->nomor_kuitansi_pph23 = $request->nomor_kuitansi_pph23;
        $pembayaran->tanggal_kuitansi_pph23 = Carbon::parse($request->tanggal_kuitansi_pph23);
        if ($request->file_kuitansi_pph23 != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_kuitansi_pph23-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_kuitansi_pph23->getClientOriginalExtension();
            $url = $name;
            $request->file_kuitansi_pph23->move(storage_path() . '/upload/file_kuitansi_pph23/', $name);

            $pembayaran->file_kuitansi_pph23 = $url;
        }

//        dd($pembayaran);

        $pembayaran->save();

//        $users = User::getUserListFromRole('P-BPU');
//        if (isset($users)) {
//            foreach ($users as $item) {
//                $jenis_user     = TIPE_INTERNAL;
//                $notif          = new Notification();
//                $notif->from    = Auth::user()->username;
//                $notif->url     = strtolower($jenis_user) . "/detail_kontrak/" . $pembayaran->kontrak->id;
//                $notif->subject = "INVOICE CREATED";
//                $notif->to      = $item->username;
//                $notif->status  = "UNREAD";
//                $notif->message = "Invoice telah diupload oleh <br/><b>" . Auth::user()->nama_user . "</b>";
//                $notif->icon    = "icon-briefcase";
//                $notif->color   = "btn-green";
//                $notif->save();
//            }
//        }

        return redirect('internal/detail_kontrak/' . $pembayaran->kontrak->id)->with('success', 'Kuitansi berhasil disimpan.');
    }

    public function sendKuitansi($id)
    {
        $pembayaran = Pembayaran::findOrFail($id);
        $pembayaran->status = KUITANSI_SENT;
        $pembayaran->save();

//        $kontrak = $pembayaran->kontrak;

        $user = Kontrak::getUser($pembayaran->kontrak);

//        $users = User::getUserListFromRole('PMJ');
//        if (isset($users)) {
//            foreach ($users as $item) {
        $jenis_user = TIPE_EKSTERNAL;
        $notif = new Notification();
        $notif->from = Auth::user()->username;
        $notif->url = strtolower($jenis_user) . "/detail_kontrak/" . $pembayaran->kontrak->id;
        $notif->subject = "KUITANSI";
        $notif->to = $user->username;
        $notif->status = "UNREAD";
        $notif->message = "Pemberitahuan kuitansi dari <br/><b>" . Auth::user()->nama_user . "</b>";
        $notif->icon = "icon-briefcase";
        $notif->color = "btn-green";
        $notif->save();
//            }
//        }

        return redirect('internal/detail_kontrak/' . $pembayaran->kontrak->id)->with('success', 'Kuitansi berhasil dikirim ke ' . $user->nama_user . '.');
    }
}
