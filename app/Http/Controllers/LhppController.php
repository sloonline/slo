<?php

namespace App\Http\Controllers;

use App\BayGardu;
use App\DjkAgenda;
use App\DjkRegistrasi;
use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\PjtDjk;
use App\Tracking;
use App\TtDjk;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Permohonan;
use App\TipeInstalasi;
use App\Provinces;
use App\Cities;

use App\Lhpp;
use App\LhppIuptl;
use App\LhppPelaksana;
use App\LhppKit;
use App\JenisInstalasi;
use App\References;
use App\InstalasiPembangkit;
use App\InstalasiTransmisi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LhppController extends Controller
{
    public function showLhpp()
    {
        $lhpp_baru = Lhpp::lhpp_baru();
        $lhpp_verifikasi = Lhpp::lhpp_verifikasi();
        $lhpp_diterima = Lhpp::lhpp_diterima();
        $lhpp_ditolak = Lhpp::lhpp_ditolak();

        return view('internal.laporan.lhpp.lhpp',
            compact('lhpp_baru', 'lhpp_diterima', 'lhpp_ditolak', 'lhpp_verifikasi'));
    }

    public function createLhpp($id = 0)
    {
        $lhpp = null;
        if ($id != 0) {
            $lhpp = Lhpp::findorfail($id);
            $ketua = $lhpp->ketua_pelaksana();
            $anggota = $lhpp->anggota();
            $pjt = $lhpp->pjt();
            $kont_pemasangan = $lhpp->kont_pemasangan();
            $kons_perencana = $lhpp->kons_perencana();
            $foto = json_decode($lhpp->foto);
        }

        #get nomor agenda
        $no_agenda = "";
        if ($lhpp != null) {
            $instalasi_id = $lhpp->instalasi_id;
            $jenis_instalasi_id = $lhpp->instalasi->id_jenis_instalasi;
            if ($lhpp->bay_gardu_id != null) {
                $instalasi_id = $lhpp->bay_gardu_id;
            }
            $djk_agenda = DjkAgenda::where('instalasi_id', $instalasi_id)
                ->where('tipe_instalasi_id', $lhpp->tipe_instalasi)
                ->where('jenis_instalasi_id', $jenis_instalasi_id)
                ->first();

            $no_agenda = @$djk_agenda->no_agenda;
        }

        $permohonan = Permohonan::all();
        $tipe_instalasi = TipeInstalasi::all();
        $provinsi = Provinces::all();
        $kota = Cities::all();
        $ref_parent = References::where('nama_reference', 'Jenis Bahan Bakar')->first();
        $jenis_bahan_bakar = References::where('parent', $ref_parent->id)->get();
        $master_pjt = PjtDjk::all();
        $master_tt = TtDjk::all();

        $tmp_pjt = DB::table('pjt_djk');
        $master_pjt_tt = DB::table('tt_djk')
            ->union($tmp_pjt)
            ->get();
        #ambil instlasi yang tidak memiliki instalasi apabila create lhpp baru
        $pembangkit = InstalasiPembangkit::doesntHave("lhpp")->has('djk_agenda')->get();
        $transmisi = InstalasiTransmisi::getUnusedLhppWithAgenda();
        $distribusi = InstalasiDistribusi::doesntHave("lhpp")->has('djk_agenda')->get();
        $pemanfaatan_tt = InstalasiPemanfaatanTT::doesntHave("lhpp")->has('djk_agenda')->get();
        $pemanfaatan_tm = InstalasiPemanfaatanTM::doesntHave("lhpp")->has('djk_agenda')->get();

        if ($lhpp != null) {
            if (@$lhpp->tipe_instalasi == ID_PEMBANGKIT) {
                $pembangkit->push(InstalasiPembangkit::findorfail($lhpp->instalasi_id));
            } else if (@$lhpp->tipe_instalasi == ID_TRANSMISI) {
                $trans = InstalasiTransmisi::findorfail($lhpp->instalasi_id);
                if ($trans->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                    $trans->bay_gardu = BayGardu::where('id', @$lhpp->bay_gardu_id)->get();
                }
                $transmisi->push($trans);
            } else if (@$lhpp->tipe_instalasi == ID_DISTRIBUSI) {
                $distribusi->push(InstalasiDistribusi::findorfail($lhpp->instalasi_id));
            } else if (@$lhpp->tipe_instalasi == ID_PEMANFAATAN_TT) {
                $pemanfaatan_tt->push(InstalasiPemanfaatanTT::findorfail($lhpp->instalasi_id));
            } else if (@$lhpp->tipe_instalasi == ID_PEMANFAATAN_TM) {
                $pemanfaatan_tm->push(InstalasiPemanfaatanTM::findorfail($lhpp->instalasi_id));
            }
        }

        return view('internal.laporan.lhpp.create_lhpp',
            compact('permohonan',
                'tipe_instalasi',
                'provinsi',
                'kota',
                'jenis_bahan_bakar',
                'pembangkit',
                'transmisi',
                'distribusi',
                'pemanfaatan_tt',
                'pemanfaatan_tm',
                'lhpp',
                'ketua',
                'anggota',
                'pjt',
                'kont_pemasangan',
                'kons_perencana',
                'foto',
                'master_pjt',
                'master_tt',
                'master_pjt_tt',
                'no_agenda')
        );
    }

    public function saveLhpp(Request $request)
    {
//        dd($request);
        $lhpp = new Lhpp();
        if ($request->lhpp_id != null) {
            $lhpp = Lhpp::findorfail($request->lhpp_id);
        }
        #save lhpp
        $lhpp->no_lhpp = $request->no_lhpp;
        $lhpp->no_slo = $request->no_slo;
        $lhpp->no_djk_agenda = $request->no_djk_agenda;
        // $lhpp->no_urut = $request->no_urut;
        $lhpp->no_permohonan = $request->no_permohonan;
        $lhpp->nama_pemohon = $request->nama_pemohon;
        $lhpp->no_surat = $request->nomor_surat;
        $lhpp->tgl_surat = $request->tanggal_surat_permohonan;
        $lhpp->nama_pemilik_instalasi = $request->nama_pemilik_instalasi;
        $lhpp->alamat_pemilik_instalasi = $request->alamat_pemilik_instalasi;
        $lhpp->nama_badan_usaha = $request->nama_bujptl;
        $lhpp->alamat_badan_usaha = $request->alamat_bujptl;
        $lhpp->kode_badan_usaha = $request->kode_bujptl;
        $lhpp->klasifikasi = $request->klasifikasi_bujptl;
        $lhpp->kualifikasi = $request->kualifikasi_bujptl;
        $lhpp->pj_badan_usaha = $request->pj_bujptl;
        $lhpp->pjt_badan_usaha = $request->pjt_bujptl;
        $lhpp->masa_berlaku = $request->masa_berlaku_bujptl;
        $lhpp->no_msl_diagram = $request->no_msl_diagram;
        $lhpp->tgl_msl_diagram = $request->tgl_msl_diagram;
        $lhpp->waktu_pelaksanaan = $request->waktu_pelaksanaan;
        $lhpp->no_spkppp = $request->no_spkppp;
        $lhpp->tgl_spkppp = $request->tgl_spkppp;
        $lhpp->hasil_pp = $request->hasil_pp;
        $lhpp->kesimpulan = $request->kesimpulan;
        $lhpp->rekomendasi = $request->rekomendasi;
        $lhpp->no_lap_p_inspeksi = $request->no_lap_p_inspeksi;
        $lhpp->instalasi_id = $request->instalasi_id;
        $lhpp->tipe_instalasi = $request->tipe_instalasi_id;
        $lhpp->tgl_lhpp = $request->tgl_lhpp;
        $lhpp->status_permohonan_djk = DJK_NOT_SENT;

        #save foto - foto pelaksanaan
        $arr_foto = [];
        if (sizeof($request->foto) > 0) {
            foreach ($request->foto as $key => $foto) {
                if ($foto != null) {
                    $filename = $this->saveFile($foto, 'foto_' . $key, $request->no_lhpp);
                    array_push($arr_foto, $filename);
                } else {
                    array_push($arr_foto, null);
                }
            }
        }
        //cek foto mana yang terbaru
        $arr_old_foto = json_decode($lhpp->foto);
        $data_foto = [];
        for ($i = 0; $i < 4; $i++) {
            if(isset($arr_foto[$i])){
                if ($arr_foto[$i] == null) {
                    (sizeof($arr_old_foto) > $i) ? array_push($data_foto, $arr_old_foto[$i]) : array_push($data_foto, null);
                } else {
                    array_push($data_foto, $arr_foto[$i]);
                }
            } else{
                if(sizeof($arr_old_foto) > $i){
                    array_push($data_foto, $arr_old_foto[$i]);
                }else{
                    array_push($data_foto, null);
                }
            }
        }
        $lhpp->foto = json_encode($data_foto);

        #cek untuk menyimpan instalasi id
        if ($request->tipe_instalasi_id == ID_TRANSMISI) {
            $lhpp->bay_gardu_id = $request->lingkup_transmisi;
        }
        #cek tipe instalasi
        $instalasi = null;
        if ($request->tipe_instalasi_id == ID_PEMBANGKIT) {
            $instalasi = InstalasiPembangkit::findorfail($request->instalasi_id);
        } else if ($request->tipe_instalasi_id == ID_TRANSMISI) {
            $instalasi = InstalasiTransmisi::findorfail($request->instalasi_id);
        } else if ($request->tipe_instalasi_id == ID_DISTRIBUSI) {
            $instalasi = InstalasiDistribusi::findorfail($request->instalasi_id);
        } else if ($request->tipe_instalasi_id == ID_PEMANFAATAN_TT) {
            $instalasi = InstalasiPemanfaatanTT::findorfail($request->instalasi_id);
        } else if ($request->tipe_instalasi_id == ID_PEMANFAATAN_TM) {
            $instalasi = InstalasiPemanfaatanTM::findorfail($request->instalasi_id);
        }

        if ($instalasi != null) {
            $pemilik = $instalasi->pemilik;
            if ($pemilik != null) {
                $lhpp->spjbtl = $pemilik->file_spjbtl;
            }
        }


        if ($request->hasFile('file_msl_diagram')) {
            $lhpp->file_gbr_msl_diagram = saveFile($request->file_msl_diagram, 'file_msl_diagram', $request->no_lhpp);
        }
        if ($request->hasFile('file_lembar_lhpp')) {
            $lhpp->lembar_lhpp = saveFile($request->file_lembar_lhpp, 'file_lembar_lhpp', $request->no_lhpp);
        }
        if ($request->hasFile('file_laporan_final')) {
            $lhpp->laporan_final = saveFile($request->file_laporan_final, 'file_laporan_final', $request->no_lhpp);
        }
        if ($request->hasFile('file_draft_sertifikat_slo')) {
            $lhpp->draft_sertifikat_slo = saveFile($request->file_draft_sertifikat_slo, 'file_draft_sertifikat_slo', $request->no_lhpp);
        }
        if ($request->hasFile('file_sertifikat_slo')) {
            $lhpp->sertifikat_slo = saveFile($request->file_sertifikat_slo, 'file_sertifikat_slo', $request->no_lhpp);
        }
        if ($request->hasFile('file_srt_permohonan_slo')) {
            $lhpp->surat_permohonan = saveFile($request->file_srt_permohonan_slo, 'file_srt_permohonan_slo', $request->no_lhpp);
        }
        if ($request->hasFile('file_srt_kesesuaian')) {
            $lhpp->surat_kesesuaian = saveFile($request->file_srt_kesesuaian, 'file_srt_kesesuaian', $request->no_lhpp);
        }
        #end file upload

        #start set no agenda
        $no_agenda = '';
        $instalasi_id = $instalasi->id;
        if ($request->lingkup_transmisi != null) {
            $instalasi_id = $request->lingkup_transmisi;
        }

        $djk_agenda = DjkAgenda::where('instalasi_id', $instalasi_id)
            ->where('tipe_instalasi_id', $request->tipe_instalasi_id)
            ->where('jenis_instalasi_id', $instalasi->id_jenis_instalasi)
            ->first();

        $no_agenda = @$djk_agenda->no_agenda;
        $lhpp->no_djk_agenda = $no_agenda;
        #end set no agenda

        $lhpp->save();

        if ($lhpp->save()) {
            // $this->saveInstalasi($request, $lhpp->id);
            #sementara save satu persatu
            if ($request->ketua_nama != null) {
                $this->saveKetua($request, $lhpp->id, ID_KETUA_PELAKSANA);
            }

            if ($request->agt_nama != null) {
                $this->saveAnggota($request, $lhpp->id, ID_ANGGOTA);
            }

            if ($request->pjt_nama != null) {
                $this->savePjt($request, $lhpp->id, ID_PJT);
            }

            if ($request->nama_kont_pmsg != null) {
                $this->saveKontraktorPemasangan($request, $lhpp->id, ID_KONT_PEMASANGAN);
            }

            if ($request->nama_kons_prcn != null) {
                $this->saveKonsultanPerencana($request, $lhpp->id, ID_KONS_PERENCANA);
            }

            #untuk menyimpan data - data hasil  uji
            if ($request->tipe_instalasi_id == ID_PEMBANGKIT) {
                $lhpp_kit = null;
                if ($request->lhpp_id != null) {
                    $lhpp_kit = LhppKit::where('lhpp_id', $request->lhpp_id)
                        ->first();
                }
                ($lhpp_kit == null) ? $lhpp_kit = new LhppKit() : $lhpp_kit;

                $lhpp_kit->lhpp_id = $lhpp->id;
                $lhpp_kit->kapasitas_hasil_uji = $request->kapasitas_hasil_uji;
                $lhpp_kit->kalori_hhv = $request->kalori_hhv;
                $lhpp_kit->kalori_lhv = $request->kalori_lhv;
                $lhpp_kit->kapasitas_modul = $request->kapasitas_modul;
                $lhpp_kit->kapasitas_inverter = $request->kapasitas_inverter;
                $lhpp_kit->jumlah_modul = $request->jumlah_modul;
                $lhpp_kit->jumlah_inverter = $request->jumlah_inverter;
                if ($request->hasFile('file_konsumsi_bb')) {
                    $lhpp_kit->url_file_konsumsi_bb = $this->saveFile($request->file_konsumsi_bb, 'file_konsumsi_bb', $request->no_lhpp);
                }
                $lhpp_kit->save();
            } else if ($request->tipe_instalasi_id == ID_TRANSMISI) {
            } else if ($request->tipe_instalasi_id == ID_DISTRIBUSI) {
            } else if ($request->tipe_instalasi_id == ID_PEMANFAATAN_TT) {
            } else if ($request->tipe_instalasi_id == ID_PEMANFAATAN_TM) {
            }

            #create tracking
            $tracking = Tracking::where('tipe_instalasi', $request->tipe_instalasi_id)
                ->where('instalasi_id', $instalasi->id)
                ->get();
            if ($tracking != null) {
                foreach ($tracking as $item) {
                    $item->lhpp_id = $lhpp->id;
                    $item->status = STATUS_REGISTRASI_SLO;
                    $item->save();
                }
            }
            #---------------
        }

        return redirect('internal/lhpp');
        #end save lhpp
    }


    public function saveFile($req_file, $param_file, $req_no_lhpp)
    {

        $timestamp = date('YmdHis');
        $nm_u_custom = '-' . $timestamp . '-' . str_replace('/','-', str_replace(' ','',$req_no_lhpp)) . '.';
        $filename = $param_file . $nm_u_custom . $req_file->getClientOriginalExtension();
        $fullPath = storage_path() . '/upload' . '/' . $param_file . '/' . $filename;
        if (File::exists($fullPath))
            File::delete($fullPath);
        $req_file->move(storage_path() . '/upload' . '/' . $param_file, $filename);
        
        return $filename;
    }

    public function saveKontraktorPemasangan($request, $lhpp_id, $tipe_iuptl)
    {
        #save kontraktor Pemasangan
        $lhpp_iuptl = null;
        if ($request->lhpp_id != null) {
            $lhpp_iuptl = LhppIuptl::where('lhpp_id', $request->lhpp_id)
                ->where('tipe_iuptl', $tipe_iuptl)
                ->first();
        }

        ($lhpp_iuptl == null) ? $lhpp_iuptl = new LhppIuptl() : $lhpp_iuptl;

        $lhpp_iuptl->nama = $request->nama_kont_pmsg;
        $lhpp_iuptl->alamat = $request->alamat_kont_pmsg;
        $lhpp_iuptl->id_provinsi = $request->prov_kont_pmsg;
        $lhpp_iuptl->id_kota = $request->kota_kont_pmsg;
        $lhpp_iuptl->no_iujptl = $request->no_iujptl_kont_pmsg;
        $lhpp_iuptl->kode_badan_usaha = $request->kode_kont_pmsg;
        $lhpp_iuptl->klasifikasi = $request->klasifikasi_kont_pmsg;
        $lhpp_iuptl->kualifikasi = $request->kualifikasi_kont_pmsg;
        $lhpp_iuptl->masa_berlaku = $request->masa_berlaku_kont_pmsg;
        $lhpp_iuptl->nama_pj_perusahaan = $request->nama_pj_kont_pmsg;
        $lhpp_iuptl->penerbit_ijin = $request->penerbit_ijin_kont_pmsg;
        $lhpp_iuptl->lhpp_id = $lhpp_id;
        $lhpp_iuptl->tipe_iuptl = $tipe_iuptl;
        $lhpp_iuptl->save();
        #end save kontraktor Pemasangan
    }

    public function saveKonsultanPerencana($request, $lhpp_id, $tipe_iuptl)
    {
        #save kontraktor Pemasangan
        $lhpp_iuptl = null;
        if ($request->lhpp_id != null) {
            $lhpp_iuptl = LhppIuptl::where('lhpp_id', $request->lhpp_id)
                ->where('tipe_iuptl', $tipe_iuptl)
                ->first();
        }

        ($lhpp_iuptl == null) ? $lhpp_iuptl = new LhppIuptl() : $lhpp_iuptl;

        $lhpp_iuptl->nama = $request->nama_kons_prcn;
        $lhpp_iuptl->alamat = $request->alamat_kons_prcn;
        $lhpp_iuptl->id_provinsi = $request->provinsi_kons_prcn;
        $lhpp_iuptl->id_kota = $request->kota_kons_prcn;
        $lhpp_iuptl->no_iujptl = $request->no_iujptl_kons_prcn;
        $lhpp_iuptl->kode_badan_usaha = $request->kode_kons_prcn;
        $lhpp_iuptl->klasifikasi = $request->klasifikasi_kons_prcn;
        $lhpp_iuptl->kualifikasi = $request->kualifikasi_kons_prcn;
        $lhpp_iuptl->masa_berlaku = $request->masa_berlaku_kons_prcn;
        $lhpp_iuptl->nama_pj_perusahaan = $request->nama_pj_kons_prcn;
        $lhpp_iuptl->penerbit_ijin = $request->penerbit_ijin_kons_prcn;
        $lhpp_iuptl->lhpp_id = $lhpp_id;
        $lhpp_iuptl->tipe_iuptl = $tipe_iuptl;
        $lhpp_iuptl->save();
        #end save kontraktor Pemasangan
    }

    public function saveKetua($request, $lhpp_id, $tipe_pelaksana)
    {
        #save pelaksana
        $lhpp_pelaksana = null;
        if ($request->lhpp_id != null) {
            $lhpp_pelaksana = LhppPelaksana::where('lhpp_id', $request->lhpp_id)
                ->where('tipe_pelaksana', $tipe_pelaksana)
                ->first();
        }

        ($lhpp_pelaksana == null) ? $lhpp_pelaksana = new LhppPelaksana() : $lhpp_pelaksana;

        $lhpp_pelaksana->nama = $request->ketua_nama;
        $lhpp_pelaksana->nik = $request->nik_ketua;
        $lhpp_pelaksana->no_reg_kompetensi = $request->ketua_noreg_kompetensi;
        $lhpp_pelaksana->klasifikasi = $request->ketua_klasifikasi;
        $lhpp_pelaksana->level = $request->ketua_level;
        $lhpp_pelaksana->lhpp_id = $lhpp_id;
        $lhpp_pelaksana->tipe_pelaksana = $tipe_pelaksana;
        $lhpp_pelaksana->save();
        #end save pelaksana
    }

    public function saveAnggota($request, $lhpp_id, $tipe_pelaksana)
    {
        #save pelaksana
        $lhpp_pelaksana = null;
        if ($request->lhpp_id != null) {
            $lhpp_pelaksana = LhppPelaksana::where('lhpp_id', $request->lhpp_id)
                ->where('tipe_pelaksana', $tipe_pelaksana)
                ->first();
        }

        ($lhpp_pelaksana == null) ? $lhpp_pelaksana = new LhppPelaksana() : $lhpp_pelaksana;

        $lhpp_pelaksana->nama = $request->agt_nama;
        $lhpp_pelaksana->nik = $request->nik_anggota;
        $lhpp_pelaksana->no_reg_kompetensi = $request->agt_noreg_kompetensi;
        $lhpp_pelaksana->klasifikasi = $request->agt_klasifikasi;
        $lhpp_pelaksana->level = $request->agt_level;
        $lhpp_pelaksana->lhpp_id = $lhpp_id;
        $lhpp_pelaksana->tipe_pelaksana = $tipe_pelaksana;
        $lhpp_pelaksana->save();
        #end save pelaksana
    }

    public function savePjt($request, $lhpp_id, $tipe_pelaksana)
    {
        #save pelaksana
        $lhpp_pelaksana = null;
        if ($request->lhpp_id != null) {
            $lhpp_pelaksana = LhppPelaksana::where('lhpp_id', $request->lhpp_id)
                ->where('tipe_pelaksana', $tipe_pelaksana)
                ->first();
        }

        ($lhpp_pelaksana == null) ? $lhpp_pelaksana = new LhppPelaksana() : $lhpp_pelaksana;

        $lhpp_pelaksana->nama = $request->pjt_nama;
        $lhpp_pelaksana->nik = $request->nik_pjt;
        $lhpp_pelaksana->no_reg_kompetensi = $request->pjt_noreg_kompetensi;
        $lhpp_pelaksana->klasifikasi = $request->pjt_klasifikasi;
        $lhpp_pelaksana->level = $request->pjt_level;
        $lhpp_pelaksana->lhpp_id = $lhpp_id;
        $lhpp_pelaksana->tipe_pelaksana = $tipe_pelaksana;
        $lhpp_pelaksana->save();
        #end save pelaksana
    }

    public function deleteLhpp($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $lhpp->delete();
        return redirect('internal/lhpp');
    }

    public function detailLhpp($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        // echo ($lhpp->pjt());exit;
        $foto = json_decode($lhpp->foto);
        $no_agenda = '';
        $no_permohonan_djk = '';
        $instalasi_id = $lhpp->instalasi->id;
        $jenis_instalasi_id = $lhpp->instalasi->id_jenis_instalasi;
        if ($lhpp->bay_gardu_id != null) {
            $instalasi_id = $lhpp->bay_gardu_id;
        }

        if ($instalasi_id != null) {

            $djk_agenda = DjkAgenda::where('instalasi_id', $instalasi_id)
                ->where('tipe_instalasi_id', $lhpp->tipe_instalasi)
                ->where('jenis_instalasi_id', $jenis_instalasi_id)->first();

            $no_agenda = @$djk_agenda->no_agenda;
            if ($no_agenda != null) {
                $djk_registrasi = DjkRegistrasi::where('djk_agenda_id', $djk_agenda->id)->first();
                $no_permohonan_djk = @$djk_registrasi->no_permohonan;
            }
        }
        // dd($lhpp->lhpp_kit);
        return view('internal.laporan.lhpp.detail_lhpp', compact('lhpp', 'foto', 'no_agenda', 'no_permohonan_djk'));
    }

    public function generateNoSlo($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $no_agenda = generateNoSLO($lhpp);
        $lhpp->no_slo = $no_agenda['no_slo'];
        $lhpp->no_urut = $no_agenda['no_urut'];
        $lhpp->save();
        return redirect('/internal/lhpp/update/' . $lhpp->id);
    }

    public function lokasiInstalasi($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $instalasi = $lhpp->instalasi;
        $long_awal = null;
        $lat_awal = null;
        $long_akhir = null;
        $lat_akhir = null;

        if ($lhpp->tipe_instalasi == ID_PEMBANGKIT) {
            $long_awal = $instalasi->longitude;
            $lat_awal = $instalasi->latitude;
        } else if ($lhpp->tipe_instalasi == ID_TRANSMISI ||
            $lhpp->tipe_instalasi == ID_PEMANFAATAN_TT ||
            $lhpp->tipe_instalasi == ID_PEMANFAATAN_TM
        ) {
            $long_awal = $instalasi->long_awal;
            $lat_awal = $instalasi->lat_awal;
            $long_akhir = $instalasi->long_akhir;
            $lat_akhir = $instalasi->lat_akhir;
        } else if ($lhpp->tipe_instalasi == ID_DISTRIBUSI) {
            $long_awal = $instalasi->longitude_awal;
            $lat_awal = $instalasi->latitude_awal;
            $long_akhir = $instalasi->longitude_akhir;
            $lat_akhir = $instalasi->latitude_akhir;
        }

        $ltlng = [
            'long_awal' => $long_awal,
            'lat_awal' => $lat_awal,
            'long_akhir' => $long_akhir,
            'lat_akhir' => $lat_akhir
        ];

        $ltlng = collect($ltlng);

        return view('public.map', compact('ltlng', 'lhpp'));
    }

    public function lhppPublic($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $foto = json_decode($lhpp->foto);
        $no_agenda = '';
        $no_permohonan_djk = '';
        $instalasi_id = $lhpp->instalasi->id;
        $jenis_instalasi_id = $lhpp->instalasi->id_jenis_instalasi;
        if ($lhpp->bay_gardu_id != null) {
            $instalasi_id = $lhpp->bay_gardu_id;
        }

        if ($instalasi_id != null) {

            $djk_agenda = DjkAgenda::where('instalasi_id', $instalasi_id)
                ->where('tipe_instalasi_id', $lhpp->tipe_instalasi)
                ->where('jenis_instalasi_id', $jenis_instalasi_id)->first();

            $no_agenda = @$djk_agenda->no_agenda;
            if ($no_agenda != null) {
                $djk_registrasi = DjkRegistrasi::where('djk_agenda_id', $djk_agenda->id)->first();
                $no_permohonan_djk = @$djk_registrasi->no_permohonan;
            }
        }

        return view('public.public_lhpp', compact('lhpp', 'foto', 'no_agenda', 'no_permohonan_djk'));
    }

    public function lhppFoto($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $foto = json_decode($lhpp->foto);
        return view('public.foto', compact('foto', 'lhpp'));
    }

    public function requestNomorAgenda($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        $instalasi = $lhpp->instalasi;
        $param = [
            'tipe_instalasi' => $lhpp->tipe_instalasi,
            'instalasi' => $instalasi,
            'bay_gardu_id' => $lhpp->bay_gardu_id,
            'waktu_pelaksanaan' => $lhpp->waktu_pelaksanaan
        ];

        #request nomor agenda ke djk
        $response = requestNomorAgenda($param);

        #send to view
        Session::flash('message', $response['message']);
        Session::flash('alert_type', $response['alert_type']);

        return redirect('internal/lhpp');
    }

    public function registrasiSlo($lhpp_id)
    {
        $lhpp = Lhpp::findorfail($lhpp_id);
        if ($lhpp != null) {
            $response = registrasiSLO($lhpp);
            Session::flash('message', @$response['message']);
            Session::flash('data_null', @$response['data_null']);
            Session::flash('alert_type', @$response['alert_type']);
        } else {
            Session::flash('message', 'LHPP Null');
            Session::flash('alert_type', 'alert-danger');
        }

        return redirect(url('internal/lhpp'));
    }


}
