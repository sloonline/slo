<?php

namespace App\Http\Controllers;

use App\BusinessArea;
use App\JenisPekerjaan;
use App\JenisPekerjaanWbs;
use App\Kontrak;
use App\Notification;
use App\Order;
use App\PengalihanBiaya;
use App\PermohonanWbsIo;
use App\Perusahaan;
use App\RabWbsIo;
use App\References;
use App\Tracking;
use App\User;
use App\WbsIo;
use App\WbsIoRealisasi;
use App\Workflow;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class WBSIOController extends Controller
{
    public function showWbsIo()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_MONITOR_WBS_IO) || User::isCurrUserAllowPermission(PERMISSION_CREATE_WBS_IO)) {
            $wbs_io = WbsIo::orderBy('updated_at', 'desc')->get();
            return view('internal.kontrak.wbs', compact('wbs_io'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function detailWbsIo($id)
    {
        $wbsIo = WbsIo::findOrFail($id);
        $jenis = References::find($wbsIo->jenis_id);
        return ($jenis->nama_reference == WBS) ? $this->detailWbs($id) : $this->detailIo($id);
    }

    public function createWbs($id = 0)
    {
        $ref_tipe = References::getRef('Tipe WBS IO');
        $ref_status = References::getRef('Status WBS IO');
        $ref_status = $ref_status->keyBy('nama_reference');
        if (!User::isCurrUserAllowPermission(PERMISSION_CLOSE_WBS_IO)) {
            $ref_status->forget("CLOSED");
        }
        if (!User::isCurrUserAllowPermission(PERMISSION_LOCK_WBS_IO)) {
            $ref_status->forget("LOCKED");
        }
        if (!User::isCurrUserAllowPermission(PERMISSION_ACTIVATE_WBSIO)) {
            $ref_status->forget("ACTIVE");
        }
        $kontrak = Kontrak::getKontrakPLN();
        $order = Order::getOrderPLN();
        $business_area = BusinessArea::all();
        $jenis_pekerjaan = JenisPekerjaanWbs::all();
        $wbs = ($id == 0) ? null : WbsIo::find($id);
        $current_wf = null;
        if ($wbs != null && WbsIo::isAllowEdit($wbs)) {
            $tipe = $wbs->tipeWbs;
            $modul_wbs = ($tipe->nama_reference == TIPE_NORMAL) ? MODUL_WBS_IO_NORMAL : MODUL_WBS_IO_EMERGENCY;
            $current_wf = WorkflowController::getCurrentWorkflow($wbs->id_flow_status, $modul_wbs);
        }
        $completed = ($id == 0) ? false : WbsIo::isWorkflowCompleted($wbs);
        $data = compact('ref_tipe', 'ref_status', 'kontrak', 'order', 'business_area', 'jenis_pekerjaan', 'wbs', 'current_wf', 'completed');
        if ($id == 0 || @$wbs->flow_status->tipe_status == CREATED || @$wbs->flow_status->tipe_status == REJECTED) {
            return view('internal.kontrak.create_wbs', $data);
        } else if ($completed == false) {
            return view('internal.kontrak.edit_wbs_normal', $data);
        } else if ($completed) {
            $tipe = $wbs->tipeWbs->nama_reference;
            if ($tipe == TIPE_NORMAL) {
                return view('internal.kontrak.edit_wbs_normal', $data);
            } else {
                return view('internal.kontrak.create_wbs', $data);
            }
        }
    }

    function detailWbs($id)
    {
        $ref_tipe = References::getRef('Tipe WBS IO');
        $kontrak = Kontrak::all();
        $jenis_pekerjaan = JenisPekerjaanWbs::all();
        $wbs = ($id == 0) ? null : WbsIo::findOrFail($id);
        $realisasi = ($wbs != null) ? $wbs->realisasi->sum('jumlah') : 0;
        $persen_realisasi = round(($realisasi / $wbs->nilai) * 100, 2);
        $sisa = $wbs->nilai - $realisasi;
        $persen_sisa = 100 - $persen_realisasi;
        $pengalihan = ((sizeof(@$wbs->pengalihan_biaya) > 0) ? @$wbs->pengalihan_biaya->sum('nilai') : 0);
        $sisa_pengalihan = (@$wbs->nilai - ((sizeof(@$wbs->pengalihan_biaya) > 0) ? @$wbs->pengalihan_biaya->sum('nilai') : 0));
        $persen_pengalihan = round(((@$wbs->nilai == 0) ? 0 : $pengalihan / @$wbs->nilai) * 100, 2);
        $persen_sisa_pengalihan = 100 - $persen_pengalihan;
        $data_workflow = array(
            'model' => MODEL_WBS_IO,
            'modul' => ($wbs->tipeWbs->nama_reference == TIPE_NORMAL) ? MODUL_WBS_IO_NORMAL : MODUL_WBS_IO_EMERGENCY,
            'url_notif' => 'detail_wbs_io/',
            'url_redirect' => 'internal/show_wbs_io/'
        );
        $workflow = WorkflowController::workflowJoss($id, TABLE_WBS_IO, ACTION_WORKFLOW);
        return view('internal.kontrak.detail_wbs', compact('ref_tipe', 'kontrak', 'business_area', 'jenis_pekerjaan', 'wbs', 'pengalihan', 'sisa_pengalihan',
            'realisasi', 'persen_realisasi', 'sisa', 'persen_sisa', 'workflow', 'data_workflow', 'persen_pengalihan', 'persen_sisa_pengalihan'));
    }


    public function saveWbsIo(Request $request)
    {
        $tipe = References::find($request->tipe);

        if ($tipe->nama_reference == TIPE_NORMAL) {
            if ($request->rab == null) {
                Session::flash('msg', 'Anda belum memilih RAB!');
                return redirect('internal/show_wbs_io');
            }
        }
        $parent = References::where('nama_reference', 'Jenis WBS IO')->first()->id;
        //$flow_status = WorkflowController::getFlowStatus(SUBMITTED, MODUL_WBS_IO_NORMAL);
        $id = $request->id;
        $wbs = ($id == 0) ? new WbsIo() : WbsIo::find($id);
        $wbs->kontrak_id = ($tipe->nama_reference == TIPE_NORMAL) ? $request->kontrak : null;
        $wbs->order_id = ($tipe->nama_reference == TIPE_EMERGENCY) ? $request->order : null;
        $wbs->jenis_id = References::getRefByNameAndParent($parent, $request->jenis)->id;
        $wbs->tipe = $request->tipe;
        $wbs->nomor = $request->nomor;
        $wbs->tahun = $request->tahun;
        $wbs->jenis_pekerjaan_wbs_id = $request->jenis_pekerjaan;
        $wbs->uraian_pekerjaan = $request->uraian_pekerjaan;
        $wbs->nilai = str_replace(',', '', $request->nilai);
        $wbs->username = Auth::user()->username;

        if ($request->jenis == WBS) {
            $wbs->business_area = $request->unit;
        } else {
            $wbs->perusahaan_id = $request->perusahaan;
        }
        if (isset($request->tgl_awal)) $wbs->tgl_awal = $request->tgl_awal;
        if (isset($request->tgl_akhir)) $wbs->tgl_akhir = $request->tgl_akhir;
        if (isset($request->nomor_project)) $wbs->nomor_project = $request->nomor_project;
        //untuk io
        if (isset($request->nomor_project)) $wbs->nomor_project = $request->nomor_project;
        if ($id == 0) {
            $parent = References::where('nama_reference', 'Status WBS IO')->first()->id;
            $status = References::getRefByNameAndParent($parent, CREATED);
            if ($status != null) {
                $wbs->status = $status->id;
            }
        } else {
            if (isset($request->status)) $wbs->status = $request->status;
        }

        if ($id == 0) $wbs->id_flow_status = 0;
//file landasan

        $wbs->save();
        if ($request->file_landasan != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_landasan_wbs_io-' . $timestamp . '-' . $request->jenis . "_" . $tipe->nama_reference . "_" . $wbs->id . "." . $request->file_landasan->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_landasan->move(storage_path() . '/upload/file_landasan_wbs_io/', $name);
            $wbs->file_landasan = $url_surat;
            $wbs->save();
        }

        if ($tipe->nama_reference == TIPE_NORMAL) {
            //delete all wbs rab
            if ($wbs->rab->count() > 0) {
                RabWbsIo::where('wbsio_id', $wbs->id)->delete();
            }
            //insert new wbs rab
            foreach ($request->rab as $rab) {
                $rabWbs = new RabWbsIo();
                $rabWbs->rab_id = $rab;
                $rabWbs->wbsio_id = $wbs->id;
                $rabWbs->save();
            }
            $modul = MODUL_WBS_IO_NORMAL;
        } else {
            //delete all wbs permohonan
            if ($wbs->permohonan->count() > 0) {
                PermohonanWbsIo::where('wbsio_id', $wbs->id)->delete();
            }
            //insert new wbs permohonan
            foreach ($request->permohonan as $permohonan) {
                $permohonanWbs = new PermohonanWbsIo();
                $permohonanWbs->permohonan_id = $permohonan;
                $permohonanWbs->wbsio_id = $wbs->id;
                $permohonanWbs->save();
            }
            $modul = MODUL_WBS_IO_EMERGENCY;
        }

        if ($id == 0) {
            $flow_status = WorkflowController::getFlowStatus(CREATED, $modul);
            $wbs->id_flow_status = $flow_status->id;
            $wbs->save();
            WorkflowController::createFlowHistory($wbs->id, $wbs->getTable(), $modul, $flow_status, CREATED);
        }

        #create tracking
        $tracking = Tracking::where('kontrak_id', $request->kontrak)->get();
        if ($tracking != null) {
            foreach ($tracking as $item) {
                $item->wbsio_id = $wbs->id;
                $item->status = STATUS_WBS;
                $item->save();
            }
        }
        #---------------

        return redirect('internal/show_wbs_io')->with('success', 'Data WBS/IO berhasil disimpan.')->with('jenis', $request->jenis);
    }


    public function createIo($id = 0)
    {
        $ref_tipe = References::getRef('Tipe WBS IO');
        $ref_status = References::getRef('Status WBS IO');
        $ref_status = $ref_status->keyBy('nama_reference');
        if (!User::isCurrUserAllowPermission(PERMISSION_CLOSE_WBS_IO)) {
            $ref_status->forget("CLOSED");
        }
        if (!User::isCurrUserAllowPermission(PERMISSION_LOCK_WBS_IO)) {
            $ref_status->forget("LOCKED");
        }
        if (!User::isCurrUserAllowPermission(PERMISSION_ACTIVATE_WBSIO)) {
            $ref_status->forget("ACTIVE");
        }
        $kontraks = Kontrak::getKontrakSwasta();
        //untuk io, maka cari kontrak yg sudah ada pembayaran
        $kontrak = array();
        foreach ($kontraks as $item) {
            if ($item->pembayaranPaid->count() > 0) {
                array_push($kontrak, $item);
            }
        }
        $order = Order::getOrderSwasta();
        $perusahaan = Perusahaan::where('kategori_perusahaan', '!=', 1)->get();
        $jenis_pekerjaan = JenisPekerjaanWbs::all();
        $io = ($id == 0) ? null : WbsIo::find($id);
        $completed = ($id == 0) ? false : WbsIo::isWorkflowCompleted($io);
        $current_wf = null;
        if ($io != null && WbsIo::isAllowEdit($io)) {
            $tipe = $io->tipeWbs;
            $modul_wbs = ($tipe->nama_reference == TIPE_NORMAL) ? MODUL_WBS_IO_NORMAL : MODUL_WBS_IO_EMERGENCY;
            $current_wf = WorkflowController::getCurrentWorkflow($io->id_flow_status, $modul_wbs);
        }
        $data = compact('io', 'ref_tipe', 'ref_status', 'kontrak', 'perusahaan', 'jenis_pekerjaan', 'order', 'completed', 'current_wf');
        if ($id == 0 || @$io->flow_status->tipe_status == CREATED || @$io->flow_status->tipe_status == REJECTED) {
            return view('internal.kontrak.create_io', $data);
        } else if ($completed == false) {
            return view('internal.kontrak.edit_io_normal', $data);
        } else if ($completed) {
            $tipe = $io->tipeWbs->nama_reference;
            if ($tipe == TIPE_NORMAL) {
                return view('internal.kontrak.edit_io_normal', $data);
            } else {
                return view('internal.kontrak.create_io', $data);
            }
        }
    }

    function detailIo($id)
    {
        $ref_tipe = References::getRef('Tipe WBS IO');
        $jenis_pekerjaan = JenisPekerjaanWbs::all();
        $io = ($id == 0) ? null : WbsIo::find($id);
        $perhitungan = WbsIo::getColorWarning($io);
        $realisasi = ($io != null) ? $io->realisasi->sum('jumlah') : 0;
        $persen_realisasi = $perhitungan['persen_realisasi'];
        $sisa = $perhitungan['saldo'];
        $persen_sisa = 100 - $persen_realisasi;
        $paid = @$io->kontrak->pembayaranPaid;
        $pembayaran = (sizeof($paid) > 0) ? $paid->sum('nilai') : 0;
        $data_workflow = array(
            'model' => MODEL_WBS_IO,
            'modul' => ($io->tipeWbs->nama_reference == TIPE_NORMAL) ? MODUL_WBS_IO_NORMAL : MODUL_WBS_IO_EMERGENCY,
            'url_notif' => 'detail_wbs_io/',
            'url_redirect' => 'internal/show_wbs_io/'
        );
        $workflow = WorkflowController::workflowJoss($id, TABLE_WBS_IO, ACTION_INTERNAL_WBS_IO);
        return view('internal.kontrak.detail_io', compact('ref_tipe', 'jenis_pekerjaan', 'io',
            'realisasi', 'persen_realisasi', 'sisa', 'persen_sisa', 'workflow', 'data_workflow', 'pembayaran'));
    }

    public function realisasi()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_REALISASI_WBS_IO)) {
            $template_wbs = 'file_template_wbs-default.xls';
            $template_io = 'file_template_io-default.xls';
            $realisasi = null;
            $jenis = null;
            if (session('msg')) {
                $realisasi = session('msg')['realisasi'];
                $jenis = session('msg')['jenis'];
            }
            $data_realisasi = WbsIoRealisasi::orderBy('tgl_upload', 'desc')->get();
//        dd($realisasi);
            return view('internal.kontrak.realisasi', compact('jenis', 'realisasi', 'template_wbs', 'template_io', 'data_realisasi'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function saveRealisasi(Request $request)
    {
        $jenis = $request->jenis;
        $arrField = ['posting_date', (($jenis == WBS) ? 'wbs_element' : 'io_element'), 'cost_element', 'jumlah', 'deskripsi'];
        $readerObject = Excel::selectSheetsByIndex(0)->load($request->file_realisasi);
        $excel = $readerObject->get($arrField);
        $filename = $request->file_realisasi->getClientOriginalName();
        $jenis = $request->jenis;
        $realisasi = array();
        $arr_wbsio_id = array();
        foreach ($excel as $item) {
            $nomor_wbs = (($jenis == WBS) ? $item->wbs_element : $item->io_element);
            $realisasi_wbs_io = new WbsIoRealisasi();
            $wbs_io_id = WbsIo::where('nomor', $nomor_wbs)->first();
            $existing_realisasi = WbsIoRealisasi::where('nomor_wbs_io', $nomor_wbs)
                ->where('posting_date', $item->posting_date)
                ->where('jumlah', $item->jumlah)
                ->where('deskripsi', $item->deskripsi)
                ->get();
            $realisasi_wbs_io->nomor_wbs_io = $nomor_wbs;
            $realisasi_wbs_io->tgl_upload = date('Y-m-d');
            $realisasi_wbs_io->posting_date = $item->posting_date;
            $realisasi_wbs_io->jumlah = $item->jumlah;
            $realisasi_wbs_io->deskripsi = $item->deskripsi;
            $realisasi_wbs_io->username = Auth::user()->username;
            $realisasi_wbs_io->cost_element = $item->cost_element;

            if ($wbs_io_id != null) {
                $realisasi_wbs_io->wbs_io_id = $wbs_io_id->id;
                if (sizeof($existing_realisasi) <= 0) {
                    $realisasi_wbs_io->save();
                    $arr = array(
                        'data' => $realisasi_wbs_io,
                        'status' => SUCCESS
                    );
                    array_push($realisasi, $arr);
                    array_push($arr_wbsio_id, $wbs_io_id->id);
                } else {
                    $arr = array(
                        'data' => $realisasi_wbs_io,
                        'status' => DUPLICATE
                    );
                    array_push($realisasi, $arr);
                }
            } else {
                $arr = array(
                    'data' => $realisasi_wbs_io,
                    'status' => ERROR
                );
                array_push($realisasi, $arr);
            }
        }

        //calculate realisasi dari wbs untuk cek validasi realisasi dan kirim notifikasi
        $arr_wbsio_id = array_unique($arr_wbsio_id);
        $parent = References::where('nama_reference', 'Status WBS IO')->first()->id;
        $status = References::getRefByNameAndParent($parent, LOCKED);
        $status_id = @$status->id;
        $users = array();
        foreach ($arr_wbsio_id as $id) {
            $wbsIo = WbsIo::find($id);
            $roles = array();
            if ($wbsIo != null) {
                $perhitungan = WbsIo::getColorWarning($wbsIo);
                $persen_realisasi = $perhitungan['persen_realisasi'];
                if ($persen_realisasi > 70 && $persen_realisasi <= 90) {
//                    $roles = [KODE_PENGENDALI_BPU, KODE_SPV_BPU, KODE_DM_BPU, KODE_MB_BPU];
                    $users = User::getAllUserByPermission(PERMISSION_NOTIF_ALERT_WBS);
                } else if ($persen_realisasi > 90) {
                    //jika lebih dari 90% maka wbs LOCKED
                    $wbsIo->status = $status_id;
                    $wbsIo->save();
//                    $roles = [KODE_PENGENDALI_KEU];
                    $users = User::getAllUserByPermission(PERMISSION_NOTIF_ALERT_WBS_LOCKED);
                }
            }
//            foreach ($roles as $role) {
//                $users = User::getUserListFromRole($role);
            foreach ($users as $user) {
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = "internal/detail_wbs_io/" . $id;
                $notif->subject = "Realisasi " . $jenis . " No. " . $wbsIo->nomor;
                $notif->to = $user->username;
                $notif->status = "UNREAD";
                $notif->message = "Realisasi " . $jenis . " No. " . $wbsIo->nomor . " mencapai <b>" . $persen_realisasi . "%</b>";
                $notif->icon = "fa fa-exclamation-circle";
                $notif->color = "btn-warning";
                $notif->save();


                //send email detail order to peminta jasa
                $data['wbs_io'] = $wbsIo;
                $data['jenis'] = $jenis;
                $data['io'] = $wbsIo;
                $data['user'] = $user;
                $data['notif'] = $notif;
                //call send email helper
                $mail_data = array(
                    "data" => (object)$data,
                    "data_alias" => "data",
                    "to" => $user->email,
                    "to_name" => $user->nama_user,
                    "subject" => 'REALISASI ' . $jenis,
                    "description" => "REALISASI " . $jenis . " No." . $wbsIo->nomor
                );
                sendEmail($mail_data, 'emails.notif_realisasi_wbs_io', 'REALISASI WBS', 'WBS/IO');
            }
//            }
        }

        Session::flash('msg', array('realisasi' => $realisasi, 'jenis' => $jenis));
        return redirect('internal/realisasi');
    }


    public function approvalWBSIO(Request $request)
    {
        $status = $request->status;
        $keterangan = $request->keterangan;

        $wbs = WbsIo::findOrFail($request->id);
        $tipe = $wbs->tipeWbs;
        $jenis = References::find($wbs->jenis_id);
        $modul_wbs = ($tipe->nama_reference == TIPE_NORMAL) ? MODUL_WBS_IO_NORMAL : MODUL_WBS_IO_EMERGENCY;

        $flow_status = WorkflowController::getFlowStatusId($status, $modul_wbs);
        $current_wf = WorkflowController::getCurrentWorkflow($flow_status->id, $modul_wbs);
        $users = WorkflowController::getAllNextUsers($current_wf->id);

        if ($flow_status->tipe_status == SENT || $flow_status->tipe_status == SUBMITTED) {
            $keterangan = SUBMITTED;
        }
        $wbs->id_flow_status = $request->status;
        $wbs->save();
        #isi data notif
        #create object notif
        if (isset($users)) {
            foreach ($users as $item) {
                if ($item->user != null) {
                    $jenis_user = ($item->user->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                    $notif = new Notification();
                    $notif->from = Auth::user()->username;
                    $notif->url = strtolower($jenis_user) . "/detail_wbs_io/" . $request->id;
                    $notif->subject = $jenis->nama_reference . " " . $flow_status->tipe_status;
                    $notif->to = $item->user->username;
                    $notif->status = "UNREAD";
                    $notif->message = $jenis->nama_reference . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                    $notif->icon = "icon-briefcase";
                    $notif->color = "btn-green";
                    $notif->save();
                }
            }
        }

        WorkflowController::createFlowHistory($request->id, $wbs->getTable(), $modul_wbs, $flow_status, $keterangan);
        return redirect('internal/show_wbs_io')->with('succes', 'Berhasil melakukan approval ' . $request->jenis)->with('jenis', $request->jenis);

    }

    public function deleteRealisasi($id)
    {
        $realisasi = WbsIoRealisasi::findOrFail($id);
        $deskripsi = $realisasi->deskripsi;
        $realisasi->delete();
        return redirect('internal/realisasi/')->with('success', 'Realisasi ' . $deskripsi . ' berhasil dihapus.');
    }

    public function deleteRealisasiWbsIo($id)
    {
        $realisasi = WbsIoRealisasi::findOrFail($id);
        $wbsIo = $realisasi->wbs_io;
        $deskripsi = $realisasi->deskripsi;
        $realisasi->delete();

        return redirect('internal/detail_wbs_io/' . $wbsIo->id)->with('success', 'Realisasi ' . $deskripsi . ' berhasil dihapus.');
    }


    public function updateStatusWbs($id)
    {
        $wbsIo = WbsIo::findOrFail($id);
        $parent = References::where('nama_reference', 'Status WBS IO')->first()->id;
        $status = References::getRefByNameAndParent($parent, ACTIVE);

        if ($status != null) {
            $wbsIo->status = $status->id;
        }

        $wbsIo->save();

        return redirect('internal/show_wbs_io/')->with('success', 'Berhasil melakukan aktivasi WBS/IO');
    }

    public function updateWbsIo(Request $request)
    {
        $id = $request->id;
        $wbsIo = WbsIo::findOrFail($id);
        $wbsIo->status = $request->status;
        $wbsIo->nomor = $request->nomor;
        if (isset($request->tgl_awal)) $wbsIo->tgl_awal = $request->tgl_awal;
        if (isset($request->tgl_akhir)) $wbsIo->tgl_akhir = $request->tgl_akhir;
        //untuk io
        if (isset($request->nomor_project)) $wbsIo->nomor_project = $request->nomor_project;
        if (isset($request->deskripsi)) $wbsIo->deskripsi = $request->deskripsi;
        if (isset($request->tgl_awal)) $wbsIo->tgl_awal = $request->tgl_awal;
        if (isset($request->tgl_akhir)) $wbsIo->tgl_akhir = $request->tgl_akhir;
        $wbsIo->save();
        $jenis = References::find($wbsIo->jenis_id);
        return redirect('internal/show_wbs_io')->with('success', 'Data WBS/IO berhasil disimpan.')->with('jenis', $jenis->nama_reference);
    }

    public function lockExpiredEmergencyWbsIo()
    {
        $parent = References::where('nama_reference', 'Status WBS IO')->first()->id;
        $status = References::getRefByNameAndParent($parent, LOCKED);
        $parent = References::where('nama_reference', 'Tipe WBS IO')->first()->id;
        $tipe = References::getRefByNameAndParent($parent, TIPE_EMERGENCY);
        //ambil yg sudah seminggu, lalu LOCK
        $wbs = WbsIo::where(DB::raw("TRUNC(created_at) - TRUNC(SYSDATE)"), '>=', -7)->where('tipe', @$tipe->id)->where('status', '!=', @$status->id)->get();
        foreach ($wbs as $item) {
            $item->status = @$status->id;
            $item->save();
            echo "Id#" . $item->id . " " . $item->jenis->nama_reference . " No. " . $item->nomor . " LOCKED" . "<br/>";
        }
    }

    public function monitoringWbs()
    {
        $wbs = WbsIo::getMonitoringWbs();
        return view('internal.kontrak.monitoring_wbs', compact('wbs'));
    }

    public function monitoringIo()
    {
        $io = WbsIo::getMonitoringIo();
        return view('internal.kontrak.monitoring_io', compact('io'));

    }

    public function exportMonitoringWbs()
    {
        $wbs = WbsIo::getMonitoringWbs();
        $data = [
            'wbs' => $wbs
        ];
        Excel::create('Monitoring WBS-' . date('Y-m-d'), function ($excel) use ($data) {
            $excel->sheet('Monitoring WBS', function ($sheet) use ($data) {
                $sheet->loadView('internal.kontrak.monitoring_wbs_export_excel', $data)
                    ->mergeCells('A1:L1')
                    ->setBorder('A3:L' . (3 + sizeof($data['wbs'])), 'thin', "D8572C");
            });
        })->download('xls');
    }

    public function exportMonitoringIo()
    {
        $io = WbsIo::getMonitoringIo();
        $data = [
            'io' => $io
        ];
        Excel::create('Monitoring IO-' . date('Y-m-d'), function ($excel) use ($data) {
            $excel->sheet('Monitoring IO', function ($sheet) use ($data) {
                $sheet->loadView('internal.kontrak.monitoring_io_export_excel', $data)
                    ->mergeCells('A1:Q1')
                    ->setBorder('A3:Q' . (3 + sizeof($data['io'])), 'thin', "D8572C");
            });
        })->download('xls');
    }

    public function pengalihanBiaya()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_REALISASI_WBS_IO)) {
            $pengalihan = PengalihanBiaya::orderBy('updated_at', 'desc')->get();
            return view('internal.kontrak.pengalihan_biaya', compact('pengalihan'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function createPengalihanBiaya($id = 0, $wbs_id = 0)
    {
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_REALISASI_WBS_IO)) {
            $pengalihan = ($id == 0) ? null : PengalihanBiaya::find($id);
            if ($pengalihan != null) $wbs_id = $pengalihan->wbs_id;
            $allWbs = WbsIo::getActivatedWBS();
            $wbs = ($wbs_id > 0) ? WbsIo::find($wbs_id) : null;
            return view('internal.kontrak.create_pengalihan_biaya', compact('pengalihan', 'wbs', 'id', 'allWbs', 'wbs_id'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function savePengalihanBiaya(Request $request)
    {
        $id = $request->id;
        $wbs = $request->wbs;
        $wbs_id = $request->wbs_id;
        $pengalihan = ($id > 0) ? PengalihanBiaya::find($id) : new PengalihanBiaya();
        $pengalihan->wbs_id = $request->wbs_id;
        $pengalihan->tgl_pengalihan = Carbon::parse($request->tgl_pengalihan)->format('Y-m-d');
        $pengalihan->nilai = str_replace(',', '', $request->nilai);
        $pengalihan->keterangan = $request->keterangan;
        $pengalihan->surat_pengalihan = $request->surat_pengalihan;
        if ($request->hasFile('file_pengalihan')) {
            if ($pengalihan->file_pengalihan != null) {
                $fullPath = storage_path() . '/upload/file_pengalihan/' . $pengalihan->file_pengalihan;
                if (File::exists($fullPath))
                    File::delete($fullPath);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_pengalihan-' . $timestamp . '-' . $request->wbs_id . "." . $request->file_pengalihan->getClientOriginalExtension();
            $request->file_pengalihan->move(storage_path() . '/upload/file_pengalihan/', $name);
            $pengalihan->file_pengalihan = $name;
        }
        $pengalihan->save();
        //cek apakah berasal dari halaman detail wbs atau bukan
        $url = ($id == 0 && $wbs > 0 && $wbs_id == $wbs) ? "internal/detail_wbs_io/" . $wbs_id : "internal/pengalihan_biaya/";
        return redirect($url)->with('success', 'Data pengalihan biaya berhasil disimpan.');
    }

    public function destroyPengalihanBiaya($id)
    {
        $pengalihan = PengalihanBiaya::findOrFail($id);
        $pengalihan->delete();
        return redirect('internal/pengalihan_biaya')->with('success', 'Data pengalihan biaya berhasil dihapus.');
    }
}
