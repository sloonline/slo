<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ArtikelRequest;
use App\Http\Controllers\Controller;
use App\Artikel;

class ArtikelControlller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$artikel = Artikel::with('user')->with('tags')->get();
		return view('list_artikel', compact('artikel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create_artikel');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtikelRequest $request)
    {
        //
        $artikel = new Artikel();
        $artikel->judul     = $request->judul;
        $artikel->lokasi    = $request->lokasi;
        $artikel->views     = $artikel->views+1;
        $artikel->isi       = $request->isi;
        $artikel->id_user   = 1;
        //$artikel->tags()->attach(1);

        $artikel->save();

        return redirect('/artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $artikel->views++;

        $artikel->save();

        return view('show', compact('artikel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $artikel = Artikel::findOrFail($id);

        return view('edit_artikel',compact('artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtikelRequest $request, $id)
    {
        //
        $artikel  = Artikel::findOrFail($id);
        $artikel->judul = $request->judul;
        $artikel->lokasi = $request->lokasi;
        $artikel->isi = $request->isi;
        $artikel->save();
        return redirect('/artikel/show/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $artikel  = Artikel::findOrFail($id);
        $artikel->delete();
        return redirect('/artikel');
    }
}
