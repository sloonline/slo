<?php

namespace App\Http\Controllers;

use App\BayGardu;
use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\JenisInstalasi;
use App\Kontraktor;
use App\References;
use App\TipeInstalasi;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\InstalasiPembangkit;
use App\InstalasiTransmisi;
use App\PermohonanDjk;
use App\PjtDjk;
use App\TtDjk;
use App\AreaDjk;
use App\RayonDjk;
use App\WilayahDjk;
use App\KotaDjk;
use App\ProvinsiDjk;
use App\SbuDjk;
use App\DjkAgenda;
use App\Lhpp;
use App\DjkLog;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller
{
    public $url_djk = URL_PROD_DJK;
 //   public $url_djk = URL_DEV_DJK;

    function execCurl($postData, $url)
    {
        $consumerId = "pusertif";
       # $secretKey = "cxDH2Zap";
	$secretKey = "Yhs6sxTW";
        $proxy = 'http://10.1.9.20:8080';
        $proxyauth = 'epusertif:P@ssw0rd';

        //Computes timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
        //Computes signature by hashing the salt with the secret key as the key
        $signature = hash_hmac('sha256', $consumerId . "&" . $tStamp, $secretKey, true);

        // base64 encode�
        $encodedSignature = base64_encode($signature);

        $header[] = "X-cons-id:" . $consumerId;
        $header[] = "X-timestamp:" . $tStamp;
        $header[] = "X-signature:" . $encodedSignature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_PROXY, $proxy);
	curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($postData != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $tmp = curl_exec($ch);
        curl_close($ch);
        
	$djk_log = new DjkLog();
        $djk_log->url = $url;
        $djk_log->post_data = json_encode($postData);
        $djk_log->response_data = json_encode($tmp);
        $djk_log->created_by = (Auth::user() != null) ? Auth::user()->nama_user : "System Scheduler";
        $djk_log->save();

	return $tmp;
    }
    
    function getLitPJT(){
        $response = $this->execCurl(null, $this->url_djk.DJK_PJT);
        $dcd_response = json_decode($response);
         dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        PjtDjk::query()->truncate();
        foreach ($dcd_response->pjt as $item) {
            $pjt = new PjtDjk();
            $pjt->id = $item->pjt_id;
            // $pjt->wilayah_id = $item->wilayah_id;
            // $pjt->area_id = $item->area_id;
            $pjt->nama = $item->nama;
            $pjt->nik = $item->nik;
            $pjt->bidang = $item->bidang;
            $pjt->subbidang = $item->subbidang;
            $pjt->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getLitTT(){
        $response = $this->execCurl(null, $this->url_djk.DJK_TT);
        $dcd_response = json_decode($response);
        #dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        TtDjk::query()->truncate();
        foreach ($dcd_response->tt as $item) {
            $tt = new TtDjk();
            $tt->id = $item->tt_id;
            // $tt->wilayah_id = $item->wilayah_id;
            // $tt->area_id = $item->area_id;
            $tt->nama = $item->nama;
            $tt->nik = $item->nik;
            $tt->bidang = $item->bidang;
            $tt->subbidang = $item->subbidang;
            $tt->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getPlnArea(){
        $response = $this->execCurl(null, DJK_AREA);
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        AreaDjk::query()->truncate();
        foreach ($dcd_response->area as $item) {
            $area = new AreaDjk();
            $area->area_id = $item->area_id;
            $area->wilayah_id = $item->wilayah_id;
            $area->nama = $item->nama;
            $area->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getPlnRayon(){
        $response = $this->execCurl(null, DJK_RAYON);
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        RayonDjk::query()->truncate();
        foreach ($dcd_response->rayon as $item) {
            $rayon = new RayonDjk();
            $rayon->unit_id = $item->unit_id;
            $rayon->wilayah_id = $item->wilayah_id;
            $rayon->area_id = $item->area_id;
            $rayon->nama = $item->nama;
            $rayon->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getPlnWilayah(){
        $response = $this->execCurl(null, $this->url_djk.DJK_WILAYAH);
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        WilayahDjk::query()->truncate();
        foreach ($dcd_response->wilayah as $item) {
            $wilayah = new WilayahDjk();
            $wilayah->id = $item->wilayah_id;
            $wilayah->nama = $item->nama;
            $wilayah->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getRefKota(){
        $response = $this->execCurl(null, $this->url_djk.DJK_KOTA);
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        KotaDjk::query()->truncate();
        foreach ($dcd_response->kota as $item) {
            $kota = new KotaDjk();
            $kota->id = $item->kota_id;
            $kota->provinsi_id = $item->provinsi_id;
            $kota->nama = $item->nama;
            $kota->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }
    
    function getRefProvinsi(){
        $response = $this->execCurl(null, $this->url_djk.DJK_PROVINSI);
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        #sementara delete insert (next ganti ke insert if not exist)
        ProvinsiDjk::query()->truncate();
        foreach ($dcd_response->provinsi as $item) {
            $provinsi = new ProvinsiDjk();
            $provinsi->id = $item->provinsi_id;
            $provinsi->nama = $item->nama;
            $provinsi->save();
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan';
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }

    function getRefSbu(){
        ini_set('max_execution_time', 3600);
        $response = $this->execCurl(null, $this->url_djk.DJK_SBU);
        $dcd_response = json_decode($response);
        #insertion renisa 18042017, sync dengan tabel kontraktor
        //get all kode kontraktor yang ada di tabel kontraktor
        $kontraktors = Kontraktor::all();
        $inserted = 0;
        $updated = 0;
        #sementara delete insert (next ganti ke insert if not exist)
        SbuDjk::query()->truncate();
        foreach ($dcd_response->sbu as $item) {
            $sbu = new SbuDjk();
            $sbu->kode = $item->kode;
            $sbu->nama = $item->nama;
            $sbu->masaberlaku = $item->masaberlaku;
            $sbu->save();
            //cek apakah ada di db
            $kontraktor = $kontraktors->where('kode',$sbu->kode)->first();
            if($kontraktor != null){
                //jika ada, maka update data
                $kontraktor->nama = $sbu->nama;
                $kontraktor->masa_berlaku = $sbu->masaberlaku;
                $kontraktor->save();
                $updated++;
            }else{
                //jika tidak ada, maka create yang baru
                $new_kontraktor = new Kontraktor();
                $new_kontraktor->kode = $sbu->kode;
                $new_kontraktor->nama = $sbu->nama;
                $new_kontraktor->masa_berlaku = $sbu->masaberlaku;
                $new_kontraktor->save();
                $inserted++;
            }
        }

        if ($dcd_response->metadata->message == 'Sukses') {
            return 'Data berhasil disimpan. Total inserted : ' .$inserted.' , total updated : '.$updated ;
        } else {
            return 'Maaf, terjadi kesalahan';
        }
    }

    function getHasilSLO($lhpp_id = null)
    {
        $param = null; 
        if($lhpp_id != null){
            $lhpp = Lhpp::findOrFail($lhpp_id);
            // $param = [
            //     "no_agenda" => $lhpp->no_djk_agenda,
            //     "no_permohonan" => $lhpp->no_djk_permohonan,
            //     "tanggal_permohonan" => $lhpp->djk_registrasi->created_at->format('Y-m-d')
            // ];
            $param = [
                "no_permohonan" => $lhpp->no_djk_permohonan
            ];
        }
        getHasilSLO($param);
        return redirect('internal/lhpp');
    }

    function showPermohonan()
    {
        $djk_agenda = DjkAgenda::all();
        foreach($djk_agenda as $key => $item){
            //cek apakah gardu induk
            if($item->jenisInstalasi){
                if ($item->jenisInstalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                    $bay = BayGardu::find($item->instalasi_id);
                    $instalasi = @$bay->instalasi;
                    @$instalasi->nama_instalasi = @$instalasi->nama_instalasi . " (" . @$bay->nama_bay . ")";
                    $item->instalasi = clone $instalasi;
                }
            }
        }
        return view('api.permohonan', compact('djk_agenda'));
    }

    function createPermohonan()
    {
        $pembangkit = InstalasiPembangkit::doesntHave("djk_agenda")->get();
        $transmisi = array();
        $dataTransmisi = InstalasiTransmisi::all();
        #transmisi perlu dicek kembali untuk relasi dg agenda karena instalasi GI menggunakan id bay
        foreach ($dataTransmisi as $row) {
            $kode_jenis = $row->jenis_instalasi->kode_instalasi;
            //cek apakah gardu induk
            if ($kode_jenis != KODE_GARDU_INDUK_TRANSMISI && $row->djk_agenda == null) {
                array_push($transmisi, $row);
            } else {
                //jika gardu induk maka ambil per bay
                $bays = $row->BayGardu;
                $nama_transmisi = $row->nama_instalasi;
                foreach ($bays as $bay) {
                    if($bay->djk_agenda == null){
                        $newTransmisi = clone $row;
                        $newTransmisi->id = 'bay_'.$bay->id;
                        $newTransmisi->nama_instalasi = $nama_transmisi . " (" . $bay->nama_bay . ")";
                        $newTransmisi->pemilik = $bay->pemilik;
                        $newTransmisi->transmisi_id = $row->id;
                        array_push($transmisi, $newTransmisi);
                    }
                }
            }
        }
        $distribusi = InstalasiDistribusi::doesntHave("djk_agenda")->get();
        $pemanfaatan_tt = InstalasiPemanfaatanTT::doesntHave("djk_agenda")->get();
        $pemanfaatan_tm = InstalasiPemanfaatanTM::doesntHave("djk_agenda")->get();
        $transmisi = collect($transmisi);
        $kota = KotaDjk::all();
        $tipe_instalasi = TipeInstalasi::orderBy('id')->get();

        // dd($transmisi);
        return view('api.create_permohonan', compact('pembangkit', 'transmisi', 'kota', 'distribusi',
            'tipe_instalasi', 'pemanfaatan_tt', 'pemanfaatan_tm'));
    }

    function savePermohonan(Request $request){
        // dd($request);
        $instalasi = null;
        $bay_transmisi = null;
        if ($request->tipe_instalasi == ID_PEMBANGKIT) {
            $instalasi = InstalasiPembangkit::findorfail($request->pembangkit_id);
        } else if ($request->tipe_instalasi == ID_TRANSMISI) {
            $jenis_instalasi = JenisInstalasi::findorfail($request->jenis_instalasi_id);
            if ($jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                $bay_id = str_replace('bay_','',$request->transmisi_id);
                $bay_transmisi = BayGardu::findOrFail($bay_id);
                $instalasi = $bay_transmisi->instalasi;
            } else {
                $instalasi = InstalasiTransmisi::findorfail($request->transmisi_id);
            }
        } else if ($request->tipe_instalasi == ID_DISTRIBUSI) {
            $instalasi = InstalasiDistribusi::findorfail($request->distribusi_id);
        } else if ($request->tipe_instalasi == ID_PEMANFAATAN_TT) {
            $instalasi = InstalasiPemanfaatanTT::findorfail($request->pemanfaatan_tt_id);
        } else if ($request->tipe_instalasi == ID_PEMANFAATAN_TM) {
            $instalasi = InstalasiPemanfaatanTM::findorfail($request->pemanfaatan_tm_id);
        }
        $tipe_instalasi = $request->tipe_instalasi;
        $waktu_pelaksanaan = $request->tanggal;
        $param = [
            'tipe_instalasi' => $tipe_instalasi,
            'instalasi' => $instalasi,
            'bay_gardu_id' => @$bay_transmisi->id,
            'waktu_pelaksanaan' => $waktu_pelaksanaan
        ];

        #request nomor agenda ke djk
        $response = requestNomorAgenda($param);

        #send to view
        Session::flash('message', $response['message']);
        Session::flash('alert_type', $response['alert_type']);

        return redirect('internal/api/showPermohonan');
    }

    function updatePermohonan($id)
    {
        $djk_agenda = DjkAgenda::findorfail($id);
        $ref_parent = References::where('nama_reference', 'Status DJK')->first();
        $status_djk = References::where('parent', $ref_parent->id)->get();
        // dd($djk_agenda->history);
        return view('api.update_permohonan', compact('djk_agenda', 'status_djk'));
    }

    function saveUpdatePermohonan(Request $request)
    {
        // dd($request);
        $response = updateStatus($request);
        
        Session::flash('message', $response['message']);
        Session::flash('alert_type', $response['alert_type']);
        return redirect('internal/api/showPermohonan');
    }

    function pengajuanSlo($agenda_id)
    {
        $permohonan_djk = PermohonanDjk::findorfail($agenda_id);
        $blade = '';
        $instalasi = null;
        $bay_transmisi = null;
        #cek tipe instalasi untuk menampilkan form input
        if ($permohonan_djk->tipe_instalasi == ID_PEMBANGKIT) {
            $blade = 'api.create_slo_pembangkit';
            $instalasi = InstalasiPembangkit::findorfail($permohonan_djk->instalasi_id);
        } else if ($permohonan_djk->tipe_instalasi == ID_TRANSMISI) {
            $blade = 'api.create_slo_transmisi';
            //cek apakah gardu induk
            if (@$permohonan_djk->jenisInstalasi->kode_instalasi == '231') {
                $bay_transmisi = BayGardu::findOrFail($permohonan_djk->instalasi_id);
                $instalasi = $bay_transmisi->instalasi;
                $instalasi->nama_instalasi = $instalasi->nama_instalasi . " (" . $bay_transmisi->nama_bay . ")";
                $instalasi->nama_bay = $instalasi->nama_instalasi;
            } else {
                $instalasi = InstalasiTransmisi::findorfail($permohonan_djk->instalasi_id);
                $instalasi->nama_bay = "";
            }
        } else if ($permohonan_djk->tipe_instalasi == ID_DISTRIBUSI) {
            $blade = 'api.create_slo_distribusi';
            $instalasi = InstalasiDistribusi::findorfail($permohonan_djk->instalasi_id);
        } else {
            $blade = 'api.create_slo_pemanfaatan';
            $instalasi = ($permohonan_djk->tipe_instalasi == ID_PEMANFAATAN_TT) ? InstalasiPemanfaatanTT::findorfail($permohonan_djk->instalasi_id) : InstalasiPemanfaatanTM::findorfail($permohonan_djk->instalasi_id);
        }
        // dd($instalasi->jenis_instalasi->kode_instalasi);
        $master_tt = TtDjk::all();
        $master_pjt = PjtDjk::all();
        $master_sbu = SbuDjk::all();
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', $permohonan_djk->tipe_instalasi)->get();
        $master_tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $jenis_bay = array(BAY_LINE, BAY_COUPLER, BAY_TRANSFORMATOR, BAY_KAPASITOR, BAY_REAKTOR);

        
        return view($blade, compact('master_pjt', 'master_tt', 'instalasi', 'permohonan_djk'
            , 'master_sbu', 'master_tegangan_pengenal', 'jenis_instalasi', 'bay_transmisi', 'jenis_bay'));
    }


    function saveSloKit(Request $request)
    {
        $permohonan_djk = PermohonanDjk::findorfail($request->permohonan_djk_id);

        #convert request to array
        $postData = $request->all();
        unset($postData['_token']);
        // dd($postData);

        $permohonan_djk = $this->saveFile($permohonan_djk, $request);
        $postData['url_foto_pelaksanaan'] = $permohonan_djk->url_foto_pelaksanaan;
        $postData['url_file_konsumsi_bahan_bakar'] = $permohonan_djk->url_file_konsumsi_bahan_bakar;
        $postData['url_detail_lhpp'] = $permohonan_djk->url_detail_lhpp;


        $result = $this->sendSloApiDjk($postData, $permohonan_djk);

        Session::flash('message', $result['message']);
        Session::flash('alert_type', $result['alert_type']);
        return redirect('internal/api/showPermohonan');
    }

    function saveSloTrans(Request $request)
    {
        $permohonan_djk = PermohonanDjk::findorfail($request->permohonan_djk_id);

        #convert request to array
        $postData = $request->all();
        unset($postData['_token']);
        // dd($postData);

        #save file upload

        $permohonan_djk = $this->saveFile($permohonan_djk, $request);

        #-----END SAVE FILE------
        $nama_bay = $request->nama_bay;
        $jenis_bay = $request->jenis_bay;
        unset($postData['jenis_bay']);
        unset($postData['nama_bay']);
        $postData['line_bay'] = "";
        $postData['bus_coupler_bay'] = "";
        $postData['transformer_bay'] = "";
        if ($permohonan_djk->tipe_instalasi == ID_TRANSMISI) {
            //cek apakah gardu induk
            if (@$permohonan_djk->jenisInstalasi->kode_instalasi == '231') {
                switch ($jenis_bay) {
                    case BAY_LINE :
                        $postData['line_bay'] = $nama_bay;
                        break;
                    case BAY_COUPLER :
                        $postData['bus_coupler_bay'] = $nama_bay;
                        break;
                    case BAY_TRANSFORMATOR :
                        $postData['transformer_bay'] = $nama_bay;
                        break;
                }
            }
        }
        $postData['url_foto_pelaksanaan'] = $permohonan_djk->url_foto_pelaksanaan;
        $postData['url_detail_lhpp'] = $permohonan_djk->url_detail_lhpp;
        unset($postData['permohonan_djk_id']);
        $result = $this->sendSloApiDjk($postData, $permohonan_djk);

        Session::flash('message', $result['message']);
        Session::flash('alert_type', $result['alert_type']);
        return redirect('internal/api/showPermohonan');
    }

    function showDetail($permohonan_id)
    {
        $permohonan = PermohonanDjk::findorfail($permohonan_id);
        $tipe_instalasi = strtolower($permohonan->tipeInstalasi->nama_instalasi);
        return view('api.detail_permohonan', compact('permohonan', 'tipe_instalasi'));
    }

    function saveSloDistribusi(Request $request)
    {

        $permohonan_djk = PermohonanDjk::findorfail($request->permohonan_djk_id);

        #convert request to array
        $postData = $request->all();
        unset($postData['_token']);

        #save file upload
        $permohonan_djk = $this->saveFile($permohonan_djk, $request);
        #-----END SAVE FILE------

        unset($postData['permohonan_djk_id']);
        $postData['url_foto_pelaksanaan'] = $permohonan_djk->url_foto_pelaksanaan;
        $postData['url_detail_lhpp'] = $permohonan_djk->url_detail_lhpp;

        $result = $this->sendSloApiDjk($postData, $permohonan_djk);

        Session::flash('message', $result['message']);
        Session::flash('alert_type', $result['alert_type']);
        return redirect('internal/api/showPermohonan');
    }

    function saveSloPemanfaatan(Request $request)
    {

        $permohonan_djk = PermohonanDjk::findorfail($request->permohonan_djk_id);

        #convert request to array
        $postData = $request->all();
        unset($postData['_token']);

        #save file upload
        $permohonan_djk = $this->saveFile($permohonan_djk, $request);
        #-----END SAVE FILE------

        unset($postData['permohonan_djk_id']);
        $postData['url_foto_pelaksanaan'] = $permohonan_djk->url_foto_pelaksanaan;
        $postData['url_detail_lhpp'] = $permohonan_djk->url_detail_lhpp;

        $result = $this->sendSloApiDjk($postData, $permohonan_djk);

        Session::flash('message', $result['message']);
        Session::flash('alert_type', $result['alert_type']);
        return redirect('internal/api/showPermohonan');
    }

    private function saveFile($permohonan_djk, Request $request)
    {
        #save  file  to FTP Server
        $ftp_server = "10.1.18.146";
        $ftp_username = "pusertif";
        $ftp_userpass = "P@ssw0rd";
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);

        $timestamp = date('YmdHis');
        #save file upload
        $nm_u_custom = '-' . $timestamp . '-' . $permohonan_djk->id . '.';
        if ($request->hasFile('url_detail_lhpp')) {
            $permohonan_djk->url_detail_lhpp = 'file_detail_lhpp' . $nm_u_custom . $request->url_detail_lhpp->getClientOriginalExtension();
            $filename = $permohonan_djk->url_detail_lhpp;
            $fullPath = storage_path() . '/upload/file_detail_lhpp/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->url_detail_lhpp->move(storage_path() . '/upload/file_detail_lhpp', $permohonan_djk->url_detail_lhpp);

            // upload file
            if (ftp_put($ftp_conn, 'slo/storage/upload/file_detail_lhpp/' . $filename, $fullPath, FTP_BINARY)) {
                $permohonan_djk->url_detail_lhpp = 'pusertif.pln.co.id/upload/' . $filename;
            } else {
                //log jika gagal
            }
        }

        if ($request->hasFile('url_foto_pelaksanaan')) {
            $permohonan_djk->url_foto_pelaksanaan = 'file_foto_pelaksanaan' . $nm_u_custom . $request->url_foto_pelaksanaan->getClientOriginalExtension();
            $filename = $permohonan_djk->url_foto_pelaksanaan;
            $fullPath = storage_path() . '/upload/file_foto_pelaksanaan/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->url_foto_pelaksanaan->move(storage_path() . '/upload/file_foto_pelaksanaan', $permohonan_djk->url_foto_pelaksanaan);

            // upload file
            if (ftp_put($ftp_conn, 'slo/storage/upload/file_foto_pelaksanaan/' . $filename, $fullPath, FTP_BINARY)) {
                $permohonan_djk->url_foto_pelaksanaan = 'pusertif.pln.co.id/upload/' . $filename;
            } else {
                //log jika gagal
            }
        }

        if ($request->hasFile('url_file_konsumsi_bahan_bakar')) {
            $permohonan_djk->url_file_konsumsi_bahan_bakar = 'file_konsumsi_bahan_bakar' . $nm_u_custom . $request->url_file_konsumsi_bahan_bakar->getClientOriginalExtension();
            $filename = $permohonan_djk->url_file_konsumsi_bahan_bakar;
            $fullPath = storage_path() . '/upload/file_konsumsi_bahan_bakar/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->url_file_konsumsi_bahan_bakar->move(storage_path() . '/upload/file_konsumsi_bahan_bakar', $permohonan_djk->url_file_konsumsi_bahan_bakar);

            // upload file
            if (ftp_put($ftp_conn, 'slo/storage/upload/file_konsumsi_bahan_bakar/' . $filename, $fullPath, FTP_BINARY)) {
                $permohonan_djk->url_file_konsumsi_bahan_bakar = 'pusertif.pln.co.id/upload/' . $filename;
            } else {
                //log jika gagal
            }
        }


        #-----END SAVE FILE------
        return $permohonan_djk;
    }

    private function sendSloApiDjk($postData, $permohonan_djk)
    {
        $message = '';
        $alert_type = '';
        $response = $this->execCurl($postData, $this->url_djk.DJK_REG);
        #get nomor agenda
        $dcd_response = json_decode($response);
        // dd($dcd_response);
        if ($dcd_response == null) {
            $message = 'Koneksi API DJK gagal!';
            $alert_type = 'alert-danger';
        } else {
            if (isset($dcd_response->metadata)) {
                if ($dcd_response->metadata->code == 1) {
                    $message = $dcd_response->metadata->message . '! Nomor Permohonan Berhasil Disimpan';
                    $alert_type = 'alert-success';

                    $permohonan_djk->no_permohonan = $dcd_response->response->no_permohonan;
                    $permohonan_djk->save();
                } else {
                    $message = $dcd_response->metadata->message;
                    $alert_type = 'alert-danger';
                }
            }
        }

        return array('message' => $message, 'alert_type' => $alert_type);

    }

    public function generateNoSlo($instalasi, $request){
        #generate nomor SLO
        $last_permohonan = PermohonanDjk::orderby('no_urut', 'desc')->first();
        $no_slo = '';
        $no_urut = '';
        $jenis_ijin_usaha = '';
        $penerbit = '01';
        $kode_jenis_instalasi = $instalasi->jenis_instalasi->kode_instalasi;
        $kota_djk = $request->instalasi_kota_id;
        $kepemilikan = '';
        $kode_sistem_jaringan = '';
        $tegangan_pengenal = '';
        $kode_sistem_distribusi = '';
        $kode_sbu = '';
        $tahun = date("y");

        /* GENERATE NOMOR SLO
         * Terdiri dari beberapa koponen
         *
         * */

        #NOMOR URUT
        $no_urut = $last_permohonan->no_urut + 1;

        #JENIS IJIN USAHA
        if(@$instalasi->pemilik->jenis_ijin_usaha == 35){
            $jenis_ijin_usaha = 'O';
        }else{
            $jenis_ijin_usaha = 'U';
        }

        #KEPEMILIKAN INSTALASI
        if(@$instalasi->pemilik->perusahaan->is_pln == 1){
            $kepemilikan = '1';
        }else{
            $kepemilikan = '2';
        }

        #KODE SISTEM JARINGAN TRANSMISI
        #KODE SISTEM JARINGAN DISTRIBUSI
        #TEGANGAN PENGENAL
        if(@$instalasi->sis_jar_tower != null){
            $ref = References::findorfail(@$instalasi->sis_jar_tower);
            $kode_sistem_jaringan = $ref->value;
        }
        if(@$instalasi->sistem_jaringan != null){
            $ref = References::findorfail(@$instalasi->sistem_jaringan);
            $kode_sistem_distribusi = $ref->value;
        }
        if(@$instalasi->tegangan_pengenal != null){
            $ref = References::findorfail(@$instalasi->tegangan_pengenal);
            $tegangan_pengenal = $ref->value;
        }

        #KODE SBU(KONTRAKTOR)
        if($instalasi->kode_kontraktor != null){
            $kode_sbu = $instalasi->kode_kontraktor;
        }else{
            $kode_sbu = '0000';
        }

        #CREATE NOMOR SLO BERDASARKAN JENIS INSTALASI BERBEDA - BEDDA
        if ($request->tipe_instalasi == ID_PEMBANGKIT || $request->tipe_instalasi == ID_PEMANFAATAN_TM || $request->tipe_instalasi == ID_PEMANFAATAN_TT  ) {
            $no_slo = $no_urut.'.'.$jenis_ijin_usaha.'.'.$penerbit.'.'.$kode_jenis_instalasi.'.'.$kota_djk.'.'.$kode_sbu.'.'.$tahun;
        }else if($request->tipe_instalasi == ID_TRANSMISI){
            $no_slo = $no_urut.'.'.$jenis_ijin_usaha.'.'.$penerbit.'.'.$kode_jenis_instalasi.'.'.$kepemilikan.$kode_sistem_jaringan.$tegangan_pengenal.'.'.$kode_sbu.'.'.$tahun;
        }else if($request->tipe_instalasi == ID_DISTRIBUSI){
            $no_slo = $no_urut.'.'.$jenis_ijin_usaha.'.'.$penerbit.'.'.$kode_jenis_instalasi.'.'.$kepemilikan.$kode_sistem_distribusi.$tegangan_pengenal.'.'.$kode_sbu.'.'.$tahun;
        }

        $arr = [
            'no urut' => $no_urut,
            'jenis ijin usaha'=> $jenis_ijin_usaha,
            'penerbit' => $penerbit,
            'kode jenis instalasi' => $kode_jenis_instalasi,
            'kepemilikan' => $kepemilikan,
            'PEMBANGKIT kota' => $kota_djk,
            'TRANSMISI kode jaringan' => $kode_sistem_jaringan,
            'TRANSMISI tegangan pengenal' => $tegangan_pengenal,
            'DISTRIBUSI sistem jaringan' => $kode_sistem_distribusi,
            'kode sbu' => $kode_sbu,
            'tahun' => $tahun
        ];

        $response = [
            'no_slo' => $no_slo,
            'no_urut' => $no_urut
        ];
        return $response;
        #end generate nomor SLO
    }

    
}
    
