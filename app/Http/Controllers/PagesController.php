<?php

namespace App\Http\Controllers;

use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\InstalasiPembangkit;
use App\InstalasiTransmisi;
use App\Lhpp;
use App\Lpi;
use App\Order;
use App\Permohonan;
use App\Produk;
use App\TipeInstalasi;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function about(){
        return view('about');
    }

    public function dashboardEksternal(){
//        $menuActive = [
//                        'dashboard'=>'active',
//                        'tracking'=>'',
//                        'order'=>'',
//                        'instalasi'=>'',
//                        'pemilik-instalasi'=>'',
//                        'surat-tugas'=>'',
//        ];
        $pembangkit         = InstalasiPembangkit::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        $transmisi          = InstalasiTransmisi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        $distribusi         = InstalasiDistribusi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        $pemanfaatan_tt     = InstalasiPemanfaatanTT::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        $pemanfaatan_tm     = InstalasiPemanfaatanTM::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();

//        $layanan            = Produk::all();
//        $tipe_instalasi     = TipeInstalasi::whereNot();
//        dd($transmisi->count());

        $kit                = TipeInstalasi::findOrFail(1);
        $layanan_kit        = $kit->produk;

        $trs                = TipeInstalasi::findOrFail(2);
        $layanan_trs        = $trs->produk;

        $dis                = TipeInstalasi::findOrFail(3);
        $layanan_dis        = $dis->produk;

        $ptt                = TipeInstalasi::findOrFail(4);
        $layanan_ptt        = $ptt->produk;

        $ptm                = TipeInstalasi::findOrFail(5);
        $layanan_ptm        = $ptm->produk;

        $permohonan_baru  = Order::getTotalNewOrderByPemintajasa();
        $jumlah_instalasi  = 0;
        $jumlah_instalasi += InstalasiPembangkit::totalByPemintaJasa();
        $jumlah_instalasi += InstalasiDistribusi::totalByPemintaJasa();
        $jumlah_instalasi += InstalasiTransmisi::totalByPemintaJasa();
        $jumlah_instalasi += InstalasiPemanfaatanTM::totalByPemintaJasa();
        $jumlah_instalasi += InstalasiPemanfaatanTT::totalByPemintaJasa();
        $lhpp = Lhpp::getByPemintaJasa();

        return view('eksternal/dashboard2', compact('pembangkit', 'transmisi', 'distribusi', 'pemanfaatan_tt', 'pemanfaatan_tm',
            'layanan_kit', 'layanan_trs', 'layanan_dis', 'layanan_ptt', 'layanan_ptm','permohonan_baru','jumlah_instalasi','lhpp'));
    }

    public function dashboardInternal(){
        $permohonan_baru = Order::getTotalNewOrder();
        $jumlah_instalasi  = 0;
        $jumlah_instalasi += InstalasiPembangkit::total();
        $jumlah_instalasi += InstalasiDistribusi::total();
        $jumlah_instalasi += InstalasiTransmisi::total();
        $jumlah_instalasi += InstalasiPemanfaatanTM::total();
        $jumlah_instalasi += InstalasiPemanfaatanTT::total();
        $lhpp = Lhpp::where('status_permohonan_djk',APPROVED)->count();
        $lpi  = Lpi::all()->count();
        $latest_permohonan  = Permohonan::getLatestOrder(20);
        $latest_slo = Lhpp::getLatest(20);


        return view('internal/dashboard', compact('permohonan_baru', 'jumlah_instalasi', 'lhpp',
            'lpi', 'latest_permohonan', 'latest_slo'));

    }
}
