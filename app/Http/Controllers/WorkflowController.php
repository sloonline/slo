<?php

namespace App\Http\Controllers;

use App\FlowHistory;
use App\FlowStatus;
use App\Kontrak;
use App\Notification;
use App\Order;
use App\Role;
use App\SortModul;
use App\User;
use App\Tracking;
use App\UserRole;
use App\Workflow;
use App\WorkflowRole;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class WorkflowController extends Controller
{
    /* ----NOTES-----
    * # Load control workflow : untuk menentukan tombol apa saja yang harus ditampilkan
    * # Create history workflow : untuk menyimpan history saat dilakukan approval
    * # Function lainnya untuk mensupport 2 function di atas
    * */
    //seen type untuk menentukan history dilihat oleh siapa (apakah internal, eksternal, atau keduanya).
    #load control workflow
    public static function workflowJoss($ref_id, $ref_table, $action = "", $seen_type = TIPE_INTERNAL)
    {
        #get flow history
        $history = self::getFlowHistory($ref_id, $ref_table, $seen_type);

        #get latest step
        $latest_step = self::getLatestHistory($ref_id, $ref_table);
        // dd($latest_step);
        #prepare data workflow
        $data_workflow = array(
            'ref_id' => $ref_id,
            'ref_table' => $ref_table,
            'keterangan' => false,
            'btn_submit' => false,
            'btn_approve' => false,
            'btn_reject' => false,
            'btn_revise' => false,
            'val_submit' => null,
            'val_approve' => null,
            'val_reject' => null,
            'val_revise' => null,
            'action' => $action,
            'btn_penawaran' => false
        );
        #check for the next step
        if (isset($latest_step)) {
            #get all next users
            //            $data_workflow['next_users'] = self::getAllNextUsers($latest_step->workflow_id);
            //    dd($latest_step);
            #get all next step workflow
            $next_step = self::getAllNextStep($latest_step->workflow_id);
            //    dd($latest_step->workflow_id);
            #check whether last step or not
            if (isset($next_step)) {
                //    dd($next_step);
                // #set data for all cases
                foreach ($next_step as $item) {
                    #check authenticated user role
                    if (self::checkRoleUser($item->flow_status_id, $item->modul)) {
                        switch ($item->tipe_status) {
                            case CREATED:
                                /*renisa.kusumah
                                status created nilainya disimpan di val_submit*/
                                #Pantess
                                $data_workflow['val_create'] = $item->flow_status_id;
                                break;
                            case SUBMITTED:
                                $data_workflow['btn_submit'] = true;
                                $data_workflow['val_submit'] = $item->flow_status_id;
                                break;
                            case SENT:
                                $data_workflow['btn_submit'] = true;
                                $data_workflow['val_submit'] = $item->flow_status_id;
                                break;
                            case APPROVED:
                                $data_workflow['keterangan'] = true;
                                $data_workflow['btn_approve'] = true;
                                $data_workflow['val_approve'] = $item->flow_status_id;
                                break;
                            case REJECTED:
                                $data_workflow['keterangan'] = true;
                                $data_workflow['btn_reject'] = true;
                                $data_workflow['val_reject'] = $item->flow_status_id;
                                break;
                            case REVISED:
                                $data_workflow['keterangan'] = true;
                                $data_workflow['btn_revise'] = true;
                                $data_workflow['val_revise'] = $item->flow_status_id;
                                break;
                        }
                        if ($item->tipe_status == SENT && $item->modul == MODUL_RAB) {
                            $data_workflow['btn_penawaran'] = true;
                        }
                    }
                }
            }
        }
        $data_workflow['history'] = $history;
        return $data_workflow;
    }

    #create history workflow
    public static function createFlowHistory($ref_id, $ref_table, $modul, $flow_status, $keterangan, $attachment = "")
    {
        $workflow = self::getCurrentWorkflow($flow_status->id, $modul);

        if (self::checkRoleUser($flow_status->id, $modul)) {
            $history = new FlowHistory();
            $history->ref_id = $ref_id;
            $history->ref_table = $ref_table;
            $history->modul = $modul;
            $history->status = $flow_status->status;
            $history->keterangan = $keterangan;
            $history->attachment = $attachment;
            $history->created_by = Auth::user()->username;
            $history->workflow_id = $workflow->id;
            $history->save();
            return true;
        } else {
            return false;
        }
    }

    #get flow status id by tipe_status and modul
    public static function getFlowStatusId($status, $modul)
    {
        return FlowStatus::where('id', $status)->where('modul', $modul)->first();
    }

    #get flow status id by tipe_status and modul
    public static function getFlowStatus($status, $modul)
    {
        return FlowStatus::where('tipe_status', $status)->where('modul', $modul)->first();
    }

    #check current user role allow to use this workflow or not
    public static function checkRoleUser($flow_status_id, $modul)
    {
        $workflow = self::getCurrentWorkflow($flow_status_id, $modul);
        $user_id = Auth::user()->id;
        $roles = UserRole::where('user_id', $user_id)->get();
        $isLogin = false;
        #check user role
        foreach ($roles as $item) {
            if ($workflow->role_id == $item->role_id) {
                $isLogin = true;
            }
        }
        return $isLogin;
    }

    #get history to create flow history
    public static function getFlowHistory($ref_id, $ref_table, $seen_type = TIPE_INTERNAL)
    {
        $seen = array($seen_type);
        array_push($seen, TIPE_ALL);
        return FlowHistory::select('fh.*')
            ->from('flow_history fh')
            ->join('workflow w', 'w.id', '=', 'fh.workflow_id')
            ->whereIn('w.seen_by', $seen)
            ->where('ref_id', $ref_id)
            ->where('ref_table', $ref_table)
            ->orderBy('fh.created_at', 'desc')
            ->get();
    }

    #get latest step in flow history table
    public static function getLatestHistory($ref_id, $ref_table)
    {
        // dd($ref_table);
        return FlowHistory::where('ref_id', $ref_id)
            ->where('ref_table', $ref_table)
            ->orderBy('id', 'desc')->limit(1)->get()->first();
    }

    #get all next step for current workflow
    public static function getAllNextStep($workflow_id, $role_id = "")
    {
        return Workflow::select('b.*, c.tipe_status')
            ->join('workflow b', 'b.prev', '=', 'workflow.id')
            ->join('flow_status c', 'c.id', '=', 'b.flow_status_id')
            ->where('workflow.id', $workflow_id)
            ->get();
    }

    #get next role
    public static function getNextRole($workflow_id)
    {
        $workflow = Workflow::find($workflow_id);
        $role_id = @$workflow->role_id;
        $role = Role::findOrFail($role_id);
        $jenis_user = @$role->users[0]->jenis_user;
        $role_name = $role->name;
        $permissionRoles = $role->permissionRole;
        $permission = array();
        foreach ($permissionRoles as $pr) {
            array_push($permission, $pr->permission->display_name);
        }
        $data = array(
            'jenis_user' => $jenis_user,
            'role_name' => $role_name,
            'permission' => collect($permission)
        );
        return $data;
    }

    public static function getNextRoleById($role_id)
    {
        $role = Role::findOrFail($role_id);
        $jenis_user = @$role->users[0]->jenis_user;
        $role_name = $role->name;
        $permissionRoles = $role->permissionRole;
        $permission = array();
        foreach ($permissionRoles as $pr) {
            array_push($permission, $pr->permission->display_name);
        }
        $data = array(
            'jenis_user' => $jenis_user,
            'role_name' => $role_name,
            'permission' => collect($permission)
        );
        return $data;
    }

    #get all next user
    public static function getAllNextUsers($workflow_id)
    {
        $workflow = Workflow::find($workflow_id);
        //        $steps = self::getAllNextStep($workflow_id);
        $role_id = @$workflow->role_id;
        $users = UserRole::where('role_id', $role_id)->get();
        return $users;
    }

    public static function getAllUserByRoleId($role_id)
    {
        $users = UserRole::where('role_id', $role_id)->get();
        return $users;
    }

    #get current workflow from workflow table by status and modul
    public static function getCurrentWorkflow($status, $modul)
    {
        //        return Workflow::where('flow_status_id', $status)
        //            ->where('modul', $modul)
        //            ->first();
        $workflow = Workflow::select('w.id,w.flow_status_id,w.role_id,w.modul,w.next,w.prev,w.seen_by')
            ->from('workflow w')
            ->join('workflow wf', function ($join) {
                $join->on('wf.NEXT', '=', 'w.id')
                    ->orOn('wf.PREV', '=', 'w.id');
            })
            ->where('w.flow_status_id', $status)
            ->where('w.modul', $modul)
            ->groupBy('w.id,w.flow_status_id,w.role_id,w.modul,w.next,w.prev,w.seen_by')->get();
        return $workflow->first();
    }

    public function showWorkflow()
    {
        $workflow = Workflow::all();
        //        dd($workflow );
        return view('internal.workflow.workflow', compact('workflow'));
    }

    public function createWorkflow($id = 0)
    {
        $flow_status = FlowStatus::all();
        $role = Role::all();
        $workflow = Workflow::all();
        $cr_workflow = null;
        if ($id > 0) {
            $cr_workflow = Workflow::findOrFail($id);
        }

        return view('internal.workflow.input_workflow', compact('flow_status', 'role', 'workflow', 'cr_workflow'));
    }

    public function saveWorkflow(Request $request)
    {
        $workflow = ($request->id == null) ? new workflow() : Workflow::findOrFail($request->id);
        $workflow->flow_status_id = $request->flow_status_id;
        $workflow->role_id = $request->role_id;
        $workflow->modul = $request->modul;
        $workflow->NEXT = $request->next_workflow_id;
        $workflow->PREV = $request->prev_workflow_id;
        $workflow->seen_by = $request->seen_by;
        $workflow->save();

        return redirect('internal/workflow');
    }

    public static function approval(Request $request)
    {
        /* Langkah - langkah approval
        * 1. update data pada table untuk modul saat ini
        * 2. kirim notifikasi
        * 3. create flow history
        * 4. redirect ke page tertentu
        * */
        #1. update data pada table untuk modul saat ini
        $ref_id = $request->id;
        $str_model = 'App\\' . $request->model;
        $ref_status = $request->status;
        $ref_keterangan = $request->keterangan;
        $ref_model = $str_model::findOrFail($ref_id);
        $flow_status = self::getFlowStatusId($ref_status, $request->modul);

        $ref_model->id_flow_status = ($flow_status != null) ? $flow_status->id : null;
        $ref_model->save();

        #2. kirim notifikasi dan action on update
        $current_wf = self::getCurrentWorkflow($flow_status->id, $request->modul);
        #cek dalam model terkait apakah ada fungsi yg dijalankan saat workflow tertentu dijalankan
        if (method_exists($str_model, 'actionOnUpdateStatus')) {
            $str_model::actionOnUpdateStatus($request, $current_wf);
        }

        #2.1 update sys_status modul sebelumnya
        if ($current_wf->next == null && $flow_status->tipe_status == APPROVED) {
            //$tracking = Tracking::where($ref_model.'_id', $ref_id)->get();
            // dd('yess');
            #sementara di off karena belum benar integrasi antar modulnya
            // $current_modul              = SortModul::where('model_name', $request->model)->first();
            // $current_num                = $current_modul->order_num;
            // $parent_model               = $ref_model;
            // $parent_model->sys_status   = $current_modul->string_value;
            // $parent_model->save();

            // for ($i = $current_num - 1; $i > 0; $i--) {
            //     $str_model = SortModul::where('order_num', $i)->first();
            //     $model = 'App\\' . $str_model->model_name;
            //     $parent_model = $model::updateSysStatus($parent_model);
            // }
        }

        if ($flow_status->tipe_status == SENT || $flow_status->tipe_status == SUBMITTED) {
            $ref_keterangan = SUBMITTED;
        }

        if ($current_wf->next != null) {
            $next_role = self::getNextRole($current_wf->next);
            # jika jenis user eksternal atau role name SPV
            # maka notifikasi hanya dikirimkan ke user yang bersangkutan
            # apabila role internal (kecuali SPV) maka akan dikirimkan ke semua user dengan role tersebut
            if (($next_role['jenis_user'] == TIPE_EKSTERNAL || $next_role['jenis_user'] == TIPE_PLN) || $next_role['permission']->search(PERMISSION_NOTIF_REGIONAL) != null) {
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->subject = $request->modul . " " . $flow_status->tipe_status;
                $notif->status = "UNREAD";
                $notif->message = $request->modul . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                if ($next_role['permission']->search(PERMISSION_NOTIF_REGIONAL) != null) {
                    $jenis_user = TIPE_INTERNAL;
                    $user_spv = $ref_model::getSPV($ref_model);
                    $notif->to = @$user_spv->username;
                    $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                    $notif->save();
                } else {
                    $jenis_user = TIPE_EKSTERNAL;
                    $user = $ref_model::getUser($ref_model);
                    $notif->to = $user->username;
                    $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                    $notif->save();
                }
            } else {
                $users = self::getAllNextUsers(@$current_wf->next);
                if (isset($users)) {
                    foreach ($users as $item) {
                        $jenis_user = TIPE_INTERNAL;
                        $notif = new Notification();
                        $notif->from = Auth::user()->username;
                        $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                        $notif->subject = $request->modul . " " . $flow_status->tipe_status;
                        $notif->status = "UNREAD";
                        $notif->message = $request->modul . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                        $notif->icon = "icon-briefcase";
                        $notif->color = "btn-green";
                        $notif->to = $item->user->username;
                        $notif->save();
                    }
                }
            }
        }

        self::createFlowHistory($ref_id, $ref_model->getTable(), $request->modul, $flow_status, $ref_keterangan);

        # START Notif Workflow
        # Code berikut untuk notifikasi menggunakan tabel bantu workflow_role
        # Untuk menambahkan custom notification ke role yang dikehendaki
        $workflow_role = WorkflowRole::where('workflow_id', $current_wf->id)->get();
        if ($workflow_role != null) {
            foreach ($workflow_role as $key => $item) {
                $next_role = self::getNextRoleById($item->role_id);
                # jika jenis user eksternal atau role name SPV
                # maka notifikasi hanya dikirimkan ke user yang bersangkutan
                # apabila role internal (kecuali SPV) maka akan dikirimkan ke semua user dengan role tersebut
                if (($next_role['jenis_user'] == TIPE_EKSTERNAL || $next_role['jenis_user'] == TIPE_PLN) || $next_role['permission']->search(PERMISSION_NOTIF_REGIONAL) != null) {
                    $notif = new Notification();
                    $notif->from = Auth::user()->username;
                    $notif->subject = $request->modul . " " . $flow_status->tipe_status;
                    $notif->status = "UNREAD";
                    if (@$item->notif_message != "" || @$item->notif_message != null) {
                        $notif->message = "<b>" . @$notifRole->notif_message . "</b>";
                    } else {
                        $notif->message = $request->modul . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                    }
                    $notif->icon = "icon-briefcase";
                    $notif->color = "btn-green";
                    if ($next_role['permission']->search(PERMISSION_NOTIF_REGIONAL) != null) {
                        $jenis_user = TIPE_INTERNAL;
                        $user_spv = $ref_model::getSPV($ref_model);
                        $notif->to = @$user_spv->username;
                        $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                        $notif->save();
                    } else {
                        $jenis_user = TIPE_EKSTERNAL;
                        $user = $ref_model::getUser($ref_model);
                        $notif->to = $user->username;
                        $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                        $notif->save();
                    }
                } else {
                    $users = self::getAllUserByRoleId($item->role_id);
                    $notifRole = $item;
                    if (isset($users)) {
                        foreach ($users as $item) {
                            $jenis_user = TIPE_INTERNAL;
                            $notif = new Notification();
                            $notif->from = Auth::user()->username;
                            $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
                            $notif->subject = $request->modul . " " . $flow_status->tipe_status;
                            $notif->status = "UNREAD";
                            if (@$notifRole->notif_message != "" || @$notifRole->notif_message != null) {
                                $notif->message = "<b>" . @$notifRole->notif_message . "</b>";
                            } else {
                                $notif->message = $request->modul . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                            }
                            $notif->icon = "icon-briefcase";
                            $notif->color = "btn-green";
                            $notif->to = $item->user->username;
                            $notif->save();
                        }
                    }
                }
            }
        }
        #END Notif Workflow

        return redirect($request->url_redirect);
    }

    public static function sendNotif($users, $request)
    {

    }

    public static function sendEmail()
    {
        $mail_data = array(
            "data" => (object)$data,
            "data_alias" => "data",
            "to" => $order->user->email,
            "to_name" => $order->user->nama_user,
            "subject" => "Nomor Order " . $order->nomor_order . " " . (($flow_status->tipe_status == APPROVED) ? 'Disetujui' : 'Perlu Diperbaiki'),
            "description" => 'ORDER ' . $flow_status->tipe_status
        );
        sendEmail($mail_data, 'emails.confirm_order', $notif->subject, 'ORDER');
    }

    // public function test(){
    //     $ref_model = 'Order';
    //     $tracking = Tracking::where($ref_model.'_id', 5)->get();
    //     // foreach($tracking as $item){
    //     //     $tracking->
    //     // }
    //     // self::createTracking(3,1, MODUL_PEKERJAAN);
    // }

    // public static function createTracking($ref_id, $modul_id, $modul = MODUL_PERMOHONAN){
    //     $tracking = new Tracking();
    //     $tracking->permohonan_id = $ref_id;
    //     $tracking->save();
    // }

    public function createNotif($id)
    {
        $workflow_id = $id;
        $workflow_role = WorkflowRole::where('workflow_id', $id)->get();
        // dd($workflow_role);
        $roles = Role::orderBy('id', 'asc')->get();
        return view('internal.workflow.workflow_role', compact('roles', 'workflow_id', 'workflow_role'));
    }

    public function storeWorkflowRole(Request $request)
    {
        $roles = $request->roles;
        WorkflowRole::where('workflow_id', $request->workflow_id)->delete();
        if ($roles != null) {
            foreach ($roles as $key => $item) {
                $workflow_role = new WorkflowRole();
                $workflow_role->workflow_id = $request->workflow_id;
                $workflow_role->role_id = $item;
                $workflow_role->save();
            }
        }

        Session::flash('success', 'Workflow role berhasil disimpan!');
        return redirect('internal/workflow');
    }
}
