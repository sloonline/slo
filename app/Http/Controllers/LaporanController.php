<?php

namespace App\Http\Controllers;

use App\BaInspeksi;
use App\LaporanDataTeknik;
use App\LaporanPekerjaan;
use App\Lpi;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pekerjaan;
use App\User;
use App\Role;
use App\Rlb;
use App\Rls;
use App\BlangkoInspeksi;
use App\PendingItem;
use App\BAPendingItem;
use App\LaporanKickOff;
use App\LaporanHilo;
use App\PermohonanRlb;
use App\PermohonanRls;
use App\StrukturHilo;
use App\FormHilo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Classes\h2oxml\HTMLtoOpenXML;
use PhpOffice\PhpWord\SimpleType\Jc;
use \Zipper;

class LaporanController extends Controller
{
    #SECTION RLB
    public function showRlb()
    {
        $rlb = Rlb::all();
        return view('internal.laporan.rlb.rlb', compact('rlb'));
    }

    public function createRlb($permohonan_rlb_id, $id = 0)
    {
        // dd($permohonan_rlb_id);
        $pekerjaan = Pekerjaan::getUnusedRlb();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);
        $rlb = null;
        if ($id > 0) {
            $rlb = Rlb::find($id);
            $p = Pekerjaan::find(@$rlb->pekerjaan_id);
            $pekerjaan->push($p);

            $data_workflow = array(
                'model' => MODEL_RLB,
                'modul' => MODUL_RLB,
                'url_notif' => 'create_rlb/' . $permohonan_rlb_id . '/',
                'url_redirect' => 'internal/permohonan_rlb/'
            );
            $workflow = WorkflowController::workflowJoss($id, TABLE_RLB, ACTION_WORKFLOW);
        }
        return view('internal.laporan.rlb.create_rlb', compact('id', 'workflow', 'data_workflow', 'pekerjaan', 'mb_bki', 'rlb', 'permohonan_rlb_id'));
    }

    public function saveRlb(Request $request)
    {
        $rlb = ($request->id == 0) ? new Rlb() : Rlb::findOrFail($request->id);
        $rlb->pekerjaan_id = $request->pekerjaan_id;
        $rlb->nomor_rekomendasi = $request->no_rekomendasi;
        $rlb->tanggal_terbit = $request->tgl_terbit;
        $rlb->no_srt_tim_inspeksi = $request->surat_permohonan_tim_inspeksi;
        $rlb->tgl_srt_tim_inspeksi = $request->tgl_surat_permohonan_tim_inspeksi;
        $rlb->manager_bki = $request->manajer_bki;
        if ($request->file_rlb != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_rlb-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_rlb->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_rlb->move(storage_path() . '/upload/file_rlb/', $name);
            $rlb->file_rlb = $url_surat;
        }
        $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_RLB);
        $rlb->id_flow_status = $flow_status->id;
        $rlb->save();

        $permohonan_rlb = PermohonanRlb::findOrFail($request->permohonan_rlb_id);
        $permohonan_rlb->rlb_id = $rlb->id;
        $permohonan_rlb->save();
        if ($request->id == 0) WorkflowController::createFlowHistory($rlb->id, $rlb->getTable(), MODUL_RLB, $flow_status, CREATED);

        Session::flash('message', 'Create RLB berhasil dilakukan!');
        // return redirect('internal/rlb');
        // return redirect('internal/permohonan_rlb');
        return redirect('internal/permohonan_rlb');
    }

    public function detailRlb($rlb_id)
    {
        $rlb = Rlb::find($rlb_id);
        // dd($rlb->user);
        $pekerjaan = Pekerjaan::all();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);
        return view('internal.laporan.rlb.detail_rlb', compact('rlb', 'pekerjaan', 'mb_bki'));
    }

    public function deleteRlb($id)
    {
        $rlb = Rlb::find($id);
        $rlb->delete();
        return redirect('internal/rlb');
    }
    #END SECTION RLB


    #SECTION RLS

    public function showRls()
    {
        $rls = Rls::all();
        return view('internal.laporan.rls.rls', compact('rls'));
    }

    public function createRls($permohonan_rls_id, $id = 0)
    {
        $pekerjaan = Pekerjaan::getUnusedRls();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);
        $rls = null;
        if ($id > 0) {
            $rls = Rls::find($id);
            $p = Pekerjaan::find(@$rls->pekerjaan_id);
            $pekerjaan->push($p);

            $data_workflow = array(
                'model' => MODEL_RLS,
                'modul' => MODUL_RLS,
                'url_notif' => 'create_rls/' . $permohonan_rls_id . '/',
                'url_redirect' => 'internal/permohonan_rls/'
            );
            $workflow = WorkflowController::workflowJoss($id, TABLE_RLS, ACTION_WORKFLOW);
        }
        return view('internal.laporan.rls.create_rls', compact('workflow', 'data_workflow', 'pekerjaan', 'mb_bki', 'rls', 'id', 'permohonan_rls_id'));
    }

    public function saveRls(Request $request)
    {
        $id = $request->id;
        $rls = ($id > 0) ? Rls::findOrFail($id) : new Rls();
        // dd($request);
        $rls->pekerjaan_id = $request->pekerjaan_id;
        $rls->nomor_rekomendasi = $request->no_rekomendasi;
        $rls->tanggal_terbit = $request->tgl_terbit;
        $rls->no_srt_tim_inspeksi = $request->surat_permohonan_tim_inspeksi;
        $rls->tgl_srt_tim_inspeksi = $request->tgl_surat_permohonan_tim_inspeksi;
        $rls->manager_bki = $request->manajer_bki;
        if ($request->file_rls != null) {
            if ($rls->file_rls != null) {
                $loc = storage_path() . '/upload/file_rls/' . $rls->file_rls;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_rls-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_rls->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_rls->move(storage_path() . '/upload/file_rls/', $name);
            $rls->file_rls = $url_surat;
        }
        $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_RLS);
        // dd($flow_status);
        $rls->id_flow_status = $flow_status->id;
        $rls->save();

        $permohonan_rls = PermohonanRls::findOrFail($request->permohonan_rls_id);
        $permohonan_rls->rls_id = $rls->id;
        $permohonan_rls->save();
        // dd('asdds');
        if ($request->id == 0) WorkflowController::createFlowHistory($rls->id, $rls->getTable(), MODUL_RLS, $flow_status, CREATED);

        Session::flash('message', 'Laporan RLS berhasil disimpan.');
        // return redirect('internal/rls');
        return redirect('internal/permohonan_rls');
    }

    public function detailRls($rls_id)
    {
        $rls = Rls::find($rls_id);
        // dd($rlb->user);
        $pekerjaan = Pekerjaan::all();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);

        return view('internal.laporan.rls.detail_rls', compact('rls', 'pekerjaan', 'mb_bki'));
    }

    public function destroyRls($id)
    {
        $rls = Rls::findOrFail($id);
        $pekerjaan = $rls->pekerjaan;
        $rls->delete();
        return redirect('internal/rls')->with('success', 'Laporan RLS ' . $pekerjaan->pekerjaan . ' Berhasil Dihapus.');
    }

    #END SECTION RLS

    #START SECTION BLANGKO INSPEKSI
    public function blangkoInspeksi()
    {
        $blangko = BlangkoInspeksi::orderBy('updated_at', 'desc')->get();
        return view('internal.laporan.blangko_inspeksi.blangko_inspeksi', compact('blangko'));
    }

    public function createBlangkoInspeksi($id = 0)
    {
        // return view('internal.laporan.blangko_inspeksi.blangko_inspeksi');
//        $pekerjaan = Pekerjaan::GetPekerjaanByStatus(PEKERJAAN_CLOSED);
        $blangko = ($id == 0) ? null : BlangkoInspeksi::findOrFail($id);
        $pekerjaan = Pekerjaan::getUnusedBlangkoInspeksi();
        $permohonan = @$blangko->pekerjaan->permohonan;
        $template = @$permohonan->lingkup_pekerjaan->template;
        $file_template = ($template != null) ? $template->where('id_produk', @$permohonan->id_produk)->first() : null;
        return view('internal.laporan.blangko_inspeksi.create_blangko_inspeksi', compact('pekerjaan', 'blangko', 'id', 'file_template'));
    }

    public function saveBlangkoInspeksi(Request $request)
    {
        $id = $request->id;
        $blangko_inspeksi = ($id > 0) ? BlangkoInspeksi::findOrFail($id) : new BlangkoInspeksi();
        $blangko_inspeksi->pekerjaan_id = $request->pekerjaan_id;
        $blangko_inspeksi->tanggal_mulai_pemeriksaan = $request->tgl_mulai;
        $blangko_inspeksi->tanggal_selesai_pemeriksaan = $request->tgl_selesai;
        $blangko_inspeksi->save();
        $file = $request->file_blangko_inspeksi;
        if ($file != null) {
            if ($blangko_inspeksi->file_blangko_inspeksi != null) {
                $loc = storage_path() . '/upload/file_blangko_inspeksi/' . $blangko_inspeksi->file_blangko_inspeksi;
                if (file_exists($loc)) {
                    unlink($loc);
                }
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_blangko_inspeksi-' . $timestamp . '-' . $blangko_inspeksi->id . "-" . $blangko_inspeksi->pekerjaan_id . "." . $file->getClientOriginalExtension();
            $blangko_inspeksi->file_blangko_inspeksi = $name;
            $file->move(storage_path() . '/upload/file_blangko_inspeksi/', $name);
            $blangko_inspeksi->save();
        }

        return redirect('internal/blangko_inspeksi')->with('success', 'Data Blangko Inspeksi Berhasil Disimpan.');
    }

    public function detailBlangkoInspeksi($id)
    {
        $blangko = BlangkoInspeksi::findOrFail($id);
        return view('internal.laporan.blangko_inspeksi.detail_blangko_inspeksi', compact('blangko'));
    }

    public function destroyBlangkoInspeksi($id)
    {
        $blangko = BlangkoInspeksi::findOrFail($id);
        $blangko->delete();
        return redirect('internal/blangko_inspeksi')->with('success', 'Blangko Inspeksi Berhasil Dihapus.');
    }
    #END SECTION BLANGKO INSPEKSI

    #START SECTION KICK OFF MEETING
    public function kickOffMeeting()
    {
        $kick_off = LaporanKickOff::all();
        return view('internal.laporan.kick_off.kick_off', compact('kick_off'));
    }

    public function createKickOffMeeting($id = 0)
    {
        $kick_off = ($id == 0) ? null : LaporanKickOff::findOrFail($id);
        $pekerjaan = Pekerjaan::getUnusedKickOffMeeting();
        return view('internal.laporan.kick_off.create_kick_off', compact('kick_off', 'pekerjaan', 'id'));
    }

    public function saveKickOffMeeting(Request $request)
    {
        $id = $request->id;
        $kick_off = ($id == 0) ? new LaporanKickOff() : LaporanKickOff::findOrFail($id);
        $kick_off->pekerjaan_id = $request->pekerjaan_id;
        $kick_off->tanggal = Carbon::parse($request->tanggal)->format('Y-m-d');
        $kick_off->waktu = $request->waktu;
        $kick_off->notulis = $request->notulis;
        $kick_off->rapat_antara = $request->rapat_antara;
        $kick_off->lokasi = $request->lokasi;
        $kick_off->agenda = $request->agenda;
        $kick_off->save();
        $file = $request->file_kick_off;
        if ($file != null) {
            if ($kick_off->file_kick_off != null) {
                $loc = storage_path() . '/upload/file_kick_off/' . $kick_off->file_kick_off;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_kick_off-' . $timestamp . '-' . $kick_off->id . "-" . $kick_off->pekerjaan_id . "." . $file->getClientOriginalExtension();
            $kick_off->file_kick_off = $name;
            $file->move(storage_path() . '/upload/file_kick_off/', $name);
            $kick_off->save();
        }
        return redirect('internal/kick_off')->with('success', 'Laporan Kick Off Meeting Berhasil Disimpan.');
    }

    public function detailKickOffMeeting($id)
    {
        $kick_off = LaporanKickOff::findOrFail($id);
        return view('internal.laporan.kick_off.detail_kick_off', compact('kick_off'));
    }

    public function destroyKickOffMeeting($id)
    {
        $kick_off = LaporanKickOff::findOrFail($id);
        $kick_off->delete();
        return redirect('internal/kick_off')->with('success', 'Laporan Kick Off Meeting Berhasil Dihapus.');
    }

    public function printKickOffMeeting($id)
    {
        $kick_off = LaporanKickOff::findOrFail($id);
        // dd($kick_off->pekerjaan);
        $filename = 'Notulen Kick Off Meeting.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord(TEMPLATE_KICKOFF);
        #set value
        $phpWord->setValue("hari", $kick_off->tanggal);
        $phpWord->setValue("notulis", $kick_off->notulis);
        $phpWord->setValue("waktu", $kick_off->waktu);
        $phpWord->setValue("rapat_antara", $kick_off->rapat_antara);
        $phpWord->setValue("lokasi", $kick_off->lokasi);
        $toOpenXML = HTMLtoOpenXML::getInstance()->fromHTML($kick_off->agenda);
        $phpWord->setValue("agenda", $toOpenXML);
        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }
    #END SECTION KICK OFF MEETING

    #START SECTION LAPORAN PENFING ITEM
    public function pendingItem()
    {
        $pekerjaan = Pekerjaan::has('pending_item')->get();
        return view('internal.laporan.pending_item.pending_item', compact('pekerjaan'));
    }

    public function createPendingItem()
    {
        $pekerjaan = Pekerjaan::getWhereDMApproved();
        // $hasil_hilo = collect(json_decode(@$pekerjaan[0]->hilo->hasil_hilo));
        // dd($hasil_hilo);
        // foreach($pekerjaan as $value){
        //     $hasil_hilo = collect(json_decode(@$value->hilo->hasil_hilo));
        //     $items =  $hasil_hilo->filter(function ($item)
        //     {
        //         return $item->is_parent == 0 && $item->hasil_kesesuaian == 0;
        //     });
        //     $value->hilo = $items;
        // }

        // dd($pekerjaan);
        return view('internal.laporan.pending_item.create_pending_item', compact('pekerjaan'));
    }

    public function savePendingItem(Request $request)
    {
        $pending_item = new PendingItem();
        $pending_item->pekerjaan_id = $request->pekerjaan_id;
        $pending_item->penjelasan = $request->penjelasan;
        $pending_item->status = CREATED;
        $pending_item->save();

        Session::flash('message', 'Create Pending Item berhasil dilakukan!');
        return redirect('internal/detail_pekerjaan_pending_item/' . $pending_item->pekerjaan_id);
    }

    public function DetailPekerjaanPendingItem($pekerjaan_id)
    {
        $pekerjaan = Pekerjaan::find($pekerjaan_id);
        $pending_item = $pekerjaan->pending_item;
        return view('internal.laporan.pending_item.detail_pekerjaan_pending_item', compact('pekerjaan', 'pending_item'));
    }

    public function DetailPendingItem($pending_item_id)
    {
        $pending_item = PendingItem::find($pending_item_id);
        $ba_pending_item = $pending_item->ba_pending_item;
        // dd($ba_pending_item);
        return view('internal.laporan.pending_item.detail_pending_item', compact('pending_item', 'ba_pending_item'));
    }

    public function deletePendingItem($id)
    {
        $pending_item = PendingItem::find($id);
        $pekerjaan = $pending_item->pekerjaan;
        $pending_item->delete();
        return redirect('internal/detail_pekerjaan_pending_item/' . $pekerjaan->id);
    }

    public function saveStatusPendingItem(Request $request)
    {
//        dd($request);
        $pending_item = PendingItem::find($request->pending_item_id);
        $pending_item->status = $request->status;
        $pending_item->save();
        Session::flash('message', 'Status Pending Item Berhasil Disimpan!');
        return redirect('internal/detail_pekerjaan_pending_item/' . $pending_item->pekerjaan->id);
    }

    public function saveTindakLanjutPendingItemm(Request $request)
    {
        $ba_pending_item = new BAPendingItem();
        $ba_pending_item->pending_item_id = $request->pending_item_id;
        $ba_pending_item->tanggal_ba = $request->tgl_berita_acara;
        $ba_pending_item->hasil_verifikasi = $request->hasil_verifikasi;
        $ba_pending_item->tanggal_verifikasi = $request->tgl_verifikasi;
        $ba_pending_item->no_ba = $request->no_ba;
        if ($request->file_berita_acara != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_ba_pending_item-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_berita_acara->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_berita_acara->move(storage_path() . '/upload/file_ba_pending_item/', $name);
            $ba_pending_item->file_ba_pending_item = $url_surat;
        }

        $ba_pending_item->save();

        $pending_item = PendingItem::find($request->pending_item_id);
//        $pending_item->status = $request->status;
        $pending_item->save();

        Session::flash('message', 'Tindak Lanjut Penyelesaian berhasil Disimpan!');
        return redirect('internal/detail_pekerjaan_pending_item/' . $pending_item->pekerjaan->id);
    }

    public function printPendingItem($pekerjaan_id)
    {
        $pekerjaan = Pekerjaan::find($pekerjaan_id);
        $pending_item = $pekerjaan->pending_item;
        $permohonan = $pekerjaan->permohonan;
        $filename = 'Pending Items ' . $pekerjaan->pekerjaan . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord(TEMPLATE_PENDING_ITEMS);
        $i = 1;
        #set value
        $phpWord->setValue("nama_instalasi", $permohonan->instalasi->nama_instalasi);
        $phpWord->setValue("peminta_jasa", $permohonan->user->perusahaan->nama_perusahaan);
        $phpWord->setValue("lokasi_instalasi", $permohonan->instalasi->alamat_instalasi);

        $total_ba = 0;
        foreach ($pending_item as $pending) {
            $total_ba += $pending->ba_pending_item->count();
        }
        $phpWord->cloneRow('pending_item', $total_ba);

        foreach ($pending_item as $pending) {
            $ba_pending = $pending->ba_pending_item;
            foreach ($ba_pending as $item) {
                $phpWord->setValue("no#" . $i, $i);
                $phpWord->setValue("pending_item#" . $i, $pending->penjelasan);
                $phpWord->setValue("status#" . $i, $pending->status);
                $phpWord->setValue("no_ba#" . $i, $item->no_ba);
                $phpWord->setValue("tgl#" . $i, $item->tanggal_ba);
                $phpWord->setValue("pemenuhan#" . $i, ($item->hasil_verifikasi == 1) ? "Memenuhi" : "Tidak Memenuhi");
                $phpWord->setValue("tgl_ver#" . $i, $item->tanggal_verifikasi);
                $i++;
            }
        }
        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }
    #END SECTION LAPORAN PENDING ITEM

    #START SECTION HILO
    public function hilo()
    {
        $hilo = LaporanHilo::orderBy('updated_at', 'desc')->get();
        return view('internal.laporan.hilo.hilo', compact('hilo'));
    }

    public function createHilo($id = 0)
    {
        $hilo = ($id == 0) ? null : LaporanHilo::findOrFail($id);
        $pekerjaan = Pekerjaan::getUnusedHilo();
        $hasil_hilo = ($id == 0) ? null : $hilo->hasil_hilo;
        $hasil_form_hilo = ($id == 0) ? null : $hilo->hasil_form_hilo;
        if ($id > 0) {
            $hasil_hilo = collect(json_decode($hasil_hilo));
            $hasil_form_hilo = collect(json_decode($hasil_form_hilo));
        }
        // dd($hilo[0]);
        return view('internal.laporan.hilo.create_hilo', compact('hilo', 'pekerjaan', 'id', 'hasil_hilo', 'hasil_form_hilo'));
    }

    public function saveHilo(Request $request)
    {
        // dd($request);
        $id = $request->id;
        $pekerjaan_id = $request->pekerjaan_id;
        $pekerjaan = Pekerjaan::findOrFail($pekerjaan_id);
        $lingkup_pekerjaan = $pekerjaan->permohonan->lingkup_pekerjaan;
        $hilo = ($id > 0) ? LaporanHilo::findOrFail($id) : new LaporanHilo();
        //ambil hasil inspeksi
        $hasil = array();
        $hasil_form = array();
        if ($id > 0) {
            //jika edit, maka detect struktur dari hasil sebelumnya
            $last_hasil = $hilo->hasil_hilo;
            $last_hasil_form = $hilo->hasil_form_hilo;
            $struktur = collect(json_decode($last_hasil));
            $form = collect(json_decode($last_hasil_form));
        } else {
            //jika buat baru, maka ambil struktur dari tabel
            $struktur = StrukturHilo::getByLingkup($lingkup_pekerjaan->id);
            $form = FormHilo::getByLingkup($lingkup_pekerjaan->id);
        }
        foreach ($struktur as $item) {
            $data = $item;
            if ($item->is_dokumen == "0") {
                $data->hasil_kesesuaian = ($request->input('kesesuaian_' . $item->id) != null) ? $request->input('kesesuaian_' . $item->id) : "";
                $data->hasil_keterangan = ($request->input('ket_' . $item->id) != null) ? $request->input('ket_' . $item->id) : "";
            } else {
                $data->hasil_kesesuaian = ($request->input('dokumen_kesesuaian_' . $item->id) != null) ? $request->input('dokumen_kesesuaian_' . $item->id) : "";
                $data->hasil_keterangan = ($request->input('dokumen_ket_' . $item->id) != null) ? $request->input('dokumen_ket_' . $item->id) : "";
            }
            array_push($hasil, $data);
        }
        foreach ($form as $item) {
            $data = $item;
            $data->hasil = ($request->input('form_' . $item->id) != null) ? $request->input('form_' . $item->id) : "";
            array_push($hasil_form, $data);
        }
        $hasil = json_encode($hasil);
        $hasil_form = json_encode($hasil_form);
        $hilo->pekerjaan_id = $pekerjaan_id;
        $hilo->tanggal_mulai_pemeriksaan = $request->tanggal_mulai;
        $hilo->tanggal_selesai_pemeriksaan = $request->tanggal_selesai;
        $hilo->lingkup_pekerjaan_id = $lingkup_pekerjaan->id;
        $hilo->hasil_hilo = $hasil;
        $hilo->hasil_form_hilo = $hasil_form;
        $hilo->save();

        if ($request->file_hilo != null) {
            if ($hilo->file_hilo != null) {
                $loc = storage_path() . '/upload/file_hilo/' . $hilo->file_hilo;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_hilo-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_hilo->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_hilo->move(storage_path() . '/upload/file_hilo/', $name);
            $hilo->file_hilo = $url_surat;
            $hilo->save();
        }
        return redirect('internal/hilo')->with('success', 'Laporan HILO Berhasil Disimpan.');
    }

    public function detailHilo($id)
    {
        $hilo = LaporanHilo::findOrFail($id);
        $hasil_hilo = $hilo->hasil_hilo;
        $hasil_hilo = collect(json_decode($hasil_hilo));
        $hasil_form_hilo = $hilo->hasil_form_hilo;
        $hasil_form_hilo = collect(json_decode($hasil_form_hilo));
        return view('internal.laporan.hilo.detail_hilo', compact('hilo', 'hasil_hilo', 'id', 'hasil_form_hilo'));
    }

    public function destroyHilo($id)
    {
        $hilo = LaporanHilo::findOrFail($id);
        $hilo->delete();
        return redirect('internal/hilo')->with('success', 'Laporan HILO Berhasil Dihapus.');
    }

    //id lingkup pekerjaan
    public function generateFormHilo($id)
    {
        $struktur = StrukturHilo::getByLingkup($id);
        $form = FormHilo::getByLingkup($id);
        return view('internal.laporan.hilo.generate_form_hilo', compact('struktur', 'form'));
    }

    public function printHilo($id)
    {
        $hilo = LaporanHilo::findOrFail($id);
        $hasil_hilo = $hilo->hasil_hilo;
        $hasil_hilo = collect(json_decode($hasil_hilo));
        $hasil_form_hilo = $hilo->hasil_form_hilo;
        $hasil_form_hilo = collect(json_decode($hasil_form_hilo));
        $document = $this->generateHiloTableForDocument($hasil_hilo, $hasil_form_hilo, $hilo);
        #create new file (storage/export/*)
        $filename = 'HILO.docx';
        $filename = str_replace('/', '_', $filename);
//        saveWord($document, $filename);
        $document->save(storage_path('exports/' . $filename));
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);

    }

    #END SECTION HILO

    public function ExportWord()
    {
        $filename = 'testing.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord('Surat_jalan_pasang_baru.docx');
        #set value
        $phpWord->setValue("no_surat_jalan", 'testing');
        $phpWord->setValue("tanggal", 'testing');
        $phpWord->setValue("pic_provider", 'testing');
        $phpWord->setValue("nama_provider", 'testing');
        $phpWord->setValue("alamat_provider", 'testing');
        $phpWord->setValue("hari", 'testing');
        $phpWord->setValue("tanggal_kegiatan", 'testing');
        $phpWord->setValue("client_name", 'testing');
        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    private function generateHiloTableForDocument($hasil_hilo, $hasil_form_hilo, $hilo)
    {

        $lingkup = $hilo->lingkup_pekerjaan->jenis_lingkup_pekerjaan;
        $mata_uji = $hasil_hilo->where('is_dokumen', '0');
        $dokumen = $hasil_hilo->where('is_dokumen', '1');

        // Define table style arrays
        $styleTable = array('borderSize' => 10, 'borderColor' => '000000');
        $styleTable_right = array('borderSize' => 10, 'borderColor' => '000000', 'align' => 'right');
        $mainStyleTable = array('borderSize' => 10, 'borderColor' => '000000', 'cellMarginLeft' => 100);
        // Define cell style arrays
        $cellHCentered = array('alignment' => Jc::CENTER, 'spaceBefore' => 150, 'spaceAfter' => 150);
        $defaultParagraph = array('spaceBefore' => 150, 'spaceAfter' => 150);
        $styleCell = array('valign' => 'center');
        $styleCellHeader = array('valign' => 'center');
        $cellColSpan = array('gridSpan' => 5, 'valign' => 'center');
        // Define font style for first row
        $fontStyle = array('bold' => true);
        $smallStyle = array('size' => 8);

        $PhpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $PhpWord->createSection();
        $PhpWord->addTableStyle('tableStyle', $styleTable);
        $PhpWord->addTableStyle('tableStyle_right', $styleTable_right);
        $PhpWord->addTableStyle('mainStyleTable', $mainStyleTable);

        #DEFINE HEADER#
        $header = $section->addHeader();
        $header->firstPage();
        $table = $header->addTable('tableStyle');
        $table->addRow();
        $table->addCell(1500, $styleCellHeader)->addText('LIK', $fontStyle, $cellHCentered);
        $cell = $table->addCell(5500, $styleCellHeader);
        $cell->addText('HASIL INSPEKSI LAIK OPERASI', $fontStyle, $cellHCentered);
        $cell->addText(strtoupper($lingkup), $fontStyle, $cellHCentered);
        $table->addCell(2200, $styleCellHeader)->addText('4/FR/LIK/6.5- A2', $fontStyle, $cellHCentered);
        #END HEADER

        $section->addTextBreak(1);
        $table = $section->addTable('mainStyleTable');

        $cell = $table->addRow(900, array('tblHeader' => true))->addCell(6000, $cellColSpan);
        //header table form
        foreach ($hasil_form_hilo as $item) {
            $cell->addTextBreak(1);
            $cell->addText($item->nama_field . "  : " . $item->hasil);
        }
        $cell->addText("Tanggal Pemeriksaan  : " . date('d M Y', strtotime($hilo->tanggal_mulai_pemeriksaan)) . ' s.d ' . date('d M Y', strtotime($hilo->tanggal_selesai_pemeriksaan)));
        $cell->addTextBreak(1);

        #START MATA UJI
        $table->addRow(900);
        // Add cells
        $table->addCell(1000, $styleCell)->addText('No.', $fontStyle, $cellHCentered);
        $table->addCell(6000, $styleCell)->addText('Mata Uji', $fontStyle, $cellHCentered);
        $table->addCell(2000, $styleCell)->addText('Kriteria Penilaian SLO', $fontStyle, $cellHCentered);
        $table->addCell(2000, $styleCell)->addText('Kesesuaian', $fontStyle, $cellHCentered);
        $table->addCell(2000, $styleCell)->addText('Keterangan', $fontStyle, $cellHCentered);
        if ($dokumen->count() > 0) {
            $table->addRow();
            $table->addCell(6000, $cellColSpan)->addText('A. Mata Uji Laik Operasi', $fontStyle, $defaultParagraph);
        }
        // Add more rows / cells
//        dd($mata_uji->pluck('kriteria'));
        foreach ($mata_uji as $item) {
            $table->addRow(900);
            if ($item->is_parent == 1) {
                $table->addCell(6000, $cellColSpan)->addText($item->nomor . " " . $item->mata_uji, $fontStyle, $defaultParagraph);
            } else {
                $table->addCell(1000)->addText($item->nomor, $styleCell, $defaultParagraph);
                $table->addCell(6000)->addText($item->mata_uji, null, $defaultParagraph);
                $table->addCell(2000)->addText(($item->kriteria == null) ? ' ' : htmlentities($item->kriteria, ENT_COMPAT, "UTF-8"), null, $cellHCentered);
                $table->addCell(2000)->addText(($item->hasil_kesesuaian == "1") ? "Ya" : (($item->hasil_kesesuaian == "") ? "" : "Tidak "), null, $cellHCentered);
                $table->addCell(2000)->addText($item->hasil_keterangan, null, $defaultParagraph);
            }
        }
        $cell = $table->addRow(900, array('cantSplit' => true))->addCell(6000, $cellColSpan);
        $cell->addText('Kolom Kesesuaian diisi dengan tanda :', $smallStyle, $defaultParagraph);
        $cell->addText('a. √ jika memenuhi,', $smallStyle, $defaultParagraph);
        $cell->addText('b. X jika tidak memenuhi,', $smallStyle, $defaultParagraph);
        $cell->addText('c. NA jika tidak ada butir pemeriksaannya (Khusus yang diberi tanda persyaratan)', $smallStyle);
        #END MATA UJI

        if ($dokumen->count() > 0) {
            $table = $section->addTable('mainStyleTable');
            #START PEMERIKSAAN DOKUMEN
            $table->addRow(900);
            $table->addCell(6000, array('gridSpan' => 4, 'valign' => 'center'))->addText('B. Pemeriksaan Dokumen', $fontStyle, $defaultParagraph);
            $table->addRow(900);
            // Add cells
            $table->addCell(1000, $styleCell)->addText('No.', $fontStyle, $cellHCentered);
            $table->addCell(8000, $styleCell)->addText('Dokumen', $fontStyle, $cellHCentered);
            $table->addCell(2000, $styleCell)->addText('Ketersediaan', $fontStyle, $cellHCentered);
            $table->addCell(2000, $styleCell)->addText('Keterangan', $fontStyle, $cellHCentered);
            // Add more rows / cells
            foreach ($dokumen as $item) {
                $table->addRow();
                $table->addCell(1000)->addText($item->nomor, $styleCell, $defaultParagraph);
                $table->addCell(6000)->addText($item->mata_uji, null, $defaultParagraph);
                $table->addCell(2000)->addText(($item->hasil_kesesuaian == "1") ? "Ya" : (($item->hasil_kesesuaian == "") ? "" : "Tidak "), null, $cellHCentered);
                $table->addCell(2000)->addText($item->hasil_keterangan, null, $defaultParagraph);
            }
            #END PEMERIKSAAN DOKUMEN
        }

        $section->addTextBreak(2);
        $table = $section->addTable('tableStyle_right');
        $table->addRow();
        $table->addCell(3000, $styleCell)->addText('Tenaga Teknik', null, $cellHCentered);
        $table->addCell(3000, $styleCell)->addText('Pelaksana Inspeksi', null, $cellHCentered);
        $table->addRow();
        $cell = $table->addCell(3000, $styleCell);
        $cell->addTextBreak(2);
        $cell->addText('.........................', null, $cellHCentered);
        $cell = $table->addCell(3000, $styleCell);
        $cell->addTextBreak(2);
        $cell->addText('.........................', null, $cellHCentered);

//        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PhpWord);
//        $sTableText = $objWriter->getWriterPart('document')->getObjectAsText($table);
//        $template->setValue('table', $sTableText);
        return $PhpWord;
    }

    #START LAPORAN DATA TEKNIK

    public function dataTeknik()
    {
        $data_teknik = LaporanDataTeknik::all();
        return view('internal.laporan.data_teknik.data_teknik', compact('data_teknik'));
    }

    public function createDataTeknik($id = 0)
    {
        $data_teknik = ($id == 0) ? null : LaporanDataTeknik::findOrFail($id);
        $permohonan = @$data_teknik->pekerjaan->permohonan;
        $template = @$data_teknik->lingkup_pekerjaan->template;
        $file_template = ($template != null) ? $template->where('id_produk', @$permohonan->id_produk)->first() : null;
        $pekerjaan = Pekerjaan::getUnusedDataTeknik();
        return view('internal.laporan.data_teknik.create_data_teknik', compact('data_teknik', 'id', 'pekerjaan', 'file_template'));
    }

    public function saveDataTeknik(Request $request)
    {
        $id = $request->id;
        $data_teknik = ($id > 0) ? LaporanDataTeknik::findOrFail($id) : new LaporanDataTeknik();
        $data_teknik->pekerjaan_id = $request->pekerjaan_id;
        $pekerjaan = Pekerjaan::find($request->pekerjaan_id);
        if ($pekerjaan != null) {
            $lingkup = $pekerjaan->permohonan->lingkup_pekerjaan;
            if ($lingkup != null) {
                $data_teknik->lingkup_pekerjaan_id = $lingkup->id;
                $file = $request->hasil_data_teknik;
                if ($file != null) {
                    if ($data_teknik->hasil_data_teknik != null) {
                        $loc = storage_path() . '/upload/file_data_teknik/' . $data_teknik->hasil_data_teknik;
                        if (file_exists($loc))
                            unlink($loc);
                    }
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = 'file_data_teknik-' . $timestamp . '-' . Auth::user()->email . "." . $request->hasil_data_teknik->getClientOriginalExtension();
                    $url_surat = $name;
                    $request->hasil_data_teknik->move(storage_path() . '/upload/file_data_teknik/', $name);
                    $data_teknik->hasil_data_teknik = $url_surat;
                }
                $data_teknik->save();

            }
        }
        return redirect('internal/data_teknik')->with('sucess', 'Laporan Data Teknik Berhasil Disimpan');
    }

    public function detailDataTeknik($id = 0)
    {
        $data_teknik = ($id == 0) ? null : LaporanDataTeknik::findOrFail($id);
        return view('internal.laporan.data_teknik.detail_data_teknik', compact('data_teknik', 'id'));
    }

    public function destroyDataTeknik($id = 0)
    {
        $data_teknik = LaporanDataTeknik::findOrFail($id);
        $data_teknik->delete();
        return redirect('internal/data_teknik')->with('success', 'Laporan Data Teknik Berhasil Dihapus.');
    }
    #END LAPORAN DATA TEKNIK

    #START PERMOHONAN RLB
    public function showPermohonanRlb()
    {
        $permohonan_rlb = PermohonanRlb::orderBy('updated_at', 'desc')->get();
        return view('internal.laporan.rlb.permohonan_rlb', compact('permohonan_rlb'));
    }

    public function createPermohonanRlb($id = 0)
    {
        $pekerjaan = Pekerjaan::getUnusedRlb();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);
        $dm_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RELEASE_RLB_RLS);
        $permohonan_rlb = null;
        if ($id > 0) {
            $permohonan_rlb = PermohonanRlb::find($id);
            $p = Pekerjaan::find(@$permohonan_rlb->pekerjaan_id);
            $pekerjaan->push($p);
        }
        // dd($permohonan_rlb);
        return view('internal.laporan.rlb.create_permohonan_rlb', compact('permohonan_rlb', 'pekerjaan', 'mb_bki', 'dm_bki'));
    }

    public function saveLaporanRlb(Request $request)
    {
        $isUpdate = false;
        if ($request->id != null) {
            $permohonan_rlb = PermohonanRlb::find($request->id);
            $isUpdate = true;
        } else {
            $permohonan_rlb = new PermohonanRlb();
        }
        $permohonan_rlb->nomor_permintaan_rlb = $request->no_permintaan_rlb;
        $permohonan_rlb->tgl_permintaan = $request->tgl_permintaan_rlb;
        $permohonan_rlb->manager = $request->manager;
        $permohonan_rlb->deputi_manager = $request->deputi_manager;
        $permohonan_rlb->pekerjaan_id = $request->pekerjaan_id;
        $permohonan_rlb->tgl_inspeksi = $request->tgl_inspeksi;
        $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_PERMOHONAN_RLB);
        $permohonan_rlb->id_flow_status = $flow_status->id;
        $permohonan_rlb->save();

        if ($request->id == null) ;
        WorkflowController::createFlowHistory($permohonan_rlb->id, $permohonan_rlb->getTable(), MODUL_PERMOHONAN_RLB, $flow_status, CREATED);

        $str = "";
        ($isUpdate) ? $str = 'Update' : 'Create';
        Session::flash('message', $str . ' Permohonan RLB Berhasil Dilakukan!');
        return redirect('internal/permohonan_rlb/');
    }

    public function detailPermohonanRlb($id)
    {
        $permohonan_rlb = PermohonanRLB::find($id);
        $data_workflow = array(
            'model' => MODEL_PERMOHONAN_RLB,
            'modul' => MODUL_PERMOHONAN_RLB,
            'url_notif' => 'detail_permohoanan_rlb/',
            'url_redirect' => 'internal/permohonan_rlb/'
        );
        $workflow = WorkflowController::workflowJoss($id, TABLE_PERMOHONAN_RLB, ACTION_WORKFLOW);
        // dd($workflow);
        return view('internal.laporan.rlb.detail_permohonan_rlb', compact('workflow', 'data_workflow', 'permohonan_rlb'));
    }

    public function deletePermohonanRlb($id)
    {
        $permohonan_rlb = PermohonanRLB::findOrFail($id);
        $permohonan_rlb->delete();
        Session::flash('message', 'Delete Permohonan RLB Berhasil Dilakukan!');
        return redirect('internal/permohonan_rlb/');
    }
    #END PERMOHONAN RLB

    #START PERMOHONAN RLS
    public function showPermohonanRls()
    {
        $permohonan_rls = PermohonanRls::all();
        return view('internal.laporan.rls.permohonan_rls', compact('permohonan_rls'));
    }

    public function createPermohonanRls($id = 0)
    {
        $pekerjaan = Pekerjaan::getUnusedRls();
        $mb_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RLB_RLS);
        $dm_bki = User::getAllUserByPermission(PERMISSION_VERIFICATION_RELEASE_RLB_RLS);
        $permohonan_rls = null;
        if ($id > 0) {
            $permohonan_rls = PermohonanRls::find($id);
            $p = Pekerjaan::find(@$permohonan_rls->pekerjaan_id);
            $pekerjaan->push($p);
        }
        // dd($permohonan_rlb);
        return view('internal.laporan.rls.create_permohonan_rls', compact('permohonan_rls', 'pekerjaan', 'mb_bki', 'dm_bki'));
    }

    public function saveLaporanRls(Request $request)
    {
        // dd($request);
        $isUpdate = false;
        if ($request->id != null) {
            $permohonan_rls = PermohonanRls::find($request->id);
            $isUpdate = true;
        } else {
            $permohonan_rls = new PermohonanRls();
        }
        $permohonan_rls->nomor_permintaan_rls = $request->no_permintaan_rls;
        $permohonan_rls->tgl_permintaan = $request->tgl_permintaan_rls;
        $permohonan_rls->manager = $request->manager;
        $permohonan_rls->deputi_manager = $request->deputi_manager;
        $permohonan_rls->pekerjaan_id = $request->pekerjaan_id;
        $permohonan_rls->tgl_inspeksi = $request->tgl_inspeksi;
        $flow_status = WorkflowController::getFlowStatus(CREATED, MODUL_PERMOHONAN_RLS);
        $permohonan_rls->id_flow_status = $flow_status->id;
        $permohonan_rls->save();

        if ($request->id == null) ;
        WorkflowController::createFlowHistory($permohonan_rls->id, $permohonan_rls->getTable(), MODUL_PERMOHONAN_RLS, $flow_status, CREATED);

        $str = "";
        ($isUpdate) ? $str = 'Update' : 'Create';
        Session::flash('message', $str . ' Permohonan RLS Berhasil Dilakukan!');
        return redirect('internal/permohonan_rls/');
    }

    public function detailPermohonanRls($id)
    {
        $permohonan_rls = PermohonanRLS::find($id);
        $data_workflow = array(
            'model' => MODEL_PERMOHONAN_RLS,
            'modul' => MODUL_PERMOHONAN_RLS,
            'url_notif' => 'detail_permohoanan_rls/',
            'url_redirect' => 'internal/permohonan_rls/'
        );
        $workflow = WorkflowController::workflowJoss($id, TABLE_PERMOHONAN_RLS, ACTION_WORKFLOW);
        // dd($workflow);
        return view('internal.laporan.rls.detail_permohonan_rls', compact('workflow', 'data_workflow', 'permohonan_rls'));
    }

    public function deletePermohonanRls($id)
    {
        $permohonan_rls = PermohonanRLS::findOrFail($id);
        $permohonan_rls->delete();
        Session::flash('message', 'Delete Permohonan RLS Berhasil Dilakukan!');
        return redirect('internal/permohonan_rls/');
    }
    #END PERMOHONAN RLS

    #START LAPORAN BERITA ACARA INSPEKSI
    public function beritaInspeksi()
    {
        $ba_inspeksi = BaInspeksi::orderBy('updated_at', 'desc')->get();
        return view('internal.laporan.ba_inspeksi.berita_inspeksi', compact('ba_inspeksi'));
    }

    public function createBeritaInspeksi($id = 0)
    {
        $ba_inspeksi = BaInspeksi::find($id);
        $pekerjaan = Pekerjaan::getUnusedBeritaInspeksi();
        $hasil_inspeksi = ($id == 0) ? null : LaporanPekerjaan::where('pekerjaan_id', $ba_inspeksi->pekerjaan_id)->first();
        $item_arr = array(
            array('Pemeriksaan Dokumen', 'dokumen', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_dokumen : ''),
            array('Pemeriksaan Design', 'design', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_design : ''),
            array('Pemeriksaan Visual', 'visual', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_visual : ''),
            array('Evaluasi Hasil Komisioning', 'komisioning', ($hasil_inspeksi != null) ? $hasil_inspeksi->evaluasi_hasil_komisioning : ''),
            array('Pengujian Sistem sebelum pemberian tegangan', 'uji_tegang', ($hasil_inspeksi != null) ? $hasil_inspeksi->pengujian_sistem : ''),
            array('Pemeriksaan Dampak Lingkungan', 'periksa_dampak', ($hasil_inspeksi != null) ? $hasil_inspeksi->dampak_lingkungan : '')
        );
        return view('internal.laporan.ba_inspeksi.create_berita_inspeksi', compact('id', 'ba_inspeksi', 'pekerjaan', 'item_arr'));
    }

    public function saveBeritaInspeksi(Request $request)
    {
        $id = $request->id;
        $baInspeksi = ($id == 0) ? new BaInspeksi() : BaInspeksi::findOrFail($id);
        $baInspeksi->pekerjaan_id = $request->pekerjaan_id;
        $baInspeksi->hari = $request->hari;
        $baInspeksi->tanggal = $request->tanggal;
        $baInspeksi->tahun_terbilang = $request->tahun_terbilang;
        $baInspeksi->dokumen_pendukung = $request->dokumen_pendukung;
        $baInspeksi->save();

        if ($request->file_berita_acara != null) {
            if ($baInspeksi->file_berita_acara != null) {
                $loc = storage_path() . '/upload/file_rls/' . $baInspeksi->file_berita_acara;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_ba_inspeksi-' . $timestamp . '-' . Auth::user()->email . "." . $request->file_berita_acara->getClientOriginalExtension();
            $url_surat = $name;
            $request->file_berita_acara->move(storage_path() . '/upload/file_ba_inspeksi/', $name);
            $baInspeksi->file_berita_acara = $url_surat;
            $baInspeksi->save();
        }
        return redirect('internal/berita_inspeksi')->with('sucess', 'Berita Acara Inspeksi Berhasil Disimpan');
    }

    public function detailBeritaInspeksi($id)
    {
        $ba_inspeksi = BaInspeksi::findOrFail($id);
        $laporan_pekerjaan = LaporanPekerjaan::where('pekerjaan_id', $ba_inspeksi->pekerjaan_id)->first();
        $hasil_inspeksi = ($id == 0) ? null : LaporanPekerjaan::where('pekerjaan_id', $ba_inspeksi->pekerjaan_id)->first();
        $item_arr = array(
            array('Pemeriksaan Dokumen', 'dokumen', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_dokumen : ''),
            array('Pemeriksaan Design', 'design', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_design : ''),
            array('Pemeriksaan Visual', 'visual', ($hasil_inspeksi != null) ? $hasil_inspeksi->pemeriksaan_visual : ''),
            array('Evaluasi Hasil Komisioning', 'komisioning', ($hasil_inspeksi != null) ? $hasil_inspeksi->evaluasi_hasil_komisioning : ''),
            array('Pengujian Sistem sebelum pemberian tegangan', 'uji_tegang', ($hasil_inspeksi != null) ? $hasil_inspeksi->pengujian_sistem : ''),
            array('Pemeriksaan Dampak Lingkungan', 'periksa_dampak', ($hasil_inspeksi != null) ? $hasil_inspeksi->dampak_lingkungan : '')
        );
        return view('internal.laporan.ba_inspeksi.detail_berita_inspeksi', compact('ba_inspeksi', 'laporan_pekerjaan', 'item_arr'));
    }

    public function destroyBeritaInspeksi($id)
    {
        $ba_inspeksi = BaInspeksi::findOrFail($id);
        $pekerjaan = $ba_inspeksi->pekerjaan->pekerjaan;
        $ba_inspeksi->delete();
        return redirect('internal/berita_inspeksi')->with('sucess', 'Berita Acara ' . $pekerjaan . ' Inspeksi Berhasil Dihapus');
    }

    public function printBeritaInspeksi($id)
    {
        $ba = BaInspeksi::findOrFail($id);
        $dokumenPendukung = explode(',', $ba->dokumen_pendukung);
        $pekerjaan = $ba->pekerjaan;
        $permohonan = $pekerjaan->permohonan;
        $hasil = $pekerjaan->laporan_pekerjaan;
        $filename = 'Berita Acara Inspeksi ' . $pekerjaan->pekerjaan . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord(TEMPLATE_BA_INSPEKSI);
        #set value
        $phpWord->setValue("hari", @$ba->hari);
        $phpWord->setValue("tahunTerbilang", @$ba->tahun_terbilang);
        $phpWord->setValue("tanggal", date('d', strtotime($ba->tanggal)));
        $phpWord->setValue("bulan", date('m', strtotime($ba->tanggal)));
        $phpWord->setValue("tahun", date('Y', strtotime($ba->tanggal)));
        $phpWord->setValue("namaInstalasi", @$permohonan->instalasi->nama_instalasi);
        $phpWord->setValue("pemintaJasa", @$permohonan->user->perusahaan->nama_perusahaan);
        $phpWord->setValue("lokasiInstalasi", @$permohonan->instalasi->alamat_instalasi);
        $phpWord->setValue("tglnspeksi", ($hasil == null) ? '' : date('d-m-Y', strtotime($hasil->tanggal_actual_mulai)));
        $phpWord->setValue("periksaDokumen", ($hasil == null) ? '' : (($hasil->pemeriksaan_dokumen == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("pemeriksaDesain", ($hasil == null) ? '' : (($hasil->pemeriksaan_design == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("periksaVisual", ($hasil == null) ? '' : (($hasil->pemeriksaan_visual == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("evaluasiKomisioning", ($hasil == null) ? '' : (($hasil->evaluasi_hasil_komisioning == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("ujiSistem", ($hasil == null) ? '' : (($hasil->pengujian_sistem == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("periksaDampak", ($hasil == null) ? '' : (($hasil->dampak_lingkungan == 1) ? 'Sesuai' : 'Tidak Sesuai'));
        $phpWord->setValue("periksaKatodik", '');
        $phpWord->cloneRow('dokumenPendukung', sizeof($dokumenPendukung));
        $i = 1;
        $alphas = range('A', 'Z');
        foreach ($dokumenPendukung as $dp) {
            $phpWord->setValue("dokumenPendukung#" . $i, strtolower($alphas[$i - 1]) . '. ' . $dp);
            $i++;
        }
//        $toOpenXML = HTMLtoOpenXML::getInstance()->fromHTML($kick_off->agenda);
//        $phpWord->setValue("agenda", $toOpenXML);
        #create new file (storage/export/*)
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    #END LAPORAN BERITA ACARA INSPEKSI

    #START PRINT RLB

    public function printPermohonanRlb($id)
    {
        $permohonanRlb = PermohonanRlb::findOrFail($id);
        $rlb = $permohonanRlb->rlb;
        $pekerjaan = $permohonanRlb->pekerjaan;
        $permohonan = $pekerjaan->permohonan;
        $instalasi = $permohonan->instalasi;
        $jenis = $instalasi->jenis_instalasi;
        switch ($jenis->tipe_instalasi) {
            case ID_PEMBANGKIT:
                $template = TEMPLATE_PERMOHONAN_RLB_PEMBANGKIT;
                break;
            case ID_DISTRIBUSI:
                $template = TEMPLATE_PERMOHONAN_RLB_DISTRIBUSI;
                break;
            case ID_TRANSMISI:
                $template = TEMPLATE_PERMOHONAN_RLB_TRANSMISI;
                break;
            default :
                $template = "";
                break;
        }
        $filename = 'Permohonan RLB ' . $pekerjaan->id . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord('template_rlb/' . $template);
        #set value
        $phpWord->setValue("nomorPermintaan", $permohonanRlb->nomor_permintaan_rlb);
        $phpWord->setValue("tglPermintaan", $permohonanRlb->tgl_permintaan);
        $phpWord->setValue("namaInstalasi", $instalasi->nama_instalasi);
        $phpWord->setValue("lokasiInstalasi", $instalasi->alamat_instalasi);
        $phpWord->setValue("lingkupInstalasi", $permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan);
        $phpWord->setValue("tglInspeksi", $permohonanRlb->tgl_inspeksi);
        $phpWord->setValue("nama_dm", $permohonanRlb->dm->nama_user);
        $phpWord->setValue("nama_mb", $permohonanRlb->mb->nama_user);
        $phpWord->setValue("nomorSurat", $permohonan->nomor_permohonan);
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    public function printRlb($id)
    {
        $rlb = Rlb::findOrFail($id);
        $pekerjaan = $rlb->pekerjaan;
        $permohonan = $pekerjaan->permohonan;
        $instalasi = $permohonan->instalasi;
        $jenis = $instalasi->jenis_instalasi;
        switch ($jenis->tipe_instalasi) {
            case ID_PEMBANGKIT:
                $template = TEMPLATE_RLB_PEMBANGKIT;
                break;
            case ID_TRANSMISI:
                $template = ($jenis->keterangan == JENIS_GARDU) ? TEMPLATE_RLB_GI : TEMPLATE_RLB_TRANSMISI;
                break;
            default :
                $template = "";
                break;
        }
        $filename = 'RLB ' . $pekerjaan->id . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord('template_rlb/' . $template);
        #set value
        $phpWord->setValue("nomorRekomendasi", $rlb->nomor_rekomendasi);
        $phpWord->setValue("namaPerusahaan", $permohonan->user->perusahaan->nama_perusahaan);
        $phpWord->setValue("tglSuratPermohonan", date('d-m-Y', strtotime($permohonan->tanggal_permohonan)));
        $phpWord->setValue("perihal", '....................');
        $phpWord->setValue("noPermintaan", $rlb->nomor_rekomendasi);
        $phpWord->setValue("tglPermintaan", $rlb->tanggal_terbit);
        $phpWord->setValue("namaInstalasi", $instalasi->nama_instalasi);
        $phpWord->setValue("lingkupPekerjaan", $permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan);
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    #END PRINT RLB

    #START PRINT RLS
    public function printPermohonanRls($id)
    {
        $permohonanRls = PermohonanRlS::findOrFail($id);
        $rls = $permohonanRls->rlb;
        $pekerjaan = $permohonanRls->pekerjaan;
        $permohonan = $pekerjaan->permohonan;
        $instalasi = $permohonan->instalasi;
        $jenis = $instalasi->jenis_instalasi;
        $template = TEMPLATE_PERMOHONAN_RLS;
        $filename = 'Permohonan RLS ' . $pekerjaan->id . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord('template_rls/' . $template);
        #set value
        $phpWord->setValue("nomorPermintaan", $permohonanRls->nomor_permintaan_rls);
        $phpWord->setValue("tglPermintaan", $permohonanRls->tgl_permintaan);
        $phpWord->setValue("namaInstalasi", $instalasi->nama_instalasi);
        $phpWord->setValue("lingkupInstalasi", $permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan);
        $phpWord->setValue("nomorSurat", $permohonan->nomor_permohonan);
        $phpWord->setValue("nama_dm", $permohonanRls->dm->nama_user);
        $phpWord->setValue("nama_mb", $permohonanRls->mb->nama_user);
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }

    public function printRls($id)
    {
        $rls = Rls::findOrFail($id);
        $pekerjaan = $rls->pekerjaan;
        $permohonan = $pekerjaan->permohonan;
        $instalasi = $permohonan->instalasi;
        $jenis = $instalasi->jenis_instalasi;
        $template = TEMPLATE_RLS;
        $filename = 'RLS ' . $pekerjaan->id . '.docx';
        $filename = str_replace('/', '_', $filename);
        #use tamplate (public/Surat_jalan_pasang_baru.docx)
        $phpWord = editWord('template_rls/' . $template);
        #set value
        $phpWord->setValue("nomorRekomendasi", $rls->nomor_rekomendasi);
        $phpWord->setValue("pemintaJasa", $permohonan->user->perusahaan->nama_perusahaan);
        $phpWord->setValue("tglSurat", date('d-m-Y', strtotime($permohonan->tanggal_permohonan)));
        $phpWord->setValue("suratInspeksi", $rls->nomor_rekomendasi);
        $phpWord->setValue("tglSuratInspeksi", $rls->tanggal_terbit);
        $phpWord->setValue("namaInstalasi", $instalasi->nama_instalasi);
        $phpWord->setValue("namaJaringan", ($instalasi->sistemJaringanTransmisi == null) ? "........" : $instalasi->sistemJaringanTransmisi->nama_reference);
        saveWord($phpWord, $filename);
        #prepare for download
        $file = storage_path('exports/' . $filename);
        $header = array(
            'Content-Type' => mime_content_type($file),
            'Content-Disposition' => 'inline; filename="' . $filename . '"',
        );
        #download new file and remove after send
        return response()->download($file, $filename, $header)->deleteFileAfterSend(true);
    }
    #END PRINT RLS


    /*-----------------------------LAPORAN EKSTERNAL-----------------------------------------*/
    #START LAPORAN UNTUK EKSTERNAL
    public function showLaporanEksternal()
    {
        $pekerjaan = Pekerjaan::getAllByPemohon();
        return view('eksternal.laporan.laporan', compact('pekerjaan'));
    }

    public function showLaporanPekerjaanEksternal($id)
    {
        $pekerjaan = Pekerjaan::findOrFail($id);
        $laporan = array();
        $laporan['kick_off'] = LaporanKickOff::where('pekerjaan_id', $id)->first();
        $laporan['data_teknik'] = LaporanDataTeknik::where('pekerjaan_id', $id)->first();
        $laporan['blangko_inspeksi'] = BlangkoInspeksi::where('pekerjaan_id', $id)->first();
        $laporan['berita_inspeksi'] = BaInspeksi::where('pekerjaan_id', $id)->first();
        $laporan['hilo'] = LaporanHilo::where('pekerjaan_id', $id)->first();
        $laporan['rlb'] = Rlb::where('pekerjaan_id', $id)->first();
        $laporan['rls'] = Rls::where('pekerjaan_id', $id)->first();
        return view('eksternal.laporan.laporan_pekerjaan', compact('pekerjaan', 'id', 'laporan'));
    }
    #END LAPORAN UNTUK EKSTERNAL
    /*-----------------------------END LAPORAN EKSTERNAL-------------------------------------*/

    //START LPI (LPI) ======================================================

    public function laporanLPI()
    {
        $lpi = Lpi::orderBy('updated_at', 'desc')->get();
        return view('internal.laporan.lpi.lpi', compact('lpi'));
    }


    public function createLPI($id = 0)
    {
        $lpi = ($id == 0) ? null : Lpi::findOrFail($id);
        $permohonan = @$lpi->permohonan;
        $pekerjaan = Pekerjaan::getUnusedLpi();
        return view('internal.laporan.lpi.create_lpi', compact('lpi', 'id', 'pekerjaan'));
    }

    public function saveLPI(Request $request)
    {
        $id = $request->id;
        $pekerjaan = Pekerjaan::find($request->pekerjaan_id);
        $lpi = ($id == 0) ? new Lpi() : Lpi::findOrFail($id);
        $lpi->nomor = $request->nomor;
        $lpi->tanggal_laporan = $request->tanggal_laporan;
        $lpi->keterangan = $request->keterangan;
        $file = $request->hasil_lpi;
        if ($file != null) {
            if ($lpi->file_lpi != null) {
                $loc = storage_path() . '/upload/file_lpi/' . $lpi->file_lpi;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_lpi-' . $timestamp . '-' . Auth::user()->email . "." . $request->hasil_lpi->getClientOriginalExtension();
            $url_surat = $name;
            $request->hasil_lpi->move(storage_path() . '/upload/file_lpi/', $name);
            $lpi->file_lpi = $url_surat;
        }
        if ($id == 0) {
            $lpi->pekerjaan_id = $request->pekerjaan_id;
            $lpi->permohonan_id = @$pekerjaan->permohonan_id;
            $lpi->instalasi_id = @$pekerjaan->permohonan->id_instalasi;
            $lpi->jenis_instalasi_id = @$pekerjaan->permohonan->instalasi->id_jenis_instalasi;
            $lpi->perusahaan_id = @$pekerjaan->permohonan->order->id_perusahaan;
            $lpi->alamat_perusahaan = @$pekerjaan->permohonan->order->perusahaan->alamat_perusahaan;
            $lpi->tipe_instalasi_id = @$pekerjaan->permohonan->instalasi->jenis_instalasi->tipe_instalasi;
            if (@$pekerjaan->permohonan->instalasi->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                $lpi->bay_id = @$pekerjaan->permohonan->bayPermohonan[0]->bay_id;
            }
        }
        $lpi->save();
        return redirect('internal/lpi')->with('sucess', 'Data LPI Berhasil Disimpan');
    }

    public function detailLPI($id = 0)
    {
        $lpi = ($id == 0) ? null : Lpi::findOrFail($id);
        return view('internal.laporan.lpi.detail_lpi', compact('lpi', 'id'));
    }

    public function destroyLPI($id = 0)
    {
        $lpi = Lpi::findOrFail($id);
        $lpi->delete();
        return redirect('internal/lpi')->with('success', 'Data LPI Berhasil Dihapus.');
    }
    //END LPI(LPI) ========================================================

}
