<?php

namespace App\Http\Controllers\Auth;

use Adldap\Adldap;
use Adldap\Connections\Configuration;
use Adldap\Exceptions\AdldapException;
use App\Group;
use App\Http\Controllers\LogController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Mockery\CountValidator\Exception;
use Validator;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/';
    protected $redirectAfterLogout = '/auth/login';
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        /*dd('LDAP');*/
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function validateLDAP(Request $request)
    {
//        dd($request);
        $credentials = $this->getCredentials($request);
        /*Begin Validate LDAP*/

        $user = null;
        //kebutuhan active directory
        $username_ad = $request->get('username');
        $password_ad = $request->get('password');
        //disimpan temporary  jika yg login eksternal
        $username = $request->get('username');
        $password = $request->get('password');
        $detail = explode("\\", $username_ad);
        if (sizeof($detail) > 1) {
            //maka user pln (pakai domain)
            $domain = strtolower(substr($username_ad, 0, strrpos($username_ad, "\\")));
            $username_ad = substr($username_ad, strrpos($username_ad, "\\") + 1, strlen($username_ad) - strrpos($username_ad, "\\"));
        } else {
            $domain = "";
        }
        $credentials = ['username' => $username_ad, 'password' => $password_ad];

        $auth_from_ad = true;
        if (env('ACTIVE_DIRECTORY', 'true')) {

            if ($domain == 'pusat') {
                $user_admin = 'simpus';
                $pass_admin = 'P@ssw0rd123#';
            } elseif ($domain == 'sutg') {
                $user_admin = 'sutg.simpus';
                $pass_admin = 'P@ssw0rd#123';
            } elseif ($domain == 'bali') {
                $user_admin = 'simpusbali';
                $pass_admin = 'simpusbali123';
            } elseif ($domain == 'nad' || $domain == 'aceh') {
                $user_admin = 'aceh.simpus';
                $pass_admin = 'P@ssw0rd#123';
                $domain = 'nad';
            } elseif ($domain == 'jatim') {
                $user_admin = 'simpusjatim';
                $pass_admin = 'P@ssw0rd';
            } else {
                $user_admin = 'simpus';
                $pass_admin = 'P@ssw0rd123#';
            }

//        Begin Validate LDAP
            $config = new Configuration();
	    if($domain == ""){
		$domain = 'na';
	    }
            $config->setAccountSuffix('@' . $domain . '.corp.pln.co.id');
            $config->setDomainControllers([$domain . '.corp.pln.co.id']);
            $config->setBaseDn('DC=' . $domain . ',DC=corp,DC=pln,DC=co,DC=id');

            $config->setPort(389);

            $config->setAdminUsername($user_admin);
            $config->setAdminPassword($pass_admin);


            $config->setFollowReferrals(false);
            $config->setUseSSL(false);
            $config->setUseTLS(false);
            $config->setUseSSO(false);

            $auth_from_ad = true;

            /*Try connect to Active Directory Server*/
            try {
                $ad = new Adldap($config);
            } /*If not connect to Active Directory*/
            catch (adLDAPException $e) {
                /*If username not registered yet*/
                $user = User::where('username', $username)->orWhere('email', $username)->orWhere('username', $username_ad);
                if (sizeof($user) >= 0) {
                    $auth_from_ad = false;

                } else {
                    /*return 'cannot connect';*/
                    return redirect('/auth/login')
                        ->with('status', 'Can\'t connect to Active Directory Server. Try again later or report to your administrator.');
                }
            }

            /*If Auth from Acttive Directory success and username not authenticate*/
            if ($auth_from_ad && !$ad->authenticate($username_ad, $password_ad)) {
                $auth_from_ad = false;
            }

            /*If auth from active directory success and username authenticate*/
            if ($auth_from_ad) {
                $user_ad = $ad->users()->find($username_ad);
                $user = null;
                /*Cek Tabel User, jika tidak ada -> ditolak, jika ada -> update data.*/
                if (User::withTrashed()->where('username', '=', $username_ad)->count() > 0) {
                    /*User lama*/
                    $user = User::withTrashed()->where('username', '=', $username_ad)->first();
                    /*User inactive*/
                    if ($user->deleted_at != '') {
                        return redirect('/auth/login')->with('status', 'User account is no longer active.');
                    }
                } else {
                    return redirect('/auth/login')->with('status', 'Your account has not been registered. Please contact admin');
                }
                if ($user != null) {
                    /*update or create*/
                    $user->username = $username_ad;
                    $user->nip_user = $user_ad->getAttribute('employeenumber')[0];
                    $user->nama_user = $user_ad->getAttribute('displayname')[0];
                    $user->password = Hash::make($password_ad);
                    $user->is_user_ad = 1;
                    $user->ad_display_name = $user_ad->getAttribute('displayname')[0];
                    $user->ad_mail = $user_ad->getAttribute('mail')[0];
                    $user->ad_company = $user_ad->getAttribute('company')[0];
                    $user->ad_department = $user_ad->getAttribute('department')[0];
                    $user->ad_title = $user_ad->getAttribute('title')[0];
                    $user->ad_employee_number = $user_ad->getAttribute('employeenumber')[0];
                    $user->save();
                }
            }

        }
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        // $credentials = $this->getCredentials($request);
        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        } else {
            if ($user == null) {
                $user = User::where('username', '=', $username_ad);
            } else {
                $user = $user->where('username', '=', $username_ad);
            }
            if (sizeof($user->get()) > 0) {
                //user exist but wrong password
                $user = $user->first();
                if ($user->password != "") {
                    if (!Hash::check($password, $user->password)) {
                        return redirect('/auth/login')->with('status', 'User account or password is incorrect');
                    }
                } else {
                    return redirect('/auth/login')->with('status', 'Your account has not been approved. Please contact admin.');
                }
            } else {
                return redirect('/auth/login')->with('status', 'Your account has not been registered. Please contact admin');
            }
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    public function getLogout()
    {
//        LogController::storeActivity('Logout','AUTHENTICATION' , 'User logout dari aplikasi.');
        Auth::logout();
        Session::flush();
        return redirect('/auth/login');
    }

}
