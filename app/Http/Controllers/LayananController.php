<?php

namespace App\Http\Controllers;

use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\InstalasiPembangkit;
use App\InstalasiTransmisi;
use App\Produk;
use App\TipeInstalasi;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pilihInstalasi($id_produk, $id_tipe_instalasi){

        $layanan            = Produk::findOrFail($id_produk);
        $tipe_instalasi     = TipeInstalasi::findOrFail($id_tipe_instalasi);

        if($id_tipe_instalasi==1) $instalasi = InstalasiPembangkit::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        elseif($id_tipe_instalasi==2) $instalasi = InstalasiTransmisi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        elseif($id_tipe_instalasi==3) $instalasi = InstalasiDistribusi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        elseif($id_tipe_instalasi==4) $instalasi = InstalasiPemanfaatanTT::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        elseif($id_tipe_instalasi==5) $instalasi = InstalasiPemanfaatanTM::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();

        $url_create = [
            '1'=>'create_instalasi_pembangkit',
            '2'=>'create_instalasi_transmisi',
            '3'=>'create_instalasi_distribusi',
            '4'=>'create_instalasi_pemanfaatan_tt',
            '5'=>'create_instalasi_pemanfaatan_tm'
        ];

        return view('eksternal/produk_layanan', compact('layanan', 'tipe_instalasi', 'instalasi', 'url_create'));
    }

    public function detailInstalasi($id_produk, $id_tipe_instalasi, $id_instalasi){

        $selected_layanan   = Produk::findOrFail($id_produk);
        $tipe_instalasi     = TipeInstalasi::findOrFail($id_tipe_instalasi);
        $lingkup_pekerjaan  = $tipe_instalasi->lingkup_pekerjaan()->get();
        $layanan            = $tipe_instalasi->produk()->get();

        if($id_tipe_instalasi==1){
            $instalasi  = InstalasiPembangkit::findOrFail($id_instalasi);
            $view       = 'detail_instalasi_pembangkit';
        }
        elseif($id_tipe_instalasi==2) {
            $instalasi = InstalasiTransmisi::findOrFail($id_instalasi);
            $view       = 'detail_instalasi_transmisi';
        }
        elseif($id_tipe_instalasi==3) {
            $instalasi = InstalasiDistribusi::findOrFail($id_instalasi);
            $view       = 'detail_instalasi_distribusi';
        }
        elseif($id_tipe_instalasi==4) {
            $instalasi = InstalasiPemanfaatanTT::findOrFail($id_instalasi);
            $view       = 'detail_instalasi_pemanfaatan_tt';
        }
        elseif($id_tipe_instalasi==5) {
            $instalasi = InstalasiPemanfaatanTM::findOrFail($id_instalasi);
            $view       = 'detail_instalasi_pemanfaatan_tm';
        }
        else{
            return redirect('/eksternal')->with('error','Instalasi tidak terdaftar');
        }
        return view('eksternal/'.$view, compact('instalasi','layanan', 'selected_layanan', 'tipe_instalasi', 'lingkup_pekerjaan'));



    }
}
