<?php

namespace App\Http\Controllers;

use App\Kontrak;
use App\Notification;
use App\RABDetail;
use App\RABPermohonan;
use App\TipeInstalasi;
use App\Tracking;
use App\UnsurBiaya;
use App\User;
use App\Workflow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Order;
use App\Permohonan;
use App\RAB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;

class RABController extends Controller
{

    public function showRancanganBiaya()
    {
        $rancangan_biaya = RAB::join('orders', 'orders.id', '=', 'rab.order_id')->orderBy('orders.updated_at', 'desc')->get();
        // dd($rancangan_biaya);
        return view('internal.rab.rab', compact('rancangan_biaya'));
    }

    public function createRancanganBiaya2($order_id, $rab_id = 0)
    {
        return $this->formRancanganBiaya($order_id, $rab_id, 'edit');
    }

    public function detailRancanganBiaya($order_id, $rab_id = 0)
    {
        return $this->formRancanganBiaya($order_id, $rab_id, 'detail');
    }

    function formRancanganBiaya($order_id, $rab_id, $mode = "edit")
    {
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        $rab = RAB::find($rab_id);
        $detail_rab = ($rab != null) ? $rab->detailRab : array();
        // dd($detail_rab);
        $order = Order::find($order_id);
        $jenis_user = $order->user->jenis_user;

        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
        }
        #menyimpan permohonan yg sudah dipakai di RAB lain atau di RAB saat ini
        $used_perm = array();
        foreach ($permohonan as $p) {
            if ($p->rab != null && $p->rab->rab != null) {
                if ($p->rab->rab_id == $rab_id) {
                    $p->status = "curr";
                } else {
                    $p->status = "used";
                }
                array_push($used_perm, $p);
            }
        }

        $workflow = WorkflowController::workflowJoss($order_id, TABLE_ORDER);
        $view = ($mode == "edit") ? "create_form_rab2" : "view_form_rab";
        return view('internal.rab.' . $view, compact(
            'workflow', 'order_id', 'jenis', 'unsur_arr', 'rab', 'detail_rab', 'permohonan', 'used_perm', 'jenis_user'));

    }

    public function storeRancanganBiaya2(Request $request)
    {
//             dd($request);
        $permohonan_id = $request->pilih_permohonan;
        $order_id = $request->order_id;
        $rab_id = $request->rab_id;
        $order = Order::find($order_id);
        $flow_status = WorkflowController::getFlowStatusId($request->status, MODUL_RAB);
        $noRAB = true;
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        foreach ($permohonan as $p) {
            if ($p->rab != null) {
                $noRAB = false;
                break;
            }
        }

        if ($noRAB && $flow_status != null && $request->status != "") {
            $order->id_flow_status = $request->status;
            $order->save();
            WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_RAB, $flow_status, CREATED);
        }

        # 1. insert into tb RAB
        if ($rab_id > 0) {
            $rab = RAB::findOrFail($rab_id);
            $del_permohonan = $rab->permohonan;
            foreach ($del_permohonan as $del) {
                $del->delete();
            }

        } else {
            $rab = new RAB();
            $rab->order_id = $order_id;

            if (date("j") == 1 && date("n") == 1) {
                $nomor_urut = 1;
            } else {
                $nomor_urut = RAB::whereYear('created_at', '=', date('Y'))->count() + 1;
            }

            $rab->no_dokumen = str_pad($nomor_urut, 6, "0", STR_PAD_LEFT) . "-" . str_pad($order_id, 6, "0", STR_PAD_LEFT) . "-" . date('Y');
        }

        $rab->jumlah_biaya = $request->total_biaya;
        $rab->ppn = $request->total_ppn;
        $rab->total_biaya = $request->total_bayar;
        $rab->file_rab = $this->saveFile($request->file_rab, 'file_rab', $order_id);
        $rab->save();

        # 2. insert into tb RAB_PERMOHONAN
        for ($i = 0; $i < sizeof($permohonan_id); $i++) {
            $rab_permohonan = new RABPermohonan();
            $rab_permohonan->permohonan_id = $permohonan_id[$i];
            $rab_permohonan->rab_id = $rab->id;
            $rab_permohonan->save();
        }

        # 3. insert into tb RAB_DETAIL
        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        $idx_unsur = 0;
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
            $unsur = $request->input($jenis[$key]['div_id'] . "_id");
            $hari = $request->input("hari_" . $idx_unsur);
            $orang = $request->input("orang_" . $idx_unsur);
            $tarif = $request->input("tarif_" . $idx_unsur);
            $jumlah = $request->input("jumlah_" . $idx_unsur);
            $jumlah_biaya = $request->input("jumlah_biaya_" . $idx_unsur);
            $rab_detail_id = $request->input("rab_detail_" . $jenis[$key]['div_id'] . "_id");
            for ($i = 0; $i < sizeof($unsur); $i++) {
                $detail_id = $rab_detail_id[$i];
                $rabDetail = ($detail_id == "") ? new RABDetail() : RABDetail::find($detail_id);
                $rabDetail->id_unsur_biaya = $unsur[$i];
                $rabDetail->id_rab = $rab->id;
                $rabDetail->kantor_hari = $hari[$i];
                $rabDetail->kantor_orang = $orang[$i];
                $rabDetail->tarif = $tarif[$i];
                $rabDetail->jumlah = $jumlah[$i];
                $rabDetail->jumlah_biaya = $jumlah_biaya[$i];
                $rabDetail->save();
            }
            $idx_unsur++;
        }

        #create tracking
        $tracking = Tracking::where('order_id', $order_id)->get();
        if ($tracking != null) {
            foreach ($tracking as $item) {
                $item->rab_id = $rab->id;
                $item->status = STATUS_RAB;
                $item->save();
            }
        }
        #---------------

        return redirect('/internal/rancangan_biaya_order/' . $order_id)->with('success', 'Data RAB berhasil disimpan.');
    }

    public function deleteRancanganBiaya($order_id, $id)
    {
        $rab = RAB::find($id);
        $rab->delete();
        $rab_permohonan = RABPermohonan::where('rab_id', $id);
        $rab_permohonan->delete();
        return redirect('/internal/rancangan_biaya_order/' . $order_id);
    }

    public function cetakRancanganBiaya()
    { //format: .xls

    }

    public function viewRABEksternal($order_id, $rab_id = 0)
    {
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        $rab = RAB::find($rab_id);
        $detail_rab = ($rab != null) ? $rab->detailRab : array();

        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
        }
        #menyimpan permohonan yg sudah dipakai di RAB lain atau di RAB saat ini
        $used_perm = array();
        foreach ($permohonan as $p) {
            if ($p->rab != null && $p->rab->rab != null) {
                if ($p->rab->rab_id == $rab_id) {
                    $p->status = "curr";
                    array_push($used_perm, $p);
                } else {
                    $p->status = "used";
                }
                array_push($used_perm, $p);
            }
        }

        return view('eksternal.view_rab', compact(
            'order_id', 'jenis', 'unsur_arr', 'rab', 'detail_rab', 'permohonan', 'used_perm'));
    }

    /*START Modul RAB*/
    #INTERNAL
    public function rancanganBiayaOrder($order_id)
    {
        $order = Order::findOrFail($order_id);
        $rab = RAB::where('order_id', $order_id)->get();
        $is_complete = RAB::cek_permohonan($order_id)->id_count;
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        $jumlah = 0;
        foreach ($permohonan as $p) {
            if ($p->rab == null) {
                $jumlah++;
            }
        }
        $data_workflow = array(
            'model' => MODEL_ORDER,
            'modul' => MODUL_RAB,
            'url_notif' => 'rancangan_biaya_order/',
            'url_redirect' => 'internal/rab/'
        );
        $workflow = WorkflowController::workflowJoss($order_id, $order->getTable(), ACTION_WORKFLOW);
        $flow_completed = (Order::getNextWorkflowStep($order, MODUL_RAB)->count() == 0) ? true : false;
        $rab_kontrak_unused = ($flow_completed == false) ? array() : Kontrak::getRabUnusedInKontrak($order);
        return view('internal.rab.rab_order', compact('order_id', 'data_workflow', 'order', 'rab', 'workflow', 'is_complete', 'jumlah', 'flow_completed', 'rab_kontrak_unused'));
    }

    public function internalApprovalRab(Request $request)
    {
        $status = $request->status;
        $keterangan = $request->keterangan;
        $order_id = $request->id;
        $surat_penawaran = $request->surat_penawaran;
        $url_surat = "";

        #get surat penawaran
        $no_surat = $request->no_surat;
        $tgl_surat = $request->tgl_surat;

        $order = Order::findOrFail($order_id);

        $flow_status = WorkflowController::getFlowStatusId($status, MODUL_RAB);

        if ($flow_status->tipe_status == SENT) {
            if ($surat_penawaran != null) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'surat_penawaran_rab-' . $timestamp . '-' . Auth::user()->email . "." . $surat_penawaran->getClientOriginalExtension();
                $url_surat = $name;
                $surat_penawaran->move(storage_path() . '/upload/surat_penawaran_rab/', $name);
            }
        }
        if ($flow_status->tipe_status == SENT || $flow_status->tipe_status == SUBMITTED) {
            $keterangan = SUBMITTED;
        }
        $order->id_flow_status = ($flow_status != null) ? $flow_status->id : null;

        #simpan data surat
        $order->file_spnw = $url_surat;
        $order->nomor_spnw = $no_surat;
        $order->tanggal_spnw = $tgl_surat;
        $order->save();

        $current_wf = WorkflowController::getCurrentWorkflow($flow_status->id, MODUL_RAB);
        $users = WorkflowController::getAllNextUsers($current_wf->id);

        #isi data notif
        #create object notif
        if (isset($users)) {
            foreach ($users as $item) {
                $jenis_user = ($item->user->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
                $notif = new Notification();
                $notif->from = Auth::user()->username;
                $notif->url = strtolower($jenis_user) . "/rancangan_biaya_order/" . $order_id;
                $notif->subject = "RAB " . $flow_status->tipe_status;
                $notif->to = $item->user->username;
                $notif->status = "UNREAD";
                $notif->message = "RAB " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
                $notif->icon = "icon-briefcase";
                $notif->color = "btn-green";
                $notif->save();
            }
        }

        WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_RAB, $flow_status, $keterangan, $url_surat);
        return redirect('internal/order');
    }

    #EKTERNAL
    public function rancanganBiayaEksternal($order_id)
    {
        $order = Order::findOrFail($order_id);
        $rab = $order->rab;
        //$workflow = WorkflowController::getFlowHistory($order_id, MODUL_RAB);
        $data_workflow = array(
            'model' => MODEL_ORDER,
            'modul' => MODUL_RAB,
            'url_notif' => 'rancangan_biaya_order/',
            'url_redirect' => 'internal/rab/'
        );
        $workflow = WorkflowController::workflowJoss($order_id, $order->getTable(), ACTION_EKSTERNAL_RAB, TIPE_ALL);
        $workflow["penolakan_rab"] = true;

        //cek apakah sudah melengkap SKKI,SKKO,PRK
        $validSkkiSkko = true;
        if (User::isUnitPLN(Auth::user())) {
            foreach ($rab as $item) {
                if ($item->no_skki == "" && $item->no_skko == "") {
                    $validSkkiSkko = false;
                    break;
                }
            }
        }
        //ambil instalasi terkait di setiap RAB
        for($i = 0 ; $i < sizeof($rab) ; $i++) {
            $permohonan = $rab[$i]->permohonan;
            $nama_instalasi = array();
            foreach ($permohonan as $row){
                array_push($nama_instalasi,$row->permohonan->instalasi->nama_instalasi);
            }
            $rab[$i]->nama_instalasi = collect($nama_instalasi)->unique();
        }

        $flow_completed = (Order::getNextWorkflowStep($order, MODUL_RAB)->count() == 0) ? true : false;
        return view('eksternal/rab_order', compact('data_workflow', 'workflow', 'order', 'rab', 'flow_order', 'validSkkiSkko', 'flow_completed'));
    }

    public function editRancanganBiayaEksternal($order_id)
    {
        $order = Order::findOrFail($order_id);
        $rab = RAB::where('order_id', $order_id)->get();
        return view('eksternal/form_rab_order', compact('data_workflow', 'workflow', 'order', 'rab', 'flow_order'));
    }

    public function storeRancanganBiayaEksternal(Request $request)
    {
        $order_id = $request->order_id;
        $skki = $request->skki;
        $skko = $request->skko;
        $prk = $request->prk;
        $order = Order::findOrFail($order_id);
        $rab = $order->rab;
        $i = 0;
        foreach ($rab as $item) {
            $item->no_skki = $skki[$i];
            $item->no_skko = $skko[$i];
            $item->no_prk = $prk[$i];
            $item->save();
            $i++;
        }
        return redirect('/eksternal/view_order')->with('success', 'Data SKKI/SKKO untuk RAB berhasil disimpan.');
    }

    public function eksternalUploadSuratPenolakan(Request $request)
    {
        $order_id = $request->order_id;
        $order = Order::findOrFail($order_id);
        $surat_tolak = $request->surat_penolakan;
        if ($surat_tolak != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_penolakan_rab-' . $timestamp . '-' . Auth::user()->email . "." . $surat_tolak->getClientOriginalExtension();
            $url_surat = $name;
            $surat_tolak->move(storage_path() . '/upload/surat_penolakan_rab/', $name);
            $order->file_stlkrab = $url_surat;
            $order->save();
        }
        return redirect('/eksternal/rancangan_biaya_order/' . $order_id)->with('success', 'Surat penolakan RAB berhasil disimpan.');

    }

    public function eksternalApprovalRab(Request $request)
    {
        $status = $request->status;
        $keterangan = $request->keterangan;
        $order_id = $request->id;
        $order = Order::findOrFail($order_id);
        $flow_status = WorkflowController::getFlowStatusId($status, MODUL_RAB);
        $order->id_flow_status = ($flow_status != null) ? $flow_status->id : null;
        $order->save();

        if ($flow_status->tipe_status == APPROVED) {
            //jika RAB di approve oleh peminta jasa maka set sys status order dan permohonan menjadi RAB

            //set sys status order
            $order->sys_status = MODUL_RAB;
            $order->save();

            //Ubah system status order dan permohonan menjadi RAB ketika RAB diapprove
            $rab = RAB::where('order_id', $order_id)->get();
            foreach ($rab as $item) {
                foreach ($item->permohonan as $perm) {
                    $permohonan = $perm->permohonan;
                    $permohonan->status = MODUL_RAB;
                    $permohonan->save();
                }
            }
        }

        #notification pada pada aplikasi
        $verificators = User::getAllUserByPermission(PERMISSION_CREATE_RAB);
        //        foreach (User::getUserListFromRole('P-BPU') as $user_role) {
        foreach ($verificators as $user_role) {
            $notif = new Notification();
            $notif->from = Auth::user()->username;
            $notif->url = "internal/rancangan_biaya_order/" . $order_id;
            $notif->to = $user_role->username;
            $notif->subject = "RAB " . $flow_status->tipe_status;
            $notif->status = "UNREAD";
            $notif->message = "RAB " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
            $notif->icon = "icon-briefcase";
            $notif->color = "btn-green";
            $notif->save();
        }

        WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_RAB, $flow_status, $keterangan);
        return redirect('/eksternal/view_order');
    }

    public function downloadExcel($order_id, $rab_id = 0)
    {
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        $rab = RAB::find($rab_id);
        $detail_rab = ($rab != null) ? $rab->detailRab : array();

        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
        }
        #menyimpan permohonan yg sudah dipakai di RAB lain atau di RAB saat ini
        $used_perm = array();
        foreach ($permohonan as $p) {
            if ($p->rab != null && $p->rab->rab != null) {
                if ($p->rab->rab_id == $rab_id) {
                    $p->status = "curr";
                } else {
                    $p->status = "used";
                }
                array_push($used_perm, $p);
            }
        }

        $data = [
            'order_id' => $order_id,
            'jenis' => $jenis,
            'unsur_arr' => $unsur_arr,
            'rab' => $rab,
            'detail_rab' => $detail_rab,
            'permohonan' => $permohonan,
            'used_perm' => $used_perm
        ];
        Excel::create('RAB-' . $rab->id . '-' . date('Y-m-d'), function ($excel) use ($data) {
            $excel->sheet('New sheet', function ($sheet) use ($data) {

                $sheet->loadView('internal.rab.rab_export_excel', $data)
                    ->mergeCells('A1:A2')
                    ->mergeCells('B1:C2')
                    ->mergeCells('D1:D2')
                    ->mergeCells('G1:G2')
                    ->mergeCells('H1:H2')
                    ->mergeCells('I1:I2')
                    ->setCellValue('E2', "HARI")
                    ->setCellValue('F2', "ORANG")
                    ->setBorder('A1:I41', 'thin', "D8572C");

                $sheet->setSize('A1', 5);
                $sheet->setSize('B1', 5);
                $sheet->setSize('C1', 50);
                $sheet->setSize('D1', 30);
                $sheet->setSize('I1', 30);


                $sheet->cell('A1:I2', function ($cell) {
                    $cell->setFont(array(
                        'size' => '12',
                        'bold' => true
                    ));
                    $cell->setValignment('center');
                    $cell->setAlignment('center');
                    $cell->setBackground('#D3D3D3');
                });

                $sheet->cell('A39:I41', function ($cell) {
                    $cell->setFont(array(
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->setColumnFormat(array(
                    'D4:D38' => '#,##0',
                    'I4:I41' => '#,##0'
                ));
            });
        })->download('xls');


        //        return view('internal.rab_export_excel', compact(
        //            'order_id', 'jenis', 'unsur_arr', 'rab', 'detail_rab', 'permohonan', 'used_perm'));
    }

    /*END Modeul RAB*/

    public function saveSuratPenawaran(Request $request)
    {
        $order_id = $request->order_id;
        $no_surat = $request->no_surat;
        $tgl_surat = $request->tgl_surat;
        $surat_penawaran = $request->surat_penawaran;

        $order = Order::findOrFail($order_id);
        if ($surat_penawaran != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_penawaran_rab-' . $timestamp . '-' . Auth::user()->email . "." . $surat_penawaran->getClientOriginalExtension();
            $url_surat = $name;
            $surat_penawaran->move(storage_path() . '/upload/surat_penawaran_rab/', $name);
            $order->file_spnw = $url_surat;
        }

        $order->nomor_spnw = $no_surat;
        $order->tanggal_spnw = $tgl_surat;
        $order->save();

        return redirect('/internal/rancangan_biaya_order/' . $order_id)->with('success', 'Berhasil menyimpan surat penawaran.');
    }


    public function eksternalUploadSuratPenugasan(Request $request)
    {
        $order_id = $request->order_id;
        $order = Order::findOrFail($order_id);
        $surat_penugasan = $request->surat_penugasan;
        if ($surat_penugasan != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_penugasan_rab-' . $timestamp . '-' . Auth::user()->email . "." . $surat_penugasan->getClientOriginalExtension();
            $url_surat = $name;
            $surat_penugasan->move(storage_path() . '/upload/surat_penugasan_rab/', $name);
            $order->file_penugasan = $url_surat;
            $order->save();
        }
        return redirect('/eksternal/rancangan_biaya_order/' . $order_id)->with('success', 'Surat penugasan berhasil disimpan.');

    }

    public function AmandemenRancanganBiaya($order_id, $rab_id)
    {
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        $rab = RAB::find($rab_id);
        $detail_rab = ($rab != null) ? $rab->detailRab : array();

        $order = Order::find($order_id);
        $jenis_user = $order->user->jenis_user;

        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
        }
        #menyimpan permohonan yg sudah dipakai di RAB lain atau di RAB saat ini
        $used_perm = array();
        foreach ($permohonan as $p) {
            if ($p->rab != null && $p->rab->rab != null) {
                if ($p->rab->rab_id == $rab_id) {
                    $p->status = "curr";
                } else {
                    $p->status = "used";
                }
                array_push($used_perm, $p);
            }
        }

        $view = "amandemen_form_rab";
        return view('internal.rab.' . $view, compact(
            'workflow', 'order_id', 'jenis', 'unsur_arr', 'rab', 'detail_rab', 'permohonan', 'used_perm', 'jenis_user'));
    }

    public function StoreAmandemen(Request $request)
    {
        // dd($request);
        $permohonan_id = $request->pilih_permohonan;
        $order_id = $request->order_id;
        $rab_id = $request->rab_id;
        $request->status = 29; #belum nemu solusi untuk status amandemen, sementara hardcode

        $order = Order::find($order_id);
        $flow_status = WorkflowController::getFlowStatusId($request->status, MODUL_RAB);
        $noRAB = true;
        $permohonan = Permohonan::where('id_orders', $order_id)->get();
        foreach ($permohonan as $p) {
            if ($p->rab != null) {
                $noRAB = false;
            }
        }

        if ($noRAB) {
            $order->id_flow_status = $request->status;
            $order->save();
            WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_RAB, $flow_status, CREATED);
        }

        # 1. insert into tb RAB
        if ($rab_id > 0) {
            $rab = RAB::findOrFail($rab_id);
            $del_permohonan = $rab->permohonan;
            foreach ($del_permohonan as $del) {
                $del->delete();
            }

        } else {
            $rab = new RAB();
            $rab->order_id = $order_id;

            if (date("j") == 1 && date("n") == 1) {
                $nomor_urut = 1;
            } else {
                $nomor_urut = RAB::whereYear('created_at', '=', date('Y'))->count() + 1;
            }

            $rab->no_dokumen = str_pad($nomor_urut, 6, "0", STR_PAD_LEFT) . "-" . str_pad($order_id, 6, "0", STR_PAD_LEFT) . "-" . date('Y');
        }

        $rab->jumlah_biaya = $request->total_biaya;
        $rab->ppn = $request->total_ppn;
        $rab->total_biaya = $request->total_bayar;
        $rab->save();

        # 2. insert into tb RAB_PERMOHONAN
        for ($i = 0; $i < sizeof($permohonan_id); $i++) {
            $rab_permohonan = new RABPermohonan();
            $rab_permohonan->permohonan_id = $permohonan_id[$i];
            $rab_permohonan->rab_id = $rab->id;
            $rab_permohonan->save();
        }

        # 3. insert into tb RAB_DETAIL
        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        $idx_unsur = 0;
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
            $unsur = $request->input($jenis[$key]['div_id'] . "_id");
            $hari = $request->input("hari_" . $idx_unsur);
            $orang = $request->input("orang_" . $idx_unsur);
            $tarif = $request->input("tarif_" . $idx_unsur);
            $jumlah = $request->input("jumlah_" . $idx_unsur);
            $jumlah_biaya = $request->input("jumlah_biaya_" . $idx_unsur);
            $rab_detail_id = $request->input("rab_detail_" . $jenis[$key]['div_id'] . "_id");
            for ($i = 0; $i < sizeof($unsur); $i++) {
                $detail_id = $rab_detail_id[$i];
                $rabDetail = ($detail_id == "") ? new RABDetail() : RABDetail::find($detail_id);
                $rabDetail->id_unsur_biaya = $unsur[$i];
                $rabDetail->id_rab = $rab->id;
                $rabDetail->kantor_hari = $hari[$i];
                $rabDetail->kantor_orang = $orang[$i];
                $rabDetail->tarif = $tarif[$i];
                $rabDetail->jumlah = $jumlah[$i];
                $rabDetail->jumlah_biaya = $jumlah_biaya[$i];
                $rabDetail->save();
            }
            $idx_unsur++;
        }

        return redirect('internal/rancangan_biaya_order/' . $order_id);
    }

    public function saveFile($req_file, $param_file, $order_id)
    {
        $timestamp = date('YmdHis');
        $nm_u_custom = '-' . $timestamp . '-' . $order_id . '.';
        $filename = $param_file . $nm_u_custom . $req_file->getClientOriginalExtension();
        $fullPath = storage_path() . '/upload' . '/' . $param_file . '/' . $filename;
        if (File::exists($fullPath))
            File::delete($fullPath);
        $req_file->move(storage_path() . '/upload' . '/' . $param_file, $filename);

        return $filename;
    }
}
