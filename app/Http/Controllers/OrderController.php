<?php

namespace App\Http\Controllers;

use App\AreaProgram;
use App\BayGardu;
use App\BayPermohonan;
use App\Cities;
use App\FlowStatus;
use App\FormFields;
use App\FormLingkup;
use App\GarduArea;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\InstalasiPembangkit;
use App\InstalasiPermohonan;
use App\InstalasiTransmisi;
use App\JenisInstalasi;
use App\JenisPekerjaan;
use App\KombinasiPekerjaan;
use App\Kontrak;
use App\LayananSuratTugas;
use App\LingkupDistribusi;
use App\LingkupPekerjaan;
use App\LingkupProgram;
use App\LokasiArea;
use App\Notification;
use App\Order;
use App\PekerjaanPermohonan;
use App\PemilikInstalasi;
use App\PenyulangArea;
use App\Permohonan;
use App\PermohonanLingkup;
use App\Perusahaan;
use App\Program;
use App\Provinces;
use App\RAB;
use App\RABDetail;
use App\RABPermohonan;
use App\References;
use App\RoleUser;
use App\SuratTugas;
use App\TipeInstalasi;
use App\Tracking;
use App\UnsurBiaya;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    /* =======================START CRUD ORDER================================ */

    public function showOrder()
    {
        //isikan kondisi sesuai session perusahaan yang login
        //sementara show all order
        $order = Order::where('id_perusahaan', Auth::user()->perusahaan_id)->orderBy('updated_at', 'DESC')->get();
        //ambil instalasi yang dipesan dari setiap order
        for ($i = 0; $i < sizeof($order); $i++) {
            $permohonan = $order[$i]->permohonan;
            $nama_instalasi = array();
            foreach ($permohonan as $row) {
                array_push($nama_instalasi, @$row->instalasi->nama_instalasi);
            }
            $order[$i]->nama_instalasi = collect($nama_instalasi)->unique();
        }
        $tipe_instalasi = TipeInstalasi::all();
        //        dd($order );
        return view('eksternal/view_order', compact('order', 'tipe_instalasi'));
    }

    //view form input order
    #function ini untuk view detail order saja oleh eksternal
    public function detailOrderEksternal($id = 0)
    {
        $tipe_instalasi = TipeInstalasi::all();
        $permohonan = Permohonan::getAllPermohonanByOrder($id);
        $permohonan_distribusi = Permohonan::getPermohonanSuratTugasByOrder($id);
        $order = Order::findOrFail($id);
        $perusahaan = $order->perusahaan;
        $data_workflow = array(
            'model' => MODEL_ORDER,
            'modul' => MODUL_ORDER,
            'url_notif' => 'detail_order/',
            'url_redirect' => 'eksternal/view_order/'
        );
        $workflow = WorkflowController::workflowJoss($id, $order->getTable(), ACTION_ORDER);
        return view('eksternal/detail_order', compact('permohonan_distribusi', 'data_workflow', 'workflow', 'perusahaan', 'tipe_instalasi', 'permohonan', 'order', 'flow_order'));
    }

    public function editOrder($id)
    {
        $order = Order::findOrFail($id);
        if (Order::isEksternalAllowEdit($order)) {
            $tipe_instalasi = TipeInstalasi::all();
            $permohonan = Permohonan::getAllPermohonanByOrder($id);
            $permohonan_distribusi = Permohonan::getPermohonanSuratTugasByOrder($id);
            $perusahaan = $order->perusahaan;
            return view('eksternal/form_order', compact('permohonan_distribusi', 'data_workflow', 'workflow', 'perusahaan', 'tipe_instalasi', 'permohonan', 'order', 'flow_order'));
        } else {
            return redirect('eksternal/view_order');
        }
    }

    public function storeGeneralOrder(Request $request)
    {
        //        dd($request);
        $order_id = $request->order_id;
        $order = Order::find($order_id);
        $order->nomor_sp = $request->nomor_sp;
        $order->tanggal_sp = $request->tanggal_sp;
        $file = $request->file_sp;
        if ($file != null) {
            if ($order->file_sp != null) {
                $loc = storage_path() . '/upload/surat_permintaan/' . $order->file_sp;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_permintaan-' . $timestamp . '-' . Auth::user()->username . "." . $file->getClientOriginalExtension();
            $order->file_sp = $name;
            $file->move(storage_path() . '/upload/surat_permintaan/', $name);
        }
        $order->save();
        return redirect('/eksternal/view_order')->with('success', 'Order anda berhasil disimpan.');
    }

    public function reviseOrder(Request $request)
    {

        /* =======WORKFLOW HISTORY========= */
        $order_id = $request->order_id;
        $order = Order::find($order_id);
        $status = $request->status;
        $keterangan = $request->keterangan;
        $flow = WorkflowController::getFlowStatusId($status, MODUL_ORDER);
        $order->id_flow_status = ($flow != null) ? $flow->id : null;
        $order->save();

        //notification ke internal bahwa order sudah diperbaiki
        $verificators = User::getAllUserByPermission(PERMISSION_CREATE_RAB);
//    foreach (User::getUserListFromRole('P-BPU') as $user_role) {
        foreach ($verificators as $user_role) {
            $notif = new Notification();
            $notif->to = $user_role->username;
            $notif->from = Auth::user()->username;
            $notif->status = "UNREAD";
            $notif->icon = "icon-note";
            $notif->url = "internal/detail_order/" . $order->id;
            $notif->subject = "ORDER REVISED";
            $notif->message = "Nomor Order " . $order->nomor_order . " telah diperbaiki oleh <br/><b>" . Auth::user()->nama_user . "</b>";
            $notif->color = "btn-warning";
            $notif->save();

            $data = array();
            $data['order'] = $order;
            $data['user'] = $user_role;
            $data['perusahaan'] = $order->perusahaan;
            $data['notif'] = $notif;
            //call send email helper
            $mail_data = array(
                "data" => (object)$data,
                "data_alias" => "data",
                "to" => $user_role->email,
                "to_name" => $user_role->nama_user,
                "subject" => 'ORDER REVISED',
                "description" => "Nomor Order " . $order->nomor_order . " - NEED APPROVAL"
            );
            sendEmail($mail_data, 'emails.notif_create_order', 'NEED APPROVAL', 'ORDER');
        }
        WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_ORDER, $flow, $keterangan);
        return redirect('/eksternal/view_order')->with('success', 'Order anda berhasil disimpan. Mohon menunggu hasil verifikasi');
    }

    public
    function submitOrder(Request $request)
    {
        $id_order = $request->order_id;
        $order = Order::findOrFail($id_order);
        $order->is_submitted = 1;
        $order->save();

        $verificators = User::getAllUserByPermission(PERMISSION_CREATE_RAB);
//        foreach (User::getUserListFromRole('P-BPU') as $user_role) {
        foreach ($verificators as $user_role) {
            $notif = new Notification();
            $notif->from = $request->username;
            $notif->url = "eksternal/create_order/" . $id_order;
            $notif->to = $user_role->username;
            $notif->subject = "Order Baru";
            $notif->status = "UNREAD";
            $notif->message = "Order baru telah diterima.";
            $notif->icon = "icon-briefcase";
            $notif->color = "btn-green";
            $notif->save();
        }

        return redirect('/eksternal/view_order/');
    }

    /* ===============================END CRUD ORDER================================== */

    public function createPermohonan($order_id, $permohonan_id = 0)
    {
        //dd($request);
        $order = Order::findOrFail($order_id);
        $kombinasi_pekerjaan = JenisPekerjaan::all();
        $permohonan = Permohonan::find($permohonan_id);
        $lingkup_pekerjaan = LingkupPekerjaan::where('tipe_instalasi_id', $permohonan->id_tipe_instalasi)->get();
        $instalasi = $permohonan->instalasi;
        $file_detail = "";
        $tipe_instalasi = $instalasi->jenis_instalasi->tipeInstalasi->nama_instalasi;
        switch ($tipe_instalasi) {
            case PEMBANGKIT:
                $file_detail = "instalasi/detail_instalasi_pembangkit";
                break;
            case TRANSMISI:
                $file_detail = "instalasi/detail_instalasi_transmisi";
                break;
            case DISTRIBUSI:
                $file_detail = "instalasi/detail_instalasi_distribusi";
                break;
            case PEMANFAATAN_TM:
                $file_detail = "instalasi/detail_instalasi_pemanfaatan_tm";
                break;
            case PEMANFAATAN_TT:
                $file_detail = "instalasi/detail_instalasi_pemanfaatan_tt";
                break;
        }
        return view('eksternal/form_permohonan', compact('lingkup_pekerjaan', 'kombinasi_pekerjaan', 'order', 'permohonan', 'order_id', 'permohonan_id', 'instalasi', 'file_detail'));
    }

    public
    function storePermohonan(Request $request)
    {
        //        dd($request);
        $id = $request->permohonan_id;
        $permohonan = ($id == 0) ? new Permohonan() : Permohonan::findOrFail($id);
        $order = Order::find($request->order_id);
        $temp_lingkup = ($id == 0) ? 0 : $permohonan->id_lingkup_pekerjaan;
        $lingkup_id = $request->lingkup_pekerjaan;
        $form = new FormFields();
        $fields = $form->form($lingkup_id);
        $file_upload = array();
        //1. insert / update detail permohonan
        $permohonan->id_orders = $request->order_id;
        if ($id == 0) {
            $last_nomor = (int)Permohonan::withTrashed()->max('nomor_permohonan');
            $next_nomor = $last_nomor + 1;
            if ($next_nomor <= 999) {
                $sisa = 4 - strlen((string)$next_nomor);
                for ($i = 0; $i < $sisa; $i++) {
                    $next_nomor = "0" . $next_nomor;
                }
            }
            $permohonan->nomor_permohonan = str_pad($next_nomor, 4, "0", STR_PAD_LEFT) . '-' . date('m') . date('Y');
        }
        if ($order->id_surat_tugas != null) {
            $lingkup_prog = explode('|', $request->lingkup_program);
            $permohonan->id_lingkup_program = $lingkup_prog[0];
            $lingkup_prog = LingkupProgram::find($lingkup_prog[0]);
            $lingkup_id = $lingkup_prog->id_lingkup_pekerjaan;
            $fields = $form->form($lingkup_id);
            $permohonan->id_program = $request->id_program;
            $permohonan->id_area_kontrak = $request->id_area_kontrak;
            $permohonan->id_lokasi_penyulang = $request->id_lokasi_penyulang;
        }
        //        $permohonan->id_lingkup_pekerjaan = $lingkup_id;
        $permohonan->save();

        //2. save uploaded file to
        if ($id == 0) {
            foreach ($fields as $field) {
                $file = $request->file($field['name']);
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'file_permohonan' . '-' . $field['name'] . '-' . $timestamp . '-' . $permohonan->id . "." . $file->getClientOriginalExtension();
                $file->move(storage_path() . '/upload/file_permohonan/', $name);
                $file_upload[$field['name']] = $name;
            }
        } else {
            $file_upload = (array)json_decode($permohonan->file_upload_lingkup);
            //jika ternyata beda lingkup pekerjaan dengan yg sebelumnya di insert maka semua file nya hapus dulu
            if ($temp_lingkup != $lingkup_id) {
                foreach ($file_upload as $file) {
                    $loc = storage_path() . '/upload/file_permohonan/' . $file;
                    if (file_exists($loc))
                        unlink($loc);
                }
                $file_upload = array();
            }
            foreach ($fields as $field) {
                $file = $request->file($field['name']);
                if ($file != null) {
                    //remove dulu yg ada
                    if ($temp_lingkup == $lingkup_id && array_key_exists($field['name'], $file_upload)) {
                        $loc = storage_path() . '/upload/file_permohonan/' . $file_upload[$field['name']];
                        if (file_exists($loc))
                            unlink($loc);
                    }
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = 'file_permohonan' . '-' . $field['name'] . '-' . $timestamp . '-' . $permohonan->id . "." . $file->getClientOriginalExtension();
                    $file->move(storage_path() . '/upload/file_permohonan/', $name);
                    $file_upload[$field['name']] = $name;
                }
            }
        }

        $file_upload = json_encode($file_upload);
        $permohonan->file_upload_lingkup = $file_upload;
        $permohonan->save();

        //3. insert / update relation permohonan & instalasi
        //        if ($order->id_surat_tugas == null) {
        //            $instalasi = InstalasiPermohonan::find($request->instalasi_permohonan_id);
        //            if ($instalasi == null) $instalasi = new InstalasiPermohonan();
        //            $instalasi->id_permohonan = $permohonan->id;
        //            $instalasi->tipe_instalasi = $permohonan->order->tipe_instalasi_id;
        //            $instalasi->id_instalasi = $request->id_instalasi;
        //            $instalasi->save();
        //        }
        //4. insert / update jenis pekerjaan permohonan
        //delete dulu yg ada
        //        PermohonanLingkup::where("permohonan_id", $permohonan->id)->delete();
        //        //insert yg baru
        //        $parent = $request->jenis_pekerjaan;
        //        $child = $request->child_pekerjaan;
        //        foreach ($child as $item) {
        //            $lingkup = new PermohonanLingkup();
        //            $lingkup->permohonan_id = $permohonan->id;
        //            $lingkup->jenis_pekerjaan_id = $parent;
        //            $lingkup->child_id = $item;
        //            $lingkup->save();
        //        }

        return redirect('eksternal/create_order/' . $request->order_id)->with('tab', 'permohonan');
    }

    public function createCustomFields($lingkup_id, $id_permohonan = 0, $status_baru)
    {
        $formField = new FormFields();
        $permohonan = Permohonan::where('id', $id_permohonan)->where('id_lingkup_pekerjaan', $lingkup_id)->first();
        $fields = $formField->form($lingkup_id, $status_baru);
        if ($permohonan != null) {
            $files = (array)json_decode($permohonan->file_upload_lingkup);
            for ($i = 0; $i < sizeof($fields); $i++) {
                $fields[$i]['value'] = (isset($files[$fields[$i]['name']])) ? $files[$fields[$i]['name']] : "";
            }
        }
        return view('eksternal.form_generate_file_permohonan', compact('fields', 'lingkup_id', 'files'));
    }

    public function createCustomFieldsVerifikasi($lingkup_id, $id_permohonan = 0, $status_baru = 0)
    {
        $formField = new FormFields();
        $fields = $formField->form($lingkup_id, $status_baru);
        $permohonan = Permohonan::where('id', $id_permohonan)->where('id_lingkup_pekerjaan', $lingkup_id)->first();
        if ($permohonan != null) {
            $files = (array)json_decode($permohonan->file_upload_lingkup, $permohonan->instalasi->status_baru);
            for ($i = 0; $i < sizeof($fields); $i++) {
                $fields[$i]['value'] = (isset($files[$fields[$i]['name']])) ? $files[$fields[$i]['name']] : "";
            }
        }
        return view('internal.order.form_generate_file_permohonan_verifikasi', compact('fields', 'lingkup_id', 'files'));
    }

    public function deletePermohonan($id)
    {
        $permohonan = Permohonan::findOrFail($id);
        $order_id = $permohonan->id_orders;
        //hapus dulu semua filenya
        $files = (array)json_decode($permohonan->file_upload_lingkup);
        foreach ($files as $file) {
            $loc = storage_path() . '/upload/file_permohonan/' . $file;
            if (file_exists($loc))
                unlink($loc);
        }
        $permohonan->delete();

        return redirect('eksternal/create_order/' . $order_id)->with('tab', 'permohonan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        //
    }

    public
    function createDetailInstalasi($tipe_instalasi, $id)
    {
        switch ($tipe_instalasi) {
            case 1:
                //pembangkit
                $pembangkit = InstalasiPembangkit::find($id);
                return view('eksternal/form_permohonan_detail_instalasi_pembangkit', compact('pembangkit'));
                break;
            case 2:
                //transmisi
                $transmisi = InstalasiTransmisi::find($id);
                return view('eksternal/form_permohonan_detail_instalasi_transmisi', compact('transmisi'));
                break;
            case 3:
                //distribusi
                break;
            case 4:
                //tt
                $pemanfaatan_tt = InstalasiPemanfaatanTT::find($id);
                return view('eksternal/form_permohonan_detail_instalasi_tt', compact('pemanfaatan_tt'));
                break;
            case 5:
                //tm
                $pemanfaatan_tm = InstalasiPemanfaatanTM::find($id);
                return view('eksternal/form_permohonan_detail_instalasi_tm', compact('pemanfaatan_tm'));
                break;
        }
    }

    /* ================AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN==================== */

    public
    function getAreaKontrak($id_program)
    {
        $program = Program::find($id_program);
        $result = array();
        $label1 = ucfirst(strtolower($program->jenisProgram->acuan_1));
        $label2 = ucfirst(strtolower($program->jenisProgram->acuan_2));
        if ($label2 == "Gi")
            $label2 = "Gardu Induk";
        switch (strtoupper($program->jenisProgram->acuan_1)) {
            case 'AREA' :
                $area = AreaProgram::where('id_program', $id_program)->get();
                foreach ($area as $row) {
                    array_push($result, array(
                        "value" => $row->id,
                        "name" => $row->businessArea->description
                    ));
                }
                break;
            case 'KONTRAK' :
                $kontrak = Kontrak::where('id_program', $id_program)->get();
                foreach ($kontrak as $row) {
                    array_push($result, array(
                        "value" => $row->id,
                        "name" => $row->nomor_kontrak
                    ));
                }
                break;
        }
        $data = array("label1" => $label1, "label2" => $label2, "result" => $result);
        echo json_encode($data);
    }

    public
    function getLokasiPenyulang($id_area_kontrak)
    {
        $area_kontrak = AreaProgram::find($id_area_kontrak);
        $result = array();
        if ($area_kontrak != null) {
            $program = Program::find($area_kontrak->id_program);
            $acuan1 = strtoupper($program->jenisProgram->acuan_1);
            switch (strtoupper($program->jenisProgram->acuan_2)) {
                case "LOKASI":
                    $data = ($acuan1 == "AREA") ? LokasiArea::where('id_area_program', $area_kontrak->id)->get() : LokasiArea::where('id_kontrak', $area_kontrak->id)->get();
                    foreach ($data as $row) {
                        array_push($result, array(
                            "value" => $row->id,
                            "name" => $row->lokasi->lokasi
                        ));
                    }
                    break;
                case "PENYULANG":
                    $data = PenyulangArea::where('id_area_program', $area_kontrak->id)->get();
                    foreach ($data as $row) {
                        array_push($result, array(
                            "value" => $row->id,
                            "name" => $row->penyulang->penyulang
                        ));
                    }
                    break;
                case "GI":
                    $data = GarduArea::where('id_area_program', $area_kontrak->id)->get();
                    foreach ($data as $row) {
                        array_push($result, array(
                            "value" => $row->id,
                            "name" => $row->garduInduk->gardu_induk
                        ));
                    }
                    break;
            }
        }
        echo json_encode($result);
    }

    public
    function getLingkupProgram($id_program, $id_lokasi_penyulang, $id_area_kontrak, $id_permohonan)
    {
        $program = Program::find($id_program);
        $result = array();
        $acuan2 = $program->jenisProgram->acuan_2;
        $used_lp = Permohonan::where('id_program', $id_program)
            ->where('id_lokasi_penyulang', $id_lokasi_penyulang)
            ->where('id_area_kontrak', $id_area_kontrak)
            ->where('id', '<>', $id_permohonan)
            ->get(array('id_lingkup_program'));
        switch (strtoupper($acuan2)) {
            case "LOKASI":
                $data = LingkupProgram::whereNotIn('id', $used_lp)->where('id_lokasi_area', $id_lokasi_penyulang)->get();
                break;
            case "PENYULANG":
                $data = LingkupProgram::whereNotIn('id', $used_lp)->where('id_penyulang_area', $id_lokasi_penyulang)->get();
                break;
            case "GI":
                $data = LingkupProgram::whereNotIn('id', $used_lp)->where('id_gardu_area', $id_lokasi_penyulang)->get();
                break;
            default:
                $data = array();
                break;
        }
        foreach ($data as $row) {
            array_push($result, array(
                "value" => $row->id . "|" . $row->id_lingkup_pekerjaan,
                "name" => $row->lingkupPekerjaan->jenis_lingkup_pekerjaan
            ));
        }
        echo json_encode($result);
    }

    /* ================END AJAX URL CASCADE DETAIL PROGRAM DI PERMOHONAN==================== */
    /* =============================================================================================================

    ORDER ACTION FOR INTERNAL USER

    ================================================================================================================ */

    public
    function showOrdersInternal()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_VIEW_ORDER) || User::isCurrUserAllowPermission(PERMISSION_VIEW_PERMOHONAN)) {
            $order = Order::where('is_submitted', 1)->orderBy('updated_at', 'DESC')->get();
            //        dd($order->user());
            for ($i = 0; $i < sizeof($order); $i++) {
                $permohonan = $order[$i]->permohonan;
                $nama_instalasi = array();
                foreach ($permohonan as $row) {
                    array_push($nama_instalasi, @$row->instalasi->nama_instalasi);
                }
                $order[$i]->nama_instalasi = collect($nama_instalasi)->unique();
            }
            return view('internal.order.orders', compact('order'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function detailOrder($id)
    {
        if (User::isCurrUserAllowPermission(PERMISSION_VIEW_PERMOHONAN)) {
            $order = Order::findOrFail($id);
            $perusahaan = $order->perusahaan;
            $tipe_instalasi = TipeInstalasi::all();
            $permohonan = Permohonan::getAllPermohonanByOrder($id);
            $permohonan_distribusi = Permohonan::getPermohonanSuratTugasByOrder($id);
            $id_perusahaan = $perusahaan->id;
            $used_st = Order::where('id_surat_tugas', '!=', "")->where('id', '<>', $id)->get(array('id_surat_tugas'));
            $surat_tugas = SuratTugas::where('id_perusahaan', $id_perusahaan)->whereNotIn('id', $used_st)->get();
            $rab = RAB::where('order_id', $id)->get();


            // dd($used_st);

            $data_workflow = array(
                'model' => MODEL_ORDER,
                'modul' => MODUL_ORDER,
                'url_notif' => 'detail_order/',
                'url_redirect' => 'internal/order/'
            );
            $workflow = WorkflowController::workflowJoss($id, $order->getTable(), ACTION_WORKFLOW);
            return view('internal.order.detail_order', compact('data_workflow', 'workflow', 'perusahaan', 'tipe_instalasi', 'permohonan', 'order', 'surat_tugas', 'rab', 'flow_order', 'permohonan_distribusi'));

        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public
    function verifyPermohonan($order_id, $permohonan_id = null)
    {
        $order = Order::findOrFail($order_id);
        $kombinasi_pekerjaan = JenisPekerjaan::all();
        $permohonan = Permohonan::find($permohonan_id);
        $lingkup_pekerjaan = LingkupPekerjaan::where('tipe_instalasi_id', $permohonan->id_tipe_instalasi)->get();
        $instalasi = $permohonan->instalasi;
        $file_detail = "";
        $tipe_instalasi = $instalasi->jenis_instalasi->tipeInstalasi->nama_instalasi;
        switch ($tipe_instalasi) {
            case PEMBANGKIT:
                $file_detail = "instalasi/detail_instalasi_pembangkit";
                break;
            case TRANSMISI:
                $file_detail = "instalasi/detail_instalasi_transmisi";
                break;
            case DISTRIBUSI:
                $file_detail = "instalasi/detail_instalasi_distribusi";
                break;
            case PEMANFAATAN_TM:
                $file_detail = "instalasi/detail_instalasi_pemanfaatan_tm";
                break;
            case PEMANFAATAN_TT:
                $file_detail = "instalasi/detail_instalasi_pemanfaatan_tt";
                break;
        }
        return view('internal.order.form_permohonan', compact('lingkup_pekerjaan', 'kombinasi_pekerjaan', 'order', 'permohonan', 'order_id', 'permohonan_id', 'instalasi', 'file_detail'));
    }

    public
    function addToCart(Request $request)
    {
        $tipe_instalasi = TipeInstalasi::findOrFail($request->id_tipe_instalasi);
        $form = new FormFields();
        $fields = $form->form($request->lingkup_pekerjaan, (isset($request->status_baru)) ? $request->status_baru : 1);
        $file_upload = array();
        #save uploaded file to storage
        foreach ($fields as $field) {
            $file = $request->file($field['name']);
            if ($file != null) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'file_permohonan' . '-' . $field['name'] . '-' . $timestamp . '-' . 0000 . "." . $file->getClientOriginalExtension();
                $file->move(storage_path() . '/upload/file_permohonan/', $name);
                $file_upload[$field['name']] = $name;
            }
        }
        $file_upload = json_encode($file_upload);

        $layanan = $tipe_instalasi->produk;

        $string = "";
        $bayOrdered = false; //ini untuk flag jika saat add to cart memilih bay yg sudah ada sebelumnya di permohonan
        $transmisi = null;
        if ($request->id_tipe_instalasi == ID_TRANSMISI) {
            $transmisi = InstalasiTransmisi::find($request->id_instalasi);
            if ($transmisi->jenis_instalasi->keterangan == JENIS_GARDU) {
                $bayPermohonan = BayPermohonan::whereIn('bay_id', $request->bay_gardu)->get();
                $permohonanIds = $bayPermohonan->pluck('permohonan_id')->toArray();
            }
        }
        foreach ($layanan as $wa) {
            if ($wa->id == $request->get('layanan_' . $wa->id)) {
                //                make permohonan
                //                array_push($arr_layanan, $wa->id);

                if ($request->id_tipe_instalasi == ID_TRANSMISI && $transmisi->jenis_instalasi->keterangan == JENIS_GARDU) {
                    $validator = Permohonan::with('produk')
                        ->whereIn('id', $permohonanIds)
                        ->where('id_produk', $wa->id)->first();
                } else {
                    $validator = Permohonan::with('produk')
                        ->where('id_tipe_instalasi', $request->id_tipe_instalasi)
                        ->where('id_instalasi', $request->id_instalasi)
                        ->where('id_produk', $wa->id)
                        ->where('id_lingkup_pekerjaan', $request->lingkup_pekerjaan)
                        ->where('status', 'CART')->first();
                }
                //jika gardu induk, cek apakah di permohonan tersebut terdapat bay yg mau dimasukan. Jika tidak ada maka buat permohonan baru
                if (count($validator) > 0) {
                    if ($request->id_tipe_instalasi == ID_TRANSMISI) {
                        if ($transmisi->jenis_instalasi->keterangan == JENIS_GARDU) {
                            $bay = ($request->bay_gardu != null) ? $request->bay_gardu : array();
                            $existed = $validator->bayPermohonan;
                            foreach ($existed as $ex) {
                                $index = array_search($ex->bay_id, $bay);
                                if ($index >= 0) {
                                    array_splice($bay, $index, 1);
                                    $bayOrdered = true;
                                }
                            }
                            if (sizeof($bay) > 0)
                                $validator = array();
                        }
                    }
                }
                if (count($validator) == 0) {

                    $permohonan = new Permohonan();
                    $permohonan->id_tipe_instalasi = $request->id_tipe_instalasi;
                    $permohonan->id_produk = $wa->id;
                    $permohonan->id_lingkup_pekerjaan = $request->lingkup_pekerjaan;
                    $permohonan->id_instalasi = $request->id_instalasi;
                    $permohonan->created_by = Auth::user()->username;
                    $permohonan->status = 'CART';
                    $last_nomor = (int)Permohonan::withTrashed()->where('SUBSTR(created_at,0,4)', date('Y'))->count();
                    if ($last_nomor == 0) {
                        $next_nomor = 1;
                    } else {
                        $next_nomor = $last_nomor + 1;
                    }
                    $permohonan->nomor_permohonan = str_pad($next_nomor, 4, "0", STR_PAD_LEFT) . '-' . date('m') . date('Y');
                    //
                    $permohonan->save();

                    //                  $permohonan->nomor_permohonan       = str_pad($permohonan->id,10,'0',STR_PAD_LEFT);
                    //                  $permohonan->nomor_permohonan = $permohonan->id;
                    $permohonan->tanggal_permohonan = $permohonan->created_at;
                    $permohonan->save();

                    $permohonan->file_upload_lingkup = $file_upload;

                    /* =========JIKA INSTALASI ADALAH GARDU MAKA MASUKAN BAY KE PERMOHONAN===== */
                    $instalasi = $permohonan->instalasi;
                    if ($instalasi->jenis_instalasi->keterangan == JENIS_GARDU) {
                        if (!(isset($bay))) {
                            $bay = ($request->bay_gardu != null) ? $request->bay_gardu : array();
                        }
                        if (sizeof($bay) > 0) {
                            $permohonan->save();
                            foreach ($bay as $by) {
                                $bayPermohonan = new BayPermohonan();
                                $bayPermohonan->permohonan_id = $permohonan->id;
                                $bayPermohonan->bay_id = $by;
                                $bayPermohonan->save();
                            }
                        }
                    } else {
                        $permohonan->save();
                    }
                } else {
                    $string .= $validator->produk->produk_layanan . ", ";
                    //                    array_push($arr_layanan, $validator->produk->produk_layanan);
                }
            }
        }

        //==============JIKA INSTALASI GARDU INDUK TRANSMISI MAKA SAVE FILE SBUJK,IUJK,dan SLD
        if ($tipe_instalasi->id == ID_TRANSMISI) {
            if (@$instalasi->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                $bays = ($request->bay_gardu != null) ? $request->bay_gardu : array();
                if (sizeof($bays) > 0) {
                    foreach ($bays as $rowBay) {
                        $data_bay = BayGardu::find($rowBay);
                        if ($data_bay != null) {
                            #save file lampiran
                            if ($request->hasFile('file_sbujk')) {
                                $filename = saveFile($request->file_sbujk, 'file_sbujk', str_replace(' ', '_', $data_bay->nama_bay));
                                $data_bay->file_sbujk = $filename;
                            }
                            if ($request->hasFile('file_iujk')) {
                                $filename = saveFile($request->file_iujk, 'file_iujk', str_replace(' ', '_', $data_bay->nama_bay));
                                $data_bay->file_iujk = $filename;
                            }
                            if ($request->hasFile('file_sld')) {
                                $filename = saveFile($request->file_sld, 'file_sld', str_replace(' ', '_', $data_bay->nama_bay));
                                $data_bay->file_sld = $filename;
                            }
                            #end save file lampiran
                            $data_bay->save();
                        }
                    }
                }
            }
        }
        //===============================END SAVE FILE SBUJK, IUJK, SLD========================
        if ($string != "") {
            if ($bayOrdered == false) {
                return redirect('/eksternal')->with('error', $string . " sudah dipilih sebelumnya!");
            } else {
                return redirect('/eksternal')->with('error', 'Permohonan berhasil dibuat. Sebagian lingkup sudah ada di permohonan sebelumnya');
            }
        } else {
            if ($bayOrdered == false) {
                return redirect('/eksternal')->with('success', 'Permohonan berhasil dibuat.');
            } else {
                return redirect('/eksternal')->with('success', 'Permohonan berhasil dibuat. Sebagian lingkup sudah ada di permohonan sebelumnya');
            }
        }
//        } else {
//            return $this->saveOrderDistribusi($request);
//        }
    }

    public
    function addToCartSuratTugas($id)
    {
        $surat_tugas = SuratTugas::find($id);
        $instalasi = $surat_tugas->instalasi;
        $produk = $surat_tugas->produk;
        //add all instalasi to permohonan
        foreach ($produk as $prd) {
            foreach ($instalasi as $item) {
                $lingkup = $item->lingkupPekerjaan;
                foreach ($lingkup as $lngkp) {
                    $permohonan = new Permohonan();
                    $permohonan->tanggal_permohonan = Carbon::now();
                    $permohonan->id_lingkup_pekerjaan = $lngkp->lingkup_id;
                    $permohonan->id_tipe_instalasi = ID_DISTRIBUSI;
                    $permohonan->id_instalasi = $item->id;
                    $permohonan->status = 'CART';
                    $permohonan->created_by = Auth::user()->username;
                    $permohonan->id_produk = $prd->produk_id;
                    $last_nomor = (int)Permohonan::withTrashed()->where('SUBSTR(created_at,0,4)', date('Y'))->count();
                    if ($last_nomor == 0) {
                        $next_nomor = 1;
                    } else {
                        $next_nomor = $last_nomor + 1;
                    }
                    $permohonan->nomor_permohonan = str_pad($next_nomor, 4, "0", STR_PAD_LEFT) . '-' . date('m') . date('Y');
                    $permohonan->save();
                }
                if (sizeof($lingkup) == 0) {
                    $permohonan = new Permohonan();
                    $permohonan->tanggal_permohonan = $permohonan->created_at;
                    $permohonan->id_tipe_instalasi = ID_DISTRIBUSI;
                    $permohonan->id_instalasi = $item->id;
                    $permohonan->status = 'CART';
                    $permohonan->created_by = Auth::user()->username;
                    $permohonan->produk_id = $prd->produk_id;
                    $permohonan->save();

                }
            }
        }
        return redirect('/eksternal/surat_tugas')->with('success', 'Permohonan berhasil dibuat.');
    }

    /* =============================================================================================================

    ORDER ACTION FOR EKSTERNAL USER

    ================================================================================================================ */
    public
    function viewKeranjang()
    {
        $permohonan = Permohonan::getAllPermohonan();
        $perusahaan = Auth::user()->perusahaan;
        $permohonan_distribusi = Permohonan::getPermohonanSuratTugas();
        $id_perusahaan = (Auth::user() == null) ? null : Auth::user()->perusahaan_id; //nanti diganti sesuai session
        return view('eksternal/view_cart2', compact('permohonan', 'perusahaan', 'id_perusahaan', 'permohonan_distribusi'));
    }

    public
    function deleteChartPermohonan($id_permohonan)
    {
        $permohonan = Permohonan::findOrFail($id_permohonan);
        $permohonan->status = "DEL";
        $permohonan->save();
        $permohonan->delete();
        return redirect('/eksternal/keranjang')->with('success', 'Permohonan berhasil dihapus.');
    }

    public
    function deleteChartPermohonanDistribusi($id)
    {
        //id = id_surat_tugas
        $permohonan = Permohonan::getPermohonanDistribusi($id);
        $permohonan_id = array_map(function ($obj) {
            return $obj->id;
        }, $permohonan);
        $permohonan = Permohonan::whereIn('id', $permohonan_id)->get();
        foreach ($permohonan as $prm) {
            $prm->status = "DEL";
            $prm->save();
            $prm->delete();
        }
        return redirect('/eksternal/keranjang')->with('success', 'Permohonan berhasil dihapus.');
    }

    public function checkOutOrder(Request $request)
    {
        //get flow status id
        $flow_status = WorkflowController::getFlowStatus(SUBMITTED, MODUL_ORDER);

        //Save order

        $order = new Order();
        //        $order->nomor_order = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString()); //sementara pake timestamp
        // jika tahun baru, reset nomor
        if (date("j") == 1 && date("n") == 1)
            $nomor_urut = 1;
        else
            $nomor_urut = Order::whereYear('created_at', '=', date('Y'))->count() + 1;

        $order->nomor_order = str_pad($nomor_urut, 6, "0", STR_PAD_LEFT) . "-" . str_pad($request->perusahaan, 6, "0", STR_PAD_LEFT) . "-" . date('Y');
        $order->tanggal_order = Carbon::now();
        $order->id_perusahaan = $request->perusahaan;
        $order->nomor_sp = $request->nomor_sp;
        $order->tanggal_sp = $request->tanggal_sp;
        $order->sys_status = "SUBMITTED";
        $order->is_submitted = '1';
        $order->created_by = Auth::user()->username;
        $order->id_flow_status = ($flow_status != null) ? $flow_status->id : null;

        $file = $request->file_sp;
        if ($file != null) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'surat_permintaan-' . $timestamp . '-' . $request->email . "." . $file->getClientOriginalExtension();
            $order->file_sp = $name;
            $file->move(storage_path() . '/upload/surat_permintaan/', $name);
        }
        $order->save();

        //update detail permohonan (status, id_orders)
        $permohonan = Permohonan::where('created_by', Auth::user()->username)
            ->where('status', 'like', '%CART%')
            ->orderBy('id', 'desc')->get();
        foreach ($permohonan as $item) {
            $item->id_orders = $order->id;
            $item->status = "CHECKOUT";
            $item->save();
        }

        //Send Email Notification to BPU
        $perusahaan = $order->perusahaan;
        $data = array();
        $user_role = Order::getSpv($order);

        $notif = new Notification();
        $notif->url = "internal/detail_order/" . $order->id;
        $notif->to = $user_role->username;
        $notif->from = Auth::user()->username;
        $notif->subject = "NEW ORDER";
        $notif->status = "UNREAD";
        $notif->message = "Terdapat Order Pekerjaan Baru " . $order->nomor_order . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
        $notif->icon = "icon-note";
        $notif->color = "btn-blue";
        $notif->save();
        $data['order'] = $order;
        $data['user'] = $user_role;
        $data['perusahaan'] = $perusahaan;
        $data['notif'] = $notif;
        //call send email helper
        $mail_data = array(
            "data" => (object)$data,
            "data_alias" => "data",
            "to" => $user_role->email,
            "to_name" => $user_role->nama_user,
            "subject" => 'NEW ORDER',
            "description" => "NEW ORDER CREATED NEED APPROVAL"
        );
        sendEmail($mail_data, 'emails.notif_create_order', 'NEED APPROVAL', 'ORDER');

        //send email detail order to peminta jasa
        $notif = new Notification();
        $notif->url = "eksternal/create_order/" . $order->id;
        $data['order'] = $order;
        $data['user'] = Auth::user();
        $data['perusahaan'] = Auth::user()->perusahaan;
        $data['notif'] = $notif;
        $data['status'] = SUBMITTED;
        //call send email helper
        $mail_data = array(
            "data" => (object)$data,
            "data_alias" => "data",
            "to" => Auth::user()->email,
            "to_name" => Auth::user()->nama_user,
            "subject" => 'NEW ORDER',
            "description" => "NEW ORDER CREATED NEED APPROVAL"
        );

        sendEmail($mail_data, 'emails.confirm_order', 'NEW ORDER', 'ORDER');
        WorkflowController::createFlowHistory($order->id, $order->getTable(), MODUL_ORDER, $flow_status, SUBMITTED);

        #create tracking
        foreach ($permohonan as $item) {
            $tracking = new Tracking();
            $tracking->permohonan_id = $item->id;
            $tracking->order_id = $order->id;
            $tracking->instalasi_id = $item->id_instalasi;
            $tracking->tipe_instalasi = $item->id_tipe_instalasi;
            $tracking->status = STATUS_ORDER;
            $tracking->user_id = Auth::user()->id;
            $tracking->save();
        }
        #---------------

        return redirect('/eksternal/detail_order/' . $order->id)->with('success', 'Berhasil melakukan checkout order.Mohon menunggu tahap verifikasi selanjutnya.');
    }

    public
    function filterBay(Request $request)
    {
        $jenis = $request->jenis;
        $data = json_decode($request->data_bay);
        $bay = array();
        foreach ($data as $dt) {
            if ($jenis == "all" || $dt->jenis_bay == $jenis) {
                array_push($bay, $dt);
            }
        }
        echo json_encode($bay);
    }

    /* ======================START APPROVAL ORDER========================== */

    public function approvalOrder(Request $request)
    {
        $status = $request->status;
        $keterangan = $request->keterangan;
        $order_id = $request->id;
        $order = Order::findOrFail($order_id);
        $notif = new Notification();
        $notif->to = $order->created_by;
        $notif->from = Auth::user()->username;
        $notif->status = "UNREAD";
        $notif->icon = "icon-note";
        $notif->url = "eksternal/create_order/" . $order->id;
        $flow_status = WorkflowController::getFlowStatusId($status, MODUL_ORDER);
        //        dd($status);
        $notif->subject = "ORDER " . $flow_status->tipe_status;
        $notif->message = "Nomor Order " . $order->nomor_order . " " . $flow_status->tipe_status;
        $notif->color = ($flow_status->tipe_status == APPROVED) ? "btn-success" : "btn-danger";
        $notif->save();
        $order->id_flow_status = ($flow_status != null) ? $flow_status->id : null;
        $order->save();

        $data = array();
        $data['order'] = $order;
        $data['user'] = $order->user;
        $data['perusahaan'] = $order->perusahaan;
        $data['notif'] = $notif;
        $data['status'] = $flow_status->tipe_status;
        //call send email helper
        $mail_data = array(
            "data" => (object)$data,
            "data_alias" => "data",
            "to" => $order->user->email,
            "to_name" => $order->user->nama_user,
            "subject" => "Nomor Order " . $order->nomor_order . " " . (($flow_status->tipe_status == APPROVED) ? 'Disetujui' : 'Perlu Diperbaiki'),
            "description" => 'ORDER ' . $flow_status->tipe_status
        );
        sendEmail($mail_data, 'emails.confirm_order', $notif->subject, 'ORDER');
        WorkflowController::createFlowHistory($order_id, $order->getTable(), MODUL_ORDER, $flow_status, $keterangan);
        return redirect('/internal/order')->with('success', 'Berhasil melakukan ' . (($flow_status->tipe_status == APPROVED) ? 'approve' : 'reject') . ' order');
    }

    /* ======================END APPROVAL ORDER========================== */

    //detail permohonan distribusi

    public function verifyDetailSuratTugas($id = null)
    {
        $surat = ($id == null) ? null : SuratTugas::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_DISTRIBUSI);
        $layanan = $tipe_instalasi->produk()->get();
        $used_layanan = ($id == null) ? array() : LayananSuratTugas::select('produk_id')->where('surat_tugas_id', $id)->get()->toArray();
        return view('internal/detail_surat_tugas', compact('surat', 'layanan', 'used_layanan'));
    }

    public
    function verifyDetailInstalasiProgram($id_program, $id_area_program, $id_parent, $id = 0)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 3)->get();  //sementara manual
        $distribusi = ($id == 0) ? null : InstalasiDistribusi::find($id);
        $ref_parent = References::where('nama_reference', 'Instalasi Distribusi')->first();
        $jenis_distribusi = References::where('parent', $ref_parent->id)->get();
        $program = Program::find($id_program);
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $user = User::where('username', $program->user_create)->get()->first();
        $pemilik_instalasi = $user->perusahaan->pemilikInstalasi;
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $parent = null;
        switch ($program->jenisProgram->acuan_2) {
            case TIPE_GARDU:
                $gardu = GarduArea::find($id_parent);
                $parent = $gardu->garduInduk->gardu_induk;
                break;
            case TIPE_PENYULANG:
                $penyulang = PenyulangArea::find($id_parent);
                $parent = $penyulang->penyulang->penyulang;
                break;
            case TIPE_LOKASI:
                $lokasi = LokasiArea::find($id_parent);
                $parent = $lokasi->lokasi->lokasi;
                break;
        }

        // longitude awal
        $longitude_awal = '';
        $longitude_awal = explode(' ° ', ($id != null) ? $distribusi->longitude_awal : '');
        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
        // longitude akhir
        $longitude_akhir = '';
        $longitude_akhir = explode(' ° ', ($id != null) ? $distribusi->longitude_akhir : '');
        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
        // latitude awal
        $latitude_awal = '';
        $latitude_awal = explode(' ° ', ($id != null) ? $distribusi->latitude_awal : '');
        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
        // latitude akhir
        $latitude_akhir = '';
        $latitude_akhir = explode(' ° ', ($id != null) ? $distribusi->latitude_akhir : '');
        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';

        array_add($distribusi, 'longitude_awal_deg', $longitude_awal_deg);
        array_add($distribusi, 'longitude_awal_min', $longitude_awal_min);
        array_add($distribusi, 'longitude_awal_sec', $longitude_awal_sec);
        array_add($distribusi, 'longitude_awal_dir', $longitude_awal_dir);
        array_add($distribusi, 'longitude_akhir_deg', $longitude_akhir_deg);
        array_add($distribusi, 'longitude_akhir_min', $longitude_akhir_min);
        array_add($distribusi, 'longitude_akhir_sec', $longitude_akhir_sec);
        array_add($distribusi, 'longitude_akhir_dir', $longitude_akhir_dir);
        array_add($distribusi, 'latitude_awal_deg', $latitude_awal_deg);
        array_add($distribusi, 'latitude_awal_min', $latitude_awal_min);
        array_add($distribusi, 'latitude_awal_sec', $latitude_awal_sec);
        array_add($distribusi, 'latitude_awal_dir', $latitude_awal_dir);
        array_add($distribusi, 'latitude_akhir_deg', $latitude_akhir_deg);
        array_add($distribusi, 'latitude_akhir_min', $latitude_akhir_min);
        array_add($distribusi, 'latitude_akhir_sec', $latitude_akhir_sec);
        array_add($distribusi, 'latitude_akhir_dir', $latitude_akhir_dir);

        //lingkup pekerjaan
        $lingkup = LingkupPekerjaan::where('tipe_instalasi_id', ID_DISTRIBUSI)->get();
        $used_lingkup = ($id == 0) ? array() : LingkupDistribusi::select('lingkup_id')->where('instalasi_id', $id)->get()->toArray();
        return view('internal/detail_instalasi_program', compact('id_area_program', 'id_program', 'id_parent',
            'distribusi', 'jenis_distribusi', 'program', 'jenis_instalasi', 'tegangan_pengenal', 'pemilik_instalasi', 'province',
            'city', 'jenis_ijin_usaha', 'pemilik', 'parent', 'lingkup', 'used_lingkup'));
    }

    public
    function internalStorePermohonan(Request $request)
    {
        //get hasil validasi file dokumen permen
        $permohonan_id = $request->permohonan_id;
        $order_id = $request->order_id;
        $permohonan = Permohonan::findOrFail($permohonan_id);
        $lingkup_id = $permohonan->id_lingkup_pekerjaan;
        $formField = new FormFields();
        $fields = $formField->form($lingkup_id, $permohonan->instalasi->status_baru);
        $permohonan = Permohonan::where('id', $permohonan_id)->where('id_lingkup_pekerjaan', $lingkup_id)->first();
        if ($permohonan != null) {
            $files = (array)json_decode($permohonan->file_upload_lingkup);
            for ($i = 0; $i < sizeof($fields); $i++) {
                //jika file nya ada, maka cek sesuai apa engga
                if (isset($files[$fields[$i]['name']])) {
                    $files["valid_" . $fields[$i]['name']] = (isset($request["valid_" . $fields[$i]['name']])) ? true : false;
                }
            }
            $new_files = json_encode($files);
            //update file upload  lingkup
            $permohonan->file_upload_lingkup = $new_files;
            $permohonan->save();
        }
        return redirect('internal/detail_order/' . $order_id)->with('success', 'Permohonan berhasil disimpan.');
    }

    private function saveOrderDistribusi(Request $request)
    {
        //ambil tipe instalasi yang dipilih
        $tipe_instalasi = TipeInstalasi::findOrFail($request->id_tipe_instalasi);
        //ambil lingkup yang dipilih
        $id_lingkups = $request->lingkup_pekerjaan_distribusi;

        //ambil layanan yang dipilih
        $layanan = $tipe_instalasi->produk;
        $string = "";
        foreach ($layanan as $wa) {
            //setiap lingkup di setiap layanan di generate menjadi 1 permohonan
            if ($wa->id == $request->get('layanan_' . $wa->id)) {
                //                make permohonan
                //                array_push($arr_layanan, $wa->id);
                foreach ($id_lingkups as $lingkup) {
                    $validator = Permohonan::with('produk')
                        ->where('id_tipe_instalasi', $request->id_tipe_instalasi)
                        ->where('id_instalasi', $request->id_instalasi)
                        ->where('id_produk', $wa->id)
                        ->where('id_lingkup_pekerjaan', $lingkup)
                        ->where('status', 'CART')->first();
                    if (count($validator) == 0) {

                        $permohonan = new Permohonan();
                        $permohonan->id_tipe_instalasi = $request->id_tipe_instalasi;
                        $permohonan->id_produk = $wa->id;
                        $permohonan->id_lingkup_pekerjaan = $lingkup;
                        $permohonan->id_instalasi = $request->id_instalasi;
                        $permohonan->created_by = Auth::user()->username;
                        $permohonan->status = 'CART';
                        $last_nomor = (int)Permohonan::withTrashed()->where('SUBSTR(created_at,0,4)', date('Y'))->count();
                        if ($last_nomor == 0) {
                            $next_nomor = 1;
                        } else {
                            $next_nomor = $last_nomor + 1;
                        }
                        $permohonan->nomor_permohonan = str_pad($next_nomor, 4, "0", STR_PAD_LEFT) . '-' . date('m') . date('Y');
                        $permohonan->save();
                        $permohonan->tanggal_permohonan = $permohonan->created_at;
                        $permohonan->save();
                    } else {
                        $string .= $validator->produk->produk_layanan . ", ";
                    }
                }
            }
        }

        if ($string != "") {
            return redirect('/eksternal')->with('error', 'Permohonan berhasil dibuat. Sebagian lingkup sudah ada di permohonan sebelumnya');
        } else {
            return redirect('/eksternal')->with('success', 'Permohonan berhasil dibuat. Sebagian lingkup sudah ada di permohonan sebelumnya');
        }
    }
}
