<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tracking;
use Illuminate\Support\Facades\Auth;

class TrackingController extends Controller
{
    public function showTracking(){
        $tracking = Tracking::all();
        // dd($tracking[0]->permohonan->user->perusahaan);
        return view('internal.tracking.tracking', compact('tracking'));
    }

    public function showTrackingEksternal(){
        $tracking = Tracking::where('user_id', Auth::user()->id)->get();
        return view('eksternal.tracking.tracking', compact('tracking'));
    }
}
