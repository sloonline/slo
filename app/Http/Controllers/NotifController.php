<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notif = Notification::where('to',Auth::user()->username)->with('froms')->get()->sortByDesc('id');

        return view('notif_list', compact('notif'));
    }

    public function readThenRedirect($id){
        $notif = Notification::findOrFail($id);
        $notif->status = 'READ';
        $notif->save();

        return redirect($notif->url);
    }
}
