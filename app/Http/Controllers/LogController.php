<?php

namespace App\Http\Controllers;

use App\LogActivity;
use App\LogEmail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LogController extends Controller
{
    public function showLogEmail()
    {
        $logs = LogEmail::orderBy('id', 'desc')->get();

        return view('internal/email_log', compact('logs'));
    }

    public function resendEmail($id)
    {
        $resend = resendEmail($id);
        if ($resend['sent'] == false) {
            Session::flash('error', 'Resend gagal dilakukan. ' . $resend['log']);
        } else {
            Session::flash('message', 'Resend berhasil dilakukan');
        }
        return redirect('/internal/log_email');
    }

    public function showLogActivity()
    {
        $logs = LogActivity::all();

        return view('internal/activity_log', compact('logs'));
    }

    public static function storeActivity($activity, $modul, $description)
    {
        $log = new LogActivity();
        $log->user_id = (Auth::user() != null) ? Auth::user()->id : "Guest";
        $log->created_by = (Auth::user() != null) ? Auth::user()->username : "Guest";
        $log->activity = $activity;
        $log->modul = $modul;
        $log->description = $description;
//        $log->save();
    }

    public static function sendEmailJob(){
        //jalankan send email
        $mail = LogEmail::whereIn('status',['CREATED','NOT SENT'])->get();
        foreach ($mail as $data){
            print_r(resendEmail($data->id))."<br/>";
        }
    }
}
