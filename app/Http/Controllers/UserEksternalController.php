<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use App\BusinessArea;
use App\Cities;
use App\Bidang;
use App\CompanyCode;
use App\Jabatan;
use App\Notification;
use App\PemilikInstalasi;
use App\Penugasan;
use App\PenugasanUser;
use App\PesanNotifikasi;
use App\Role;
use App\SubBidang;
use App\StatusNotif;
use App\References;

use App\User;
use App\KategoriPerusahaan;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Perusahaan;
use App\Provinces;
use DateTime;
use Illuminate\Support\Facades\File;
use Mail;
use Hash;
use Illuminate\Support\Facades\Session;

class UserEksternalController extends Controller
{
    private $user_pj;
    private $perusahaan_pj;

    public function createUserPemintaJasa() //swasta
    {
        //dd("createUserPemintaJasa");
        $province = Provinces::all();
        $city = Cities::all();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        return view('front/register', compact('province', 'city', 'jenis_ijin_usaha'));
    }

    /*START Register user peminta jasa non PLN*/
    public function storeExternal(UserRequest $request)
    {
        //cek apakah email sudah dipakai sebelumnya
        $email = User::where('email', $request->email_user)->count();
        //cek apakah username sudah dipakai sebelumnya
        /*Username ditiadakan, diganti email
        $username = User::where('username', $request->username)->count();
        */
        $username = User::where('username', $request->email_user)->count();
        if ($email == 0 && $username == 0) {
            $user = $this->insertExternal($request);
            //notify verifikator untuk approval user
            $verificators = User::getAllUserByPermission(PERMISSION_VERIFICATE_USER);
//            foreach (User::getUserListFromRole('P-BPU') as $user_role) {
            foreach ($verificators as $user_role) {
                $notif = new Notification();
//                $notif->from = $request->username;
                $notif->from = $request->email_user;
                $notif->url = "internal/det_reg_peminta/" . $user->id;
                $notif->to = $user_role->username;
                $notif->subject = "NEW USER REQUEST";
                $notif->status = "UNREAD";
                $notif->message = "Permintaan user baru telah diterima.";
                $notif->icon = "icon-users";
                $notif->color = "btn-blue";
                $notif->save();

                //cari detail perusahaan
                /*$email = User::Join('perusahaan','perusahaan.id','=','users.perusahaan_id')
                    ->where('users.username', $notif->from);*/
                //dd($this->perusahaan_pj);
                $notif['userrole_name'] = $user_role->nama_user;
                $notif['perusahaan'] = $this->perusahaan_pj->nama_perusahaan;
                $notif['alamat_perusahaan'] = $this->perusahaan_pj->alamat_perusahaan;
                $notif['nama_user'] = $this->user_pj->nama_user;
                $notif['username'] = $this->user_pj->username;

                //call send email helper
                $mail_data = array(
                    "data" => $notif,
                    "data_alias" => "notif",
                    "to" => $user_role->email,
                    "to_name" => $user_role->username,
                    "subject" => $notif->subject,
                    "description" => "NEW USER CREATED NEED APPROVAL"
                );
                sendEmail($mail_data, 'emails.notif_create_user', 'NEED APPROVAL', 'REGISTER');
//                Mail::send('emails.notif_create_user', ['notif' => $notif], function ($m) use ($notif, $user_role) {
//                    $m->from('pusertif@pln.co.id', 'PLN Pusertif');
//                    $m->to($user_role->email, $user_role->username)->subject($notif->subject);
//                });
            }
            Session::flash('message', 'Registrasi user berhasil dilakukan!');
            return redirect('/');
        } else {
            Session::flash('error', 'Registrasi user gagal dilakukan. Email ' . $request->email_user . ' sudah digunakan sebelumnya.');
            return redirect('/register');
        }
    }

    /*END*/

    public function editProfileUser($id)
    {
        $user = User::join('perusahaan', 'perusahaan.id', '=', 'users.perusahaan_id')
            ->leftjoin('provinces', 'provinces.id', '=', 'perusahaan.id_province')
            ->leftjoin('cities', 'cities.id', '=', 'perusahaan.id_city')
            ->findOrFail($id);

        if ($user->id_province != null) {
            $province = Provinces::where('id', '<>', $user->id_province)->get();
            $city = Cities::where('id', '<>', $user->id_city)->get();
        } else {
            $province = Provinces::all();
            $city = Cities::all();
        }
        $kategori = KategoriPerusahaan::find($user->perusahaan->kategori_perusahaan);
        $company_code = CompanyCode::all();
        $business_area = BusinessArea::all();
        return view('eksternal/edit_user_peminta_jasa_pln', compact('user', 'province', 'city', 'id', 'company_code', 'business_area', 'kategori'));
    }

    public function updateProfileUser(Request $request, $id)
    {
        //cek apakah email sudah dipakai sebelumnya
        $email = User::where('email', $request->email_user)->where('id', '!=', $id)->count();
        //cek apakah username sudah dipakai sebelumnya
        $username = User::where('username', $request->username)->where('id', '!=', $id)->count();
        if ($email == 0 && $username == 0) {
            $user = User::findOrFail($id);
            $perusahaan = Perusahaan::findOrFail($user->perusahaan_id);
            $perusahaan->nama_pemimpin_perusahaan = $request->nama_pemimpin_perusahaan;
            $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
            $perusahaan->no_fax_perusahaan = $request->no_fax_perusahaan;
            $perusahaan->email_perusahaan = $request->email_perusahaan;
            $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
            $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
            $perusahaan->id_province = $request->provinsi;
            $perusahaan->id_city = $request->kabupaten;
            $company = CompanyCode::where('company_code', $request->id_company_code)->first();
            if ($company != null) {
                $perusahaan->id_company_code = $company->id;
                $perusahaan->id_business_area = $request->id_business_area;
                $business_area = BusinessArea::find($request->id_business_area);
                $perusahaan->nama_perusahaan = ($business_area != null) ? $business_area->description : "PLN";
            }

            $user->nama_user = $request->nama_user;
            $user->username = $request->username;
            $user->email = $request->email_user;
            $user->tempat_lahir_user = $request->tempat_lahir;
            $user->tanggal_lahir = Carbon::parse($request->tanggal_lahir)->format('Y-m-d H:i:s');//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
            $user->gender_user = $request->gender_user;
            $user->no_hp_user = $request->no_hp_user;
            $user->alamat_user = $request->alamat_user;

            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $nm_p_custom = '-' . $timestamp . '-' . $request->email_perusahaan . '.';
            $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';

            /*--------------------------------------update file upload perusahaan-----------------------------*/
//            if ($request->hasFile('file_npwp_perusahaan')) {
//                $filename = $perusahaan->file_npwp_perusahaan;
//                $fullPath = storage_path() . '/upload/npwp_perusahaan/' . $filename;
//                if (File::exists($fullPath)) File::delete($fullPath);
//                $perusahaan->file_npwp_perusahaan = 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension();
//                $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension());
//            }

            if ($request->hasFile('file_surat_ijin_usaha')) {
                $filename = $perusahaan->file_siup_perusahaan;
                $fullPath = storage_path() . '/upload/surat_ijin_usaha/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $perusahaan->file_siup_perusahaan = 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension();
                $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension());
            }

            if ($request->hasFile('file_skdp')) {
                $filename = $perusahaan->file_skdp;
                $fullPath = storage_path() . '/upload/surat_keterangan_domisili_perusahaan/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $perusahaan->file_skdp = 'file_skdp' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension();
                $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension());
            }

            if ($request->hasFile('file_situ')) {
                $filename = $perusahaan->file_situ_perusahaan;
                $fullPath = storage_path() . '/upload/surat_ijin_tempat_usaha/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $perusahaan->file_situ_perusahaan = 'file_situ' . $nm_p_custom . $request->file_situ->getClientOriginalExtension();
                $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension());
            }

            if ($request->hasFile('file_tdp_perusahaan')) {
                $filename = $user->file_tdp_perusahaan;
                $fullPath = storage_path() . '/upload/tanda_daftar_perusahaan/' . $filename;
                if (File::exists($fullPath)) File::delete($fullPath);
                $perusahaan->file_tdp_perusahaan = 'file_tdp_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension();
                $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension());
            }
            /*------------------------------------------------------------------------------------------------*/
            /*--------------------------------------upload file upload user-----------------------------------*/
//            if ($request->hasFile('file_npwp_user')) {
//                $filename = $user->file_npwp_user;
//                $fullPath = storage_path() . '/upload/npwp_user/' . $filename;
//                if (File::exists($fullPath)) File::delete($fullPath);
//                $user->file_npwp_user = 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension();
//                $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
//            }
//
//            if ($request->hasFile('file_ktp_user')) {
//                $filename = $user->file_ktp_user;
//                $fullPath = storage_path() . '/upload/ktp_user/' . $filename;
//                if (File::exists($fullPath)) File::delete($fullPath);
//                $user->file_ktp_user = 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension();
//                $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
//            }

//            if ($request->hasFile('file_foto_user')) {
//                $filename = $user->file_foto_user;
//                $fullPath = storage_path() . '/upload/foto_user/' . $filename;
//                if (File::exists($fullPath)) File::delete($fullPath);
//                $user->file_foto_user = 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension();
//                $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension());
//            }
            /*------------------------------------------------------------------------------------------------*/

            $perusahaan->save();
            $user->save();
            Session::flash('message', 'Profile anda berhasil diubah');
        } else if ($email != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Email ' . $request->email . ' sudah digunakan sebelumnya.');
        } else if ($username != 0) {
            Session::flash('error', 'Registrasi user gagal dilakukan. Username ' . $request->username . ' sudah digunakan sebelumnya.');
        }
        $verificators = User::getAllUserByPermission(PERMISSION_VERIFICATE_USER);
//        foreach (User::getUserListFromRole('P-BPU') as $user_role) {
        foreach ($verificators as $user_role) {
            $notif = new Notification();
//                $notif->from = $request->username;
            $notif->from = $request->email;
            $notif->url = "internal/edit_user_peminta_jasa_pln/" . $id;
            $notif->to = $user_role->username;
            $notif->subject = "UPDATE PROFILE USER";
            $notif->status = "UNREAD";
            $notif->message = "User telah melakukan update profil.";
            $notif->icon = "icon-users";
            $notif->color = "btn-blue";
            $notif->save();

            //cari detail perusahaan
            /*$email = User::Join('perusahaan','perusahaan.id','=','users.perusahaan_id')
                ->where('users.username', $notif->from);*/
            //dd($this->perusahaan_pj);
            $notif['jenis_user'] = $user->jenis_user;
            $notif['userrole_name'] = $user_role->nama_user;
            $notif['nama_perusahaan'] = $business_area->description;
            $notif['alamat_perusahaan'] = $request->alamat_perusahaan;
            $notif['nama_user'] = $request->nama_user;
            $notif['username'] = $request->username;

            //call send email helper
            $mail_data = array(
                "data" => $notif,
                "data_alias" => "notif",
                "to" => $user_role->email,
                "to_name" => $user_role->username,
                "subject" => $notif->subject,
                "description" => "NOTIFICATION UPDATE PROFILE"
            );
            sendEmail($mail_data, 'emails.notif_edit_user', 'NEED APPROVAL', 'REGISTER');
//                Mail::send('emails.notif_create_user', ['notif' => $notif], function ($m) use ($notif, $user_role) {
//                    $m->from('pusertif@pln.co.id', 'PLN Pusertif');
//                    $m->to($user_role->email, $user_role->username)->subject($notif->subject);
//                });
        }
        return redirect('eksternal/profil/' . $id);
    }

    public function insertExternal(Request $request)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        //dd($timestamp);

        $user = new User();
        //dd(User::orderBy('id','desc')->first()->id+1);
        //       $last = User::orderBy('id','desc')->first();
        // $user->id = ($last == null) ? 1 : $last->id+1;
        $user->email = $request->email_user;
        //$user->username = $request->username;
        $user->username = $request->email_user;
        $user->nama_user = strtoupper($request->nama_user);
        //$user->password = $request->password;
        $user->tempat_lahir_user = strtoupper($request->tempat_lahir);
        $user->gender_user = strtoupper($request->gender_user);
        $user->tanggal_lahir = Carbon::parse($request->tanggal_lahir)->format('Y-m-d H:i:s');//date('Y-m-d H:i:s', strtotime($request->tgl_lahir));
        //dd($user->tanggal_lahir);
        $user->alamat_user = $request->alamat_user;
        $user->no_ktp_user = $request->no_ktp_user;
        $user->no_hp_user = $request->no_hp_user;
        $user->is_pln = '0';
        $user->jenis_user = "EKSTERNAL";
        /*--------------khusus user external-------------------------------------------------------------*/

        $nm_u_custom = '-' . $timestamp . '-' . $request->email_user . '.';
        $user->file_npwp_user = ($request->hasFile('file_npwp_user')) ? 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension() : "";
        $user->file_ktp_user = ($request->hasFile('file_ktp_user')) ? 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension() : "";
        $user->file_foto_user = ($request->hasFile('file_foto_user')) ? 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension() : "";
        /*------------------------------------------------------------------------------------------------*/
        $user->status_user = "CREATED";
        $user->save();
        //simpan user di var local
        $this->user_pj = $user;

        $perusahaan = new Perusahaan();
        // $perusahaan->id = Perusahaan::orderBy('id', 'desc')->first()->id+1;
        $perusahaan->nama_perusahaan = strtoupper($request->nama_perusahaan);
        $perusahaan->nama_pemimpin_perusahaan = strtoupper($request->nama_pemimpin_perusahaan);
        $perusahaan->no_npwp_perusahaan = $request->no_npwp_perusahaan;
        $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
        $perusahaan->id_province = $request->provinsi;
        $perusahaan->id_city = $request->kabupaten;
        $perusahaan->kode_pos_perusahaan = $request->kode_pos_perusahaan;
        $perusahaan->no_telepon_perusahaan = $request->no_telepon_perusahaan;
        $perusahaan->no_fax_perusahaan = $request->no_fax_perusahaan;
        $perusahaan->email_perusahaan = ($request->email_perusahaan != null) ? $request->email_perusahaan : "";


        $nm_p_custom = '-' . $timestamp . '-' . $request->email_perusahaan . '.';
        $perusahaan->file_npwp_perusahaan = ($request->hasFile('file_npwp_perusahaan')) ? 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension() : "";
        $perusahaan->file_siup_perusahaan = ($request->hasFile('file_surat_ijin_usaha')) ? 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension() : "";
        $perusahaan->file_skdomisili_perusahaan = ($request->hasFile('file_skdp')) ? 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension() : "";
        $perusahaan->file_situ_perusahaan = ($request->hasFile('file_situ')) ? 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension() : "";
        $perusahaan->file_tdp_perusahaan = ($request->hasFile('file_tdp_perusahaan')) ? 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension() : "";
        //dd($perusahaan);
        $perusahaan->save();

        $pemilik = new PemilikInstalasi();
        //dd($pemilik);
        // $pemilik->id = PemilikInstalasi::orderBy('id', 'desc')->first()->id+1;
        $pemilik->nama_pemilik = $perusahaan->nama_perusahaan;
        $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
        $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
        $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
        $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu)->format('Y-m-d H:i:s');
        $pemilik->nama_kontrak = $request->nama_kontrak;
        $pemilik->no_kontrak = $request->no_kontrak;
        $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak)->format('Y-m-d H:i:s');
        $pemilik->no_spjbtl = $request->no_spjbtl;
        $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl)->format('Y-m-d H:i:s');
        $pemilik->perusahaan_id = $perusahaan->id;
        $pemilik->id_province = $request->provinsi;
        $pemilik->id_city = $request->kabupaten;

        $pemilik->save();

//dd($pemilik);

        /*-----------------------------file upload pemilik instalasi------------------------------------*/
        if ($request->hasFile('file_surat_iu')) {
            $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $nm_u_custom . $request->file_surat_iu->getClientOriginalExtension());
        }

        if ($request->hasFile('file_kontrak_sewa')) {
            $pemilik->file_kontrak = 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $nm_u_custom . $request->file_kontrak_sewa->getClientOriginalExtension());
        }

        if ($request->hasFile('file_spjbtl')) {
            $pemilik->file_spjbtl = 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_spjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $nm_u_custom . $request->file_spjbtl->getClientOriginalExtension());
        }

        $pemilik->save();

        //simpan perusahaan di var local
        $this->perusahaan_pj = $perusahaan;
//        $user->perusahaan()->save($perusahaan);
        $user = User::findOrFail($user->id);
        $user->perusahaan_id = $perusahaan->id;
        $user->save();
        /*$perusahaan->save();
        $user->perusahaan_id = $perusahaan->id;
        $user->save();*/

        /*----move user file upload*/
        if ($request->hasFile('file_npwp_user')) {
            $filename = $user->file_npwp_user;
            $fullPath = storage_path() . '/upload/npwp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user' . $nm_u_custom . $request->file_npwp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_ktp_user')) {
            $filename = $user->file_ktp_user;
            $fullPath = storage_path() . '/upload/ktp_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user' . $nm_u_custom . $request->file_ktp_user->getClientOriginalExtension());
        }

        if ($request->hasFile('file_foto_user')) {
            $filename = $user->file_foto_user;
            $fullPath = storage_path() . '/upload/foto_user/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user' . $nm_u_custom . $request->file_foto_user->getClientOriginalExtension());
        }
        /* $request->file_npwp_user->move(storage_path() . '/upload/npwp_user', 'npwp_user_' . $user->email . '.' . $request->file_npwp_perusahaan->getClientOriginalExtension());
         $request->file_ktp_user->move(storage_path() . '/upload/ktp_user', 'ktp_user_' . $user->email . '.' . $request->file_ktp_user->getClientOriginalExtension());
         $request->file_foto_user->move(storage_path() . '/upload/foto_user', 'foto_user_' . $user->email . '.' . $request->file_foto_user->getClientOriginalExtension());*/
        /*--------------------*/

        /*----move perusahaan file upload*/
        if ($request->hasFile('file_npwp_perusahaan')) {
            $filename = $perusahaan->file_npwp_perusahaan;
            $fullPath = storage_path() . '/upload/npwp_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan' . $nm_p_custom . $request->file_npwp_perusahaan->getClientOriginalExtension());
        }

        if ($request->hasFile('file_surat_ijin_usaha')) {
            $filename = $perusahaan->file_siup_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha' . $nm_p_custom . $request->file_surat_ijin_usaha->getClientOriginalExtension());
        }

        if ($request->hasFile('file_skdp')) {
            $filename = $perusahaan->file_skdp;
            $fullPath = storage_path() . '/upload/surat_keterangan_domisili_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan' . $nm_p_custom . $request->file_skdp->getClientOriginalExtension());
        }

        if ($request->hasFile('file_situ')) {
            $filename = $perusahaan->file_situ_perusahaan;
            $fullPath = storage_path() . '/upload/surat_ijin_tempat_usaha/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha' . $nm_p_custom . $request->file_situ->getClientOriginalExtension());
        }

        if ($request->hasFile('file_tdp_perusahaan')) {
            $filename = $user->file_tdp_perusahaan;
            $fullPath = storage_path() . '/upload/tanda_daftar_perusahaan/' . $filename;
            if (File::exists($fullPath)) File::delete($fullPath);
            $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan' . $nm_p_custom . $request->file_tdp_perusahaan->getClientOriginalExtension());
        }
        /*
        $request->file_npwp_perusahaan->move(storage_path() . '/upload/npwp_perusahaan', 'npwp_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_npwp_perusahaan->getClientOriginalExtension());
        $request->file_surat_ijin_usaha->move(storage_path() . '/upload/surat_ijin_usaha', 'surat_ijin_usaha_' . $perusahaan->email_perusahaan . '.' . $request->file_surat_ijin_usaha->getClientOriginalExtension());
        $request->file_skdp->move(storage_path() . '/upload/surat_keterangan_domisili_perusahaan', 'surat_keterangan_domisili_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_skdp->getClientOriginalExtension());
        $request->file_situ->move(storage_path() . '/upload/surat_ijin_tempat_usaha', 'surat_ijin_tempat_usaha_' . $perusahaan->email_perusahaan . '.' . $request->file_situ->getClientOriginalExtension());
        $request->file_tdp_perusahaan->move(storage_path() . '/upload/tanda_daftar_perusahaan', 'tanda_daftar_perusahaan_' . $perusahaan->email_perusahaan . '.' . $request->file_tdp_perusahaan->getClientOriginalExtension());*/
        /*-------------------------------*/

        //notify to user email
        $user->nama_perusahaan = $perusahaan->nama_perusahaan;
        $user->alamat_perusahaan = $perusahaan->alamat_perusahaan;
        $mail_user = $user;
        $mail_user->registerID = $this->makeIDRegisterExternal();
        //call send email helper
        $mail_data = array(
            "data" => $user,
            "data_alias" => "user",
            "to" => $mail_user->email,
            "to_name" => $mail_user->nama_user,
            "subject" => "Registrasi User",
            "description" => "Registrasi User Eksternal"
        );
        sendEmail($mail_data, 'emails.create_user', 'CREATE', 'REGISTER');
//        Mail::send('emails.create_user', ['user' => $user], function ($m) use ($mail_user) {
//            $m->from('pusertif@pln.co.id', 'PLN Pusertif');
//            $m->to($mail_user->email, $mail_user->nama_user)->subject('NEW USER CREATED');
//        });
        return $user;
    }


    protected function makeIDRegisterExternal()
    {
        $jumlahuser = User::where('perusahaan_id', '<>', '')->count();
        $userterakhir = User::where('perusahaan_id', '<>', '')->orderby('no_registrasi', 'desc')->first();
        $angka = $jumlahuser + 1;
        if ($jumlahuser == 0) {
            $angkaterakhir = '0000';
        } else {
            $angkaterakhir = substr($userterakhir->no_registrasi, 4, 4);
        }

        if ($angka <= 9999) {
            if ($angka < 10) {
                $idpel = '000' . $angka;
            } else if ($angka < 100) {
                $idpel = '00' . $angka;
            } else if ($angka < 1000) {
                $idpel = '0' . $angka;
            } else {
                $idpel = $angka;
            }
        } else if ($angka == 10000) {
            $idpel = 'A000';
        } else {
            $a = substr($angkaterakhir, 0, 1);
            $b = substr($angkaterakhir, 1, 3) + 1;

            if ($b > 999) {
                $a++;
            }

            if ($b < 10) {
                $b = '00' . $b;
            } else if ($b < 100) {
                $b = '0' . $b;
            } else if ($b <= 999) {
                $b = $b;
            } else {
                $b = '000';
            }
            $idpel = strtoupper($a) . $b;
        }


        $now = new DateTime();
        $now = substr($now->format('Y'), 2, 2);

        return "0000" . $idpel . $now;
    }
}
