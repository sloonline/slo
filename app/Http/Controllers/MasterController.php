<?php

namespace App\Http\Controllers;

use App\AreaProgram;
use App\BayGardu;
use App\Bidang;
use App\BusinessArea;
use App\Cities;
use App\FormHilo;
use App\GarduArea;
use App\GarduInduk;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\BidangRequest;
use App\Http\Requests\ModulRequest;
use App\InstalasiDistribusi;
use App\InstalasiPemanfaatanTM;
use App\InstalasiPemanfaatanTT;
use App\InstalasiPembangkit;
use App\InstalasiTransmisi;
use App\JabatanInspeksi;
use App\JenisInstalasi;
use App\JenisProgram;
use App\Keahlian;
use App\KeahlianPersonil;
use App\Kontrak;
use App\KontrakProgram;
use App\Kontraktor;
use App\LayananSuratTugas;
use App\LingkupDistribusi;
use App\LingkupPekerjaan;
use App\LingkupProgram;
use App\Lokasi;
use App\LokasiArea;
use App\Modul;
use App\ModulRole;
use App\PemilikInstalasi;
use App\Penyulang;
use App\PenyulangArea;
use App\Peralatan;
use App\Permission;
use App\PermissionRole;
use App\Permohonan;
use App\PersonilInspeksi;
use App\PersonilTraining;
use App\Perusahaan;
use App\Program;
use App\Provinces;
use App\References;
use App\Role;
use App\StatusPekerja;
use App\StrukturHilo;
use App\SubBidang;
use App\SuratTugas;
use App\TipeInstalasi;
use App\Training;
use App\UnsurBiaya;
use App\User;
use App\Workflow;
use Carbon\Carbon;
use Faker\Provider\Person;
use Guzzle\Service\Command\RequestSerializerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Expr\AssignOp\Mod;
use App\Http\Controllers\Input;
use App\HistoryPeralatan;
use App\PersonilBidang;
use App\LingkupPersonil;
use App\JabatanPersonil;

class MasterController extends Controller
{
    /* ====-MASTER BIDANG START==============================---- */

    public function showBidang()
    {
        $bidang = Bidang::all();
        return view('master/bidang', compact('bidang'));
    }

    public function insertBidang($id = "")
    {
        //$bidang = Bidang::find(1);
        $id = ($id == "") ? 0 : $id;
        $bidang = null;

        if ($id > 0) {
            $bidang = Bidang::find($id);
        }
        return view('master/input_bidang', compact('bidang'));
    }

    public function storeBidang(BidangRequest $request)
    {
        //        dd($request->input('id'));
        $id = $request->id;
        if ($id == 0) {
            //insert
            $bidang = new Bidang();
            $bidang->nama_bidang = $request->nama_bidang;
            $bidang->ket_bidang = $request->ket_bidang;
        } else {
            //update
            $bidang = Bidang::find($id);
            $bidang->nama_bidang = $request->nama_bidang;
            $bidang->ket_bidang = $request->ket_bidang;
        }
        $bidang->save();
        return redirect('master/bidang');
    }

    public function isAktifBidang($id)
    {
        $bidang = Bidang::find($id);
        if ($bidang->isaktif == 0) {
            $bidang->isaktif = 1;
        } else {
            $bidang->isaktif = 0;
        }
        $bidang->save();
        return redirect('master/bidang');
    }

    /* ===============MASTER BIDANG END====================================== */


    /* ====-MASTER SUB BIDANG START==============================---- */

    public function showSubBidang()
    {
        $sub_bidang = SubBidang::all();
        return view('master/sub_bidang', compact('sub_bidang'));
    }

    public function insertSubBidang($id = "")
    {
        $id = ($id == "") ? 0 : $id;
        $sub_bidang = null;
        $bidang = Bidang::where('isAktif', 1)->get();
        if ($id > 0) {
            $sub_bidang = SubBidang::find($id);
        }
        return view('master/input_sub_bidang', compact('sub_bidang', 'bidang'));
    }

    public function storeSubBidang(Requests\SubBidangRequest $request)
    {
        $id = $request->id;
        if ($id == 0) {
            //insert
            $sub_bidang = new SubBidang();
        } else {
            //update
            $sub_bidang = SubBidang::find($id);
        }
        $sub_bidang->nama_sub_bidang = $request->nama_sub_bidang;
        $sub_bidang->id_bidang = $request->id_bidang;
        $sub_bidang->save();
        return redirect('master/sub_bidang');
    }

    public function isAktifSubBidang($id)
    {
        $sub_bidang = SubBidang::find($id);
        if ($sub_bidang->isaktif == 0) {
            $sub_bidang->isaktif = 1;
        } else {
            $sub_bidang->isaktif = 0;
        }
        $sub_bidang->save();
        return redirect('master/sub_bidang');
    }

    /* ===============MASTER SUB BIDANG END====================================== */

    /* ====-MASTER MODUL START==============================---- */

    public function showModul()
    {
        $modul = Modul::all();
        return view('master/modul', compact('modul'));
    }

    public function insertModul($id = "")
    {
        //$bidang = Bidang::find(1);
        $id = ($id == "") ? 0 : $id;
        $modul = null;

        if ($id > 0) {
            $modul = Bidang::find($id);
        }
        return view('master/input_modul', compact('modul'));
    }

    public function storeModul(ModulRequest $request)
    {
        $id = $request->id;
        $modul = ($id == 0) ? new Modul() : Modul::findOrFail($id);
        $modul->nama_modul = $request->nama_modul;
        $modul->kode = $request->kode;
        $modul->save();
        return redirect('master/modul');
    }

    public function isAktifModul($id)
    {
        $modul = Modul::find($id);
        if ($modul->isaktif == 0) {
            $modul->isaktif = 1;
        } else {
            $modul->isaktif = 0;
        }
        $modul->save();
        return redirect('master/modul');
    }

    /* ===============MASTER MODUL END====================================== */


    /* ====-MASTER HAK AKSES==============================---- */

    public function showHakAkses()
    {
        $permission = Permission::withTrashed()->get();
        return view('master/hak_akses', compact('permission'));
    }

    public function insertHakAkses($id = 0)
    {
        $permission = Permission::withTrashed()->find($id);
        $modul = Modul::all();
        return view('master/input_hak_akses', compact('permission', 'modul'));
    }

    public function storeHakAkses(ModulRequest $request)
    {
        $id = $request->id;
        $permission = ($id == 0) ? new Permission() : Permission::withTrashed()->findOrFail($id);
        $permission->modul_id = $request->modul_id;
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        return redirect('master/hak_akses');
    }

    public function isAktifHakAkses($id)
    {
        $permission = Permission::withTrashed()->findOrFail($id);
        if ($permission->trashed()) {
            $permission->restore();
        } else {
            $permission->delete();
        }
        return redirect('master/hak_akses');
    }

    /* ===============MASTER HAK AKSES====================================== */

    /* ===============MASTER HAK AKSES MODUL START====================================== */

    public function showHakAksesModul()
    {
        $roles = Role::orderBy('display_name', 'asc')->get();
        return view("master/modul_role", compact("roles"));
    }

    public function inputHakAksesModul(Request $request)
    {
        //jika baru pilih role saja
        if (isset($request->arr_permission)) {
            $this->storeHakAksesModul($request);
        }
        $roles = Role::orderBy('display_name', 'asc')->get();
        $permission_role = PermissionRole::where('role_id', $request->id_role)->get();
        $role = Role::findOrFail($request->id_role);
        $id_permission = PermissionRole::select('permission_id')->where('role_id', $request->id_role)->get();
        $modules = Modul::orderBy('id', 'asc')->get();
        $permission = Permission::whereNotIn('id', $id_permission)->get();
        return view("master/modul_role", compact("roles", "modules", "permission_role", "role", 'permission'));
    }

    private function storeHakAksesModul(Request $request)
    {
        $id_role = $request->id_role;
        $role = Role::findOrFail($id_role);
        $arr_permission = explode(" ", $request->arr_permission);
        //delete dulu semua yg ada sebelumnya
        PermissionRole::where("role_id", $id_role)->delete();
        //insert yg baru
        foreach ($arr_permission as $permission) {
            if ($permission != "") {
                $permission_role = new PermissionRole();
                $permission_role->role_id = $id_role;
                $permission_role->permission_id = $permission;
                $permission_role->save();
            }
        }
        Session::flash('message', 'Pengaturan level akses ' . $role->nama_role . ' berhasil dilakukan!');
    }

    /* ===============MASTER HAK AKSES MODUL END====================================== */

    /* ====-MASTER ROLES START==============================---- */

    public function showLevelAkses()
    {
        $roles = Role::withTrashed()->get();
        return view('master/roles', compact('roles'));
    }

    public function insertLevelAkses($id = "")
    {
        $id = ($id == "") ? 0 : $id;
        $role = null;

        if ($id > 0) {
            $role = Role::withTrashed()->findOrFail($id);
        }
        return view('master/input_roles', compact('role'));
    }

    public function storeLevelAkses(Request $request)
    {
        $id = $request->id;
        if ($id == 0) {
            //insert
            $role = new Role();
            $role->name = $request->name;
            $role->display_name = $request->display_name;
        } else {
            //update
            $role = Role::withTrashed()->findOrFail($id);
            $role->name = $request->name;
            $role->display_name = $request->display_name;
        }
        $role->save();
        return redirect('master/level_akses');
    }

    public function isAktifLevelAkses($id)
    {
        $role = Role::withTrashed()->findOrFail($id);
        if ($role->trashed()) {
            $role->restore();
        } else {
            $workflow = Workflow::where('role_id', $id)->get();
            if(sizeOf($workflow) > 0){
                return redirect('master/level_akses')->with('error', 'Role tidak dapat dihapus karena digunakan pada Master Workflow!');;
            } else {
                $role->delete();
            }
        }
        return redirect('master/level_akses');
    }

    /* ===============MASTER ROLES END====================================== */

    /* MASTER PEMILIK INSTALASI */
    public function showPemilikInstalasi()
    {
        $perusahaan_id = Auth::user()->perusahaan_id;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', $perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();
        $view = '';
        if (Auth::user()->jenis_user == TIPE_INTERNAL) {
            $view = 'master.view_pemilik_instalasi';
        } else {
            $view = 'eksternal/view_pemilik_instalasi';
        }
        return view($view, compact('pemilik'));
    }

    public function insertPemilikInstalasi($id = null)
    {
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        $pemilik = ($id != null) ? PemilikInstalasi::findOrFail($id) : null;
        if (Auth::user()->jenis_user == TIPE_INTERNAL) {
            $view = 'master.create_pemilik_instalasi';
        } else {
            $view = 'eksternal/create_pemilik_instalasi';
        }
        return view($view, compact('province', 'city', 'pemilik', 'jenis_ijin_usaha'));
    }

    public function detailPemilikInstalasi($id = null)
    {
        $pemilik = ($id != null) ? PemilikInstalasi::findOrFail($id) : null;

        if (Auth::user()->jenis_user == TIPE_INTERNAL) {
            $view = 'master.detail_pemilik_instalasi';
        } else {
            $view = 'eksternal/detail_pemilik_instalasi';
        }
        return view($view, compact('pemilik'));
    }

    public function storePemilikInstalasi(Request $request)
    {
        $id = $request->id;
        $pemilik = new PemilikInstalasi();
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

        if ($id != null) {
            //update
            $pemilik = PemilikInstalasi::FindorFail($id);
            $pemilik->user_update = Auth::user()->username;
        } else {
            $pemilik->user_create = Auth::user()->username;
        }

        $pemilik->nama_pemilik = $request->nama_pemilik;
        $pemilik->alamat_pemilik = $request->alamat_pemilik;
        $pemilik->id_province = $request->provinsi;
        $pemilik->id_city = $request->kabupaten;
        $pemilik->kode_pos_pemilik = $request->kode_pos;
        $pemilik->telepon_pemilik = $request->telepon;
        $pemilik->no_fax_pemilik = $request->no_fax;
        $pemilik->email_pemilik = $request->email_pemilik;
        $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
        $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
        $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
        $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
        if ($request->has_sewa == 1) {
            $pemilik->nama_kontrak = $request->nama_kontrak;
            $pemilik->no_kontrak = $request->no_kontrak;
            $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
            $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
        } else {
            $pemilik->nama_kontrak = '';
            $pemilik->no_kontrak = '';
            $pemilik->tgl_pengesahan_kontrak = null;
            $pemilik->masa_berlaku_kontrak = null;
        }
        $pemilik->no_spjbtl = $request->no_spjbtl;
        $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
        $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
        $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
        $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
        /* $model->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
        $model->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
        $model->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension(); */

        $pemilik->save();

        /* -----------------------------file upload pemilik instalasi------------------------------------ */
        if ($request->hasFile('file_surat_iu')) {
            $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
        }

        if ($request->has_sewa == 1) {
            if ($request->hasFile('file_kontrak_sewa')) {
                $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
            }
        } else {
            $pemilik->file_kontrak = null;
        }

        if ($request->hasFile('file_spjbtl')) {
            $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
            $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
        }

        $pemilik->save();
        /* ----------------------------end file upload pemilik instalasi---------------------------------- */
        Session::flash('message', 'Data Pemilik Instalasi Berhasil Tersimpan');

        if (Auth::user()->jenis_user == TIPE_INTERNAL) {
            $url = 'master/pemilik_instalasi';
        } else {
            $url = 'eksternal/pemilik_instalasi';
        }

        return redirect($url);
    }

    public function deletePemilikInstalasi($id)
    {
        try {
            DB::table('pemilik_instalasi')->where('id', $id)->delete();
            Session::flash('message', 'Pemilik Instalasi Berhasil Dihapus');
        } catch (\Exception $e) {
            Session::flash('error', 'Pemilik instalasi tidak dapat dihapus. Data sedang digunakan.');
        }

        if (Auth::user()->jenis_user == TIPE_INTERNAL) {
            $url = 'master/pemilik_instalasi';
        } else {
            $url = 'eksternal/pemilik_instalasi';
        }

        return redirect($url);
    }

    /* ====================END MASTER PEMILIK INSTALASI================================ */

    /* MASTER INSTALASI PEMBANGKIT */

    public function showPembangkit()
    {
        $pembangkit = InstalasiPembangkit::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        return view('eksternal/view_instalasi_pembangkit', compact('pembangkit'));
    }

    public function detailPembangkit($id)
    {
        $instalasi = InstalasiPembangkit::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(1);
        $layanan = $tipe_instalasi->produk()->get();
        $lingkup_pekerjaan = $instalasi->jenis_instalasi->lingkupPekerjaan()->get();
        $permohonan = $instalasi->getRiwayat();
        $selected_layanan = array();

        //kelompokan berdasarkan jenis instalasi unuk memudahkan pencarian nanti
        $used = Permohonan::getaGroupedOrdered(ID_PEMBANGKIT, $id);

        return view('eksternal/detail_instalasi_pembangkit', compact('instalasi', 'layanan', 'selected_layanan', 'lingkup_pekerjaan', 'tipe_instalasi', 'permohonan', 'used'));
    }

    public function insertPembangkit($id = null)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 1)->get(); //sementara kondisi manual
        $ref_parent = References::where('nama_reference', 'Jenis Bahan Bakar')->first();
        $jenis_bahan_bakar = References::where('parent', $ref_parent->id)->get();
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $pembangkit = ($id != null) ? InstalasiPembangkit::findOrFail($id) : null;
        //         print_r($pembangkit);
        //        exit();

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        //        $perusahaan         = Auth::user()->perusahaan->pemilikInstalasi;
        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();

        $list_kontraktor = Kontraktor::all();
        return view('eksternal/create_instalasi_pembangkit2',
            compact('jenis_instalasi', 'province', 'city', 'jenis_bahan_bakar', 'pemilik', 'pembangkit',
                'jenis_ijin_usaha', 'pemilik_instalasi', 'list_kontraktor'));
    }

    public function storePembangkit(Request $request)
    {
//        dd($request);
        $timestamp = date('YmdHis');
        $id = $request->id;
        $pembangkit = new InstalasiPembangkit();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;
        if ($id != 0) {
            //update
            $pembangkit = InstalasiPembangkit::findOrFail($id);
        }

        //Data Umum Instalasi
        $pembangkit->status_baru = $request->status_baru;
        $pembangkit->status_slo = $request->status_slo;
        $pembangkit->id_jenis_instalasi = $request->jenis_instalasi;
        $pembangkit->nama_instalasi = $request->nama_instalasi;
        $pembangkit->nomor_pembangkit = $request->no_unit_pembangkit;
        $pembangkit->nomor_generator = $request->no_seri_generator;
        $pembangkit->nomor_mesin = $request->no_seri_turbin;
        $pembangkit->jumlah_modul = $request->jumlah_modul;
        $pembangkit->jumlah_inverter = $request->jumlah_inverter;
        $pembangkit->id_bahan_bakar = $request->jenis_bahan_bakar;
        $pembangkit->kode_kontraktor = $request->kode_kontraktor;
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $pembangkit->kontraktor = $kontraktor->nama;
        } else {
            $pembangkit->kontraktor = "";
        }
        #cek jika status instalasi lama, set kontraktor menjadi 0000
        if ($request->status_baru == 0) {
            $pembangkit->kode_kontraktor = "0000";
            $pembangkit->kontraktor = "PT. Tidak Ada Pembangunan";
        }

        //        Kapasitas
        $pembangkit->kapasitas_terpasang = $request->kapasitas_terpasang;
        $pembangkit->kapasitas_hasil_uji = $request->kapasitas_hasil_uji;
        $pembangkit->kapasitas_modul = $request->kapasitas_modul_per_unit;
        $pembangkit->kapasitas_inverter = $request->kapasitas_inverter_per_unit;

        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $pembangkit->pemilik_instalasi_id = $request->pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $pembangkit->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            //            $timestamp                      = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = trim($request->alamat_pemilik);
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();

            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $filename = saveFile($request->file_surat_iu, 'file_siup_pemilik_instalasi', $request->email_pemilik);
                $pemilik->file_siup = $filename;
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }
            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }
            $pemilik->save();

            $pembangkit->pemilik_instalasi_id = $pemilik->id;
        }

        //        Lokasi Instalasi
        $pembangkit->alamat_instalasi = $request->alamat_instalasi;
        $pembangkit->id_provinsi = $request->provinsi;
        $pembangkit->id_kota = $request->kabupaten;
        $pembangkit->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
        $pembangkit->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;
        $pembangkit->created_by = Auth::user()->username;
        $pembangkit->tipe_pemilik = $tipe_pemilik;

        #save lokasi format desimal
        $pembangkit->longitude = $request->longitude;
        $pembangkit->latitude = $request->latitude;

        #save file pembangkit
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $request->email_pemilik);
            $pembangkit->file_sbujk = $filename;
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $request->email_pemilik);
            $pembangkit->file_iujk = $filename;
        }
        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $request->email_pemilik);
            $pembangkit->file_sld = $filename;
        }
        #end save file pembangkit
        $pembangkit->save();

        $nm_u_custom = '-' . $timestamp . '-' . $pembangkit->id . '.';

        if ($request->status_baru == '0') {
            if ($request->hasFile('file_pernyataan')) {
                $pembangkit->file_pernyataan = ($request->hasFile('file_pernyataan')) ? 'file_pernyataan-pembangkit' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension() : "";
                $filename = $pembangkit->file_pernyataan;
                $fullPath = storage_path() . '/upload/file_pernyataan/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataan->move(storage_path() . '/upload/file_pernyataan', 'file_pernyataan-pembangkit' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension());
            }
        } else {
            $pembangkit->file_pernyataan = '';
        }

        if ($request->hasFile('file_foto_1')) {
            $pembangkit->foto_1 = ($request->hasFile('file_foto_1')) ? 'foto_kit_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension() : "";
            $filename = $pembangkit->foto_1;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_1->move(storage_path() . '/upload/foto_instalasi', 'foto_kit_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension());
        }
        if ($request->hasFile('file_foto_2')) {
            $pembangkit->foto_2 = ($request->hasFile('file_foto_2')) ? 'foto_kit_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension() : "";
            $filename = $pembangkit->foto_2;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_2->move(storage_path() . '/upload/foto_instalasi', 'foto_kit_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension());
        }


        $pembangkit->save();

        //        Session::flash('message', 'Data Instalasi Pembangkit berhasil disimpan');
        return redirect('eksternal/view_instalasi_pembangkit')->with('success', 'Data Instalasi Pembangkit berhasil disimpan');
    }

    public function deletePembangkit($id)
    {
        $pembangkit = InstalasiPembangkit::findOrFail($id);
        //cek dulu apakah dipakai di permohonan atau tidak, jika daipakai maka tidak dapat dihapus
        if (sizeof($pembangkit->getPermohonan()) > 0) {
            Session::flash('message', 'Data Instalasi Digunakan dalam data order. Tidak dapat dihapus');
        } else {
            $pembangkit->delete();
            Session::flash('message', 'Data Instalasi Pembangkit Berhasil Dihapus');
        }
        return redirect('eksternal/view_instalasi_pembangkit');
    }

    /* END MASTER INSTALASI PEMBANGKIT */

    /* MASTER INSTALASI TRANSMISI */

    public function showTransmisi()
    {
        $transmisi = InstalasiTransmisi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        return view('eksternal/view_instalasi_transmisi', compact('transmisi'));
    }

    public function detailTransmisi($id)
    {
        $instalasi = InstalasiTransmisi::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_TRANSMISI);
        $layanan = $tipe_instalasi->produk()->get();
        $lingkup_pekerjaan = $instalasi->jenis_instalasi->lingkupPekerjaan()->get();
        $permohonan = $instalasi->getRiwayat();
        $selected_layanan = null;
        $bay = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->get());
        $file_sbujk = array();
        $file_iujk = array();
        $file_sld = array();
        foreach ($bay as $row) {
            if ($row->file_sbujk != null) $file_sbujk[$row->id] = $row->file_sbujk;
            if ($row->file_iujk != null) $file_iujk[$row->id] = $row->file_iujk;
            if ($row->file_sld != null) $file_sld[$row->id] = $row->file_sld;

        }
        //kelompokan berdasarkan jenis instalasi unuk memudahkan pencarian nanti
        $used = Permohonan::getaGroupedOrdered(ID_TRANSMISI, $id);
        return view('eksternal/detail_instalasi_transmisi', compact('bay', 'file_sbujk', 'file_iujk', 'file_sld', 'instalasi', 'layanan', 'selected_layanan', 'lingkup_pekerjaan', 'tipe_instalasi', 'permohonan', 'used'));
    }

    public function insertTransmisi($id = null)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 2)->get(); //sementara kondisi manual

        $ref_parent = References::where('nama_reference', 'Sistem Jaringan Transmisi')->first();
        $sistem_jaringan = References::where('parent', $ref_parent->id)->get();

        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();

        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $transmisi = ($id != null) ? InstalasiTransmisi::findOrFail($id) : null;
        $tipe_transmisi = ($transmisi != null) ? @$transmisi->jenis_instalasi->keterangan : JENIS_TRANSMISI;

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();

        //DATA BAY
        $bay = array();
        $bay[BAY_LINE] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_LINE)->get());
        $bay[BAY_KAPASITOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_KAPASITOR)->get());
        $bay[BAY_REAKTOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_REAKTOR)->get());
        $bay[BAY_TRANSFORMATOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_TRANSFORMATOR)->get());
        $bay[BAY_COUPLER] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_COUPLER)->get());
        $bay[PHB] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', PHB)->get());
        $bay[BAY_CUSTOM] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $id)->where('jenis_bay', BAY_CUSTOM)->get());
        $list_kontraktor = Kontraktor::all();
        return view('eksternal/create_instalasi_transmisi2',
            compact('bay', 'tegangan_pengenal', 'sistem_jaringan', 'jenis_instalasi', 'province', 'city',
                'jenis_bahan_bakar', 'pemilik', 'transmisi', 'jenis_ijin_usaha', 'pemilik_instalasi', 'list_kontraktor'));
    }

    public function storeTransmisi(Request $request)
    {
        $timestamp = date('YmdHis');
        $id = $request->id;
        $transmisi = new InstalasiTransmisi();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;

        if ($id != 0) {
            //update
            $transmisi = InstalasiTransmisi::findOrFail($id);
        }

        //      Data Umum
        $transmisi->status_baru = $request->status_baru;
        $transmisi->status_slo = $request->status_slo;
        $transmisi->id_jenis_instalasi = $request->jenis_instalasi;
        $transmisi->nama_instalasi = $request->nama_instalasi;
        $transmisi->sis_jar_tower = $request->sistem_jaringan_transmisi;
        $transmisi->panjang_tt = $request->panjang_saluran;
        $transmisi->jml_tower = $request->jumlah_tower;
        $transmisi->jenis_saluran = $request->jenis_saluran;


        $jenis = JenisInstalasi::find($request->jenis_instalasi);
        //        JIKA JARINGAN TRANSMISI
        if ($jenis->keterangan != JENIS_GARDU) {
            $transmisi->sutet = $request->sutet;
            $transmisi->sktt = $request->sktt;
            $transmisi->sklt = $request->sklt;
        } else {
            $transmisi->jenis_saluran = "";
        }

        $transmisi->kode_kontraktor = $request->kode_kontraktor;
        if ($transmisi->status_baru == 0) {
            $request->kode_kontraktor = '0000';
            $transmisi->kode_kontraktor = '0000';
        }
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $transmisi->kontraktor = $kontraktor->nama;
        } else {
            $transmisi->kontraktor = "";
        }

        //      Kapasitas
        $transmisi->kapasitas_gi = $request->kapasitas_gardu_induk;
        $transmisi->kap_pemutus_tenaga = $request->kapasitas_pemutus_tenaga;
        $transmisi->kap_tf_tenaga = $request->kapasitas_transformator_tenaga;
        $transmisi->kap_bank_kapasitor = $request->kapasitas_bank_kapasitor;
        $transmisi->kap_bank_reaktor = $request->kapasitas_bank_reaktor;
        $transmisi->tegangan_pengenal = $request->tegangan_pengenal;


        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $transmisi->pemilik_instalasi_id = $request->pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $transmisi->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            //            $timestamp                      = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = trim($request->alamat_pemilik);
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();


            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }

            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $transmisi->pemilik_instalasi_id = $pemilik->id;
        }

        //      Lokasi Instalasi
        $transmisi->alamat_instalasi = $request->alamat_instalasi;
        $transmisi->id_provinsi = $request->provinsi;
        $transmisi->id_kota = $request->kabupaten;
        $transmisi->alamat_akhir_instalasi = $request->alamat_akhir_instalasi;
        $transmisi->id_provinsi_akhir = $request->provinsi_akhir;
        $transmisi->id_kota_akhir = $request->kabupaten_akhir;
//        $transmisi->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
//        $transmisi->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;

        #koordinat desimal
        $transmisi->long_awal = $request->long_awal;
        $transmisi->lat_awal = $request->lat_awal;

        if ($jenis->keterangan != JENIS_GARDU) {
//            $transmisi->longitude_akhir = $request->longitude_akhir_deg . ' ° ' . $request->longitude_akhir_min . ' \' ' . $request->longitude_akhir_sec . ' " ' . $request->longitude_akhir_dir;
//            $transmisi->latitude_akhir = $request->latitude_akhir_deg . ' ° ' . $request->latitude_akhir_min . ' \' ' . $request->latitude_akhir_sec . ' " ' . $request->latitude_akhir_dir;
            #koordinat desimal
            $transmisi->long_akhir = $request->long_akhir;
            $transmisi->lat_akhir = $request->lat_akhir;
        }


        $transmisi->created_by = Auth::user()->username;
        $transmisi->tipe_pemilik = $tipe_pemilik;

        //        $transmisi->pemilik_instalasi_id = $request->pemilik_instalasi;

        $transmisi->save();

        //        INSERT DETAIL BAY UNTUK GARDU INDUK

//        if ($jenis->keterangan == JENIS_GARDU) {
//
//            $bay_line = json_decode($request->data_bay_line);
//            $bay_kapasitor = json_decode($request->data_bay_kapasitor);
//            $bay_coupler = json_decode($request->data_bay_coupler);
//            $bay_transformator = json_decode($request->data_bay_transformator);
//            $bay_reaktor = json_decode($request->data_bay_reaktor);
//            $phb = json_decode($request->data_phb);
//            $bay_custom = json_decode($request->data_bay_custom);
//            $all_bay = array_merge($bay_line, $bay_kapasitor);
//            $all_bay = array_merge($all_bay, $bay_coupler);
//            $all_bay = array_merge($all_bay, $bay_transformator);
//            $all_bay = array_merge($all_bay, $bay_reaktor);
//            $all_bay = array_merge($all_bay, $phb);
//            $all_bay = array_merge($all_bay, $bay_custom);
//            $this->storeBay($all_bay, $transmisi);
//        }
        $nm_u_custom = '-' . $timestamp . '-' . $transmisi->id . '.';
        if ($request->status_baru == '0') {
            if ($request->hasFile('file_pernyataan')) {
                $transmisi->file_pernyataan = ($request->hasFile('file_pernyataan')) ? 'file_pernyataan-transmisi' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension() : "";
                $filename = $transmisi->file_pernyataan;
                $fullPath = storage_path() . '/upload/file_pernyataan/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataan->move(storage_path() . '/upload/file_pernyataan', 'file_pernyataan-transmisi' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension());
            }
        } else {
            $transmisi->file_pernyataan = '';
        }

        if ($request->hasFile('file_foto_1')) {
            $transmisi->foto_1 = ($request->hasFile('file_foto_1')) ? 'foto_trs_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension() : "";
            $filename = $transmisi->foto_1;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_1->move(storage_path() . '/upload/foto_instalasi', 'foto_trs_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension());
        }
        if ($request->hasFile('file_foto_2')) {
            $transmisi->foto_2 = ($request->hasFile('file_foto_2')) ? 'foto_trs_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension() : "";
            $filename = $transmisi->foto_2;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_2->move(storage_path() . '/upload/foto_instalasi', 'foto_trs_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension());
        }


        #save file lampiran
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $transmisi->nama_instalasi);
            $transmisi->file_sbujk = $filename;
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $transmisi->nama_instalasi);
            $transmisi->file_iujk = $filename;
        }
        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $transmisi->nama_instalasi);
            $transmisi->file_sld = $filename;
        }
        #end save file lampiran

        $transmisi->save();


        //        Session::flash('message', 'Data Instalasi Transmisi Berhasil Disimpan');
        return redirect('eksternal/view_instalasi_transmisi')->with('success', 'Data Instalasi Transmisi Berhasil Disimpan');
    }

    private function storeBay($data_bay, $transmisi)
    {
        $existBay = $transmisi->BayGardu;
        foreach ($data_bay as $dt) {
            $bay = ($dt->id > 0) ? BayGardu::find($dt->id) : new BayGardu();
            $bay->nama_bay = $dt->nama_bay;
            $bay->jenis_bay = $dt->jenis_bay;
            $bay->tipe_pemilik = $dt->tipe_pemilik;
            $bay->pemilik_id = $dt->pemilik_id;
            $bay->instalasi_id = $transmisi->id;
            $bay->kapasitas_pemutus = $dt->kapasitas_pemutus;
            $bay->kapasitas_trafo = $dt->kapasitas_trafo;
            $bay->tegangan_pengenal = $dt->tegangan_pengenal;
            $bay->kode_kontraktor = @$dt->kode_kontraktor;
            if (@$dt->kode_kontraktor != '') {
                $kontraktor = Kontraktor::where('kode', $dt->kode_kontraktor)->first();
                $bay->kontraktor = @$kontraktor->nama;
            }
            $bay->save();
            //remove updated bay from exist bay
            foreach ($existBay as $i => $eb) {
                if ($eb->id == $bay->id) {
                    unset($existBay[$i]);
                    break;
                }
            }
        }
        //sisa exist bay, didelete karena tandanya bay tersebut sudah dihapus user
        foreach ($existBay as $eb) {
            $eb->delete();
        }
    }

    public function deleteTransmisi($id)
    {
        $transmisi = InstalasiTransmisi::findOrFail($id);
        //cek dulu apakah dipakai di permohonan atau tidak, jika daipakai maka tidak dapat dihapus
        if (sizeof($transmisi->getPermohonan()) > 0) {
            Session::flash('message', 'Data Instalasi Digunakan dalam data order. Tidak dapat dihapus');
        } else {
            $transmisi->delete();
            Session::flash('message', 'Data Instalasi Transmisi Berhasil Dihapus');
        }
        return redirect('eksternal/view_instalasi_transmisi');
    }

    /* END MASTER INSTALASI TRANSMISI */

    /* MASTER INSTALASI DISTRIBUSI */

    public function showDistribusi()
    {
        $distribusi = InstalasiDistribusi::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        return view('eksternal/view_instalasi_distribusi', compact('distribusi'));
    }

    public function detailDistribusi($id)
    {
        $instalasi = InstalasiDistribusi::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_DISTRIBUSI);
        $layanan = $tipe_instalasi->produk()->get();
//        $lingkup_pekerjaan = $tipe_instalasi->lingkup_pekerjaan()->get();
        $lingkup_pekerjaan = $instalasi->jenis_instalasi->lingkupPekerjaan()->get();
//        dd($lingkup_pekerjaan[0]->lingkupPekerjaan->jenis_lingkup_pekerjaan);
        $permohonan = $instalasi->getRiwayat();
        $selected_layanan = null;
        //kelompokan berdasarkan jenis instalasi unuk memudahkan pencarian nanti
        $used = Permohonan::getaGroupedOrdered(ID_DISTRIBUSI, $id);
        return view('eksternal/detail_instalasi_distribusi', compact('instalasi', 'layanan', 'selected_layanan', 'lingkup_pekerjaan', 'tipe_instalasi', 'permohonan', 'used'));
    }

    public function insertDistribusi($id = null)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 3)->get();  //sementara manual
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $ref_parent = References::where('nama_reference', 'Sistem Jaringan Distribusi')->first();
        $sistem_jaringan_distribusi = References::where('parent', $ref_parent->id)->get();
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $distribusi = ($id != null) ? InstalasiDistribusi::findOrFail($id) : null;
        $jenis_program = JenisProgram::all();

        // longitude awal
//        $longitude_awal = '';
//        $longitude_awal = explode(' ° ', ($id != null) ? $distribusi->longitude_awal : '');
//        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
        // longitude akhir
//        $longitude_akhir = '';
//        $longitude_akhir = explode(' ° ', ($id != null) ? $distribusi->longitude_akhir : '');
//        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
        // latitude awal
//        $latitude_awal = '';
//        $latitude_awal = explode(' ° ', ($id != null) ? $distribusi->latitude_awal : '');
//        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
        // latitude akhir
//        $latitude_akhir = '';
//        $latitude_akhir = explode(' ° ', ($id != null) ? $distribusi->latitude_akhir : '');
//        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';

//        array_add($distribusi, 'longitude_awal_deg', $longitude_awal_deg);
//        array_add($distribusi, 'longitude_awal_min', $longitude_awal_min);
//        array_add($distribusi, 'longitude_awal_sec', $longitude_awal_sec);
//        array_add($distribusi, 'longitude_awal_dir', $longitude_awal_dir);
//        array_add($distribusi, 'longitude_akhir_deg', $longitude_akhir_deg);
//        array_add($distribusi, 'longitude_akhir_min', $longitude_akhir_min);
//        array_add($distribusi, 'longitude_akhir_sec', $longitude_akhir_sec);
//        array_add($distribusi, 'longitude_akhir_dir', $longitude_akhir_dir);
//        array_add($distribusi, 'latitude_awal_deg', $latitude_awal_deg);
//        array_add($distribusi, 'latitude_awal_min', $latitude_awal_min);
//        array_add($distribusi, 'latitude_awal_sec', $latitude_awal_sec);
//        array_add($distribusi, 'latitude_awal_dir', $latitude_awal_dir);
//        array_add($distribusi, 'latitude_akhir_deg', $latitude_akhir_deg);
//        array_add($distribusi, 'latitude_akhir_min', $latitude_akhir_min);
//        array_add($distribusi, 'latitude_akhir_sec', $latitude_akhir_sec);
//        array_add($distribusi, 'latitude_akhir_dir', $latitude_akhir_dir);

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();


        return view('eksternal/create_instalasi_distribusi', compact('tegangan_pengenal', 'distribusi', 'pemilik', 'jenis_instalasi',
            'province', 'city', 'jenis_ijin_usaha', 'pemilik_instalasi', 'jenis_program', 'sistem_jaringan_distribusi'));
    }

    public function storeDistribusi(Request $request)
    {
//        dd($request);
        $timestamp = date('YmdHis');
        $id = $request->id;
        $distribusi = new InstalasiDistribusi();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;
        if ($id != 0) {
            //update
            $distribusi = InstalasiDistribusi::findOrFail($id);
        }

        //        Data Umum
        $distribusi->status_baru = $request->status_baru;
        $distribusi->status_slo = $request->status_slo;
        $distribusi->id_jenis_instalasi = $request->jenis_instalasi;
        $distribusi->nama_instalasi = $request->nama_instalasi;
        $distribusi->panjang_kms = $request->panjang_saluran;
        $distribusi->jumlah_tiang = $request->jumlah_tiang;
        $distribusi->jml_gardu = $request->jumlah_gardu_distribusi;
        $distribusi->jumlah_panel = $request->jumlah_panel;
        $distribusi->tegangan_pengenal = $request->tegangan_pengenal;
        $distribusi->sistem_jaringan = $request->sistem_jaringan_distribusi;

        //        Kapasitas
        $distribusi->kapasitas_gardu = $request->kapasitas_gardu_distribusi;
        $distribusi->kapasitas_arus_hubung_singkat = $request->kapasitas_arus_hubung_singkat;

        // additional info
        $distribusi->id_program = $request->id_program;
        $distribusi->area_apd = $request->area_apd;
        $distribusi->gi_gh = $request->gi_gh;
        $distribusi->kontrak = $request->kontrak;
        $distribusi->lokasi = $request->lokasi;
        $distribusi->penyulang = $request->penyulang;
        $distribusi->keterangan = $request->keterangan;


        $nm_u_custom = '-' . $timestamp . '-' . $distribusi->id . '.';
        $distribusi->status_baru = $request->status_baru;

        if ($request->status_baru == '0') {
            if ($request->hasFile('file_pernyataan')) {
                $distribusi->file_pernyataan = ($request->hasFile('file_pernyataan')) ? 'file_pernyataan-distribusi' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension() : "";
                $filename = $distribusi->file_pernyataan;
                $fullPath = storage_path() . '/upload/file_pernyataan/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataan->move(storage_path() . '/upload/file_pernyataan', 'file_pernyataan-distribusi' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension());
            }
        } else {
            $distribusi->file_pernyataan = '';
        }


        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $distribusi->pemilik_instalasi_id = $pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $distribusi->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = trim($request->alamat_pemilik);
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();

            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }
            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $distribusi->pemilik_instalasi_id = $pemilik->id;
        }

        //      Lokasi Instalasi
        $distribusi->alamat_instalasi = $request->alamat_instalasi;
        $distribusi->id_provinsi = $request->provinsi;
        $distribusi->id_kota = $request->kabupaten;
//        $distribusi->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
//        $distribusi->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;
//        $distribusi->longitude_akhir = $request->longitude_akhir_deg . ' ° ' . $request->longitude_akhir_min . ' \' ' . $request->longitude_akhir_sec . ' " ' . $request->longitude_akhir_dir;
//        $distribusi->latitude_akhir = $request->latitude_akhir_deg . ' ° ' . $request->latitude_akhir_min . ' \' ' . $request->latitude_akhir_sec . ' " ' . $request->latitude_akhir_dir;
        $distribusi->longitude_awal = $request->longitude_awal;
        $distribusi->latitude_awal = $request->latitude_awal;
        $distribusi->longitude_akhir = $request->longitude_akhir;
        $distribusi->latitude_akhir = $request->latitude_akhir;
        $distribusi->alamat_akhir_instalasi = $request->alamat_akhir_instalasi;
        $distribusi->id_provinsi_akhir = $request->provinsi_akhir;
        $distribusi->id_kota_akhir = $request->kabupaten_akhir;


        $distribusi->created_by = Auth::user()->username;
        $distribusi->updated_by = Auth::user()->username;
        $distribusi->id_perusahaan = Auth::user()->perusahaan_id;
        $distribusi->tipe_pemilik = $tipe_pemilik;


        $distribusi->kode_kontraktor = $request->kode_kontraktor;
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $distribusi->kontraktor = $kontraktor->nama;
        } else {
            $distribusi->kontraktor = "";
        }
        #cek jika status instalasi lama, set kontraktor menjadi 0000
        if ($request->status_baru == 0) {
            $distribusi->kode_kontraktor = "0000";
            $distribusi->kontraktor = "PT. Tidak Ada Pembangunan";
        }

        #save file distribusi
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $request->pemilik_instalasi_id);
            $distribusi->file_sbujk = $filename;
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $request->pemilik_instalasi_id);
            $distribusi->file_iujk = $filename;
        }
        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $request->pemilik_instalasi_id);
            $distribusi->file_sld = $filename;
        }
        #end save file distribusi

        $distribusi->save();

        //        Foto Instalasi
        $nm_u_custom = '-' . $timestamp . '-' . $distribusi->id . '.';

        if ($request->hasFile('file_foto_1')) {
            $distribusi->foto_1 = ($request->hasFile('file_foto_1')) ? 'foto_dis_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension() : "";
            $filename = $distribusi->foto_1;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_1->move(storage_path() . '/upload/foto_instalasi', 'foto_dis_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension());
        }
        if ($request->hasFile('file_foto_2')) {
            $distribusi->foto_2 = ($request->hasFile('file_foto_2')) ? 'foto_dis_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension() : "";
            $filename = $distribusi->foto_2;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_2->move(storage_path() . '/upload/foto_instalasi', 'foto_dis_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension());
        }

        $distribusi->save();

        //        Session::flash('message', 'Data Instalasi Distribusi Berhasil Disimpan');
        return redirect('eksternal/view_instalasi_distribusi')->with('success', 'Data Instalasi Distribusi Berhasil Disimpan');
    }

    public function deleteDistribusi($id)
    {
        $distribusi = InstalasiDistribusi::findOrFail($id);
        //cek dulu apakah dipakai di permohonan atau tidak, jika daipakai maka tidak dapat dihapus
        if (sizeof($distribusi->getPermohonan()) > 0) {
            Session::flash('message', 'Data Instalasi Digunakan dalam data order. Tidak dapat dihapus');
        } else {
            $distribusi->delete();
            Session::flash('message', 'Data Instalasi Distribusi Berhasil Dihapus');
        }
        return redirect('eksternal/view_instalasi_distribusi')->with('success', 'Data Instalasi Distribusi Berhasil Dihapus');
    }

    /* END MASTER INSTALASI DISTRIBUSI */

    /* MASTER INSTALASI PEMANFAATAN TEGANGAN TINGGI */

    public function showPemanfaatanTT()
    {
        $pemanfaatan_tt = InstalasiPemanfaatanTT::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        return view('eksternal/view_instalasi_pemanfaatan_tt', compact('pemanfaatan_tt'));
    }

    public function detailPemanfaatanTT($id)
    {
        $instalasi = InstalasiPemanfaatanTT::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_PEMANFAATAN_TT);
        $lingkup_pekerjaan = $tipe_instalasi->lingkup_pekerjaan()->get();
        $layanan = $tipe_instalasi->produk()->get();
        $permohonan = $instalasi->getRiwayat();
        $selected_layanan = null;

        //kelompokan berdasarkan jenis instalasi unuk memudahkan pencarian nanti
        $used = Permohonan::getaGroupedOrdered(ID_PEMANFAATAN_TT, $id);
        return view('eksternal/detail_instalasi_pemanfaatan_tt', compact('instalasi', 'layanan', 'selected_layanan', 'lingkup_pekerjaan', 'tipe_instalasi', 'permohonan', 'used'));
    }

    public function insertPemanfaatanTT($id = null)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 4)->get();  //sementara manual
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $pemanfaatan_tt = ($id != null) ? InstalasiPemanfaatanTT::findOrFail($id) : null;
//        // longitude awal
//        $longitude_awal = '';
//        $longitude_awal = explode(' ° ', ($id != null) ? $pemanfaatan_tt->longitude_awal : '');
//        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
//        // longitude akhir
//        $longitude_akhir = '';
//        $longitude_akhir = explode(' ° ', ($id != null) ? $pemanfaatan_tt->longitude_akhir : '');
//        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
//        // latitude awal
//        $latitude_awal = '';
//        $latitude_awal = explode(' ° ', ($id != null) ? $pemanfaatan_tt->latitude_awal : '');
//        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
//        // latitude akhir
//        $latitude_akhir = '';
//        $latitude_akhir = explode(' ° ', ($id != null) ? $pemanfaatan_tt->latitude_akhir : '');
//        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';
//
//        array_add($pemanfaatan_tt, 'longitude_awal_deg', $longitude_awal_deg);
//        array_add($pemanfaatan_tt, 'longitude_awal_min', $longitude_awal_min);
//        array_add($pemanfaatan_tt, 'longitude_awal_sec', $longitude_awal_sec);
//        array_add($pemanfaatan_tt, 'longitude_awal_dir', $longitude_awal_dir);
//        array_add($pemanfaatan_tt, 'longitude_akhir_deg', $longitude_akhir_deg);
//        array_add($pemanfaatan_tt, 'longitude_akhir_min', $longitude_akhir_min);
//        array_add($pemanfaatan_tt, 'longitude_akhir_sec', $longitude_akhir_sec);
//        array_add($pemanfaatan_tt, 'longitude_akhir_dir', $longitude_akhir_dir);
//        array_add($pemanfaatan_tt, 'latitude_awal_deg', $latitude_awal_deg);
//        array_add($pemanfaatan_tt, 'latitude_awal_min', $latitude_awal_min);
//        array_add($pemanfaatan_tt, 'latitude_awal_sec', $latitude_awal_sec);
//        array_add($pemanfaatan_tt, 'latitude_awal_dir', $latitude_awal_dir);
//        array_add($pemanfaatan_tt, 'latitude_akhir_deg', $latitude_akhir_deg);
//        array_add($pemanfaatan_tt, 'latitude_akhir_min', $latitude_akhir_min);
//        array_add($pemanfaatan_tt, 'latitude_akhir_sec', $latitude_akhir_sec);
//        array_add($pemanfaatan_tt, 'latitude_akhir_dir', $latitude_akhir_dir);

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();

        $list_kontraktor = Kontraktor::all();

        return view('eksternal/create_instalasi_pemanfaatan_tt2', compact('tegangan_pengenal', 'pemanfaatan_tt', 'pemilik', 'jenis_instalasi', 'province', 'city', 'jenis_ijin_usaha', 'pemilik_instalasi', 'list_kontraktor'));
    }

    public function storePemanfaatanTT(Request $request)
    {
        $timestamp = date('YmdHis');
        $id = $request->id;
        $pemanfaatan_tt = new InstalasiPemanfaatanTT();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;
        if ($id != 0) {
            //update
            $pemanfaatan_tt = InstalasiPemanfaatanTT::findOrFail($id);
        }

        //        Data Umum
        $pemanfaatan_tt->status_baru = $request->status_baru;
        $pemanfaatan_tt->status_slo = $request->status_slo;
        $pemanfaatan_tt->id_jenis_instalasi = $request->jenis_instalasi;
        $pemanfaatan_tt->nama_instalasi = $request->nama_instalasi;
        $pemanfaatan_tt->phb_tm = $request->phb_tm;
        $pemanfaatan_tt->phb_tr = $request->phb_tr;
        $pemanfaatan_tt->penyedia_tl = $request->penyedia_tenaga_listrik;
        $pemanfaatan_tt->tegangan_pengenal = $request->tegangan_pengenal;
        //        $pemanfaatan_tt->kontraktor = $request->kontraktor;
        $pemanfaatan_tt->kode_kontraktor = $request->kode_kontraktor;
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $pemanfaatan_tt->kontraktor = $kontraktor->nama;
        } else {
            $pemanfaatan_tt->kontraktor = "";
        }

        #cek jika status instalasi lama, set kontraktor menjadi 0000
        if ($request->status_baru == 0) {
            $pemanfaatan_tt->kode_kontraktor = "0000";
            $pemanfaatan_tt->kontraktor = "PT. Tidak Ada Pembangunan";
        }


        //        Kapasitas
        $pemanfaatan_tt->kapasitas_trafo = $request->kapasitas_trafo_terpasang;
        $pemanfaatan_tt->daya_sambung = $request->daya_tersambung;


        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $pemanfaatan_tt->pemilik_instalasi_id = $request->pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $pemanfaatan_tt->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            //            $timestamp                      = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = trim($request->alamat_pemilik);
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();

            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }

            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $pemanfaatan_tt->pemilik_instalasi_id = $pemilik->id;
        }

        //      Lokasi Instalasi
        $pemanfaatan_tt->alamat_instalasi = $request->alamat_instalasi;
        $pemanfaatan_tt->id_provinsi = $request->provinsi;
        $pemanfaatan_tt->id_kota = $request->kabupaten;

//        $pemanfaatan_tt->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
//        $pemanfaatan_tt->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;
//        $pemanfaatan_tt->longitude_akhir = $request->longitude_akhir_deg . ' ° ' . $request->longitude_akhir_min . ' \' ' . $request->longitude_akhir_sec . ' " ' . $request->longitude_akhir_dir;
//        $pemanfaatan_tt->latitude_akhir = $request->latitude_akhir_deg . ' ° ' . $request->latitude_akhir_min . ' \' ' . $request->latitude_akhir_sec . ' " ' . $request->latitude_akhir_dir;

        $pemanfaatan_tt->longitude_awal = $request->longitude_awal;
        $pemanfaatan_tt->latitude_awal = $request->latitude_awal;
        $pemanfaatan_tt->longitude_akhir = $request->longitude_akhir;
        $pemanfaatan_tt->latitude_akhir = $request->latitude_akhir;


        $pemanfaatan_tt->alamat_akhir_instalasi = $request->alamat_akhir_instalasi;
        $pemanfaatan_tt->id_provinsi_akhir = $request->provinsi_akhir;
        $pemanfaatan_tt->id_kota_akhir = $request->kabupaten_akhir;


        $pemanfaatan_tt->created_by = Auth::user()->username;
        $pemanfaatan_tt->tipe_pemilik = $tipe_pemilik;

        #save file pemanfaatan tt
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $request->pemilik_instalasi_id);
            $pemanfaatan_tt->file_sbujk = $filename;
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $request->pemilik_instalasi_id);
            $pemanfaatan_tt->file_iujk = $filename;
        }
        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $request->pemilik_instalasi_id);
            $pemanfaatan_tt->file_sld = $filename;
        }
        #end save file pemanfaatan tt

        $pemanfaatan_tt->save();

        //        Foto Instalasi
        $nm_u_custom = '-' . $timestamp . '-' . $pemanfaatan_tt->id . '.';

        if ($request->status_baru == '0') {
            if ($request->hasFile('file_pernyataan')) {
                $pemanfaatan_tt->file_pernyataan = ($request->hasFile('file_pernyataan')) ? 'file_pernyataan-pemanfaatantt' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension() : "";
                $filename = $pemanfaatan_tt->file_pernyataan;
                $fullPath = storage_path() . '/upload/file_pernyataan/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataan->move(storage_path() . '/upload/file_pernyataan', 'file_pernyataan-pemanfaatantt' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension());
            }
        } else {
            $pemanfaatan_tt->file_pernyataan = '';
        }

        if ($request->hasFile('file_foto_1')) {
            $pemanfaatan_tt->foto_1 = ($request->hasFile('file_foto_1')) ? 'foto_tt_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension() : "";
            $filename = $pemanfaatan_tt->foto_1;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_1->move(storage_path() . '/upload/foto_instalasi', 'foto_tt_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension());
        }
        if ($request->hasFile('file_foto_2')) {
            $pemanfaatan_tt->foto_2 = ($request->hasFile('file_foto_2')) ? 'foto_tt_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension() : "";
            $filename = $pemanfaatan_tt->foto_2;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_2->move(storage_path() . '/upload/foto_instalasi', 'foto_tt_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension());
        }


        $pemanfaatan_tt->save();


        //        $pemanfaatan_tt->pemilik_instalasi_id = $request->pemilik_instalasi;
        //        $pemanfaatan_tt->alamat_instalasi = $request->alamat_instalasi;
        //        $pemanfaatan_tt->id_provinsi = $request->provinsi;
        //        $pemanfaatan_tt->id_kota = $request->kabupaten;
        //        $pemanfaatan_tt->longitude_awal = $request->longitude_awal;
        //        $pemanfaatan_tt->latitude_awal = $request->latitude_awal;
        //        $pemanfaatan_tt->longitude_akhir = $request->longitude_akhir;
        //        $pemanfaatan_tt->latitude_akhir = $request->latitude_akhir;


        $pemanfaatan_tt->save();
        //        Session::flash('message', 'Data Instalasi Pemanfaatan Tegangan Tinggi Berhasil Disimpan');
        return redirect('eksternal/view_instalasi_pemanfaatan_tt')->with('success', 'Data Instalasi Pemanfaatan Tegangan Tinggi Berhasil Disimpan');
    }

    public function deletePemanfaatanTT($id)
    {
        $pemanfaatan_tt = InstalasiPemanfaatanTT::findOrFail($id);
        //cek dulu apakah dipakai di permohonan atau tidak, jika daipakai maka tidak dapat dihapus
        if (sizeof($pemanfaatan_tt->getPermohonan()) > 0) {
            Session::flash('message', 'Data Instalasi Digunakan dalam data order. Tidak dapat dihapus');
        } else {
            $pemanfaatan_tt->delete();
            Session::flash('message', 'Data Instalasi Pemanfaatan Tegangan Tinggi Berhasil Dihapus');
        }
        return redirect('eksternal/view_instalasi_pemanfaatan_tt');
    }

    /* END MASTER INSTALASI PEMANFAATAN TEGANGAN TINGGI */

    /* MASTER INSTALASI PEMANFAATAN TEGANGAN MENENGAH */

    public function showPemanfaatanTM()
    {
        $pemanfaatan_tm = InstalasiPemanfaatanTM::where('created_by', Auth::user()->username)->orderBy('id', 'desc')->get();
        return view('eksternal/view_instalasi_pemanfaatan_tm', compact('pemanfaatan_tm'));
    }

    public function detailPemanfaatanTM($id)
    {
        $instalasi = InstalasiPemanfaatanTM::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_PEMANFAATAN_TM);
        $lingkup_pekerjaan = $tipe_instalasi->lingkup_pekerjaan()->get();
        $layanan = $tipe_instalasi->produk()->get();
        $permohonan = $instalasi->getRiwayat();
        $selected_layanan = null;

        //kelompokan berdasarkan jenis instalasi unuk memudahkan pencarian nanti
        $used = Permohonan::getaGroupedOrdered(ID_PEMANFAATAN_TM, $id);
        return view('eksternal/detail_instalasi_pemanfaatan_tm', compact('instalasi', 'layanan', 'selected_layanan', 'lingkup_pekerjaan', 'tipe_instalasi', 'permohonan', 'used'));
    }

    public function insertPemanfaatanTM($id = null)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 5)->get();  //sementara manual
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $pemanfaatan_tm = ($id != null) ? InstalasiPemanfaatanTM::findOrFail($id) : null;

//        // longitude awal
//        $longitude_awal = '';
//        $longitude_awal = explode(' ° ', ($id != null) ? $pemanfaatan_tm->longitude_awal : '');
//        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
//        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
//        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
//        // longitude akhir
//        $longitude_akhir = '';
//        $longitude_akhir = explode(' ° ', ($id != null) ? $pemanfaatan_tm->longitude_akhir : '');
//        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
//        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
//        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
//        // latitude awal
//        $latitude_awal = '';
//        $latitude_awal = explode(' ° ', ($id != null) ? $pemanfaatan_tm->latitude_awal : '');
//        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
//        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
//        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
//        // latitude akhir
//        $latitude_akhir = '';
//        $latitude_akhir = explode(' ° ', ($id != null) ? $pemanfaatan_tm->latitude_akhir : '');
//        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
//        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
//        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';
//
//        array_add($pemanfaatan_tm, 'longitude_awal_deg', $longitude_awal_deg);
//        array_add($pemanfaatan_tm, 'longitude_awal_min', $longitude_awal_min);
//        array_add($pemanfaatan_tm, 'longitude_awal_sec', $longitude_awal_sec);
//        array_add($pemanfaatan_tm, 'longitude_awal_dir', $longitude_awal_dir);
//        array_add($pemanfaatan_tm, 'longitude_akhir_deg', $longitude_akhir_deg);
//        array_add($pemanfaatan_tm, 'longitude_akhir_min', $longitude_akhir_min);
//        array_add($pemanfaatan_tm, 'longitude_akhir_sec', $longitude_akhir_sec);
//        array_add($pemanfaatan_tm, 'longitude_akhir_dir', $longitude_akhir_dir);
//        array_add($pemanfaatan_tm, 'latitude_awal_deg', $latitude_awal_deg);
//        array_add($pemanfaatan_tm, 'latitude_awal_min', $latitude_awal_min);
//        array_add($pemanfaatan_tm, 'latitude_awal_sec', $latitude_awal_sec);
//        array_add($pemanfaatan_tm, 'latitude_awal_dir', $latitude_awal_dir);
//        array_add($pemanfaatan_tm, 'latitude_akhir_deg', $latitude_akhir_deg);
//        array_add($pemanfaatan_tm, 'latitude_akhir_min', $latitude_akhir_min);
//        array_add($pemanfaatan_tm, 'latitude_akhir_sec', $latitude_akhir_sec);
//        array_add($pemanfaatan_tm, 'latitude_akhir_dir', $latitude_akhir_dir);

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
//        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $pemilik = PemilikInstalasi::all();

        $list_kontraktor = Kontraktor::all();

        return view('eksternal/create_instalasi_pemanfaatan_tm2', compact('tegangan_pengenal', 'pemanfaatan_tm', 'pemilik', 'jenis_instalasi', 'province', 'city', 'pemilik_instalasi', 'jenis_ijin_usaha', 'list_kontraktor'));
    }

    public function storePemanfaatanTM(Request $request)
    {
        $timestamp = date('YmdHis');
        $id = $request->id;
        $pemanfaatan_tm = new InstalasiPemanfaatanTM();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;

        if ($id != 0) {
            //update
            $pemanfaatan_tm = InstalasiPemanfaatanTM::findOrFail($id);
        }

        //        Data Umum
        $pemanfaatan_tm->status_baru = $request->status_baru;
        $pemanfaatan_tm->status_slo = $request->status_slo;
        $pemanfaatan_tm->id_jenis_instalasi = $request->jenis_instalasi;
        $pemanfaatan_tm->nama_instalasi = $request->nama_instalasi;
        $pemanfaatan_tm->phb_tm = $request->phb_tm;
        $pemanfaatan_tm->phb_tr = $request->phb_tr;
        $pemanfaatan_tm->penyedia_tl = $request->penyedia_tenaga_listrik;
        $pemanfaatan_tm->tegangan_pengenal = $request->tegangan_pengenal;
        //        $pemanfaatan_tm->kontraktor = $request->kontraktor;
        $pemanfaatan_tm->kode_kontraktor = $request->kode_kontraktor;
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $pemanfaatan_tm->kontraktor = $kontraktor->nama;
        } else {
            $pemanfaatan_tm->kontraktor = "";
        }

        #cek jika status instalasi lama, set kontraktor menjadi 0000
        if ($request->status_baru == 0) {
            $pemanfaatan_tm->kode_kontraktor = "0000";
            $pemanfaatan_tm->kontraktor = "PT. Tidak Ada Pembangunan";
        }

        //        Kapasitas
        $pemanfaatan_tm->kapasitas_trafo = $request->kapasitas_trafo_terpasang;
        $pemanfaatan_tm->daya_sambung = $request->daya_tersambung;

        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $pemanfaatan_tm->pemilik_instalasi_id = $request->pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $pemanfaatan_tm->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = $request->alamat_pemilik;
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();

            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }

            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $pemanfaatan_tm->pemilik_instalasi_id = $pemilik->id;
        }

        //      Lokasi Instalasi
        $pemanfaatan_tm->alamat_instalasi = $request->alamat_instalasi;
        $pemanfaatan_tm->id_provinsi = $request->provinsi;
        $pemanfaatan_tm->id_kota = $request->kabupaten;

//        $pemanfaatan_tm->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
//        $pemanfaatan_tm->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;
//        $pemanfaatan_tm->longitude_akhir = $request->longitude_akhir_deg . ' ° ' . $request->longitude_akhir_min . ' \' ' . $request->longitude_akhir_sec . ' " ' . $request->longitude_akhir_dir;
//        $pemanfaatan_tm->latitude_akhir = $request->latitude_akhir_deg . ' ° ' . $request->latitude_akhir_min . ' \' ' . $request->latitude_akhir_sec . ' " ' . $request->latitude_akhir_dir;

        $pemanfaatan_tm->longitude_awal = $request->longitude_awal;
        $pemanfaatan_tm->latitude_awal = $request->latitude_awal;
        $pemanfaatan_tm->longitude_akhir = $request->longitude_akhir;
        $pemanfaatan_tm->latitude_akhir = $request->latitude_akhir;

        $pemanfaatan_tm->alamat_akhir_instalasi = $request->alamat_akhir_instalasi;
        $pemanfaatan_tm->id_provinsi_akhir = $request->provinsi_akhir;
        $pemanfaatan_tm->id_kota_akhir = $request->kabupaten_akhir;

        $pemanfaatan_tm->created_by = Auth::user()->username;
        $pemanfaatan_tm->tipe_pemilik = $tipe_pemilik;

        #save file pemanfaatan tm
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $request->pemilik_instalasi_id);
            $pemanfaatan_tm->file_sbujk = $filename;
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $request->pemilik_instalasi_id);
            $pemanfaatan_tm->file_iujk = $filename;
        }
        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $request->pemilik_instalasi_id);
            $pemanfaatan_tm->file_sld = $filename;
        }
        #end save file pemanfaatan tm

        $pemanfaatan_tm->save();

        //        Foto Instalasi
        $nm_u_custom = '-' . $timestamp . '-' . $pemanfaatan_tm->id . '.';

        if ($request->status_baru == '0') {
            if ($request->hasFile('file_pernyataan')) {
                $pemanfaatan_tm->file_pernyataan = ($request->hasFile('file_pernyataan')) ? 'file_pernyataan-pemanfaatantm' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension() : "";
                $filename = $pemanfaatan_tm->file_pernyataan;
                $fullPath = storage_path() . '/upload/file_pernyataan/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataan->move(storage_path() . '/upload/file_pernyataan', 'file_pernyataan-pemanfaatantm' . $nm_u_custom . $request->file_pernyataan->getClientOriginalExtension());
            }
        } else {
            $pemanfaatan_tm->file_pernyataan = '';
        }

        if ($request->hasFile('file_foto_1')) {
            $pemanfaatan_tm->foto_1 = ($request->hasFile('file_foto_1')) ? 'foto_tm_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension() : "";
            $filename = $pemanfaatan_tm->foto_1;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_1->move(storage_path() . '/upload/foto_instalasi', 'foto_tm_1' . $nm_u_custom . $request->file_foto_1->getClientOriginalExtension());
        }
        if ($request->hasFile('file_foto_2')) {
            $pemanfaatan_tm->foto_2 = ($request->hasFile('file_foto_2')) ? 'foto_tm_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension() : "";
            $filename = $pemanfaatan_tm->foto_2;
            $fullPath = storage_path() . '/upload/foto_instalasi/' . $filename;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $request->file_foto_2->move(storage_path() . '/upload/foto_instalasi', 'foto_tm_2' . $nm_u_custom . $request->file_foto_2->getClientOriginalExtension());
        }


        $pemanfaatan_tm->save();

        //        $pemanfaatan_tm->pemilik_instalasi_id = $request->pemilik_instalasi;
        //        $pemanfaatan_tm->alamat_instalasi = $request->alamat_instalasi;
        //        $pemanfaatan_tm->id_provinsi = $request->provinsi;
        //        $pemanfaatan_tm->id_kota = $request->kabupaten;
        //        $pemanfaatan_tm->longitude_awal = $request->longitude_awal;
        //        $pemanfaatan_tm->latitude_awal = $request->latitude_awal;
        //        $pemanfaatan_tm->longitude_akhir = $request->longitude_akhir;
        //        $pemanfaatan_tm->latitude_akhir = $request->latitude_akhir;


        $pemanfaatan_tm->save();
        //        Session::flash('message', 'Data Instalasi Pemanfaatan Tegangan Menengah Berhasil Disimpan');
        return redirect('eksternal/view_instalasi_pemanfaatan_tm')->with('success', 'Data Instalasi Pemanfaatan Tegangan Menengah Berhasil Disimpan');
    }

    public function deletePemanfaatanTM($id)
    {
        $pemanfaatan_tm = InstalasiPemanfaatanTM::findOrFail($id);
        //cek dulu apakah dipakai di permohonan atau tidak, jika daipakai maka tidak dapat dihapus
        if (sizeof($pemanfaatan_tm->getPermohonan()) > 0) {
            Session::flash('message', 'Data Instalasi Digunakan dalam data order. Tidak dapat dihapus');
        } else {
            $pemanfaatan_tm->delete();
            Session::flash('message', 'Data Instalasi Pemanfaatan Tegangan Menengah Berhasil Dihapus');
        }
        return redirect('eksternal/view_instalasi_pemanfaatan_tm');
    }

    /* END MASTER INSTALASI PEMANFAATAN TEGANGAN MENENGAH */


    /* ====MASTER JENIS PROGRAM START========================= */

    public function showJenisProgram()
    {
        $jp = JenisProgram::all();
        return view('master/jenis_program', compact('jp'));
    }

    public function insertJenisProgram($id = "")
    {
        $id = ($id == "") ? 0 : $id;
        $jp = null;

        if ($id > 0) {
            $jp = JenisProgram::findOrFail($id);
        }
        return view('master/input_jenis_program', compact('id', 'jp'));
    }

    public function storeJenisProgram(Request $request)
    {
        $id = $request->id;
        if ($id == 0) {
            //insert
            $jp = new JenisProgram();
            $jp->jenis_program = $request->jp;
        } else {
            //update
            $jp = JenisProgram::findOrFail($id);
            $jp->jenis_program = $request->jp;
        }
        $jp->save();
        return redirect('master/jenis_program');
    }

    public function deleteJenisProgram($id)
    {
        $jp = JenisProgram::findOrFail($id);
        $jp->delete();
        return redirect('master/jenis_program');
    }

    /* ====MASTER JENIS PROGRAM END=========================== */


    /* MASTER PERALATAN */

    public function showPeralatan()
    {
        if(User::isCurrUserAllowPermission(PERMISSION_VIEW_MASTER_PERALATAN)){
            $peralatan = Peralatan::all();
            $grouped = $peralatan->groupBy('sub_bidang_id');
            $sub_ids = $grouped->keys();
            $sub_bid = SubBidang::whereIn('id',$sub_ids)->get();
            return view('master/peralatan', compact('peralatan','sub_bid'));
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function insertPeralatan($id = null)
    {
        if(User::isCurrUserAllowPermission(PERMISSION_CREATE_MASTER_PERALATAN) || User::isCurrUserAllowPermission(PERMISSION_EDIT_MASTER_PERALATAN)) {
            $bidang = Bidang::all();
            $sub_bidang = SubBidang::all();
            $ref_parent = References::where('nama_reference', 'Kondisi Alat')->first();
            $kondisi_alat = References::where('parent', $ref_parent->id)->get();
            $ref_parent = References::where('nama_reference', 'Lokasi Alat')->first();
            $lokasi_alat = References::where('parent', $ref_parent->id)->get();
            $ref_parent = References::where('nama_reference', 'Kategori Alat')->first();
            $kategori_alat = References::where('parent', $ref_parent->id)->get();
            $peralatan = ($id != null) ? Peralatan::findOrFail($id) : null;
            return view('master/input_peralatan', compact('bidang', 'sub_bidang', 'peralatan', 'kondisi_alat', 'lokasi_alat', 'kategori_alat'));
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }


    public function detailPeralatan($id)
    {
        $peralatan = ($id != null) ? Peralatan::findOrFail($id) : null;
        $history_peralatan = HistoryPeralatan::where('peralatan_id', $id)->orderBy('created_at', 'desc')->get();
        return view('master/detail_peralatan', compact('peralatan', 'history_peralatan'));
    }

    public function storePeralatan(Request $request)
    {
        $id = $request->id;
        $peralatan = new Peralatan();
        if ($id != 0) {
            //update
            $peralatan = Peralatan::findOrFail($id);
        }

        $peralatan->nama_alat = $request->nama_alat;
        $peralatan->merk_alat = $request->merk_alat;
        $peralatan->tipe_alat = $request->tipe_alat;
        $peralatan->nomor_seri = $request->nomor_seri;
        $peralatan->bidang_id = $request->bidang;
        $peralatan->sub_bidang_id = $request->sub_bidang;
        $peralatan->kondisi_alat = $request->kondisi_alat;
        $peralatan->lokasi_alat = $request->lokasi_alat;
        $peralatan->kategori_alat = $request->kategori_alat;

        $peralatan->save();

        $file = $request->foto_peralatan;
        if ($file != null) {
            if ($peralatan->foto_peralatan != null) {
                $loc = storage_path() . '/upload/foto_peralatan/' . $peralatan->foto_peralatan;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'foto_peralatan-' . $timestamp . '-' . $peralatan->id . "-" . $peralatan->nama . "." . $file->getClientOriginalExtension();
            $peralatan->foto_peralatan = $name;
            $file->move(storage_path() . '/upload/foto_peralatan/', $name);
            $peralatan->save();
        }

        Session::flash('message', 'Data Peralatan Berhasil Disimpan');
        return redirect('master/peralatan');
    }

    public function deletePeralatan($id)
    {
        if(User::isCurrUserAllowPermission(PERMISSION_EDIT_MASTER_PERALATAN)) {
            $peralatan = Peralatan::findOrFail($id);
            $peralatan->delete();
            Session::flash('message', 'Data Peralatan Berhasil Dihapus');
            return redirect('master/peralatan');
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    /* END MASTER PERALATAN */

    /* ===================== MASTER PERSONIL INSPEKSI=============== */

    public function showPersonilInspeksi()
    {
        if (User::isCurrUserAllowPermission(PERMISSION_VIEW_PERSONIL)) {
            $personil = PersonilInspeksi::orderBy('created_at', 'asc')->get();
            return view('master/personil_inspeksi', compact('personil'));
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function createPersonilInspeksi($id = 0)
    {
        if (User::isCurrUserAllowPermission(PERMISSION_CREATE_PERSONIL) || User::isCurrUserAllowPermission(PERMISSION_EDIT_PERSONIL)) {
            $personil = PersonilInspeksi::find($id);
            $jabatan = JabatanInspeksi::all();
            $status = StatusPekerja::all();
            $bidang = Bidang::where('isaktif', 1)->get();
            $sub_bidang = SubBidang::where('isaktif', 1)->get();
            $user = User::where('perusahaan_id', null)->get();
            $tipe_instalasi = TipeInstalasi::all();
            $jabatan_personil = ($id == 0) ? array() : $personil->jabatan_personil->pluck('jabatan_inspeksi_id')->all();
            $personil_bidang = ($id == 0) ? array() : $personil->personil_bidang->pluck('sub_bidang_id')->all();
            $lingkup_personil = ($id == 0) ? array() : $personil->lingkup_personil->pluck('lingkup_pekerjaan_id')->all();
            return view('master/input_personil_inspeksi', compact('id', 'jabatan_personil', 'personil_bidang', 'lingkup_personil', 'personil', 'jabatan', 'status', 'bidang', 'sub_bidang', 'user', 'tipe_instalasi'));
        } else {
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function storePersonilInspeksi(Request $request)
    {
        //1. Save data personil
        // dd($request);
        $id = $request->id;
        $id_user = $request->user_id;
        $personil = ($id == 0) ? new PersonilInspeksi() : PersonilInspeksi::findOrFail($id);
        $personil->id_status_pekerja = $request->id_status_pekerja;
        $personil->id_bidang = $request->id_bidang;
        if ($id_user != 0) {
            $personil->id_user = $id_user;
        } else {
            $personil->nip = $request->nip;
            $personil->email = $request->email;
            $personil->nama = $request->nama;
            $personil->tempat_lahir = $request->tempat_lahir;
            $personil->tanggal_lahir = $request->tanggal_lahir;
            $personil->jenis_kelamin = $request->jenis_kelamin;
            $personil->status_menikah = $request->status_menikah;
            $personil->no_hp = $request->no_hp;
            $personil->alamat = $request->alamat;
            $personil->grade = $request->grade;
            $personil->nama_perusahaan = $request->nama_perusahaan;
        }
        // dd($personil);
        $personil->save();
        if (isset($request->foto_personil)) {
            $file = $request->foto_personil;
            if ($personil->foto_personil != null) {
                $loc = storage_path() . '/upload/foto_personil/' . $personil->foto_personil;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'foto_personil-' . $timestamp . '-' . $personil->id . "-" . $personil->nama . "." . $file->getClientOriginalExtension();
            $personil->foto_personil = $name;
            $file->move(storage_path() . '/upload/foto_personil/', $name);
            $personil->save();
        }
        //2. insert to sub bidang_personil
        PersonilBidang::where('personil_inspeksi_id', $personil->id)->delete();
        if ($request->input('sub_bidang_' . $personil->id_bidang) != null) {
            foreach ($request->input('sub_bidang_' . $personil->id_bidang) as $item) {
                $personilBidang = new PersonilBidang();
                $personilBidang->personil_inspeksi_id = $personil->id;
                $personilBidang->bidang_id = $personil->id_bidang;
                $personilBidang->sub_bidang_id = $item;
                $personilBidang->save();
            }
        }

        //3.insert to jabatan_personil
        JabatanPersonil::where('personil_inspeksi_id', $personil->id)->delete();
        foreach ($request->id_jabatan_inspeksi as $item) {
            $jabatanPersonil = new JabatanPersonil();
            $jabatanPersonil->personil_inspeksi_id = $personil->id;
            $jabatanPersonil->jabatan_inspeksi_id = $item;
            $jabatanPersonil->save();
        }

        //4. insert to lingkup personil
        LingkupPersonil::where('personil_inspeksi_id', $personil->id)->delete();
        $instalasi = TipeInstalasi::all();
        foreach ($instalasi as $inst) {
            if ($request->input('lingkup_pekerjaan_' . $inst->id) != null) {
                foreach ($request->input('lingkup_pekerjaan_' . $inst->id) as $item) {
                    $lingkupPersonil = new LingkupPersonil();
                    $lingkupPersonil->personil_inspeksi_id = $personil->id;
                    $lingkupPersonil->lingkup_pekerjaan_id = $item;
                    $lingkupPersonil->tipe_instalasi_id = $inst->id;
                    $lingkupPersonil->save();
                }
            }
        }

        if ($id == 0) {
            return redirect('master/personil_inspeksi/input/' . $personil->id)->with('success', 'Data personil berhasil disimpan');
        } else {
            return redirect('master/personil_inspeksi')->with('success', 'Data personil berhasil disimpan');
        }
    }

    public function isAktifPersonilInspeksi($id)
    {
        $personil = PersonilInspeksi::findOrFail($id);
        if ($personil->is_aktif == 0) {
            $personil->is_aktif = 1;
        } else {
            $personil->is_aktif = 0;
        }
        $personil->save();
        return redirect('master/personil_inspeksi');
    }

    /* ===================== END MASTER PERSONIL INSPEKSI=============== */


    /* ===============MASTER TRAINING============ */

    public function showTraining()
    {
        if(User::isCurrUserAllowPermission(PERMISSION_VIEW_PERSONIL)) {
            $training = Training::all();
            return view('master/training', compact('training'));
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function createTraining($id = 0)
    {
        if(User::isCurrUserAllowPermission(PERMISSION_CREATE_PERSONIL) || User::isCurrUserAllowPermission(PERMISSION_EDIT_PERSONIL)) {
            $training = ($id == 0) ? null : Training::find($id);
            return view('master/input_training', compact('training', 'id'));
        }else{
            return redirect('/internal')->with('error', 'You are not authorized.');
        }
    }

    public function storeTraining(Request $request)
    {
        $id = $request->id;
        $training = ($id == 0) ? new Training() : Training::findOrFail($id);
        $training->nama_training = $request->nama_training;
        $training->penyelenggara = $request->penyelenggara;
        $training->periode = $request->periode;
        $training->lokasi = $request->lokasi;
        $training->save();
        return redirect('master/training')->with('success', 'Data training berhasil disimpan');
    }

    public function deleteTraining($id)
    {
        $training = Training::findOrFail($id);
        $personil = $training->personil()->get();
        if (sizeof($personil) > 0) {
            return redirect('master/training')->with('fail', 'Data training digunakan pada personil, tidak dapat dihapus');
        } else {
            $training->delete();
            return redirect('master/training')->with('success', 'Data training berhasil dihapus');
        }
    }

    /* ===============END MASTER TRAINING============ */

    /* ===============MASTER PERSONIL TRAINING============ */

    public function createPersonilTraining($personil_id, $id = 0)
    {
        $personil_training = PersonilTraining::find($id);
        $personil = PersonilInspeksi::find($personil_id);
        if ($personil->user != null) {
            $user = $personil->user;
            $personil->nama = $user->nama_user;
            $personil->email = $user->email;
            $personil->nip = $user->nip_user;
        }
        $training = Training::all();
        return view('master/input_personil_training', compact('personil_training', 'personil', 'training', 'id', 'personil_id'));
    }

    public function storePersonilTraining(Request $request)
    {
        $id = $request->id;
        $personil_training = ($id == 0) ? new PersonilTraining() : PersonilTraining::find($id);
        $personil_training->personil_id = $request->personil_id;
        $personil_training->training_id = $request->training_id;
        $personil_training->save();
        return redirect('master/personil_inspeksi/input/' . $request->personil_id)->with('tab', 'training');
    }

    public function deletePersonilTraining($id)
    {
        $personil_training = PersonilTraining::findOrFail($id);
        $personil_training->delete();
        return redirect('master/personil_inspeksi/input/' . $personil_training->personil_id)->with('tab', 'training')
            ->with('success', 'Data riwayat berhasil dihapus !');
    }

    /* ===============END MASTER PERSONIL TRAINING============ */

    /* ===============MASTER KEAHLIAN============ */

    public function showKeahlian()
    {
        $keahlian = Keahlian::all();
        return view('master/keahlian', compact('keahlian'));
    }

    public function createKeahlian($id = 0)
    {
        $keahlian = ($id == 0) ? null : Keahlian::find($id);
        return view('master/input_keahlian', compact('keahlian', 'id'));
    }

    public function storeKeahlian(Request $request)
    {
        $id = $request->id;
        $keahlian = ($id == 0) ? new Keahlian() : Keahlian::findOrFail($id);
        $keahlian->nama_keahlian = $request->nama_keahlian;
        $keahlian->deskripsi = $request->deskripsi;
        $keahlian->save();
        return redirect('master/keahlian')->with('success', 'Data kompetensi berhasil disimpan');
    }

    public function deleteKeahlian($id)
    {
        $keahlian = Keahlian::findOrFail($id);
        $personil = $keahlian->personil()->get();
        if (sizeof($personil) > 0) {
            return redirect('master/keahlian')->with('fail', 'Data kompetensi digunakan pada personil, tidak dapat dihapus');
        } else {
            $keahlian->delete();
            return redirect('master/keahlian')->with('success', 'Data kompetensi berhasil dihapus');
        }
    }

    /* ===============END MASTER KEAHLIAN============ */

    /* ===============MASTER KEAHLIAN PERSONIL============ */

    public function createKeahlianPersonil($personil_id, $id = 0)
    {
        $keahlian_personil = KeahlianPersonil::find($id);
        $personil = PersonilInspeksi::find($personil_id);
//        $level = array("Pemula", "Menengah", "Mahir");
        $keahlian = Keahlian::all();
        if ($personil->user != null) {
            $user = $personil->user;
            $personil->nama = $user->nama_user;
            $personil->email = $user->email;
            $personil->nip = $user->nip_user;
        }
        return view('master/input_keahlian_personil', compact('keahlian_personil', 'personil', 'keahlian', 'id', 'personil_id'));
    }

    public function storeKeahlianPersonil(Request $request)
    {
        $id = $request->id;
        $keahlian_personil = ($id == 0) ? new KeahlianPersonil() : KeahlianPersonil::find($id);
        $keahlian_personil->keahlian_id = $request->keahlian_id;
        $keahlian_personil->personil_id = $request->personil_id;
        $keahlian_personil->level_keahlian = $request->level_keahlian;
        $keahlian_personil->periode = $request->periode;
        $keahlian_personil->lembaga = $request->lembaga;
        $keahlian_personil->no_sertifikat = $request->no_sertifikat;
        $keahlian_personil->save();

        $file = $request->file_sertifikat;
        if ($file != null) {
            if ($keahlian_personil->file_sertifikat != null) {
                $loc = storage_path() . '/upload/file_sertifikat/' . $keahlian_personil->file_sertifikat;
                if (file_exists($loc))
                    unlink($loc);
            }
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = 'file_sertifikat-' . $timestamp . '-' . $keahlian_personil->id . "-" . $keahlian_personil->personil->nama . "." . $file->getClientOriginalExtension();
            $keahlian_personil->file_sertifikat = $name;
            $file->move(storage_path() . '/upload/file_sertifikat/', $name);
            $keahlian_personil->save();
        }
        return redirect('master/personil_inspeksi/input/' . $request->personil_id)->with('tab', 'keahlian');
    }

    public function deleteKeahlianPersonil($id)
    {
        $keahlian_personil = KeahlianPersonil::findOrFail($id);
        $loc = "";
        if ($keahlian_personil->file_sertifikat != null) {
            $loc = storage_path() . '/upload/file_sertifikat/' . $keahlian_personil->file_sertifikat;
        }
        $keahlian_personil->delete();
        if (file_exists($loc))
            unlink($loc);
        return redirect('master/personil_inspeksi/input/' . $keahlian_personil->personil_id)->with('tab', 'keahlian')
            ->with('success', 'Data riwayat berhasil dihapus !');
    }

    /* ===============END MASTER KEAHLIAN PERSONIL============ */

    public function getNamaPemilikInstalasi(Request $request)
    {
        $data['response'] = 'false';
        $pemilik_instalasi = PemilikInstalasi::where('nama_pemilik', 'like', '%' . $request->nama_pemilik . '%')->get();
        if (sizeof($pemilik_instalasi) != 0) {
            $data['response'] = 'true';
            $data['message'] = array();
            foreach ($pemilik_instalasi as $item) {
                $arr_nama = array(
                    'value' => $item->nama_pemilik,
                    'alamat' => $item->alamat_pemilik,
                    'id_province' => $item->id_province,
                    'id_city' => $item->id_city,
                    'kode_pos' => $item->kode_pos_pemilik,
                    'telepon' => $item->telepon_pemilik,
                    'fax' => $item->no_fax_pemilik,
                    'email' => $item->email_pemilik);
                array_push($data['message'], $arr_nama);
            }
        }
        echo json_encode($data);
    }

    public function showUnsur()
    {
        $unsur = UnsurBiaya::withTrashed()->get();
        return view('master/unsur', compact('unsur'));
    }

    public function insertUnsur($id = "")
    {
        $id = ($id == "") ? 0 : $id;
        $unsur = null;
        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();

        if ($id > 0) {
            $unsur = UnsurBiaya::find($id);
        }
        //        dd($jenis);
        return view('master/input_unsur', compact('unsur', 'jenis'));
    }

    public function storeUnsur(Request $request)
    {
        $id = $request->id;
        $unsur = null;
        if ($id == 0) {
            $unsur = new UnsurBiaya();
        } else {
            $unsur = UnsurBiaya::find($id);
        }

        $unsur->kode = $request->kode;
        $unsur->unsur = $request->nama_unsur;
        $unsur->jenis = $request->jenis;
        $unsur->tarif = $request->tarif;
        $unsur->save();
        return redirect('master/unsur');
    }

    public function isAktifUnsur($id)
    {
        $unsur = UnsurBiaya::withTrashed()->where('id', $id)->first();
        //        dd($unsur);
        if ($unsur->deleted_at != null) {
            $unsur->deleted_at = null;
            $unsur->save();
        } else {
            $unsur->delete();
        }

        return redirect('master/unsur');
    }

    public function previewUnsur()
    {
        $jenis = UnsurBiaya::select(DB::raw('DISTINCT(jenis)'))->get();
        $unsur_arr = array();
        foreach ($jenis as $key => $item) {
            $unsur_arr[$item->jenis] = UnsurBiaya::where('jenis', $item->jenis)->get();
            $jenis[$key]['div_id'] = str_replace(' ', '', $item->jenis);
        }
        return view('master.preview_unsur_biaya', compact('jenis', 'unsur_arr'));
    }

    //START MASTER PROGRAM DISTRIBUSI===================================================

    public function showSuratTugas()
    {
        $id_perusahaan = (Auth::user() != null) ? Auth::user()->perusahaan_id : 0;
        $surat = SuratTugas::where('id_perusahaan', $id_perusahaan)->get();
        return view('eksternal/surat_tugas', compact('surat'));
    }

    public function insertSuratTugas($id = null)
    {
        $surat = ($id == null) ? null : SuratTugas::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_DISTRIBUSI);
        $layanan = $tipe_instalasi->produk()->get();
        $used_layanan = ($id == null) ? array() : LayananSuratTugas::select('produk_id')->where('surat_tugas_id', $id)->get()->toArray();
        return view('eksternal/create_surat_tugas', compact('surat', 'layanan', 'used_layanan'));
    }

    public function detailSuratTugas($id = null)
    {
        $surat = ($id == null) ? null : SuratTugas::findOrFail($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_DISTRIBUSI);
        $layanan = $tipe_instalasi->produk()->get();
        $used_layanan = ($id == null) ? array() : LayananSuratTugas::select('produk_id')->where('surat_tugas_id', $id)->get()->toArray();
        return view('eksternal/detail_surat_tugas', compact('surat', 'layanan', 'used_layanan'));
    }

    public function storeSuratTugas(Request $request)
    {
        $id = $request->id;
        $surat = null;
        $id_perusahaan = Auth::user()->perusahaan_id;
        if ($id == 0) {
            //insert surat tugas baru
            $surat = new SuratTugas();
            $surat->id_perusahaan = $id_perusahaan;
            $surat->user_create = ($request->session()->get('email') == null) ? "-" : $request->session()->get('key');
            $surat->user_update = "-";
        } else {
            //update surat tugas baru
            $surat = SuratTugas::findOrFail($id);
            $surat->user_update = ($request->session()->get('email') == null) ? "-" : $request->session()->get('key');
        }
        if ($surat != null) {
            $surat->periode = $request->periode;
            $surat->nomor_surat = $request->nomor_surat;
            $surat->tanggal_surat = $request->tanggal_surat;
            $file = $request->file_surat;
            if ($file != null) {
                if ($surat->file_surat != null) {
                    $loc = storage_path() . '/upload/surat_tugas/' . $surat->file_surat;
                    if (file_exists($loc))
                        unlink($loc);
                }
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = 'surat_tugas-' . $timestamp . '-' . $surat->periode . "." . $file->getClientOriginalExtension();
                $surat->file_surat = $name;
                $file->move(storage_path() . '/upload/surat_tugas/', $name);
            }
            $surat->save();
            //delete eksisting produk layanan
            LayananSuratTugas::where('surat_tugas_id', $surat->id)->delete();
            //insert produk layanan
            foreach ($request->produk as $prd) {
                $layananSurat = new LayananSuratTugas();
                $layananSurat->produk_id = $prd;
                $layananSurat->surat_tugas_id = $surat->id;
                $layananSurat->save();
            }
            Session::flash('message', 'Data Surat Tugas Berhasil Tersimpan');
        }
        return redirect('eksternal/create_surat_tugas/' . $surat->id)->with('success', 'Surat Tugas Berhasil Disimpan.');
    }

    public function deleteSuratTugas($id)
    {
        $surat = SuratTugas::findOrFail($id);
        if ($surat->isInUsed()) {
            $type = "error";
            $text = "Surat Tugas dipakai dalam order, tidak dapat dihapus";
        } else {
            $surat->delete();
            $type = "success";
            $text = "Surat Tugas berhasil dihapus";
        }
        return redirect('eksternal/surat_tugas')->with($type, $text);
    }

    //program distribusi=============================================================
    public function detailProgramDistribusi($id_surat_tugas, $id = 0)
    {
        $surat = SuratTugas::find($id_surat_tugas);
        $program = Program::find($id);
        $jenis_program = JenisProgram::all();

        return view('eksternal/program-distribusi/detail_program', compact('surat', 'program', 'id', 'jenis_program', 'id_surat_tugas'));
    }

    public function insertProgramDistribusi($id_surat_tugas, $id = 0)
    {
        $surat = SuratTugas::find($id_surat_tugas);
        $program = Program::find($id);
        $jenis_program = JenisProgram::all();

        return view('eksternal/program-distribusi/form_program', compact('surat', 'program', 'id', 'jenis_program', 'id_surat_tugas'));
    }

    public function storeProgramDistribusi(Request $request)
    {
        $id = $request->id;
        $program = ($id == 0) ? new Program() : Program::find($id);
        $program->deskripsi = $request->deskripsi;
        $program->id_jenis_program = $request->id_jenis_program;
        $program->id_surat_tugas = $request->id_surat_tugas;
        $program->periode = $request->periode;
        if ($id == 0) {
            $program->user_create = Auth::user()->username;
        }
        $program->user_update = Auth::user()->username;
        $program->perusahaan_id = Auth::user()->perusahaan_id;
        $program->save();

        return redirect('eksternal/create_surat_tugas/' . $request->id_surat_tugas)->with('success', 'Data Program Distribusi Berhasil Disimpan')->with('tab', 'program');
    }

    public function deleteProgramDistribusi($id_surat_tugas, $id)
    {
        $program = Program::findOrFail($id);
        $program->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //area program===================================================
    public function detailAreaProgram($id_program, $id = 0)
    {
        $ba = BusinessArea::all();
        $program = Program::find($id_program);
        $ap = ($id == 0) ? null : AreaProgram::find($id);
        $lokasi_penyulang = array();
        return view('eksternal/program-distribusi/detail_area', compact('id', 'ap', 'ba', 'program', 'lokasi_penyulang'));
    }

    public function insertAreaProgram($id_program, $id = 0)
    {
        $ba = BusinessArea::all();
        $program = Program::find($id_program);
        $ap = ($id == 0) ? null : AreaProgram::find($id);
        $lokasi_penyulang = array();
        return view('eksternal/program-distribusi/form_area', compact('id', 'ap', 'ba', 'program', 'lokasi_penyulang'));
    }

    public function storeAreaProgram(Request $request)
    {
        $id = $request->id;
        $ap = ($id == 0) ? new AreaProgram() : AreaProgram::findOrFail($id);
        $ap->business_area = $request->ba;
        $ap->id_program = $request->id_program;
        $ap->save();
        $program = Program::find($request->id_program);
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with('success', 'Data Area Program Berhasil Tersimpan')->with('tab', 'program');
    }

    public function deleteAreaProgram($id_surat_tugas, $id)
    {
        $area_program = AreaProgram::findOrFail($id);
        $area_program->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Area Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //kontrak program============================================================
    public function detailKontrakProgram($id_program, $id = 0)
    {
        $kontrak = ($id == 0) ? null : KontrakProgram::findOrFail($id);
        $program = Program::find($id_program);
        $lokasi = array();
        return view('eksternal/program-distribusi/detail_kontrak', compact('kontrak', 'program', 'lokasi'));
    }

    public function insertKontrakProgram($id_program, $id = 0)
    {
        $kontrak = ($id == 0) ? null : KontrakProgram::findOrFail($id);
        $program = Program::find($id_program);
        $lokasi = array();
        return view('eksternal/program-distribusi/form_kontrak', compact('kontrak', 'program', 'lokasi'));
    }

    public function storeKontrakProgram(Request $request)
    {
        $id = $request->id;
        $kontrak = ($id == 0) ? new KontrakProgram() : KontrakProgram::findOrFail($id);
        if ($id == 0) {
            //insert kontrak
            $kontrak->user_create = ($request->session()->get('email') == null) ? "-" : $request->session()->get('key');
            $kontrak->user_update = "-";
        } else {
            //update kontrak
            $kontrak->user_update = ($request->session()->get('email') == null) ? "-" : $request->session()->get('key');
        }
        $program = Program::find($request->id_program);
        if ($kontrak != null) {
            $kontrak->id_program = $request->id_program;
            $kontrak->periode = $request->periode;
            $kontrak->nomor_kontrak = $request->nomor_kontrak;
            $kontrak->tanggal_kontrak = $request->tanggal_kontrak;
            $kontrak->vendor = $request->vendor;
            $kontrak->save();
            $status = "success";
            $text = "Data Kontrak Program Berhasil Disimpan";
        } else {
            $status = "error";
            $text = "Data Kontrak Program Gagal Disimpan";
        }
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with($status, $text)->with('tab', 'program');
    }

    public function deleteKontrakProgram($id_surat_tugas, $id)
    {
        $kontak_program = KontrakProgram::findOrFail($id);
        $kontak_program->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Kontrak Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //lokasi program====================================
    public function detailLokasiArea($id_program, $id_area_kontrak, $id = 0)
    {
        $program = Program::findOrFail($id_program);
        $lokasi_area = ($id == 0) ? null : LokasiArea::findOrFail($id);
        $kontrak = (strtoupper($program->jenisProgram->acuan_1) == TIPE_KONTRAK) ? KontrakProgram::find($id_area_kontrak) : null;
        $area = (strtoupper($program->jenisProgram->acuan_1) == TIPE_AREA) ? AreaProgram::find($id_area_kontrak) : null;
        $parent = ($kontrak != null) ? $kontrak->nomor_kontrak : $area->businessArea->description;
        $id_area = ($area != null) ? $id_area_kontrak : null;
        $id_kontrak = ($kontrak != null) ? $id_area_kontrak : null;
        return view('eksternal/program-distribusi/detail_lokasi', compact('lokasi_area', 'kontrak', 'area', 'program', 'parent', 'id_area', 'id_kontrak'));
    }

    public function insertLokasiArea($id_program, $id_area_kontrak, $id = 0)
    {
        $program = Program::findOrFail($id_program);
        $lokasi_area = ($id == 0) ? null : LokasiArea::findOrFail($id);
        $kontrak = (strtoupper($program->jenisProgram->acuan_1) == TIPE_KONTRAK) ? KontrakProgram::find($id_area_kontrak) : null;
        $area = (strtoupper($program->jenisProgram->acuan_1) == TIPE_AREA) ? AreaProgram::find($id_area_kontrak) : null;
        $parent = ($kontrak != null) ? $kontrak->nomor_kontrak : $area->businessArea->description;
        $id_area = ($area != null) ? $id_area_kontrak : null;
        $id_kontrak = ($kontrak != null) ? $id_area_kontrak : null;
        return view('eksternal/program-distribusi/form_lokasi', compact('lokasi_area', 'kontrak', 'area', 'program', 'parent', 'id_area', 'id_kontrak'));
    }

    public function storeLokasiArea(Request $request)
    {
        $lokasi_id = $request->id_lokasi;
        $lokasi = Lokasi::find($lokasi_id);
        if ($lokasi == null) {
            //insert to lokasi
            $lokasi = new Lokasi();
            $lokasi->lokasi = $request->lokasi;
            $lokasi->save();
        }
        //insert to lokasi_area
        $id = $request->id;
        $lokasi_area = ($id != 0) ? LokasiArea::find($id) : new LokasiArea();
        $program = Program::find($request->id_program);
        if ($lokasi_area != null) {
            $lokasi_area->id_area_program = $request->id_area_program;
            $lokasi_area->id_kontrak = $request->id_kontrak;
            $lokasi_area->id_lokasi = $lokasi->id;
            $lokasi_area->save();
            $status = "success";
            $text = "Data Lokasi Program Berhasil Disimpan";
        } else {
            $status = "error";
            $text = "Data Lokasi Program Gagal Disimpan";
        }
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with($status, $text)->with('tab', 'program');
    }

    public function deleteLokasiArea($id_surat_tugas, $id)
    {
        $lokasi_area = LokasiArea::findOrFail($id);
        $lokasi_area->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Lokasi Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //penyulang area
    public function detailPenyulangArea($id_program, $id_area_program, $id = 0)
    {
        $area = AreaProgram::find($id_area_program);
        $program = $area->program;
        $penyulang_area = ($id == 0) ? null : PenyulangArea::findOrFail($id);
        return view('eksternal/program-distribusi/detail_penyulang', compact('penyulang_area', 'id_area_program', 'area', 'id_program', 'program'));
    }

    public function insertPenyulangArea($id_program, $id_area_program, $id = 0)
    {
        $area = AreaProgram::find($id_area_program);
        $program = $area->program;
        $penyulang_area = ($id == 0) ? null : PenyulangArea::findOrFail($id);
        return view('eksternal/program-distribusi/form_penyulang', compact('penyulang_area', 'id_area_program', 'area', 'id_program', 'program'));
    }

    public function storePenyulangArea(Request $request)
    {
        $penyulang_id = $request->id_penyulang;
        $penyulang = Penyulang::find($penyulang_id);
        if ($penyulang == null) {
            //insert to penyulang
            $penyulang = new Penyulang();
            $penyulang->penyulang = $request->penyulang;
            $penyulang->save();
        }
        //insert to lokasi_area
        $id = $request->id;
        $program = Program::find($request->id_program);
        $penyulang_area = ($id != 0) ? PenyulangArea::find($id) : new PenyulangArea();
        if ($penyulang_area != null) {
            $penyulang_area->id_area_program = $request->id_area_program;
            $penyulang_area->id_penyulang = $penyulang->id;
            $penyulang_area->save();
            $status = "success";
            $text = "Data Penyulang Program Berhasil Disimpan";
        } else {
            $status = "error";
            $text = "Data Penyulang Program Gagal Disimpan";
        }
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with($status, $text)->with('tab', 'program');
    }

    public function deletePenyulangArea($id_surat_tugas, $id)
    {
        $penyulang_area = PenyulangArea::findOrFail($id);
        $penyulang_area->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Penyulang Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //gardu area
    public function detailGarduArea($id_program, $id_area_program, $id = 0)
    {
        $area = AreaProgram::find($id_area_program);
        $program = $area->program;
        $gardu_area = ($id == 0) ? null : GarduArea::findOrFail($id);
        return view('eksternal/program-distribusi/detail_gardu', compact('gardu_area', 'id_area_program', 'area', 'id_program', 'program'));
    }

    public function insertGarduArea($id_program, $id_area_program, $id = 0)
    {
        $area = AreaProgram::find($id_area_program);
        $program = $area->program;
        $gardu_area = ($id == 0) ? null : GarduArea::findOrFail($id);
        return view('eksternal/program-distribusi/form_gardu', compact('gardu_area', 'id_area_program', 'area', 'id_program', 'program'));
    }

    public function storeGarduArea(Request $request)
    {
        $gardu_id = $request->id_gardu;
        $gardu = GarduInduk::find($gardu_id);
        if ($gardu == null) {
            //insert to gardu induk
            $gardu = new GarduInduk();
            $gardu->gardu_induk = $request->gardu;
            $gardu->save();
        }
        //insert to lokasi_area
        $id = $request->id;
        $program = Program::find($request->id_program);
        $gardu_area = ($id != 0) ? GarduArea::find($id) : new GarduArea();
        if ($gardu_area != null) {
            $gardu_area->id_area_program = $request->id_area_program;
            $gardu_area->id_gardu_induk = $gardu->id;
            $gardu_area->save();
            $status = "success";
            $text = "Data Gardu Program Berhasil Disimpan";
        } else {
            $status = "error";
            $text = "Data Gardu Program Gagal Disimpan";
        }
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with($status, $text)->with('tab', 'program');
    }

    public function deleteGarduArea($id_surat_tugas, $id)
    {
        $gardu_area = GarduArea::findOrFail($id);
        $gardu_area->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Gardu Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //instalasi program distribusi
    public function detailInstalasiProgram($id_program, $id_area_program, $id_parent, $id = 0)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 3)->get();  //sementara manual
        $distribusi = ($id == 0) ? null : InstalasiDistribusi::find($id);
        $ref_parent = References::where('nama_reference', 'Instalasi Distribusi')->first();
        $jenis_distribusi = References::where('parent', $ref_parent->id)->get();
        $program = Program::find($id_program);
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $parent = null;
        switch ($program->jenisProgram->acuan_2) {
            case TIPE_GARDU:
                $gardu = GarduArea::find($id_parent);
                $parent = $gardu->garduInduk->gardu_induk;
                break;
            case TIPE_PENYULANG:
                $penyulang = PenyulangArea::find($id_parent);
                $parent = $penyulang->penyulang->penyulang;
                break;
            case TIPE_LOKASI:
                $lokasi = LokasiArea::find($id_parent);
                $parent = $lokasi->lokasi->lokasi;
                break;
        }

        // longitude awal
        $longitude_awal = '';
        $longitude_awal = explode(' ° ', ($id != null) ? $distribusi->longitude_awal : '');
        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
        // longitude akhir
        $longitude_akhir = '';
        $longitude_akhir = explode(' ° ', ($id != null) ? $distribusi->longitude_akhir : '');
        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
        // latitude awal
        $latitude_awal = '';
        $latitude_awal = explode(' ° ', ($id != null) ? $distribusi->latitude_awal : '');
        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
        // latitude akhir
        $latitude_akhir = '';
        $latitude_akhir = explode(' ° ', ($id != null) ? $distribusi->latitude_akhir : '');
        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';

        array_add($distribusi, 'longitude_awal_deg', $longitude_awal_deg);
        array_add($distribusi, 'longitude_awal_min', $longitude_awal_min);
        array_add($distribusi, 'longitude_awal_sec', $longitude_awal_sec);
        array_add($distribusi, 'longitude_awal_dir', $longitude_awal_dir);
        array_add($distribusi, 'longitude_akhir_deg', $longitude_akhir_deg);
        array_add($distribusi, 'longitude_akhir_min', $longitude_akhir_min);
        array_add($distribusi, 'longitude_akhir_sec', $longitude_akhir_sec);
        array_add($distribusi, 'longitude_akhir_dir', $longitude_akhir_dir);
        array_add($distribusi, 'latitude_awal_deg', $latitude_awal_deg);
        array_add($distribusi, 'latitude_awal_min', $latitude_awal_min);
        array_add($distribusi, 'latitude_awal_sec', $latitude_awal_sec);
        array_add($distribusi, 'latitude_awal_dir', $latitude_awal_dir);
        array_add($distribusi, 'latitude_akhir_deg', $latitude_akhir_deg);
        array_add($distribusi, 'latitude_akhir_min', $latitude_akhir_min);
        array_add($distribusi, 'latitude_akhir_sec', $latitude_akhir_sec);
        array_add($distribusi, 'latitude_akhir_dir', $latitude_akhir_dir);

        //lingkup pekerjaan
        $lingkup = LingkupPekerjaan::where('tipe_instalasi_id', ID_DISTRIBUSI)->get();
        $used_lingkup = ($id == 0) ? array() : LingkupDistribusi::select('lingkup_id')->where('instalasi_id', $id)->get()->toArray();
        return view('eksternal/program-distribusi/detail_instalasi_program', compact('id_area_program', 'id_program', 'id_parent', 'distribusi', 'jenis_distribusi', 'program', 'jenis_instalasi', 'tegangan_pengenal', 'pemilik_instalasi', 'province', 'city', 'jenis_ijin_usaha', 'pemilik', 'parent', 'lingkup', 'used_lingkup'));
    }

    public function insertInstalasiProgram($id_program, $id_area_program, $id_parent, $id = 0)
    {
        $jenis_instalasi = JenisInstalasi::where('tipe_instalasi', 3)->get();  //sementara manual
        $distribusi = ($id == 0) ? null : InstalasiDistribusi::find($id);
        $ref_parent = References::where('nama_reference', 'Instalasi Distribusi')->first();
        $jenis_distribusi = References::where('parent', $ref_parent->id)->get();
        $program = Program::find($id_program);
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();
        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
        $province = Provinces::all();
        $city = Cities::with('province')->get();
        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();
        $pemilik = PemilikInstalasi::where('perusahaan_id', Auth::user()->perusahaan_id)->get();
        $parent = null;
        switch ($program->jenisProgram->acuan_2) {
            case TIPE_GARDU:
                $gardu = GarduArea::find($id_parent);
                $parent = $gardu->garduInduk->gardu_induk;
                break;
            case TIPE_PENYULANG:
                $penyulang = PenyulangArea::find($id_parent);
                $parent = $penyulang->penyulang->penyulang;
                break;
            case TIPE_LOKASI:
                $lokasi = LokasiArea::find($id_parent);
                $parent = $lokasi->lokasi->lokasi;
                break;
        }

        // longitude awal
        $longitude_awal = '';
        $longitude_awal = explode(' ° ', ($id != null) ? $distribusi->longitude_awal : '');
        $longitude_awal_deg = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' \' ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_min = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal = explode(' " ', ($id != null) ? $longitude_awal[1] : '');
        $longitude_awal_sec = ($id != null) ? $longitude_awal[0] : '';
        $longitude_awal_dir = ($id != null) ? $longitude_awal[1] : '';
        // longitude akhir
        $longitude_akhir = '';
        $longitude_akhir = explode(' ° ', ($id != null) ? $distribusi->longitude_akhir : '');
        $longitude_akhir_deg = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' \' ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_min = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir = explode(' " ', ($id != null) ? $longitude_akhir[1] : '');
        $longitude_akhir_sec = ($id != null) ? $longitude_akhir[0] : '';
        $longitude_akhir_dir = ($id != null) ? $longitude_akhir[1] : '';
        // latitude awal
        $latitude_awal = '';
        $latitude_awal = explode(' ° ', ($id != null) ? $distribusi->latitude_awal : '');
        $latitude_awal_deg = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' \' ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_min = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal = explode(' " ', ($id != null) ? $latitude_awal[1] : '');
        $latitude_awal_sec = ($id != null) ? $latitude_awal[0] : '';
        $latitude_awal_dir = ($id != null) ? $latitude_awal[1] : '';
        // latitude akhir
        $latitude_akhir = '';
        $latitude_akhir = explode(' ° ', ($id != null) ? $distribusi->latitude_akhir : '');
        $latitude_akhir_deg = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' \' ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_min = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir = explode(' " ', ($id != null) ? $latitude_akhir[1] : '');
        $latitude_akhir_sec = ($id != null) ? $latitude_akhir[0] : '';
        $latitude_akhir_dir = ($id != null) ? $latitude_akhir[1] : '';

        array_add($distribusi, 'longitude_awal_deg', $longitude_awal_deg);
        array_add($distribusi, 'longitude_awal_min', $longitude_awal_min);
        array_add($distribusi, 'longitude_awal_sec', $longitude_awal_sec);
        array_add($distribusi, 'longitude_awal_dir', $longitude_awal_dir);
        array_add($distribusi, 'longitude_akhir_deg', $longitude_akhir_deg);
        array_add($distribusi, 'longitude_akhir_min', $longitude_akhir_min);
        array_add($distribusi, 'longitude_akhir_sec', $longitude_akhir_sec);
        array_add($distribusi, 'longitude_akhir_dir', $longitude_akhir_dir);
        array_add($distribusi, 'latitude_awal_deg', $latitude_awal_deg);
        array_add($distribusi, 'latitude_awal_min', $latitude_awal_min);
        array_add($distribusi, 'latitude_awal_sec', $latitude_awal_sec);
        array_add($distribusi, 'latitude_awal_dir', $latitude_awal_dir);
        array_add($distribusi, 'latitude_akhir_deg', $latitude_akhir_deg);
        array_add($distribusi, 'latitude_akhir_min', $latitude_akhir_min);
        array_add($distribusi, 'latitude_akhir_sec', $latitude_akhir_sec);
        array_add($distribusi, 'latitude_akhir_dir', $latitude_akhir_dir);

        //lingkup pekerjaan
        $lingkup = LingkupPekerjaan::where('tipe_instalasi_id', ID_DISTRIBUSI)->get();
        $used_lingkup = ($id == 0) ? array() : LingkupDistribusi::select('lingkup_id')->where('instalasi_id', $id)->get()->toArray();
        return view('eksternal/program-distribusi/form_instalasi_program', compact('id_area_program', 'id_program', 'id_parent', 'distribusi', 'jenis_distribusi', 'program', 'jenis_instalasi', 'tegangan_pengenal', 'pemilik_instalasi', 'province', 'city', 'jenis_ijin_usaha', 'pemilik', 'parent', 'lingkup', 'used_lingkup'));
    }

    public function storeInstalasiProgram(Request $request)
    {
        $timestamp = date('YmdHis');
        $id = $request->id;
        $distribusi = new InstalasiDistribusi();
        $pemilik_instalasi_id = $request->pemilik_instalasi_id;
        $pemilik_instalasi_baru_id = $request->pemilik_instalasi_baru_id;
        $tipe_pemilik = $request->tipe_pemilik;
        $program = Program::find($request->id_program);
        $id_parent = $request->id_parent;
        if ($id != 0) {
            //update
            $distribusi = InstalasiDistribusi::findOrFail($id);
        }

        //Data Parent
        $distribusi->id_jenis_instalasi_distribusi = $request->id_jenis_instalasi_distribusi;
        switch ($program->jenisProgram->acuan_2) {
            case TIPE_GARDU:
                $distribusi->id_gardu_area = $id_parent;
                break;
            case TIPE_PENYULANG:
                $distribusi->id_penyulang_area = $id_parent;
                break;
            case TIPE_LOKASI:
                $distribusi->id_lokasi_area = $id_parent;
                break;
        }

        //        Data Umum
        $distribusi->id_program = $request->id_program;
        $distribusi->id_surat_tugas = $program->id_surat_tugas;
        $distribusi->status_baru = $request->status_baru;
        $distribusi->id_jenis_instalasi = $request->id_jenis_instalasi;
        $distribusi->nama_instalasi = $request->nama_instalasi;
        $distribusi->panjang_kms = $request->panjang_kms;
        $distribusi->jml_cubicle = $request->jml_cubicle;
        $distribusi->jml_gardu = $request->jml_gardu;
        $distribusi->tegangan_pengenal = $request->tegangan_pengenal;
        $distribusi->id_perusahaan = Auth::user()->perusahaan_id;
        $distribusi->keterangan = $request->keterangan;

        //        Kapasitas
        $distribusi->kapasitas_gardu = $request->kapasitas_gardu;
        $distribusi->kapasitas_arus_hubung_singkat = $request->kapasitas_arus_hubung_singkat;


        //        Pemilik Instalasi
        if ($tipe_pemilik == MILIK_SENDIRI) {
            $distribusi->pemilik_instalasi_id = $pemilik_instalasi_id;
        } elseif ($tipe_pemilik == TERDAFTAR) {
            $distribusi->pemilik_instalasi_id = $request->pemilik_instalasi_lain;
        } else {
            //            Create Pemilik Instalasi
            $pemilik = ($id == 0) ? new PemilikInstalasi() : PemilikInstalasi::find($pemilik_instalasi_baru_id);
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->alamat_pemilik = trim($request->alamat_pemilik);
            $pemilik->id_province = $request->provinsi_pemilik;
            $pemilik->id_city = $request->kabupaten_pemilik;
            $pemilik->kode_pos_pemilik = $request->kode_pos;
            $pemilik->telepon_pemilik = $request->telepon;
            $pemilik->no_fax_pemilik = $request->no_fax;
            $pemilik->email_pemilik = $request->email_pemilik;

            $pemilik->jenis_ijin_usaha = $request->jenis_ijin_usaha;
            $pemilik->penerbit_ijin_usaha = $request->penerbit_ijin_usaha;
            $pemilik->no_ijin_usaha = $request->no_ijin_usaha;
            $pemilik->masa_berlaku_iu = Carbon::parse($request->masa_berlaku_iu);
            if ($request->has_sewa == 1) {
                $pemilik->nama_kontrak = $request->nama_kontrak;
                $pemilik->no_kontrak = $request->no_kontrak;
                $pemilik->tgl_pengesahan_kontrak = Carbon::parse($request->tgl_pengesahan_kontrak);
                $pemilik->masa_berlaku_kontrak = Carbon::parse($request->masa_berlaku_kontrak);
            } else {
                $pemilik->nama_kontrak = '';
                $pemilik->no_kontrak = '';
                $pemilik->tgl_pengesahan_kontrak = null;
                $pemilik->masa_berlaku_kontrak = null;
            }
            $pemilik->no_spjbtl = $request->no_spjbtl;
            $pemilik->tgl_spjbtl = Carbon::parse($request->tgl_spjbtl);
            $pemilik->masa_berlaku_spjbtl = Carbon::parse($request->masa_berlaku_spjbtl);
            $pemilik->user_create = Auth::user()->username;
            $pemilik->user_update = Auth::user()->username;
            $pemilik->perusahaan_id = Auth::user()->perusahaan_id;
            $cstm_fl_nm = '-' . $timestamp . '-' . $request->email_pemilik . '.';
            $pemilik->save();

            /* -----------------------------file upload pemilik instalasi------------------------------------ */
            if ($request->hasFile('file_surat_iu')) {
                $pemilik->file_siup = 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_siup_pemilik_instalasi/' . $pemilik->file_siup;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_surat_iu->move(storage_path() . '/upload/file_siup_pemilik_instalasi', 'file_siup_pemilik_instalasi' . $cstm_fl_nm . $request->file_surat_iu->getClientOriginalExtension());
            }

            if ($request->has_sewa == 1) {
                if ($request->hasFile('file_kontrak_sewa')) {
                    $pemilik->file_kontrak = 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension();
                    $fullPath = storage_path() . '/upload/file_kontrak/' . $pemilik->file_kontrak;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_kontrak_sewa->move(storage_path() . '/upload/file_kontrak', 'file_kontrak' . $cstm_fl_nm . $request->file_kontrak_sewa->getClientOriginalExtension());
                }
            } else {
                $pemilik->file_kontrak = null;
            }
            if ($request->hasFile('file_spjbtl')) {
                $pemilik->file_spjbtl = 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension();
                $fullPath = storage_path() . '/upload/file_spjbtl/' . $pemilik->file_spjbtl;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_pernyataanjbtl->move(storage_path() . '/upload/file_spjbtl', 'file_spjbtl' . $cstm_fl_nm . $request->file_pernyataanjbtl->getClientOriginalExtension());
            }

            $pemilik->save();

            $distribusi->pemilik_instalasi_id = $pemilik->id;
        }

        //      Lokasi Instalasi
        $distribusi->lokasi = $request->lokasi;
        $distribusi->id_provinsi = $request->provinsi;
        $distribusi->id_kota = $request->kabupaten;
        $distribusi->longitude_awal = $request->longitude_awal_deg . ' ° ' . $request->longitude_awal_min . ' \' ' . $request->longitude_awal_sec . ' " ' . $request->longitude_awal_dir;
        $distribusi->latitude_awal = $request->latitude_awal_deg . ' ° ' . $request->latitude_awal_min . ' \' ' . $request->latitude_awal_sec . ' " ' . $request->latitude_awal_dir;
        $distribusi->longitude_akhir = $request->longitude_akhir_deg . ' ° ' . $request->longitude_akhir_min . ' \' ' . $request->longitude_akhir_sec . ' " ' . $request->longitude_akhir_dir;
        $distribusi->latitude_akhir = $request->latitude_akhir_deg . ' ° ' . $request->latitude_akhir_min . ' \' ' . $request->latitude_akhir_sec . ' " ' . $request->latitude_akhir_dir;

        if ($id == 0) {
            $distribusi->created_by = Auth::user()->username;
        }
        $distribusi->updated_by = Auth::user()->username;
        $distribusi->tipe_pemilik = $tipe_pemilik;

        $distribusi->save();

        //save lingkup pekerjaan
        //delete yg lama
        LingkupDistribusi::where('instalasi_id', $distribusi->id)->delete();
        //insert yg baru
        $lingkup = $request->lingkup_pekerjaan;
        foreach ($lingkup as $item) {
            $lingkupDistribusi = new LingkupDistribusi();
            $lingkupDistribusi->instalasi_id = $distribusi->id;
            $lingkupDistribusi->lingkup_id = $item;
            $lingkupDistribusi->save();
        }
        $status = "success";
        $text = "Data Instalasi Berhasil Disimpan";
        return redirect('eksternal/create_surat_tugas/' . $program->id_surat_tugas)->with($status, $text)->with('tab', 'program');
    }

    public function deleteInstalasiProgram($id_surat_tugas, $id)
    {
        $instalasi_distribusi = InstalasiDistribusi::findOrFail($id);
        $instalasi_distribusi->delete();
        return redirect('eksternal/create_surat_tugas/' . $id_surat_tugas)->with('success', 'Data Instalasi Program Distribusi Berhasil Dihapus')->with('tab', 'program');
    }

    //global function edit & delete from komponen program distribusi


    public function editKomponenProgramDistribusi($param1 = 0, $param2 = 0, $param3 = 0, $param4 = 0, $tipe)
    {
        switch ($tipe) {
            case TIPE_PROGRAM:
                return $this->insertProgramDistribusi($param1, $param2);
                break;
            case TIPE_AREA:
                return $this->insertAreaProgram($param1, $param2);
                break;
            case TIPE_KONTRAK:
                return $this->insertKontrakProgram($param1, $param2);
                break;
            case TIPE_LOKASI:
                return $this->insertLokasiArea($param1, $param2, $param3);
                break;
            case TIPE_PENYULANG:
                return $this->insertPenyulangArea($param1, $param2, $param3);
                break;
            case TIPE_GARDU:
                return $this->insertGarduArea($param1, $param2, $param3);
                break;
            case TIPE_INSTALASI_PROGRAM:
                return $this->insertInstalasiProgram($param1, $param2, $param3, $param4);
                break;
        }
    }

    public function deleteKomponenProgramDistribusi($id_surat_tugas, $id, $tipe)
    {
        switch ($tipe) {
            case TIPE_PROGRAM:
                return $this->deleteProgramDistribusi($id_surat_tugas, $id);
                break;
            case TIPE_AREA:
                return $this->deleteAreaProgram($id_surat_tugas, $id);
                break;
            case TIPE_KONTRAK:
                return $this->deleteKontrakProgram($id_surat_tugas, $id);
                break;
            case TIPE_LOKASI:
                return $this->deleteLokasiArea($id_surat_tugas, $id);
                break;
            case TIPE_PENYULANG:
                return $this->deletePenyulangArea($id_surat_tugas, $id);
                break;
            case TIPE_GARDU:
                return $this->deleteGarduArea($id_surat_tugas, $id);
                break;
            case TIPE_INSTALASI_PROGRAM:
                return $this->deleteInstalasiProgram($id_surat_tugas, $id);
                break;
        }
    }

    public function detailKomponenProgramDistribusi($param1 = 0, $param2 = 0, $param3 = 0, $param4 = 0, $tipe)
    {
        switch ($tipe) {
            case TIPE_PROGRAM:
                return $this->detailProgramDistribusi($param1, $param2);
                break;
            case TIPE_AREA:
                return $this->detailAreaProgram($param1, $param2);
                break;
            case TIPE_KONTRAK:
                return $this->detailKontrakProgram($param1, $param2);
                break;
            case TIPE_LOKASI:
                return $this->detailLokasiArea($param1, $param2, $param3);
                break;
            case TIPE_PENYULANG:
                return $this->detailPenyulangArea($param1, $param2, $param3);
                break;
            case TIPE_GARDU:
                return $this->detailGarduArea($param1, $param2, $param3);
                break;
            case TIPE_INSTALASI_PROGRAM:
                return $this->detailInstalasiProgram($param1, $param2, $param3, $param4);
                break;
        }
    }

    //lokasi
    public function getLokasi(Request $request)
    {
        $data['response'] = 'false';
        $lokasi = Lokasi::where('LOWER(lokasi)', 'like', '%' . strtolower($request->lokasi) . '%')->get();
        if (sizeof($lokasi) != 0) {
            $data['response'] = 'true';
            $data['message'] = array();
            foreach ($lokasi as $item) {
                $arr_nama = array(
                    'value' => $item->lokasi,
                    'id' => $item->id);
                array_push($data['message'], $arr_nama);
            }
        }
        echo json_encode($data);
    }

    //penyulang
    public function getPenyulang(Request $request)
    {
        $data['response'] = 'false';
        $penyulang = Penyulang::where('LOWER(penyulang)', 'like', '%' . strtolower($request->penyulang) . '%')->get();
        if (sizeof($penyulang) != 0) {
            $data['response'] = 'true';
            $data['message'] = array();
            foreach ($penyulang as $item) {
                $arr_nama = array(
                    'value' => $item->penyulang,
                    'id' => $item->id);
                array_push($data['message'], $arr_nama);
            }
        }
        echo json_encode($data);
    }

    //gardu
    public function getGardu(Request $request)
    {
        $data['response'] = 'false';
        $gardu = GarduInduk::where('LOWER(gardu_induk)', 'like', '%' . strtolower($request->gardu_induk) . '%')->get();
        if (sizeof($gardu) != 0) {
            $data['response'] = 'true';
            $data['message'] = array();
            foreach ($gardu as $item) {
                $arr_nama = array(
                    'value' => $item->gardu_induk,
                    'id' => $item->id);
                array_push($data['message'], $arr_nama);
            }
        }
        echo json_encode($data);
    }

    //END MASTER PROGRAM DISTRIBUSI=====================================================

    //START MASTER STRUKTUR HILO ======================================================

    public function showStrukturHilo($lingkup_id = 0)
    {
        $lingkup = LingkupPekerjaan::all();
        $struktur = StrukturHilo::where('lingkup_pekerjaan_id', $lingkup_id)->orderBy('id', 'desc')->get();
        $lingkup_pekerjaan = LingkupPekerjaan::find($lingkup_id);
        return view('master/struktur_hilo', compact('lingkup', 'struktur', 'lingkup_id', 'lingkup_pekerjaan'));
    }


    public function createStrukturHilo($lingkup_id)
    {
        $lingkup = LingkupPekerjaan::find($lingkup_id);
        return view('master/input_struktur_hilo', compact('lingkup', 'lingkup_id'));
    }

    public function storeStrukturHilo(Request $request)
    {
        $lingkup_id = $request->lingkup_id;
        $id = $request->id;
        $struktur = ($id > 0) ? StrukturHilo::findOrFail($id) : new StrukturHilo();
        $struktur->lingkup_pekerjaan_id = $lingkup_id;
        $struktur->mata_uji = $request->mata_uji;
        $struktur->kriteria = $request->kriteria;
        $struktur->is_parent = $request->is_parent;
        $struktur->is_dokumen = $request->is_dokumen;
        $struktur->nomor = $request->nomor;
        $struktur->sort_num = $request->sort_num;
        $struktur->save();
        return redirect('master/struktur_hilo/' . $lingkup_id)->with('success', 'Mata uji berhasil disimpan');
    }

    public function editStrukturHilo($id)
    {
        $struktur = StrukturHilo::find($id);
        $lingkup = $struktur->lingkup_pekerjaan;
        $lingkup_id = $struktur->lingkup_pekerjaan_id;
        return view('master/input_struktur_hilo', compact('struktur', 'id', 'lingkup', 'lingkup_id'));
    }

    public function destroyStrukturHilo($id)
    {
        $struktur = StrukturHilo::find($id);
        $lingkup = $struktur->lingkup_pekerjaan_id;
        $struktur->delete();
        return redirect('master/struktur_hilo/' . $lingkup)->with('success', 'Mata uji berhasil dihapus');
    }

    public function previewStrukturHilo($lingkup_id)
    {
        $struktur = StrukturHilo::getByLingkup($lingkup_id);
        $lingkup_pekerjaan = LingkupPekerjaan::find($lingkup_id);
        $form = FormHilo::getByLingkup($lingkup_id);
        return view('master/preview_struktur_hilo', compact('struktur', 'lingkup_id', 'lingkup_pekerjaan', 'form'));
    }

    //END MASTER STRUKTUR HILO ======================================================

    #MANUAL USER
    public function downloadManual()
    {
        $filename = 'file_manual-29012018.pdf';
        return redirect('upload/' . $filename);
    }
    #END MANUAL USER


    //START DETAIL INSTALASI UNTUK INTERNAL ===========================================
    public function internalDetailPembangkit($id)
    {
        $instalasi = InstalasiPembangkit::find($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_PEMBANGKIT);
        $permohonan = $instalasi->getRiwayat();

        return view('internal/instalasi/detail_instalasi_pembangkit', compact('instalasi', 'tipe_instalasi', 'permohonan'));
    }

    public function internalDetailTransmisi($id)
    {
        $instalasi = InstalasiTransmisi::find($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_TRANSMISI);
        $permohonan = $instalasi->getRiwayat();
        $bay = BayGardu::where('instalasi_id', $id)->get();

        return view('internal/instalasi/detail_instalasi_transmisi', compact('instalasi', 'tipe_instalasi', 'permohonan', 'bay'));

    }

    public function internalDetailDistribusi($id)
    {
        $instalasi = InstalasiDistribusi::find($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_DISTRIBUSI);
        $permohonan = $instalasi->getRiwayat();

        return view('internal/instalasi/detail_instalasi_distribusi', compact('instalasi', 'tipe_instalasi', 'permohonan'));
    }

    public function internalDetailPemanfaatanTm($id)
    {
        $instalasi = InstalasiPemanfaatanTM::find($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_PEMANFAATAN_TM);
        $permohonan = $instalasi->getRiwayat();

        return view('internal/instalasi/detail_instalasi_pemanfaatan_tm', compact('instalasi', 'tipe_instalasi', 'permohonan'));

    }

    public function internalDetailPemanfaatanTt($id)
    {
        $instalasi = InstalasiPemanfaatanTT::find($id);
        $tipe_instalasi = TipeInstalasi::findOrFail(ID_PEMANFAATAN_TT);
        $permohonan = $instalasi->getRiwayat();

        return view('internal/instalasi/detail_instalasi_pemanfaatan_tt', compact('instalasi', 'tipe_instalasi', 'permohonan'));

    }
    //END DETAIL INSTALASI UNTUK INTERNAL ===========================================

    //START INPUT BAY GARDU INDUK TRANSMISI===========================================
    public function getAllGarduByTransmisi($transmisi_id)
    {
        $all_bay = array();
        $all_bay[BAY_LINE] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_LINE)->get());
        $all_bay[BAY_KAPASITOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_KAPASITOR)->get());
        $all_bay[BAY_REAKTOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_REAKTOR)->get());
        $all_bay[BAY_TRANSFORMATOR] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_TRANSFORMATOR)->get());
        $all_bay[BAY_COUPLER] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_COUPLER)->get());
        $all_bay[PHB] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', PHB)->get());
        $all_bay[BAY_CUSTOM] = BayGardu::loadPemilik(BayGardu::where('instalasi_id', $transmisi_id)->where('jenis_bay', BAY_CUSTOM)->get());
        return $all_bay;
    }

    public function insertBayGardu($transmisi_id, $tipe)
    {
        Session::flash('jenis_bay', $tipe);
        return redirect('eksternal/edit_bay_gardu/' . $transmisi_id);
    }

    public function showBayGardu($transmisi_id)
    {
        $transmisi = InstalasiTransmisi::find($transmisi_id);
        $all_bay = $this->getAllGarduByTransmisi($transmisi_id);
        #hide PHB
        $all_bay = array_diff_key($all_bay, ["PHB" => "hide"]);
        #end hide PHB
        return view('eksternal/data_bay_instalasi_transmisi', compact('all_bay', 'transmisi'));

    }

    public function editBayGardu($transmisi_id, $id = 0)
    {
        $all_bay = $this->getAllGarduByTransmisi($transmisi_id);
        #hide PHB
        $all_bay = array_diff_key($all_bay, ["PHB" => "hide"]);
        #end hide PHB

        $bay = BayGardu::find($id);
        $transmisi = InstalasiTransmisi::find($transmisi_id);
        $ref_parent = References::where('nama_reference', 'Tegangan Pengenal')->first();
        $tegangan_pengenal = References::where('parent', $ref_parent->id)->get();

        $ref_parent = References::where('nama_reference', 'Jenis Ijin Usaha Pemilik Instalasi')->first();
        $jenis_ijin_usaha = References::where('parent', $ref_parent->id)->get();

        $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
        $pemilik = PemilikInstalasi::all();
        $jenis = @$bay->jenis_bay;
        if (session('jenis_bay')) {
            $jenis = session('jenis_bay');
        }
        return view('eksternal/create_bay_instalasi_transmisi', compact('jenis', 'id', 'bay', 'all_bay', 'transmisi', 'tegangan_pengenal', 'jenis_ijin_usaha', 'pemilik_instalasi', 'pemilik'));
    }

    public function storeBayGardu(Request $request)
    {
        $id = $request->id;
        $transmisi_id = $request->transmisi_id;
        $nama = $request->form_nama_bay;
        $bay = ($request->id > 0) ? BayGardu::find($request->id) : new BayGardu();
        $bay->nama_bay = $request->form_nama_bay;
        if ($request->id == 0) $bay->jenis_bay = $request->form_jenis_bay;
        $bay->tipe_pemilik = $request->form_tipe_pemilik;
        if ($request->form_tipe_pemilik == MILIK_SENDIRI) {
            $pemilik_instalasi = Auth::user()->perusahaan->pemilikInstalasi;
            $bay->pemilik_id = $pemilik_instalasi->id;
        } elseif ($request->form_tipe_pemilik == TERDAFTAR) {
            $bay->pemilik_id = $request->form_pemilik_instalasi_lain;
        }
        $bay->instalasi_id = $request->transmisi_id;
        $bay->kapasitas_pemutus = $request->form_kapasitas_pemutus;
        $bay->kapasitas_trafo = $request->form_kapasitas_trafo;
        $bay->tegangan_pengenal = $request->form_tegangan_pengenal;
        $bay->kode_kontraktor = @$request->kode_kontraktor;
        $bay->status_baru = @$request->status_baru;
        if (@$request->status_baru == 0) {
            $bay->status_slo = @$request->status_slo;
        } else {
            $bay->status_slo = '';
        }

        if ($bay->status_baru == 0) {
            $request->kode_kontraktor = '0000';
            $bay->kode_kontraktor = '0000';
        }
        if ($request->kode_kontraktor != '') {
            $kontraktor = Kontraktor::where('kode', $request->kode_kontraktor)->first();
            $bay->kontraktor = @$kontraktor->nama;
        } else {
            $bay->kontraktor = "";
        }

        $bay->save();
        if ($request->hasFile('file_sbujk')) {
            $filename = saveFile($request->file_sbujk, 'file_sbujk', $bay->id . "_" . str_replace(' ', '_', $request->form_nama_bay));
            $bay->file_sbujk = $filename;
        } else if ($bay->kode_kontraktor == '0000') {
            $fullPath = storage_path() . '/upload/file_sbujk/' . $bay->file_sbujk;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $bay->file_sbujk = "";
        }
        if ($request->hasFile('file_iujk')) {
            $filename = saveFile($request->file_iujk, 'file_iujk', $bay->id . "_" . str_replace(' ', '_', $request->form_nama_bay));
            $bay->file_iujk = $filename;
        } else if ($bay->kode_kontraktor == '0000') {
            $fullPath = storage_path() . '/upload/file_iujk/' . $bay->file_iujk;
            if (File::exists($fullPath))
                File::delete($fullPath);
            $bay->file_iujk = "";
        }

        if ($request->hasFile('file_sld')) {
            $filename = saveFile($request->file_sld, 'file_sld', $bay->id . "_" . str_replace(' ', '_', $request->form_nama_bay));
            $bay->file_sld = $filename;
        }

        if ($request->status_baru == '0') {
            if ($request->hasFile('file_sp')) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $nm_u_custom = '-' . $timestamp . '_bay' . $bay->id . '.';
                $bay->surat_pernyataan_status = ($request->hasFile('file_sp')) ? 'file_sp' . $nm_u_custom . $request->file_sp->getClientOriginalExtension() : "";
                $filename = $bay->surat_pernyataan_status;
                $fullPath = storage_path() . '/upload/file_sp/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $request->file_sp->move(storage_path() . '/upload/file_sp', 'file_sp' . $nm_u_custom . $request->file_sp->getClientOriginalExtension());
            }
        } else {
            if ($bay->kode_kontraktor == '0000') {
                if ($request->hasFile('file_sp')) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $nm_u_custom = '-' . $timestamp . '_bay' . $bay->id . '.';
                    $bay->surat_pernyataan_status = ($request->hasFile('file_sp')) ? 'file_sp' . $nm_u_custom . $request->file_sp->getClientOriginalExtension() : "";
                    $filename = $bay->surat_pernyataan_status;
                    $fullPath = storage_path() . '/upload/file_sp/' . $filename;
                    if (File::exists($fullPath))
                        File::delete($fullPath);
                    $request->file_sp->move(storage_path() . '/upload/file_sp', 'file_sp' . $nm_u_custom . $request->file_sp->getClientOriginalExtension());
                }
            } else {
                $filename = $bay->surat_pernyataan_status;
                $fullPath = storage_path() . '/upload/file_sp/' . $filename;
                if (File::exists($fullPath))
                    File::delete($fullPath);
                $bay->surat_pernyataan_status = '';
            }
        }

        $bay->save();
        Session::flash('success', 'Data Bay ' . $nama . ' berhasil disimpan!');
        return redirect('eksternal/detail_bay_gardu/' . $transmisi_id);
    }

    public function deleteBayGardu($id)
    {
        $bay = BayGardu::find($id);
        $nama = @$bay->nama_bay;
        $transmisi_id = @$bay->instalasi_id;
        if ($bay != null) $bay->delete();
        Session::flash('success', 'Data Bay ' . $nama . ' berhasil dihapus!');
        return redirect('eksternal/detail_bay_gardu/' . $transmisi_id);
    }
    //END INPUT BAY GARDU INDUK TRANSMISI===========================================
}
