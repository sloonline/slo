<?php

namespace App\Http\Controllers;

use App\BusinessArea;
use App\Cities;
use App\Bidang;
use App\CompanyCode;
use App\Jabatan;
use App\Notification;
use App\Penugasan;
use App\PenugasanUser;
use App\PesanNotifikasi;
use App\Role;
use App\SubBidang;
use App\StatusNotif;
use App\References;

use App\User;
use App\KategoriPerusahaan;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Perusahaan;
use App\Provinces;
use DateTime;
use Illuminate\Support\Facades\File;
use Mail;
use Hash;
use Illuminate\Support\Facades\Session;


class UserController extends Controller
{
    private $user_pj;
    private $perusahaan_pj;

    /*====function ajax, check email is already exist====*/
    public static function checkEmailAlreadyExisted($email_user)
    {
        $email = User::where('email', $email_user)->count();
        $username = User::where('username', $email_user)->count();
        if ($email == 0 && $username == 0) {
            echo "valid";
        } else {
            echo "invalid";
        }
    }

    public static function viewProfileuser($id)
    {
        $user = User::find($id);
        $province = Provinces::all();
        $city = Cities::all();
        if ($user->perusahaan_id != null) {
            $kategori = KategoriPerusahaan::find($user->perusahaan->kategori_perusahaan);
            //dd($user->file_foto_user);
        } else {
            $roles = Role::orderBy('id', 'asc')->get();
            $user_roles = ($id != null) ? UserRole::where('user_id', $id)->get() : array();
            $kategori = null;
        }

        //dd($user->perusahaan->company_code);
        if ($user->jenis_user == "INTERNAL") {
            return view('internal.registrasi.profil', compact('user', 'province', 'city', 'id', 'kategori', 'roles', 'user_roles'));
        } else {
            return view('eksternal/profil', compact('user', 'province', 'city', 'id', 'kategori', 'roles', 'user_roles'));
        }
    }
}
