<?php

namespace App\Http\Controllers;

use App\Kontraktor;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use PhpSpec\Exception\Example\ErrorException;

class KontraktorController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request $request
  * @param  int $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function updateFromDJK()
  {

    $url = "http://slo.djk.esdm.go.id/api/sbu.php";
    try {
      $json = json_decode(file_get_contents($url), true);
    } catch (\Exception $e) {
      return "Error! Failed to connect API DJK.<br>".$e->getMessage();
    }

    $jml_api = count($json);

    if ($jml_api > 0) {

      $jml_local = Kontraktor::all()->count();

      if ($jml_api > $jml_local) {
        Kontraktor::truncate();
        foreach ($json as $wa) {
          $kontraktor = new Kontraktor();
          $kontraktor->kode = $wa['kode'];
          $kontraktor->nama = strtoupper($wa['nama']);
          if ($wa['masaberlaku'] != '0000-00-00')
          $kontraktor->masa_berlaku = Carbon::parse($wa['masaberlaku']);
          $kontraktor->save();

          //                    echo $kontraktor->id . "-" . $kontraktor->kode . "-" . $kontraktor->nama . "-" . $kontraktor->masa_berlaku . "<br>";
        }
        return "Kontraktor updated at " . Carbon::now();
      } else
      return "Kontraktor is up-to-date";
    }
  }
  public function searchKontraktor($key){
    $data = Kontraktor::where("CONCAT(LOWER(kode),CONCAT(' - ',LOWER(nama)))",'like','%'.strtolower($key).'%')->skip(0)->take(10)->get(['nama','kode'])->toArray();
    $result = array("kontraktor" => $data);
    return json_encode($result);
  }
}
