<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaInspeksi extends Model
{
    protected $table = 'ba_inspeksi';

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }
}
