<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Lhpp extends Model
{
    protected $table = 'Lhpp';
    use SoftDeletes;

    public function lhpp_iuptl()
    {
        return $this->hasOne('App\LhppIuptl');
    }

    public function lhpp_pelaksana()
    {
        return $this->hasMany('App\LhppPelaksana');
    }

    public function instalasi()
    {
        $tipe_instalasi_id = $this->tipe_instalasi;
        $instalasi_id = $this->instalasi_id;
        $bay_gardu_id = $this->bay_gardu_id;

        if ($tipe_instalasi_id == ID_PEMBANGKIT) {
            return $this->belongsTo('App\InstalasiPembangkit', 'instalasi_id', 'id');
        } else if ($tipe_instalasi_id == ID_TRANSMISI) {
            return $this->belongsTo('App\InstalasiTransmisi', 'instalasi_id', 'id');
        } else if ($tipe_instalasi_id == ID_DISTRIBUSI) {
            return $this->belongsTo('App\InstalasiDistribusi', 'instalasi_id', 'id');
        } else if ($tipe_instalasi_id == ID_PEMANFAATAN_TT) {
            return $this->belongsTo('App\InstalasiPemanfaatanTT', 'instalasi_id', 'id');
        } else if ($tipe_instalasi_id == ID_PEMANFAATAN_TM) {
            return $this->belongsTo('App\InstalasiPemanfaatanTM', 'instalasi_id', 'id');
        }else{
            $this->instalasi_id = -1;
            return $this->belongsTo('App\InstalasiPembangkit', 'instalasi_id', 'id');
        }
    }

    public function tipeInstalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi', 'id');
    }

    public function ketua_pelaksana()
    {
        $tmp = LhppPelaksana::where('lhpp_id', $this->id)
            ->where('tipe_pelaksana', ID_KETUA_PELAKSANA)
            ->first();
        return $tmp;
    }

    public function anggota()
    {
        $tmp = LhppPelaksana::where('lhpp_id', $this->id)
            ->where('tipe_pelaksana', ID_ANGGOTA)
            ->first();
        return $tmp;
    }

    public function pjt()
    {
        $tmp = LhppPelaksana::where('lhpp_id', $this->id)
            ->where('tipe_pelaksana', ID_PJT)
            ->first();
        return $tmp;
    }

    public function kont_pemasangan()
    {
        $tmp = LhppIuptl::where('lhpp_id', $this->id)
            ->where('tipe_iuptl', ID_KONT_PEMASANGAN)
            ->first();
        return $tmp;
    }

    public function kons_perencana()
    {
        $tmp = LhppIuptl::where('lhpp_id', $this->id)
            ->where('tipe_iuptl', ID_KONS_PERENCANA)
            ->first();
        return $tmp;
    }

    public function lhpp_kit()
    {
        return $this->hasOne('App\LhppKit');
    }

    public function pjt_djk()
    {
        return $this->belongsTo('App\PjtDjk', 'nik_pjt', 'id');
    }

    public function tt_djk()
    {
        return $this->belongsTo('App\TtDjk', 'nik_tt', 'id');
    }

    public function gardu()
    {
        return $this->belongsTo('App\BayGardu', 'bay_gardu_id', 'id');
    }

    static function lhpp_baru()
    {
        $lhpp = Lhpp::where('status_permohonan_djk', DJK_NOT_SENT)->get();
        return $lhpp;
    }

    static function lhpp_verifikasi()
    {
        $lhpp = Lhpp::where('status_permohonan_djk', DJK_SENT)->get();
        return $lhpp;
    }

    static function lhpp_diterima()
    {
        $lhpp = Lhpp::where('status_permohonan_djk', DJK_APPROVED)->get();
        return $lhpp;
    }

    static function lhpp_ditolak()
    {
        $lhpp = Lhpp::where('status_permohonan_djk', DJK_REJECTED)->get();
        return $lhpp;
    }

    public function log_hasil()
    {
//        return  $tmp = DB::select('select * from (select * from djk_hasil where djk_hasil.no_permohonan = '.$this->no_djk_permohonan.' order by created_at) where rownum = 1');
        return $this->hasMany('App\DjkHasil', 'no_permohonan', 'no_djk_permohonan')->orderBy('created_at');
    }

    public function djk_hasil()
    {
        return $tmp = DjkHasil::select('*')
            ->where('no_permohonan', $this->no_djk_permohonan)
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function agenda()
    {
        return $this->belongsTo('App\DjkAgenda', 'no_djk_agenda', 'no_agenda');
    }

    public function djk_registrasi()
    {
        return $this->belongsTo('App\DjkRegistrasi', 'no_djk_permohonan', 'no_permohonan');
    }


    public static function getLatest($total)
    {
        $data = Lhpp::orderBy('updated_at', 'desc')->take($total)->get();

        return $data;
    }


    public static function getByPemintaJasa()
    {
        $data = Lhpp::whereHas('instalasi', function ($query) {
            $query->whereHas('user', function ($query2) {
                $query2->where('username', Auth::user()->username);
            });
        });

        return ($data == null) ? 0 : $data->count();

    }

}
