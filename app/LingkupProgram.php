<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LingkupProgram extends Model
{
    //
    use SoftDeletes;
    protected $table = 'lingkup_program';

    public function garduArea(){
        return $this->belongsTo('App\GarduArea', 'id_gardu_area', 'id');
    }

    public function lokasiArea(){
        return $this->belongsTo('App\LokasiArea', 'id_lokasi_area', 'id');
    }

    public function penyulangArea(){
        return $this->belongsTo('App\PenyulangArea', 'id_penyulang_area', 'id');
    }

    public function lingkupPekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan', 'id_lingkup_pekerjaan', 'id');
    }
}
