<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PenyulangArea extends Model
{
    //
    use SoftDeletes;
    protected $table = 'penyulang_area';

    public function penyulang(){
        return $this->belongsTo('App\Penyulang', 'id_penyulang', 'id');
    }

    public function areaProgram(){
        return $this->belongsTo('App\AreaProgram','id_area_program','id');
    }

    public function instalasi(){
        return $this->hasMany('App\InstalasiDistribusi','id_penyulang_area','id');
    }



}
