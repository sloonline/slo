<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    //
    use SoftDeletes;
    protected $table = 'program';

    public function suratTugas()
    {
        return $this->belongsTo('App\suratTugas', 'id_surat_tugas', 'id');
    }

    public function jenisProgram()
    {
        return $this->belongsTo('App\jenisProgram', 'id_jenis_program', 'id');
    }

    public function areaProgram()
    {
        return $this->hasMany('App\AreaProgram', 'id_program', 'id');
    }

    public function kontrakProgram()
    {
        return $this->hasMany('App\KontrakProgram', 'id_program', 'id');
    }

    public function firstChild()
    {
        $acuan = $this->jenisProgram->acuan_1;
        $data = array();
        switch ($acuan) {
            case TIPE_AREA:
                foreach ($this->areaProgram as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->businessArea->description;
                    $row->obj = $item;
                    array_push($data, $row);
                }
                break;
            case TIPE_KONTRAK:
                foreach ($this->kontrakProgram as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->nomor_kontrak;
                    $row->obj = $item;
                    array_push($data, $row);
                }
                break;

        }
        return $data;
    }


    //parent adalah object diatasnya
    public function secondChild($parent)
    {
        $jenisProgram = $this->jenisProgram;
        $data = $parent->child($jenisProgram->acuan_2);
        return $data;
    }

    public function instalasi(){
        return $this->hasMany('App\InstalasiDistribusi', 'id_program', 'id');
    }

    public function perusahaan(){
        return $this->belongsTo('App\Perusahaan','id_perusahaan','id');
    }
}
