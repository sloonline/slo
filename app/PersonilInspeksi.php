<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PekerjaanPersonil;
use App\Pekerjaan;

class PersonilInspeksi extends Model
{
  protected $table = "personil_inspeksi";

  public function jabatan_inspeksi(){
    return $this->belongsTo('App\JabatanInspeksi','id_jabatan_inspeksi','id');
  }

  public function status_pekerja(){
    return $this->belongsTo('App\StatusPekerja','id_status_pekerja','id');
  }

  public function bidang(){
    return $this->belongsTo('App\Bidang','id_bidang','id');
  }

  public function sub_bidang(){
    return $this->belongsTo('App\SubBidang','id_sub_bidang','id');
  }

  public function training(){
    return $this->hasMany('App\PersonilTraining','personil_id','id');
  }

  public function keahlian(){
    return $this->hasMany('App\KeahlianPersonil','personil_id','id');
  }

  public function user(){
    return $this->belongsTo('App\User','id_user','id');
  }

  public function jabatan(){
    return  $this->belongsToMany('App\JabatanInspeksi', 'jabatan_personil', 'personil_inspeksi_id', 'jabatan_inspeksi_id')->withTimestamps();
  }

  public function pekerjaan_personil(){
    return $this->hasMany('App\PekerjaanPersonil', 'personil_id', 'id');
  }

  public function isBooked($id, $tgl_pekerjaan){
    /*
    1. cek di table mapping PekerjaanPersonil
    2. jika ada, check tanggal dari kontraknya, apakah tanggal pekerjaan
    yang akan dilaksanakan di antara tanggal jadwal pdkontrak yang ada
    3. return true jika booked, false jika ternyata available
    */
    $tmp_tanggal = $tgl_pekerjaan;//'17-06-2017';
    $isBooked = false;
    $mp_pekerjaan_personil = PekerjaanPersonil::where('personil_id', $id)->get();
    if(sizeOf($mp_pekerjaan_personil)>0){
      foreach($mp_pekerjaan_personil as $item){
        $tgl_mulai = @$item->pekerjaan->wbs_io->kontrak->jadwal->tanggal_mulai;
        $tgl_selesai = @$item->pekerjaan->wbs_io->kontrak->jadwal->tanggal_selesai;
        $isBooked = self::check_in_range($tgl_mulai,$tgl_selesai,$tmp_tanggal);
        if($isBooked){
          $pekerjaan = Pekerjaan::find($item->pekerjaan_id);
          if($pekerjaan->status_pekerjaan == PEKERJAAN_CLOSED){
            $isBooked = false;
          }
        }
      }
    }
    // dd($isBooked);
    return $isBooked;
  }

  public static function check_in_range($start_date, $end_date, $date_from_user)
  {
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = strtotime($date_from_user);

    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
  }


  public function jabatan_personil(){
    return $this->hasMany('App\JabatanPersonil','personil_inspeksi_id','id');
  }

  public function personil_bidang(){
    return $this->hasMany('App\PersonilBidang','personil_inspeksi_id','id');
  }

  public function lingkup_personil(){
    return $this->hasMany('App\LingkupPersonil','personil_inspeksi_id','id');
  }

}
