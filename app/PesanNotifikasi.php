<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesanNotifikasi extends Model
{
    protected $table = 'pesan_notifikasi';

    public function status_notif(){
        return $this->belongsTo("App/StatusNotif");
    }
}
