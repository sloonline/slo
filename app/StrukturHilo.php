<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StrukturHilo extends Model
{
  protected $table = "struktur_hilo";
  use SoftDeletes;

  public function pekerjaan(){
    return $this->belongsTo('App\Pekerjaan','pekerjaan_id', 'id');
  }

  public function lingkup_pekerjaan(){
    return $this->belongsTo('App\LingkupPekerjaan','lingkup_pekerjaan_id', 'id');
  }

  public static function getByLingkup($lingkup_pekerjaan_id){
    return StrukturHilo::where('lingkup_pekerjaan_id',$lingkup_pekerjaan_id)->orderBy('sort_num')->orderBy('nomor')->orderBy('id')->get();
  }
}
