<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontraktor extends Model
{
    //
    protected $table = "kontraktor";
    protected $dates = ['masa_berlaku'];

    public function instalasiPembangkit(){
        return $this->hasMany('App\InstalasiPembangkit', 'kode_kontraktor', 'kode');
    }
    public function instalasiTransmisi(){
        return $this->hasMany('App\InstalasiTransmisi', 'kode_kontraktor', 'kode');
    }
    public function instalasiDistribusi(){
        return $this->hasMany('App\InstalasiDistribusi', 'kode_kontraktor', 'kode');
    }
    public function instalasiPemanfaatanTT(){
        return $this->hasMany('App\InstalasiPemanfaatanTT', 'kode_kontraktor', 'kode');
    }
    public function instalasiPemanfaatanTM(){
        return $this->hasMany('App\InstalasiPemanfaatanTM', 'kode_kontraktor', 'kode');
    }
    public function bayGardu(){
        return $this->hasMany('App\BayGardu', 'kode_kontraktor', 'kode');
    }
}
