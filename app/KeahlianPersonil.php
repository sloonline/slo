<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeahlianPersonil extends Model
{
    protected $table = "keahlian_personil";

    public function keahlian(){
        return $this->belongsTo('App\Keahlian','keahlian_id','id');
    }
    
    public function personil(){
        return $this->belongsTo('App\PersonilInspeksi','personil_id','id');
    }
}
