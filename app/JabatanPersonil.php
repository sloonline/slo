<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanPersonil extends Model
{//
  protected $table = "jabatan_personil";
  protected $primaryKey = null;
  public $incrementing = false;

  public function personil(){
    return  $this->belongsToMany('App\PersonilInspeksi', 'jabatan_personil', 'jabatan_inspeksi_id', 'personil_inspeksi_id')->withTimestamps();
  }

  public function jabatan(){
    return $this->belongsTo('App\JabatanInspeksi','jabatan_inspeksi_id','id');
  }
}
