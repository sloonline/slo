<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenugasanUser extends Model
{
    //
    protected $table = 'penugasan_user';
    protected $primaryKey = null;
    public $incrementing = false;

    public function penugasan(){
        return $this->belongsTo('App\Penugasan','penugasan_id', 'id');
    }
}