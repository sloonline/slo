<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonilBidang extends Model
{
  protected $table="personil_bidang";

  public function personil(){
      return $this->belongsTo('App\PersonilInspeksi', 'personil_inspeksi_id', 'id');
  }

  public function bidang(){
      return $this->belongsTo('App\Bidang', 'bidang_id', 'id');
  }

  public function sub_bidang(){
      return $this->belongsTo('App\SubBidang', 'sub_bidang_id', 'id');
  }
}
