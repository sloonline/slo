<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peralatan extends Model
{
  protected $table = 'peralatan';

  public function bidang(){
    return $this->belongsTo('App\Bidang', 'bidang_id', 'id');
  }

  public function subBidang(){
    return $this->belongsTo('App\SubBidang', 'sub_bidang_id', 'id');
  }

  public function kategori(){
    return $this->belongsTo('App\References','kategori_alat','id');
  }

  public function status(){
    return $this->belongsTo('App\References','kondisi_alat','id');
  }

  public function lokasi(){
    return $this->belongsTo('App\References','lokasi_alat','id');
  }

  public static function getAvailable(){
    return Peralatan::whereHas('status', function ($query) {
        $query->where('nama_reference', PERALATAN_NORMAL);
    })->get();
  }

  public static function getOptionStatusForInspeksi($current){
    //untuk menentukan pilihan status yang mungkin dipilih sesuai keadaan saat ini
    $status = array();
    switch($current){
      case PERALATAN_BOOKED:
        $status = array($current,PERALATAN_DIPAKAI);
      break;
        case PERALATAN_DIPAKAI:
          $status = array($current,PERALATAN_NORMAL,PERALATAN_RUSAK);
        break;
    }
    $ref_status = References::where('nama_reference', 'Kondisi Alat')->first()->id;
    return References::getRefByArrNameAndParent($ref_status,$status);
  }
}
