<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonilTraining extends Model
{
    protected $table = "personil_training";
    
    public function personil(){
        return $this->belongsTo('App\Personil','personil_id','id');
    }

    public function training(){
        return $this->belongsTo('App\Training','training_id','id');
    }
}
