<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanPekerjaan extends Model
{
    protected $table="laporan_pekerjaan";

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }
}
