<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayPermohonan extends Model
{
    protected $table = 'bay_permohonan';

    public function bayGardu()
    {
        return $this->belongsTo('App\BayGardu', 'bay_id', 'id')->withTrashed();
    }


    public function permohonan()
    {
        return $this->belongsTo('App\Permohonan', 'permohonan_id', 'id');
    }

}
