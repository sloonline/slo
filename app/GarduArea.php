<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GarduArea extends Model
{
    //
    protected $table = 'gardu_area';

    public function garduInduk(){
        return $this->belongsTo('App\GarduInduk', 'id_gardu_induk', 'id');
    }

    public function areaProgram(){
        return $this->belongsTo('App\AreaProgram', 'id_area_program', 'id');
    }

    public function instalasi(){
        return $this->hasMany('App\InstalasiDistribusi','id_gardu_area','id');
    }


    use SoftDeletes;
}
