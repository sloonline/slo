<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    //
    protected $table = 'role_user';
    protected $primaryKey = null;
    public $incrementing = false;


    public function roles()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    public function penugasan()
    {
        return $this->belongsTo('App\Penugasan', 'role_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}