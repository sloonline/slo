<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerusahaanSpv extends Model
{
    protected $table='perusahaan_spv';

    public function spv()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'perusahaan_id', 'id');
    }
}
