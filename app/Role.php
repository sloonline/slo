<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $table = 'roles';
    use SoftDeletes;

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function getLastID(){
        $lastid = $this->all()->sortByDesc('id')->first();
        if($lastid==null) $id = 1;
        else $id = $lastid->id + 1;

        return $id;
    }

    public function workflow(){
        return $this->hasMany('app\Workflow', 'flow_status_id', 'id');
    }

    public function permissionRole(){
        return $this->hasMany('App\PermissionRole', 'role_id', 'id');
    }
}
