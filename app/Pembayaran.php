<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';

    public function kontrak(){
        return $this->belongsTo('App\Kontrak','id_kontrak','id');
    }
}
