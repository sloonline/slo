<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rls extends Model
{
    protected $table = 'rls';

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function user(){
        return  $this->belongsTo('App\User', 'manager_bki', 'id');
    }

    public static function getSPV($rls)
    {
        $user = $rls->pekerjaan->permohonan->user;
        return User::getSpv($user);
    }
}
