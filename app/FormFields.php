<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFields extends Model
{

    protected $table = "dokumen_permen";
    protected $primaryKey = "dokumen_id";


    //status_instalasi lama(0) atau baru (1)
    public function form($id, $status_instalasi = 1)
    {
        $data = FormFields::where('id_jenis',$id);
        if($status_instalasi == 0)  $data->where('status','ALL');
        return $data->get();
    }
}
