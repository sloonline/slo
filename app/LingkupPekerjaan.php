<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkupPekerjaan extends Model
{
    protected $table = 'lingkup_pekerjaan';

    protected $fillable = [
                    'jenis_lingkup_pekerjaan',

    ];

    public function tipe_instalasi(){
    	return $this->belongsTo('App\TipeInstalasi');
    }

    public function instalasiDistribusi(){
        return $this->hasMany('App\LingkupDistribusi','lingkup_id','id');
    }

    public function struktur_hilo(){
      return $this->hasOne('App\StrukturHilo','lingkup_pekerjaan_id','id');
    }

    public function template(){
        return $this->hasMany('App\LingkupPekerjaanTemplate','id_lingkup_pekerjaan','id');
    }

}
