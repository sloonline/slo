<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\JabatanInspeksi;

class JabatanInspeksi extends Model
{
    //
    protected $table = "jabatan_inspeksi";

    public function personil(){
        return  $this->belongsToMany('App\PersonilInspeksi', 'jabatan_personil', 'jabatan_inspeksi_id', 'personil_inspeksi_id')->withTimestamps();
    }

    // public static function availablePersonil(){
    //     $jabatan = JabatanInspeksi::all();
    //     foreach($jabatan as $item){
    //         $item->personil;
    //         dd($item->personil[0]->pekerjaan_personil[0]->pekerjaan->wbs_io->kontrak->jadwal);
    //     }
    // }
}
