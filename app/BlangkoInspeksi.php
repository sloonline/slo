<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlangkoInspeksi extends Model
{
    protected $table='blangko_inspeksi';
    use SoftDeletes;

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }
}
