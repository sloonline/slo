<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Peralatan;

class PermohonanPeralatan extends Model
{
    protected $table="permohonan_peralatan";

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function flow_status()
    {
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public function pekerjaan_peralatan(){
        return $this->hasMany('App\PekerjaanPeralatan','permohonan_peralatan_id','id');
    }

    public function id_pekerjaan_peralatan(){
        return $this->hasMany('App\PekerjaanPeralatan','permohonan_peralatan_id','id')->select(['peralatan_id']);
    }


    public static function isAllowEdit($permohonan)
    {
        $latest_step = WorkflowController::getLatestHistory($permohonan->id, $permohonan->getTable());
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        if ($next == null) {
            return false;
        } else {
            $submit = $next->where('tipe_status', SUBMITTED)->first();
            $created = $next->where('tipe_status', CREATED)->first();
            return (in_array(array('role_id' => @$submit->role_id), Auth::user()->user_roles_id->toArray()) ||
                in_array(array('role_id' => @$created->role_id), Auth::user()->user_roles_id->toArray()));
        }
    }

    public static function isAllowManagePeralatan($permohonan)
    {
        $latest_step = WorkflowController::getLatestHistory($permohonan->id, $permohonan->getTable());
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        //jika workflow sudah selesai maka bisa manage peralatan (set jadi use atau dikembalikan dll)
        return ($latest_step != null && $next->count() == 0) ? true : false;
    }

    public static function peralatan($permohonan){
      $pekerjaan_peralatan = $permohonan->pekerjaan_peralatan;
      $peralatan_id = $pekerjaan_peralatan->pluck('peralatan_id')->toArray();
      $peralatan = Peralatan::whereIn('id',$peralatan_id)->get();
      return $peralatan;
    }
}
