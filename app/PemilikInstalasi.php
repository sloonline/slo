<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PemilikInstalasi extends Model
{
    //
    use SoftDeletes;
    protected $table = 'pemilik_instalasi';
    protected $dates = ['masa_berlaku_iu', 'tgl_pengesahan_kontrak', 'masa_berlaku_kontrak', 'tgl_spjbtl',
                        'masa_berlaku_spjbtl'];

    public function instalasiPembangkit()
    {
        return $this->hasMany('App\instalasiPembangkit', 'pemilik_instalasi_id', 'id');
    }

    public function instalasiTransmisi()
    {
        return $this->hasMany('App\instalasiTransmisi', 'pemilik_instalasi_id', 'id');
    }

    public function instalasiPemanfaatanTT()
    {
        return $this->hasMany('App\instalasiPemanfaatanTT', 'pemilik_instalasi_id', 'id');
    }

    public function instalasiPemanfaatanTM()
    {
        return $this->hasMany('App\instalasiPemanfaatanTM', 'pemilik_instalasi_id', 'id');
    }

    public function getInstalasi($tipe_instalasi)
    {
        $instalasi = array();
        switch ($tipe_instalasi) {
            case 1:
                return $this->instalasiPembangkit()->get();
                break;
            case 2:
                return $this->instalasiTransmisi()->get();
                break;
            case 4:
                return $this->instalasiPemanfaatanTT()->get();
                break;
            case 5:
                return $this->instalasiPemanfaatanTM()->get();
                break;
            default:
                return array();
            break;
        }
        return $instalasi;
    }
    
    public function jenisIjinUsaha(){
        return $this->belongsTo('App\References','jenis_ijin_usaha','id');
    }

    public function city(){
        return $this->belongsTo('App\Cities','id_city','id');
    }

    public function province(){
        return $this->belongsTo('App\Provinces','id_province','id');
    }

    public function perusahaan(){
        return $this->belongsTo('App\Perusahaan', 'perusahaan_id', 'id');
    }
}
