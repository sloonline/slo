<?php

namespace App;

class Date
{
    private $tgl;

    public function __construct($tgl){
        $this->$tgl = $tgl;
    }

    public static function tgl_indonesia($tgl) {
        $BulanIndo = array("Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli", "Agustus", "September",
            "Oktober", "November", "Desember");

        $tahun = substr($tgl, 0, 4);
        $bulan = substr($tgl, 5, 2);
        $date = substr($tgl, 8, 2);

        $result = $date . " " . $BulanIndo[(int) $bulan - 1] . " " . $tahun;
        return($result);
    }

}
