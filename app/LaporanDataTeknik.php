<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanDataTeknik extends Model
{
    protected $table='laporan_data_teknik';
    use SoftDeletes;

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }
    
    public function lingkup_pekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan','lingkup_pekerjaan_id', 'id');
    }
}
