<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KombinasiPekerjaan extends Model
{
    protected $table = 'kombinasi_pekerjaan';

    public function jenis_pekerjaan(){
    	return $this->belongsTo('App\JenisPekerjaan');
    }
}
