<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\WorkflowController;

class PermohonanRls extends Model
{
    protected $table = 'permohonan_rls';

    public function pekerjaan(){
      return $this->belongsTo('App\Pekerjaan','pekerjaan_id', 'id');
    }

    public function flow_status(){
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public static function updateSysStatus($parent)
    {
        //        $obj_model = $parent->kontrak;
        //        $obj_model->sys_status = $parent->sys_status;
        //        $obj_model->save();
        //
        //        return $obj_model;
        return $parent;
    }

    public function rls(){
        return $this->belongsTo('App\rls', 'rls_id', 'id');
    }

    public static function isAllowrls($permohonan_rls){
        $next = null;
        $latest_step = WorkflowController::getLatestHistory($permohonan_rls->id, $permohonan_rls->getTable());
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        // dd($permohonan_rls->getTable());
        if(count($next) > 0){
            return false;
        }else{
            return true;
        }
    }
    
    public function dm(){
        return $this->belongsTo('App\User','deputi_manager','id');
    }

    public function mb(){
        return $this->belongsTo('App\User','manager','id');
    }

    public static function getSPV($permohonan_rls)
    {
        $user = $permohonan_rls->pekerjaan->permohonan->user;
        return User::getSpv($user);
    }
}
