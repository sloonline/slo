<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\WorkflowController;

class PermohonanRlb extends Model
{
    protected $table = 'permohonan_rlb';

    public function pekerjaan(){
      return $this->belongsTo('App\Pekerjaan','pekerjaan_id', 'id');
    }

    public function flow_status(){
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public static function updateSysStatus($parent)
    {
        //        $obj_model = $parent->kontrak;
        //        $obj_model->sys_status = $parent->sys_status;
        //        $obj_model->save();
        //
        //        return $obj_model;
        return $parent;
    }

    public function rlb(){
        return $this->belongsTo('App\Rlb', 'rlb_id', 'id');
    }

    public static function isAllowRlb($permohonan_rlb){
        $next = null;
        $latest_step = WorkflowController::getLatestHistory($permohonan_rlb->id, $permohonan_rlb->getTable());
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        // dd($permohonan_rlb->getTable());
        if(count($next) > 0){
            return false;
        }else{
            return true;
        }
    }

    public function dm(){
        return $this->belongsTo('App\User','deputi_manager','id');
    }

    public function mb(){
        return $this->belongsTo('App\User','manager','id');
    }

    public static function getSPV($permohonan_rlb)
    {
        $user = $permohonan_rlb->pekerjaan->permohonan->user;
        return User::getSpv($user);
    }

}
