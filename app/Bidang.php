<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidang extends Model
{
    //
    use SoftDeletes;
    protected $table = 'bidang';

    public function sub_bidang(){
        return $this->hasMany('App\SubBidang', 'id_bidang', 'id');
    }
}
