<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Kontrak extends Model
{
    //
    use SoftDeletes;

    protected $table = "kontrak";

    public function kontrak_version()
    {
        return $this->hasMany('App\KontrakVersion', 'id_kontrak', 'id');
    }

    public function latest_kontrak()
    {
        return $this->belongsTo('App\KontrakVersion', 'id_kontrak_version', 'id');
    }

    public function flow_status()
    {
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public function jadwal()
    {
        return $this->hasOne('App\JadwalPelaksanaan', 'kontrak_id', 'id');
    }

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'perusahaan_id', 'id');
    }

    public static function getKontrakPLN()
    {
        $query = DB::table('kontrak k')
            ->join('kontrak_version_rab kvr', 'kvr.id_kontrak_version', '=', 'k.id_kontrak_version')
            ->join('perusahaan p', 'p.id', '=', 'k.perusahaan_id')
            ->join('kategori_perusahaan kp', 'kp.id', '=', 'p.kategori_perusahaan')
            ->where('kp.nama_kategori', PLN)
            ->where('k.status', 'SCHEDULED')
            ->select('k.id,k.id_kontrak_version,k.id_flow_status', 'k.perusahaan_id')
            ->groupby('k.id,k.id_kontrak_version,k.id_flow_status', 'k.perusahaan_id')
            ->get();
        $kontrak = Kontrak::hydrate($query);
        return $kontrak;

    }

    public static function getKontrakSwasta()
    {
        $query = DB::table('kontrak k')
            ->join('kontrak_version_rab kvr', 'kvr.id_kontrak_version', '=', 'k.id_kontrak_version')
            ->join('perusahaan p', 'p.id', '=', 'k.perusahaan_id')
            ->join('kategori_perusahaan kp', 'kp.id', '=', 'p.kategori_perusahaan')
            ->where('kp.nama_kategori', '!=', PLN)
            ->where('k.status', 'SCHEDULED')
            ->select('k.id,k.id_kontrak_version,k.id_flow_status', 'k.perusahaan_id')
            ->groupby('k.id,k.id_kontrak_version,k.id_flow_status', 'k.perusahaan_id')
            ->get();
        $kontrak = Kontrak::hydrate($query);
        return $kontrak;
    }

    public static function isAllowEdit($kontrak)
    {
        $latest_step = WorkflowController::getLatestHistory(@$kontrak->id, @$kontrak->getTable());
        $next = WorkflowController::getAllNextStep($latest_step->workflow_id)->where('modul', MODUL_KONTRAK);
        // dd($next);
        if ($next == null) {
            return false;
        } else {
            $submit = $next->where('tipe_status', SUBMITTED)->first();
            // if($submit == null){
            //     $submit = $next->where('tipe_status', REJECTED)->first();
            // }
            // dd($submit);
            $roles = Auth::user()->user_roles_id->toArray();
            return (in_array(array('role_id' => @$submit->role_id), $roles));
        }
    }

    public static function updateSysStatus($parent)
    {
        // dd($parent->kontrak);
        $obj_model = null;
        if ($parent->wbs_io != null) {
            $obj_model = $parent->wbs_io->kontrak;
            $obj_model->sys_status = $parent->sys_status;
            $obj_model->save();
        } else {
            $obj_model = $parent->kontrak;
            $obj_model->sys_status = $parent->sys_status;
            $obj_model->save();
        }

        return $obj_model;
    }

    public function pembayaran()
    {
        return $this->hasMany('App\Pembayaran', 'id_kontrak', 'id')->orderBy('termin_ke');
    }


    public function pembayaranPaid()
    {
        return $this->hasMany('App\Pembayaran', 'id_kontrak', 'id')->where('status', PAID);
    }

    public static function getUser($kontrak)
    {
        $user = $kontrak->latest_kontrak->kontrak_version_rab[0]->rab->order->user;
        return $user;
    }

    public static function getSPV($kontrak)
    {
        //        $business_area = $kontrak->latest_kontrak->kontrak_version_rab[0]->rab->order->user->perusahaan->id_business_area;
        //        $spv = businessAreaUser::where('business_area_id', $business_area)->first();

        $user = $kontrak->latest_kontrak->kontrak_version_rab[0]->rab->order->user;
        return User::getSpv($user);
    }

    //cek apakah RAB dipakai di kontrak apa engga
    public static function getKontrakByRab($rab)
    {
        $kontrak = Kontrak::whereHas('latest_kontrak', function ($query) use ($rab) {
            $query->whereHas('kontrak_version_rab', function ($sub) use ($rab) {
                $sub->where('id_rab', $rab->id);
            });
        })->get();
        return $kontrak;
    }

    public static function getKontrakByOrder($order)
    {
        $rab_id = $order->rab->pluck('id')->all();
        $kontrak = Kontrak::whereHas('latest_kontrak', function ($query) use ($rab_id) {
            $query->whereHas('kontrak_version_rab', function ($sub) use ($rab_id) {
                $sub->whereIn('id_rab', $rab_id);
            });
        })->get();
        return $kontrak;
    }

    public static function getRabUnusedInKontrak($order)
    {
        $unused = array();
        foreach ($order->rab as $rab) {
            $kontrak = self::getKontrakByRab($rab);
            if (sizeof($kontrak) == 0) {
                array_push($unused, $rab);
            }
        }
        return $unused;
    }

    public static function getNamaInstalasiByKontrak($kontrak)
    {
        $nama_instalasi = array();
        $kontrakRab = $kontrak->latest_kontrak->kontrak_version_rab;
        foreach ($kontrakRab as $row) {
            $rab = $row->rab;
            $permohonan = $rab->permohonan;
            foreach ($permohonan as $data) {
                array_push($nama_instalasi, @$data->permohonan->instalasi->nama_instalasi);
            }
        }
        return collect($nama_instalasi)->unique();
    }

    public static function getNamaInstalasiDanLingkupByKontrak($kontrak_version_rab)
    {
        $nama_instalasi = array();
//        $kontrakRab = $kontrak->latest_kontrak->kontrak_version_rab;
//        dd($kontrakRab);
//        foreach ($kontrakRab as $row) {
        $rab = $kontrak_version_rab->rab;
        $permohonan = $rab->permohonan;
        foreach ($permohonan as $data) {
            if (@$data->permohonan->instalasi->jenis_instalasi->keterangan == JENIS_GARDU) {
                $bay = Permohonan::getBay(@$data->permohonan);
                array_push($nama_instalasi, array(
                    "instalasi" => @$bay->nama_bay,
                    "lingkup" => @$data->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan,
                    "layanan" => @$data->permohonan->produk->produk_layanan
                ));

            } else {
                array_push($nama_instalasi, array(
                    "instalasi" => @$data->permohonan->instalasi->nama_instalasi,
                    "lingkup" => @$data->permohonan->lingkup_pekerjaan->jenis_lingkup_pekerjaan,
                    "layanan" => @$data->permohonan->produk->produk_layanan
                ));
            }
        }
//        }
        return collect($nama_instalasi)->unique();
    }

    public static function getInstalasi($kontrak)
    {
        $instalalsi = array();
        $kontrakRab = $kontrak->latest_kontrak->kontrak_version_rab;
        foreach ($kontrakRab as $row) {
            $rab = $row->rab;
            $permohonan = $rab->permohonan;
            foreach ($permohonan as $data) {
                array_push($instalalsi, @$data->permohonan->instalasi);
            }
        }
        return collect($instalalsi)->unique();
    }

}
