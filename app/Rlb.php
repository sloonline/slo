<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rlb extends Model
{
    protected $table = 'rlb';

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function user(){
        return  $this->belongsTo('App\User', 'manager_bki', 'id');
    }

    public static function getSPV($rlb)
    {
        $user = $rlb->pekerjaan->permohonan->user;
        return User::getSpv($user);
    }
}
