<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LaporanKickOff extends Model
{
  protected $table = "laporan_kick_off";
  use SoftDeletes;


  public function pekerjaan(){
    return $this->belongsTo('App\Pekerjaan','pekerjaan_id', 'id');
  }
}
