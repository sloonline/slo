<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    protected $table = 'perusahaan';

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function company_code(){
        return $this->belongsTo('App\CompanyCode','id_company_code', 'id');
    }

    public function kategori(){
        return $this->belongsTo('App\KategoriPerusahaan','kategori_perusahaan', 'id');
    }


    public function business_area(){
        return $this->belongsTo('App\BusinessArea','id_business_area','id');
    }


    // public function business_area_code(){
    //     return $this->belongsTo('App\BusinessArea','id_business_area','business_area');
    // }

    public function business_area_user(){
        return  $this->hasOne('App\businessAreaUser', 'business_area_id',  'id');
    }

    public function city(){
        return $this->belongsTo('App\Cities','id_city','id');
    }

    public function province(){
        return $this->belongsTo('App\Provinces','id_province','id');
    }

    public function pemilikInstalasi(){
        return $this->hasOne('App\PemilikInstalasi', 'perusahaan_id', 'id');
    }

    public function perusahaanSpv(){
        return $this->hasOne('App\PerusahaanSpv', 'perusahaan_id', 'id');
//        return $this->hasOne('App\BusinessAreaUser', 'business_area_id', 'id');
    }

}
