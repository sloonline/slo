<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlowStatus extends Model
{
    protected $table = 'flow_status';
    
    public function workflow(){
        return $this->hasMany('app\Workflow', 'flow_status_id', 'id');
    }
    
}
