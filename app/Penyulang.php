<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penyulang extends Model
{
    //
    protected $table = 'penyulang';

    public function businessArea(){
        return $this->belongsTo('App\BusinessArea', 'business_area', 'id');
    }
}
