<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    //
    protected $table = 'lokasi';

    public function businessArea(){
        return $this->belongsTo('App\BusinessArea', 'business_area', 'id');
    }
}
