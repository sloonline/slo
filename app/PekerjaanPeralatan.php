<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PekerjaanPeralatan extends Model
{
  protected $table='pekerjaan_peralatan';

  public function pekerjaan(){
    return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
  }

  public function peralatan(){
    return $this->belongsTo('App\Peralatan','peralatan_id','id');
  }


  public function status_peralatan(){
    return $this->belongsTo('App\References','status','id');
  }


}
