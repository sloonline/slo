<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCode extends Model
{
    //
    protected $table = 'company_code';

    public function businessArea(){
        return $this->hasMany('App\BusinessArea', 'company_code', 'company_code');
    }

    public function perusahaan(){
        return $this->hasOne('App\Perusahaan', 'id_company_code', 'id');
    }

}
