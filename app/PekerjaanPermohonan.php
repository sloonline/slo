<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PekerjaanPermohonan extends Model
{
    protected $table = 'pekerjaan_permohonan';

    protected $fillable = [
                    'id_permohonan','id_pekerjaan'

    ];

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'id_pekerjaan', 'id');
    }
    public function permohonan(){
        return $this->belongsTo('App\Permohonan', 'id_permohonan', 'id');
    }

}
