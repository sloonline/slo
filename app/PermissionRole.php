<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionRole extends Model
{
    use SoftDeletes;
    protected $table = "permission_role";
    
    public function permission(){
        return $this->belongsTo('App\Permission','permission_id','id');
    }

    public function role(){
        return $this->belongsTo('App\Role','role_id','id');
    }
}
