<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'city',
        'id_province'

    ];

    public function province(){
        return $this->belongsTo('App\Provinces', 'id_province', 'id');
    }

    public function user(){
        return $this->hasMany('App\Users', 'id_province', 'id');
    }


}
