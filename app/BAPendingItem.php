<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BAPendingItem extends Model
{
    protected $table='BA_PENDING_ITEM';

    public function pending_item()
    {
        return $this->belongsTo('App\PendingItem', 'pending_item_id', 'id');
    }
}
