<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusNotif extends Model
{
    //
    protected $table = 'status_notif';

    protected $fillable = [
        'user_id',
        'status',
        'deskripsi'
    ];
}
