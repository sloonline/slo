<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RAB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InstalasiPembangkit extends Model
{
    use SoftDeletes;
    protected $table = 'instalasi_pembangkit';
    protected $fillable = [
        'id_jenis_instalasi', 'nama_instalasi', 'alamat_instalasi', 'id_provinsi',
        'id_kota', 'longitude_awal', 'latitude_awal', 'longitude_akhir', 'latitude_akhir',
        'kapasitas_terpasang', 'kapasitas_hasil_uji', 'nomor_pembangkit', 'nomor_generator', 'nomor_mesin',
        'kapasitas_modul', 'kapasitas_inverter', 'jumlah_modul', 'jumlah_inverter', 'id_bahan_bakar', 'surat_pernyataan', 'kontraktor'
    ];

    public function jenis_instalasi()
    {
        return $this->belongsTo('App\JenisInstalasi', 'id_jenis_instalasi', 'id');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Provinces', 'id_provinsi', 'id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Cities', 'id_kota', 'id');
    }

    public function pemilik()
    {
        return $this->belongsTo('App\PemilikInstalasi', 'pemilik_instalasi_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by', 'username');
    }

    public function bahanBakar()
    {
        return $this->belongsTo('App\References', 'id_bahan_bakar', 'id');
    }

    public function getPermohonan()
    {
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', '1')
            ->orderBy('tanggal_permohonan')->get();

        return $permohonan;
    }

    public function getRiwayat()
    {
        $permohonan = Permohonan::select('permohonan.*,orders.nomor_order', 'orders.tanggal_order', 'produk.produk_layanan')
            ->join('orders', 'orders.id = permohonan.id_orders')
            ->join('produk', 'produk.id = permohonan.id_produk')
            ->where('permohonan.id_instalasi', $this->id)
            ->where('permohonan.id_tipe_instalasi', '1')
            ->whereIn('orders.id', RAB::select('order_id')->get())
            ->orderBy('orders.tanggal_order', 'asc', 'permohonan.id', 'asc')->get();

        return $permohonan;
    }

    public function isAllowEdit()
    {
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', ID_PEMBANGKIT)
            ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function isOrderRej(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
        ->where('id_tipe_instalasi', ID_PEMBANGKIT)
        ->join('orders', 'orders.id','=', 'permohonan.id_orders')
        ->where('orders.id_flow_status', 3) #order rejected
        ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function djk_agenda()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('tipe_instalasi_id',ID_PEMBANGKIT);
    }

    public function lhpp()
    {
        return $this->hasOne('App\Lhpp', 'instalasi_id', 'id')->where('tipe_instalasi',ID_PEMBANGKIT);
    }

    public static function total(){
        $data = DB::table('instalasi_pembangkit')
            ->select('count(id) as total')
            ->get();
        return $data[0]->total;

    }

    public static function totalByPemintaJasa(){
        $data = DB::table('instalasi_pembangkit')
            ->select('count(id) as total')
            ->where('created_by', Auth::user()->username)
            ->get();
        return $data[0]->total;

    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'username');
    }

}
