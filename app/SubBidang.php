<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubBidang extends Model
{
    //
    use SoftDeletes;
    protected $table = 'sub_bidang';

    public function bidang(){
        return $this->belongsTo('App\Bidang', 'id_bidang', 'id');
    }

}
