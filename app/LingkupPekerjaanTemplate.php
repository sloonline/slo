<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkupPekerjaanTemplate extends Model
{
    protected $table = 'lingkup_pekerjaan_template';

    public function lingkup_pekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan');
    }

    public function produk(){
        return $this->belongsTo('App\Produk');
    }
}
