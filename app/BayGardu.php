<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BayGardu extends Model
{
    use SoftDeletes;
    protected $table = 'bay_gardu';

    public function pemilik()
    {
        return $this->belongsTo('App\PemilikInstalasi', 'pemilik_id', 'id');
    }

    public function pengenal()
    {
        return $this->belongsTo('App\References', 'tegangan_pengenal', 'id');
    }


    public function instalasi()
    {
        return $this->belongsTo('App\InstalasiTransmisi', 'instalasi_id', 'id');
    }

    protected function loadPemilik($bay)
    {
        for ($i = 0; $i < sizeof($bay); $i++) {
            $bay[$i]->nama_pemilik = ($bay[$i]->tipe_pemilik == MILIK_SENDIRI) ? "" : $bay[$i]->pemilik->nama_pemilik;
            $bay[$i]->nama_tegangan_pengenal = ($bay[$i]->tegangan_pengenal == null) ? "" : $bay[$i]->pengenal->nama_reference;
            $bay[$i]->nama_kontraktor = $bay[$i]->kode_kontraktor ." - ".$bay[$i]->kontraktor;
        }
        return $bay;
    }

    public function kontraktor(){
        return $this->belongsTo('App\Kontraktor', 'kode_kontraktor', 'kode');
    }

    public function djk_agenda()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('tipe_instalasi_id',ID_TRANSMISI)->where('jenis_instalasi_id', GARDU_INDUK);
    }

    public function lhpp()
    {
        return $this->hasOne('App\Lhpp', 'bay_gardu_id', 'id')->where('tipe_instalasi',ID_TRANSMISI);
    }

    public function permohonan(){
        return $this->hasMany('App\BayPermohonan', 'bay_id', 'id')->whereHas('permohonan', function ($query) {
            $query->where('status', '!=','DEL');
        });
    }

    public static function isPermohonanRej($arra_idpermohonan){
        $permohonan = Permohonan::whereIn('permohonan.id', $arra_idpermohonan)
            ->join('orders', 'orders.id','=', 'permohonan.id_orders')
            ->where('orders.id_flow_status', 3) #order rejected
            ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return true;
        } else {
            return false;
        }
    }

}
