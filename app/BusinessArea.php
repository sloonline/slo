<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessArea extends Model
{
    //
    protected $table = 'business_area';

    public function companyCode(){
        return $this->belongsTo('App\CompanyCode', 'company_code', 'company_code');
    }

    public function garduInduk(){
        return $this->hasMany('App\GarduInduk', 'business_area', 'id');
    }

    public function penyulang(){
        return $this->hasMany('App\Penyulang', 'business_area', 'id');
    }

    public function spv_business_code(){
        return $this->hasOne('App\BusinessAreaUser', 'business_area_id', 'business_area');
    }

    public function areaProgram(){
        return $this->hasMany('App\AreaProgram', 'business_area', 'id');
    }

    public function perusahaan(){
        return $this->hasOne('App\Perusahaan', 'id_business_area', 'id');
    }
}
