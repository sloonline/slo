<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\PendingItem;

class Pekerjaan extends Model
{
    protected $table = 'pekerjaan';
    use SoftDeletes;


    public function peralatan()
    {
        return $this->hasMany('App\PekerjaanPeralatan', 'pekerjaan_id', 'id');
    }

    public function permohonan()
    {
        return $this->belongsTo('App\Permohonan', 'permohonan_id', 'id');
    }

    public function wbs_io()
    {
        return $this->belongsTo('App\WbsIo', 'wbs_id', 'id');
    }

    public function pekerjaan_personil()
    {
        return $this->hasMany('App\PekerjaanPersonil', 'pekerjaan_id', 'id');
    }


    public function id_personil()
    {
        return $this->hasMany('App\PekerjaanPersonil', 'pekerjaan_id', 'id')->select(['personil_id', 'jabatan_id']);
    }

    public function flow_status()
    {
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public function pending_item()
    {
        return $this->hasMany('App\PendingItem', 'pekerjaan_id', 'id');
    }

    public static function GetPekerjaanByStatus($status_pekerjaan)
    {
        $pekerjaan = Pekerjaan::where('status_pekerjaan', $status_pekerjaan)->get();
        return $pekerjaan;
    }

    public static function getSPV($pekerjaan)
    {
        // dd($pekerjaan->permohonan->user->perusahaan->perusahaanSpv->spv);
        $user = $pekerjaan->permohonan->user;
        return User::getSpv($user);
    }

    public static function updateSysStatus($parent)
    {
        $obj_model = $parent->pekerjaan;
        $obj_model->sys_status = $parent->sys_status;
        $obj_model->save();

        return $obj_model;
    }


    public static function isAllowEdit($pekerjaan)
    {
        $latest_step = WorkflowController::getLatestHistory($pekerjaan->id, $pekerjaan->getTable());
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        if ($next == null) {
            return false;
        } else {
            $submit = $next->where('tipe_status', SUBMITTED)->first();
            $created = $next->where('tipe_status', CREATED)->first();
            return (in_array(array('role_id' => @$submit->role_id), Auth::user()->user_roles_id->toArray()) ||
                in_array(array('role_id' => @$created->role_id), Auth::user()->user_roles_id->toArray()));
        }
    }

    public static function isLastApproval($pekerjaan)
    {
        $isLast = false;
        $latest_step = WorkflowController::getLatestHistory($pekerjaan->id, $pekerjaan->getTable());
        // dd($latest_step);
        $next = ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id) : null;
        // dd(sizeOf($next));
        if (sizeOf($next) <= 0) {
            $isLast = true;
        }

        return $isLast;
    }

    private static function getFlowStatusDmApproved()
    {
        //get flow status id where pekerjaan dm approve
        $workflow = Workflow::where('modul', MODUL_PEKERJAAN)->where('NEXT', null)->first();
        return ($workflow != null) ? $workflow->flow_status_id : null;
    }

    public static function getWhereDMApproved()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->get();
    }

    public function kickOffMeeting()
    {
        return $this->hasMany('App\LaporanKickOff', 'pekerjaan_id', 'id');
    }

    public function dataTeknik()
    {
        return $this->hasMany('App\LaporanDataTeknik', 'pekerjaan_id', 'id');
    }

    public function blangkoInspeksi()
    {
        return $this->hasMany('App\BlangkoInspeksi', 'pekerjaan_id', 'id');
    }

    public function hilo()
    {
        return $this->hasOne('App\LaporanHilo');
    }

    public function rlb()
    {
        return $this->hasMany('App\Rlb', 'pekerjaan_id', 'id');
    }

    public function rls()
    {
        return $this->hasMany('App\Rls', 'pekerjaan_id', 'id');
    }

    public static function getUnusedKickOffMeeting()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('kickOffMeeting')->get();
    }

    public static function getUnusedDataTeknik()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('dataTeknik')->get();
    }

    public static function getUnusedBlangkoInspeksi()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('blangkoInspeksi')->get();
    }

    public static function getUnusedHilo()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('hilo')->get();
    }

    public static function getUnusedRlb()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('rlb')->get();
    }

    public static function getUnusedRls()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('rls')->get();
    }

    public function berita_inspeksi()
    {
        return $this->hasMany('App\BaInspeksi', 'pekerjaan_id', 'id');
    }

    public static function getUnusedBeritaInspeksi()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('berita_inspeksi')->get();
    }

    public function laporan_pekerjaan()
    {
        return $this->hasOne('App\LaporanPekerjaan', 'pekerjaan_id', 'id');
    }

    public static function getAllByPemohon(){
        return Pekerjaan::whereHas('permohonan', function ($query) {
            $query->whereHas('order', function ($query2) {
                $query2->where('id_perusahaan',Auth::user()->perusahaan_id);
            });
        })->get();
    }

    public function lpi()
    {
        return $this->hasMany('App\Lpi', 'pekerjaan_id', 'id');
    }

    public static function getUnusedLpi()
    {
        return Pekerjaan::where('id_flow_status', self::getFlowStatusDmApproved())->whereDoesntHave('lpi')->get();
    }
}
