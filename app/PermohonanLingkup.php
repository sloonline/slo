<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermohonanLingkup extends Model
{
    //
    protected $table = 'permohonan_lingkup';

    public function jenis_pekerjaan(){
        return $this->belongsTo("App\JenisPekerjaan","jenis_pekerjaan_id","id");
    }

    public function child(){
        return $this->belongsTo("App\JenisPekerjaan","child_id","id");
    }

    public function permohonan(){
        return $this->belongsTo("App\Permohonan","permohonan_id","id");
    }
}
