<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuratTugas extends Model
{
    //
    use SoftDeletes;
    protected $table = 'surat_tugas';

    public function program(){
        return $this->hasMany('App\Program', 'id_surat_tugas', 'id');
    }

    public function instalasi(){
        return $this->hasMany('App\InstalasiDistribusi', 'id_surat_tugas', 'id');
    }

    public function produk(){
        return $this->hasMany('App\LayananSuratTugas','surat_tugas_id','id');
    }

    public function isInUsed(){
        //cek apakah sedang digunakan dalam order apa tidak, jika ya maka tidak boleh dihapus
        //get all instalasi
        $instalasi = $this->hasMany('App\InstalasiDistribusi', 'id_surat_tugas', 'id')->select('id')->get()->toArray();
        //check on permohonan
        $permohonan = Permohonan::whereIn('id_instalasi',$instalasi)->where('id_tipe_instalasi',ID_DISTRIBUSI)->get();
        return (sizeof($permohonan) > 0) ? true : false;
    }

    public function perusahaan(){
        return $this->belongsTo('App\Perusahaan','id_perusahaan','id');
    }

}
