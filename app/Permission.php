<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $table = 'permissions';
    use SoftDeletes;

    public function modules(){
        return $this->belongsTo('App\Module','module_id','id');
    }

    public function getLastID(){
        $lastid = $this->all()->sortByDesc('id')->first();
        if($lastid==null) $id = 1;
        else $id = $lastid->id + 1;

        return $id;
    }
}
