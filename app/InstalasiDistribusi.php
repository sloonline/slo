<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InstalasiDistribusi extends Model
{
    use SoftDeletes;
    protected $table = 'instalasi_distribusi';

    public function jenis_instalasi()
    {
        return $this->belongsTo('App\JenisInstalasi', 'id_jenis_instalasi', 'id');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Provinces', 'id_provinsi', 'id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Cities', 'id_kota', 'id');
    }

    public function provinsi_akhir()
    {
        return $this->belongsTo('App\Provinces', 'id_provinsi_akhir', 'id');
    }

    public function kota_akhir()
    {
        return $this->belongsTo('App\Cities', 'id_kota_akhir', 'id');
    }

    public function pemilik()
    {
        return $this->belongsTo('App\PemilikInstalasi', 'pemilik_instalasi_id', 'id')->withTrashed();
    }

    public function teganganPengenal()
    {
        return $this->belongsTo('App\References', 'tegangan_pengenal', 'id');
    }


    public function getPermohonan()
    {
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', '3')
            ->orderBy('tanggal_permohonan')->get();

        return $permohonan;
    }

    public function getRiwayat()
    {
        $permohonan = Permohonan::select('permohonan.*,orders.nomor_order', 'orders.tanggal_order', 'produk.produk_layanan')
            ->join('orders', 'orders.id = permohonan.id_orders')
            ->join('produk', 'produk.id = permohonan.id_produk')
            ->where('permohonan.id_instalasi', $this->id)
            ->where('permohonan.id_tipe_instalasi', '3')
            ->whereIn('orders.id', RAB::select('order_id')->get())
            ->orderBy('orders.tanggal_order', 'asc', 'permohonan.id', 'asc')->get();

        return $permohonan;
    }


    public function suratTugas()
    {
        return $this->belongsTo('App\SuratTugas', 'id_surat_tugas', 'id')->withTrashed();
    }


    public function program()
    {
        return $this->belongsTo('App\Program', 'id_program', 'id')->withTrashed();
    }

    public function lingkupPekerjaan()
    {
        return $this->hasMany('App\LingkupDistribusi', 'instalasi_id', 'id');
    }

    public static function parentId($instalasi)
    {
        if ($instalasi->id_lokasi_area != null) {
            return $instalasi->id_lokasi_area;
        } else if ($instalasi->id_penyulang_area != null) {
            return $instalasi->id_penyulang_area;
        } else if ($instalasi->id_gardu_area != null) {
            return $instalasi->id_gardu_area;
        } else {
            return 0;
        }
    }

    public static function grandParentId($instalasi)
    {
        $parentId = self::parentId($instalasi);
        $program = $instalasi->program;
        $acuan = $program->jenisProgram->acuan_1;
        $lokasi = LokasiArea::find($parentId);
        $penyulang = PenyulangArea::find($parentId);
        $gardu = GarduArea::find($parentId);
        switch ($acuan) {
            case TIPE_KONTRAK:
                if ($instalasi->id_lokasi_area != null) {
                    return $lokasi->id_kontrak;
                } else {
                    return 0;
                }
                break;
            case TIPE_AREA:
                if ($instalasi->id_lokasi_area != null) {
                    return $lokasi->id_area_program;
                } else if ($instalasi->id_penyulang_area != null) {
                    return $penyulang->id_area_program;
                } else if ($instalasi->id_gardu_area != null) {
                    return $gardu->id_area_program;
                } else {
                    return 0;
                }
                break;
        }
    }

    public function perusahaan(){
        return $this->belongsTo('App\Perusahaan', 'id_perusahaan', 'id');
    }

    public function kontraktor(){
        return $this->belongsTo('App\Kontraktor', 'kode_kontraktor', 'kode');
    }

    public function jenis_program()
    {
        return $this->belongsTo('App\JenisProgram', 'id_program', 'id');
    }

    public function isAllowEdit(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', ID_DISTRIBUSI)
            ->orderBy('tanggal_permohonan')->get();
        if(sizeof($permohonan) > 0){
            return false;
        }else{
            return true;
        }
    }

    public function djk_agenda()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('tipe_instalasi_id',ID_DISTRIBUSI);
    }

    public function lhpp()
    {
        return $this->hasOne('App\Lhpp', 'instalasi_id', 'id')->where('tipe_instalasi',ID_DISTRIBUSI);
    }

    public static function total(){
        $data = DB::table('instalasi_distribusi')
            ->select('count(id) as total')
            ->get();
        return $data[0]->total;
    }

    public static function totalByPemintaJasa(){
        $data = DB::table('instalasi_distribusi')
            ->select('count(id) as total')
            ->where('created_by', Auth::user()->username)
            ->get();
        return $data[0]->total;

    }

    public function isOrderRej(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', ID_DISTRIBUSI)
            ->join('orders', 'orders.id','=', 'permohonan.id_orders')
            ->where('orders.id_flow_status', 3) #order rejected
            ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'username');
    }
}
