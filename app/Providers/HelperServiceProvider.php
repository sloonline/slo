<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()  
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require base_path().'/app/Helpers/EmailHelper.php';
        require base_path().'/app/Helpers/ActiveNavHelper.php';
        require base_path().'/app/Helpers/ExportWordHelper.php';
        require base_path().'/app/Helpers/HtmlConverterHelper.php';
        require base_path().'/app/Helpers/ApiDjkHelper.php';
    }
}
