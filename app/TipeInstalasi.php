<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeInstalasi extends Model
{
      protected $table = 'tipe_instalasi';

    protected $fillable = [
                    'nama_instalasi',

    ];

    public function jenisInstalasi(){
    	return $this->hasMany('App\jenisInstalasi');
    }

    public function lingkup_pekerjaan(){
    	return $this->hasMany('App\LingkupPekerjaan');
    }

    public function produk(){
        return $this->belongsToMany('App\Produk')->withTimestamps();
    }
}
