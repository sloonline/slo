<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    public function getLastID(){
        $lastid = $this->all()->sortByDesc('id')->first();
        if($lastid==null) $id = 1;
        else $id = $lastid->id + 1;

        return $id;
    }

    public function tos(){
        return $this->belongsTo('App\User', 'to', 'username');
    }

    public function froms(){
        return $this->belongsTo('App\User', 'from', 'username');
    }
}
