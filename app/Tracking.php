<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = 'tracking';

    public function permohonan(){
        return $this->belongsTo('App\Permohonan', 'permohonan_id', 'id');
    }

    public function order(){
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }

    public function rab(){
        return $this->belongsTo('App\Rab', 'rab_id', 'id');
    }

    public function kontrak(){
        return $this->belongsTo('App\Kontrak', 'kontrak_id', 'id');
    }

    public function wbs_io(){
        return $this->belongsTo('App\WbsIo', 'wbsio_id', 'id');
    }

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function lhpp(){
        return $this->belongsTo('App\Lhpp', 'lhpp_id', 'id');
    }
}


