<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RAB extends Model
{
    use SoftDeletes;
    protected $table = 'rab';

    public function detailRab(){
        return $this->hasMany('App\RABDetail', 'id_rab', 'id');
    }

    public function permohonan(){
        return $this->hasMany('App\RABPermohonan', 'rab_id', 'id');
    }

    public static function cek_permohonan($order_id){
        $permohonan = DB::table('permohonan')
                        ->select(DB::raw('count(id) as id_count'))
                        ->where('id_orders', $order_id)
                        ->whereNotIn('id', function($query) use ($order_id){
                            $query->select('b.permohonan_id')
                                  ->from('rab a')
                                  ->join('rab_permohonan b', 'a.ID', '=', 'b.RAB_ID')
                                  ->where('order_id', $order_id);
                        })->first();
        return $permohonan;
    }

    public function flow_status(){
        return $this->belongsTo('App\FlowStatus','id_flow_status','id');
    }
    
    public function order(){
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }


    public static function isAllowEdit($rab){
        Order::isAllowEditRab(@$rab->order);
    }


    public static function updateSysStatus($parent){
        $obj_model = $parent->latest_kontrak->kontrak_version_rab;
        foreach ($obj_model as $item){
            $item->rab->sys_status = $parent->sys_status;
            $item->rab->save();
        }
        
        return $obj_model ;
    }
}
