<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkupDistribusi extends Model
{
    protected $table = 'lingkup_distribusi';

    public function instalasi(){
        return $this->belongsTo('App\InstalasiDistribusi','instalasi_id','id');
    }


    public function lingkupPekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan','lingkup_id','id');
    }
}
