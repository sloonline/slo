<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalPelaksanaan extends Model
{
    protected $table = 'jadwal_pelaksanaan';
    
    public function kontrak(){
        return $this->belongsTo('App\Kontrak', 'kontrak_id', 'id');
    }
}
