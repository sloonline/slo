<?php

namespace App;

use App\Http\Controllers\WorkflowController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class Order extends Model
{
    protected $table = 'orders';
    use SoftDeletes;

    protected $fillable = [
        'nomor_order',
        'tanggal_order',
        'id_perusahaan',
        'nomor_sp', // nomor surat permintaan
        'tanggal_sp', // tanggal surat permintaan
        'file_sp', //file surat permintaan

    ];

    public function perusahaan()
    {
        return $this->belongsTo('App\Perusahaan', 'id_perusahaan', 'id');
    }

    public function tipe_instalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi_id', 'id');
    }

    public function permohonan()
    {
        return $this->hasMany('App\Permohonan', 'id_orders', 'id');
    }

    public function flow_status()
    {
        return $this->belongsTo('App\FlowStatus', 'id_flow_status', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'username');
    }

    public function rab()
    {
        return $this->hasMany('App\RAB', 'order_id', 'id');
    }


    public static function getOrderPLN()
    {
        $query = DB::table('orders o')
            ->join('perusahaan p', 'p.id', '=', 'o.id_perusahaan')
            ->join('flow_status f', 'f.id', '=', 'o.id_flow_status')
            ->where('p.kategori_perusahaan', 1)
            ->where('f.tipe_status', APPROVED)
            ->where('f.modul', MODUL_ORDER)
            ->select('o.*')
            ->get();
        $order = Order::hydrate($query);
        return $order;

    }

    public static function getOrderSwasta()
    {
        $query = DB::table('orders o')
            ->join('perusahaan p', 'p.id', '=', 'o.id_perusahaan')
            ->join('flow_status f', 'f.id', '=', 'o.id_flow_status')
            ->where('p.kategori_perusahaan', '!=', 1)
            ->where('f.tipe_status', APPROVED)
            ->where('f.modul', MODUL_ORDER)
            ->select('o.*')
            ->get();
        $order = Order::hydrate($query);
        return $order;
    }

    static function getNextWorkflowStep($order, $modul)
    {
        $latest_step = WorkflowController::getLatestHistory(@$order->id, @$order->getTable());
        return ($latest_step != null) ? WorkflowController::getAllNextStep($latest_step->workflow_id)->where('modul', $modul) : null;
    }

    public static function isAllowEditRab($order)
    {
        $next = self::getNextWorkflowStep($order, MODUL_RAB);
        if ($next == null) {
            return false;
        } else {
            $submit = $next->where('tipe_status', SUBMITTED)->first();
            $created = $next->where('tipe_status', CREATED)->first();
            $roles = Auth::user()->user_roles_id->toArray();
            return (in_array(array('role_id' => @$submit->role_id), $roles) || in_array(array('role_id' => @$created->role_id), $roles));
        }
    }

    public static function isAllowEditPenawaranRab($order)
    {
        $next = self::getNextWorkflowStep($order, MODUL_RAB);
        if ($next == null) {
            return false;
        } else {
            $sent = $next->where('tipe_status', SENT)->first();
            $roles = Auth::user()->user_roles_id->toArray();
            return in_array(array('role_id' => @$sent->role_id), $roles);
        }
    }

    public static function isEksternalAllowEditRab($order)
    {
        $next = self::getNextWorkflowStep($order, MODUL_RAB);
        if ($next == null) {
            return false;
        } else {
            $approve = $next->where('tipe_status', APPROVED)->first();
            $roles = Auth::user()->user_roles_id->toArray();
            return in_array(array('role_id' => @$approve->role_id), $roles);
        }
    }


    public static function isEksternalAllowEdit($order)
    {
        $next = self::getNextWorkflowStep($order, MODUL_ORDER);
        if ($next == null) {
            return false;
        } else {
            $submit = $next->where('tipe_status', SUBMITTED)->first();
            $revised = $next->where('tipe_status', REVISED)->first();
            $roles = Auth::user()->user_roles_id->toArray();
            return (in_array(array('role_id' => @$submit->role_id), $roles) || in_array(array('role_id' => @$revised->role_id), $roles));
        }
    }


    public static function isAllowEdit($order)
    {
        $next = self::getNextWorkflowStep($order, MODUL_ORDER);
        if ($next == null) {
            return false;
        } else {
            $approve = $next->where('tipe_status', APPROVED)->first();
            $roles = Auth::user()->user_roles_id->toArray();
            return in_array(array('role_id' => @$approve->role_id), $roles);
        }
    }

    public static function updateSysStatus($parent)
    {
        $obj_model = $parent[0]->rab->order;
        $obj_model->sys_status = $parent[0]->rab->sys_status;
        $obj_model->save();

        return $obj_model;
    }

    //ini dipakai ketika workflow dijalankan untuk execute beberapa case
    public static function actionOnUpdateStatus($request, $currentWf)
    {
        //jika ternyata next nya adalah create RAB, maka send email ke peminta jasa (order approved)
        if ($currentWf->modul == MODUL_ORDER && $currentWf->nextWf->modul == MODUL_RAB) {
            $ref_id = $request->id;
            $order = self::findOrFail($ref_id);
            $ref_status = $request->status;
            $flow_status = WorkflowController::getFlowStatusId($ref_status, $request->modul);
            $jenis_user = ($order->user->jenis_user != TIPE_INTERNAL) ? TIPE_EKSTERNAL : TIPE_INTERNAL;
            $notif = new Notification();
            $notif->from = Auth::user()->username;
            $notif->url = strtolower($jenis_user) . '/' . $request->url_notif . $ref_id;
            $notif->subject = $request->modul . " " . $flow_status->tipe_status;
            $notif->to = $order->user->username;
            $notif->status = "UNREAD";
            $notif->message = $request->modul . " " . $flow_status->tipe_status . " oleh <br/><b>" . Auth::user()->nama_user . "</b>";
            $notif->icon = "icon-briefcase";
            $notif->color = "btn-green";
            $notif->save();

            $data = array();
            $data['order'] = $order;
            $data['user'] = $order->user;
            $data['perusahaan'] = $order->perusahaan;
            $data['notif'] = $notif;
            $data['status'] = $flow_status->tipe_status;
            //call send email helper
            $mail_data = array(
                "data" => (object)$data,
                "data_alias" => "data",
                "to" => $order->user->email,
                "to_name" => $order->user->nama_user,
                "subject" => "Nomor Order " . $order->nomor_order . " " . (($flow_status->tipe_status == APPROVED) ? 'Disetujui' : 'Perlu Diperbaiki'),
                "description" => 'ORDER ' . $flow_status->tipe_status
            );
            sendEmail($mail_data, 'emails.confirm_order', $notif->subject, 'ORDER');
        }
    }

    //get summary order
    public static function calculateSummary($orders)
    {
        $data = array();
        $data[SUBMITTED] = 0;
        $data[APPROVED] = 0;
        $data[REJECTED] = 0;
        $data[REVISED] = 0;
        foreach ($orders as $order) {
            if (@$order->flow_status->modul == MODUL_ORDER) {
                switch (@$order->flow_status->tipe_status) {
                    case SUBMITTED :
                        $data[SUBMITTED]++;
                        break;
                    case APPROVED:
                        $data[APPROVED]++;
                        break;
                    case REJECTED :
                        $data[REJECTED]++;
                        break;
                    case REVISED :
                        $data[REVISED]++;
                        break;
                }
            }
        }
        return $data;
    }

    public static function getUser($order)
    {
        return $order->user;
    }

    public static function getSPV($order)
    {
//        $business_area = $order->user->perusahaan->id_business_area;
//        $spv = businessAreaUser::where('business_area_id', $business_area)->first();

//        $spv = $order->user->perusahaan->perusahaanSpv->spv;
        $user = $order->user;
        return User::getSpv($user);
    }


    public static function getTotalNewOrder()
    {

        $orders = Order::whereHas('flow_status', function ($query) {
            $query->where('tipe_status', SUBMITTED)->where('modul', MODUL_ORDER);
        });

        return ($orders == null) ? 0 : $orders->count();
    }

    public static function getTotalNewOrderByPemintajasa()
    {

        $orders = Order::where('id_perusahaan', Auth::user()->perusahaan_id)
            ->whereHas('flow_status', function ($query) {
                $query->where('tipe_status', SUBMITTED)->where('modul', MODUL_ORDER);
            });

        return ($orders == null) ? 0 : $orders->count();
    }

}
