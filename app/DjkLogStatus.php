<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DjkLogStatus extends Model
{
    protected $table = 'djk_log_status';

    public function djkAgenda(){
        return $this->belongsTo('App\DjkAgenda', 'djk_agenda_id', 'id');
    }

    public function status(){
        return $this->belongsTo('App\References', 'status_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

