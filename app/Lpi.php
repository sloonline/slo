<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lpi extends Model
{
    protected $table='lpi';
    use SoftDeletes;

    public function pekerjaan(){
        return $this->belongsTo('App\Pekerjaan', 'pekerjaan_id', 'id');
    }

    public function lingkup_pekerjaan(){
        return $this->belongsTo('App\LingkupPekerjaan','lingkup_pekerjaan_id', 'id');
    }

    public function instalasi()
    {
        if ($this->tipe_instalasi_id == 1) {
            return $this->belongsTo('App\InstalasiPembangkit', 'instalasi_id', 'id');
        } elseif ($this->tipe_instalasi_id == 2) {
            return $this->belongsTo('App\InstalasiTransmisi', 'instalasi_id', 'id');
        } elseif ($this->tipe_instalasi_id == 3) {
            return $this->belongsTo('App\InstalasiDistribusi', 'instalasi_id', 'id');
        } elseif ($this->tipe_instalasi_id == 4) {
            return $this->belongsTo('App\InstalasiPemanfaatanTT', 'instalasi_id', 'id');
        } elseif ($this->tipe_instalasi_id == 5) {
            return $this->belongsTo('App\InstalasiPemanfaatanTM', 'instalasi_id', 'id');
        } else {
            return null;
        }
    }

    public function bay()
    {
        return $this->belongsTo('App\BayGardu', 'bay_id', 'id');
    }

    public function jenis_instalasi()
    {
        return $this->belongsTo('App\JenisInstalasi', 'jenis_instalasi_id', 'id');
    }

    public function permohonan()
    {
        return $this->belongsTo('App\Permohonan', 'permohonan_id', 'id');
    }

    public function tipe_instalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi_id', 'id');
    }
}
