<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InstalasiTransmisi extends Model
{
    use SoftDeletes;
    protected $table = 'instalasi_transmisi';

    protected $fillable = [
        'id_jenis_instalasi', 'nama_instalasi', 'alamat_instalasi', 'id_provinsi',
        'id_kota', 'longitude_awal', 'latitude_awal', 'longitude_akhir', 'latitude_akhir',
        'kapasitas_gi', 'bay_line', 'bay_bus_coupler', 'bay_tf_tenaga', 'bay_reaktor', 'bay_kapasitor',
        'sutet', 'sktt', 'sklt', 'panjang_tt', 'jml_tower', 'sis_jar_tower',
        'kap_pemutus_tenaga', 'kap_tf_tenaga', 'kap_bank_kapasitor', 'kap_bank_reaktor', 'tegangan_pengenal', 'surat_pernyataan',
        'alamat_akhir_instalasi', 'id_provinsi_akhir', 'id_kota_akhir', 'kontraktor'

    ];

    public function jenis_instalasi()
    {
        return $this->belongsTo('App\JenisInstalasi', 'id_jenis_instalasi', 'id');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Provinces', 'id_provinsi', 'id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Cities', 'id_kota', 'id');
    }

    public function pemilik()
    {
        return $this->belongsTo('App\PemilikInstalasi', 'pemilik_instalasi_id', 'id');
    }

    public function teganganPengenal()
    {
        return $this->belongsTo('App\References', 'tegangan_pengenal', 'id');
    }

    public function sistemJaringanTransmisi()
    {
        return $this->belongsTo('App\References', 'sis_jar_tower', 'id');
    }

    public function getPermohonan()
    {
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', '2')
            ->orderBy('tanggal_permohonan')->get();

        return $permohonan;
    }

    public function getRiwayat()
    {
        $permohonan = Permohonan::select('permohonan.*,orders.nomor_order', 'orders.tanggal_order', 'produk.produk_layanan')
            ->join('orders', 'orders.id = permohonan.id_orders')
            ->join('produk', 'produk.id = permohonan.id_produk')
            ->where('permohonan.id_instalasi', $this->id)
            ->where('permohonan.id_tipe_instalasi', '2')
            ->whereIn('orders.id', RAB::select('order_id')->get())
            ->orderBy('orders.tanggal_order', 'asc', 'permohonan.id', 'asc')->get();

        return $permohonan;
    }

    public function BayGardu()
    {
        return $this->hasMany('App\BayGardu', 'instalasi_id', 'id');
    }

    public function kontraktor()
    {
        return $this->belongsTo('App\Kontraktor', 'kode_kontraktor', 'kode');
    }

    public function isAllowEdit()
    {
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', ID_TRANSMISI)
            ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function djk_agenda()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('jenis_instalasi_id',$this->id_jenis_instalasi)->where('tipe_instalasi_id', ID_TRANSMISI);
    }

    public function djk_agenda_withoutJenis()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('tipe_instalasi_id', ID_TRANSMISI);
    }

    public function lhpp()
    {
        return $this->hasOne('App\Lhpp', 'instalasi_id', 'id')->where('tipe_instalasi', ID_TRANSMISI);
    }

    public static function getUnusedLhppWithAgenda()
    {
        #select all transmisi yang jenis instalasi bukan gardu
        $transmisi = InstalasiTransmisi::doesntHave("lhpp")
            ->whereHas('djk_agenda_withoutJenis', function($q){
            $q->where('djk_agenda.jenis_instalasi_id', '<>',GARDU_INDUK);
            })->get();
        #select all transmisi yang jenis instalasi gardu
        $bay = BayGardu::doesntHave("lhpp")
            ->whereHas('djk_agenda', function($q){
                $q->where('djk_agenda.jenis_instalasi_id', GARDU_INDUK);
            })->get();

        $gardu_induk = InstalasiTransmisi::has('BayGardu')
            ->get();

        $gi_has_agenda = array();
        foreach ($gardu_induk as $item) {
            $bay_gardu = array();
            $hasAgenda = false;
            foreach ($bay as $b) {
                if (@$b->instalasi->id == $item->id) {
                    $hasAgenda = true;
                    array_push($bay_gardu, $b);
                }
            }
            $item->bay_gardu = $bay_gardu;
            if($hasAgenda){
                array_push($gi_has_agenda, $item);
            }
        }
        $transmisi = $transmisi->merge($gi_has_agenda);
        return $transmisi;
    }

    public static function total(){
        $data = DB::table('instalasi_transmisi')
            ->select('count(id) as total')
            ->get();
        return $data[0]->total;
    }

    public function isOrderRej(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
        ->where('id_tipe_instalasi', ID_TRANSMISI)
        ->join('orders', 'orders.id','=', 'permohonan.id_orders')
        ->where('orders.id_flow_status', 3) #order rejected
        ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function totalByPemintaJasa(){
        $data = DB::table('instalasi_transmisi')
            ->select('count(id) as total')
            ->where('created_by', Auth::user()->username)
            ->get();
        return $data[0]->total;

    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'username');
    }

}
