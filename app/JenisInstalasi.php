<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisInstalasi extends Model
{
    protected $table = 'jenis_instalasi';

    protected $fillable = [
                    'kode_instalasi','jenis_instalasi','tipe_instalasi',

    ];

    public function tipeInstalasi(){
    	 return $this->belongsTo('App\TipeInstalasi', 'tipe_instalasi');
    }


    public function lingkupPekerjaan(){
        return $this->hasMany('App\JenisIntalasiPekerjaan','jenis_instalasi_id','id');
    }

}
