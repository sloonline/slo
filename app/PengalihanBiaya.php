<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengalihanBiaya extends Model
{
    protected $table = "pengalihan_biaya";

    use SoftDeletes;

    public function wbs(){
        return $this->belongsTo('App\WbsIo', 'wbs_id', 'id');
    }
}
