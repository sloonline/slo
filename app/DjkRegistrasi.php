<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DjkRegistrasi extends Model
{
    protected $table = 'djk_registrasi';
    
    public function djk_agenda(){
        return $this->belongsTo('App\DjkAgenda', 'djk_agenda_id','id');
    }
}
