<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LhppKit extends Model
{
    protected $table='lhpp_kit';

    public function lhpp() {
        return $this->belongsTo('App\Lhpp');
    }
}
