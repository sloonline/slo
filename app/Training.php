<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use SoftDeletes;

    protected $table = "training";

    public function personil(){
        return $this->hasMany('App\PersonilInspeksi','training_id','id');
    }
}
