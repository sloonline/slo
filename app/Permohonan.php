<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Permohonan extends Model
{
    use SoftDeletes;
    protected $table = 'permohonan';

    protected $fillable = [
        'id_order',
        'nomor_permohonan',
        'tanggal_permohonan',
        'id_lingkup_pekerjaan',
        'id_jenis_usaha',// ambil dari Reference - Jenis Usaha
        'nomor_ijin_usaha',
        'masa_ijin_usaha',
        'file_ijin_usaha',
        'nama_kontrak',
        'nomor_kontrak',
        'tanggal_kontrak',
        'masa_kontrak',
        'file_kontrak',
        'nomor_sp_jbtl', // nomor surat perjanjian jual beli tenaga listrik
        'tanggal_sp_jbtl', // tanggal surat perjanjian jual beli tenaga listrik
        'masa_sp_jbtl', // masa surat perjanjian jual beli tenaga listrik
        'file_sp_jbtl', // file surat perjanjian jual beli tenaga listrik
        'nama_instalasi',
        'alamat_instalasi',
        'id_provinsi',
        'id_kota'

    ];

    protected $dates = ['tanggal_permohonan'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'id_orders', 'id');
    }

    public function lingkup_pekerjaan()
    {
        return $this->belongsTo('App\LingkupPekerjaan', 'id_lingkup_pekerjaan', 'id');
    }

    public function jenis_usaha()
    {
        return $this->belongsTo('App\References', 'id_jenis_usaha', 'id');
    }

    public function provinces()
    {
        return $this->belongsTo('App\Provinces', 'id_provinsi', 'id');
    }

    public function cities()
    {
        return $this->belongsTo('App\Cities', 'id_kota', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'username');
    }

    public function instalasi_permohonan()
    {
        return $this->belongsTo('App\InstalasiPermohonan', 'id', 'id_permohonan');
    }

    public function instalasi()
    {
        if ($this->id_tipe_instalasi == 1) {
            return $this->belongsTo('App\InstalasiPembangkit', 'id_instalasi', 'id');
        } elseif ($this->id_tipe_instalasi == 2) {
            return $this->belongsTo('App\InstalasiTransmisi', 'id_instalasi', 'id');
        } elseif ($this->id_tipe_instalasi == 3) {
            return $this->belongsTo('App\InstalasiDistribusi', 'id_instalasi', 'id');
        } elseif ($this->id_tipe_instalasi == 4) {
            return $this->belongsTo('App\InstalasiPemanfaatanTT', 'id_instalasi', 'id');
        } elseif ($this->id_tipe_instalasi == 5) {
            return $this->belongsTo('App\InstalasiPemanfaatanTM', 'id_instalasi', 'id');
        } else {
            return redirect('/eksternal')->with('error', 'Instalasi tidak terdaftar.');
        }
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk', 'id_produk', 'id');
    }

    public function tipeInstalasi()
    {
        return $this->belongsTo('App\TipeInstalasi', 'id_tipe_instalasi', 'id');
    }

    public function rab()
    {
        return $this->hasOne('App\RABPermohonan', 'permohonan_id', 'id');
    }

    public function bayPermohonan()
    {
        return $this->hasMany('App\BayPermohonan', 'permohonan_id', 'id');
    }

    // public static function getOrdered($id_tipe, $id)
    // {
    //     return Permohonan::where('id_tipe_instalasi', $id_tipe)
    //         ->where('created_by', Auth::user()->username)
    //         ->where('id_orders', null) //why null, testing purpose?
    //         ->where('id_instalasi', $id)
    //         ->get();
    // }

    // public function pekerjaan(){
    //     return $this->belongsTo('App\Pekerjaan', 'permohonan_id', 'id');
    // }

    public static function getOrdered($id_tipe, $id)
    {
        return Permohonan::where('id_tipe_instalasi', $id_tipe)
            ->where('created_by', Auth::user()->username)
            ->where('id_instalasi', $id)
            ->get();
    }


    public static function getaGroupedOrdered($id_tipe, $id)
    {
        $ordered = self::getOrdered($id_tipe, $id);
        $used = array();
        foreach ($ordered as $data) {
            $id = $data->id_lingkup_pekerjaan;
            if (isset($result[$id])) {
                $used[$id][] = $data;
            } else {
                $used[$id] = array($data);
            }
        }
        return $used;
    }

    //ambil semua permohonan kecuali distribusi
    public static function getAllPermohonan()
    {
//        return Permohonan::where('created_by', Auth::user()->username)->where('id_orders', null)->where('id_tipe_instalasi', '!=', ID_DISTRIBUSI)->orderBy('id', 'desc')->get();
        return Permohonan::where('created_by', Auth::user()->username)->where('id_orders', null)->orderBy('id', 'desc')->get();
    }

    public static function getAllPermohonanByOrder($order_id)
    {
        return Permohonan::where('id_orders', $order_id)->orderBy('id', 'desc')->get();
    }

    public static function getPermohonanDistribusi($id_surat_tugas = null)
    {

        $permohonan = DB::table('permohonan p')
            ->select('p.*,d.id_surat_tugas')
            ->join('instalasi_distribusi d', 'd.id', '=', 'p.id_instalasi')
            ->where('p.status', 'CART')
            ->where('p.created_by', Auth::user()->username)
            ->where('p.id_tipe_instalasi', ID_DISTRIBUSI);
        if ($id_surat_tugas != null) {
            $permohonan->where('id_surat_tugas', $id_surat_tugas);
        }
        return $permohonan->get();
    }


    public static function getPermohonanDistribusiByOrder($order_id)
    {
        $permohonan = DB::table('permohonan p')
            ->select('p.*,d.id_surat_tugas')
            ->join('instalasi_distribusi d', 'd.id', '=', 'p.id_instalasi')
            ->where('p.id_orders', $order_id)
            ->where('p.id_tipe_instalasi', ID_DISTRIBUSI);
        return $permohonan->get();
    }

    //ambil permohonan yang dari surat tugas
    public static function getPermohonanSuratTugas()
    {
        $permohonan = self::getPermohonanDistribusi();
        $surat_id = array_map(function ($obj) {
            return $obj->id_surat_tugas;
        }, $permohonan);
        $surat = SuratTugas::whereIn('id', $surat_id)->get();
        return $surat;
    }

    public static function getPermohonanSuratTugasByOrder($order_id)
    {
        $permohonan = self::getPermohonanDistribusiByOrder($order_id);
        $surat_id = array_map(function ($obj) {
            return $obj->id_surat_tugas;
        }, $permohonan);
        $surat = SuratTugas::whereIn('id', $surat_id)->get();
        return $surat;
    }

    // public function HasPekerjaan($permohonan_id){
    //     $user_permohonan = Permohonan::whereIn('id', function($query){
    //         $query->select('permohonan_id')
    //         ->from('Pekerjaan');
    //     })->get()->toArray();
    // }


    public static function getLatestOrder($total)
    {

        $permohonan = Permohonan::whereHas('order', function ($query) {
            $query->whereHas('flow_status', function ($query2) {
                $query2->where('tipe_status', SUBMITTED)->where('modul', MODUL_ORDER);
            });
        })->orderBy('updated_at', 'desc')->take($total)->get();

        return $permohonan;
    }


    public static function getLatestOrderByPemintaJasa($total)
    {

        $permohonan = Permohonan::whereHas('order', function ($query) {
            $query->whereHas('flow_status', function ($query2) {
                $query2->where('tipe_status', SUBMITTED)->where('modul', MODUL_ORDER);
            })
                ->where('id_perusahaan', Auth::user()->perusahaan_id);
        })->orderBy('updated_at', 'desc')->take($total)->get();

        return $permohonan;
    }

    public static function getBay($permohonan)
    {
        $baypermohonan = $permohonan->bayPermohonan;
        if ($baypermohonan != null) {
            if (sizeof($baypermohonan) > 0) {
                $data = $baypermohonan[0];
                return $data->bayGardu;
            }
        }

        return null;
    }

}
