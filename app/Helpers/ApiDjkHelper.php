<?php
use App\BayGardu;
use App\InstalasiTransmisi;
use App\DjkLog;
use App\DjkAgenda;
use App\DjkRegistrasi;
use App\DjkHasil;
use Illuminate\Support\Facades\Auth;
use App\Lhpp;
use App\References;
use App\DjkLogStatus;
use App\JadwalPelaksanaan;
use Carbon\Carbon;

function execCurl($postData, $param_url)
{
    // dd($postData);
    $consumerId = "pusertif";
    //$secretKey = "cxDH2Zap";
    $secretKey = "Yhs6sxTW";
    $proxy = 'http://10.1.9.20:8080';
    $proxyauth = 'epusertif:P@ssw0rd';
    // $url_djk = URL_PROD_DJK;
    $url_djk = env('URL_API_DJK','http://103.87.161.97/slo/api/');
    //Computes timestamp
    date_default_timezone_set('UTC');
    $tStamp = strval((time()+50) - strtotime('1970-01-01 00:00:00'));
    //Computes signature by hashing the salt with the secret key as the key
    $signature = hash_hmac('sha256', $consumerId . "&" . $tStamp, $secretKey, true);
    // base64 encode�
    $encodedSignature = base64_encode($signature);

    $header[] = "X-cons-id:" . $consumerId;
    $header[] = "X-timestamp:" . $tStamp;
    $header[] = "X-signature:" . $encodedSignature;
//        dd($url_djk . $param_url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url_djk . $param_url);
   curl_setopt($ch, CURLOPT_PROXY, $proxy);
   curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    if ($postData != null) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
    }
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $tmp = curl_exec($ch);
    curl_close($ch);

    $djk_log = new DjkLog();
    $djk_log->url = $url_djk . $param_url;
    $djk_log->post_data = json_encode($postData);
    $djk_log->response_data = json_encode($tmp);
    $djk_log->created_by = (Auth::user() != null) ? Auth::user()->nama_user : "System Scheduler";
    $djk_log->save();

    return $tmp;
}

function requestNomorAgenda($param)
{
    #tipe_instalasi, instalasi, bay_gardu_id, waktu_pelaksanaan
    #inisial variable
    $tipe_instalasi = $param['tipe_instalasi'];
    $instalasi = $param['instalasi'];
    $bay_gardu_id = $param['bay_gardu_id'];
    $waktu_pelaksanaan = $param['waktu_pelaksanaan'];

    $no_agenda = '';
    $message = '';
    $alert_type = '';
    $response_code = 0;

    #create post data
    #$lhpp = $param['lhpp'];
    #$instalasi = $param['instalasi'];
    #$tipe_instalasi = $lhpp->tipe_instalasi;
    $kapasitas = 0;
    $pemilik_instalasi = $instalasi->pemilik;

    if ($tipe_instalasi == ID_PEMBANGKIT) {
        $kapasitas = $instalasi->kapasitas_terpasang;
    } elseif ($tipe_instalasi == ID_TRANSMISI) {
        //cek apakah gardu induk atau bukan, jika gardu induk maka ambil data bay nya
        if ($instalasi->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
            $bay = BayGardu::findOrFail($bay_gardu_id);
            $instalasi = InstalasiTransmisi::findOrFail($bay->instalasi_id);
            $instalasi->id = $bay->id;
            #$instalasi->nama_instalasi = $instalasi->nama_instalasi . " (" . $bay->nama_bay . ")";
            #---> Informasi dari PUSERTIF bahwa nama bay tdk perlu dicantumkan
            # karena tidak akan ditulis di sertifikat
        }
        $kapasitas = $instalasi->kapasitas_gi . " MVA";
    } else if ($tipe_instalasi == ID_DISTRIBUSI) {
        $kapasitas = $instalasi->kapasitas_gardu;
    } else if ($tipe_instalasi == ID_PEMANFAATAN_TM) {
        $kapasitas = $instalasi->kapasitas_trafo;
        $tipe_instalasi = '4';
    } else if ($tipe_instalasi == ID_PEMANFAATAN_TT) {
        $kapasitas = $instalasi->kapasitas_trafo;
    }

    $postData = [
        'tanggal' => $waktu_pelaksanaan,
        'pemilik' => [
            'nama' => @$instalasi->pemilik->nama_pemilik,
            'alamat' => (@$instalasi->pemilik->alamat_pemilik == null) ? "-" : @$instalasi->pemilik->alamat_pemilik,
            'kota_id' => $pemilik_instalasi->city->kode_kota,
        ],
        'instalasi' => [
            'nama' => $instalasi->nama_instalasi,
            'lokasi' => ($instalasi->alamat_instalasi == null) ? "-" : $instalasi->alamat_instalasi,
            'kota_id' => $instalasi->kota->kode_kota,
            'jenis' => $tipe_instalasi,
            'kapasitas' => $kapasitas
            ]
        ];
        
//            dd($postData);
        
        #get nomor agenda
        $response = execCurl($postData, DJK_CRT_PMH);
        $dcd_response = json_decode($response);
        //    dd($dcd_response);
        if ($dcd_response == null) {
            $message = 'Koneksi API DJK gagal!';
            $alert_type = 'alert-danger';
            $no_agenda = "-";
        } else {
            if (isset($dcd_response->metadata)) {
                if ($dcd_response->metadata->code == 1) {
                    $response_code = 1;
                    $no_agenda = $dcd_response->response->no_agenda;
                    $message = 'Permohonan Nomor Agenda Berhasil Dilakukan';
                    $alert_type = 'alert-success';
                } else {
                    $message = $dcd_response->metadata->message;
                    $alert_type = 'alert-danger';
                }
            }
        }
        
        $response = [
            'no_agenda' => $no_agenda,
            'message' => $message,
            'alert_type' => $alert_type,
            'response_code' => $response_code
        ];
        //    dd($response);
        #if success save to djk_agenda
        if ($response['response_code'] == 1) {
            $djk_agenda = new DjkAgenda();
            $djk_agenda->instalasi_id = $instalasi->id;
            $djk_agenda->tipe_instalasi_id = $tipe_instalasi;
            $djk_agenda->jenis_instalasi_id = @$instalasi->id_jenis_instalasi;
            $djk_agenda->no_agenda = $response['no_agenda'];
            $djk_agenda->save();
        }
        
        return $response;
    }
    
    function registrasiSLO($lhpp)
    {
        $resp = [];
        $tmp = [];
        $postData = [];
        
        $instalasi = $lhpp->instalasi;
        //    dd($instalasi);
        $instalasi_id = $instalasi->id;
        if ($lhpp->bay_gardu_id != null) {
            $instalasi_id = $lhpp->bay_gardu_id;
        }
        $djk_agenda = DjkAgenda::where('instalasi_id', $instalasi_id)
            ->where('tipe_instalasi_id', $lhpp->tipe_instalasi)
            ->where('jenis_instalasi_id', $instalasi->id_jenis_instalasi)->first();
        $lhpp_instalasi = null;

        #set date 
        $tgl_lhpp = '';
        if($lhpp->tgl_lhpp != null){
            setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
            $tgl_lhpp = strftime ('%d %B %Y', strtotime(@$lhpp->tgl_lhpp));
            $tgl_lhpp = 'Tanggal '. $tgl_lhpp;
        }
        #end set date
        
        if ($djk_agenda != null) {
            $postData = [
                'no_agenda' => @$djk_agenda->no_agenda,
                'no_lhpp' => $lhpp->no_lhpp .' '. $tgl_lhpp,
                'no_slo' => $lhpp->no_slo,
                'kode_sbu' => ($instalasi->kode_kontraktor) ? $instalasi->kode_kontraktor : '0000',
                'nik_pjt' => @$lhpp->pjt()->nik,
                'nik_tt' => @$lhpp->ketua_pelaksana()->nik, #yang menjadi ketua pelaksana adala tenaga teknik
                'url_detail_lhpp' => url(URL_LHPP_DETAIL . $lhpp->id),
                'url_koordinat_lokasi' => url(URL_LHPP_LOKASI . $lhpp->id),
                'url_foto_pelaksanaan' => url(URL_LHPP_FOTO . $lhpp->id)
            ];
        }
        
        if ($lhpp->tipe_instalasi == ID_PEMBANGKIT) {
            $tmp = dataKit($lhpp, $instalasi);
        } else if ($lhpp->tipe_instalasi == ID_TRANSMISI) {
            $tmp = dataTrans($lhpp, $instalasi);
            if ($lhpp->bay_gardu_id != null) {
                $postData['kode_sbu'] = $lhpp->gardu->kode_kontraktor;
            }
        } else if ($lhpp->tipe_instalasi == ID_DISTRIBUSI) {
            $tmp = dataDistribusi($lhpp, $instalasi);
        } else if ($lhpp->tipe_instalasi == ID_PEMANFAATAN_TT || $lhpp->tipe_instalasi == ID_PEMANFAATAN_TM) {
            $tmp = dataPemanfaatan($lhpp, $instalasi);
        }
        
        $postData = array_merge($postData, $tmp);
        $data_null = [];
        foreach ($postData as $key => $item) {
            if ($item == null) {
                array_push($data_null, $key);
            }
        }
        // dd($postData);
        if (sizeof($data_null) > 0) {
            $resp['data_null'] = $data_null;
            $resp['message'] = 'Mohon Lengkapi Data Berikut : ';
            $resp['alert_type'] = 'alert-danger';
        } else {
            $response = execCurl($postData, DJK_REG);
            $dcd_response = json_decode($response);
            // dd($dcd_response);
            if ($dcd_response == null) {
                $resp['message'] = 'Koneksi API DJK gagal!';
                $resp['alert_type'] = 'alert-danger';
            } else {
                if (isset($dcd_response->metadata)) {
                    if ($dcd_response->metadata->code == 1) {
                        $resp['message'] = $dcd_response->metadata->message . '! Nomor Permohonan Berhasil Disimpan';
                        $resp['alert_type'] = 'alert-success';
                        
                        #save to table DJK_PERMOHONAN
                        $djk_registrasi = new DjkRegistrasi();
                        $djk_registrasi->djk_agenda_id = $djk_agenda->id;
                        $djk_registrasi->no_permohonan = $dcd_response->response->no_permohonan;
                        $djk_registrasi->save();
                        #setelah berhasil mendapatkan nomor permohonan
                        #simpan juga no permohonan di tb LHPP
                        $lhpp->no_djk_permohonan = $dcd_response->response->no_permohonan;
                        $lhpp->status_permohonan_djk = DJK_SENT;
                        $lhpp->save();
                    } else {
                        $resp['message'] = $dcd_response->metadata->message;
                        $resp['alert_type'] = 'alert-danger';
                    }
                }
            }
        }

        // dd($resp);
        
        return $resp;
    }
    
    function generateNoSLO($lhpp)
    {
        #generate nomor SLO
        $last_lhpp = Lhpp::whereNotNull('no_urut')
			->orderby('no_urut', 'desc')
			->whereYear('created_at', '=', date('Y'))
			->first();
	
        //    echo ($last_lhpp);exit;
        $instalasi = $lhpp->instalasi;
        
        $no_slo = '';
        $no_urut = '';
        $jenis_ijin_usaha = '';
        $penerbit = '01';
        $kode_jenis_instalasi = $instalasi->jenis_instalasi->kode_instalasi;
        $kota_djk = $instalasi->kota->kode_kota;
        $kepemilikan = '';
        $kode_sistem_jaringan = '';
        $tegangan_pengenal = '';
        $kode_sistem_distribusi = '';
        $kode_sbu = '';
        $tahun = date("y");
        
        /* GENERATE NOMOR SLO
        * Terdiri dari beberapa koponen
        *
        * */
        
        #NOMOR URUT

        if($last_lhpp->no_urut != '999' && is_numeric($last_lhpp->no_urut)){
            $no_urut = ($lhpp->no_urut != null && $lhpp->no_urut > 0) ? $lhpp->no_urut : $last_lhpp->no_urut + 1;
        }else{
            $no_urut = customNumber($last_lhpp->no_urut);
        }

        if($lhpp->created_at->year > $last_lhpp->created_at->year ){
            #need reset true
            $no_urut = '001';
        }

	#formating no_urut
	$no_urut = str_pad($no_urut, 3, '0',STR_PAD_LEFT );

        #JENIS IJIN USAHA
        if (@$instalasi->pemilik->jenis_ijin_usaha == 35) {
            $jenis_ijin_usaha = 'O';
        } else {
            $jenis_ijin_usaha = 'U';
        }
        
        #KEPEMILIKAN INSTALASI
        if (@$instalasi->pemilik->perusahaan->is_pln == 1) {
            $kepemilikan = '1';
        } else {
            $kepemilikan = '2';
        }
        
        #KODE SISTEM JARINGAN TRANSMISI
        #KODE SISTEM JARINGAN DISTRIBUSI
        #TEGANGAN PENGENAL
        if (@$instalasi->sis_jar_tower != null) {
            $ref = References::findorfail(@$instalasi->sis_jar_tower);
            $kode_sistem_jaringan = $ref->value;
        }
        if (@$instalasi->sistem_jaringan != null) {
            $ref = References::findorfail(@$instalasi->sistem_jaringan);
            $kode_sistem_distribusi = $ref->value;
        }
        if (@$instalasi->tegangan_pengenal != null) {
            $ref = References::findorfail(@$instalasi->tegangan_pengenal);
            $tegangan_pengenal = $ref->value;
        }
        
        #KODE SBU(KONTRAKTOR)
        if ($instalasi->kode_kontraktor != null) {
            $kode_sbu = $instalasi->kode_kontraktor;
        } else {
            $kode_sbu = '0000';
        }
        
        if ($instalasi->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
            $ref = References::findorfail(@$lhpp->gardu->tegangan_pengenal);
            $tegangan_pengenal = @$ref->value;
            
            if (@$lhpp->gardu->kode_kontraktor != null) {
                $kode_sbu = @$lhpp->gardu->kode_kontraktor;
            } else {
                $kode_sbu = '0000';
            }
        }
        
        #CREATE NOMOR SLO BERDASARKAN JENIS INSTALASI BERBEDA - BEDDA
        if ($lhpp->tipe_instalasi == ID_PEMBANGKIT || $lhpp->tipe_instalasi == ID_PEMANFAATAN_TM || $lhpp->tipe_instalasi == ID_PEMANFAATAN_TT) {
            $no_slo = $no_urut . '.' . $jenis_ijin_usaha . '.' . $penerbit . '.' . $kode_jenis_instalasi . '.' . $kota_djk . '.' . $kode_sbu . '.' . $tahun;
        } else if ($lhpp->tipe_instalasi == ID_TRANSMISI) {
            $no_slo = $no_urut . '.' . $jenis_ijin_usaha . '.' . $penerbit . '.' . $kode_jenis_instalasi . '.' . $kepemilikan . $kode_sistem_jaringan . $tegangan_pengenal . '.' . $kode_sbu . '.' . $tahun;
        } else if ($lhpp->tipe_instalasi == ID_DISTRIBUSI) {
            $no_slo = $no_urut . '.' . $jenis_ijin_usaha . '.' . $penerbit . '.' . $kode_jenis_instalasi . '.' . $kepemilikan . $kode_sistem_distribusi . $tegangan_pengenal . '.' . $kode_sbu . '.' . $tahun;
        }
        
        $arr = [
            'no urut' => $no_urut,
            'jenis ijin usaha' => $jenis_ijin_usaha,
            'penerbit' => $penerbit,
            'kode jenis instalasi' => $kode_jenis_instalasi,
            'kepemilikan' => $kepemilikan,
            'PEMBANGKIT kota' => $kota_djk,
            'TRANSMISI kode jaringan' => $kode_sistem_jaringan,
            'TRANSMISI tegangan pengenal' => $tegangan_pengenal,
            'DISTRIBUSI sistem jaringan' => $kode_sistem_distribusi,
            'kode sbu' => $kode_sbu,
            'tahun' => $tahun
        ];
        //    dd($arr);
        $response = [
            'no_slo' => $no_slo,
            'no_urut' => $no_urut
        ];
        //    dd($response);
        return $response;
        #end generate nomor SLO
        
    }
    
    function dataKit($lhpp, $instalasi)
    {
        $tmp = [];
        
        if ($lhpp != null && $instalasi != null) {
            $lhpp_instalasi = $lhpp->lhpp_kit;
            $bahan_bakar = '';
            if (@$instalasi->id_bahan_bakar != null) {
                $ref = References::findorfail($instalasi->id_bahan_bakar);
                $bahan_bakar = $ref->value;
            }
            
            $tmp = [
                'jenis_instalasi' => $instalasi->jenis_instalasi->kode_instalasi,
                'latitude' => $instalasi->latitude,
                'longitude' => $instalasi->longitude,
                'kapasitas_terpasang' => $instalasi->kapasitas_terpasang,
                'kapasitas_hasil_uji' => $lhpp_instalasi->kapasitas_hasil_uji,
                'no_unit_pembangkit' => $instalasi->nomor_pembangkit,
                'no_seri_generator' => $instalasi->nomor_generator,
                'no_seri_turbin' => $instalasi->nomor_mesin,
                'kapasitas_modul_per_unit' => ($lhpp_instalasi->kapasitas_modul == null) ? 0 : $lhpp_instalasi->kapasitas_modul,
                'kapasitas_inverter_per_unit' => ($lhpp_instalasi->kapasitas_inverter == null) ? 0 : $lhpp_instalasi->kapasitas_inverter,
                'jumlah_modul' => ($lhpp_instalasi->jumlah_modul == null) ? 0 : $lhpp_instalasi->jumlah_modul,
                'jumlah_inverter' => ($lhpp_instalasi->jumlah_inverter == null) ? 0 : $lhpp_instalasi->jumlah_inverter,
                'bahan_bakar' => $bahan_bakar,
                'net_plant_heat_rate_hhv' => $lhpp_instalasi->kalori_hhv,
                'sfc' => $lhpp_instalasi->kalori_lhv,
                'url_file_konsumsi_bahan_bakar' => url('/upload').'/'.$lhpp_instalasi->url_file_konsumsi_bb,
            ];
        }
        
        return $tmp;
    }
    
    function dataTrans($lhpp, $instalasi)
    {
        $tmp = [];
        
        if ($lhpp != null && $instalasi != null) {
            
            if (@$instalasi->tegangan_pengenal != null) {
                $ref = References::findorfail(@$instalasi->tegangan_pengenal);
                $tegangan_pengenal = $ref->value;
            } else {
                $tegangan_pengenal = "";
            }
            
            $tmp = [
                'jenis_instalasi' => $instalasi->jenis_instalasi->kode_instalasi,
                'latitude' => $instalasi->lat_awal,
                'longitude' => $instalasi->long_awal,
                'panjang_saluran' => ($instalasi->panjang_tt == null) ? '0' : $instalasi->panjang_tt,
                'kapasitas_gardu_induk' => $instalasi->kapasitas_gi,
                'line_bay' => '-',
                'bus_coupler_bay' => '-',
                'transformer_bay' => '-',
                'kapasitas_pemutus_tenaga' => (@$instalasi->kap_pemutus_tenaga == null) ? '-' : $instalasi->kap_pemutus_tenaga,
                'kapasitas_trafo_tenaga' => (@$instalasi->kap_tf_tenaga == null) ? '-' : $instalasi->kap_tf_tenaga,
                'tegangan_pengenal' => $tegangan_pengenal,
                'jumlah_tower' => ($instalasi->jml_tower == null) ? '0' : $instalasi->jml_tower
            ];
            
            //jika jenis instalasi gardu induk, maka masukan data bay nya
            if (@$instalasi->jenis_instalasi->kode_instalasi == KODE_GARDU_INDUK_TRANSMISI) {
                $bay = $lhpp->gardu;
                $jenis_bay = @$bay->jenis_bay;
                $tmp['kapasitas_pemutus_tenaga'] = (@$bay->kapasitas_pemutus == null) ? '-' : @$bay->kapasitas_pemutus;
                $tmp['kapasitas_trafo_tenaga'] = (@$bay->kapasitas_trafo == null) ? '-' : @$bay->kapasitas_trafo;
                switch ($jenis_bay) {
                    case BAY_LINE :
                    $tmp['line_bay'] = $bay->nama_bay;
                    break;
                case BAY_COUPLER :
                    $tmp['bus_coupler_bay'] = $bay->nama_bay;
                    break;
                case BAY_TRANSFORMATOR :
                    $tmp['transformer_bay'] = $bay->nama_bay;
                    break;
                case BAY_REAKTOR :
                    $tmp['transformer_bay'] = $bay->nama_bay;
                    break;
                case BAY_KAPASITOR :
                    $tmp['transformer_bay'] = $bay->nama_bay;
                    break;
            }
        }
    }

    return $tmp;
}

function dataDistribusi($lhpp, $instalasi)
{

    $tmp = [];

    if ($lhpp != null && $instalasi != null) {

        if (@$instalasi->tegangan_pengenal != null) {
            $ref = References::findorfail(@$instalasi->tegangan_pengenal);
            $tegangan_pengenal = $ref->value;
        } else {
            $tegangan_pengenal = "";
        }

        $tmp = [
            'jenis_instalasi' => $instalasi->jenis_instalasi->kode_instalasi,
            'latitude' => $instalasi->latitude_awal,
            'longitude' => $instalasi->longitude_awal,
            'panjang_saluran' => $instalasi->panjang_kms,
            'jumlah_gardu_distribusi' => $instalasi->jml_gardu,
            'kapasitas_gardu_distribusi' => $instalasi->kapasitas_gardu,
            'jumlah_panel' => $instalasi->jumlah_panel,
            'kapasitas_arus_hubung_singkat' => $instalasi->kapasitas_arus_hubung_singkat,
            'tegangan_pengenal' => $tegangan_pengenal
        ];
    }

    return $tmp;
}


function dataPemanfaatan($lhpp, $instalasi)
{

    $tmp = [];

    if ($lhpp != null && $instalasi != null) {

        $tmp = [
            'jenis_instalasi' => $instalasi->jenis_instalasi->kode_instalasi,
            'latitude' => $instalasi->latitude_awal,
            'longitude' => $instalasi->longitude_awal,
            'kapasitas_terpasang' => $instalasi->kapasitas_trafo,
            'daya_tersambung' => $instalasi->daya_sambung,
            'phb_tegangan_menengah' => $instalasi->phb_tm,
            'phb_tegangan_rendah' => $instalasi->phb_tr,
            'penyedia_tenaga_listrik' => $instalasi->penyedia_tl
        ];
    }

    return $tmp;
}

function updateStatus($request)
{
    // dd($request);
    $djk_agenda = DjkAgenda::findorfail($request->djk_agenda_id);
    $djk_log_status = new DjkLogStatus();
    $status = References::findorfail($request->status);

    $message = '';
    $alert_type = '';

    $postData = [
        'no_agenda' => $request->no_agenda,
        'status' => $status->value,
        'keterangan' => $request->keterangan,
    ];

    $response = execCurl($postData, DJK_UPDT_PMH);

    #get response
    $dcd_response = json_decode($response);
    // dd($dcd_response);
    if ($dcd_response == null) {
        $message = 'Koneksi API DJK gagal!';
        $alert_type = 'alert-danger';
    } else {
        if (isset($dcd_response->metadata)) {
            if ($dcd_response->metadata->code == 1) {
                $message = $dcd_response->metadata->message;
                $alert_type = 'alert-success';

                $djk_log_status->status_id = $request->status;
                $djk_log_status->keterangan = $request->keterangan;
                $djk_log_status->user_id = @Auth::user()->id;
                $djk_log_status->djk_agenda_id = $request->djk_agenda_id;
                $djk_log_status->save();

                if ($djk_log_status->save()) {
                    $djk_agenda->status_id = $request->status;
                    $djk_agenda->keterangan = $request->keterangan;
                    $djk_agenda->save();
                }
            } else {
                $message = $dcd_response->metadata->message;
                $alert_type = 'alert-danger';
            }
        }
    }

    $response = [
        'message' => $message,
        'alert_type' => $alert_type
    ];

    return $response;
}

function saveFile($req_file, $param_file, $id)
{
    $id = "0";
    $timestamp = date('YmdHis');
    $nm_u_custom = '-' . $timestamp . '-' . $id . '.';
    $filename = $param_file . $nm_u_custom . $req_file->getClientOriginalExtension();
    $filename = str_replace("/", "_", $filename);
    $fullPath = storage_path() . '/upload' . '/' . $param_file . '/' . $filename;
    if (File::exists($fullPath))
        File::delete($fullPath);
        $req_file->move(storage_path() . '/upload' . '/' . $param_file, $filename);
        
        return $filename;
    }
    
    function getHasilSLO($param = null)
    {
        $djk_hasil = DjkHasil::orderBy('created_at', 'desc')->get();
        $message = '';
        $alert_type = '';
        $permohonan_djk = null;
        $isInsert = false;
        $lhpp = null;
        if($param != null){
            $response = execCurl($param, DJK_HASIL);
        }else{
            $response = execCurl(null, DJK_HASIL);
        }
        
        $dcd_response = json_decode($response);

        if ($dcd_response == null) {
            $message = 'Koneksi API DJK gagal!';
            $alert_type = 'alert-danger';
        } else {
            if (isset($dcd_response->metadata)) {
                if ($dcd_response->metadata->code == 1) {
                    $message = $dcd_response->metadata->message;
                    $alert_type = 'alert-success';
                    $permohonan_djk = $dcd_response->permohonan;
                } else {
                    $message = $dcd_response->metadata->message;
                    $alert_type = 'alert-danger';
                }
            }
        }
        
        $count_diterima = 0;
        $count_ditolak = 0;
        if ($permohonan_djk != null) {
            foreach ($permohonan_djk as $key => $pm) {
                $p_match = $djk_hasil->where('no_permohonan', $pm->no_permohonan);
                $lhpp = Lhpp::where('no_djk_permohonan', $pm->no_permohonan)->first();
                if (sizeof($p_match) > 0) {
                    #find object untuk update data
                    $p_match = $p_match->first();
                    $p_existing = DjkHasil::findorfail($p_match->id);
                    #cek apakah ada perubahan dengan hasil sebelumnya
                    #jika ada perubahan, create new record
                    if(($p_existing->hasil_registrasi != $pm->hasil_registrasi && $p_existing->alasan_penolakan != @$pm->alasan_penolakan)
                        || ($p_existing->hasil_registrasi == $pm->hasil_registrasi && $p_existing->alasan_penolakan != @$pm->alasan_penolakan)){
                        $isInsert = true;
                    }
                }
                else {
                    $isInsert = true;
                }
                
                #lakukan insert new record ke table DJK_HASIL
                if($isInsert){
                    if($lhpp !=  null){
                        $new_djk_hasil = new DjkHasil();
                        $new_djk_hasil->hasil_registrasi = @$pm->hasil_registrasi;
                        $new_djk_hasil->no_permohonan = @$pm->no_permohonan;
                        $new_djk_hasil->tanggal_terbit = @$pm->tanggal_terbit;
                        $new_djk_hasil->tanggal_berakhir = @$pm->tanggal_berakhir;
                        $new_djk_hasil->alasan_penolakan = @$pm->alasan_penolakan;
                        $new_djk_hasil->url_sertifikat = @$pm->url_sertifikat;
                        $new_djk_hasil->save();
                        if ($pm->hasil_registrasi == 1) {
                            $lhpp->status_permohonan_djk = DJK_APPROVED;
                            $lhpp->save();
                            $count_diterima++;
                        } else {
                            $lhpp->status_permohonan_djk = DJK_REJECTED;
                            $lhpp->save();
                            $count_ditolak++;
                        }
                    }
                }
                $lhpp = null;
            }
        }
        
        $response = [
            'message' => 'Update Hasil ' . $message . '! ' . $count_diterima . ' Permohonan Baru Diterima, ' . $count_ditolak . ' Permohonan Baru Ditolak',
            'alert_type' => $alert_type
        ];
        
        // dd($response);

        Session::flash('message', $response['message']);
        Session::flash('alert_type', $response['alert_type']);
        
    }

function InstalasiHasSchedule(){
    $jadwal = JadwalPelaksanaan::orderBy('id', 'desc')->paginate(100);
    // dd($jadwal[0]->kontrak);
}

function customNumber($param){
    $init_num = $param;
    $init_num = strval($init_num);
    $arr_num = str_split($init_num);
    $arr_alp = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $init_num_index = count($arr_num) - 1;
    // dd($arr_num);
    $isTbh = false;
    $isLast = false;
    $new_seq = array();
    $tiga = $arr_num[2];
    $dua = $arr_num[1];
    $satu = $arr_num[0];

    #digit ketiga
    if(is_numeric($tiga) && $tiga < 9){
        $tiga++;
        $isTbh = true;
    }elseif($tiga == 9){
        $tiga = 0;
        #cek digit #2 apakah alpha?
        if(!is_numeric($dua)){
            #apakah digit dua alpha habis?
            if($dua == 'Z'){
                $tiga = $arr_alp[0]; #first alpha
            }
        }
    }
    #cek jika !isTbh dan digit #3 alpha
    elseif(!is_numeric($tiga)){
        if(!$isTbh){
            $index = array_search($tiga, $arr_alp);
            if($index != 25){
                $tiga = $arr_alp[$index + 1];
                $isTbh = true;
            }else{
                $isLast = true;
            }
        }
    }



    #digit kedua
    if(is_numeric($dua) && $dua < 9){
        if(!$isTbh){
            $dua++; 
            $isTbh = true;
        }
    }elseif($dua == 9){
        if(!$isTbh){
            $dua = 0;
            #cek digit #1 apakah alpha?
            if(!is_numeric($satu)){
                $dua = $arr_alp[0]; #first alpha
            }
        }
    }
    #cek jika !isTbh dan digit #2 alpha
    elseif(!is_numeric($dua)){
        if(!$isTbh){
            $index = array_search($dua, $arr_alp);
            if($index != 25){
                $dua = $arr_alp[$index + 1];
                $isTbh = true;
            }
        }
    }




    #digit pertama
    if(is_numeric($satu) && $satu < 9){
        (!$isTbh) ? $satu++ : $satu = $satu;
    }elseif($satu == 9){
        if(!$isTbh){
            $satu = $arr_alp[0]; #first alpha
        }
    }
    #cek jika !isTbh dan digit #1 alpha
    elseif(!is_numeric($satu)){
        if(!$isTbh && $isLast){
            $index = array_search($satu, $arr_alp);
            if($index != 25){
                $satu = $arr_alp[$index + 1];
                $dua = 0;
                $tiga = 1; 
                $isTbh = true;
            }
        }
    }

    if((!is_numeric($dua) && $tiga == 0) || $dua == 0 && $tiga == 0){
        $tiga++;
    }
    $new_seq[2] = $tiga;
    $new_seq[1] = $dua;
    $new_seq[0] = $satu;
    $init_num = $new_seq[0].$new_seq[1].$new_seq[2];
    return $init_num;
}


    
    
