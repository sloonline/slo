<?php
/**
 * Created by PhpStorm.
 * User: Rezpa Aditya
 * Date: 11/15/2016
 * Time: 10:11 PM
 */

function editWord($template){
    $source = \Illuminate\Support\Facades\URL::asset('template_word/'.$template);
    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($source);
    return $templateProcessor;
}

function saveWord($templateProcessor, $filename){
    $templateProcessor->saveAs(storage_path('exports/'.$filename));
}
