<?php
use Illuminate\Support\Facades\Mail;
use App\LogEmail;

/**
 * Created by PhpStorm.
 * User: renisaskusumah
 * Date: 9/11/2016
 * Time: 9:32 PM
 */

/*global function to send message, and after message sent, store to database as log email record*/
/*Send mail function
 * parameter (in array): 1. email view file
 *                       2. email data alias
 *                       3. array email data
 *                       4. subject
 *                       5. description
 *                       6. to
 *                       7. name
 *                       8. action (created,approved,pending,etc)
 *                       9. modul (user,order, dll)
 * */
function sendEmail($data, $view, $action, $modul)
{
    $email_data = $data['data'];
    $alias = (isset($data['alias']) ? $data['alias'] : $data['data_alias']);
    $issent = false;
    $sent_log = "";
//    try {
//        Mail::send($view, [$alias => $email_data], function ($m) use ($data) {
//            $m->from('pusertif@pln.co.id', 'PLN Pusertif');
//            $m->to($data['to'], $data['to_name'])->subject($data['subject']);
//        });
//        if (count(Mail::failures()) > 0) {
//            $issent = false;
//            foreach (Mail::failures as $fail) {
//                $sent_log .= " " . $fail;
//            }
//        }else{
//            $issent = true;
//        }
//    } catch (Swift_TransportException $e) {
//        $issent = false;
//        $sent_log = $e->getMessage();
//    }
    storeEmailLog($data, $view, $action, $modul, $issent, $sent_log);
}

function storeEmailLog($data, $view, $action, $modul, $issent, $sent_log)
{
    //create email object to store to log email
    $alias = (isset($data['alias']) ? $data['alias'] : $data['data_alias']);
    $log = new LogEmail();
    $log->to = $data['to'];
    $log->to_name = $data['to_name'];
    $log->subject = $data['subject'];
    $log->file_view = $view;
    $log->email_data_alias = $alias;
    $log->email_data = json_encode($data['data']);
    $log->modul = $modul;
    $log->action = $action;
    $log->description = $data['description'];
    $log->status = ($issent == false) ? "CREATED" : "SENT";
    $log->status_message = $sent_log;

    $log->save();
}

function resendEmail($id_email)
{
    $mail_log = LogEmail::findOrFail($id_email);
    $email_data = json_decode($mail_log->email_data);
    $data = $mail_log;
//    $email_data = (array)$email_data;
    $issent = false;
    try {
        Mail::send($mail_log->file_view, [$mail_log->email_data_alias => $email_data], function ($m) use ($data) {
            $m->from('epusertif@pln.co.id', 'PLN Pusertif');
            $m->to($data['to'], $data['to_name'])->subject($data['subject']);
        });
        if (count(Mail::failures()) > 0) {
            $issent = false;
        } else {
            $issent = true;
        }
        $sent_log = "";
    } catch (Swift_TransportException $e) {
        $issent = false;
        $sent_log = $e->getMessage();
    } catch (Exception  $e) {
        $issent = false;
        $sent_log = $e->getMessage();
    }
    //jika aksi kirim adalah pertama kali (lanjutan dari created) atau sebelumnya not sent maka jika issent = true, update row log
    if ($mail_log->status == "CREATED" || $mail_log->status == "NOT SENT") {
        updateEmailLog($id_email, $issent, $sent_log);
    }
    return array("sent" => $issent, "log" => $sent_log);
}


function updateEmailLog($id_email, $issent, $sent_log)
{
    //create email object to store to log email
    $log = LogEmail::findOrFail($id_email);
    $log->status = ($issent == false) ? "NOT SENT" : "SENT";
    $log->status_message = $sent_log;

    $log->save();
}
