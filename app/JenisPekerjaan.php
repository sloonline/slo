<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPekerjaan extends Model
{
    protected $table = 'jenis_pekerjaan';

    protected $fillable = [
                    'jenis_pekerjaan',

    ];

    public function kombinasi_pekerjaan(){
    	return $this->hasMany('App\KombinasiPekerjaan');
    }

}
