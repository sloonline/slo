<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InstalasiPemanfaatanTM extends Model
{
    use SoftDeletes;
    protected $table = 'instalasi_pemanfaatan_tm';

    protected $fillable = [
                    'id_jenis_instalasi','nama_instalasi','alamat_instalasi','id_provinsi',
        'id_kota','longitude_awal','latitude_awal','longitude_akhir','latitude_akhir',
        'kapasitas_trafo','daya_sambung','phb_tm','phb_tr','penyedia_tl','tegangan_pengenal','surat_pernyataan','kontraktor'

    ];

    public function jenis_instalasi(){
        return $this->belongsTo('App\JenisInstalasi', 'id_jenis_instalasi', 'id');
    }
    public function provinsi(){
        return $this->belongsTo('App\Provinces', 'id_provinsi', 'id');
    }
    public function kota(){
        return $this->belongsTo('App\Cities', 'id_kota', 'id');
    }
    public function provinsi_akhir(){
        return $this->belongsTo('App\Provinces', 'id_provinsi_akhir', 'id');
    }
    public function kota_akhir(){
        return $this->belongsTo('App\Cities', 'id_kota_akhir', 'id');
    }
    public function pemilik(){
        return $this->belongsTo('App\PemilikInstalasi', 'pemilik_instalasi_id', 'id');
    }

    public function teganganPengenal(){
        return $this->belongsTo('App\References','tegangan_pengenal','id');
    }

    public function getPermohonan(){
        $permohonan = Permohonan::where('id_instalasi',$this->id)
            ->where('id_tipe_instalasi', '5')
                ->orderBy('tanggal_permohonan')->get();

        return $permohonan;
    }
      public function getRiwayat() {
        $permohonan = Permohonan::select('permohonan.*,orders.nomor_order','orders.tanggal_order','produk.produk_layanan')
                ->join('orders','orders.id = permohonan.id_orders')
                ->join('produk','produk.id = permohonan.id_produk')
                ->where('permohonan.id_instalasi', $this->id)
                ->where('permohonan.id_tipe_instalasi', '5')
                ->whereIn('orders.id',RAB::select('order_id')->get())
                ->orderBy('orders.tanggal_order','asc','permohonan.id','asc')->get();

        return $permohonan;
    }

    public function kontraktor(){
        return $this->belongsTo('App\Kontraktor', 'kode_kontraktor', 'kode');
    }

    public function isAllowEdit(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
            ->where('id_tipe_instalasi', ID_PEMANFAATAN_TM)
            ->orderBy('tanggal_permohonan')->get();
        if(sizeof($permohonan) > 0){
            return false;
        }else{
            return true;
        }
    }

    public function djk_agenda()
    {
        return $this->hasOne('App\DjkAgenda', 'instalasi_id', 'id')->where('tipe_instalasi_id',ID_PEMANFAATAN_TM);
    }

    public function lhpp()
    {
        return $this->hasOne('App\Lhpp', 'instalasi_id', 'id')->where('tipe_instalasi',ID_PEMANFAATAN_TM);
    }

    public static function total(){
        $data = DB::table('instalasi_pemanfaatan_tm')
            ->select('count(id) as total')
            ->get();
        return $data[0]->total;
    }

    public function isOrderRej(){
        $permohonan = Permohonan::where('id_instalasi', $this->id)
        ->where('id_tipe_instalasi', ID_PEMANFAATAN_TM)
        ->join('orders', 'orders.id','=', 'permohonan.id_orders')
        ->where('orders.id_flow_status', 3) #order rejected
        ->orderBy('tanggal_permohonan')->get();
        if (sizeof($permohonan) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function totalByPemintaJasa(){
        $data = DB::table('instalasi_pemanfaatan_tm')
            ->select('count(id) as total')
            ->where('created_by', Auth::user()->username)
            ->get();
        return $data[0]->total;

    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'username');
    }
}
