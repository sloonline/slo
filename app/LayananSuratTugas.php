<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LayananSuratTugas extends Model
{
    protected $table = 'layanan_surat_tugas';

    public function produk(){
        return $this->belongsTo('App\Produk','produk_id','id');
    }

    public function suratTugas(){
        return $this->belongsTo('App\SuratTugas','surat_tugas_id','id');
    }
}
