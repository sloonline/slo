<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlowHistory extends Model
{
    protected $table = 'flow_history';

    public function workflow(){
        return $this->belongsTo('App\Workflow','workflow_id','id');
    }
}
