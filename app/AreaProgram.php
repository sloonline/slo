<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaProgram extends Model
{
    //
    protected $table = 'area_program';

    public function program(){
        return $this->belongsTo('App\Program', 'id_program', 'id');
    }

    public function businessArea(){
        return $this->belongsTo('App\BusinessArea', 'business_area', 'id');
    }

    public function garduArea(){
        return $this->hasMany('App\GarduArea', 'id_area_program', 'id');
    }

    public function lokasiArea(){
        return $this->hasMany('App\LokasiArea', 'id_area_program', 'id');
    }

    public function penyulangArea(){
        return $this->hasMany('App\PenyulangArea', 'id_area_program', 'id');
    }

    public function child($tipe){
        $data = array();
        switch($tipe){
            case TIPE_GARDU:
                foreach ($this->garduArea as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->garduInduk->gardu_induk;
                    $row->instalasi = $item->instalasi;
                    array_push($data, $row);
                }
                break;
            case TIPE_LOKASI:
                foreach ($this->lokasiArea as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->lokasi->lokasi;
                    $row->instalasi = $item->instalasi;
                    array_push($data, $row);
                }
                break;
            case TIPE_PENYULANG:
                foreach ($this->penyulangArea as $item) {
                    $row = (object)array();
                    $row->id = $item->id;
                    $row->name = $item->penyulang->penyulang;
                    $row->instalasi = $item->instalasi;
                    array_push($data, $row);
                }
                break;

        }
        return $data;

    }

    use SoftDeletes;
}
